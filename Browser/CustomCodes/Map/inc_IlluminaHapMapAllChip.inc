<?php
// loaded in main:
//   - track xml ($Array)
//   - $colormap if present
//   - groupname ($prevgroup) ==> is current one !
//   - $plotruler  : 1 if first track from new group


// get the HapMap Control projects per chiptype
$chipq = mysql_query("SELECT ID,Controles FROM chiptypes");
$chipstring = '';
while ($row = mysql_fetch_array($chipq)) {
	if ($row['Controles'] == '') {
		continue;
	}
	$chipstring .= $row['ID'] . '-';
}
$chipstring = substr($chipstring,0,-1);
// 2. Get the data into an array
$data = array();
$source = $Array->{'map'}->{'source'};
if ($source == 'database') {
	$querystring = '';
	// check for packing-dependent queries
	$queries = $Array->xpath('map/database/query');
	if (count($queries) > 1) { // multiple queries, search packing-match
		$lastquery = '';
		foreach ($queries as $querynode) {
			$lastquery = $querynode;
			if ($querynode->attributes()->{'packing'} == "$packing") {
				$querystring = $querynode;
				break;
			}		
		}
		// pick last query if no packing match.
		if ($querystring == '') {
			$querystring = $lastquery;
		}
	}
	else { // only a single query, use this.
		$querystring = $Array->{'map'}->{'database'}->{'query'};
	}
	$querystring = str_replace('%start%', $start, $querystring);
	$querystring = str_replace('%stop%', $stop, $querystring);
	$querystring = str_replace('%chr%', $chr, $querystring);
	$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
	$querystring = str_replace('%project%', $project, $querystring);
	$querystring = str_replace('%userid%',$userid,$querystring);
	$querystring = str_replace('%condensepart%','',$querystring);
	$querystring = str_replace('%chipstring%',$chipstring,$querystring);
	$query = mysql_query($querystring);
	while ($row = mysql_fetch_array($query)) {
		$data[] = $row;
	}
}
if (isset($Array->{'map'}->{'onHover'}->{$packing})) {
	$onHover = $Array->{'map'}->{'onHover'}->{$packing};

	switch($packing) {
		case 'full':
			$nrrows = print_map_full($onHover,$data, $y);	
			$y += $nrrows*10;
			$fullheight += 17 + $nrrows*10 + $newgroup*12;
			break;
		case 'pack';
			$y = print_map_pack($onHover,$data,$y);
			$fullheight += 14 + $newgroup*12 + $afterfull*3;
			break;
	}
}



?>
