<?php
// loaded in main:
//   - track xml ($Array)
//   - $colormap if present
//   - groupname ($prevgroup) ==> is current one !
//   - $plotruler  : 1 if first track from new group

$displayname = $Array->{'plot'}->{'displayname'};
$tmpheight = 17 + 23;

if ($plotruler == 1) {
	$fulloffset = 5;
	$tmpheight += 10;
}
else {
	$fulloffset = 0; 
}

// create handle for this subimage
$positions[$trackname] = $fullheight;
$heights[$trackname] = $tmpheight;
$fullheight += $tmpheight;
$subimages[$trackname] = ImageCreate($width,$tmpheight);
$colors = array();
$colors = LoadColors($subimages[$trackname],'Main_Colors.xml');
#imagefill($subimages[$trackname],0,0,$colors['white']);
DrawGrid($subimages[$trackname],$xoff,$tmpheight,$start,$stop,$scalef,$colors['lightblue']);
$y = 0;

// top & bottom lines
imageline($subimages[$trackname],$xoff,$fulloffset,$width,$fulloffset,$colors['lightgray']);
imageline($subimages[$trackname],$xoff,$tmpheight-1,$width,$tmpheight-1,$colors['lightgray']);

// draw trackname
$y = 0;
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($displayname)*$fontwidth;
$txtx = $xoff + ($width-$xoff)/2 - $txtwidth/2;
if ($plotruler == 1) {
	imagestring($subimages[$trackname],2,$txtx,$y-6,$displayname,$colors['black']);
	$y-3;
}
else {
	imagestring($subimages[$trackname],2,$txtx,$y-1,$displayname,$colors['black']);
	$y+=2;
}

#$y = $y+10;	
# LOAD DGV TRACK for selected studies
$dgvtypes = array(1 => "Gain", 2 => "Loss", 3 => "inv");
$dgvcolors = array(1 => 'blue', 2 => 'red', 3 => 'mediumgray');
$dgvtitles = array(1 => "CN Gain", 2 => "CN Loss", 3 => "Inversion");
// DGV gold has only gain and loss
for ($i = 1; $i <= 2; $i++) {
	$y = $y+10;
	//print label
	$string = "$dgvtitles[$i]";
	$fontwidth = imagefontwidth(2);
	$txtwidth = strlen($string)*$fontwidth;
	$txtx = $xoff-$txtwidth-3;
	imagestring($subimages[$trackname],2,$txtx,$y-2,$string,$colors['black']);
	$query = mysql_query("SELECT start, stop, max_start, max_stop FROM DGV_gold WHERE chr = '$chr' AND vsubtype = '".$dgvtypes[$i]."' AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) ORDER BY start, stop");
	#imagestring($image,2,$xoff,$y-5,$qstring,$black);	
	while ($abrow = mysql_fetch_array($query)) {
		#$y = $y+10;
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
        $cmstart = $abrow['max_start'];
        $cmstop = $abrow['max_stop'];
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		// min region
        imagefilledrectangle($subimages[$trackname],$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+7,$colors[$dgvcolors[$i]]);
        // max region.
        $scaled_max_start = max(0,intval(round(($cmstart-$start) * $scalef)));
        $scaled_max_stop = max(0,intval(round(($cmstop-$start) * $scalef)));
        imageline($subimages[$trackname],$xoff+$scaled_max_start,$y+5,$xoff+$scaledstart,$y+5,$colors[$dgvcolors[$i]]);
        imageline($subimages[$trackname],$xoff+$scaled_max_start,$y+2,$xoff+$scaled_max_start,$y+7,$colors[$dgvcolors[$i]]);
        imageline($subimages[$trackname],$xoff+$scaled_max_stop,$y+5,$xoff+$scaledstop,$y+5,$colors[$dgvcolors[$i]]);
        imageline($subimages[$trackname],$xoff+$scaled_max_stop,$y+2,$xoff+$scaled_max_stop,$y+7,$colors[$dgvcolors[$i]]);
	}
}


?>
