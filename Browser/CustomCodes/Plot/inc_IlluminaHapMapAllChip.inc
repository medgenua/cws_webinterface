<?php
// loaded in main:
//   - track xml ($Array)
//   - $colormap if present
//   - groupname ($prevgroup) ==> is current one !
//   - $plotruler  : 1 if first track from new group


// get the HapMap Control projects per chiptype
$chipq = mysql_query("SELECT ID,Controles FROM chiptypes");
$chipstring = '';
while ($row = mysql_fetch_array($chipq)) {
	if ($row['Controles'] == '') {
		continue;
	}
	$chipstring .= $row['ID'] . '-';
}
$chipstring = substr($chipstring,0,-1);
// 2. Get the data into an array
$data = array();
$displayname = $Array->{'plot'}->{'displayname'};
$source = $Array->{'plot'}->{'source'};
if ($source == 'database') {
	$querystring = '';
	// check for packing-dependent queries
	$queries = $Array->xpath('plot/database/query');
	if (count($queries) > 1) { // multiple queries, search packing-match
		$lastquery = '';
		foreach ($queries as $querynode) {
			$lastquery = $querynode;
			if ($querynode->attributes()->{'packing'} == "$packing") {
				$querystring = $querynode;
				break;
			}		
		}
		// pick last query if no packing match.
		if ($querystring == '') {
			$querystring = $lastquery;
		}
	}
	else { // only a single query, use this.
		$querystring = $Array->{'plot'}->{'database'}->{'query'};
	}
	$querystring = str_replace('%start%', $start, $querystring);
	$querystring = str_replace('%stop%', $stop, $querystring);
	$querystring = str_replace('%chr%', $chr, $querystring);
	$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
	$querystring = str_replace('%project%', $project, $querystring);
	$querystring = str_replace('%userid%',$userid,$querystring);
	$querystring = str_replace('%condensepart%','',$querystring);
	$querystring = str_replace('%chipstring%',$chipstring,$querystring);
	$query = mysql_query($querystring);
	while ($row = mysql_fetch_array($query)) {
		$data[] = $row;
	}
}
// run countquery to determine height
if (isset($Array->{'plot'}->{'database'}->{'countquery'})){
	$queries = $Array->xpath('plot/database/countquery');
	if (count($queries) > 1) { // multiple queries, search packing-match
		$lastquery = '';
		foreach ($queries as $querynode) {
			$lastquery = $querynode;
			if ($querynode->attributes()->{'packing'} == "$packing") {
				$querystring = $querynode;
				break;
			}		
		}
		// pick last query if no packing match.
		if ($querystring == '') {
			$querystring = $lastquery;
		}
	}
	else { // only a single query, use this.
		$querystring = $Array->{'plot'}->{'database'}->{'query'};
	}
	$querystring = $Array->{'plot'}->{'database'}->{'countquery'};
	$querystring = str_replace('%start%', $start, $querystring);
	$querystring = str_replace('%stop%', $stop, $querystring);
	$querystring = str_replace('%chr%', $chr, $querystring);
	$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
	$querystring = str_replace('%project%', $project, $querystring);
	$querystring = str_replace('%userid%',$userid,$querystring);
	$querystring = str_replace('%chipstring%',$chipstring,$querystring);
	$query = mysql_query($querystring);
	$tmpheight = 17 + 10*mysql_num_rows($query);
}
else {
	$tmpheight = 17 + 10*count($data);
}
// Y-offset
if ($plotruler == 1) {
	$fulloffset = 5;
	$tmpheight += 10;
}
else {
	$fulloffset = 0; 
}

// create handle for this subimage
$positions[$trackname] = $fullheight;
$heights[$trackname] = $tmpheight;
$fullheight += $tmpheight;
$subimages[$trackname] = ImageCreate($width,$tmpheight);
$colors = array();
$colors = LoadColors($subimages[$trackname],'Main_Colors.xml');
#imagefill($subimages[$trackname],0,0,$colors['white']);
DrawGrid($subimages[$trackname],$xoff,$tmpheight,$start,$stop,$scalef,$colors['lightblue']);

$y = 0;
// top & bottom lines
imageline($subimages[$trackname],$xoff,$fulloffset,$width,$fulloffset,$colors['lightgray']);
imageline($subimages[$trackname],$xoff,$tmpheight-1,$width,$tmpheight-1,$colors['lightgray']);
//trackname
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($displayname)*$fontwidth;
$txtx = $xoff + ($width-$xoff)/2 - $txtwidth/2;
if ($plotruler == 1) {
	imagestring($subimages[$trackname],2,$txtx,$y-6,$displayname,$colors['black']);
	$y+=7;
}
else {
	imagestring($subimages[$trackname],2,$txtx,$y-1,$displayname,$colors['black']);
	$y+=12;
}

// process entries
$prevgrouping = '';
$prevlabel = '';
foreach ($data as $key => $row) {
	if (isset($row['grouping'])) {
		if ($row['grouping'] != $prevgrouping) {
			//new item, print name
			if (strlen($prevlabel) > 19) {
				$printlabel = '..'.substr($prevlabel, (strlen($prevlabel)-18));
			}
			else {
				$printlabel = $prevlabel;
			}
			$fontwidth = imagefontwidth(2);
			$txtwidth = strlen($printlabel)*$fontwidth;
			$txtx = $xoff-$txtwidth-3;
			imagestring($subimages[$trackname],2,$txtx,$y-2,$printlabel,$colors['black']);
			$prevlabel = $row['grouplabel'];
			// increase $y if second group
			if ($prevgrouping != '') {
				$y +=10;
			}
		}
		// set prevgrouping to current grouping
		$prevgrouping = $row['grouping'];
	}
	// no grouping variable available, use itemname
	else {
		if (strlen($row['ItemName']) > 19) {
			$row['ItemName'] = '..' . substr($row['ItemName'], (strlen($row['ItemName'])-18));
		}
		$fontwidth = imagefontwidth(2);
		$txtwidth = strlen($row['ItemName'])*$fontwidth;
		$txtx = $xoff-$txtwidth-3;
		// do not skip line for first entry, set prevgrouping to anything not zero
		if ($prevgrouping != '') {
			$y+=10;
		}
		$prevgrouping = 1;
		// print gene name
		imagestring($subimages[$trackname],2,$txtx,$y-2,$row['ItemName'],$colors['black']);
	}

	// PLOT REGIONS 
	// position
	$cstart = $row['start'];
	$cstop = $row['stop'];
	// get color from database
	if (isset($row['color'])) {
		$ccolorname = guess_color($colormap,$row['color'],$colors);
	}
	else {
		$ccolorname = 'black';
	}
	// define alerts to apply
	$tmp = choose_color($alerting, $row,$ccolorname);
	$ccolorname = $tmp[0];
	$drawbox = $tmp[1];
	settype($ccolorname,'string');
	$ccolor = $colors[$ccolorname];
	// correct if extending beyond plot
	if ($cstart < $start) $cstart = $start;
	if ($cstop > $stop) $cstop = $stop;
	// scale coordinates
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	// plot region rectangle
	imagefilledrectangle($subimages[$trackname],$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+7,$ccolor);
	//draw box
	if ($drawbox == 1) {
		imagerectangle($subimages[$trackname],$xoff+$scaledstart-2,$y,$xoff+$scaledstop+2,$y+9,$colors['black']);
	}
}

// last label
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($prevlabel)*$fontwidth;
$txtx = $xoff-$txtwidth-3;
imagestring($subimages[$trackname],2,$txtx,$y-2,$prevlabel,$colors['black']);
$prevlabel = $row['grouplabel'];

?>
