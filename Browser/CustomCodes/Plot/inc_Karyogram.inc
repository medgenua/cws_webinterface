<?php

// get centromere position
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];

// determine the height needed for this track
$tmpheight = 25;

// create handle for this subimage
$positions[$trackname] = $fullheight;
$heights[$trackname] = $tmpheight;
$fullheight += $tmpheight;
$subimages[$trackname] = ImageCreate($width,$tmpheight);
$colors = array();
$colors = LoadColors($subimages[$trackname],'Main_Colors.xml');
imagefill($subimages[$trackname],0,0,$colors['white']);


// get query string from xml
$querystring = $Array->{'plot'}->{'database'}->{'query'};
#$querystring = $querystring['value'];
// replace placeholders with variable values
$querystring = str_replace('%start%', $start, $querystring);
$querystring = str_replace('%stop%', $stop, $querystring);
$querystring = str_replace('%chr%', $chr, $querystring);
$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
// create query
$query = mysql_query($querystring);
// process entries
while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$name = $row['name'];
	$ccolor = $row['color'];
	// scale positions for plotting
	$scaledstart = intval(round(($cstart-$start)*$scalef));
	$scaledstop = intval(round(($cstop-$start)*$scalef));
	// this creates triangles for centromere
	if ($cstop == $lastp) {
		if ($ccolor != 'gneg') {
			imagefilledpolygon($subimages[$trackname], array($xoff+$scaledstart,0,$xoff+$scaledstop,10,$xoff+$scaledstart,20),3, $colors[$colormap[$ccolor]]);
		}
		imagepolygon($subimages[$trackname], array($xoff+$scaledstart,0,$xoff+$scaledstop,10,$xoff+$scaledstart,20),3,$colors['black']);
		$nexttriang = 1;
	}
	elseif (isset($nexttriang) && $nexttriang == 1) {
		if ($ccolor != 'gneg') {
			imagefilledpolygon($subimages[$trackname], array($xoff+$scaledstart,10,$xoff+$scaledstop,0,$xoff+$scaledstop,20),3,$colors[$colormap[$ccolor]]);
		}
		imagepolygon($subimages[$trackname], array($xoff+$scaledstart,10,$xoff+$scaledstop,0,$xoff+$scaledstop,20),3,$colors['black']);
		$nexttriang = 0;
	}
	else{
		if ($ccolor != 'gneg') {
			imagefilledrectangle($subimages[$trackname], $xoff+$scaledstart, 0, $xoff+$scaledstop, 20, $colors[$colormap[$ccolor]]);
		}
		imagerectangle($subimages[$trackname], $xoff+$scaledstart, 0, $xoff+$scaledstop, 20, $colors['black']);
	}
	// label the cytoband if wide enough
	$fontwidth = imagefontwidth(3);
	$fullname = $chrtxt . $name;
	$txtwidth = strlen($fullname)*$fontwidth;
	if ($txtwidth+5 < ($scaledstop-$scaledstart)) {
		$txtx = $xoff + ($scaledstop+$scaledstart)/2 - $txtwidth/2;
		if ($ccolor != "gpos100") {
			imagestring($subimages[$trackname],3,$txtx,4,$fullname,$colors['black']);		
		}
		else {
			imagestring($subimages[$trackname],3,$txtx,4,$fullname,$colors['white']);
		}
	}
}

?>
