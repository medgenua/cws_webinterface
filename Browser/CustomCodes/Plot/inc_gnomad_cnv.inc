<?php
// loaded in main:
//   - track xml ($Array)
//   - $colormap if present
//   - groupname ($prevgroup) ==> is current one !
//   - $plotruler  : 1 if first track from new group

$displayname = $Array->{'plot'}->{'displayname'};
$tmpheight = 17 + 40;

if ($plotruler == 1) {
	$fulloffset = 5;
	$tmpheight += 10;
}
else {
	$fulloffset = 0; 
}

// create handle for this subimage
$positions[$trackname] = $fullheight;
$heights[$trackname] = $tmpheight;
$fullheight += $tmpheight;
$subimages[$trackname] = ImageCreate($width,$tmpheight);
$colors = array();
$colors = LoadColors($subimages[$trackname],'Main_Colors.xml');
#imagefill($subimages[$trackname],0,0,$colors['white']);
DrawGrid($subimages[$trackname],$xoff,$tmpheight,$start,$stop,$scalef,$colors['lightblue']);
$y = 0;

// top & bottom lines
imageline($subimages[$trackname],$xoff,$fulloffset,$width,$fulloffset,$colors['lightgray']);
imageline($subimages[$trackname],$xoff,$tmpheight-1,$width,$tmpheight-1,$colors['lightgray']);

// draw trackname
$y = 0;
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($displayname)*$fontwidth;
$txtx = $xoff + ($width-$xoff)/2 - $txtwidth/2;
if ($plotruler == 1) {
	imagestring($subimages[$trackname],2,$txtx,$y-6,$displayname,$colors['black']);
	$y-3;
}
else {
	imagestring($subimages[$trackname],2,$txtx,$y-1,$displayname,$colors['black']);
	$y+=2;
}

#$y = $y+10;	
# LOAD gnomad TRACK for selected studies
$gnomadtypes = array(1 => " = 'DUP'", 2 => "= 'DEL'", 3 => "NOT IN ('DUP', 'DEL')");
$gnomadcolors = array(1 => 'blue', 2 => 'red', 3 => 'mediumgray');
$gnomadtitles = array(1 => "CN Gain", 2 => "CN Loss", 3 => "Insertion/Inversion");

// gnomad gold has DUP / DEL and INV/INS
for ($i = 1; $i <= 3; $i++) {
	$y = $y+10;
	//print label
	$string = "$gnomadtitles[$i]";
	$fontwidth = imagefontwidth(2);
	$txtwidth = strlen($string)*$fontwidth;
	$txtx = $xoff-$txtwidth-3;
	imagestring($subimages[$trackname],2,$txtx,$y-2,$string,$colors['black']);
	$query = mysql_query("SELECT start, end FROM `gnomAD_SV_CNV` WHERE chr = '$chr' AND `type` $gnomadtypes[$i] AND ((start BETWEEN '$start' AND '$stop') OR (end BETWEEN '$start' AND '$stop') OR (start <= '$start' AND end >= '$stop')) ORDER BY start, end");
	#imagestring($image,2,$xoff,$y-5,$qstring,$black);	
	while ($abrow = mysql_fetch_array($query)) {
		#$y = $y+10;
		$cstart = $abrow['start'];
		$cstop = $abrow['end'];
        
        
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		// min region
        imagefilledrectangle($subimages[$trackname],$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+7,$colors[$gnomadcolors[$i]]);
        
	}
}


?>
