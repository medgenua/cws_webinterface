<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$dc = array();
$dc_query = mysql_query("SELECT id, name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
}

$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);

###################
# GET FORM PARAMS #
###################
// subtip from region browser
$cid = '';
$aid = '';
$sdc = '';
//if (!isset($included)) {
//	$included = 0;
//}
if (isset($_GET['st'])) {
	$p = explode("-",$_GET['aid']);
	$aid = $p[0];
	$cid = $p[1];
	$userid = $_GET['uid'];
	$included = 0;
}
else {
	$cid = $_GET['c'];
	$aid = $_GET['a'];
	$sdc = $_GET['s'];
}
if ($included != 1) {
	echo "<div class=nadruk style='margin-bottom:0.5em;'>Suggested Classification Details:</div>";
}
else {
	echo "<h3>Suggested Classification Details: included</h3>";
}	
//reference samples
$cq = mysql_query("SELECT TrackOnly,IncludeParents,IncludeControls FROM `Users_x_Classifiers` WHERE uid = '$userid' AND cid = '$cid'");
$row = mysql_fetch_array($cq);
$details = array();
$details['TrackOnly'] = $row['TrackOnly'];
$details['IncludeParents'] = $row['IncludeParents'];
$details['IncludeControls'] = $row['IncludeControls'];
$where = '';
$info = '';
if ($details['TrackOnly'] == 1) {
	$where .= " AND s.intrack = 1 AND a.idproj = s.trackfromproject";
	$info .= "<li>Track samples only</li>";
}
if ($details['IncludeParents'] == 0) {
	$where .= " AND p.collection <> 'Parents'";
}
else {
	$info .= "<li>Include parental data</li>";
}
if ($details['IncludeControls'] == 0) {
	$where .= " AND p.collection <> 'Controls'";
}
else {
	$info .= "<li>Include control data</li>";
}
// Classifier details. 
$query = mysql_query("SELECT c.Name, c.Comment, c.WholeGenome, c.Gender,c.CN,c.public,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter,COUNT(cr.id) AS nrreg FROM `Classifier` c LEFT JOIN `Classifier_x_Regions` cr ON c.id = cr.cid WHERE c.id = '$cid' GROUP BY cr.cid ");
$cntypes = array(0=>"Hom.Del",1=>"Het.Del",2=>"LOH",3=>"1xDup",4=>"2xDup");
$row = mysql_fetch_array($query);
if ($inluded != 1) {
	echo "<div style='float:left;width:48%'><span class=underline>Classifier details</span>";
}
else {
	echo "<div><span class=underline>Classifier details</span>";
}
echo "<ul id=ul-simple style='padding-top:0em;'>";
echo "<li> - Name: ".$row['Name']."</li>";
echo "<li> - Comments : ".$row['Comment']."</li>";
if ($row['WholeGenome'] == 1) {
	echo "<li> - Type: Whole Genome</li>";
}
else {
	echo "<li> - Type: Targeted (".$row['nrreg']." regions)</li>";
}
echo "<li> - Genders: ".$row['Gender']."</li>";
echo "<li> - Copy Numbers : ".$row['CN']."</li>";
echo "<li> - Suggest Class : ". $dc[$row['dc']] ."</li>";
echo "<li> - Required Matches : ".$row['snr']."</li>";
echo "$info";
//echo "<li> - Ratios : ".$row['sbigger']. "x (bigger), ".$row['ssmaller']."x (smaller), ". $row['soinner']."x (overlap inner part), ".$row['soouter']."bp (overlap extending)</li>";

echo "</ul></p>";
echo "</div>";

// GET CNV details
$query = mysql_query("SELECT a.chr, a.start, a.stop, a.cn, a.class, a.inheritance, s.chip_dnanr, s.gender FROM `aberration` a JOIN `sample` s ON a.sample = s.id WHERE a.id = '$aid'");
$row = mysql_fetch_array($query);
$chr = $row['chr'];
$start = $row['start'];
$cn = $row['cn'];
$class = $row['class'];

$inheritance = $row['inheritance'];
$stop = $row['stop'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
$sample = $row['chip_dnanr'];
$gender = $row['gender'];
$inh = array(0=>'ND', 1=>'Pat', 2=> 'Mat', 3=>'DN', 'DN' => 'De Novo', 'Pat' => 'Paternal', 'Mat' => 'Maternal', 'ND' => 'Not Defined');
if ($included != 1) {
	echo "<div style='float:left;width:48%'><span class=underline>CNV details</span>";
}
else {
	echo "<div><span class=underline>CNV details</span>";
}
echo "<ul id=ul-simple style='padding-top:0em;'>";
echo "<li> - Sample: $sample</li>";
echo "<li> - Gender : $gender</li>";
echo "<li> - Region: chr$chrtxt:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',')."</li>";
echo "<li> - Size: $sizeformat bp</li>";
echo "<li> - Copy Number : $cn</li>";
echo "<li> - Inheritance : ".$inh[$inh[$inheritance]]."</li>";
echo "<li> - Current Class : ". $dc[$class] ."</li>";
echo "</ul></p>";
echo "</div>";


$userid = $_SESSION['userID'];
echo "<br style='clear:both;' />";
$rand = rand(0,1000);
echo "<img src='ClassifierDetailsPlot.php?i=$included&amp;c=$cid&amp;a=$aid' border=0 usemap='#cdp'>\n";
echo "</p>";
// gather data for map
echo "<map name='cdp' id='cdp'>";
// classifier details
$cq = mysql_query("SELECT c.Gender,c.CN,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter FROM `Classifier` c WHERE c.id = '$cid' ");
$details = mysql_fetch_array($cq);
$details['CN'] = explode(",",$details['CN']);
// reference regions.
$end = $stop;
$refq = mysql_query("SELECT a.chr,a.start,a.stop,a.cn,a.class,a.sample,a.idproj FROM `sample` s JOIN `project` p JOIN `aberration` a JOIN `projectpermission` pp ON a.idproj = p.id AND a.sample = s.id AND a.idproj = pp.projectid WHERE pp.userid = '$userid' AND a.sample != '$sid' AND chr = '$chr' AND ( (start BETWEEN $start AND $end) OR (stop BETWEEN $start AND $end) OR (start <= $start AND stop >= $end)) $where");
$refcnvs = array();
while ($rrow = mysql_fetch_array($refq)) {
	// same cn needed?
	if ($details['scn'] == 1) {
		if ($details['scnd'] == 'exact' && $rrow['cn'] != $cn) {
			continue;
		}
		elseif ($details['scnd'] == 'type') {
			if ($rrow['cn'] >= 2 && $cn < 2) {
				continue;
			}
			if ($rrow['cn'] <= 2 && $cn > 2) {
				continue;
			}
			if ($rrow['cn'] == 2 && $cn != 2) {
				continue;
			}
		}
	}
	// same dc needed?
	if ($rrow['class'] == 2	) {
		$rrow['class'] = 1;
	}
	if ($details['sdc'] == 1 && $rrow['class'] != $details['dc']) {
		continue;
	}
	
	// size constraints.
	// bigger?
	if ($rrow['start'] < $start && $rrow['stop'] > $end && $details['sbigger'] != -1 && $details['sbigger'] < ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1)) {
		continue;
	}
	//smaller
	if ($rrow['start'] > $start && $rrow['stop'] < $end && $details['ssmaller'] >  ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1)) {
		continue;
	}
	// overlap/extend.
	$ovstart = max($start,$rrow['start']);
	$ovend = min($end,$rrow['stop']);
	// ratio inner
	
	if ($details['soinner'] > ($ovend-$ovstart+1)/($rrow['stop'] - $rrow['start'] + 1)) {
		continue;
	}
	// size extending 5'.
	if ($start < $rrow['start'] && $end <= $rrow['stop'] && ($rrow['start'] - $start + 1) > $details['soouter']) {
		continue;
	}
	//size extending 3'.
	if ($rrow['start'] <= $start && $rrow['stop'] < $end && ($end - $rrow['stop'] + 1) > $details['soouter']) {
		continue;
	}
	// passed all.
	$refcnvs[] = $rrow;

}
if ($included == 1) {
	$window = $stop-$start+100000;
}
else {
	$window = $stop-$start+10000;
}
$nrlines = count($refcnvs);
if ($nrlines > 50) {
	$refcnvs = array_slice($refcnvs,0,50);
	$nrlines = 52;
}
$height = $nrlines*10 + 20;
//Specify constant values
if ($included == 1) {
	$width = 800; //Image width in pixels
}
else {
	$width = 450; //Image width in pixels
}

// offset and scale.
$xoff = 155;
$scalef = ($width - $xoff -5)/($window);


$csample = '';
$y = 5;
//while ($row = mysql_fetch_array($query)) {
foreach($refcnvs as $key => $row) {
	$y = $y + 10;
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$sid = $row['sample'];
	$cgender = $row['gender'];
	$ccn = $row['cn'];
	$cclass = $row['class'];
	$pid = $row['idproj'];
	$title = "<div style='background-color:#cccccc;text-align:left;padding:3px;'>chr$chrtxt:$cstart-$cstop ; CN:$ccn ; Class:". $dc[$cclass];
	$ystart = $y+3;
	$ystop = $y+9;
	// get title (list of all regions in this case).
	$title = str_replace("'","\'",$title) . '</div>';
	echo "<area shape='rect' coords='0,$ystart,$width,$ystop' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='index.php?page=details&amp;project=$pid&amp;sample=$sid' target='_blank'>";
}
echo "</map>";


?>
