<?php
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);

#####################
# GENERAL VARIABLES #
#####################
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$dc = array();
$dc_query = mysql_query("SELECT id, name, abbreviated_name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = array(
		'abbr' => $row['abbreviated_name'],
		'full' => $row['name'],
	);
}



$aid = $_GET['id'];
$userid = $_GET['u'];
if(isset($_GET['s'])) {
	$show = $_GET['s'];
}
else {
	$show = 'i';
}
$packing = $_GET['packing'];

// get data from database
$result = mysql_query("SELECT a.LiftedFrom, a.cn, a.start, a.stop, a.chr, a.sample, a.inheritance, a.class, a.confidence, a.nrgenes, a.size, a.platform, s.chiptype, s.gender, a.nrprobes, a.idproj, a.pubmed, p.naam FROM `cus_aberration` a JOIN `cus_project` p JOIN `cus_sample` s ON a.idproj = s.idproj AND a.sample = s.idsamp AND a.idproj = p.id WHERE a.id = '$aid'");
$row = mysql_fetch_array($result);
$cn = $row['cn'];
$start = $row['start'];
$stop = $row['stop'];
$chr = $row['chr'];
$chrtxt = $chromhash[ $chr];
$platform = $row['platform'];
$pubmed = $row['pubmed'];
if ($platform == '') {
	$platform = 'Not Specified';
}
$chiptype = $row['chiptype'];
if ($chiptype == '') {
	$chiptype = 'Not Specified';
}
$gender = $row['gender'];
if ($gender == '') {
	$gender = 'Not Specified';
}
$sample = $row['sample'];
$pid = $row['idproj'];
$size = $row['size'];
$lifted = $row['LiftedFrom'];
if ($lifted != '') {
	$lifted = "<span class=italic style='font-size:xx-small;color:#610000;'>(Lifted from UCSC$lifted)</span>";
}
$confidence = $row['confidence'];
$nrprobes = $row['nrprobes'];
$nrgenes = $row['nrgenes'];
$class= $row['class'];
$inheritance = $row['inheritance'];
#$clinic = $row['clinical'];
$project = $row['naam'];
$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');

// check permissions for this cnv
$permquery = mysql_query("SELECT editcnv FROM cus_projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
$permrow = mysql_fetch_array($permquery);
$editcnv = $permrow['editcnv'];

# Check parental info
#$parq = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
#$parents = mysql_query("$parq");
#$parrow = mysql_fetch_array($parents);
#$father = $parrow['father'];
#$ppid = $parrow['father_project'];
#$mother = $parrow['mother'];
#$mpid = $parrow['mother_project'];


// PRINT TITLE  
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
echo "<h3 style='margin-top:5px'>$region</h3>";
// get links to ensembl, ucsc and dgv
$lquery = mysql_query("SELECT Resource, link FROM db_links WHERE Resource IN ('UCSC','Ensembl','DGV')");
while ($row = mysql_fetch_array($lquery)) {
	$link = $row['link'];
	if ($row['Resource'] == 'Ensembl') {
		$linkparts = explode('@@@',$link) ;
		if ($size < 1000000) {
			$link = $linkparts[0];
		}
		else {
			$link = $linkparts[1];
		}
	}
	$link = str_replace('%c',$chrtxt,$link);
	$link = str_replace('%s',$start,$link);
	$link = str_replace('%e',$stop,$link);
	$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
	$link = str_replace('%u',$ucscdb,$link);
	$links[$row['Resource']] = $link;
}
echo "<span style='font-style:italic;font-size:9px;position:absolute;left:400px;top:1px;'>";
echo "<a class=ttsmall href='".$links['UCSC']."' target='_blank'>UCSC</a><br/>";
echo "<a class=ttsmall href='".$links['Ensembl']."' target='_blank'>ENSEMBL</a><br/>";
echo "<a class=ttsmall href='".$links['DGV']."' target='_blank'>DGV</a><br/>";
echo "</span>";	

echo "</div>\n";


if ($show == 'i') {
	// PRINT CNV DETAILS
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	## LEFT PANEL
	echo "<div style='float:left;width:50%;'>";
	echo "<span class=nadruk>CNV Details</span> $lifted";
	echo " <ul id=ul-simple>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	echo "<li>- Copy Number : $cn</li>";
	if ($nrprobes != '') {
		echo "<li>- Number of Probes : $nrprobes</li>";
	}
	if ($nrgenes > 0) {
		echo "<li>- Affected Genes : <a class=tt href='index.php?page=genes&chr=$chr&start=$start&stop=$stop' target=new>$nrgenes</a></li>";
	}
	else {
		echo "<li>- Affected Genes : 0</li>";
	}
	if ($confidence != '') {
		echo "<li>- Calling Score: $confidence</li>";
	}
	echo "</ul>";

	// PRINT CLASSIFICATION DETAILS
	echo "<span class=nadruk>CNV Classification</span>";
	/*
	echo "<span class=italic style='font-size:9px;'> (set class: ";
	echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_setclass.php?aid=$aid&class=1&u=$userid&ftt=1')\">1</a>&nbsp;";
	echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_setclass.php?aid=$aid&class=2&u=$userid&ftt=1')\">2</a>&nbsp;";
	echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_setclass.php?aid=$aid&class=3&u=$userid&ftt=1')\">3</a>&nbsp;";
	echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_setclass.php?aid=$aid&class=4&u=$userid&ftt=1')\">4</a>&nbsp;";
	echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_setclass.php?aid=$aid&class=5&u=$userid&ftt=1')\">FP</a>&nbsp;";
	echo "<a class=img href=\"javascript:void(null)\" onclick=\"return popitup('cus_deletecnv.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/delete.gif' width=8px height=8px></a>";
	echo ")</span>";
	*/
	echo "<ul id=ul-simple>";
	if (preg_match('/de\s*novo/i',$inheritance)) {
		$inheritance = 3;
	}
	elseif (preg_match('/mater/i',$inheritance) || preg_match('/mother/i',$inheritance)) {
		$inheritance = 2;
	}
	elseif (preg_match('/pater/i',$inheritance) || preg_match('/father/i',$inheritance)) {
		$inheritance = 1;
	}
	elseif (preg_match('/not\s*defined/i',$inheritance)) {
		$inheritance = 0;
	}

	if (is_numeric($inheritance)) {
		echo "<li>- Inheritance: ". $inh[$inh[$inheritance]] . "</li>\n";
	}
	elseif($inheritance != '') {
		echo "<li>- Inheritance: $inheritance</li>\n";
	}
	else {
		echo "<li>- Inheritance: Not Defined</li>\n";
	}
	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM cus_log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		if ($class == '0' ) {
			$class = 'Not Defined';
		}
		if (array_key_exists($class, $dc)) {
			echo "<li>- Diagnostic Class: ".$dc[$class]['full']."<span class=italic style='font-size:9px;'>$setby</span>";
		}
		else {
			echo "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
		}

		//echo "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
	}
	else {
		echo "<li>- Diagnostic Class: Not Defined ";
	}
	if (isset($arguments) && $arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
	}
	echo "</li>\n";
	if (isset($pubmed) && $pubmed != '') {
		echo "<li>- <a class=tt href='index.php?page=literature&type=read&cstm=1&aid=$aid' target='_blank'>Show Associated Publications</a></li>";
	}

	echo "</ul>\n";
	if ($editcnv == 1) {
		echo "<span class=nadruk>Edit CNV</span>";
		echo "<ul id=ul-simple>";
		## set class
		echo "<li><span class=italic style='font-size:9px;'>- Set class: ";
		foreach ($dc as $class => $names) {
			echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_setclass.php?aid=$aid&class=$class&u=$userid&ftt=1')\">" . $names['abbr'] . "</a>&nbsp;";
		}
		echo "</span></li>";
		## set inheritance
		echo "<li><span class=italic style='font-size:9px;'>- Set Inheritance: ";
		$setlinks = array('0' => "<a class=ttsmall title='Not Defined' href=\"javascript:void(null)\" onclick=\"return popitup('cus_setinheritance.php?aid=$aid&inh=0&u=$userid&ftt=1')\">Not Defined</a>,", '1' =>  "<a class=ttsmall title='Paternal' href=\"javascript:void(null)\" onclick=\"return popitup('cus_setinheritance.php?aid=$aid&inh=1&u=$userid&ftt=1')\">Paternal</a>,", '2' => "<a class=ttsmall title='Maternal' href=\"javascript:void(null)\" onclick=\"return popitup('cus_setinheritance.php?aid=$aid&inh=2&u=$userid&ftt=1')\">Maternal</a>,", '3' => "<a class=ttsmall title='De Novo' href=\"javascript:void(null)\" onclick=\"return popitup('cus_setinheritance.php?aid=$aid&inh=3&u=$userid&ftt=1')\">De Novo</a>,"); 
		$setlinks[$inheritance] = '';
		$string = '';
		for ($i = 0; $i<= 3; $i++) {
			$string .= $setlinks[$i];
		}
		$string = substr($string, 0,-1);
		echo $string;
		echo "</span></li>";
		## edit cnv
		echo "<li><span class=italic style='font-size:9px;'>- Edit CNV: ";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_deletecnv.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/delete.gif' width=8px height=8px></a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('cus_editcnv.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/edit.gif' width=8px height=8px></a></span></li>";
		## set validation
		echo "<li><span class=italic style='font-size:9px;'>- Edit Validation Details: <a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setvalidation.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/edit.gif' width=8px height=8px></a></span></li>";
		echo "<li><span class=italic style='font-size:9px;'>- Add/Edit Remarks: <a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setremark.php?aid=$aid&u=$userid')\"><img src='images/content/edit.gif' width=8px height=8px></a></span></li>";
	
	}

	echo "</div>\n";
	
	## RIGHT PANEL
	echo "<div style='float:right;width:46%;'>";
	// PRINT SAMPLE DETAILS
	$query = mysql_query("SELECT COUNT(id) as aantal FROM cus_aberration WHERE sample = '$sample' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	
	echo "<span class=nadruk>Sample Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=cusdetails&project=$pid&sample=$sample' target=new>$sample</a></li>\n";
	echo "<li>- Gender : $gender</li>\n";
	echo "<li>- #CNV's : $ncnvs</li>\n";
	echo "<li>- Project : $project</li>\n";
	#if ($clinic != '') {
	#	echo "<li>- Clinical Description: $clinic</li>\n";
	#}
	echo "</ul>";
	echo "<span class=nadruk>Platform Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Platform : $platform</li>\n";
	echo "<li>- Chiptype: $chiptype</li>";	
	echo "</ul>";
	echo "</div>";
	echo "<p></p>";

	echo "</div>";
	// SET SUB TIPS
	$mouseplot = "onmouseover=\"setcustomsubtip('cus_plots','$aid','$userid',event,'$pid')\"";
	$styleplot = "text-decoration:underline";
	$styleclin = 'text-decoration:none;';
	$styletool = 'text-decoration:none;';
	$stylekaryo = 'text-decoration:none;';
	$mouseclin = "onmouseover=\"setcustomsubtip('cus_clinical','$sample','$userid',event,'$pid')\"";
	$mousetool = "onmouseover=\"setcustomsubtip('cus_tools','$aid','$userid',event,'$pid')\"";
	$mousekaryo = "onmouseover=\"setcustomsubtip('cus_karyo','$sample','$userid',event,'$pid')\"";
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
	echo "<span style='float:left;width:25%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
	echo "<span style='float:left;width:24%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
	echo "<span style='float:left;width:24%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
	echo "<span style='float:left;width:25%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
	echo "<br/></div>";
	echo "<div id=subtip style='display:none'>";
	echo "</div>";
	echo "<div id=plotdiv>";
	$checkdata = mysql_query("SELECT structure FROM cus_datapoints WHERE id = '$aid'");
	$cstm = 1;
	if (mysql_num_rows($checkdata) > 0) {
		include('inc_create_plot.inc');
	}
	else {
		echo "<p><span class=nadruk>Plot:</span><br/>No Probe Level Data Available For Plotting</p>";
	}
	echo "</div>";

	// CLOSE TIP DIV
	echo "</div>";
	ob_flush();
	flush();
}
?>
