<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
$userid = $_SESSION['userID'];
$uid = $userid;

###################
# GET FORM PARAMS #
###################
#$uid = $_GET['uid'];
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$type = $_GET['type']; # numeric 
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
# GET CONTROL DATA FROM DATABASE
$dgvtypes = array(1 => "Gain", 2 => "Loss", 3 => "inv");
$dgvcolors = array(1 => 'blue', 2 => 'red', 3 => 'mediumgray');
$dgvtypelong = array('Gain' => 'Copy Number Gains', 'Loss' => 'Copy Number Losses', 'inv' => 'Inversion');
$dgvlong = $dgvtypelong[$dgvtypes[$type]];
echo "<div>";
echo "<div class=nadruk>Toronto DGV Gold : $dgvlong: </div>";
echo "<span style='position:absolute;right:2px;top:1px;'>";
echo "<a class=tt style='font-style:italic;font-size:0.8em;' href='javascript:void(0)' onclick=\"LoadPreferences('PickDGVstudies')\">Set Included Studies</a>";
echo "</span>";
echo "</div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Region: Chr$chr:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
echo "<li> - Size : $sizeformat bp<li>";
echo "<li> - Type : $dgvlong</li>";
echo "<li> - <a class=tt href='http://projects.tcag.ca/cgi-bin/variation/gbrowse/$ucscdb/?start=$start;stop=$stop;ref=chr$chrtxt;width=760;version=100;label=chromosome:overview-cytoband-RefGene-super_Duplication-Variation-Inversion-InDel;grid=on' target='_blank'>Show in DGV-Browser</a></li>\n";
echo "</ul></p>";
echo "<p>";
echo "<img src='dgvplot.php?&amp;u=$uid&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;type=".$dgvtypes[$type]."' border=0 usemap='#tt'>\n";
echo "</p>";

	
// get data for map
echo "<map name='tt'>\n";
$querystring = "SELECT id, frequency, start, stop, max_start, max_stop, num_samples, num_total_samples, num_studies, num_variants, num_platforms FROM DGV_gold WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) AND vsubtype = '".$dgvtypes[$type]."' ORDER BY start";

$result = mysql_query($querystring);
$y = 8;
$cy = $y;
#$xoff = 110;

while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
    $y = $y + 9;

	// compose title
	$title = "<div style='background-color:#fff;text-align:left;padding:3px;'><u>DGV Study Details: </u><br/>";
    $title .= " - Sample frequency: ".$row['frequency']." (".$row['num_samples']."/".$row['num_total_samples'].")<br/>";
    $title .= " - Nr.Studies: ".$row['num_studies']."<br/>";
	$title .= " - Nr.Platforms: ".$row['num_platforms']."<br/>";


	
	$title = str_replace("'","\'",$title) . '</div>';
	// add map item
	$toy = $y + 9;
    $id = $row['id'];
	echo "<area shape='rect' coords='0,$cy,400,$toy' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='http://dgv.tcag.ca/gb2/gbrowse_details/dgv2_$ucscdb?;name=$id' target='_blank'>\n";
	
}

echo "</map>";
echo "</p><p style='font-size:0.8em;'>";
echo "If the DGV entries extend the currently viewed genomic window, small white arrows are added at the extending side.</p><p style='font-size:0.8em'>";
echo "Hover your mouse over the rows (and wait) for study details. It will show up after a short while. Click the row to go to the pubmed entry of the study.</p>";

?>
