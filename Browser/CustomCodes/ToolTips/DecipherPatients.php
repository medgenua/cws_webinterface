<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);

###################
# GET FORM PARAMS #
###################
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');

echo "<div class=nadruk>Decipher Cases </div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Region: chr$chrtxt:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',')."</li>";
echo "<li> - Size: $sizeformat bp<li>";
echo "</ul></p>";
echo "<p>";

$rand = rand(0,1000);
echo "<img src='decipherplot.php?&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;r=$rand' border=0 usemap='#decipher'>\n";
echo "</p>";
// gather data for map
echo "<map name='decipher' id='decipher'>";
$query = mysql_query("SELECT DISTINCT(caseid)as caseid, phenotype FROM decipher_patients WHERE chr = '$chr' AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) ORDER BY caseid");
$csample = '';
$y = 15;
$xoff = 55;
while ($row = mysql_fetch_array($query)) {
	$cpheno = $row['phenotype'];
	$caseid = $row['caseid'];
	$ystart = $y+3;
	$ystop = $y+9;
	// get title (list of all regions in this case).
	$subquery = mysql_query("SELECT chr, start, stop, type, inheritance FROM decipher_patients WHERE caseid = '$caseid'");
	$title = "<div style='background-color:#cccccc;text-align:left;padding:3px;'><u>Decipher Case $caseid: CNV Overview:</u>";
	while ($subrow = mysql_fetch_array($subquery)) {
		$title .= "<br/>&nbsp;&nbsp;- Chr".$chromhash[$subrow['chr']].":".number_format($subrow['start'],0,'',','). "-".number_format($subrow['stop'],0,'',',').":" ;
		$title .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:0.8em;'>Type: ".$subrow['type'] ."<br/>";
		$title .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Origin: ".$subrow['inheritance'].'</span>';
	}
	$title .= "<br/><br/><u>Decipher Case $caseid: Clinical Details:</u><br/>&nbsp;&nbsp;- ";
	if ($cpheno == '') {
		$title .= "No Details available<br/>";
	}
	else {
		$cpheno = str_replace(';',"<br/>&nbsp;&nbsp;- ",$cpheno);
		$title .= $cpheno."<br/>";
	}
	$title = str_replace("'","\'",$title) . '</div>';
	$rquery = mysql_query("SELECT link FROM `db_links` WHERE Resource = 'DECIPHER Patients'");
	$rrow = mysql_fetch_array($rquery);
	$declink = $rrow['link'];
	$declink = str_replace('%d',$caseid,$declink);
	echo "<area shape='rect' coords='0,$ystart,399,$ystop' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='$declink' target='_blank'>";
	$y = $y +10;
}
echo "</map>";
echo "</p><p style='font-size:0.8em;'>";
echo "Hover your mouse over the rows (and wait) for further information on the Decipher Samples. It will show up after a short while. Click the row to examine the case further on the Decipher pages. ";
#echo "More Info: <a class=tt href='index.php?page=showdecipher&amp;type=patients&amp;chr=$chr&amp;start=$start&amp;end=$stop'>Link</a></li>";


?>
