<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
$ecarucaSize = $_SESSION['ecarucaSize'];
$userid = $_SESSION['userID'];

###################
# GET FORM PARAMS #
###################
#$gID = $_GET['q'];
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');

echo "<div class=nadruk>Ecaruca Cases</div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Region : chr$chrtxt:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',') . "</li>";
echo "<li> - Size: $sizeformat bp</li>";
echo "</ul></p>";
echo "<p>";

$rand = rand(0,1000);
echo "<img src='ecarucaplot.php?&amp;u=$userid&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;r=$rand' border=0 usemap='#ecaruca'>\n";
echo "</p>";
 
# CREATE MAP.
echo "<map name='ecaruca' id='ecaruca'>";
$querystring = "SELECT a.start, a.stop, a.caseid FROM ecaruca a WHERE a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) AND (stop - start) < $ecarucaSize ORDER BY a.caseid, a.start";
//echo "<div class=nadruk>Currently Reformatting !</div>\n";
$result = mysql_query($querystring);
$csample = "";
$cct = "";
//echo "<ul id=ul-simple>\n";
$y = 5;
$cy = $y;
$xoff = 51;
while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$caseid = $row['caseid'];
	if ($caseid != $csample) {
		if ($csample != '') {
			$ystart = $y+3;
			$ystop = $y+7;
			// get title (list of all regions in this case).
			$subquery = mysql_query("SELECT chr, start, stop FROM ecaruca WHERE caseid = '$csample'");
			$title = "<div style='background-color:#cccccc;text-align:left;padding:3px;'><u>ECARUCA Case $csample: CNV Overview:</u>";
			while ($subrow = mysql_fetch_array($subquery)) {
				$title .= "<br/>&nbsp;&nbsp;- Chr".$chromhash[$subrow['chr']].":".number_format($subrow['start'],0,'',','). "-".number_format($subrow['stop'],0,'',',');
			}
			$title = str_replace("'","\'",$title) . '</div>';
			echo "<area shape='rect' coords='0,$ystart,399,$ystop' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='http://umcecaruca01.extern.umcn.nl:8080/ecaruca/initCaseDetails.db?selected_case_id=$csample' target='_blank'>";
			$title= '';
		}
		
		//echo "</ul>\n$chip_dnanr<ul id=ul-simple>\n";
		$csample = $caseid;
		$cy = $y+14;
		$y = $y +10;
		$cct = '';
	}
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	#$title = $title."<br/> - Chr$chrtxt:".number_format($start,0,'',',') . "-" . number_format($stop,0,'',',')";
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	#imagefilledrectangle($image,$xoff+$scaledstart,$y+4,$xoff+$scaledstop,$y+6,$gpos50);
}
// get title (list of all regions in this case).
$ystart = $y+3;
$ystop = $y+7;
$subquery = mysql_query("SELECT chr, start, stop FROM ecaruca WHERE caseid = '$csample'");
$title = "<div style='background-color:#cccccc;text-align:left;padding:3px;'><u>ECARUCA Case $csample: CNV Overview:</u>";
while ($subrow = mysql_fetch_array($subquery)) {
	$title .= "<br/>&nbsp;&nbsp;- Chr".$chromhash[$subrow['chr']].":".number_format($subrow['start'],0,'',','). "-".number_format($subrow['stop'],0,'',',');
}
$title = str_replace("'","\'",$title) . '</div>';
echo "<area shape='rect' coords='0,$ystart,399,$ystop' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='http://umcecaruca01.extern.umcn.nl:8080/ecaruca/initCaseDetails.db?selected_case_id=$csample' target='_blank'>";
echo "</map>";

echo "</p><p style='font-size:0.8em;'>";
echo "Hover your mouse over the rows (and wait) for further information on the Ecaruca Samples. It will show up after a short while. Click the row to examine the case further on the Ecaruca pages. You will need to log in on their website.</p>";

?>
