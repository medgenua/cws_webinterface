<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ecarucaSize = $_SESSION['ecarucaSize'];
$userid = $_SESSION['userID'];
$uid = $userid;
###################
# GET FORM PARAMS #
###################
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$cn = $_GET['cn'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
# GET CONTROL DATA FROM DATABASE
echo "<div class=nadruk>chr$chr:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',') . "</div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Size: $sizeformat bp<li>";
echo "<li> - Copy Number: $cn</li>";
echo "<li> - Condensed Classes: $classes</li>"; 
echo "</ul></p>";
echo "<p>";
echo "<img src='ParentPlot_image.php?&amp;u=$uid&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;cn=$cn' border=0 usemap='#tt'>\n";
echo "</p>";

# create map;
echo "<map name='tt'>";
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$querystring = "SELECT a.id AS aid, a.start, a.stop, s.chip_dnanr, p.chiptype, p.id AS pid, s.id AS sid, a.class FROM aberration a JOIN sample s JOIN project p JOIN projectpermission pp ON s.id = a.sample AND a.idproj = p.id AND a.idproj = pp.projectid WHERE  s.intrack = 1 AND s.trackfromproject = p.id AND pp.userid = '$uid' AND p.collection = 'Parents' AND chr = '$chr' AND cn = '$cn' $locquery ORDER BY s.chip_dnanr, p.chiptype, a.start";
$result = mysql_query($querystring);
$nrrows = mysql_num_rows($result);
$csample = "";
$cct = "";
$y = 8;
$cy = $y;
$xoff = 180;
while ($row = mysql_fetch_array($result)) {
	$cclass = $row['class'];
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$aid = $row['aid'];
	$chip_dnanr = $row['chip_dnanr'];
	$chiptype = $row['chiptype'];
	$pid = $row['pid'];
	$sid = $row['sid'];
	if ($chip_dnanr != $csample) {
		$csample = $chip_dnanr;
		$cy = $y+14;
		$y = $y +4;
		$cct = '';
	}
	
	if ($chiptype != $cct) {
		$foy = $y+8;
		$toy = $y+18;
		$title = "<div style='background-color:#fff;text-align:left;padding:3px;'><u>Parental Data: </u><br/>";
		$title .= " - Full Region: Chr$chrtxt:".number_format($cstart,0,'',',')."-".number_format($cstop,0,'',',')."<br/>";
		$title .= "<img style='width:275px;' src='plot.php?aid=$aid' border=0>";
		$title = str_replace("'","\'",$title) . '</div>';
		echo "<area shape='rect' coords='0,$foy,400,$toy' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='index.php?page=details&project=$pid&sample=$sid' target='_blank'>\n";
		$y = $y+10;
		$cct = $chiptype;
	}
}
echo "</map>\n";
echo "</p><p style='font-size:0.8em;'>";
echo "Hover your mouse over the rows (and wait) for probe level plots. It will show up after a short while. Click the row to examine the carrier parent further.</p>";

?>
