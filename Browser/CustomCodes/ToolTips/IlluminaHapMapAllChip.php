<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];
$uid = $userid;

###################
# GET FORM PARAMS #
###################
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
#$chiptype = $_GET['ct'];
#$chips = $_GET['chips'];
$cn = $_GET['cn'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
$scalef = 280/($size);
# GET CONTROL DATA FROM DATABASE
echo "<div class=nadruk>chr$chr:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',') . "</div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Size: $sizeformat bp<li>";
echo "<li> - Copy Number: $cn</li>";
echo "</ul></p>";
echo "<p>";
echo "<img src='smallimage.php?&amp;u=$userid&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;cn=$cn' border=0 usemap='#tt'>\n";
echo "</p>";

#foreach ($chiptypes as $key => $cid) {
$query = mysql_query("SELECT Controles, ID FROM chiptypes"); # WHERE ID = '$cid'");
#$row = mysql_fetch_array($query);
while ($row = mysql_fetch_array($query)) {
	if ($row['Controles'] == '') {
		# no controle project for this chip
		continue;
	}
	$cid = $row['ID'];
	$correlations[$cid] = $row['Controles']; 
	$instring .= $row['Controles'] . ',';
}
#}
$instring = substr($instring,0,-1); 


echo "<map name='tt'>";
$querystring = "SELECT a.id AS aid, a.start, a.stop, s.chip_dnanr, s.id AS sid, p.chiptype, p.id AS pid FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE ps.calc = 1 AND ps.INIWAVE = 0 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2 AND a.idproj IN ($instring) AND a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))  AND a.cn = '$cn' ORDER BY s.chip_dnanr, p.chiptype, a.start";
$result = mysql_query($querystring);
$csample = "";
$cct = "";
$y = 5;
$cy = $y;
$xoff = 110;
while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$csid = $row['sid'];
	$cpid = $row['pid'];
	$aid = $row['aid'];
	$chip_dnanr = $row['chip_dnanr'];
	$chiptype = $row['chiptype'];
	if ($chip_dnanr != $csample) {
		#if ($csample != '') {
			#imagestring($image,1,10,($cy+$y)/2-2,$csample,$black);
			//if ($y != $cy) {
			//	imagefilledrectangle($image,47,$cy-1,47,$y+7,$black);	
			//}
			#imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
		#}
		
		//echo "</ul>\n$chip_dnanr<ul id=ul-simple>\n";
		$csample = $chip_dnanr;
		$cy = $y+14;
		$y = $y +4;
		$cct = '';
	}
	
	if ($chiptype != $cct) {
		$y = $y+10;
		#$chipstring = preg_replace('/(Human)(.*)/',"$2",$chiptype);		

		#imagestring($image,1,50,$y-2,$chipstring,$black);
		$cct = $chiptype;
	}
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	#imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+2,$cns[$cn]);
	$title = "<div style='background-color:#fff;text-align:left;padding:3px;'><u>HapMap Control Data: </u><br/>";
	$title .= " - Full Region: Chr$chrtxt:".number_format($cstart,0,'',',')."-".number_format($cstop,0,'',',')."<br/>";
	$title .= "<img style='width:275px;' src='plot.php?aid=$aid' border=0>";
	$title = str_replace("'","\'",$title) . '</div>';
	$foy = $y;
	$toy = $y+5;
	$fox = $xoff+$scaledstart;
	$tox = $xoff+$scaledstop;
	echo "<area shape='rect' coords='$fox,$foy,$tox,$toy' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='index.php?page=details&project=$cpid&sample=$csid' target='_blank'>\n";
	#echo "Full Region: Chr$chrtxt:".number_format($cstart,0,'',',')."-".number_format($cstop,0,'',',')."<br/>";
	$title = '';
	//$region = "chr$chrtxt:". number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	//$nrq = mysql_query("SELECT COUNT(id) AS 'aantal' FROM aberration WHERE sample = '$sid' AND idproj = '$pid'");
	//$res = mysql_fetch_array($nrq);
	//$nr = $res['aantal'];
	//echo "<li><a class=tt href='index.php?page=details&amp;project=$pid&amp;sample=$sid' target=new>$chiptype</a>: $region</li>";
}
echo "</map>";
echo "</p><p style='font-size:0.8em;'>";
echo "Hover your mouse over the rows (and wait) for probe level plots. It will show up after a short while. Click the row to examine the carrier further. If you don't have access, join the 'Control Data' usergroup.</p>";


#imagestring($image,1,10,($cy+$y)/2-2,$csample,$black);
//if ($y != $cy) {
//	imagefilledrectangle($image,47,$cy-1,47,$y+7,$black);
//}


?>
