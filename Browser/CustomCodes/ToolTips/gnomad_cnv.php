<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
$userid = $_SESSION['userID'];
$uid = $userid;

###################
# GET FORM PARAMS #
###################
#$uid = $_GET['uid'];
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$type = $_GET['type']; # numeric 
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
trigger_error('in tooltip');
# GET CONTROL DATA FROM DATABASE
$dgvtypes = array(1 => "= 'DUP'", 2 => "= 'DEL'", 3 => "NOT IN ('DEL','DUP')");
$dgvcolors = array(1 => 'blue', 2 => 'red', 3 => 'mediumgray');
$dgvtypelong = array("= 'DUP'" => 'Copy Number Gains', "= 'DEL'" => 'Copy Number Losses', "NOT IN ('DEL','DUP')" => 'Insertion/Inversion');
$dgvlong = $dgvtypelong[$dgvtypes[$type]];
echo "<div>";
echo "<div class=nadruk>gnomAD/SV : $dgvlong: </div>";
echo "</div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Region: Chr$chr:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
echo "<li> - Size : $sizeformat bp<li>";
echo "<li> - Type : $dgvlong</li>";
//echo "<li> - <a class=tt href='https://gnomad.broadinstitute.org/region/$chrtxt-$start-$stop?dataset=gnomad_sv_r2_1' target='_blank'>Show in DGV-Browser</a></li>\n";
echo "</ul></p>";
echo "<p>";
echo "<img src='gnomad_cnv_plot.php?&amp;u=$uid&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;type=$type' border=0 usemap='#tt'>\n";
echo "</p>";

	
// get data for map
echo "<map name='tt'>\n";
$querystring = "SELECT * FROM `gnomAD_SV_CNV` WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (end BETWEEN '$start' AND '$stop') OR (start <= '$start' AND end >= '$stop')) AND type $dgvtypes[$type] ORDER BY start";

$result = mysql_query($querystring);
$y = 10;
$cy = $y;
#$xoff = 110;

while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['end'];
    $y = $y + 9;
    $id = $row['id'];
	// compose title
	$title = "<div style='background-color:#fff;text-align:left;padding:3px;'><u>gnomAD_SV CNV Details: </u><br/>";
    $title .= " - Position : chr$chrtxt:$cstart-$cstop <br/>";
	$title .= " - Type : ".$dgvtypelong[$dgvtypes[$type]]."<br/>";
	$title .= " - Nr.HomRef : ".$row['n_homref']."<br/>";
    $title .= " - Nr.HetZyg : ".$row['n_het']."<br/>";
	$title .= " - Nr.HomAlt : ".$row['n_homalt']."<br/>";
	$title .= " - Overall AF: ".$row['af_all']."<br/>";
    $title .= " - Max.Pop AF: ".$row['popmax_af']."<br/>";
    $title .= " - Evidence  : ".$row['evidence']."<br/>";
    $title .= " - Quality   : ".$row['qual']."<br/>";
    $title .= " - Filter    : ".$row['Filter']."<br/>";

	
	$title = str_replace("'","\'",$title) . '</div>';
	// add map item
	$toy = $y + 9;
	echo "<area shape='rect' coords='0,$cy,400,$toy' onmouseover=\"return overlib('$title');\" onmouseout=\"return nd();\" href='https://gnomad.broadinstitute.org/variant/$id?dataset=gnomad_sv_r2_1' target='_blank'>\n";
	
}

echo "</map>";
echo "</p><p style='font-size:0.8em;'>";
echo "If the SV entries extend the currently viewed genomic window, small white arrows are added at the extending side.</p><p style='font-size:0.8em'>";
echo "Hover your mouse over the rows (and wait) for details. It will show up after a short while. .</p>";

?>
