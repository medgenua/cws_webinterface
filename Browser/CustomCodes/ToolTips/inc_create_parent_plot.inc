

<?php
#######################
# CONNECT TO DATABASE #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$mapname = "map" . rand(1,3000);
if ($setupd == 2) {
	echo "<img src='parent_UPDplot.php?aid=$aid&psid=$psid&ppid=$ppid' usemap='#$mapname' border=0>\n";
}
else {
	echo "<img src='parent_plot.php?aid=$aid&psid=$psid&ppid=$ppid' usemap='#$mapname' border=0>\n";
}
# GET some data
$query = mysql_query("SELECT a.start, a.stop, a.chr FROM aberration a WHERE a.id = '$aid'");
$row = mysql_fetch_array($query);
$chr = $row['chr'];
$start = $row['start'];
$firstpos = $start - 250000;
$stop = $row['stop'];
$lastpos = $stop + 250000;
$window = $lastpos-$firstpos+1;
$scalex = 380 / $window;

//CREATE MAP FOR GENES
echo "<map name='$mapname'>";
$xoff = 25;
$locquery = "AND ((start BETWEEN '$firstpos' AND '$lastpos') OR (stop BETWEEN '$firstpos' AND '$lastpos') OR (start <= '$firstpos' AND stop >= '$lastpos'))";
$genequery = mysql_query("SELECT symbol, start, stop FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");


$y = 435;
//imagefilledrectangle($image,$xoff,48,$plotwidth,52,$gpos25);
while ($generow = mysql_fetch_array($genequery)) {
	$symbol = $generow['symbol'];	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	$ystart = $y-4;
	$yend = $y+4;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' title='$symbol'>\n";
	

}
# DRAW ENCODE GENES MAP
	
$y = $y + 10;
$genequery = mysql_query("SELECT start, stop, symbol FROM encodesum WHERE chr = '$chr' $locquery ORDER BY start");
while ($generow = mysql_fetch_array($genequery)) {
	$symbol = $generow['symbol'];	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	$ystart = $y-4;
	$yend = $y+4;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' title='$symbol'>\n";
	
	}


echo "</map>\n";

ob_flush();
flush();
?>


