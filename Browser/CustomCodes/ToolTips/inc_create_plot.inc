<?php
#######################
# CONNECT TO DATABASE #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$mapname = "map" . rand(1,3000);
echo "<div id='loadingdiv' style='text-align:center;'><img src='images/content/ajax-loader.gif'><br/>Loading Plot</div>";
flush();

if(!isset($cstm)){ 	
	$cstm = '';
}
if (!isset($loh)) {
	$loh = 0;
}

###################
## INCLUDE IMAGE ##
###################
if ($setupd == 2) {
	## plot data from parents_upd table
	echo "<img src='plotUPD.php?aid=$aid&sid=$sid&pid=$pid' usemap='#$mapname' border=0 onload=\"document.getElementById('loadingdiv').style.display='none';\">\n";
}
if (isset($mos)) {
	echo "<img src='plot.php?aid=$aid&u=$userid&cstm=$cstm&upd=$setupd&loh=$loh&mos=$mos' usemap='#$mapname' border=0 onload=\"document.getElementById('loadingdiv').style.display='none';\">\n";
}
else {
	## plot data from aberrations(_LOH) table (might fetch upd data from parents_upd, but region is stored as aberration.
	echo "<img src='plot.php?aid=$aid&u=$userid&cstm=$cstm&upd=$setupd&loh=$loh' usemap='#$mapname' border=0 onload=\"document.getElementById('loadingdiv').style.display='none';\">\n";
}
#echo "<script type='text/javascript'>document.getElementById('loadingdiv').style.display='none';</script>";
# GET some data
if ($cstm == 1) {
	$query = mysql_query("SELECT a.start, a.stop, a.chr FROM cus_aberration a WHERE a.id = '$aid'");
}
elseif ($loh == 1) {
	$query = mysql_query("SELECT a.start, a.stop, a.chr FROM aberration_LOH a WHERE a.id = '$aid'");
}
else {
	$query = mysql_query("SELECT a.start, a.stop, a.chr FROM aberration a WHERE a.id = '$aid'");
}
$row = mysql_fetch_array($query);
$chr = $row['chr'];
$start = $row['start'];
$firstpos = $start - 250000;
$stop = $row['stop'];
$lastpos = $stop + 250000;
$window = $lastpos-$firstpos+1;
$scalex = 380 / $window;

//CREATE MAP FOR GENES
echo "<map name='$mapname'>";
$xoff = 25;
$locquery = "AND ((start BETWEEN '$firstpos' AND '$lastpos') OR (stop BETWEEN '$firstpos' AND '$lastpos') OR (start <= '$firstpos' AND stop >= '$lastpos'))";
$genequery = mysql_query("SELECT symbol, start, stop FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");

if ($cstm == 1)  {
	# custom data, check available data
	#$test = mysql_query("SELECT a.cn, a.start, a.stop, a.chr, d.content, d.structure, a.avgLogR FROM cus_aberration a JOIN cus_datapoints d ON a.id = d.id WHERE a.id = '$aid'");
	#$row = mysql_fetch_array($test);
	#echo "structure: ". $row['structure'] ."<br/>";
	$strquery = mysql_query("SELECT structure FROM cus_datapoints WHERE id = '$aid'");
	$strrow = mysql_fetch_array($strquery);
	$structure = $strrow['structure'];
	$items = str_split($structure,1);
	if ($items[1] == 1 && ($items[2] == 1 || $items[3] == 1)) {
		#echo " logR and (BAF or GT) => two panels<br/>";
		$y = 435;
	}
	else {
		#echo "one panel<br/>";
		# one panel
		$y = 230;
	}
}
elseif ($loh == 1)  {
	# custom data, check available data
	#$test = mysql_query("SELECT a.cn, a.start, a.stop, a.chr, d.content, d.structure, a.avgLogR FROM cus_aberration a JOIN cus_datapoints d ON a.id = d.id WHERE a.id = '$aid'");
	#$row = mysql_fetch_array($test);
	#echo "structure: ". $row['structure'] ."<br/>";
	$strquery = mysql_query("SELECT structure FROM datapoints_LOH WHERE id = '$aid'");
	$strrow = mysql_fetch_array($strquery);
	$structure = $strrow['structure'];
	$items = str_split($structure,1);
	if ($items[1] == 1 && ($items[2] == 1 || $items[3] == 1)) {
		#echo " logR and (BAF or GT) => two panels<br/>";
		$y = 435;
	}
	else {
		#echo "one panel<br/>";
		# one panel
		$y = 230;
	}
}
else {
	$y = 435;
}
//imagefilledrectangle($image,$xoff,48,$plotwidth,52,$gpos25);
while ($generow = mysql_fetch_array($genequery)) {
	$symbol = $generow['symbol'];	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	$ystart = $y-4;
	$yend = $y+4;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' title='$symbol'>\n";
	

}
# DRAW ENCODE GENES MAP
	
$y = $y + 10;
$genequery = mysql_query("SELECT start, stop, symbol FROM encodesum WHERE chr = '$chr' $locquery ORDER BY start");
while ($generow = mysql_fetch_array($genequery)) {
	$symbol = $generow['symbol'];	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	$ystart = $y-4;
	$yend = $y+4;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' title='$symbol'>\n";
	
	}


echo "</map>\n";

ob_flush();
flush();
?>


