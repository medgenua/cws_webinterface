<?php
//echo "<div id=ttcontainer >";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$dc = array();
$dc_query = mysql_query("SELECT id, name, abbreviated_name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = array(
		'abbr' => $row['abbreviated_name'],
		'full' => $row['name'],
	);
}

////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#######################
# CONNECT TO DATABASE #
#######################
include('../../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
// get posted vars
$aid = $_GET['id'];
$userid = $_GET['u'];
if(isset($_GET['s'])) {
	$show = $_GET['s'];
}
else {
	$show = 'i';
}
$packing = $_GET['packing'];
#$fromimage = $_GET['fi'];
#$fromtable = $_GET['ft'];
if (isset($_GET['loh'])) {
	$loh = $_GET['loh'];
}
else {
	$loh = 0;
}
$table = "aberration";
$fields = ', a.cn, a.inheritance';

$query = "SELECT p.chiptype, a.start, a.stop, a.chr, s.chip_dnanr, s.gender, a.nrsnps, a.total_nrsnps, a.nrgenes, a.sample, a.idproj, a.class, a.pubmed, a.validation, a.validationdetails, a.LiftedFrom $fields FROM $table a JOIN sample s JOIN project p ON a.sample = s.id AND a.idproj = p.id WHERE a.id = '$aid'";
$result = mysql_query($query);
$row = mysql_fetch_array($result);
$chiptype = $row['chiptype'];
$gender = $row['gender'];
$sid = $row['sample'];
$pid = $row['idproj'];
if ($loh != 1) {
	$cn = $row['cn'];
	$inheritance = $row['inheritance'];
}
$start = $row['start'];
$stop = $row['stop'];
$size = $stop - $start + 1;
$chr = $row['chr'];
$chrtxt = $chromhash[ $chr];
$sample = $row['chip_dnanr'];
$nrbins = $row['nrsnps'];
$nrtotalbins = $row['total_nrsnps'];
$nrgenes = $row['nrgenes'];
$class= $row['class'];
$pubmed = $row['pubmed'];
$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
$regiontxt = $region;
$validation = $row['validation'];
$lifted = $row['LiftedFrom'];
if ($lifted != '') {
	$lifted = "<span class=italic style='font-size:xx-small;color:#610000'>(Lifted from UCSC$lifted)</span>";
}
$validationdetails = $row['validationdetails'];

// check permissions for this cnv
$permquery = mysql_query("SELECT editcnv FROM projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
$permrow = mysql_fetch_array($permquery);
$editcnv = $permrow['editcnv'];

# Check parental info
if ($loh != 1) {
	$parq = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
	$parents = mysql_query("$parq");
	$parrow = mysql_fetch_array($parents);
	$father = $parrow['father'];
	$ppid = $parrow['father_project'];
	$mother = $parrow['mother'];
	$mpid = $parrow['mother_project'];

	// if parents exist , set these values
	if ($mother != 0 || $father != 0) {
	  # SET mouseover entries
	  if ($show == 'i') {
		$mousei = '';
		$stylei = '';
		$stylef = 'text-decoration:none;';
		$stylem = 'text-decoration:none;';
		#$mousef = "onmouseover=\"Tip(CustomTT('1&amp;id=$aid&amp;s=f','IlluminaSearchSamples','$userid','$packing',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousef = "onmouseover=\"CustomTT('1&amp;id=$aid&amp;s=f','IlluminaSearchSamples','$userid','$packing',event)\" ";

		#$mousem = "onmouseover=\"Tip(CustomTT('1&amp;id=$aid&amp;s=m','IlluminaSearchSamples','$userid','$packing',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousem = "onmouseover=\"CustomTT('1&amp;id=$aid&amp;s=m','IlluminaSearchSamples','$userid','$packing',event)\"";
	  }
	  elseif ($show == 'f') {
		$psid = $father;
		$ppid = $ppid;
		#$mousei = "onmouseover=\"Tip(CustomTT('1&amp;id=$aid&amp;s=i','IlluminaSearchSamples','$userid','$packing',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousei = "onmouseover=\"CustomTT('1&amp;id=$aid&amp;s=i','IlluminaSearchSamples','$userid','$packing',event)\" ";
		$mousef = '';
		$stylei = "text-decoration:none;"; 
		$stylef = '';
		$stylem = "text-decoration:none;";
		#$mousem = "onmouseover=\"Tip(CustomTT('1&amp;id=$aid&amp;s=m','IlluminaSearchSamples','$userid','$packing',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousem = "onmouseover=\"CustomTT('1&amp;id=$aid&amp;s=m','IlluminaSearchSamples','$userid','$packing',event)\" ";

	  }
	  elseif ($show == 'm') {
		$psid = $mother;
		$ppid = $mpid;
		#$mousei = "onmouseover=\"Tip(CustomTT('1&amp;id=$aid&amp;s=i','IlluminaSearchSamples','$userid','$packing',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousei = "onmouseover=\"CustomTT('1&amp;id=$aid&amp;s=i','IlluminaSearchSamples','$userid','$packing',event)\"";


		#$mousef = "onmouseover=\"Tip(CustomTT('1&amp;id=$aid&amp;s=f','IlluminaSearchSamples','$userid','$packing',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousef = "onmouseover=\"CustomTT('1&amp;id=$aid&amp;s=f','IlluminaSearchSamples','$userid','$packing',event)\" ";

		$mousem = '';
		$stylei = "text-decoration:none;";
		$stylef = 'text-decoration:none;';
		$stylem = '';
	  }
	}
}

// classifiers
//////////////////////
// LOAD CLASSIFIERS //
//////////////////////
$cq = mysql_query("SELECT c.id, c.Name,c.Gender,c.WholeGenome,c.CN,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter,uc.Type,uc.MinSNP FROM `Classifier` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE uc.uid = '$userid' AND uc.Type IN (2,3)");
$classifiers = array();
if (mysql_num_rows($cq) > 0) {
	while ($row = mysql_fetch_array($cq)) {
		// skip classifier not matching the sample gender.
		$togender = explode(",",$row['Gender']);
		if (!in_array($gender,$togender)) {
			continue;
		}
		// skip if nrsnps is too low
		if ($row['MinSNP'] > $nrsnps) {
			continue;
		}
		if ($row['WholeGenome'] == 1) {
			if (!isset($classifiers['wg'])) {
				$classifiers['wg'] = array();
			}
			//convert some to arrays.
			$row['Gender'] = $togender;
			$row['CN'] = explode(",",$row['CN']);
			$classifiers['wg'][] = $row;
		}
		else {
			// get regions.
			$cid = $row['id'];
			$sq = mysql_query("SELECT chr, start, stop FROM `Classifier_x_Regions` WHERE cid = $cid");
			$regions = array();
			while ($sr = mysql_fetch_row($sq)) {
				$regions[] = $sr;
			}
			if (!isset($classifiers['local'])) {
				$classifiers['local'] = array();
			}
			//convert some to arrays.
			$row['Gender'] = $togender;
			$row['CN'] = explode(",",$row['CN']);

			$classifiers['local'][] = array('id'=>$row['id'],'Name'=>$row['Name'],'Gender'=>$row['Gender'],'CN'=>$row['CN'],'dc'=>$row['dc'],'scn'=>$row['scn'],'scnd'=>$row['scnd'],'sdc'=>$row['sdc'],'snr'=>$row['snr'],'sbigger'=>$row['sbigger'],'ssmaller'=>$row['ssmaller'],'soinner'=>$row['soinner'],'soouter'=>$row['soouter'],'regions'=>$regions);
	
		}
	}
}
// 1. whole genome.
$refcnvs = NULL;
$suggestedclass = 99;
$appliedrule = '';
$appliedid = '';
$userid = $_SESSION['userID'];
$end = $stop;
if (isset($classifiers['wg'])) {
	// load the reference cnvs.
	$refcnvs = array();
	$refq = mysql_query("SELECT chr,start,stop,cn,class FROM `$table` a JOIN `projectpermission` pp ON a.idproj = pp.projectid WHERE pp.userid = '$userid' AND a.sample != '$sid' AND chr = '$chr' AND ( (start BETWEEN $start AND $end) OR (stop BETWEEN $start AND $end) OR (start <= $start AND stop >= $end))");
	while ($row = mysql_fetch_array($refq)) {
		$refcnvs[] = $row;
	}
	// loop the genome wide classifiers.
	foreach($classifiers['wg'] as $key => $details) {
		// check cn needed in classifier
		if (!in_array($cn,$details['CN'])) {
			continue;
		}
		$matches = 0;
		// loop regions.
		foreach($refcnvs as $key => $rrow) {
			// same cn needed?
			if ($details['scn'] == 1) {
				if ($details['scnd'] == 'exact' && $rrow['cn'] != $cn) {
					continue;
				}
				elseif ($details['scnd'] == 'type') {
					if ($rrow['cn'] >= 2 && $cn < 2) {
						continue;
					}
					if ($rrow['cn'] <= 2 && $cn > 2) {
						continue;
					}
					if ($rrow['cn'] == 2 && $cn != 2) {
						continue;
					}
				}
			}
			
			if ($details['sdc'] == 1 && $rrow['class'] != $details['dc']) {
				continue;
			}
			// size constraints.
			$sizeratio = ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1);
			if ($rrow['start'] < $start && $rrow['stop'] > $end && $details['sbigger'] != -1 && $details['sbigger'] < $sizeratio) {
				continue;
			}
			//smaller
			if ($rrow['start'] > $start && $rrow['stop'] < $end && $details['ssmaller'] > $sizeratio) {
				continue;
			}
			// overlap/extend.
			$ovstart = max($start,$rrow['start']);
			$ovend = min($end,$rrow['stop']);
			// ratio inner
			if ($details['soinner'] > ($ovend-$ovstart+1)/($rrow['stop'] - $rrow['start'] + 1)) {
				continue;
			}
            // size extending 5'.
            if ($start < $rrow['start'] && $end <= $rrow['stop'] && ($rrow['start'] - $start + 1) > $details['soouter']) {
                continue;
            }
            //size extending 3'.
            if ($rrow['start'] <= $start && $rrow['stop'] < $end && ($end - $rrow['stop'] + 1) > $details['soouter']) {
                continue;
            }
			// passed all.
			$matches++;
			if ($matches >= $details['snr']) {
				break;
			}
		}
		if ($matches >= $details['snr']) {
			// ok. suggest if lower class.
			if ($details['dc'] < $suggestedclass) {
				$suggestedclass = $details['dc'];
				$appliedrule = $details['Name'];
				$appliedid = $details['id'];
			}
		}
	}
}
if (isset($classifiers['local'])) {
	//load the reference cnvs
	if ($refcnvs === NULL ) {
		$refcnvs = array();
		$refq = mysql_query("SELECT chr,start,stop,cn,class FROM `$table` a JOIN `projectpermission` pp ON a.idproj = pp.projectid WHERE pp.userid = '$userid' AND a.sample != '$sid' AND chr = '$chr' AND ( (start BETWEEN $start AND $end) OR (stop BETWEEN $start AND $end) OR (start <= $start AND stop >= $end))");
		while ($row = mysql_fetch_array($refq)) {
			$refcnvs[] = $row;
		}
	}
	//loop region specific classifiers.
	foreach($classifiers['local'] as $key => $details) {
		// check cn needed in classifier
		if (!in_array($cn,$details['CN'])) {
			continue;
		}
		$matches = 0;
		// matching regions in classifier ?
		$ok = 0;
		foreach($details['regions'] as $rkey => $region) {
			// correct chr? && overlap ?
			if ($chr != $region[0] || $region[2] < $start || $region[1] > $end) {
				continue;
			}
			else {
				$ok = 1;
				break;
			}
		}
		if ($ok == 0) {
			continue;
		}
		$matches = 0;
		// ok. apply classifier: loop ref regions.
		foreach($refcnvs as $key => $rrow) {
			// same cn needed?
			if ($details['scn'] == 1) {
				if ($details['scnd'] == 'exact' && $rrow['cn'] != $cn) {
					continue;
				}
				elseif ($details['scnd'] == 'type') {
					if ($rrow['cn'] >= 2 && $cn < 2) {
						continue;
					}
					if ($rrow['cn'] <= 2 && $cn > 2) {
						continue;
					}
					if ($rrow['cn'] == 2 && $cn != 2) {
						continue;
					}
				}
			}
			
			// same dc needed?
			if ($rrow['class'] == 2	) {
				$rrow['class'] = 1;
			}
			if ($details['sdc'] == 1 && $rrow['class'] != $details['dc']) {
				continue;
			}
			// size constraints.
			// bigger?
			if ($rrow['start'] < $start && $rrow['stop'] > $end && $details['sbigger'] != -1 && $details['sbigger'] < ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1)) {
				continue;
			}
			//smaller
			if ($rrow['start'] > $start && $rrow['stop'] < $end && $details['ssmaller'] >  ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1)) {
				continue;
			}
			// overlap/extend.
			$ovstart = max($start,$rrow['start']);
			$ovend = min($end,$rrow['stop']);
			// required ratio inner
			if ($details['soinner'] > ($ovend-$ovstart+1)/($rrow['stop'] - $rrow['start'] + 1)) {
				continue;
			}
            // size extending 5'.
            if ($start < $rrow['start'] && $end <= $rrow['stop'] && ($rrow['start'] - $start + 1) > $details['soouter']) {
                continue;
            }
            //size extending 3'.
            if ($rrow['start'] <= $start && $rrow['stop'] < $end && ($end - $rrow['stop'] + 1) > $details['soouter']) {
                continue;
            }
			// passed all.

			$matches++;
			if ($matches >= $details['snr']) {
				break;
			}
		}
		if ($matches >= $details['snr']) {
			// ok. suggest if lower class.
			if ($details['dc'] < $suggestedclass) {
				$suggestedclass = $details['dc'];
				$appliedrule = $details['Name'];
				$appliedid = $details['id'];
				
			}
		}
			
	}
}



// PRINT TITLE  
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
echo "<h3 style='margin-top:5px'>$regiontxt</h3>";
// get links to ensembl, ucsc and dgv
$lquery = mysql_query("SELECT Resource, link FROM db_links WHERE Resource IN ('UCSC','Ensembl','DGV')");
while ($row = mysql_fetch_array($lquery)) {
	$link = $row['link'];
	if ($row['Resource'] == 'Ensembl') {
		$linkparts = explode('@@@',$link) ;
		if ($size < 1000000) {
			$link = $linkparts[0];
		}
		else {
			$link = $linkparts[1];
		}
	}
	$link = str_replace('%c',$chrtxt,$link);
	$link = str_replace('%s',$start,$link);
	$link = str_replace('%e',$stop,$link);
	$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
	$link = str_replace('%u',$ucscdb,$link);
	$links[$row['Resource']] = $link;
}
echo "<span style='font-style:italic;font-size:9px;position:absolute;left:400px;top:1px;'>";
echo "<a class=ttsmall href='".$links['UCSC']."' target='_blank'>UCSC</a><br/>";
echo "<a class=ttsmall href='".$links['Ensembl']."' target='_blank'>ENSEMBL</a><br/>";
echo "<a class=ttsmall href='".$links['DGV']."' target='_blank'>DGV</a><br/>";
echo "</span>";	
echo "</div>\n";

// PRINT FAMILY
if ($loh != 1 && ($mother != 0 || $father != 0)) {
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center' >";
	echo "<div  style='float:left;padding-left:5pt;$stylei' class=nadruk $mousei>INDEX</div>\n";
	echo "<div  style='float:right;padding-right:5pt;$stylem' class=nadruk $mousem>MOTHER</div>\n";
	echo "<span style='text-align:center;$stylef' class=nadruk $mousef>FATHER</span>\n";
	echo "</div>";
}

// CASE ONE: show the tooltip for the index patient. 
if ($show == 'i') {
	// PRINT CNV DETAILS
	echo "<div id='tipcontainer'>";
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	## LEFT PANEL
	echo "<div style='float:left;width:50%;'>";
	echo "<span class=nadruk>CNV Details</span> $lifted";
	echo " <ul id=ul-simple>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	if ($cn == 9) {
		echo "<li>- UPD-type: $updtype</li>";
	}
	else {
		echo "<li>- Copy Number : $cn</li>";
	}
	echo "<li>- Number of Bins : $nrbins ($nrtotalbins)</li>";
	if ($nrgenes > 0) {
		echo "<li>- Affected Genes : <a class=tt href='index.php?page=genes&chr=$chr&start=$start&stop=$stop' target=new>$nrgenes</a></li>";
	}
	else {
		echo "<li>- Affected Genes : 0</li>";
	}
	
	echo "</ul>";

	// PRINT CLASSIFICATION DETAILS
	echo "<span class=nadruk>CNV Classification</span>";
	echo "<ul id=ul-simple>";
	if ($loh != 1) {
		echo "<li>- Inheritance: ". $inh[$inh[$inheritance]];
		echo "</li>\n";
	}
	if ($loh == 1) {
		$ltable = "log_loh";
	}
	else {
		$ltable = "log";
	}

	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		echo "<li>- Diagnostic Class: ".$dc[$class]['full']." <span class=italic style='font-size:9px;'>$setby</span>";
	}
	else {
		echo "<li>- Diagnostic Class: Not Defined ";
	}
	if (isset($arguments) && $arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
	}
	echo "</li>\n";
	if ($suggestedclass < 99) {
		echo "<li>- Suggested Class: ".$dc[$suggestedclass]['full']."<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Applied rule:</span><a class=tt style='font-size:9px;' href='index.php?page=ClassifierDetails&amp;a=$aid&amp;c=$appliedid' target='_blank'>$appliedrule</a></div>";
	}
	# validation...
	if ($validation != '') {
		if ($validationdetails != '') {
			$valtitle = $validationdetails;
		}
		else {
			$valtitle= 'No Additional Details Specified';
		}
		echo "<li><span title='$valtitle'>- Validated by $validation </span>";
	}
	else {
		echo "<li>- Not Validated";
	}
	# check log for user who validated 
	$logq = mysql_query("SELECT uid, entry,arguments FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$arguments = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Validation/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
			$arguments = $logrow['arguments'];
			$logentry = $logrow['entry'];
			break;
		}
	}
	if ($setby != '') {
		echo "<span class=italic style='font-size:9px;'>$setby</span>";
	}
	if ($arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Was adapted:</span> $arguments</div>";
	}
	echo "</li>";
	if ($loh != 1) {	
	  if (($mother != 0 && $father != 0) && $cn >= 1 && $cn <= 3) {
		echo "<li>- <a href='index.php?page=PoO&cn=$cn&aid=$aid' target='_blank' class=tt>Check Parent Of Origin</a></li>"; 
	  }
	  ## new: check for de novo deletions/duplications in presence of single parent 
	  elseif (($mother != 0 || $father != 0) && $cn == 1) {
		echo "<li>- <a href='index.php?page=PoO&cn=$cn&aid=$aid' target='_blank' class=tt>Check Parent Of Origin</a></li>";
	  }
	}
	if ($pubmed != '') {
		echo "<li>- <a class=tt href='index.php?page=literature&type=read&aid=$aid&loh=$loh' target='_blank'>Show Associated Publications</a></li>";
	}
	echo "</ul>\n";
	
	// end of left panel of tipcontainer
	echo "</div>";

	## RIGHT PANEL
	echo "<div style='float:right;width:46%;'>";
	// PRINT SAMPLE DETAILS
	$query = mysql_query("select COUNT(id) as aantal FROM $table WHERE sample = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	echo "<span class=nadruk>Sample Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$pid&sample=$sid' target=new>$sample</a></li>\n";
	echo "<li>- Gender : $gender</li>\n";
	echo "<li>- #CNV's : $ncnvs</li>\n";
	echo "<span class=nadruk>Data Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Library kit: $chiptype</li>";
	echo "</ul>";
	if ($editcnv == 1) {
		echo "<span class=nadruk>Edit CNV</span>";
		echo "<ul id=ul-simple>";
		## set class
		echo "<li><span class=italic style='font-size:9px;'>- Set class: ";
		foreach ($dc as $class => $names) {
			echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setclass.php?aid=$aid&class=$class&u=$userid&ftt=1&loh=$loh')\">" . $names['abbr'] . "</a>&nbsp;";
		}
		echo "</span></li>";
		## set inheritance
		if ($loh != 1) {
			echo "<li><span class=italic style='font-size:9px;'>- Set Inheritance: ";
			$setlinks = array('0' => "<a class=ttsmall title='Not Defined' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=0&u=$userid&ftt=1&loh=$loh')\">Not Defined</a>,", '1' =>  "<a class=ttsmall title='Paternal' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=1&u=$userid&ftt=1&loh=$loh')\">Paternal</a>,", '2' => "<a class=ttsmall title='Maternal' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=2&u=$userid&ftt=1&loh=$loh')\">Maternal</a>,", '3' => "<a class=ttsmall title='De Novo' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=3&u=$userid&ftt=1&loh=$loh')\">De Novo</a>,"); 
			$setlinks[$inheritance] = '';
			$string = '';
			for ($i = 0; $i<= 3; $i++) {
				$string .= $setlinks[$i];
			}
			$string = substr($string, 0,-1);
			echo $string;
			echo "</span></li>";
		}
		## edit cnv
		echo "<li><span class=italic style='font-size:9px;'>- Edit CNV: ";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('deletecnv.php?aid=$aid&u=$userid&ftt=1&loh=$loh')\"><img src='images/content/delete.gif' width=8px height=8px></a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('editcnv.php?aid=$aid&u=$userid&ftt=1&loh=$loh&swgs=1')\"><img src='images/content/edit.gif' width=8px height=8px></a></span></li>";
		## set validation
		echo "<li><span class=italic style='font-size:9px;'>- Edit Validation Details: <a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setvalidation.php?aid=$aid&u=$userid&ftt=1&loh=$loh')\"><img src='images/content/edit.gif' width=8px height=8px></a></span></li>";
		echo "<li><span class=italic style='font-size:9px;'>- Add/Edit Remarks: <a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setremark.php?aid=$aid&u=$userid&loh=$loh')\"><img src='images/content/edit.gif' width=8px height=8px></a></span></li>";
	
	}

	// end of right panel of tipcontainer
	echo "</div>";
	echo "<p></p>";
	// end of top  (div class=sectie)
	echo "</div>"; 
	ob_flush();
	flush();
	if ($cn == 9) {
		$styleplot = "text-decoration:none";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$styleclass = 'text-decoration:nen;';
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
		$mouseclass = "onmouseover=\"setsubtip('classifier','$aid','$appliedid','$userid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		if ($suggestedclass < 99) {
			# 6 entries, each 16%
			echo "<span style='float:left;width:16%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
			echo "<span style='float:left;width:16%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
			echo "<span style='float:left;width:16%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
			echo "<span style='float:left;width:16%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
			echo "<span style='float:left;width:16%;text-align:center;'><span id=linkclassifier style='$styleclass' class=nadruk $mouseclass>Classifier</span></span>\n";
			
		}
		else {
			# 5 entries, each 20%
			echo "<span style='float:left;width:20%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
			echo "<span style='float:left;width:19%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
			echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
			echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		}
		echo "<br/></div>";
		echo "<div id=UPDtable style='padding-left:5px'>";
		echo "<div class=nadruk>Informative Probes for $updtype</div>";
		echo "<p><table cellspacing=0 width='75%'>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
		echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
		$datapoints = explode('@',$updgts);
		foreach($datapoints as $key => $item) {
			$entries = explode(';',$item);
			if (isset($upddata[$sid][$entries[0]])) {
				$slogr = $upddata[$sid][$entries[0]]['logR'];
			}
			else {
				$slogr = '.';
			}
			if (isset($upddata[$updfid][$entries[0]])) {
				$flogr = $upddata[$updfid][$entries[0]]['logR'];
			}
			else {
				$flogr = '.';
			}
			if (isset($upddata[$updmid][$entries[0]])) {
				$mlogr = $upddata[$updmid][$entries[0]]['logR'];
			}
			else {
				$mlogr = '.';
			}

			echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>$slogr</td><td>".$entries[2]."</td><td NOWRAP>$flogr</td><td>".$entries[3]."</td><td NOWRAP>$mlogr</td></tr>";
		}
		echo "</table>";
		echo "</div>";
		echo "<div id=subtip style='display:none'>";
		echo "</div>";
		echo "<div id=plotdiv style='display:none'>";
		$setupd = 1;
		include('inc_create_plot.inc');
		echo "</div>";
	}
	else {
	  	if ($loh == 1) {
			$ls = "loh-";
	  	}
	  	else {
			$ls = "";
   	  	}
		$mouseplot = "onmouseover=\"setsubtip('plots','$ls$aid','$userid',event)\"";
		$styleplot = "text-decoration:underline";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$styleclass = 'text-decoration:none;';
		$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$ls$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
		$mouseclass = "onmouseover=\"setsubtip('classifier','$aid-$appliedid','$userid',event)\"";
		
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		if ($suggestedclass < 99 ) {
			# 5 entries, each 20%
			echo "<span style='float:left;width:19%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
			echo "<span style='float:left;width:20%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
			echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
			echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
			echo "<span style='float:left;width:20%;text-align:center;'><span id=linkclassifier style='$styleclass' class=nadruk $mouseclass>Classifier</span></span>\n";
		}
		else {		
			# 4 entries, each 25%
			echo "<span style='float:left;width:25%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
			echo "<span style='float:left;width:24%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
			echo "<span style='float:left;width:24%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
			echo "<span style='float:left;width:25%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		}
		echo "<br/></div>";
		echo "<div id=subtip style='display:none'>";
		echo "</div>";
		echo "<div id=plotdiv>";
		$setupd = 0;
		include('inc_create_plot.inc');
		echo "</div>";
		// print following to prevent javascript from crashin on non-existing div/span
		echo "<div id=UPDtable ><span id=linkprobes></span></div>";


	}
}
// CASE 2 : print tooltip for the parent, in context of the index. (NOT available for LOH regions) 
else {
	// PRINT CNV DETAILS (for cnv from child)
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	echo "<div style='float:left;width:48%;'>";
	echo "<span class=nadruk>Offspring CNV Details</span>";
	echo " <ul id=ul-simple>";
	echo "<li>- Sample: $sample</li>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	if ($cn == 9) {
		echo "<li>- UPD-type: $updtype</li>";
	}
	else {
		echo "<li>- Copy Number : $cn</li>";
	}
	echo "</ol></li>";
	echo "</ul>";
	// PRINT CLASSIFICATION DETAILS
	echo "<span class=nadruk>Offspring CNV Classification</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Inheritance: ". $inh[$inh[$inheritance]] . "</li>\n";
	$nrlines = 0;
	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		if ($class == 5) {
			echo "<li>- Diagnostic Class: False Positive <span class=italic style='font-size:9px;'>$setby</span>";
		}
		else {
			echo "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
		}
	}
	else {
		echo "<li>- Diagnostic Class: Not Defined ";
	}
	if (isset($arguments) && $arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
		$nrlines--;
	}
	echo "</li>\n";
	echo "</ul>\n";
	echo" </div>\n";
	// PRINT PARENT DETAILS
	
	$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate,SentrixID, SentrixPos FROM projsamp WHERE idsamp = '$psid' AND idproj = '$ppid'");
	$row = mysql_fetch_array($query);
	$lsd = $row['LRRSD'];
	$bsd = $row['BAFSD'];
	$wave = $row['WAVE'];
	$iniwave = $row['INIWAVE'];
	$callrate = $row['callrate'];
	$chippos = $row['SentrixID'].'_'.$row['SentrixPos'];
	$remark = "<span class=nadruk>Quality Remarks:</span><ul id=ul-simple>";
	$remarkstring = $remark;
	
	if ($callrate < 0.994) {
		//$cbg = 'style="background:red;color:black"';
		$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
		$nrlines++;
	}
	if ($lsd > 0.2 && $lsd < 0.3) {
		$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
	}
	if ($lsd >=0.3) {
		$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
		$nrlines++;
	}
	if ($bsd >= 0.6) {
		$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
		$nrlines++;
	} 
	if ($iniwave != 0) {
		$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
		$nrlines++;
	}
	$query = mysql_query("select COUNT(id) as aantal FROM $table WHERE sample = '$psid' AND idproj = '$ppid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	
	$query = mysql_query("select chip_dnanr, gender FROM sample WHERE id = '$psid'");
	$row = mysql_fetch_array($query);
	$parentname = $row['chip_dnanr'];
	$gender = $row['gender'];
	
	$query = mysql_query("SELECT chiptype FROM project WHERE id = '$ppid'");
	$row = mysql_fetch_array($query);
	$chiptype = $row['chiptype'];
	echo "<div style='float:right;width:48%;'>";
	#echo "<div style='position:absolute;left:215px;top:80px;'>";
	echo "<span class=nadruk>Parental Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$ppid&sample=$psid' target=new>$parentname</a></li>\n";
	echo "<li>- Chiptype: $chiptype</li>";
	echo "<li>- Gender : $gender</li>\n";
	echo "<li>- #CNV's : $ncnvs</li>\n";
	if ($remarkstring != $remark) {
		echo "</ul>\n";
		#echo "<p>";
		echo $remarkstring;
		echo "</ul>";
		#echo "</p>";
	}
	else {
		echo "<li>- Quality : OK</li>\n";
		echo "</ul>\n";
	}
	echo "<span class=nadruk>Chip Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Type: $chiptype</li>";
	if ($chippos != '_') {
		echo "<li>- ID: $chippos</li>";
	}
	else {
		echo "<li>- ID: NA</li>";
	}
	echo "</ul>";

	echo "</div>\n";
	echo "<p></p>";
	echo "</div>";
	ob_flush();
	flush();
	
	if ($cn == 9) {
		#if ($userid == 1) {
		$styleplot = "text-decoration:none";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$mouseupd = "onmouseover=\"setsubtip('upd','$aid','$userid',event)\"";
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		# 5 entries, each 20%
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
		echo "<span style='float:left;width:19%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
		echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		echo "<br/></div>";
		echo "<div id=UPDtable style='padding-left:5px'>";
		echo "<div class=nadruk>Informative Probes for $updtype</div>";
		echo "<p><table cellspacing=0 width='75%'>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
		echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
		$datapoints = explode('@',$updgts);
		foreach($datapoints as $key => $item) {
			$entries = explode(';',$item);
			if (isset($upddata[$sid][$entries[0]])) {
				$slogr = $upddata[$sid][$entries[0]]['logR'];
			}
			else {
				$slogr = '.';
			}
			if (isset($upddata[$updfid][$entries[0]])) {
				$flogr = $upddata[$updfid][$entries[0]]['logR'];
			}
			else {
				$flogr = '.';
			}
			if (isset($upddata[$updmid][$entries[0]])) {
				$mlogr = $upddata[$updmid][$entries[0]]['logR'];
			}
			else {
				$mlogr = '.';
			}

			echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>$slogr</td><td>".$entries[2]."</td><td NOWRAP>$flogr</td><td>".$entries[3]."</td><td NOWRAP>$mlogr</td></tr>";

			#echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
		}
		echo "</table>";
		echo "</div>";
		echo "<div id=subtip style='display:none'>";
		echo "</div>";
		echo "<div id=plotdiv style='display:none'>";
		$setupd = 1;
		include('inc_create_plot.inc');
		echo "</div>";

	}
	else {
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$styleplot = "text-decoration:underline";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$mouseclin = "onmouseover=\"setsubtip('clinical','$psid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$psid','$ppid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		# 4 entries, each 25%
		echo "<span style='float:left;width:25%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
		echo "<span style='float:left;width:24%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
		echo "<span style='float:left;width:24%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
		echo "<span style='float:left;width:25%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		echo "<br/></div>";
		echo "<div id=subtip style='display:none'>";
		#include('inc_create_plot.inc');
		echo "</div>";
		echo "<div id=plotdiv>";
		$setupd = 0;
		include('inc_create_parent_plot.inc');
		echo "</div>";
		// print following to prevent javascript from crashin on non-existing div/span
		echo "<div id=UPDtable ><span id=linkprobes></span></div>";


	}
	
	
}
//echo "</div>";	
?>

