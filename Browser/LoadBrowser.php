<?php

// unset session vars from editing cnv
if (isset($_SESSION['setfromeditcnv'])) {
	unset($_SESSION['sample']);
	unset($_SESSION['project']);
	unset($_SESSION['setfromeditcnv']);
}

// LOAD FUNCTIONS //
require('functions.php');

// update preferences => moved to index.php

?>
<script type='text/javascript' src='javascripts/ajax_tooltips.js'></script>
<script type='text/javascript' src='javascripts/setclass.js'></script>
<?php
flush();

//////////////
// SETTINGS //
//////////////
$plotwidth = $_COOKIE['plotwidth'];
$xoff = 120;
// todo set xoff from cookie!
if ($start != "" && $stop != ""){
	$scalef = ($plotwidth  - $xoff - 1)/($stop-$start+1);
}
else {
	$start = 0;
	$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
	$row = mysql_fetch_array($result);
	$largest = $row['stop'];
	$scalef = ($plotwidth - $xoff - 1)/$largest;
	$stop = $largest;
}

///////////////////
// 1. NAVIGATION //
///////////////////
// Zooming factors
$size = $stop - $start + 1;
$size15 = round($size*15/100);
$size50 = round($size/2);
$size95 = round($size*95/100);
$out15 = $size * 3/2;
$out5 = $size * 5;
$out10 = $size * 10;
$in15 = $size * 2/3;
$in5 = $size / 5;
$in10 = $size / 10;
// Navigate Left
$startleft15 = $start - $size15;
$stopleft15 = $stop - $size15;
if ($startleft15 < 1) $startleft15 = 1;
$regionleft15 = "chr$chrtxt:$startleft15-$stopleft15";
$startleft50 = $start - $size50;
$stopleft50 = $stop - $size50;
if ($startleft50 < 1) $startleft50 = 1;
$regionleft50 = "chr$chrtxt:$startleft50-$stopleft50";
$startleft95 = $start - $size95;
$stopleft95 = $stop - $size95;
if ($startleft95 < 1) $startleft95 = 1;
$regionleft95 = "chr$chrtxt:$startleft95-$stopleft95";
// Navigate Right
$startright15 = $start + $size15;
$stopright15 = $stop + $size15;
$regionright15 = "chr$chrtxt:$startright15-$stopright15";
$startright50 = $start + $size50;
$stopright50 = $stop + $size50;
$regionright50 = "chr$chrtxt:$startright50-$stopright50";
$startright95 = $start + $size95;
$stopright95 = $stop + $size95;
$regionright95 = "chr$chrtxt:$startright95-$stopright95";
//zoom out
$startout15 = $start - round(($out15 - $size)/2);
$stopout15 = $stop + round(($out15 - $size)/2);
$regionout15 = "chr$chrtxt:$startout15-$stopout15";
$startout5 = $start - round(($out5 - $size)/2);
$stopout5 = $stop + round(($out5 - $size)/2);
$regionout5 = "chr$chrtxt:$startout5-$stopout5";
$startout10 = $start - round(($out10 - $size)/2);
$stopout10 = $stop + round(($out10 - $size)/2);
$regionout10 = "chr$chrtxt:$startout10-$stopout10";
//zoom in
$startin15 = $start + round(($size - $in15)/2);
$stopin15 = $stop - round(($size - $in15)/2);
$regionin15 = "chr$chrtxt:$startin15-$stopin15";
$startin5 = $start + round(($size - $in5)/2);
$stopin5 = $stop - round(($size - $in5)/2);
$regionin5 = "chr$chrtxt:$startin5-$stopin5";
$startin10 = $start + round(($size - $in10)/2);
$stopin10 = $stop - round(($size - $in10)/2);
$regionin10 = "chr$chrtxt:$startin10-$stopin10";
// page dependent dynamic code
if ($page == 'overview' || $page == 'overviewbrowse') {
	$linkpart = "page=overview&amp;p=$project&amp;c=$chr";
}
else {
	$linkpart = "page=results&amp;type=region&amp;v=i";
}
if (isset($_GET['cstm'])) {
	$custom = $_GET['cstm'] ;
}
else {
	$custom = '';
}
// project details if needed
$projpart = '';
if ($project != "") {
	$projpart = "&amp;p=$project";
	$linkpart .= "&amp;p=$project";
	if ($custom == 1) {
		$projpart .= "&amp;cstm=1";
		$linkpart .= "&amp;cstm=1";
	}
}
// print update notification
if (!isset($_COOKIE['hidenavupdate']) || $_COOKIE['hidenavupdate'] != 1) {
	// first javascript function
	$note = "<script type='text/javascript'>function sethideCookie(){days=30; myDate = new Date(); myDate.setTime(myDate.getTime()+(days*24*60*60*1000)); document.cookie = 'hidenavupdate=1; expires=' + myDate.toGMTString();}</script>";
	$note .= "<div id=navupdate style='margin-left:2em;border:1px solid red;width:90%'>";
	$note .= "<span style='float:right'><a style='color:red' href='javascript:void(0)' onclick=\"sethideCookie();document.getElementById('navupdate').style.display='none';\">X</a></span>";
	$note .= "<p><span class=nadruk style='color:red'>Navigation &amp; settings update: </span><br/>";
	$note .= "- Automatic context pop-ups have been replaced by click-to-show, for a more stable experience<br/>";
	$note .= "- Close pop-ups by clicking outside a hotlinked area. <br/>";
	$note .= "- Plot preferences are accessible by the 'preferences' link in the top-right corner<br/>";
	$note .= "- Where appropriate, pop-ups contains links to relevant settings in their top-right corner as well<br/>";
	$note .= "</p></div>";
	echo $note;
}

// print the navigation menu
$menu = "<p>\n";
$menu .= "<center>";
$menu .= "<table cellspacing = 0>\n";
$menu .= "<tr>\n";
$menu .= " <th $firstcell class=center>Move Left</th>\n";
$menu .= " <th class=center>Move Right</th>\n";
$menu .= " <th class=center>Zoom In</th>\n";
$menu .= " <th class=center>Zoom out</th>\n";
$menu .= "</tr>\n";
$menu .= "<tr>\n";
$menu .= " <td $firstcell><input type=button class=button value='15%'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionleft15'\">"; 
$menu .= " <input type=button class=button value='50%'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionleft50'\">";
$menu .= " <input type=button class=button value='95%'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionleft95'\"></td>\n";
$menu .= " <td><input type=button class=button value='15%'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionright15'\">"; 
$menu .= " <input type=button class=button value='50%'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionright50'\">";
$menu .= " <input type=button class=button value='95%'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionright95'\"></td>\n";
$menu .= " <td><input type=button class=button value='1.5x'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionin15'\">"; 
$menu .= " <input type=button class=button value='5x'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionin5'\">";
$menu .= " <input type=button class=button value='10x'  onClick=\"location.href='index.php?$linkpart&amp;region=$regionin10'\"></td>\n";
$menu .= " <td><input type=button class=button value='1.5x' onClick=\"location.href='index.php?$linkpart&amp;region=$regionout15'\">"; 
$menu .= " <input type=button class=button value='5x' onClick=\"location.href='index.php?$linkpart&amp;region=$regionout5'\">";
$menu .= " <input type=button class=button value='10x' onClick=\"location.href='index.php?$linkpart&amp;region=$regionout10'\"></td>";
$menu .= "</tr>\n";
$menu .= "<tr>\n";
$menu .= " <td colspan=4 $firstcell><img id=chrplot src='chromosomeplot.php?pw=$plotwidth&amp;c=$chr&amp;start=$start&amp;stop=$stop' usemap='#chr' border=0 onload='loadKaryo()'></td>\n";
$menu .= "</tr>\n";
$menu .= "</table>\n";
$menu .= "</center>\n";
$menu .= "</p>\n";
echo $menu;
unset($menu);
flush();

//////////////////////////////////
// CREATE CHR POSITION PLOT MAP //
//////////////////////////////////

$result =  mysql_query("SELECT start FROM cytoBand WHERE chr = '$chr' ORDER BY start LIMIT 1");
$row = mysql_fetch_array($result);
$chrstart = $row['start'];
$result =  mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$chrstop = $row['stop'];
$chrwindow = $chrstop-$chrstart+1;
$scalechr = ($plotwidth-1)*0.80/($chrwindow);
echo "<map name='chr'>";
$query = mysql_query("SELECT start, stop, name FROM cytoBand WHERE chr= '$chr' ORDER BY start");
while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$name = $row['name'];
	$scaledstart = intval(round(($cstart)*$scalechr));
	$scaledstop = intval(round(($cstop)*$scalechr));
	$xstart = $scaledstart;
	$xend = $scaledstop;
	if ($xend - $xstart < 5) {
		$xend = $xend+1;
		$xstart = $start -1;
	}
	echo "<area shape='rect' coords='$xstart,10,$xend,20' title='". $chromhash[$chr] . "$name' href='index.php?$linkpart&amp;region=chr$chrtxt:$cstart-$cstop'>";
}
echo "</map>";

flush();

///////////////////////////
// Load the browser plot //
///////////////////////////
echo "<div id='loadingbrowser'><div style='text-align:center'><img src='images/content/ajax-loader.gif' width=50px /><br/>Loading Results...</div></div>";
flush();
echo "<img id=browser usemap='#browsermap' src='Browser/image.php?pw=$plotwidth&amp;xoff=$xoff&amp;l=$level&amp;u=$userid&amp;c=$chr&amp;start=$start&amp;stop=$stop$projpart' border=0 onload=\"document.getElementById('loadingbrowser').innerHTML='';loadArea()\">\n";
flush();

//////////////////
// LoAD the map //
////////////////// 
// check the loaded tracks
$prefq = mysql_query("SELECT LOHshow, LOHminsize, LOHminsnp, condense, condense_classes, condense_null, ecarucaSize,annotations,tracks,DGVinclude FROM usersettings WHERE id = '$userid'");
$prefrow = mysql_fetch_array($prefq);
$LOHshow = $prefrow['LOHshow'];
$LOHminsize = $prefrow['LOHminsize'];
$LOHminsnp = $prefrow['LOHminsnp'];
$Condense = $prefrow['condense'];
$ToCondense = $prefrow['condense_classes'];
$NullCondense = $prefrow['condense_null'];
$ecarucaSize = $prefrow['ecarucaSize'];
$annotations = $prefrow['annotations'];
$showtracks = explode(',',$prefrow['tracks']);
$DGVinc = $prefrow['DGVinclude'];
//start map
echo "<map id='browsermap' name='browsermap'>";
// add the link to preference settings
$xstart = $plotwidth-75;
$xstop = $plotwidth;
echo "<area shape='rect' coords='$xstart,1,$xstop,10' title='Set Plot Preferences' onClick=\"LoadPreferences('PickTracks')\">";
// starting height (scale/pref) = 25
$fullheight = 25; // this increases for each track. 
$prevpacking = '';
$prevgroup = '';
$nrsubgroups = 0;

//////////////////
// LOAD FILTERS //
//////////////////
$exp = array("IlluminaProjectSamples","CustomProjectSamples","IlluminaSearchSamples","CustomSearchSamples","Healthy","Carriers");
$query = mysql_query("SELECT f.Name, uf.fid,  uf.Type, uf.ApplyTo,fr.start, fr.stop,fr.rid,fr.Comment FROM `Users_x_Filters` uf JOIN `Filters_x_Regions` fr JOIN `Filters` f ON f.fid = fr.fid AND fr.fid = uf.fid WHERE uf.uid = '$userid' AND uf.Type != 0 AND fr.chr = '$chr' AND ((fr.start BETWEEN $start AND $stop) OR (fr.stop BETWEEN $start AND $stop) OR (fr.start <= $start AND fr.stop >= $stop)) ORDER BY Type ASC");
$filter = array("1" => array(), "2" => array(), "3" => array());
while ($row = mysql_fetch_array($query)) {
	// adjust size to viewed region
	if ($row['start'] < $start) {
		$row['start'] = $start;
	}
	if ($row['stop'] > $stop) {
		$row['stop'] = $stop;
	}
	// create fid entry in filter hash
	if (!isset($filter[$row['Type']][$row['fid']])) {
		$filter[$row['Type']][$row['fid']] = array();
	}
	// create applyTo entry in filter hash for fid
	if (!isset($filter[$row['Type']][$row['fid']]['at'])) {
		if ($row['ApplyTo'] == 'Experimental') {
			$at = $exp;
		}
		else {
			$at = explode(";",$row['ApplyTo']);
		}
		$filter[$row['Type']][$row['fid']]['at'] = $at;
	}
	// create regions array for fid 
	if (!isset($filter[$row['Type']][$row['fid']]['regions'])) {
		$filter[$row['Type']][$row['fid']]['regions'] = array();
	}
	$filter[$row['Type']][$row['fid']]['regions'][$row['rid']] = array();		
	$filter[$row['Type']][$row['fid']]['regions'][$row['rid']]['start'] = $row['start'];
	$filter[$row['Type']][$row['fid']]['regions'][$row['rid']]['stop'] = $row['stop'];
	$filter[$row['Type']][$row['fid']]['regions'][$row['rid']]['comment'] = $row['Comment'];
	$filter[$row['Type']][$row['fid']]['Name'] = $row['Name'];	
}	


//process tracks
$printed = 0;
foreach($showtracks as $key => $trackname) {
	$nrsubgroups = 0; // newtrack 
	// skip if empty trackname
	if ($trackname == '') continue;
	if (!file_exists("Browser/Tracks/$trackname.xml")) continue;
	// track can be printed. increment counter
	$printed++;
	// load track configuration (include Browser/ as this code is included from root path.
	$Array = simplexml_load_file("Browser/Tracks/$trackname.xml");
	// skip if track is hidden. 
	if (isset($Array->{'plot'}->{'HideOn'})) {
		foreach ($Array->{'plot'}->{'HideOn'}->children() as $ckey => $cval) {
			settype($ckey,'string');
			if (${$ckey} == $cval) {
				$hidden[$trackname] = 1;
				continue 2; // goto next track
			}
		}
	}

	// set packing style
	$packing = $Array->{'packing'};
	// define start Y based on packing
	$y = $fullheight;
	// remember top height for filter onhover event
	$fheight = $y+3;
	// process
	if ($Array->{'plot'}->{'group'} != '' && $Array->{'plot'}->{'group'} != "$prevgroup") {
		//newgroup
		$newgroup = 1;
		$prevgroup = $Array->{'plot'}->{'group'};
		$y += 12;
		if ($packing == 'full') {
			$y += 12;
		}
	}
	else {
		if ($packing == 'full') {
			$y += 17;
		}
		$newgroup = 0;
	}
	$afterfull = 0;
	if ($packing == 'pack' && $prevpacking == 'full') {
		$y = $y+3;
		$afterfull = 1;
	}
	$prevpacking = $packing;
	// set map to track title
	$displayname = $Array->{'plot'}->{'displayname'};
	settype($displayname,'string');
	$ystart = $y-10;
	if ($packing == 'pack') {
		if ($prevpacking == 'full') {
			$ystart += 3;
		}
		else {
			$ystart += 10;
		}
		if (strlen($displayname) > 19) {
			$displayname = '..'.substr($displayname, (strlen($displayname)-18));
		}
		$fontwidth = imagefontwidth(2);
		$txtwidth = strlen($displayname)*$fontwidth;
		$txtx = $xoff-$txtwidth-3;
		$yend = $ystart+10;
		echo "<area shape='rect' coords='$txtx,$ystart,$xoff,$yend' title='$displayname' href=\"javascript:void('0')\">\n";
	}
	elseif ($packing == 'full')  {
		$ystart -= 3;
	 	// print tracklabel
		$fontwidth = imagefontwidth(2);
		$txtwidth = strlen($displayname)*$fontwidth;
		$xstart = $xoff + ($plotwidth-$xoff)/2 - $txtwidth/2;
		$xend = $xstart + $txtwidth;
		#if ($newgroup == 1) {
		#	$ystart -= 15;
		#}
		$yend = $ystart + 10;
		echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$displayname' href=\"javascript:void('0')\">\n";
	}
	// check for custom code
	$customcode = $Array->{'map'}->{'CustomCode'};
	
	if ($customcode == 1) {
		include("CustomCodes/Map/inc_$trackname.inc");
	}
	else{
		// check onHover event
		if (isset($Array->{'map'}->{'onHover'}->{$packing})) {
			$onHover = $Array->{'map'}->{'onHover'}->{$packing};
			// get query 
			$queries = $Array->xpath('map/database/query');
			if (count($queries) > 1) { // multiple queries, search packing-match
				$lastquery = '';
				foreach ($queries as $querynode) {
					$lastquery = $querynode;
					if ($querynode->attributes()->{'packing'} == "$packing") {
						$querystring = $querynode;
						break;
					}		
				}
				// pick last query if no packing match.
				if ($querystring == '') {
					$querystring = $lastquery;
				}
			}
			else { // only a single query, use this.
				$querystring = $Array->{'map'}->{'database'}->{'query'};
			}
			$querystring = str_replace('%start%', $start, $querystring);
			$querystring = str_replace('%stop%', $stop, $querystring);
			$querystring = str_replace('%chr%', $chr, $querystring);
			$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
			$querystring = str_replace('%project%', $project, $querystring);
			$querystring = str_replace('%userid%',$userid,$querystring);
			$querystring = str_replace('%condensepart%','',$querystring);
			$querystring = str_replace('%ecarucaSize%',$ecarucaSize,$querystring);
			$query = mysql_query($querystring);
			// get data
			$data = array();
			$nrfiltered = 0;
			while ($row = mysql_fetch_array($query)) {
				
				## skip filtered LOH regions .
				if (isset($row['gender']) && $chr >= 23 && ($row['gender'] == 'Male' || $row['gender'] == '')) {
					// DO NOT SKIP LOH for male ON X/Y
				}
				elseif (isset($Array->{'Filters'}->{'LOH'}) && $row['color'] == 'color2') {
					## filter if lohshow == 0
					if ($LOHshow == 0 ) {
						$nrfiltered++;
						continue;
					}
					## filter if too small
					if ($LOHminsize > ($row['stop'] - $row['start'] + 1)) {
						$nrfiltered++;
						continue;
					}
					## filter if not enough snps
					if ($LOHminsnp > $row['nrsnps']) {
						$nrfiltered++;
						continue;
					}
				}
				$data[] = $row;
			}
			// print map based on packing type
			switch($packing) {
				case 'full':
					$nrrows = print_map_full($onHover,$data, $y);	
					$y += $nrrows*10;
					$fullheight += 17 + $nrrows*10 + $newgroup*12 + $nrsubgroups*10 ;
					break;
				case 'pack';
					$y = print_map_pack($onHover,$data,$y);
					$fullheight += 14 + $newgroup*12 + $afterfull*3 + $nrsubgroups*10;
					break;
			}
		}
		else {
			// no onhover specified, skip to next track.
			// todo: increase fullheight !
			if ($packing == 'pack') {
				if ($prevpacking == 'full') {
					$tmpheight = 17;
				}
				else {
					$tmpheight = 14 + $newgroup*12 + $afterfull*3 + $nrsubgroups*10;;
				}
			}
			elseif ($packing == 'full') {
				if (isset($Array->{'plot'}->{'database'}->{'countquery'})){
					$queries = $Array->xpath('plot/database/countquery');
					if (count($queries) > 1) { // multiple queries, search packing-match
						$lastquery = '';
						foreach ($queries as $querynode) {
						$lastquery = $querynode;
							if ($querynode->attributes()->{'packing'} == "$packing") {
								$querystring = $querynode;
								break;
							}		
						}
						// pick last query if no packing match.
						if ($querystring == '') {
							$querystring = $lastquery;
						}
					}
					else { // only a single query, use this.
						$querystring = $Array->{'plot'}->{'database'}->{'query'};
					}
	
					$querystring = $Array->{'plot'}->{'database'}->{'countquery'};
					$querystring = str_replace('%start%', $start, $querystring);
					$querystring = str_replace('%stop%', $stop, $querystring);
					$querystring = str_replace('%chr%', $chr, $querystring);
					$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
					$querystring = str_replace('%project%', $project, $querystring);
					$querystring = str_replace('%userid%',$userid,$querystring);
					$querystring = str_replace('%ecarucaSize%',$ecarucaSize,$querystring);
					$query = mysql_query($querystring);
					$tmpheight = 17 + 10*mysql_num_rows($query);  
					// take into account subgroups etc
				}
				else {
					$tmpheight = 17 + 10*count($data);
					// take into account subgroups etc.
				}
			}
			if ($plotruler == 1) {
				$tmpheight += 10;
			}
			else {
				$fulloffset = 0; 
			}
			$fullheight += $tmpheight;
			$prevpacking = $packing;
			continue;	
		}
	}
	// filter : onhover event in top 8px of every track.
	for ($ftype = 1; $ftype <= 3; $ftype++) {
		foreach ($filter["$ftype"] as $fid => $array) {
			// check if it is applied to this filter.
			if (in_array("$trackname", $filter["$ftype"][$fid]['at'])) {
				// loop $region_ids	
				foreach ($filter["$ftype"][$fid]['regions'] as $rid => $rarray) {
					$fstart = $rarray['start'];
					$fstop = $rarray['stop'];
					$fcomment = $rarray['comment'];
					$fscaledstart = intval(round(($fstart-$start) * $scalef));
					$fscaledstop = intval(round(($fstop-$start) * $scalef));
					$xstart = $xoff + $fscaledstart;
					$xend = $xoff + $fscaledstop;
					if ($xend - $xstart < 5) {
						$xend = $xend + 1;
						$xstart = $xstart - 1;
					}
					if ($ftype == 1) {
						$yend = $fullheight - 2;
					}
					else {
						$yend = $fheight + 8;
					}
					$title = "Filter: ".$filter["$ftype"][$fid]['Name'] .":\n".$fcomment;
					#$title = nl2br($title);
					echo "<area shape='rect' coords='$xstart,$fheight,$xend,$yend' title='$title' href=\"javascript:void('0')\">\n";
				}
			}
		}
	}	

}

echo "</map>";

// if $printed == 0 => show the overlay.
if ($printed == 0) {
	echo "<script type='text/javascript'>\n";
	// set content main
	$content = '<h3 >Problem detected: No display tracks set.</h3>';
	$content .= '<p >Please pick the tracks you want to display in the genome browser below and press "Submit". <br/>You can change this selection by clicking on "Preferences" in the top right corner of the plot.</p>';
	// start form to submit selection
	$action = "index.php?";
	// add get variables to action
	foreach ($_GET as $gkey => $gval) {
		$action .= "$gkey=$gval&";
	}
	$action = substr($action,0,-1);
	$content .= "<form action=\"$action\"  method=POST>";
	$content .= "<input type=hidden name=\"updatepreferences\" value=1 />";
	// add posted variables as hidden fields
	foreach ($_POST as $pkey => $pval) {
		$content .= "<input type=hidden name=\"$pkey\" value=\"$gval\" />";
	}
	// set content track list & selection
	$Array = simplexml_load_file("Browser/Main_Configuration.xml");
	$content .= "<form action=\"\"  method=POST>";
	$content .= "<table cellspacing=0>";
	$content .= "<tr><th>Show</th><th>Track Name</th><th>Track Description</th></tr>";
	foreach ($Array->{'tracks'}->children() as $ckey => $cval) {
		if ($Array->{'tracks'}->{$ckey}->{'enabled'} != 1) {
			continue;
		}
		$label = $Array->{'tracks'}->{$ckey}->{'description'};
		$content .= "<tr><td><input type=\"checkbox\" name=\"tracks[]\" value=\"$ckey\" /></td><td>$ckey</td><td>$label</td></tr>";
	}	
	$content .= "<tr><td class=last colspan=3>&nbsp;&nbsp;</td></tr></table>";
	$content .= "<p><input type=submit class=button value=\"Update Tracks\" name=\"set\">";
	$content .= "</form></p>";
	// print content
	echo "document.getElementById('infodiv').innerHTML = '$content';\n";	
	// set display to true
	echo "document.getElementById('infodiv').style.display = '';\n";
	echo "</script>\n";
}
?>
</div>
<script type="text/javascript">
// passed variables from php
var plotstart = <?php echo $start; ?>;
var plotstop = <?php echo $stop; ?>;
var xoff = <?php echo $xoff; ?>;
var plotwidth = <?php echo $plotwidth; ?>;
var chrstart = <?php echo $chrstart; ?>;
var chrstop = <?php echo $chrstop; ?>;
var scalechr = <?php echo $scalechr; ?>;
var scalef = (plotwidth  - xoff - 1)/(plotstop - plotstart + 1);
var chrom = '<?php echo $chrtxt; ?>';
function loadArea() {
     $('img#browser').imgAreaSelect({
        handles: false,
	autoHide: true,
	fadeSpeed: 50,
	movable: false,
	minHeight: document.getElementById('browser').height,
	onSelectEnd: function (img, selection) {
		// calculate selected coordinates;	
		var window = plotstop - plotstart + 1;
		var selstart = selection.x1;
		var selstop = selection.x2;
		if (selstart < xoff) {
			return;
		}
		if (selstop - selstart < 2) {
			return;
		}
        	var newstart = Math.round(plotstart + (selstart - xoff + 1)/scalef);
		var newstop = Math.round(plotstart + (selstop - xoff + 1)/scalef);
		//alert('selected width: ' + plotwidth + '; target from ' + newstart + ' to ' + newstop );
		location.href='index.php?<?php $linkpart = str_replace('amp;','',$linkpart);echo $linkpart; ?>&region=chr'+chrom+':'+newstart+'-'+newstop; 
   	} 
    });
}

function loadKaryo() {
     $('img#chrplot').imgAreaSelect({
        handles: false,
	autoHide: true,
	fadeSpeed: 50,
	movable: false,
	minHeight: document.getElementById('chrplot').height,
	onSelectEnd: function (img, selection) {
		// calculate selected coordinates;	
		var window = chrstop - chrstart + 1;
		var selstart = selection.x1;
		var selstop = selection.x2;
		//if (selstart == 0 ) {
		//	return;
		//}
		if (selstop - selstart < 2) {
			return;
		}
		//alert('start '+chrstart+' stop '+chrstop+' scale '+chrscale);
        	var newstart = Math.round(chrstart + ( selstart + 1 )/ (scalechr));
		var newstop = Math.round(chrstart + (selstop + 1)/scalechr);
		//alert('selected width: ' + plotwidth + '; target from ' + newstart + ' to ' + newstop );
		location.href='index.php?<?php $linkpart = str_replace('amp;','',$linkpart);echo $linkpart; ?>&region=chr'+chrom+':'+newstart+'-'+newstop; 
   	} 
    });
}

function LoadPreferences(subtype) {
	// put temporary response in place
	document.getElementById('infodiv').innerHTML = "<div style='text-align:center;height:100%;padding-top:15%'><img src='images/layout/loading.gif' height=50px /><br/><h3>Loading Preferences...</h3></div>";
	// set height
	var D = document;
    	var bodyh =  Math.max(
	        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
       	 	Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
       	 	Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    		);
	bodyh = bodyh*1 - 25*1; 
	//if (divh < bodyh) {
		document.getElementById('infodiv').style.height = bodyh+'px';
	//}
	// show the div
	document.getElementById('infodiv').style.display = '';
	// fetch the preferences pages with ajax
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
	  alert ("Browser does not support HTTP Request");
	  return;
	  }
	var url="Browser/setplotprefs.php";
	url=url+"?subtype="+subtype;
	url=url+"&rv="+Math.random();
	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState==4)
		{
			
			// start form to submit selection
			// add get variables to action
			<?php 
			$newcontent = "index.php?";
			foreach ($_GET as $gkey => $gval) {
				$newcontent .= "$gkey=$gval&";
			}
			$newcontent = substr($newcontent,0,-1);
			$newcontent = "<form action=\"$newcontent\"  method=POST>";
			// add hidden variable triggering db update
			$newcontent .= "<input type=hidden name=\"updatepreferences\" value=1 />";
			// add posted variables as hidden fields
			foreach ($_POST as $pkey => $pval) {
				$newcontent .= "<input type=hidden name=\"$pkey\" value=\"$pval\" />";
			}
			//print the newcontent
			echo "document.getElementById('infodiv').innerHTML='$newcontent'+xmlhttp.responseText+'</form>';";
			?>	
			// readjust height if needed
    			bodyh =  Math.max(
			        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
       	 			Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
		       	 	Math.max(D.body.clientHeight, D.documentElement.clientHeight)
		    		);
			bodyh = bodyh*1; 
			document.getElementById('infodiv').style.height = bodyh+'px';
			window.scrollTo(0,0);
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}


</script>

<div id="txtHint">something must appear here</div>
</div><!-- end of relative position div -->
<?php

//////////////
//FUNCTIONS //
//////////////

