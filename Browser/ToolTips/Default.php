<?php
// get variables
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$trackname = $_GET['track'];
$uid = $_GET['u'];
$packing = $_GET['packing'];
if (array_key_exists('id',$_GET) && $_GET['id'] != '-1') {
	$aid = $_GET['id'];
}
else {
	$aid = '';
}
$userid = $uid;

// GENERAL VARIABLES 
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chrtxt = $chromhash[$chr];

ob_start();
// CONNECT TO DATABASE 
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = substr($_SESSION["dbname"],1);

// 1: Get some info from track.xml
if ($trackname == '') {
	echo "<span class=nadruk>Error:</span><br/>TrackName missing.";
	exit;
}
if (!file_exists("../Tracks/$trackname.xml")) {
	echo "<b>Error:</b><br/>Track Configuration XML not found.";
	exit;
}
$Array = simplexml_load_file("../Tracks/$trackname.xml");
$type = $Array->{'type'}; // gene vs bar
$displayname = $Array->{'plot'}->{'displayname'};

// 2. Tooltip Title
echo "<div>";
echo "<div class=nadruk>$displayname</div>";
echo "<span style='position:absolute;right:2px;top:1px;'>";
echo "<a class=tt style='font-style:italic;font-size:0.8em;' href='javascript:void(0)' onclick=\"LoadPreferences('PickDBlinks')\">Set Resources</a>";
echo "</span>";
echo "</div>";
echo "<p style='margin-top:0em;margin-left:1em;'>";
echo "- Region: chr$chrtxt:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',') ."<br/>";

//3. plot pack members for region based query (non-full with aid)
if ($aid == '') {
   switch($type) {
	case 'gene':
		echo "- All Transcripts:<br/>";		
		echo "<img style='margin-left:1em;margin-top:0.2em;' src='Browser/ToolTips/Default_pack_plot.php?c=$chr&amp;start=$start&amp;stop=$stop&amp;track=$trackname' border=0>\n";
		break;
	case 'bar' :
		echo "- All Entries:</br>";
		echo "<img style='margin-left:1em;margin-top:0.2em;' src='Browser/ToolTips/Default_pack_plot.php?c=$chr&amp;start=$start&amp;stop=$stop&amp;track=$trackname' border=0>\n";
		break;
   }
}

// set type for db query: bar or gene
switch($type) {
	case 'gene':
		$rtype = 1;
		break;
	case 'bar' :
		$rtype = 2;
		break;
}


// 4. Resources

// get all links from user settings
$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$uid'");
$row = mysql_fetch_array($query);
$db_links = $row['db_links'];
// get actual links from db_links table matching the resource type (rtype)
$query = mysql_query("SELECT Resource, link, Description FROM `db_links` WHERE type = '$rtype' AND id IN ($db_links)");
$links = array();
$idx = 0;
while ($row = mysql_fetch_array($query)) {
	$links[$idx]['Resource'] = $row['Resource'];
	$links[$idx]['link'] = $row['link'];
	$links[$idx]['descr'] = $row['Description'];
	$idx++;
}
// load data from the resource query. 
$queries = $Array->{'map'}->{'resources'}->{'query'};
if (count($queries) > 1) { // multiple queries, search packing-match
	$lastquery = '';
	foreach ($queries as $querynode) {
		$lastquery = $querynode;
		if ($querynode->attributes()->{'packing'} == "$packing") {
			$rquery = $querynode;
			break;
		}		
	}
	// pick last query if no packing match.
	if ($rquery == '') {
		$rquery = $lastquery;
	}
}
else { // only a single query, use this.
	$rquery = $Array->{'map'}->{'resources'}->{'query'};
}
settype($rquery,'string');
$rquery = str_replace('%start%', $start, $rquery);
$rquery = str_replace('%stop%', $stop, $rquery);
$rquery = str_replace('%chr%', $chr, $rquery);
$rquery = str_replace('%chrtxt%', $chrtxt, $rquery);
if (isset($project) && $project != '') $rquery = str_replace('%project%', $project, $rquery);
$rquery = str_replace('%userid%',$userid,$rquery);
$rquery = str_replace('%condensepart%','',$rquery);
if ($aid != '' ) $rquery = str_replace('%aid%', $aid, $rquery);
$rmap = $Array->{'map'}->{'resources'}->{'map'};
$dmap = $Array->{'map'}->{'resources'}->{'datamap'};
// for each data, loop links, and replace stuff in resource-map.
$query = mysql_query($rquery);
while ($row = mysql_fetch_array($query)) {
	echo "<p><span class='nadruk' >".$row['Title'] . "</span>"; //." (chr$chrtxt:".$row['Start']."-".$row['Stop'].")</span>";
	echo "<ul id='ul-simple' style='margin-top:-1em' >";
	$idx = 1;
	while (array_key_exists('data'.$idx,$row) && $row['data'.$idx] != '') {
		
		$itemname = $dmap->{'data'.$idx};
		# split attribute defines the item delimiter.
		$items = array(); 
		if (isset($itemname->attributes()->{'split'})) {
			$split_on = $itemname->attributes()->{'split'};
			$items = explode($split_on,$row['data'.$idx]);
		}
		else {
			$items[] = $row['data'.$idx];
		}	
		settype($itemname,'string');
		echo "<li style='padding-left:1em;'>- <span class='underline'>$itemname :</span>";
		echo "<ul id=ul-simple>";
		foreach ($items as $key => $item) {
			if ($item == '' || preg_match('/^\s+$/',$item)) {
				continue;
			}
			else {
				echo "<li style='padding-left:1em;'>- $item</li>";	
			}
		}
		echo "</ul>";
		$idx++;	
	}
	$first = 1;	
	foreach ($links as $idx => $array) {
		$name = $array['Resource'];
		$link = $array['link'];
		$title = $array['descr'];
		$replaced = 0;
		foreach ($rmap->children() as $key => $value) {
			settype($key,'string');
			settype($value,'string');
			if (strstr($link, '%'.$key))  {
				if ($row[$value] == '') {
					continue 2;
				}
				$link = str_replace('%'.$key,$row[$value],$link);
				$replaced = 1;
			}
		}
		// check for %r (region)
		if (strstr($link,'%r')) {
			$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
			$replaced = 1;
		}
		// check for %u (ucsc build)
		if (strstr($link,'%u')) {
			$link = str_replace('%u',"$ucscdb",$link);
			$replaced = 1;
		}
		// check for itemid (%a)
		if (strstr($link,'%a') && $aid != '') {
			$link = str_replace('%a',"$aid",$link);
			$replaced = 1;
		}

		if ($replaced == 0 && strstr($link,'%')) {
			#echo" skipped: $link<br/>";
			continue;
		}
		if ($first == 1) {
			echo "<li style='padding-left:1em;'>- <span class=underline>Public Resources :</span>";
			echo "<ul id=ul-simple>";
			$first = 0;
		}
		echo "<li style='padding-left:1em;'>- <a class=tt title='$title' href='$link' target='_blank'>$name</a></li>";
	}
	if ($first == 0) {
		echo "</ul>";
	}
	echo "</li></p>";
}

?>

