<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["X"] = "X";
$chromhash["Y"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$trackname = $_GET['track'];

// LOAD FUNCTIONS //
require('../functions.php');

// load the 'full' view query
$Array = simplexml_load_file("../Tracks/$trackname.xml");
$type = $Array->{'type'}; // gene vs bar
$querystring = '';
$source = $Array->{'plot'}->{'source'};
// check for packing-dependent queries
if ($source == 'database'){
  $queries = $Array->xpath('plot/database/query');
  if (count($queries) > 1) { // multiple queries, search packing-match
	$lastquery = '';
	foreach ($queries as $querynode) {
		$lastquery = $querynode;
		if ($querynode->attributes()->{'packing'} == "full") {
			$querystring = $querynode;
			break;
		}		
	}
	// pick last query if no packing match.
	if ($querystring == '') {
		$querystring = $lastquery;
	}
  }
  else { // only a single query, use this.
	$querystring = $Array->{'plot'}->{'database'}->{'query'};
  }
}
$querystring = str_replace('%start%', $start, $querystring);
$querystring = str_replace('%stop%', $stop, $querystring);
$querystring = str_replace('%chr%', $chr, $querystring);
$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
$querystring = str_replace('%project%', $project, $querystring);
$querystring = str_replace('%userid%',$userid,$querystring);
$querystring = str_replace('%condensepart%','',$querystring);
$query = mysql_query($querystring);
//load data
$data = array();
while ($row = mysql_fetch_array($query)) {
	$data[] = $row;
}

// set alerting
$alerting = array();
if (isset($Array->{'plot'}->{'Alerting'})) {
	$alerting = $Array->{'plot'}->{'Alerting'};
}

if (isset($Array->{'map'}->{'tooltip'}->{'labelwidth'})){
	$xoff = $Array->{'map'}->{'tooltip'}->{'labelwidth'};
	settype($xoff,'integer');
}
elseif (stristr($querystring, 'ItemName') || stristr($querystring, 'grouping')) {
	$xoff = 75;
}
else {
	$xoff = 153;
}
# DEFINE IMAGE PROPERTIES
// set width & window
#$xoff = 100;
$width = 410;
$window = $stop-$start+1;
$scalef = ($width-$xoff)/($window);
// calculate height
if (isset($Array->{'plot'}->{'database'}->{'countquery'})){
	$queries = $Array->xpath('plot/database/countquery');
	if (count($queries) > 1) { // multiple queries, search packing-match
		$lastquery = '';
		foreach ($queries as $querynode) {
			$lastquery = $querynode;
			if ($querynode->attributes()->{'packing'} == "$packing") {
				$querystring = $querynode;
				break;
			}		
		}
		// pick last query if no packing match.
		if ($querystring == '') {
			$querystring = $lastquery;
		}
	}
	else { // only a single query, use this.
		$querystring = $Array->{'plot'}->{'database'}->{'query'};
	}
	$querystring = $Array->{'plot'}->{'database'}->{'countquery'};
	$querystring = str_replace('%start%', $start, $querystring);
	$querystring = str_replace('%stop%', $stop, $querystring);
	$querystring = str_replace('%chr%', $chr, $querystring);
	$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
	$querystring = str_replace('%project%', $project, $querystring);
	$querystring = str_replace('%userid%',$userid,$querystring);
	$query = mysql_query($querystring);
	$height = 25 + 10*mysql_num_rows($query);
}
else {
	$height = 25 + 10*count($data);
}



//Create the image resource
$image = ImageCreate(($width+5), $height);
// load colors
$colors = array();
$colors = LoadColors($image,'../Main_Colors.xml');
// get the color mapping array
$colormap = array();
if(isset($Array->{'plot'}->{'colormap'})) {
	$colormapxml = $Array->{'plot'}->{'colormap'};
	foreach ($colormapxml->children() as $ckey => $cval ) {
		settype($ckey,'string');
		settype($cval,'string');
		$colormap[$ckey] = $cval; 
	}
}

#Fill background
imagefill($image,0,0,$colors['white']);

//needed variables
$y = 20;
$cy = $y;
$cstudid = '';
$prevgrouping = '';
$prevlabel = '';


foreach ($data as $key => $row) {
	// print label. If grouping variable is present, use this.
	if (isset($row['grouping'])) {
		if ($row['grouping'] != $prevgrouping) {
			//new item, print name
			if (strlen($prevlabel) > 19) {
				$printlabel = '..'.substr($prevlabel, (strlen($prevlabel)-18));
			}
			else {
				$printlabel = $prevlabel;
			}
			$fontwidth = imagefontwidth(1);
			$txtwidth = strlen($printlabel)*$fontwidth;
			$txtx = $xoff-$txtwidth-3;
			imagestring($image,1,$txtx,$y-2,$printlabel,$colors['black']);
			$prevlabel = $row['grouplabel'];
			// increase $y if second group
			if ($prevgrouping != '') {
				$y +=10;
			}
		}
		// set prevgrouping to current grouping
		$prevgrouping = $row['grouping'];
	}
	// no grouping variable available, use itemname if present
	elseif (isset($row['ItemName'])) {
		// trim item name if needed
		$fontwidth = imagefontwidth(1);
		if (strlen($row['ItemName'])*$fontwidth > $xoff - 3) {
			$row['ItemName'] = '..' . substr($row['ItemName'],(strlen($row['ItemName']) - ceil(($xoff - 3) /$fontwidth)));
		}
		#if (strlen($row['ItemName']) > 19) {
		#	$row['ItemName'] = '..' . substr($row['ItemName'], (strlen($row['ItemName'])-18));
		#}
		#$fontwidth = imagefontwidth(1);
		$txtwidth = strlen($row['ItemName'])*$fontwidth;
		$txtx = $xoff-$txtwidth-3;
		// do not skip line for first entry, set prevgrouping to anything not zero
		if ($prevgrouping != '') {
			$y+=10;
		}
		$prevgrouping = 1;
		// print gene name
		imagestring($image,1,$txtx,$y-2,$row['ItemName'],$colors['black']);
	}
	// no name indication present, use region
	else {
		$thelabel = "Chr".$chromhash[$chr].":".number_format($row['start'],0,'',',').'-'.number_format($row['stop'],0,'',',');
		$fontwidth = imagefontwidth(1);
		if (strlen($thelabel)*$fontwidth > $xoff - 3) {
			$thelabel = '..' . substr($thelabel,(strlen($thelabel) - ceil(($xoff - 3) /$fontwidth)));
		}
		$txtwidth = strlen($thelabel)*$fontwidth;
		$txtx = $xoff-$txtwidth-3;
		// do not skip line for first entry, set prevgrouping to anything not zero
		if ($prevgrouping != '') {
			$y+=10;
		}
		$prevgrouping = 1;
		// print gene name
		imagestring($image,1,$txtx,$y-2,$thelabel,$colors['black']);

	}
	// distinction for plotting genes and bars
	if ($type == 'gene') {
		// position
		$cstart = $row['start'];
		$cstop = $row['stop'];
		$cstrand = $row['strand'];
		// get color from database
		if (isset($row['color'])) {
			$ccolorname = guess_color($colormap,$row['color'],$colors);
		}
		else {
			$ccolorname = 'black';
		}
		// define alerts to apply
		$tmp = choose_color($alerting, $row,$ccolorname);
		$ccolorname = $tmp[0];
		$drawbox = $tmp[1];
		settype($ccolorname,'string');
		$ccolor = $colors[$ccolorname];
		// get positions of 'exons'
		$cexstart = $row['exonstarts'];
		$cexstarts = explode(',',$cexstart);
		$cexend = $row['exonends'];
		$cexends = explode(',',$cexend);
		// correct if extending beyond plot
		if ($cstart < $start) $cstart = $start;
		if ($cstop > $stop) $cstop = $stop;
		// scale coordinates
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		// draw general boundary line
		imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$ccolor);
		// add arrows based on strand
		if ($cstrand == '+' || $cstrand == 1) {
			$hooktip = $xoff + $scaledstart + 5;
			while ($hooktip <= $scaledstop + $xoff -1) {
				imageline($image,$hooktip,$y+2,$hooktip-2,$y,$ccolor);
				imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$ccolor);
				$hooktip = $hooktip +5;
			}
		}
		elseif ($cstrand == '-' || $cstrand == '-1')  {
			$hooktip = $xoff + $scaledstart +2;
			while ($hooktip <= $scaledstop + $xoff - 5) {
				imageline($image,$hooktip,$y+2,$hooktip+2,$y,$ccolor);
				imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$ccolor);
				$hooktip = $hooktip +5;
			}
		}
		foreach($cexstarts as $key => $cexs) {
			$cexe = $cexends[$key];
			if ($cexe < $start) continue;  // skip if end < plot-start
			if ($cexs > $stop ) continue; // skip if start > plot-start
			$cexscaledstart = intval(round(($cexs-$start) * $scalef));
			$cexscaledstop = intval(round(($cexe-$start) * $scalef));
			if ($cexscaledstop-$cexscaledstart < 1){
				$cexscaledstop = $cexscaledstart +1;
			} 
			imagefilledrectangle($image,$xoff+$cexscaledstart,$y-1,$xoff+$cexscaledstop,$y+5,$ccolor);
		}
	}
	elseif ($type == 'bar') {
		// position
		$cstart = $row['start'];
		$cstop = $row['stop'];
		// get color from database
		if (isset($row['color'])) {
			$ccolorname = guess_color($colormap,$row['color'],$colors);
		}
		else {
			$ccolorname = 'black';
		}
		// define alerts to apply
		$tmp = choose_color($alerting, $row,$ccolorname);
		$ccolorname = $tmp[0];
		$drawbox = $tmp[1];
		settype($ccolorname,'string');
		$ccolor = $colors[$ccolorname];
		// correct if extending beyond plot
		if ($cstart < $start) $cstart = $start;
		if ($cstop > $stop) $cstop = $stop;
		// scale coordinates
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		// plot region rectangle
		imagefilledrectangle($image,$xoff+$scaledstart,$y-1,$xoff+$scaledstop,$y+5,$ccolor);
		//draw box
		if ($drawbox == 1) {
			imagerectangle($image,$xoff+$scaledstart-2,$y-3,$xoff+$scaledstop+2,$y+7,$colors['black']);
		}
		// check for error bars
		if (substr($ccolorname,0,5) != 'light') {
			$errorcolor = 'light'.$ccolorname;
		}
		else {
			$errorcolor = $ccolorname;
		}
		if (isset($row['errorlow']) && $cstart > $start) {
			if ($row['errorlow'] < $start) {
				$elow = $start;
			}
			else {
				$elow = $row['errorlow'];
			}
			$scaledlow = intval(round(($elow - $start)*$scalef));
			# horizontal
			imagefilledrectangle($image,$xoff+$scaledlow,$y+2,$xoff+$scaledstart-1,$y+2,$colors[$errorcolor]);
			# vertical
			if ($row['errorlow'] >= $start) {
				imageline($image,$xoff+$scaledlow,$y-1,$xoff+$scaledlow,$y+5,$colors[$errorcolor]);
			}
		}
		if (isset($row['errorhigh']) && $cstop < $stop) {
			if ($row['errorhigh'] > $stop) {
				$ehigh = $stop;
			}
			else {
				$ehigh = $row['errorhigh'];
			}
			$scaledhigh = intval(round(($ehigh - $start)*$scalef));
			# horizontal
			imagefilledrectangle($image,$xoff+$scaledhigh,$y+2,$xoff+$scaledstop+1,$y+2,$colors[$errorcolor]);
			# vertical
			if ($row['errorhigh'] <= $stop) {
				imageline($image,$xoff+$scaledhigh,$y-1,$xoff+$scaledhigh,$y+5,$colors[$errorcolor]);
			}
		}
		// draw hooks if specified in xml, strand is specified and bar is sufficiently long
		if (isset($Array->{'barhooks'}) && array_key_exists('strand', $row) && $scaledstop - $scaledstart > 7){
			$cstrand = $row['strand'];
			$ccolor = $Array->{'map'}->{'tooltip'}->{'hooks'};
			settype($ccolor,'string');
			$ccolor = $colors[$ccolor];
			if ($cstrand == '+' || $cstrand == 1) {
				$hooktip = $xoff + $scaledstart + 5;
				while ($hooktip <= $scaledstop + $xoff -1) {
					imageline($image,$hooktip,$y+2,$hooktip-2,$y,$ccolor);
					imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$ccolor);
					$hooktip = $hooktip +5;
				}
			}
			elseif ($cstrand == '-' || $cstrand == '-1')  {
				$hooktip = $xoff + $scaledstart +2;
				while ($hooktip <= $scaledstop + $xoff - 5) {
					imageline($image,$hooktip,$y+2,$hooktip+2,$y,$ccolor);
					imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$ccolor);
					$hooktip = $hooktip +5;
				}
			}
		}	
	}	
}
// last label
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($prevlabel)*$fontwidth;
$txtx = $xoff-$txtwidth-3;
imagestring($image,2,$txtx,$y-2,$prevlabel,$colors['black']);
$prevlabel = $row['grouplabel'];


/*
	// check ItemName, otherwise set region as name
	if (isset($row['ItemName'])) {
		$itemname = $row['ItemName'];
		$itemname = str_replace('Chr23','ChrX',$itemname);
		$itemname = str_replace('Chr24','ChrY',$itemname);
	}
	else {
		$itemname = "Chr".$chromhash[$chr].":".number_format($cstart,0,'',',').'-'.number_format($cstop,0,'',',');
	}
	// check for alerts
	#$cstrand = $row['strand'];
	#$ochr = $row['otherchr'];
	#$ostart = $row['otherstart'];
	#$ostop = $row['otherstop'];
	#$fraction = $row['fraction'];
	#if ($fraction < 0.98) {
	#	$color = $gpos25;
	#}
	#elseif ($fraction < 0.99) {
	#	$color = $gpos50;
	#}
	#else {
	#	$color = $gpos75;
	#}
	#$otherregion = "Chr". $chromhash[$ochr] .":".number_format($ostart,0,'',','). '-'.number_format($ostop,0,'',',');
	$fraction = number_format($fraction,3,'.','');
	$y = $y + 8;
	
	imagestring($image,1,2,$y-2,$otherregion,$black);
	imagestring($image,1,150,$y-2,$fraction,$black);	
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	if ($scaledstop - $scaledstart < 1) {
		$scaledstop++;
	}
	imagefilledrectangle($image,$xoff+$scaledstart,$y+1,$xoff+$scaledstop,$y+3,$color);

*/	


# Draw the table borders
imagestring($image,2,10,1,"Entries",$colors['darkgray']);
#imagestring($image,2,147,1,"Match",$colors['darkgray']);
imagefilledrectangle($image,0,0,0,$y+10,$colors['mediumgray']);
imagefilledrectangle($image,0,0,$width+4,0,$colors['mediumgray']);
imagefilledrectangle($image,0,$y+10,$width+4,$y+10,$colors['mediumgray']);
imagefilledrectangle($image,$width+4,0,$width+4,$y+10,$colors['mediumgray']);
imagefilledrectangle($image,$xoff-2,0,$xoff-2,$y+10,$colors['mediumgray']);
#imagefilledrectangle($image,180,0,180,$y+10,$colors['mediumgray']);
imagefilledrectangle($image,0,15,$width+4,15,$colors['mediumgray']);		

#draw position indications: 
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,$xoff,7,$xoff+4,4,$colors['darkgray']);
imageline($image,$xoff,7,$xoff+4,10,$colors['darkgray']);
imageline($image,$xoff,7,$xoff+11,7,$colors['darkgray']);
#imageline($image,$xstop,18,$xstop,14,$colors['black']);
#imageline($image,$xstart,16,$xstop,16,$colors['black']);
imagestring($image,2,$xoff+14,1,$formatstart,$colors['darkgray']);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
imageline($image, $width-1,7,$width-11,7,$colors['darkgray']);
imageline($image, $width-1,7,$width-5,4,$colors['darkgray']);
imageline($image, $width-1,7,$width-5,10,$colors['darkgray']);
imagestring($image,2,$width - 12 - $txtwidth,2,$formatstop,$colors['darkgray']);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 




?>

