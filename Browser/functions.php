<?php
##########################
# COLOR LOADING FUNCTION #
##########################
function LoadColors($imagehandle,$xmlfile) {
	$array = array();
	$carray =  simplexml_load_file($xmlfile);
	foreach ($carray as $colorname => $value) {
		$r = $carray->{$colorname}->r;
		settype($r,'int');
		$g = $carray->{$colorname}->g;
		settype($g,'int');
		$b = $carray->{$colorname}->b;
		settype($b,'int');
		$a = $carray->{$colorname}->a;
		settype($a,'int');
		$array[$colorname] = imagecolorallocatealpha($imagehandle,$r,$g,$b,$a); 
	}
	return $array;
}

////////////////////////
// DRAW GRID FUNCTION //
////////////////////////
function DrawGrid($imagehandle,$xoff,$height,$start,$stop,$scalef,$linecolor) {
	$window = $stop-$start+1;
	// line positions
	$xline1 = intval(round(($start-$start)*$scalef));
	$xline2 = intval(round((($window / 10))*$scalef));
	$xline3 = intval(round((($window / 10)*2)*$scalef));
	$xline4 = intval(round((($window / 10)*3)*$scalef));
	$xline5 = intval(round((($window / 10)*4)*$scalef));
	$xline6 = intval(round((($window / 10)*5)*$scalef));
	$xline7 = intval(round((($window / 10)*6)*$scalef));
	$xline8 = intval(round((($window / 10)*7)*$scalef));
	$xline9 = intval(round((($window / 10)*8)*$scalef));
	$xline10 = intval(round((($window / 10)*9)*$scalef));
	$xline11 = intval(round(($stop-$start)*$scalef));
	// plot lines
	imageline($imagehandle,$xoff+$xline1,0,$xoff+$xline1,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline2,0,$xoff+$xline2,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline3,0,$xoff+$xline3,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline4,0,$xoff+$xline4,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline5,0,$xoff+$xline5,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline6,0,$xoff+$xline6,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline7,0,$xoff+$xline7,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline8,0,$xoff+$xline8,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline9,0,$xoff+$xline9,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline10,0,$xoff+$xline10,$height,$linecolor);
	imageline($imagehandle,$xoff+$xline11,0,$xoff+$xline11,$height,$linecolor);

}

////////////////////////////
// COLOR MAPPING FUNCTION //
////////////////////////////
function guess_color($colormap,$colorname,$colors) {
	$returnname = 'black';
	// check if existing color 
	if (isset($colors[$colorname])) {
		$returnname = $colorname;
	}
	// check if colorname is present in colormap (color exchanger per track)
	if (isset($colormap[$colorname])) {
		$returnname = $colormap[$colorname];
	}
	// not present, try some presets. 
	else {
		if (is_numeric($colorname)) { // numeric guessing : optimal for CN (all raw logR < 1 == red)
			if ($colorname >=4 ) {
				$returnname = 'blue';
			}
			elseif ($colorname >= 2.8) {
				$returnname = 'blue';
			}
			elseif ($colorname >= 1.8 ) {
				$returnname = 'orange';
			}
			elseif ($colorname >= 0.75) {
				$returnname = 'red';
			}
			else {
				$returnname = 'red';
			}
		}
		else {
			// try regex
			if (preg_match("/gain/i",$colorname) || preg_match("/gan/i",$colorname) || preg_match("/dup/i",$colorname) || preg_match("/tri/i",$colorname)) {
				$returnname = 'blue';
			}
			elseif (preg_match("/los/i",$colorname) || preg_match("/del/i",$colorname)) {
				$returnname = 'red';
			}
			elseif (preg_match("/loh/i",$colorname)) {
				$returnname = 'orange';
			}
		}
	}
	// we have a colorname. check for existence
	if (isset($colors[$returnname])) {
		return $returnname;
	}
	else {
		return 'black';
	}
}
////////////////////////////
// ALERT SETTING FUNCTION //
////////////////////////////
function choose_color($alerting, $row,$colorname) {
	$return = array($colorname,0,FALSE);
	// loop alerts set them in order. 
	foreach ($alerting->children() as $key => $val ) {
		// get values from alert
		$skey = $key;
		settype($skey,'string');
		$trigger = $alerting->{$key}->{'trigger'};
		$triggers = explode(',',$trigger); // only used for limit == equal
		$limit = $alerting->{$key}->{'limit'};
		$type = $alerting->{$key}->{'type'};
		$exclusive = $alerting->{$key}->{'exclusive'};
		//check if value is available in row
		if (!array_key_exists($skey,$row)) {
			continue;
		}
		// set missing to zero
		if ($row[$skey] == '') $row[$skey] = 0;
		// check if treshold is met.
		if ($limit == 'higher' && $row[$skey] >= $trigger) {
			if ($type == 'setcolor') {
				$return[0] = $alerting->{$key}->{'color'};
			}
			elseif ($type == 'lighter') {
				$return[0] = 'light'.$colorname;
			}
			elseif ($type == 'box') {
				$return[1] = 1;
			}
			elseif ($type == 'lightbox') {
				$return[1] = 2;
			}
			if ($type == 'hatching') {
				$return[2] = TRUE;
			}
			else {
				$return[2] = FALSE;
			}
			if ($exclusive == 1) {
				
				return ($return);
			}

		}
		elseif ($limit == 'lower' && $row[$skey] <= $trigger) {
			if ($type == 'setcolor') {
				$return[0] = $alerting->{$key}->{'color'};
			}
			elseif ($type == 'lighter') {
				$return[0] = 'light'.$colorname;
			}
			elseif ($type == 'box') {
				$return[1] = 1;
			}
			elseif ($type == 'lightbox') {
				$return[1] = 2;
			}
			if ($type == 'hatching') {
				$return[2] = TRUE;
			}
			else {
				$return[2] = FALSE;
			}
			if ($exclusive == 1) {
				return ($return);
			}

		}
		elseif ($limit == 'equal' && in_array($row[$skey], $triggers)) {
			if ($type == 'setcolor') {
				$return[0] = $alerting->{$key}->{'color'};
			}
			elseif ($type == 'lighter') {
				$return[0] = 'light'.$colorname;
			}
			elseif ($type == 'box') {
				$return[1] = 1;
			}
			elseif ($type == 'lightbox') {
				$return[1] = 2;
				$return[0] = 'light'.$colorname;
			}
			if ($type == 'hatching') {
				$return[2] = TRUE;
			}
			else {
				$return[2] = FALSE;
			}
			if ($exclusive == 1) {
				return ($return);
			}

		}
		
	}
	return ($return);
 
}
///////////////////////////////////
// PRINT THE MAP OF BROWSER PLOT //
///////////////////////////////////
// packing = full
function print_map_full($onHover,$data, $y) {
	//global vars
	global $scalef, $start, $stop, $xoff,$trackname,$userid,$chr,$nrsubgroups,$filter;
	// local variables
	$prevss = '';
	$prevgrouping = '';
	$laststart = -1;
	$laststop = -1;
	$lastid = -1;
	$ystart = $y -2;
	$yend = $y + 4;
	$nrrows = 0;
	// loop data
	foreach ($data as $key => $row) {
		// check for new line => grouping =~ samples for example
		if (array_key_exists('grouping',$row)) {
			
			// new AND NOT FIRST group, print the last item and go to next line
			if ($row['grouping'] != $prevgrouping && $prevgrouping != '') {
				// print previous item to map
				if ($laststart < $start) $laststart = $start;
				if ($laststop > $stop) $laststop = $stop;
				// hard negative filters need to skip.
				$skip = 0;
				foreach ($filter["1"] as $fid => $array) {
					// check if it is applied to this filter.
					if (in_array("$trackname", $filter["1"][$fid]['at'])) {
						// loop $region_ids	
						foreach ($filter["1"][$fid]['regions'] as $rid => $rarray) {
							$fstart = $rarray['start'];
							$fstop = $rarray['stop'];
							// adjust stop to start of filter
							if ($fstart >= $laststart && $fstart <= $laststop && $fstop >= $laststop) {
								$laststop = $fstart;
								
							}
							// adjust start to filter end
							if ($fstop >= $laststart && $fstop <= $laststop && $fstart <= $laststart) {
								$laststart = $fstop;
							}
							// split region on spanning filtered region
							// TODO
							// completely skip regions inside filtered region.
							if ($fstart <= $laststart && $fstop >= $laststop) {
								$skip = 1;
								continue 2;
							}
							
						}
					}
				}

				//	
				$scaledstart = intval(round(($laststart-$start)*$scalef));
				$scaledstop = intval(round(($laststop-$start)*$scalef));
				$xstart = $xoff + $scaledstart;
				$xend = $xoff + $scaledstop;
				if ($xend - $xstart < 5) {
					$xend = $xend + 1;
					$xstart = $xstart - 1;
				}
				if ($skip == 0) {
				  switch($onHover) {
					case 'Title' :
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$lasttitle' href='index.php'>\n";
						break;
					case 'DefaultTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');DefaultTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;track=$trackname&amp;id=$lastid','$userid','full',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;
					case 'CustomTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;id=$lastid','$trackname','$userid','full',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;

				  }
				}
				$laststop = -1;
				$laststart = -1;
				$lastid = -1;
				// goto next line
				$nrrows++; 
				$y +=10;
				$yend = $y + 4;
				$ystart = $y-2;
			}
			// set prevgrouping to current grouping
			$prevgrouping = $row['grouping'];
		}
		// no grouping variable present => each item on own line!
		else {
			if ($prevgrouping != '') {  // set to 1 after first item, prevents skipping line on first item.
				// print previous item to map 
				if ($laststart < $start) $laststart = $start;
				if ($laststop > $stop) $laststop = $stop;
				// hard negative filters need to skip.
				$skip = 0;
				foreach ($filter["1"] as $fid => $array) {
					// check if it is applied to this filter.
					if (in_array("$trackname", $filter["1"][$fid]['at'])) {
						// loop $region_ids	
						foreach ($filter["1"][$fid]['regions'] as $rid => $rarray) {
							$fstart = $rarray['start'];
							$fstop = $rarray['stop'];
							// adjust stop to start of filter
							if ($fstart >= $laststart && $fstart <= $laststop && $fstop >= $laststop) {
								$laststop = $fstart;
								
							}
							// adjust start to filter end
							if ($fstop >= $laststart && $fstop <= $laststop && $fstart <= $laststart) {
								$laststart = $fstop;
							}
							// split region on spanning filtered region
							// TODO
							// completely skip regions inside filtered region.
							if ($fstart <= $laststart && $fstop >= $laststop) {
								$skip = 1;
								continue 2;
							}
							
						}
					}
				}

				$scaledstart = intval(round(($laststart-$start)*$scalef));
				$scaledstop = intval(round(($laststop-$start)*$scalef));
				$xstart = $xoff + $scaledstart;
				$xend = $xoff + $scaledstop;
				if ($xend - $xstart < 5) {
					$xend = $xend + 1;
					$xstart = $xstart - 1;
				}
				if ($skip == 0) {
				  switch($onHover) {
					case 'Title' :
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$lasttitle' href='index.php'>\n";
						break;
					case 'DefaultTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');DefaultTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;track=$trackname&amp;id=$lastid','$userid','full',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;
					case 'CustomTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;id=$lastid','$trackname','$userid','full',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;


				  }
				}
				$laststop = -1;
				$laststart = -1;
				$lastid = -1;
				// goto next line
				$nrrows++;
				$y+=10;
				$yend = $y+4;
				$ystart = $y-2;
			}
			$prevgrouping = 1;
		}
		// leave space for the subset title
		if ( array_key_exists('subset', $row) && $row['subset'] != "$prevss") {
			$y+=10;
			$yend = $y+4;
			$ystart = $y-2;
			//$prevss = $row['subset'];
			// increase height accordingly.
			#if ($prevss != '') {
				$nrsubgroups++;
			#}
			$prevss = $row['subset'];
			 
		}
		// process values 
		if ($row['start'] >= $laststop ) { 
			if ($laststop > -1) {   // only happens with grouping.
				// print map
				if ($laststart < $start) $laststart = $start;
				if ($laststop > $stop) $laststop = $stop;
				// hard negative filters need to skip.
				$skip = 0;
				foreach ($filter["1"] as $fid => $array) {
					// check if it is applied to this filter.
					if (in_array("$trackname", $filter["1"][$fid]['at'])) {
						// loop $region_ids	
						foreach ($filter["1"][$fid]['regions'] as $rid => $rarray) {
							$fstart = $rarray['start'];
							$fstop = $rarray['stop'];
							// adjust stop to start of filter
							if ($fstart >= $laststart && $fstart <= $laststop && $fstop >= $laststop) {
								$laststop = $fstart;
								
							}
							// adjust start to filter end
							if ($fstop >= $laststart && $fstop <= $laststop && $fstart <= $laststart) {
								$laststart = $fstop;
							}
							// split region on spanning filtered region
							// TODO
							// completely skip regions inside filtered region.
							if ($fstart <= $laststart && $fstop >= $laststop) {
								$skip = 1;
								continue 2;
							}
							
						}
					}
				}

				$scaledstart = intval(round(($laststart-$start)*$scalef));
				$scaledstop = intval(round(($laststop-$start)*$scalef));
				$xstart = $xoff + $scaledstart;
				$xend = $xoff + $scaledstop;
				if ($xend - $xstart < 5) {
					$xend = $xend + 1;
					$xstart = $xstart - 1;
				}
				if ($skip == 0) {
				  switch($onHover) {
					case 'Title' :
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$lasttitle' href='index.php'>\n";
						break;
					case 'DefaultTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');DefaultTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;track=$trackname&amp;id=$lastid','$userid','full',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;
					case 'CustomTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;id=$lastid','$trackname','$userid','full',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;
				  }
				}

			}
			// set last* variables
			$laststart = $row['start'];
			$laststop = $row['stop'];
			$lasttitle = '';
			if (isset($row['Title'])) {
				$lasttitle = $row['Title'];
			}
			if (isset($row['ItemID'])) {
				$lastid = $row['ItemID'];
			}
		}
		elseif ($row['stop'] > $laststop) {    // extend map region : only happens with grouping. 
			$laststop = $row['stop'];
			if (isset($row['ItemID'])) {
				$lastid = $row['ItemID'];
			}

		}
	}	
	// print last datarow
	if (count($data)>0) {
		if ($laststart < $start) $laststart = $start;
		if ($laststop > $stop) $laststop = $stop;
		// hard negative filters need to skip.
		$skip = 0;
		foreach ($filter["1"] as $fid => $array) {
			// check if it is applied to this filter.
			if (in_array("$trackname", $filter["1"][$fid]['at'])) {
				// loop $region_ids	
				foreach ($filter["1"][$fid]['regions'] as $rid => $rarray) {
					$fstart = $rarray['start'];
					$fstop = $rarray['stop'];
					// adjust stop to start of filter
					if ($fstart >= $laststart && $fstart <= $laststop && $fstop >= $laststop) {
						$laststop = $fstart;
						
					}
					// adjust start to filter end
					if ($fstop >= $laststart && $fstop <= $laststop && $fstart <= $laststart) {
						$laststart = $fstop;
					}
					// split region on spanning filtered region
					// TODO
					// completely skip regions inside filtered region.
					if ($fstart <= $laststart && $fstop >= $laststop) {
						$skip = 1;
						continue 2;
					}
					
				}
			}
		}

		$scaledstart = intval(round(($laststart-$start)*$scalef));
		$scaledstop = intval(round(($laststop-$start)*$scalef));
		$xstart = $xoff + $scaledstart;
		$xend = $xoff + $scaledstop;
		if ($xend - $xstart < 5) {
			$xend = $xend + 1;
			$xstart = $xstart - 1;
		}
		$nrrows++;
		if ($skip == 0) {
		  switch($onHover) {
			case 'Title' :
				echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$lasttitle' href='index.php'>\n";
				break;
			case 'DefaultTT' :
				$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');DefaultTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;track=$trackname&amp;id=$lastid','$userid','full',event)\" ";
				echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
				break;
			case 'CustomTT' :
				$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;id=$lastid','$trackname','$userid','full',event)\" ";
				echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
				break;
		  }
		}
	}
	return $nrrows;
}

// packing = pack
function print_map_pack($onHover,$data, $y) {
	// global vars
	global $scalef, $start, $stop, $xoff,$trackname,$chr,$userid;
	// local variables
	$laststart = -1;
	$laststop = -1;
	$lastid = -1;
	$ystart = $y + 3;
	$yend = $y + 9;
	// loop data
	foreach ($data as $key => $row) {
		if ($row['start'] >= $laststop ) {
			
			if ($laststop > -1) {
				// print map
				if ($laststart < $start) $laststart = $start;
				if ($laststop > $stop) $laststop = $stop;
				// hard negative filters need to skip.
				$skip = 0;
				if (isset($filter["1"])) {
				   foreach ($filter["1"] as $fid => $array) {
					// check if it is applied to this filter.
					if (in_array("$trackname", $filter["1"][$fid]['at'])) {
						// loop $region_ids	
						foreach ($filter["1"][$fid]['regions'] as $rid => $rarray) {
							$fstart = $rarray['start'];
							$fstop = $rarray['stop'];
							// adjust stop to start of filter
							if ($fstart >= $laststart && $fstart <= $laststop && $fstop >= $laststop) {
								$laststop = $fstart;
								
							}
							// adjust start to filter end
							if ($fstop >= $laststart && $fstop <= $laststop && $fstart <= $laststart) {
								$laststart = $fstop;
							}
							// split region on spanning filtered region
							// TODO
							// completely skip regions inside filtered region.
							if ($fstart <= $laststart && $fstop >= $laststop) {
								$skip = 1;
								continue 2;
							}
							
						}
					}
				   }
				}

				$scaledstart = intval(round(($laststart-$start)*$scalef));
				$scaledstop = intval(round(($laststop-$start)*$scalef));
				$xstart = $xoff + $scaledstart;
				$xend = $xoff + $scaledstop;
				if ($xend - $xstart < 5) {
					$xend = $xend + 1;
					$xstart = $xstart - 1;
				}
				if ($skip == 0) {
			  	  switch($onHover) {
					case 'Title' :
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$lasttitle' href='index.php'>\n";
						break;
					case 'DefaultTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');DefaultTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;track=$trackname','$userid','pack',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;
					case 'CustomTT' :
						$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;id=$lastid','$trackname','$userid','pack',event)\" ";
						echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
						break;
				  }
				}

			}
			// set last* variables
			$laststart = $row['start'];
			$laststop = $row['stop'];
			$lasttitle = $row['Title'];
			if (isset($row['ItemID'])) {
				$lastid = $row['ItemID'];
			}

		}
		elseif ($row['stop'] > $laststop) {
			$laststop = $row['stop'];
			if (isset($row['ItemID'])) {
				$lastid = $row['ItemID'];
			}

		}
	}	
	// print last datarow
	if ($laststart < $start) $laststart = $start;
	if ($laststop > $stop) $laststop = $stop;
	// hard negative filters need to skip.
	$skip = 0;
	if (isset($filter["1"])) {	
	  foreach ($filter["1"] as $fid => $array) {
		// check if it is applied to this filter.
		if (in_array("$trackname", $filter["1"][$fid]['at'])) {
			// loop $region_ids
			foreach ($filter["1"][$fid]['regions'] as $rid => $rarray) {
				$fstart = $rarray['start'];
				$fstop = $rarray['stop'];
				// adjust stop to start of filter
				if ($fstart >= $laststart && $fstart <= $laststop && $fstop >= $laststop) {
					$laststop = $fstart;
				
				}
				// adjust start to filter end
				if ($fstop >= $laststart && $fstop <= $laststop && $fstart <= $laststart) {
					$laststart = $fstop;
				}
				// split region on spanning filtered region
				// TODO
				// completely skip regions inside filtered region.
				if ($fstart <= $laststart && $fstop >= $laststop) {
					$skip = 1;
					continue 2;
				}
			}
		}
	  }
	}

	$scaledstart = intval(round(($laststart-$start)*$scalef));
	$scaledstop = intval(round(($laststop-$start)*$scalef));
	$xstart = $xoff + $scaledstart;
	$xend = $xoff + $scaledstop;
	if ($xend - $xstart < 5) {
		$xend = $xend + 1;
		$xstart = $xstart - 1;
	}
	if ($skip == 0) {
	  switch($onHover) {
		case 'Title' :
			echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' title='$lasttitle' href='index.php'>\n";
			break;
		case 'DefaultTT' :
			$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');DefaultTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;track=$trackname','$userid','pack',event)\" ";
			echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
			break;
		case 'CustomTT' :
			$events = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$laststart&amp;stop=$laststop&amp;id=$lastid','$trackname','$userid','pack',event)\" ";
			echo "<area shape='rect' coords='$xstart,$ystart,$xend,$yend' $events>";
			break;
	  }
	}
	return $y;
}
?>
