<?php
//Tell the browser what kind of file is coming in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$font = 'arial'; 

#######################
# CONNECT TO DATABASE #
#######################
include("../.LoadCredentials.php");
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");

###################
# GET POSTED VARS #
###################
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
if (isset($_GET['p'])) {
	$project = $_GET['p'];
}
else {
	$project = '';
}
$start = $_GET['start'];
$stop = $_GET['stop'];
$userid = $_GET['u'];
$level = $_GET['l'];
$plotwidth = $_GET['pw'] ;
$xoff = $_GET['xoff'];
if (isset($_GET['cstm'])) {
	$custom = $_GET['cstm'] ;
}
else {
	$custom = '';
}

##############################
# CHECK WHAT TO SHOW IN PLOT #
##############################
$prefq = mysql_query("SELECT LOHshow, LOHminsize, LOHminsnp, condense, condense_classes, condense_null, ecarucaSize,annotations,tracks,DGVinclude FROM usersettings WHERE id = '$userid'");
$prefrow = mysql_fetch_array($prefq);
$LOHshow = $prefrow['LOHshow'];
$LOHminsize = $prefrow['LOHminsize'];
$LOHminsnp = $prefrow['LOHminsnp'];
$Condense = $prefrow['condense'];
$ToCondense = $prefrow['condense_classes'];
$NullCondense = $prefrow['condense_null'];
$ecarucaSize = $prefrow['ecarucaSize'];
$annotations = $prefrow['annotations'];
$showtracks = explode(',',$prefrow['tracks']);
$DGVinc = $prefrow['DGVinclude'];

// LOAD FUNCTIONS //
require('functions.php');

###################################
# DEFINE SOME VARS for IMAGE SIZE #
###################################
$prevgroup = '';
$window = $stop-$start+1;
$scalef = ($plotwidth-$xoff-1)/($window);
$width = $plotwidth; // shorthand
$fullheight = 0; // Image height, incremented for each track

##############################
# Create the image resources #
##############################
$subimages = array(); // contains the image handles
$positions = array(); // offsets of subimages in final canvas
$heigths = array(); // heights of subimages, needed for imagecopy
$hidden = array(); // list hidden tracks

//////////////////
// LOAD FILTERS //
//////////////////
$exp = array("IlluminaProjectSamples","CustomProjectSamples","IlluminaSearchSamples","CustomSearchSamples","Healthy","Carriers");
$query = mysql_query("SELECT uf.fid, uf.Type, uf.ApplyTo,fr.start, fr.stop FROM `Users_x_Filters` uf JOIN `Filters_x_Regions` fr ON fr.fid = uf.fid WHERE uf.uid = '$userid' AND uf.Type != '0' AND fr.chr = '$chr' AND ((fr.start BETWEEN $start AND $stop) OR (fr.stop BETWEEN $start AND $stop) OR (fr.start <= $start AND fr.stop >= $stop)) ORDER BY Type ASC");
$filter = array("1" => array(), "2" => array(), "3" => array());
while ($row = mysql_fetch_array($query)) {
	// adjust size to viewed region
	if ($row['start'] < $start) {
		$row['start'] = $start;
	}
	if ($row['stop'] > $stop) {
		$row['stop'] = $stop;
	}
	// create fid entry in filter hash
	if (!isset($filter[$row['Type']][$row['fid']])) {
		$filter[$row['Type']][$row['fid']] = array();
	}
	// create applyTo entry in filter hash for fid
	if (!isset($filter[$row['Type']][$row['fid']]['at'])) {
		if ($row['ApplyTo'] == 'Experimental') {
			$at = $exp;
		}
		else {
			$at = explode(";",$row['ApplyTo']);
		}
		$filter[$row['Type']][$row['fid']]['at'] = $at;
	}
	// create regions array for fid 
	if (!isset($filter[$row['Type']][$row['fid']]['regions'])) {
		$filter[$row['Type']][$row['fid']]['regions'] = array();
	}
	$filter[$row['Type']][$row['fid']]['regions'][$row['start']] = $row['stop'];
	
}	
///////////////
// SET SCALE //
/////////////// 
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
	}
else {
	$wintresh = 8000000;
	$scale = 500000;
	$stext = "500";
	while ($window < $wintresh) {
		$scale = $scale / 2;
		$stext = $stext / 2;
		$wintresh = $wintresh / 2;
	}
	$stext = number_format($stext,1,'.','') . " Kb";
} 

///////////////////////////////
// Create Subimage for scale //
///////////////////////////////
$prevgroup = 'Scale';
$prevpacking = '';
$positions['Scale'] = $fullheight;
$subimages['Scale'] = imagecreatetruecolor($width, 25);
$heights['Scale'] = 25;
$fullheight += 25;
$colors = array();
$colors = LoadColors($subimages['Scale'],'Main_Colors.xml');
imagefill($subimages['Scale'],0,0,$colors['white']);
//settings link
$fontwidth = imagefontwidth(2);
$pref = 'Preferences';
$txtwidth = strlen($pref)*$fontwidth;
$txtx = $width - $txtwidth - 3;
imagestring($subimages['Scale'],2,$txtx,1,$pref,$colors['mediumgray']);
//scale
$scaledscale = intval(round($scale*$scalef));
imagerectangle($subimages['Scale'], 0, 0, 60, 20, $colors['black']);
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($stext)*$fontwidth;
$txtx = 30 -($txtwidth/2);
imagestring($subimages['Scale'],3,$txtx,1,$stext,$colors['black']);
$xstart = 30-($scaledscale/2);
$xstop = 30+($scaledscale/2);
imageline($subimages['Scale'],$xstart,18,$xstart,14,$colors['black']);
imageline($subimages['Scale'],$xstop,18,$xstop,14,$colors['black']);
imageline($subimages['Scale'],$xstart,16,$xstop,16,$colors['black']);

///////////////////
// Render tracks //
///////////////////

foreach($showtracks as $key => $trackname) {
	// skip if empty trackname
	if ($trackname == '') continue;
	if (!file_exists("Tracks/$trackname.xml")) continue;
	// load track configuration
	$Array = simplexml_load_file("Tracks/$trackname.xml");	
	// check for 'HideOn' Variables
	if (isset($Array->{'plot'}->{'HideOn'})) {
		foreach ($Array->{'plot'}->{'HideOn'}->children() as $ckey => $cval) {
			settype($ckey,'string');
			if (${$ckey} == $cval) {
				$hidden[$trackname] = 1;
				continue 2; // goto next track
			}
		}
	}

	// check if custom code is specified
	$customcode = $Array->{'plot'}->{'CustomCode'};
	// get default packing mode
	$packing = $Array->{'packing'};
	// get data type (gene / bar)
	$type = $Array->{'type'};
	// get the color mapping array
	$colormap = array();
	if(isset($Array->{'plot'}->{'colormap'})) {
		$colormapxml = $Array->{'plot'}->{'colormap'};
		foreach ($colormapxml->children() as $ckey => $cval ) {
			settype($ckey,'string');
			settype($cval,'string');
			$colormap[$ckey] = $cval; 
		}
	}
	// check track group
	if ($Array->{'plot'}->{'group'} != '' && $Array->{'plot'}->{'group'} != "$prevgroup") {
		$prevgroup = $Array->{'plot'}->{'group'};
		$plotruler = 1;
	}
	else {
		$plotruler = 0;
	}
	// load custom code if specified
	if ($customcode == 1) {
		include("CustomCodes/Plot/inc_$trackname.inc");
	}
	// otherwise, default parsing
	else {
		// 1. get settings
		$displayname = $Array->{'plot'}->{'displayname'};
		settype($displayname,'string');
		$source = $Array->{'plot'}->{'source'};
		$alerting = array();
		if (isset($Array->{'plot'}->{'Alerting'})) {
			$alerting = $Array->{'plot'}->{'Alerting'};
		}
		// 2. Get the data into an array
		$data = array();
		if ($source == 'database') {
			$nrfiltered = 0;
			$querystring = '';
			// check for packing-dependent queries
			$queries = $Array->xpath('plot/database/query');
			if (count($queries) > 1) { // multiple queries, search packing-match
				$lastquery = '';
				foreach ($queries as $querynode) {
					$lastquery = $querynode;
					if ($querynode->attributes()->{'packing'} == "$packing") {
						$querystring = $querynode;
						break;
					}		
				}
				// pick last query if no packing match.
				if ($querystring == '') {
					$querystring = $lastquery;
				}
			}
			else { // only a single query, use this.
				$querystring = $Array->{'plot'}->{'database'}->{'query'};
			}
			$querystring = str_replace('%start%', $start, $querystring);
			$querystring = str_replace('%stop%', $stop, $querystring);
			$querystring = str_replace('%chr%', $chr, $querystring);
			$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
			$querystring = str_replace('%project%', $project, $querystring);
			$querystring = str_replace('%userid%',$userid,$querystring);
			$querystring = str_replace('%condensepart%','',$querystring);
			$querystring = str_replace('%ecarucaSize%',$ecarucaSize,$querystring);
			$query = mysql_query($querystring);
			// keep track of number of subsets (subtitles) if present in query
			$filteredfromsid = array();
			$datafromsids = array();
			if (strstr($querystring,'subset')) {
				$prevss = '';
				$subsets = 0;
				while ($row = mysql_fetch_array($query)) {
					## skip filtered LOH regions .
					if (isset($row['gender']) && $chr >= 23 && ($row['gender'] == 'Male' || $row['gender'] == '')){
						// DO NOT FILTER LOH ON X/Y for MALE
					}
					elseif (isset($Array->{'Filters'}->{'LOH'}) && $row['color'] == 'color2') {
						## filter if lohshow == 0
						if ($LOHshow == 0 ) {
							$filteredfromsid[$row['grouping']] = 1;
							continue;
						}
						## filter if too small
						if ($LOHminsize > ($row['stop'] - $row['start'] + 1)) {
							$filteredfromsid[$row['grouping']] = 1;
							continue;
						}
						## filter if not enough snps
						if ($LOHminsnp > $row['nrsnps']) {
							$filteredfromsid[$row['grouping']] = 1;
							continue;
						}
					}
					if ($row['subset'] != $prevss) {
						$subsets++;
					}
					$prevss = $row['subset'];
					$data[] = $row;
					$datafromsids[$row['grouping']] = 1;
				}
			}
			else {
				$subsets = 0;
				while ($row = mysql_fetch_array($query)) {
					## skip filtered LOH regions .
					if (isset($row['gender']) && $chr >= 23 && ($row['gender'] == 'Male' || $row['gender'] == '')) {
						// DO NOT SKIP CN2 on MALE X/Y
					}
					elseif (isset($Array->{'Filters'}->{'LOH'}) && $row['color'] == 'color2') {
						## filter if lohshow == 0
						if ($LOHshow == 0 ) {
							$filteredfromsid[$row['grouping']] = 1;
							continue;
						}
						## filter if too small
						if ($LOHminsize > ($row['stop'] - $row['start'] + 1)) {
							$filteredfromsid[$row['grouping']] = 1;
							continue;
						}
						## filter if not enough snps
						if ($LOHminsnp > $row['nrsnps']) {
							$filteredfromsid[$row['grouping']] = 1;
							continue;
						}
					}
					$data[] = $row;
                    //trigger_error(json_encode($row));
					if (isset($row['grouping'])) {
						$datafromsids[$row['grouping']] = 1;
					}
				}
			}
		}
		//////////////////////////////////
		// 3. Create and prepare canvas //
		//////////////////////////////////
		// filtered height = 10*(nrsamples from which all cnvs were filtered)
		$nrfiltered = 0;
		foreach($filteredfromsid as $sid => $dummy) {
			if (!array_key_exists($sid,$datafromsids)) {
				## no data left for this sid => $nrfiltered++;
				$nrfiltered++;
			}
		}
		// calculate height
		if ($packing == 'pack') {
			if ($prevpacking == 'full') {
				$tmpheight = 17;
			}
			else {
				$tmpheight = 14;
			}
		}
		elseif ($packing == 'full') {
			if (isset($Array->{'plot'}->{'database'}->{'countquery'})){
				$queries = $Array->xpath('plot/database/countquery');
				if (count($queries) > 1) { // multiple queries, search packing-match
					$lastquery = '';
					foreach ($queries as $querynode) {
						$lastquery = $querynode;
						if ($querynode->attributes()->{'packing'} == "$packing") {
							$querystring = $querynode;
							break;
						}		
					}
					// pick last query if no packing match.
					if ($querystring == '') {
						$querystring = $lastquery;
					}
				}
				else { // only a single query, use this.
					$querystring = $Array->{'plot'}->{'database'}->{'countquery'};
				}

				//$querystring = $Array->{'plot'}->{'database'}->{'countquery'};
				$querystring = str_replace('%start%', $start, $querystring);
				$querystring = str_replace('%stop%', $stop, $querystring);
				$querystring = str_replace('%chr%', $chr, $querystring);
				$querystring = str_replace('%chrtxt%', $chrtxt, $querystring);
				$querystring = str_replace('%project%', $project, $querystring);
				$querystring = str_replace('%userid%',$userid,$querystring);
				$querystring = str_replace('%ecarucaSize%',$ecarucaSize,$querystring);
				$query = mysql_query($querystring);
				$tmpheight = 17 + 10*mysql_num_rows($query) - 10*$nrfiltered;
			}
			else {
				$tmpheight = 17 + 10*count($data);# - 10*$nrfiltered;
			}
		}
		if ($plotruler == 1) {
			$fulloffset = 5;
			$tmpheight += 12;
		}
		else {
			$fulloffset = 0;
			#$tmpheight += 3; 
		}
		// add height for subsets titles
		$tmpheight = $tmpheight + $subsets*10;
		// create canvas
		$subimages[$trackname] = imagecreatetruecolor($width,$tmpheight);
		// load colors
		$colors = array();
		$colors = LoadColors($subimages[$trackname],'Main_Colors.xml');
		imagefill($subimages[$trackname],0,0,$colors['white']);
		
		// draw grid
 		DrawGrid($subimages[$trackname],$xoff,$tmpheight,$start,$stop,$scalef,$colors['titanwhite']); 
		//  store track height
		$positions[$trackname] = $fullheight;
		$fullheight += $tmpheight;
		$heights[$trackname] = $tmpheight;
		$y = 0;
		// DRAW THIN BARS FOR FULL-PACKED TRACKS 
		if ($packing == 'full') {
			// top only if previous packing != full
			if ($prevpacking != 'full') {
				imageline($subimages[$trackname],$xoff,$fulloffset,$width,$fulloffset,$colors['lightgray']);
			}
			imageline($subimages[$trackname],$xoff,$tmpheight-1,$width,$tmpheight-1,$colors['lightgray']);
		}
		///////////////////////////////////////
		// 4. Plot seperating rule if needed //
		///////////////////////////////////////
		//imagestring($subimages[$trackname],1,$xoff+5,$y+7,$querystring,$colors['black']);
		if ($plotruler == 1) {
			imagestring($subimages[$trackname],3,0,$y-2,$prevgroup,$colors['black']);
			imagefilledrectangle($subimages[$trackname],$xoff,$y+5,$plotwidth,$y+6,$colors['mediumgray']);
			$y += 12;
		}
		//////////////////////
		// 5. Plot the data //
		//////////////////////
		// OPTION ONE : PACK => ALL ON ONE LINE
		if ($packing == 'pack') {
			if ($prevpacking == 'full') {
				$y = $y+3;
			}
			if (strlen($displayname) > 19) {
				$displayname = '..'.substr($displayname, (strlen($displayname)-18));
			}
			$fontwidth = imagefontwidth(2);
			$txtwidth = strlen($displayname)*$fontwidth;
			$txtx = $xoff-$txtwidth-3;
			imagestring($subimages[$trackname],2,$txtx,$y-2,$displayname,$colors['black']);
			foreach ($data as $key => $row) {
				if ($type == 'gene') {
					// position
					$cstart = $row['start'];
					$cstop = $row['stop'];
					$cstrand = $row['strand'];
					// get color from database
					if (isset($row['color'])) {
						$ccolorname = guess_color($colormap,$row['color'],$colors);
					}
					else {
						$ccolorname = 'black';
					}
					// define alerts to apply
					$tmp = choose_color($alerting, $row,$ccolorname);
					$ccolorname = $tmp[0];
					$drawbox = $tmp[1];
					settype($ccolorname,'string');
					$ccolor = $colors[$ccolorname];
					// get positions of 'exons'
					$cexstart = $row['exonstarts'];
					$cexstarts = explode(',',$cexstart);
					$cexend = $row['exonends'];
					$cexends = explode(',',$cexend);
					// correct if extending beyond plot
					if ($cstart < $start) $cstart = $start;
					if ($cstop > $stop) $cstop = $stop;
					// scale coordinates
					$scaledstart = intval(round(($cstart-$start) * $scalef));
					$scaledstop = intval(round(($cstop-$start) * $scalef));
					// draw general boundary line
					imageline($subimages[$trackname],$xoff+$scaledstart,$y+5,$xoff+$scaledstop,$y+5,$ccolor);
					// add arrows based on strand
					if ($cstrand == '+' || $cstrand == 1) {
						$hooktip = $xoff + $scaledstart + 5;
						while ($hooktip <= $scaledstop + $xoff -1) {
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+3,$ccolor);
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+7,$ccolor);
							$hooktip = $hooktip +5;
						}
					}
					elseif ($cstrand == '-' || $cstrand == '-1')  {
						$hooktip = $xoff + $scaledstart +2;
						while ($hooktip <= $scaledstop + $xoff - 5) {
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+3,$ccolor);
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+7,$ccolor);
							$hooktip = $hooktip +5;
						}
					}
					foreach($cexstarts as $key => $cexs) {
						$cexe = $cexends[$key];
						if ($cexe < $start) continue;  // skip if end < plot-start
						if ($cexs > $stop ) continue; // skip if start > plot-start
						if ($cexe > $stop) $cexe = $stop;
						if ($cexs < $start) $cexs = $start;
						$cexscaledstart = intval(round(($cexs-$start) * $scalef));
						$cexscaledstop = intval(round(($cexe-$start) * $scalef));
						if ($cexscaledstop-$cexscaledstart < 1){
							$cexscaledstop = $cexscaledstart +1;
						} 
						imagefilledrectangle($subimages[$trackname],$xoff+$cexscaledstart,$y+2,$xoff+$cexscaledstop,$y+8,$ccolor);
					}
	
			
				} 
				elseif ($type == 'bar') {
					// position
					$cstart = $row['start'];
					$cstop = $row['stop'];
					// get color from database
					if (isset($row['color'])) {
						$ccolorname = guess_color($colormap,$row['color'],$colors);
					}
					else {
						$ccolorname = 'black';
					}

					// define alerts to apply
					$tmp = choose_color($alerting, $row,$ccolorname);
					$ccolorname = $tmp[0];
					$drawbox = $tmp[1];
					settype($ccolorname,'string');
					$ccolor = $colors[$ccolorname];
					// correct if extending beyond plot
					if ($cstart < $start) $cstart = $start;
					if ($cstop > $stop) $cstop = $stop;
					// scale coordinates
					$scaledstart = intval(round(($cstart-$start) * $scalef));
					$scaledstop = intval(round(($cstop-$start) * $scalef));
					// plot region rectangle
					imagefilledrectangle($subimages[$trackname],$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+7,$ccolor);
					//draw box
					if ($drawbox == 1) {
						imagerectangle($subimages[$trackname],$xoff+$scaledstart-2,$y,$xoff+$scaledstop+2,$y+9,$colors['black']);
					}
					elseif ($drawbox == 2) {
						imagerectangle($subimages[$trackname],$xoff+$scaledstart-2,$y,$xoff+$scaledstop+2,$y+9,$colors['lightpink']);
					}
					// draw hooks if specified in xml, strand is specified and bar is sufficiently long
					if (defined($Array->{'map'}->{'tooltip'}->{'hooks'}) && array_key_exists($row, 'strand') && $scaledstop - $scaledstart > 7){
						$cstrand = $row['strand'];
						$ccolor = $colors[$Array->{'map'}->{'tooltip'}->{'hooks'}];
						if ($cstrand == '+' || $cstrand == 1) {
							$hooktip = $xoff + $scaledstart + 5;
							while ($hooktip <= $scaledstop + $xoff -1) {
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+3,$ccolor);
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+7,$ccolor);
								$hooktip = $hooktip +5;
							}
						}
						elseif ($cstrand == '-' || $cstrand == '-1')  {
							$hooktip = $xoff + $scaledstart +2;
							while ($hooktip <= $scaledstop + $xoff - 5) {
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+3,$ccolor);
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+7,$ccolor);
								$hooktip = $hooktip +5;
							}
						}

					}	


				}// end type
			} // end foreach data
		} 
		// OPTION TWO: FULL: each item on its own line
		elseif ($packing == 'full')  {
			$prevgrouping = '';  // grouping : eg sample id
			$prevlabel = '';     // label: eg sample name
			$prevss = '';	     // subset : group of samples based on some criterium in query
		 	// print tracklabel
			$fontwidth = imagefontwidth(2);
			$txtwidth = strlen($displayname)*$fontwidth;
			$txtx = $xoff + ($width-$xoff)/2 - $txtwidth/2;
			if ($plotruler == 1) {
				imagestring($subimages[$trackname],2,$txtx,$y-6,$displayname,$colors['black']);
				$y+=7;
			}
			else {
				imagestring($subimages[$trackname],2,$txtx,$y-1,$displayname,$colors['black']);
				$y+=12;
			}
			foreach ($data as $key => $row) {
				// print label. If grouping variable is present, use this.
				if ( array_key_exists('grouping', $row) ) {
					if ($row['grouping'] != $prevgrouping) {
						//new item, print name
						if (strlen($prevlabel) > 19) {
							$printlabel = '..'.substr($prevlabel, (strlen($prevlabel)-18));
						}
						else {
							$printlabel = $prevlabel;
						}
						$fontwidth = imagefontwidth(2);
						$txtwidth = strlen($printlabel)*$fontwidth;
						$txtx = $xoff-$txtwidth-3;
						imagestring($subimages[$trackname],2,$txtx,$y-2,$printlabel,$colors['black']);
						$prevlabel = $row['grouplabel'];
						// increase $y if second group
						if ($prevgrouping != '') {
							$y +=10;
						}
					}
					// set prevgrouping to current grouping
					$prevgrouping = $row['grouping'];
				}
				// no grouping variable available, use itemname
				else {
					if (strlen($row['ItemName']) > 19) {
						$row['ItemName'] = '..' . substr($row['ItemName'], (strlen($row['ItemName'])-18));
					}
					$fontwidth = imagefontwidth(2);
					$txtwidth = strlen($row['ItemName'])*$fontwidth;
					$txtx = $xoff-$txtwidth-3;
					// do not skip line for first entry, set prevgrouping to anything not zero
					if ($prevgrouping != '') {
						$y+=10;
					}
					$prevgrouping = 1;
					// print gene name
					imagestring($subimages[$trackname],2,$txtx,$y-2,$row['ItemName'],$colors['black']);
				}
				// print subset title if present
				if ( array_key_exists('subset', $row) && $row['subset'] != $prevss) {
					$fontwidth = imagefontwidth(1);
					$txtwidth = strlen($row['subset'])*$fontwidth;
					$txtx = $xoff + ($width-$xoff)/2 - $txtwidth/2;
					imagestring($subimages[$trackname],1,$txtx,$y,$row['subset'],$colors['black']);
					$y+=10;
					$prevss = $row['subset'];
				}

				// distinction for plotting genes and bars
				if ($type == 'gene') {
					// position
					$cstart = $row['start'];
					$cstop = $row['stop'];
					$cstrand = $row['strand'];
					// get color from database
					if (isset($row['color'])) {
						$ccolorname = guess_color($colormap,$row['color'],$colors);
					}
					else {
						$ccolorname = 'black';
					}
					// define alerts to apply
					$tmp = choose_color($alerting, $row,$ccolorname);
					$ccolorname = $tmp[0];
					$drawbox = $tmp[1];
					settype($ccolorname,'string');
					$ccolor = $colors[$ccolorname];
					// get positions of 'exons'
					$cexstart = $row['exonstarts'];
					$cexstarts = explode(',',$cexstart);
					$cexend = $row['exonends'];
					$cexends = explode(',',$cexend);
					// correct if extending beyond plot
					if ($cstart < $start) $cstart = $start;
					if ($cstop > $stop) $cstop = $stop;
					// scale coordinates
					$scaledstart = intval(round(($cstart-$start) * $scalef));
					$scaledstop = intval(round(($cstop-$start) * $scalef));
					// draw general boundary line
					imageline($subimages[$trackname],$xoff+$scaledstart,$y+5,$xoff+$scaledstop,$y+5,$ccolor);
					// add arrows based on strand
					if ($cstrand == '+' || $cstrand == 1) {
						$hooktip = $xoff + $scaledstart + 5;
						while ($hooktip <= $scaledstop + $xoff -1) {
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+3,$ccolor);
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+7,$ccolor);
							$hooktip = $hooktip +5;
						}
					}
					elseif ($cstrand == '-' || $cstrand == '-1')  {
						$hooktip = $xoff + $scaledstart +2;
						while ($hooktip <= $scaledstop + $xoff - 5) {
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+3,$ccolor);
							imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+7,$ccolor);
							$hooktip = $hooktip +5;
						}
					}
					foreach($cexstarts as $key => $cexs) {
						$cexe = $cexends[$key];
						if ($cexe < $start) continue;  // skip if end < plot-start
						if ($cexs > $stop ) continue; // skip if start > plot-end
						if ($cexs < $start ) $cexs = $start;
						if ($cexe > $stop ) $cexe = $stop;
						$cexscaledstart = intval(round(($cexs-$start) * $scalef));
						$cexscaledstop = intval(round(($cexe-$start) * $scalef));
						if ($cexscaledstop-$cexscaledstart < 1){
							$cexscaledstop = $cexscaledstart +1;
						}
						imagefilledrectangle($subimages[$trackname],$xoff+$cexscaledstart,$y+2,$xoff+$cexscaledstop,$y+8,$ccolor);
						if ($cexscaledstop - $cexscaledstart > 10 ) {
							if ($cstrand == '+' || $cstrand == 1) {
								$hooktip = $xoff + $scaledstart + 5;
								while ($hooktip <= $scaledstop + $xoff -1) {
									imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+3,$colors['white']);
									imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+7,$colors['white']);
									$hooktip = $hooktip +5;
								}
							}
							elseif ($cstrand == '-' || $cstrand == '-1')  {
								$hooktip = $xoff + $scaledstart +2;
								while ($hooktip <= $scaledstop + $xoff - 5) {
									imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+3,$colors['white']);
									imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+7,$colors['white']);
									$hooktip = $hooktip +5;
								}
							}

 						}
					}
	
				}
				elseif ($type == 'bar') {
					// position
					$cstart = $row['start'];
					$cstop = $row['stop'];
					// get color from database
					if (isset($row['color'])) {
						$ccolorname = guess_color($colormap,$row['color'],$colors);
					}
					else {
						$ccolorname = 'black';
					}
					// define alerts to apply
					$tmp = choose_color($alerting, $row,$ccolorname);
					$ccolorname = $tmp[0];
					$drawbox = $tmp[1];
					$hatching = $tmp[2];
					settype($ccolorname,'string');
					$ccolor = $colors[$ccolorname];
					// correct if extending beyond plot
					if ($cstart < $start) $cstart = $start;
					if ($cstop > $stop) $cstop = $stop;
					// scale coordinates
					$scaledstart = intval(round(($cstart-$start) * $scalef));
					$scaledstop = intval(round(($cstop-$start) * $scalef));
					// plot region rectangle
					
					if ($hatching == TRUE) {
						$hatching_color = 12;
						$hatching_white = 4;
						$hatching_interval = $hatching_color + $hatching_white;
						for ($i = 0; $i <= floor(($scaledstop-$scaledstart) / $hatching_interval); $i++){
							$interval_start = $xoff + $scaledstart + ($i * $hatching_interval);
							$interval_stop = $xoff + $scaledstart + (($i+1) * $hatching_interval) -1;
							$cnv_stop = $xoff + $scaledstop;
							// hatching interval fully in CNV?
							if ($interval_stop < $cnv_stop) {
								imagerectangle($subimages[$trackname], $interval_start, $y+2, $interval_start + $hatching_white , $y+7, $ccolor);
								imagefilledrectangle($subimages[$trackname], $interval_start + $hatching_white + 1, $y+2, $interval_stop, $y+7, $ccolor);
							}
							else {
								$finalsegment_length = $cnv_stop - $interval_start;
								if ($finalsegment_length <= $hatching_white) {
									imagerectangle($subimages[$trackname], $interval_start, $y+2, $cnv_stop , $y+7, $ccolor);
								}
								else {
									imagerectangle($subimages[$trackname], $interval_start, $y+2, $interval_start + $hatching_white , $y+7, $ccolor);
									imagefilledrectangle($subimages[$trackname], $interval_start + $hatching_white + 1, $y+2, $cnv_stop, $y+7, $ccolor);

								}
							}
									
						}

					}
					else {
						imagefilledrectangle($subimages[$trackname],$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+7,$ccolor);
					}
						

					//draw box
					if ($drawbox == 1) {
						imagerectangle($subimages[$trackname],$xoff+$scaledstart-2,$y,$xoff+$scaledstop+2,$y+9,$colors['black']);
					}
					elseif ($drawbox == 2) {
						imagerectangle($subimages[$trackname],$xoff+$scaledstart-2,$y,$xoff+$scaledstop+2,$y+9,$colors['red']);
					}
					// check for error bars
					if (substr($ccolorname,0,5) != 'light') {
						$errorcolor = 'light'.$ccolorname;
					}
					else {
						$errorcolor = $ccolorname;
					}
					if (isset($row['errorlow']) && $cstart > $start && $row['errorlow'] != 0 && $row['errorlow'] != '') {
						if ($row['errorlow'] < $start) {
							$elow = $start;
						}
						else {
							$elow = $row['errorlow'];
						}
						$scaledlow = intval(round(($elow - $start)*$scalef));
						# horizontal
						imagefilledrectangle($subimages[$trackname],$xoff+$scaledlow,$y+5,$xoff+$scaledstart-1,$y+5,$colors[$errorcolor]);
						# vertical
						if ($row['errorlow'] >= $start) {
							imageline($subimages[$trackname],$xoff+$scaledlow,$y+2,$xoff+$scaledlow,$y+8,$colors[$errorcolor]);
						}
					}
					if (isset($row['errorhigh']) && $cstop < $stop && $row['errorhigh'] != 0 && $row['errorhigh'] != '') {
						if ($row['errorhigh'] > $stop) {
							$ehigh = $stop;
						}
						else {
							$ehigh = $row['errorhigh'];
						}
						$scaledhigh = intval(round(($ehigh - $start)*$scalef));
						# horizontal
						imagefilledrectangle($subimages[$trackname],$xoff+$scaledhigh,$y+5,$xoff+$scaledstop+1,$y+5,$colors[$errorcolor]);
						# vertical
						if ($row['errorhigh'] <= $stop) {
							imageline($subimages[$trackname],$xoff+$scaledhigh,$y+2,$xoff+$scaledhigh,$y+8,$colors[$errorcolor]);
						}
					}
					// draw hooks if specified in xml, strand is specified and bar is sufficiently long
					if (isset($Array->{'barhooks'}) && array_key_exists('strand', $row) && $scaledstop - $scaledstart > 7){
						imagestring($subimages[$trackname],2,100,100,'hoera',$colors['black']);
						$cstrand = $row['strand'];
						$ccolor = $Array->{'map'}->{'tooltip'}->{'hooks'};
						settype($ccolor,'string');
						$ccolor = $colors[$ccolor];
						if ($cstrand == '+' || $cstrand == 1) {
							$hooktip = $xoff + $scaledstart + 5;
							while ($hooktip <= $scaledstop + $xoff -1) {
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+3,$ccolor);
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip-2,$y+7,$ccolor);
								$hooktip = $hooktip +5;
							}
						}
						elseif ($cstrand == '-' || $cstrand == '-1')  {
							$hooktip = $xoff + $scaledstart +2;
							while ($hooktip <= $scaledstop + $xoff - 5) {
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+3,$ccolor);
								imageline($subimages[$trackname],$hooktip,$y+5,$hooktip+2,$y+7,$ccolor);
								$hooktip = $hooktip +5;
							}
						}

					}	



				}	
			}
			// last label
			$fontwidth = imagefontwidth(2);
			$txtwidth = strlen($prevlabel)*$fontwidth;
			$txtx = $xoff-$txtwidth-3;
			imagestring($subimages[$trackname],2,$txtx,$y-2,$prevlabel,$colors['black']);
			$prevlabel = '';
			if (isset($row['grouplabel'])) {
				$prevlabel = $row['grouplabel'];
			}
			
		}// end packing

	} // end default rendering
	$prevpacking = $packing;
	// apply filter !
	$theight = $heights[$trackname];
	$fcolors = array('3' => $colors['alphared'], "2" => $colors['alphagray'], "1" => $colors['mediumgray']);
	for ($ftype = 1; $ftype <= 3; $ftype++) {
		// plot full height rectangle, but leave space on top for title.
		$theight = $heights[$trackname];
		foreach ($filter["$ftype"] as $fid => $array) {
			// check if it is applied to this filter.
			if (in_array("$trackname", $filter["$ftype"][$fid]['at'])) {
				foreach ($filter["$ftype"][$fid]['regions'] as $fstart => $fstop) {
					$fscaledstart = intval(round(($fstart-$start) * $scalef));
					$fscaledstop = intval(round(($fstop-$start) * $scalef));
					imagefilledrectangle($subimages[$trackname],$xoff+$fscaledstart,(0+$fulloffset),$xoff+$fscaledstop,$theight,$fcolors[$ftype]);
				}
			}
		}
	}	
}
	

////////////////////////
// CREATE FINAL IMAGE //
////////////////////////
// create canvas
$image = ImageCreate($width,$fullheight);
$colors = array();
$colors = LoadColors($image,'Main_Colors.xml');
imagefill($image,0,0,$colors['white']);
// put scale
imagecopy($image,$subimages['Scale'],0,0,0,0,$width,$heights['Scale']);
// put other tracks
foreach($showtracks as $key => $trackname) {
	// skip empty
	if ($trackname == '') continue;
	// skip undefined subimages
	if (!array_key_exists($trackname, $subimages)) continue;
	// add to canvas
	imagecopy($image,$subimages[$trackname],0,$positions[$trackname],0,0,$width, $heights[$trackname]);
}

// enable antialias
if (function_exists('imageantialias')) {
	imageantialias($image,true);
}

//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image);
foreach($showtracks as $key => $trackname) {
	if ($trackname == '') continue;
	if (!array_key_exists($trackname, $subimages)) continue;
	ImageDestroy($subimages[$trackname]);
}



 
?>

