<?php
//////////////////////////////////////////////////////////////////
// OUTPUT FROM THIS SCRIPT IS SEND TO JAVASCRIPT AJAX           //
//   => DO NOT USE ' or " but only escaped \" in output strings //
//////////////////////////////////////////////////////////////////


// start session && connect to db if needed 
$sessionid = session_id();
if (empty($sessionid)) {
        session_start();
}
if (file_exists('../.LoadCredentials.php')) {
	require('../.LoadCredentials.php');
}
elseif (file_exists('.LoadCredentials.php')) {
	require('.LoadCredentials.php');
}
else {
	echo "<h3>ERROR: Could not load credentials</h3>";
	exit;
}
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];

// get subtype => contains action to take / taken
if (isset($_GET['subtype'])) {
	$subtype = $_GET['subtype'];
}
//if (isset($_POST['set'])) {
//	echo "hello";
//	exit;
//	$subtype = $_POST['set'];
//}
// Show tracklist for current user
if ($subtype == 'PickTracks') {
	$query = mysql_query("SELECT tracks FROM usersettings WHERE id = '$userid'");
	$row = mysql_fetch_array($query);
	// output = table with checkboxes and the submit button
	$output = "<h3>Select Displayed Annotation Tracks</h3><p><br/></p>";	 
	$output .= "<p><table cellspacing=0>";
	$output .= "<tr><th>Show</th><th>Track Name</th><th>Track Description</th></tr>";
	$tracks = explode(',',$row['tracks']);
	// load possible tracks xml
	if (file_exists('Browser/Main_Configuration.xml')) {
		$Array = simplexml_load_file("Browser/Main_Configuration.xml");
	}
	elseif (file_exists('Main_Configuration.xml')) {
		$Array = simplexml_load_file("Main_Configuration.xml");
	}
	else {
		echo "ERROR: Main_Configuration.xml was not found<br/>";
		exit;
	}
	foreach ($Array->{'tracks'}->children() as $ckey => $cval) {
		if ($Array->{'tracks'}->{$ckey}->{'enabled'} != 1) {
			continue;
		}
		$checked = '';
		if (in_array($ckey,$tracks)) {
			$checked = 'CHECKED';
		}
		$label = $Array->{'tracks'}->{$ckey}->{'description'};
		$output .= "<tr><td><input type=\"checkbox\" name=\"tracks[]\" value=\"$ckey\" $checked /></td><td>$ckey</td><td>$label</td></tr>";	
	}
	$output .= "<tr><td class=last colspan=3>&nbsp;&nbsp;</td></tr></table></p>";
	$output .= "<p><input type=submit class=button value=\"Update Tracks\" name=\"set\"> &nbsp; <input type=button class=button value=\"Cancel\"  onclick=\"document.getElementById('infodiv').style.display='none'\"></p>";
	echo $output;
	

}
// tracks were updated.
elseif ($subtype == 'Update Tracks') {
	// set content track list & selection
	$tracklist = '';
	$Array = simplexml_load_file("Browser/Main_Configuration.xml");
	foreach ($Array->{'tracks'}->children() as $ckey => $cval) {
		if ($Array->{'tracks'}->{$ckey}->{'enabled'} != 1) {
			continue;
		}
		if (in_array($ckey,$_POST['tracks'])) {
			$tracklist .= "$ckey,";
		}
	}
	if ($tracklist != '') {
		$tracklist = substr($tracklist,0,-1);
	}
	mysql_query("UPDATE usersettings SET tracks = '$tracklist' WHERE id='$userid'");	
}
// pick links to public databases
elseif ($subtype == 'PickDBlinks') {
	$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$userid'");
	$row = mysql_fetch_array($query);
	// output = table with checkboxes and the submit button
	$output = "<h3>Select Displayed Links To Public Databases</h3><p><br/></p>";
	// 1. gene based	 
	$output .= "<p><table cellspacing=0>";
	$output .= "<tr><th colspan=3 style='font-style:italic;font-size:1.3em'>Gene based resources</th></tr>";
	$output .= "<tr><th>Show</th><th>Name</th><th>Database Description</th></tr>";
	$db_links = explode(',',$row['db_links']);
	// load possible links from database
	$query = mysql_query("SELECT id, Resource, Description FROM db_links WHERE type = 1 ORDER BY Resource");
	while ($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['Resource'];
		$descr = $row['Description'];
		$checked = '';
		if (in_array($id,$db_links)) {
			$checked = 'CHECKED';
		}
		$output .= "<tr>\n";
		$output .= " <td ><input type=checkbox name='dblinks[]' value='$id' $checked></td>";
		$output .= " <td NOWRAP>$name</td>";
		$output .= " <td >$descr</td>\n";
		$output .= "</tr>\n";
	}
	$output .= "<tr><td class=last colspan=3>&nbsp;&nbsp;</td></tr>";
	// 2. Region based	 
	$output .= "<tr><th colspan=3 style='font-style:italic;padding-top:1em;font-size:1.3em'>Region based resources</th></tr>";
	$output .= "<tr><th>Show</th><th>Name</th><th>Database Description</th></tr>";
	// load possible links from database
	$query = mysql_query("SELECT id, Resource, Description FROM db_links WHERE type = 2 ORDER BY Resource");
	while ($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['Resource'];
		$descr = $row['Description'];
		$checked = '';
		if (in_array($id,$db_links)) {
			$checked = 'CHECKED';
		}
		$output .= "<tr>\n";
		$output .= " <td ><input type=checkbox name='dblinks[]' value='$id' $checked ></td>";
		$output .= " <td NOWRAP>$name</td>";
		$output .= " <td >$descr</td>\n";
		$output .= "</tr>\n";
	}
	$output .= "<tr><td class=last colspan=3>&nbsp;&nbsp;</td></tr>";
	$output .= "</table></p>";
	$output .= "<p><input type=submit class=button value=\"Update Database Links\" name=\"set\"> &nbsp; <input type=button class=button value=\"Cancel\"  onclick=\"document.getElementById('infodiv').style.display='none'\"></p>";
	echo $output;

}
// database links were updated
elseif ($subtype == 'Update Database Links') {
	// selected links
	$links = $_POST['dblinks'];
	// implode to , seperated list
	$linklist = '';
	foreach ($links as $lkey => $lval) {
		$linklist .= "$lval,";
	}
	if ($linklist != '') {
		$linklist = substr($linklist,0,-1);
	}
	mysql_query("UPDATE usersettings SET db_links = '$linklist' WHERE id='$userid'");
	$_SESSION['db_links'] = $linklist;	
}
// pick DGV studies
elseif ($subtype == 'PickDGVstudies') {
	$output = "<h3>Select Studies included in the DGV Track</h3><p><br/></p>";
	$output .= "<p>The Toronto Database of Genomic Variants containts CNVs detected in healthy individuals. These data are obtained from a large collection of studies, listed below. Special care should be taken in selecting the included studies, as there is a large bias in the data towards the HapMap samples.</p>";
	// get user settings
	$query = mysql_query("SELECT DGVinclude FROM usersettings WHERE id = '$userid'");
	$row = mysql_fetch_array($query);
	$dgvinclude = explode(',',$row['DGVinclude']);	
	// output studiels	 
	$output .= "<p><table cellspacing=0>";
	$query = mysql_query("SELECT id, pubid, paper, method, samples, gain, loss, inversion FROM DGV_studies ORDER BY paper");
	$output .= "<tr>";
	$output .= "<th >USE</th>\n";
	$output .= "<th >Study</th>\n";
	$output .= "<th >Method</th>\n";
	$output .= "<th >Samples</th>\n";
	$output .= "<th >Entries</th>\n";
	$output .= "</tr>";
	while ($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$pubid = $row['pubid'];
		$paper = $row['paper'];
		$method = $row['method'];
		$samples = $row['samples'];
		$gain = $row['gain'];
		$loss = $row['loss'];
		$inversion = $row['inversion'];
		$total = $gain + $loss + $inversion;
		$checked = '';
		if (in_array($id,$dgvinclude)) {
			$checked = 'CHECKED';
		}
		$output .= "<tr>\n";
		$output .= " <td ><input type=checkbox name='DGVinc[]' value='$id' $checked></td>";
		$output .= " <td NOWRAP><a href='http://www.ncbi.nlm.nih.gov/pubmed/$pubid' target='_blank' style='color:white'>$paper</a></td>";
		$output .= " <td>$method</td>";
		$output .= " <td>$samples</td>";
		$output .= " <td>$total</td>\n";
		$output .= "</tr>\n";
	}	
	$output .= "<tr><td class=last colspan=5>&nbsp;&nbsp;</td></tr>";
	$output .= "</table></p>";
	$output .= "<p><input type=submit class=button value=\"Update Included DGV Studies\" name=\"set\"> &nbsp; <input type=button class=button value=\"Cancel\"  onclick=\"document.getElementById('infodiv').style.display='none'\"></p>";

	echo $output;
}
// DGV studies were updated
elseif ($subtype == 'Update Included DGV Studies') {
	// selected links
	$studies = $_POST['DGVinc'];
	// implode to , seperated list
	$dgvlist = '';
	foreach ($studies as $skey => $sval) {
		$dgvlist .= "$sval,";
	}
	if ($dgvlist != '') {
		$dgvlist = substr($dgvlist,0,-1);
	}
	mysql_query("UPDATE usersettings SET DGVinclude = '$dgvlist' WHERE id='$userid'");
	$_SESSION['DGVinclude'] = $dgvlist;
	

}
elseif ($subtype == 'PickRes') {
	$checks = array("0" => "", "1" => "CHECKED", '' => '');
	$LOHshow = $_SESSION['LOHshow'];
	$LOHminsize = $_SESSION['LOHminsize'];
	$LOHminsnps = $_SESSION['LOHminsnp'];

	/////////////
	// RESULTS //
	/////////////
	echo "<h3>Change CNV Filtering</h3>\n";
	echo "<p>Specify how CNV results should be organised in the graphical overviews. Excluding regions by filtering here also influences tabular overiews and created reports.</p><p>\n";
	echo "<table cellspacing=0>\n";
	echo "<tr>\n";
	echo " <th colspan=5 >LOH-regions</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td ><input type=checkbox name='showloh' ".$checks[$LOHshow]."></td>";
	echo " <td NOWRAP>Show LOH Regions (CN2 regions on X for Males are still shown)</td>";
	echo " <td colspan=3>Show Regions of decreased heterozygosity rate. These regions are called by VanillaICE</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td ><input type=text size=8 name=minsizeloh value='$LOHminsize'></td>";
	echo " <td NOWRAP>Minimal LOH Size</td>";
	echo " <td>Specify the minimal LOH region size to be reported (in basepairs)</td>";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td ><input type=text size=8 name=minsnpsloh value='$LOHminsnps'></td>";
	echo " <td NOWRAP>Min. nrSNPS in LOH</td>";
	echo " <td>Specify the minial number of probes a LOH region should contain to be reported</td>";
	echo "</tr>";
	/*
	echo "<tr>\n";
	echo "<td ><input type=checkbox name=condense ".$checks[$Condense] ."></td>\n";
	echo "<td NOWRAP>Collapse CNVs</td>\n";
	echo "<td>Collapse CNVs from the diagnostic classes specified below into a single line per copy number state.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	if ($ToCondense == 0) {
		$ToCondense = '';
	}
	echo "<td ><input type=text name=tocondense size=8 value='$ToCondense'></td>\n";
	echo "<td NOWRAP>Classes to collapse</td>\n";
	echo "<td>The diagnostic classes specified here will be bundled into a condensed track. Seperate by comma.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell><input type=checkbox name=nullcondense ". $checks[$NullCondense]."></td>\n";
	echo "<td NOWRAP>Collapse NA class</td>\n";
	echo "<td>Include CNVs without a specified diagnostic class in the bundled condensed track.</td>\n";
	echo "</tr>\n";
	*/

	echo "</table></p>";
	echo "<p><input type=submit class=button value=\"Update CNV Filtering\" name=\"set\"> &nbsp; <input type=button class=button value=\"Cancel\"  onclick=\"document.getElementById('infodiv').style.display='none'\"></p>";



}
elseif ($subtype == "Update CNV Filtering") {

	$showloh = $_POST['showloh'] || 0;
	$minsizeloh = $_POST['minsizeloh'] ;
	$minsnpsloh = $_POST['minsnpsloh'] ;
	//$Condense = $_POST['condense'] || 0;
	//$ToCondense = $_POST['tocondense'];
	//if ($ToCondense == '') {
	//	$ToCondense = 0;
	//}
	//$NullCondense = $_POST['nullcondense'] || 0;
	$_SESSION['LOHshow'] = $showloh;
	$_SESSION['LOHminsize'] = $minsizeloh;
	$_SESSION['LOHminsnp'] = $minsnpsloh;
	//$_SESSION['Condense'] = $Condense;
	//$_SESSION['ToCondense'] = $ToCondense;
	//$_SESSION['NullCondense'] = $NullCondense;
	//mysql_query("UPDATE usersettings SET LOHshow = '$showloh', LOHminsize = '$minsizeloh', LOHminsnp = '$minsnpsloh', condense = '$Condense', condense_classes = '$ToCondense', condense_null = '$NullCondense' WHERE id = '$uid'");
	mysql_query("UPDATE usersettings SET LOHshow = '$showloh', LOHminsize = '$minsizeloh', LOHminsnp = '$minsnpsloh' WHERE id = '$uid'");
}
elseif ($subtype == 'ann') {
	$RefSeq = $_POST['RefSeq'] || 0;
	$GCcode = $_POST['GCcode'] || 0;
	$GCncode = $_POST['GCncode'] || 0;
	$SegDup = $_POST['SegDup'] || 0;
	$DECsyn = $_POST['DECsyn'] || 0;
	$DECpat = $_POST['DECpat'] || 0;
	$ecaruca = $_POST['ecaruca'] || 0;
	$ecarucaSize = $_POST['ecarucaSize'];
	$annotations = $_POST['userAnn'];
	if ($RefSeq == '') {
		$RefSeq = 0;
	}
	if ($GCcode == '') {
		$GCcode = 0;
	}
	if ($GCncode == '') {
		$GCncode = 0;
	}
	if ($SegDup == '') {
		$SegDup = 0;
	}
	if ($DECsyn == '') {
		$DECsyn = 1;
	}
	if ($DECpat == '') {
		$DECpat = 1;
	}
	if ($ecaruca == '') {
		$ecaruca = 1;
	}
	if ($ecarucaSize == '') {
		$ecarucasize = 30000000;
	}
	$includestring = '';
	foreach ($annotations as $key => $val) {
		$includestring .= $val . ',';
	}
	if ($includestring != '') {
		$includestring = substr($includestring, 0,-1);
	}
	// set session vars
	$_SESSION['RefSeq'] = $RefSeq;
	$_SESSION['GCcode'] = $GCcode;
	$_SESSION['GCncode'] = $GCncode;
	$_SESSION['SegDup'] = $SegDup;
	$_SESSION['DECsyn'] = $DECsyn;
	$_SESSION['DECpat'] = $DECpat;
	$_SESSION['ecaruca'] = $ecaruca;
	$_SESSION['ecarucaSize'] = $ecarucaSize;
	$_SESSION['annotations'] = $includestring;
	// update table
	mysql_query("UPDATE usersettings SET RefSeq = '$RefSeq', GCcode = '$GCcode', GCncode = '$GCncode',SegDup = '$SegDup',DECsyn = '$DECsyn', DECpat = '$DECpat', ecaruca = '$ecaruca', ecarucaSize = '$ecarucaSize' , annotations = '$includestring' WHERE id = '$uid'");
}
elseif ($subtype == 'con') {
	$DGVshow = $_POST['DGV_show'] || 0;
	$DGVinc = $_POST['DGVinc'];
	$HapSame = $_POST['HapSame'] || 0;
	$HapAll = $_POST['HapAll'] || 0;
	if ($DGVshow == '') {
		$DGVshow = 0;
	}
	if ($HapSame == '') {
		$HapSame = 0;
	}
	if ($HapAll == '') {
		$HapAll = 0;
	}
	$_SESSION['DGVshow'] = $DGVshow;
	$_SESSION['HapSame'] = $HapSame;
	$_SESSION['HapAll'] = $HapAll;
	$includestring = '';
	sort($DGVinc);
	foreach ($DGVinc as $key => $val) {
		$includestring .= $val . ',';
	}
	if ($includestring != '') {
		$includestring = substr($includestring, 0,-1);
	}
	$_SESSION['DGVinclude'] = $includestring;
	mysql_query("UPDATE usersettings SET DGVshow = '$DGVshow', DGVinclude = '$includestring', HapSame = '$HapSame', HapAll = '$HapAll' WHERE id = '$uid'");

}
elseif ($subtype == 'publ') {
	$dblinks = $_POST['dblinks'];
	$string = '';
	foreach($dblinks as $dbid ) {
		$string .= "$dbid,";
	}
	$string = substr($string, 0,-1);
	$_SESSION['db_links'] = $string;
	mysql_query("UPDATE usersettings SET db_links = '$string' WHERE id = '$uid'");


}
// SET EMTPY VALUES TO ZERO 
// UPDATE SESSION VARS
// CREATE INCLUDE STRING 
// UPDATE DATABASE

//echo "<meta http-equiv='refresh' content='0;URL=index.php?page=settings&type=plots&st=$subtype&v=s'>\n";
?> 
