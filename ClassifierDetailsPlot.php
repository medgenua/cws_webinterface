<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$cid = $_GET['c'];
$aid = $_GET['a'];
$userid = $_SESSION['userID'];
$included = $_GET['i'];
// cnv details
$query = mysql_query("SELECT a.chr, a.start, a.stop, a.cn, s.id, s.chip_dnanr, s.gender FROM `aberration` a JOIN `sample` s ON a.sample = s.id WHERE a.id = '$aid'");
$row = mysql_fetch_array($query);
$chr = $row['chr'];
$start = $row['start'];
$cn = $row['cn'];
$chrtxt = $chromhash[ $chr ];
$sid = $row['id'];
$stop = $row['stop'];
$end = $stop;
$sname = $row['chip_dnanr'];
$sgender =$row['gender'];
// classifier details
$cq = mysql_query("SELECT c.Gender,c.CN,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter FROM `Classifier` c WHERE c.id = '$cid' ");
$details = mysql_fetch_array($cq);
$details['CN'] = explode(",",$details['CN']);
// reference samples
$cq = mysql_query("SELECT TrackOnly,IncludeParents,IncludeControls,MinSNP FROM `Users_x_Classifiers` WHERE uid = '$userid' AND cid = '$cid'");
$row = mysql_fetch_array($cq);
$details['TrackOnly'] = $row['TrackOnly'];
$details['IncludeParents'] = $row['IncludeParents'];
$details['IncludeControls'] = $row['IncludeControls'];
$detials['MinSNP'] = $row['MinSNP'];
$where = '';
if ($details['TrackOnly'] == 1) {
	$where .= " AND s.intrack = 1 AND a.idproj = s.trackfromproject";
}
if ($details['IncludeParents'] == 0) {
	$where .= " AND p.collection <> 'Parents'";
}
if ($details['IncludeControls'] == 0) {
	$where .= " AND p.collection <> 'Controls'";
}

// reference regions.
$refq = mysql_query("SELECT a.chr,a.start,a.stop,a.cn,a.class,s.gender,s.chip_dnanr FROM `aberration` a JOIN `sample` s JOIN `project` p JOIN `projectpermission` pp ON a.idproj = p.id AND a.sample = s.id AND a.idproj = pp.projectid WHERE pp.userid = '$userid' AND a.sample != '$sid' AND chr = '$chr' AND ( (start BETWEEN $start AND $end) OR (stop BETWEEN $start AND $end) OR (start <= $start AND stop >= $end)) $where");
$refcnvs = array();
while ($rrow = mysql_fetch_array($refq)) {
	// same cn needed?
	if ($details['scn'] == 1) {
		if ($details['scnd'] == 'exact' && $rrow['cn'] != $cn) {
			continue;
		}
		elseif ($details['scnd'] == 'type') {
			if ($rrow['cn'] >= 2 && $cn < 2) {
				continue;
			}
			if ($rrow['cn'] <= 2 && $cn > 2) {
				continue;
			}
			if ($rrow['cn'] == 2 && $cn != 2) {
				continue;
			}
		}
	}
	// same dc needed?
	if ($rrow['class'] == 2	) {
		$rrow['class'] = 1;
	}
	if ($details['sdc'] == 1 && $rrow['class'] != $details['dc']) {
		continue;
	}
	
	// size constraints.
	// bigger?
	if ($rrow['start'] < $start && $rrow['stop'] > $end && $details['sbigger'] != -1 && $details['sbigger'] < ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1)) {
		continue;
	}
	//smaller
	if ($rrow['start'] > $start && $rrow['stop'] < $end && $details['ssmaller'] >  ($end-$start+1)/ ($rrow['stop'] - $rrow['start']+1)) {
		continue;
	}
	// overlap/extend.
	$ovstart = max($start,$rrow['start']);
	$ovend = min($end,$rrow['stop']);
	// ratio inner
	
	if ($details['soinner'] > ($ovend-$ovstart+1)/($rrow['stop'] - $rrow['start'] + 1)) {
		continue;
	}
	// size extending 5'.
	if ($start < $rrow['start'] && $end <= $rrow['stop'] && ($rrow['start'] - $start + 1) > $details['soouter']) {
		continue;
	}
	//size extending 3'.
	if ($rrow['start'] <= $start && $rrow['stop'] < $end && ($end - $rrow['stop'] + 1) > $details['soouter']) {
		continue;
	}
	// passed all.
	$refcnvs[] = $rrow;

}



if ($included == 1) {
	$window = $stop-$start+100000;
}
else {
	$window = $stop-$start+10000;
}	
#$cn = $_GET['cn'];


# DEFINE IMAGE PROPERTIES
//$querystring = "SELECT COUNT(id) AS aantal FROM decipher_patients WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) GROUP BY caseid";

//$result = mysql_query($querystring);
//$nrlines = mysql_num_rows($result);
$nrlines = count($refcnvs);
$skipped = 0;
if ($nrlines > 50) {
	$skipped = $nrlines - 50;
	$refcnvs = array_slice($refcnvs,0,50);
	$nrlines = 52;
}
//$nrlines = 52;
$height = $nrlines*10 + 20;
	
//$height = 300;
//$height = 60 + $nrabs*10;
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
}
elseif ($window > 8000000 ) {
	$scale = 500000;
	$stext = "500 Kb";
}
else {
	$scale = 250000;
	$stext = "250 Kb";
} 
//Specify constant values
if ($included == 1) {
	$width = 800; //Image width in pixels
}
else {
	$width = 450; //Image width in pixels
}

// offset and scale.
$xoff = 155;
$scalef = ($width - $xoff -5)/($window);
# get centromere position
//$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
//$row = mysql_fetch_array($result);
//$lastp = $row['stop'];


//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$orange = ImageColorAllocate($image,255, 179,0);

$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue, '5' => $gpos50);
#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21


$csample = "";
$y = 5;

# Draw the CNV of interest 
imagefilledrectangle($image,1,1,$width-1,$y+9,$gpos25);
imagestring($image,2,10,1,$sname,$black);
imagestring($image,2,110,1,$sgender,$black);
if ($included == 1) {
	$pstart = $start - 50000;
	$pstop = $stop + 50000;
}
else {
	$pstart = $start - 5000;
	$pstop = $stop + 5000;
}
$scaledstart = intval(round(($start-$pstart) * $scalef));
$scaledstop = intval(round(($stop-$pstart) * $scalef));
imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+6,$cns[$cn]);


#draw position indications: 
//$formatstart = number_format($start-500,0,'',',');
//$formatstop = number_format($stop+500,0,'',',');
//imageline($image,$xoff,7,$xoff+4,4,$gpos75);
//imageline($image,$xoff,7,$xoff+4,10,$gpos75);
//imageline($image,$xoff,7,$xoff+10,7,$gpos75);
//imageline($image,$xstop,18,$xstop,14,$black);
//imageline($image,$xstart,16,$xstop,16,$black);
//imagestring($image,2,$xoff+14,1,$formatstart,$gpos75);


//$fontwidth = imagefontwidth(2);
//$txtwidth = strlen($formatstop)*$fontwidth;
//imageline($image, 390,7,380,7,$gpos75);
//imageline($image, 390,7,386,4,$gpos75);
//imageline($image, 390,7,386,10,$gpos75);
//imagestring($image,2,378-$txtwidth,2,$formatstop,$gpos75);


//while ($row = mysql_fetch_array($result)) {
foreach($refcnvs as $key => $row) {
	$y = $y + 10;
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$csample = $row['chip_dnanr'];
	$cgender = $row['gender'];
	$ccn = $row['cn']; 
	$a5 = 0;
	$a3 = 0;
	if ($cstart < $pstart) {
		$cstart = $pstart;
		$a5 = 1;
	}
	if ($cstop > $pstop) {
		$cstop = $pstop;
		$a3 = 1;
	}
	$scaledstart = intval(round(($cstart-$pstart) * $scalef));
	$scaledstop = intval(round(($cstop-$pstart) * $scalef));
	imagestring($image,1,10,$y+2,$csample,$gpos50);
	imagestring($image,1,110,$y+2,$cgender,$gpos50);

	imagefilledrectangle($image,$xoff+$scaledstart,$y+4,$xoff+$scaledstop,$y+6,$cns[$ccn]);
	if ($a5 == 1) {
		imageline($image,$xoff+$scaledstart-3,$y+5,$xoff+$scaledstart-1,$y+6,$black);
		imageline($image,$xoff+$scaledstart-3,$y+5,$xoff+$scaledstart-1,$y+4,$black);
	}
	if ($a3 == 1) {
		imageline($image,$xoff+$scaledstop+3,$y+5,$xoff+$scaledstop+1,$y+6,$black);
		imageline($image,$xoff+$scaledstop+3,$y+5,$xoff+$scaledstop+1,$y+4,$black);

	}
}

imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);
if ($skipped > 0) {
	imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
	imagefilledrectangle($image,$xoff-5,0,$xoff-5,$y+10,$gpos50);	
	imagefilledrectangle($image,$xoff-55,0,$xoff-55,$y+10,$gpos50);
	
	$y = $y+10;
	imagestring($image,1,10,$y+2,"$skipped more CNVs were not plotted.",$gpos50);
	imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
	imagefilledrectangle($image,$width-1,0,$width-1,$y+10,$gpos50);
	imagefilledrectangle($image,0,0,0,$y+10,$gpos50);
	
}
else {
	
	# Draw the table borders
	imagefilledrectangle($image,0,0,0,$y+10,$gpos50);
	imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
	imagefilledrectangle($image,$width-1,0,$width-1,$y+10,$gpos50);
	#imagefilledrectangle($image,47,0,47,$height-1,$gpos50);
	imagefilledrectangle($image,$xoff-5,0,$xoff-5,$y+10,$gpos50);
	imagefilledrectangle($image,$xoff-55,0,$xoff-55,$y+10,$gpos50);
}



		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

