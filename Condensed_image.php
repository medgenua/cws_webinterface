<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$userid = $_GET['u'];
#$chips = $_GET['chips'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$window = $stop-$start+1;
$cn = $_GET['cn'];
$NullCondense = $_GET['nc'];
$ToCondense = $_GET['tc'];

$NC = array('AND a.class IS NOT NULL','OR a.class IS NULL');
$condensepart = "AND (a.class IN ($ToCondense) ". $NC[$NullCondense] .")";


# DEFINE IMAGE PROPERTIES
$querystring = "SELECT COUNT(a.id) AS aantal FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s JOIN projectpermission pp ON pp.projectid = p.id AND a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND s.intrack = 1 AND s.trackfromproject = p.id AND a.cn = '$cn' $condensepart GROUP BY s.chip_dnanr, p.chiptype";

#$querystring = "SELECT COUNT(a.id) AS aantal FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id GROUP BY s.chip_dnanr, p.chiptype";
#$querystring = "SELECT COUNT(a.id) AS aantal FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE pp.userid = '$userid' GROUP BY s.chip_dnanr ";
$result = mysql_query($querystring);
$nrlines = mysql_num_rows($result);
	
$querystring = "SELECT COUNT(a.id) AS aantal FROM aberration a JOIN project p JOIN projsamp ps JOIN projectpermission pp JOIN sample s ON pp.projectid = p.id AND a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND s.intrack = 1 AND s.trackfromproject = p.id AND a.cn = '$cn' $condensepart GROUP BY s.chip_dnanr";
$result = mysql_query($querystring);
$nrsamples = mysql_num_rows($result);

$height = $nrlines*10 + $nrsamples*4 + 20;
#$height = 500;
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
}
else {
	$scale = 500000;
	$stext = "500 kb";
} 

$scalef = 210/($window);
#$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
#$row = mysql_fetch_array($result);
#$lastp = $row['stop'];


//Specify constant values
$width = 400; //Image width in pixels
//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$blue  = imageColorallocate($image,0,0,255); 
$darklightblue = ImageColorAllocate($image,122,189,255);
$lightblue = ImageColorAllocate($image,235,235,255);
$green = ImageColorAllocate($image,0,190,0);
$lightgreen = ImageColorAllocate($image,156,255,56);
$purple = ImageColorAllocate($image,136,34,135);
$lightpurple = ImageColorAllocate ($image, 208, 177, 236);
$orange = ImageColorAllocate($image,255, 179,0);
$lightorange = ImageColorAllocate($image,255,210,127);


$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
$cnslight = array('0' => $lightred, '1' => $lightred, '2' => $lightorange, '3' => $darklightblue, '4' => $darklightblue);

#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

$querystring = "SELECT a.start, a.stop, s.chip_dnanr, p.chiptype, a.class FROM aberration a JOIN projectpermission pp JOIN project p JOIN projsamp ps JOIN sample s ON pp.projectid = p.id AND a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND s.intrack = 1 AND s.trackfromproject = p.id AND a.cn = '$cn' $condensepart ORDER BY s.chip_dnanr, p.chiptype, a.start";

$result = mysql_query($querystring);
$nrrows = mysql_num_rows($result);

$csample = "";
$cct = "";
$y = 8;
$cy = $y;
$xoff = 180;
while ($row = mysql_fetch_array($result)) {
	$cclass = $row['class'];
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$chip_dnanr = $row['chip_dnanr'];
	$chiptype = $row['chiptype'];
	if ($chip_dnanr != $csample) {
		if ($csample != '') {
			imagestring($image,1,10,($cy+$y)/2-2,$csample,$black);
			imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
		}
		
		$csample = $chip_dnanr;
		$cy = $y+14;
		$y = $y +4;
		$cct = '';
	}
	
	if ($chiptype != $cct) {
		$y = $y+10;
		$chipstring = preg_replace('/(Human)(.*)/',"$2",$chiptype);		

		imagestring($image,1,120,$y-2,$chipstring,$black);
		$cct = $chiptype;
	}
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
		if ($cclass == 4) {
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+2,$cnslight[$cn]);
	}
	else {
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+2,$cns[$cn]);
	}
	if ($cclass == 1 || $cclass == 2) {
		imagerectangle($image,$xoff+$scaledstart-2,$y-2,$xoff+$scaledstop+2,$y+4,$black);
	}
}

imagestring($image,1,10,($cy+$y)/2-2,$csample,$black);
//if ($y != $cy) {
//	imagefilledrectangle($image,47,$cy-1,47,$y+7,$black);
//}

# Draw the table borders
imagestring($image,2,10,1,"Sample",$gpos75);
imagestring($image,2,120,1,"ChipType",$gpos75);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);
imagefilledrectangle($image,0,0,0,$height,$gpos50);
#imagefilledrectangle($image,0,5,$width-1,5,$gpos50);
imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,$height-1,$width-1,$height-1,$gpos50);
imagefilledrectangle($image,$width-1,0,$width-1,$height-1,$gpos50);
imagefilledrectangle($image,117,0,117,$height-1,$gpos50);
imagefilledrectangle($image,178,0,178,$height-1,$gpos50);
#imagefilledrectangle($image,0,85,$width-1,85,$gpos50);		

#draw position indications: 
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,180,7,184,4,$gpos75);
imageline($image,180,7,184,10,$gpos75);
imageline($image,180,7,190,7,$gpos75);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
imagestring($image,2,194,1,$formatstart,$gpos75);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
imageline($image, 390,7,380,7,$gpos75);
imageline($image, 390,7,386,4,$gpos75);
imageline($image, 390,7,386,10,$gpos75);
imagestring($image,2,378-$txtwidth,2,$formatstop,$gpos75);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

