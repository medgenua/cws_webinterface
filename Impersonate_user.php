<?php
///////////////////
// START SESSION //
///////////////////
#session_set_cookie_params(3600 * 24); // Session life set to 1 day
ini_set("session.cookie_lifetime",3600*24); // session life set to 1 day
session_start(); //Allows you to use sessions

//#######################
//# CONNECT to database #
//#######################
$ok = include('.LoadCredentials.php');
if ($ok != 1) {
	include('inc_installation_problem.inc');
	exit();
}
// SET DATABASE HERE. use same build as source session. 
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

// check if there is a user logged in of level >= 3
if ($_SESSION['level'] < 3) {
	echo "You are not allowed to Impersonate other users!";
	exit();
}

// user to impersonate
$newuid = $_POST['ImpersonateID'];
if ($newuid == '') {
	echo "No userid specified.";
	exit();
}

// get user details
$query = "SELECT level, FirstName, LastName, username,email,projectsrun, samplesrun FROM users WHERE id = '$newuid'";
$auth  = mysql_query("$query"); 
if (mysql_num_rows($auth) == 0) {
	echo "User ID does not match an existing user.<br/>";
	exit();
}
else {
	$_SESSION['logged_in'] = "true";
	$row = mysql_fetch_array($auth);
	$_SESSION['username'] = $row['username'];
	$_SESSION['email'] = stripslashes($row['email']); //Saves your username (email in this case).
	$_SESSION['level'] = $row['level'];
	$_SESSION['FirstName'] = $row['FirstName'];
	$_SESSION['LastName'] = $row['LastName'];
	$_SESSION['userID'] = $newuid;
	$_SESSION['projectsrun'] = $row['projectsrun'];
	$_SESSION['samplesrun'] = $row['samplesrun'];
$settingquery = mysql_query("SELECT DGVshow, DGVinclude, HapSame, HapAll, RefSeq, GCcode, GCncode,SegDup,LOHshow, LOHminsize, LOHminsnp, condense, condense_classes, condense_null, db_links, ecaruca,ecarucaSize,DECsyn,DECpat FROM usersettings WHERE id = '$newuid'");
	$settingrow = mysql_fetch_array($settingquery);
	$_SESSION['DGVshow'] = $settingrow['DGVshow'];
	$_SESSION['DGVinclude'] = $settingrow['DGVinclude'];
	$_SESSION['HapSame'] = $settingrow['HapSame'];
	$_SESSION['HapAll'] = $settingrow['HapAll'];
	$_SESSION['RefSeq'] = $settingrow['RefSeq'];
	$_SESSION['GCcode'] = $settingrow['GCcode'];
	$_SESSION['GCncode'] = $settingrow['GCncode'];
	$_SESSION['SegDup'] = $settingrow['SegDup'];		
	$_SESSION['LOHshow'] = $settingrow['LOHshow'];
	$_SESSION['LOHminsize'] = $settingrow['LOHminsize'];
	$_SESSION['LOHminsnp'] = $settingrow['LOHminsnp'];
	$_SESSION['Condense'] = $settingrow['condense'];
	$_SESSION['ToCondense'] = $settingrow['condense_classes'];
	$_SESSION['NullCondense'] = $settingrow['condense_null'];
	$_SESSION['db_links'] = $settingrow['db_links'];
	$_SESSION['ecaruca'] = $settingrow['ecaruca'];
	$_SESSION['ecarucaSize'] = $settingrow['ecarucaSize'];
	$_SESSION['DECsyn'] = $settingrow['DECsyn'];
	$_SESSION['DECpat'] = $settingrow['DECpat'];
	$_SESSION['freshlogin'] = 1;
	//$_SESSION['freshlogin'] = 1;
	// redirect to page_main.
	echo "<meta http-equiv='refresh' content='0;URL=index.php?page=main'>\n";	
}

?>

