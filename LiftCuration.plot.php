<?php

//Tell the browser what kind of file is come in
header("Content-Type: image/png");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

//$font = 'arial'; 

#####################
## GET posted data ##
#####################
$aid = $_GET['aid'];
$table = $_GET['t'];
$build = $_GET['b'];

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis$build";
mysql_select_db("$db");


##########################
## CNV DETAILS AND DATA ##
##########################
if ($table == 'aberration') {
	$datatable = 'datapoints';
	$query = mysql_query("SELECT ct.ID, a.cn, a.start, a.stop, a.chr, d.content, ct.name, a.avgLogR FROM aberration a JOIN chiptypes ct JOIN project p JOIN datapoints d JOIN projsamp ps ON a.idproj = ps.idproj AND p.chiptypeid = ct.ID AND a.sample = ps.idsamp AND ps.idproj = p.id AND a.id = d.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($query);
	$chr = $row['chr'];
	$cn = $row['cn'];
	$start = $row['start'];
	$chipid = $row['ID'];
	$chiptype =  $row['name'];
	$firstpos = $start - 250000;
	$stop = $row['stop'];
	$avgLogR = $row['avgLogR'];
	$lastpos = $stop + 250000;
	$content = $row['content'];
	$height = 515;
	$toppanel = 1;
	$toptitle = 'Log R Ratio';
	$bottompanel = 2;
	$bottomtitle = 'B-Allele Frequence';
	$toptype = 'logr';
	$bottomtype = 'baf';
	$increment = 3;
	$window = $lastpos-$firstpos+1;
}
elseif ($table == 'cus_aberration') {
	$datatable = 'cus_datapoints';
	$query = mysql_query("SELECT a.cn, a.start, a.stop, a.chr, d.content, d.structure, a.avgLogR FROM cus_aberration a JOIN cus_datapoints d ON a.id = d.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($query);
	$chr = $row['chr'];
	$cn = $row['cn'];
	$start = $row['start'];
	$firstpos = $start - 250000;
	$stop = $row['stop'];
	$avgLogR = $row['avgLogR'];
	$lastpos = $stop + 250000;
	$content = $row['content'];
	$structure = $row['structure'];
	$items = str_split($structure,1);
	$bottomtype = -1;
	$increment = 1;
	if ($items[1] == 1 && ($items[2] == 1 || $items[3] == 1)) {
		# TWO PANELS
		$increment++;
		$toppanel = 1; # more values, LogR is second
		$toptitle = 'Log R Ratio';
		$toptype = 'logr';
		if ($items[3] == 1) {
			if ($items[2] == 1) {
				$bottompanel = 3;  # need fourth value (pos, logR, gt, baf)
				$increment = $increment + 2;
			}
			else {
				$bottompanel = 2; # need third (pos, logR, baf)
				$increment++;
			}
			$bottomtitle = 'B-Allele Frequency';
			$bottomtype = 'baf';
		}
		else {
			$increment++;
			$bottompanel = 2;  # need third (pos, logR, gt)
			$bottomtitle = 'Genotype';
			$bottomtype = 'gt';
			include('inc_Gaussian.inc');
		}
		$height= 515;
	}
	else {
		# one panel
		$height = 335;
		if ($items[1] == 1) {
			$increment++;
			$toppanel = 1; # two values => need second
			$toptitle = 'Log R Ratio';
			$toptype = 'logr';
		}
		elseif ($items[3] == 1) {
			$increment++;
			if ($items[2] == 1) {
				$increment++;
				$toppanel = 2; # three values, need third (pos, gt, baf)
			}
			else {
				$toppanel = 1; # two values => need second (pos, baf)
			}
			$toptitle = 'B-Allele Frequency';
			$toptype = 'baf';
		}
		elseif ($items[2] == 1) {
			$increment++;
			$toppanel = 1; # two values => need second
			$toptitle = 'Genotype';
			$toptype = 'gt';
			include('inc_Gaussian.inc');
		}
	}
	$window = $lastpos-$firstpos+1;

}
elseif ($table == 'parents_regions') {
	$datatable = 'parents_datapoints';
	$query = mysql_query("SELECT a.start, a.stop, a.chr, d.content, a.avgLogR FROM parents_regions a JOIN parents_datapoints d ON a.id = d.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($query);
	$chr = $row['chr'];
	$start = $row['start'];
	$firstpos = $start;# - 250000;
	$stop = $row['stop'];
	$avgLogR = $row['avgLogR'];
	$lastpos = $stop;# + 250000;
	$content = $row['content'];
	$height = 515;
	$toppanel = 1;
	$toptitle = 'Log R Ratio';
	$bottompanel = 2;
	$bottomtitle = 'B-Allele Frequence';
	$toptype = 'logr';
	$bottomtype = 'baf';
	$increment = 3;
	$window = $stop - $start + 1;
	$start = $firstpos + 250000;
	$stop = $lastpos - 250000;
}

#$window = $lastpos-$firstpos+1; 

//Specify constant values
$width = 415; //Image width in pixels
$height = $height; // Image height in pixels
$xoff = 25;

//Create the image resource
$image = imagecreatetruecolor($width, $height);

//making opaque colors
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$gray = ImageColorAllocate($image,190,190,190);
$orange = ImageColorAllocate($image,255, 179,0);
// making transparent colors;
$red75   = ImageColorAllocateAlpha($image, 255, 0, 0,90);
$blue75  = imageColorallocateAlpha($image,0,0,255,90); 
$green75 = imageColorAllocateAlpha($image,0,190,0,90);
$purple75 = ImageColorAllocateAlpha($image,136,34,135,90);
$orange75 = ImageColorAllocateAlpha($image,255, 179,0,90);
$cns = array('0' => $red75, '1' => $red75, '2' => $orange75, '3' => $blue75, '4' => $blue75);
$cnsopaque = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);

#Fill background
imagefill($image,0,0,$white);

# DRAW MAIN GRAPH FIELDS
imagerectangle($image,$xoff,15,$xoff+380,195,$black);
if ($bottomtype != -1) {
	imagerectangle($image,$xoff,220,$xoff+380,400,$black);
}

#imagestring($image,2,5,30,$toppanel,$black);

# PRINT TITLES
$fontwidth = imagefontwidth(4);
$txtwidth = strlen($toptitle)*$fontwidth;
$txtx = $xoff + 190 - ($txtwidth/2);
imagestring($image,4,$txtx,-3,$toptitle,$black);
if ($bottompanel != -1) {
	#$title = "B Allele Frequency";
	$fontwidth = imagefontwidth(4);
	$txtwidth = strlen($bottomtitle)*$fontwidth;
	$txtx = $xoff + 190 - ($txtwidth/2);
	imagestring($image,4,$txtx,200,$bottomtitle,$black);
}
# DRAW GRID LINES
if ($toptype == 'logr') {
	imageline($image,$xoff,38,$xoff + 380,38,$gray);
	imageline($image,$xoff,60,$xoff + 380,60,$gray);
	imageline($image,$xoff,83,$xoff+380,83,$gray);
	imageline($image,$xoff,105,$xoff+380,105,$gray);
	imageline($image,$xoff,128,$xoff+380,128,$gray);
	imageline($image,$xoff,150,$xoff+380,150,$gray);
	imageline($image,$xoff,173,$xoff+380,173,$gray);
}
else {
	imageline($image,$xoff,51,$xoff+380,51,$gray);
	imageline($image,$xoff,87,$xoff+380,87,$gray);
	imageline($image,$xoff,123,$xoff+380,123,$gray);
	imageline($image,$xoff,159,$xoff+380,159,$gray);
} 
if ($bottomtype == 'baf' || $bottomtype == 'gt') {
	imageline($image,$xoff,256,$xoff+380,256,$gray);
	imageline($image,$xoff,292,$xoff+380,292,$gray);
	imageline($image,$xoff,328,$xoff+380,328,$gray);
	imageline($image,$xoff,364,$xoff+380,364,$gray);
}

# PRINT SCALES
if ($toptype == 'logr') {
	imagestring($image,2,$xoff-7,8,'2',$black);
	imagestring($image,2,$xoff-7,53,'0',$black);
	imagestring($image,2,$xoff-12,98,'-2',$black);
	imagestring($image,2,$xoff-12,143,'-4',$black);
	imagestring($image,2,$xoff-12,187,'-6',$black);
}
elseif ($toptype == 'baf') {
	imagestring($image,2,$xoff-7,8,'1',$black);
	imagestring($image,2,$xoff-20,44,'0.8',$black);
	imagestring($image,2,$xoff-20,80,'0.6',$black);
	imagestring($image,2,$xoff-20,116,'0.4',$black);
	imagestring($image,2,$xoff-20,154,'0.2',$black);
	imagestring($image,2,$xoff-7,187,'0',$black);
}
elseif ($toptype == 'gt') {
	imagestring($image,2,$xoff ,2,"(NC's are discarded)",$black);
	imagestring($image,2,$xoff-20,8,'BB',$black);
	imagestring($image,2,$xoff-20,97,'AB',$black);
	imagestring($image,2,$xoff-20,187,'AA',$black);
}
if ($bottomtype == 'baf') {
	imagestring($image,2,$xoff-7,213,'1',$black);
	imagestring($image,2,$xoff-20,249,'0.8',$black);
	imagestring($image,2,$xoff-20,285,'0.6',$black);
	imagestring($image,2,$xoff-20,321,'0.4',$black);
	imagestring($image,2,$xoff-20,357,'0.2',$black);
	imagestring($image,2,$xoff-7,393,'0',$black);
}
elseif ($bottomtype == 'gt') {
	imagestring($image,2,$xoff-7,213,'BB',$black);
	imagestring($image,2,$xoff-20,302,'AB',$black);
	imagestring($image,2,$xoff-7,393,'AA',$black);
}


$firstx = number_format($firstpos,0,'',',');
$lastx = number_format($lastpos,0,'',',');
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($lastx)*$fontwidth;
$txtx = $xoff+362 - $txtwidth;
if ($bottomtype != -1) {
	imageline($image,$xoff,400,$xoff,420,$black);
	imageline($image,$xoff+2,410,$xoff+5,407,$black);
	imageline($image,$xoff+2,410,$xoff+5,413,$black);
	imageline($image,$xoff+2,410,$xoff+15,410,$black);
	imagestring($image,2,$xoff+18,404,$firstx,$black);
	imageline($image,$xoff+380,400,$xoff+380,420,$black);
	imageline($image,$xoff+378,410,$xoff+375,407,$black);
	imageline($image,$xoff+378,410,$xoff+375,413,$black);
	imageline($image,$xoff+378,410,$xoff+365,410,$black);
	imagestring($image,2,$txtx,404,$lastx,$black);
}
else {
	imageline($image,$xoff,195,$xoff,215,$black);
	imageline($image,$xoff+2,205,$xoff+5,202,$black);
	imageline($image,$xoff+2,205,$xoff+5,208,$black);
	imageline($image,$xoff+2,205,$xoff+15,205,$black);
	imagestring($image,2,$xoff+18,199,$firstx,$black);
	imageline($image,$xoff+380,195,$xoff+380,215,$black);
	imageline($image,$xoff+378,205,$xoff+375,202,$black);
	imageline($image,$xoff+378,205,$xoff+375,208,$black);
	imageline($image,$xoff+378,205,$xoff+365,205,$black);
	imagestring($image,2,$txtx,199,$lastx,$black);
}

# DRAW DATAPOINTS
$datapoints = explode("_",$content);
$nrelements = count($datapoints);
$scalex = 380 / $window;
$tlogrzero = 60;
$tbafzero = 195;
$blogrzero = 265;
$bbafzero = 400;
$scalel = 180 / 8;
$scaleb = 180 / 1;
$gts = array('AA' => 0, 'AB' => 0.5, 'BB' => 1 );
$zeroc = array("0" => $black, "1" => $red);
for ($i=0;$i<$nrelements;$i+=$increment) {
	## POSITION AND ZERO'ED
	$pos = $datapoints[$i];
	$x = $xoff + intval(round(($pos-$firstpos)*$scalex));
	if (defined($chipid)) {
		$query = mysql_query("SELECT zeroed FROM probelocations WHERE chromosome = '$chr' AND position = '$pos' AND chiptype = '$chipid'");
		$zres = mysql_fetch_array($query);
		$zeroed = $zres['zeroed'];
	}
	else {
		$zeroed = 0;
	}
	## TOP PANEL
    if (count($datapoints) > $i+$toppanel) {
	    $tvalue = $datapoints[$i+$toppanel];
	    if ($toptype == 'logr') {
	    	if ( $tvalue < -6) {
	    		$tvalue = -6;
	    	}
	    	$ty = $tlogrzero - ($tvalue*$scalel);
	    }
	    elseif ($toptype == 'baf') {
	    	$ty = $tbafzero - ($tvalue*$scaleb);
	    }
	    elseif ($toptype == 'gt') {
	    	#if ($tvalue != 'NC') {
	    	$gtval = $gts[$tvalue] + gauss_ms(0,0.01);
	    	#}
	    	#else {
	    	#	$gtval = 0.5 + gauss_ms(0,0.5);
	    	#}
	    	if ($gtval > 1) {
	    		$gtval = 2 - $gtval ;
	    	}
	    	elseif ($gtval < 0) {
	    		$gtval = abs($gtval);
	    	}
	    	$ty = $tbafzero - ($gtval * $scaleb);
	    }
	    imagefilledellipse($image,$x,$ty,5,5,$zeroc[$zeroed]);
    }
	## BOTTOM PANEL
	if ($bottomtype != -1 && count($datapoints) >= $i+$bottompanel) {
		$bvalue = $datapoints[$i+$bottompanel];
		if ($bottomtype == 'baf') {
			$by = $bbafzero - ($bvalue*$scaleb);
		}
		elseif ($bottomtype == 'gt') {
			$gtval = $gts[$bvalue] + abs(gauss_ms(0,0.01));
			$gtval = 1;
			if ($gtval > 1) {
				$gtval = 2 - $gtval ;
			}
			$by = $bbafzero - ($gtval * $scaleb);
		}
		imagefilledellipse($image,$x,$by,5,5,$zeroc[$zeroed]);
	}
}
# DRAW CNV delineation
$startx = $xoff + intval(round(($start-$firstpos)*$scalex));
$stopx = $xoff + intval(round(($stop-$firstpos)*$scalex));
if (isset($cn)) {
	imagefilledrectangle($image,$startx,15,$stopx,195,$cns[$cn]);

	if ($bottomtype != -1) {
		imagefilledrectangle($image,$startx,220,$stopx,400,$cns[$cn]);
	}
	# print expected intensity for goal copy number
	$style = array($cnsopaque[$cn], $cnsopaque[$cn], $cnsopaque[$cn], $cnsopaque[$cn], $cnsopaque[$cn], IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT);
	imagesetstyle($image, $style);

}
else {
	imagerectangle($image,$startx,15,$stopx,195,$gpos50);

	if ($bottomtype != -1) {
		imagerectangle($image,$startx,220,$stopx,400,$gpos50);
	}
	# print expected intensity for goal copy number
	$style = array($cnsopaque[2], $cnsopaque[2], $cnsopaque[2], $cnsopaque[2], $cnsopaque[2], IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT);
	imagesetstyle($image, $style);


}
if (defined($avgLogR)) {
	$logavg = $tlogrzero - ($avgLogR*$scalel);
	imageline($image,$startx,$logavg,$stopx,$logavg,$black);
}
# DRAW SCALE BOX
$scales = array("3Mb" => 3000000, "1Mb" => 1000000, "500Kb" => 500000, "250Kb" => 250000, "100Kb" => 100000, "75Kb" => 75000, "50Kb" => 50000);
foreach($scales as $stext => $scale) {
	$scaledscale = intval(round($scale*$scalex));
	if ($scaledscale < 55) {
		imagerectangle($image, $xoff + 320, 0, $xoff+380, 13, $black);
		$fontwidth = imagefontwidth(1);
		$txtwidth = strlen($stext)*$fontwidth;
		$txtx = $xoff + 350 -($txtwidth/2);
		imagestring($image,1,$txtx,1,$stext,$black);
		$xstart = $xoff + 350 -($scaledscale/2);
		$xstop = $xoff + 350 +($scaledscale/2);
		imageline($image,$xstart,11,$xstart,7,$black);
		imageline($image,$xstop,11,$xstop,7,$black);
		imageline($image,$xstart,9,$xstop,9,$black);
		break;
	}
}

##############################
## SET OFFSET FOR GENES ETC ##
##############################
if ($bottomtype != -1) {
	$yoff = 435;
}
else {
	$yoff = 230;
}
# DRAW GENES
$locquery = "AND ((start BETWEEN '$firstpos' AND '$lastpos') OR (stop BETWEEN '$firstpos' AND '$lastpos') OR (start <= '$firstpos' AND stop >= '$lastpos'))";
$genequery = mysql_query("SELECT start, stop, strand, omimID, morbidID,exonstarts,exonends FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");

$title = "Genes (RefSeq & ENCODE)";
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($title)*$fontwidth;
$txtx = $xoff + 190 - ($txtwidth/2);
imagestring($image,3,$txtx,$yoff - 19,$title,$black);
imageline($image,$xoff,$yoff - 18,$xoff + 380,$yoff - 18,$black);

$y = $yoff;
while ($generow = mysql_fetch_array($genequery)) {
	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	$gomim = $generow['omimID'];
	$gmorbid = $generow['morbidID'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($gmorbid != '') {
		$gcolor = $red;
	}
	elseif ($gomim != '') {
		$gcolor = $blue;
	}
	else {
		$gcolor = $black;
	}
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	imageline($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y,$hooktip-2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip-2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y,$hooktip+2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip+2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $firstpos) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $lastpos) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$firstpos) * $scalex));
		$gexscaledstop = intval(round(($gexe-$firstpos) * $scalex));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-3,$xoff+$gexscaledstop,$y+3,$gcolor);
	}

}
# DRAW ENCODE CODING GENES
$y = $y + 10;
$genequery = mysql_query("SELECT start, stop, strand, exonstarts,exonends FROM encodesum WHERE chr = '$chr' $locquery ORDER BY start");
while ($generow = mysql_fetch_array($genequery)) {

	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	$gcolor = $gray;
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	imageline($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y,$hooktip-2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip-2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y,$hooktip+2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip+2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $firstpos) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $lastpos ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$firstpos) * $scalex));
		$gexscaledstop = intval(round(($gexe-$firstpos) * $scalex));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-3,$xoff+$gexscaledstop,$y+3,$gcolor);
	}

}
/*
if (isset($cstm) && $cstm != 1) {
	# DRAW CONTROLES (HAPMAP)
	$y = $y+10;
	$title = "HapMap for $chiptype";
	$fontwidth = imagefontwidth(3);
	$txtwidth = strlen($title)*$fontwidth;
	$txtx = $xoff + 190 - ($txtwidth/2);
	imagestring($image,3,$txtx,$y,$title,$black);
	imageline($image,$xoff,$y,$xoff+380,$y,$black);
	$chipstring = $chipid;
	$chipstring = str_replace('4','10',$chipstring);
	if ($chr == 23) {
		$arr = array(0,1,2,3,4);
	}
	else {
		$arr = array(0,1,3,4);
	}
	$y = $y+5;
	foreach ($arr as $i) {
		$y = $y+10;
		$string = "CN:$i";
		imagestring($image,2,$xoff-25,$y-5,$string,$gray);
		$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
		$abquery = mysql_query("$querystring");
		while ($abrow = mysql_fetch_array($abquery)) {
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $firstpos) {
				$cstart = $firstpos;
			}
			if ($cstop > $lastpos) {
				$cstop = $lastpos;
			}
			$scaledstart = intval(round(($cstart-$firstpos) * $scalex));
			$scaledstop = intval(round(($cstop-$firstpos) * $scalex));
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnsopaque[$i]);
		}
	
	}
	$y = $y+10;
	imageline($image,$xoff+380,$yoff - 35,$xoff+380,$y,$black);
	imageline($image,$xoff,$yoff - 35,$xoff,$y,$black);
	imageline($image,$xoff,$y,$xoff+380,$y,$black);
}
else {
	## DRAW CONTROLS (DGV)
	$y = $y+10;
	$title = "Toronto DGV";
	$fontwidth = imagefontwidth(3);
	$txtwidth = strlen($title)*$fontwidth;
	$txtx = $xoff + 190 - ($txtwidth/2);
	imagestring($image,3,$txtx,$y,$title,$black);
	imageline($image,$xoff,$y,$xoff+380,$y,$black);
	# LOAD DGV TRACK for selected studies
	$dgvtypes = array(1 => "gain", 2 => "loss", 3 => "inv");
	$dgvcolors = array(1 => $blue, 2 => $red, 3 => $gray);
	$dgvtitles = array(1 => "Gain", 2 => "Loss", 3 => "Inv.");
	$y = $y+5;
	for ($i = 1; $i <= 3; $i++) {
		$y = $y+10;
		$string = $dgvtitles[$i];
		imagestring($image,1,$xoff-25,$y-5,$string,$gray);
		$dgvquery = mysql_query("SELECT start, stop FROM DGV_full WHERE chr = '$chr' AND $dgvtypes[$i] > 0 AND study IN ($DGVinc) $locquery ORDER BY start, stop");
		$nrab = mysql_num_rows($dgvquery);
		
		while ($abrow = mysql_fetch_array($dgvquery)) {
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $firstpos) {
				$cstart = $firstpos;
			}
			if ($cstop > $lastpos) {
				$cstop = $lastpos;
			}
			$scaledstart = intval(round(($cstart-$firstpos) * $scalex));
			$scaledstop = intval(round(($cstop-$firstpos) * $scalex));
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$dgvcolors[$i]);
		}
	}
	$y = $y+10;
	imageline($image,$xoff+380,$yoff - 35,$xoff+380,$y,$black);
	imageline($image,$xoff,$yoff - 35,$xoff,$y,$black);
	imageline($image,$xoff,$y,$xoff+380,$y,$black);

}
*/
//Output header

	
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

