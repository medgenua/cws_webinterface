#!/usr/bin/perl

use Number::Format;
use Getopt::Std;
use DBI;
#use Storable; 
use Data::Dumper; 
# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);

$| = 1;

# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y"; 
$chromhash{ "X" } = "23";
$chromhash{ "Y" } = "24";
$chromhash{ "M" } = "25";
$chromhash{ "25" } = "M";

##########################
# COMMAND LINE ARGUMENTS #
##########################
# L = Looking at a new database, LiftOver old values if needed
# D = Use specific database (mandatory if L is specified), otherwise use default.
# d = Database to use as source (mandatory if L is specified.
# u = User ID, mandatory
# t = comma seperated list of tables to lift.
# a = autocorrect failed lifts based on probes (for illumina CNVs)
# G = Gap size for lift merging
# p = try to autocorrect parental regions based on the corresponding offspring regions. 

getopts('LD:d:u:t:aG:p', \%opts);  # option are in %opts

print "1/ Connecting to the database\n";
my $db = "";

if ($opts{'D'}){
	$db = "CNVanalysis".$opts{'D'};
	$targetdb = $opts{'D'};
	print "\t=> Using user specified database $db\n";
}
else {
	# connect to GenomicBuilds DB to get the current Genomic Build Database. 
    my %attr = (PrintError => 0,RaiseError => 0,mysql_auto_reconnect => 1,mysql_local_infile => 1);

	my $dbhgb = DBI->connect("dbi:mysql:GenomicBuilds:$host",$userid,$userpass,\%attr);
	my $gbsth =$dbhgb->prepare("SELECT name, StringName FROM CurrentBuild");
	$gbsth->execute();
	my @row = $gbsth->fetchrow_array();
	$gbsth->finish();
	$db = "CNVanalysis".$row[0];
	$newbuild = $row[1];
	print "\t=> Using current build database (build ".$row[1].")\n";	
}
$connectionInfocnv="dbi:mysql:$db:$host"; 
my %attr = (PrintError => 0,RaiseError => 0,mysql_auto_reconnect => 1,mysql_local_infile => 1);

$dbh = DBI->connect($connectionInfocnv,$userid,$userpass,\%attr) ;

if ($opts{'d'}) {
	$sourcedb = $opts{'d'};
	$longsourcedb = "CNVanalysis$sourcedb";
}
if ($opts{'u'}) {
	$uid = $opts{'u'};
}
else {
	$uid = 1;
}
if ($opts{'a'}) {
    print "Autocorrecting failed lifts using probe data\n";
	$autocorrect = 1;
}
else{
	$autocorrect = 0;
}
if ($opts{'G'}) {
	$Gap = $opts{'G'};
}
else {
	$Gap = 10000;
}
if ($opts{'p'}) {
    print "Autocorrecting failed parental lifts using offspring data\n";
	$autoparents = 1;
}
else{
	$autoparents = 0;
}

my @tables;
if ($opts{'t'}) {
	@tables = split(/,/,$opts{'t'});
}	
if (scalar(@tables) == 0) {
	print "No tables specified.\n";
	print "Process done.\n";
	system("echo 0 > /site/status/LiftOver.status");
	exit();
}


###################################################
print "#######################################\n";#
print "## READ IN OLD AND NEW DATALOCATIONS ##\n";#
print "#######################################\n";#
###################################################
# old locations
print "Old locations\n";
my %oldbyname = ();
$sth = $dbh->prepare("SELECT chromosome, position, name,chiptype FROM `$longsourcedb`.`probelocations` ");
my $rowcache;
my $rows = $sth->execute();
if ($rows < 10000) {
	$max = $rows;
}
else {
     $max = 10000;
}
while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	my $chr = $result->[0];
	my $pos = $result->[1];
	my $name = $result->[2];
	my $chiptype = $result->[3];
	## there are some incorrect mappings by illumina (same probe, shifted by 1 nucleotide on different chips)
	## this is NOT global (eg 0-based vs 1-based index), but only for a subset of probes on the chip. 
	## We need to store by name-chip combination to solve this. This takes way more memory (just over 2Gb for 5M probes...
	$oldbyname{"$name-$chiptype"} = "$chr-$pos";
}
$sth->finish();
# new locations
print "New Locations\n";
my %poshash = ();
$sth = $dbh->prepare("SELECT chromosome, position, name,chiptype FROM `$db`.`probelocations` ");
$rowcache = ();
$rows = $sth->execute();
if ($rows < 10000) {
	$max = $rows;
}
else {
     $max = 10000;
}
while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
	my $chr = $result->[0];
	my $pos = $result->[1];
	my $name = $result->[2];
	my $chiptype = $result->[3];
	my $oldpos = $oldbyname{"$name-$chiptype"};
	$poshash{$oldpos} = "$chr-$pos";
}
$sth->finish();
# store. (for debug)
#store \%poshash, "/home/webstoredev/poshash.hash";
# restore: 
#%poshash = %{retrieve( "/home/webstoredev/poshash.hash")};
%oldbyname = ();
#############################
## START PROCESSING TABLES ##
#############################
foreach (@tables) {
	my $table = $_;
	$outname = $table;
	#print "Starting lift for $table\n";
	if ($table eq 'datapoints') {
		&LiftDatapoints;
        
		next;
	}
	if ($table eq 'BAFSEG') {
		&LiftBAFSEG;
		next;
	}
	if ($table eq 'parents_datapoints') {
		&LiftParentsDatapoints;
		next;
	}
	if ($table eq 'cus_datapoints') {
		&LiftCusDatapoints;
		next;
	}
	if ($table eq 'parents_upd_datapoints') {
		&LiftUPDDatapoints;
		next;
	}
    if ($table eq 'datapoints_LOH') {
        &LiftLOHDatapoints;
        next;
    }
}

##################################
## AUTOCORRECT PARENTAL REGIONS ##
##################################
if ($autoparents == 1) {
	print "Checking parent_offspring CNV relations to rescue failed parental regions.\n";
	my $rescued = 0;
	# main query
	my $query = "SELECT itemID, ID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = 'parents_regions' AND frombuild ='$sourcedb' AND tobuild = '$targetdb'";
	my $sth = $dbh->prepare($query);
	# sub query to fetch offspring CNV-ID
	my $subquery = "SELECT po.aid, pr.avgLogR, pr.sid FROM `CNVanalysis$sourcedb`.parent_offspring_cnv_relations po JOIN `CNVanalysis$sourcedb`.parents_regions pr ON po.parent_aid = pr.id AND po.parent_sid = pr.sid WHERE pr.id = ?";
	my $ssth = $dbh->prepare($subquery);
	# sub query to fetch lifted coordinates from offpring CNV
	my $subsubquery = "SELECT chr, start, stop FROM `CNVanalysis$targetdb`.aberration WHERE id = ?";
	my $sssth = $dbh->prepare($subsubquery);
	# subquery to fetch avg logR from parental region 
	#my $avgquery = "SELECT avgLogR FROM `CNVanalysis$sourcedb`.parents_regions WHERE id = ?";
	#my $avgsth = $dbh->prepare($avgquery);
	# execute main query
	my $rows = $sth->execute();
	my $rowcache = ();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		my $aid = $result->[0];
		my $failedid = $result->[1];
		# execute first subquery
		$ssth->execute($aid);
		my ($oaid,$avg,$sid) = $ssth->fetchrow_array();
		# execute second subquery
		my $srows = $sssth->execute($oaid);
		if ($srows > 0) {
			$rescued++;
			my ($newchr, $newstart, $newstop) = $sssth->fetchrow_array();
			#$avgsth->execute($aid);
			#my ($avg) = $avgsth->fetchrow_array();
			$dbh->do("INSERT INTO `CNVanalysis$targetdb`.parents_regions (id, chr, start, stop, sid, avgLogR, LiftedFrom) VALUES ('$aid', '$newchr', '$newstart', '$newstop', '$sid', '$avg', '$sourcedb')");
			$dbh->do("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE ID = '$failedid'");
		}
	}
	$sth->finish();
	$ssth->finish();
	$sssth->finish();
	#$avgsth->finish();
    if ($rescued > 0) {
	    print "Rescued $rescued out of $rows items\n";
    }
}

# finished 
############################
## DATAPOINTS ARE UPDATED ##
############################
# store this in database !
$dbh->do("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step = '5' WHERE frombuild ='$sourcedb' AND tobuild = '$targetdb'");
# change status for website
system("echo 0 > $sitedir/status/LiftOver.status");















####################
### SUBROUTINES  ###
####################
sub LiftDatapoints {
	my $table = 'datapoints';
	print "\n";
	print "###########################\n";
	print "## Processing DATAPOINTS ## \n";
	print "###########################\n";
	## get content and other needed info on cnvs
	$sth = $dbh->prepare("SELECT a.chr,  a.id, p.chiptypeid, a.nrsnps, a.start, a.stop FROM `$longsourcedb`.aberration a JOIN `$longsourcedb`.project p ON p.id = a.idproj ");
	my $rowcache = ();
	my $rows = $sth->execute();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	## create list of failed cnv lifts for autocorrection.
	my %faileditems = ();
	if ($autocorrect == 1) { 
		print "Getting failed items\n";
        print "SELECT itemID,ID, Reason FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$sourcedb' AND tobuild = '$targetdb' AND tablename = 'aberration'\n";
		my $failed = $dbh->prepare("SELECT itemID,ID, Reason FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$sourcedb' AND tobuild = '$targetdb' AND tablename = 'aberration'");
		my $srows = $failed->execute();
		my $scache = ();
		if ($srows < 10000) {
			$smax = $srows;
		}
		else {
		     $smax = 10000;
		}
		while (my $sresult = shift(@$scache) || shift( @{$scache = $failed->fetchall_arrayref(undef,$smax)|| []})) {
            # itemID (aberration) => failure_id (ID) 
			$faileditems{$sresult->[0]}{'id'} = $sresult->[1];
            $faileditems{$sresult->[0]}{'reason'} = $sresult->[2];
		}
	}
	print "nr of items to do: $rows\n";
    print "nr of failed items to process: ".keys(%faileditems)."\n";

	## open file output
	open OUT, ">$maintenancedir/LiftOver/Files/DataPoints.lift.tmp" or die("could not open output file $maintenancedir/LiftOver/Files/DataPoints.lift.tmp\n");
	print "processing actual aberrations datapoints\n";
	my $counter = 0;
	my $tresh = 10;
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		$counter++;
		if ($counter > ($tresh * ($rows / 100))) {
		#	print "#####################\n";
			print "$tresh%... ";
		#	print "#####################\n";
			$tresh = $tresh + 10;
		}
		my $chr = $result->[0];
		my $lf = $sourcedb;
		my $id = $result->[1];
		#if (!exists($faileditems{$id})) {
		#	next;
		#}
		my $chipid = $result->[2];
		my $nrsnps = $result->[3];
		my $astart = $result->[4];
		my $astop = $result->[5];
		# get datapoints 
		my $dsth = $dbh->prepare("select content, LiftedFrom,structure,datatype FROM `$longsourcedb`.datapoints WHERE id = $id");
		$dsth->execute();
		my @dr = $dsth->fetchrow_array();
		$dsth->finish();
		my $content = $dr[0];
		if ($dr[1] != '') {
			$lf = $dr[1];
		}
		my $struct = $dr[2];
		my $dtype = $dr[3];
		# no content, print out and skip
		if ($content eq '') {
			#print "$id : No datapoints available\n";
			print OUT "$id\t\t$lf\t$struct\t$dtype\n";
			next;
		}
		# split content
		my @items = split(/_/,$content);
		my $nr = scalar(@items);
		my $newcontent = '';
		my @probes = ();
		push @probes, $chipid;
		push @probes, $chr;
		#push @probes, $nrsnps;
		push @probes, $astart;
		push @probes, $astop;
		my $sum = 0;
		my $nrinregion = 0;
		my $nrtotal = 0;
		my $sourcenr = 0;
        # Illumina data is either 1110 or 1111 as stucture.
        my $increment = 3;
        if ($struct eq '1111') {
            $increment = 4;
        }
		for ($i = 0; $i < $nr ; $i = $i + $increment) {
			$sourcenr++; # count all
			my $pos = $items[$i];
			my $lr = $items[$i+1];
			my $baf = $items[$i+2];
			my $newpos = $poshash{"$chr-$pos"};
			if ($newpos eq '') {
				## probe missing in new coordinates
				next;
			}  
            #my ($newchr,$newpos) = split(/-/,$newpos);
			$newpos =~ m/(\d+)-(\d+)/;
			$newchr = $1;
			$newpos = $2;
			if ($newchr != $chr) {
				## probe remapped to other chr. 
				next;
			}
			if ($pos >= $astart && $pos <= $astop) {
				$sum = $sum + $lr;
				$nrinregion++;
			}
			$nrtotal++;
			$newcontent .= "$newpos"."_".$lr."_"."$baf"."_";
            if ($increment == 4) {
                $newcontent .= $items[$i+3]."_";
            }
			push @probes, $newpos;
		}
		my $avglr = 0;
		if ($nrinregion > 0) {
			$avglr = $sum / $nrinregion;
		}
		## store last items		
		unshift @probes, $avglr;
		unshift @probes, $nrtotal;
		unshift @probes, $sourcenr;
        if ($id == 2991414) {
            print Dumper($newcontent);
        }
		print OUT "$id\t$newcontent\t$lf\t$struct\t$dtype\n";	
		## if this cnv failed to lift, store probes for further analysis.
		if (exists $faileditems{$id}) {
			my $nrpr = scalar(@probes);
			#print "Storing $nrpr probes for aid = $id\n";
			$faileditems{$id}{'probes'} = [ @probes ] ;
		}
			
	}
	$sth->finish();
	close OUT;
	## load output file to database
	print "\nLoading to database\n";
	$dbh->do("TRUNCATE TABLE `$db`.`datapoints`");
	$dbh->do("LOAD DATA LOCAL INFILE '$maintenancedir/LiftOver/Files/DataPoints.lift.tmp' INTO TABLE `$db`.`datapoints`");
    unlink("$maintenancedir/LiftOver/Files/DataPoints.lift.tmp");

    # store to file. for debug
    #store \%faileditems, "/home/webstoredev/faileditems.hash";
    # restore:
    #%faileditems = %{retrieve( "/home/webstoredev/faileditems.hash")};
	## check failed items
	print "Processing failed items\n";
    my $rescued = 0;
	foreach (keys(%faileditems)) {
		my $aid = $_;
		my $failedid = $faileditems{$aid}{'id'};
		if (!exists $faileditems{$aid}{'probes'}) {
			#print "No probes present for $aid\n";
			# no probes present, skip
			next;
		}
		## array with probe locations.
		my @probes = @{ $faileditems{$aid}{'probes'} };
		#print "aid $aid => " . scalar(@probes) . " items in probes array\n";
		# get non-probe info from front of array
		my $sourcenr = shift(@probes);
		my $nrp = shift(@probes);
		my $avglr = shift(@probes);
		my $chiptypeid = shift(@probes);
		my $chr = shift(@probes);
		#my $nrp = shift(@probes);
		my $astart = shift(@probes);
		my $astop = shift(@probes);
		# sort and get min-max position
		@probes = sort(@probes);
		my $first = $probes[0];
		my $last = $probes[$#probes];
		## if to be accepted, all probes retrieved should be in the @probes array, in the same order. 
		## this would mean that the region is still covered by just these probes, in the same order, on the new array.
		## if probes were deleted, they are deleted both in the @probes and database. 
		## in this case, check how many are deleted (accepted maximum : 5). (this is checked first for performance)
		## NOTE: this check is over the probes in actual cnv region + 250Kb 5' and 3' . 
        
        ## REVISED 
        ##   - if region is within 10% deviation from original size, with a max of 250kb ; accept lift based on start/end probe lift. 
        
        # get start/end position of the lifted probes
        my $newstart = $poshash{"$chr-$astart"};
        my $newstart_full = $poshash{"$chr-$astart"};
        my $newstop = $poshash{"$chr-$astop"};
        my $newstop_full = $poshash{"$chr-$astop"};
        $newstart =~ s/(\d+)-(\d+)/$2/;
		$newstop =~ s/(\d+)-(\d+)/$2/;
        # missing probes on border : move in
        if ($newstart eq "") {
            ## get probes. 
            #print("Border probe did not lift, querying region for first lifted position.")
		    my $psth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND chiptype = '$chiptypeid' AND position BETWEEN $first AND $last ORDER BY position LIMIT 1");
		    my $prows = $psth->execute();
            my @prow = $psth->fetchrow_array();
            $newstart = $prow[0];
        }
        if ($newstop eq "") {
            #print("Border probe did not lift, querying region for last lifted position.")
		    my $psth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND chiptype = '$chiptypeid' AND position BETWEEN $first AND $last ORDER BY position DESC LIMIT 1");
		    my $prows = $psth->execute();
            my @prow = $psth->fetchrow_array();
            $newstop = $prow[0];
        }
        if ($astop == $astart) {
            # increment to prevent division by zero
            print("zero size cnv ? : $astart - $astop ; aid = $aid\n");
            $astop++;
        }
        if (($newstop-$newstart) / ($astop-$astart) >= 0.9 && ($newstop-$newstart) / ($astop-$astart) <= 1.1  && abs(($newstop-$newstart) - ($astop-$astart)) <= 250000) {
            $rescued++;
            #print("rescueing $aid : $chr:$astart-$astop as $chr:$newstart-$newstop  : reason was : ".$faileditems{$aid}{'reason'}."\n");

        }
        else {
            print("Size difference is too big : $chr:$astop-$astart vs $chr:$newstop-$newstart   : ". ($newstop-$newstart) / ($astop-$astart) . " \n");
            next;
        }

        # get largestart based on lifted probe positions.
		my $newlstart = 0;
		my $newlstop = 'ter';
		my $lsth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND position < '$newstart' AND chiptype = '$chiptypeid' ORDER BY position DESC LIMIT 1");
		my $nr = $lsth->execute();
		if ($nr != 0) {
			my @r = $lsth->fetchrow_array();
			$newlstart = $r[0];
		}
		$lsth->finish();
		$lsth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND position > '$newstart' AND chiptype = '$chiptypeid' ORDER BY position ASC LIMIT 1");
		$nr = $lsth->execute();
		if ($nr != 0) {
			my @r = $lsth->fetchrow_array();
			$newlstop = $r[0];
		}
		$lsth->finish();

		## got needed info. move and update from deleted to cnv
		my $dsth = $dbh->prepare("SELECT cn, sample, idproj, inheritance, class, seenby, validation, validationdetails, pubmed FROM `$db`.`deletedcnvs` WHERE id = '$aid'");
		$dsth->execute();
		my @r = $dsth->fetchrow_array();
		my $cn = $r[0];
		my $sample = $r[1];
		my $pid = $r[2];
		my $inh = $r[3];
		my $dc = $r[4];
		my $sb = $r[5];
		my $val = $r[6];
		my $vald = $r[7];
		my $pm = $r[8];
		my $size = $newstop - $newstart + 1;
		$dsth->finish();
		my $isth = $dbh->prepare("INSERT INTO `$db`.`aberration` (id, cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, seenby, nrsnps, size, LiftedFrom, validation, validationdetails, pubmed, avgLogR) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$isth->execute($aid,$cn,$newstart,$newstop,$chr,$newlstart,$newlstop,$sample,$pid,$inh,$dc,$sb,$nrp,$size,$sourcedb,$val,$vald,$pm,$avglr);
		$isth->finish(); 
		$dbh->do("DELETE FROM `$db`.`deletedcnvs` WHERE id = $aid");
		$dbh->do("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE ID = '$failedid'");  
		#print "Rescued Region chr$chr:$astart-$astop as chr$chr:$newstart-$newstop\n";
	}
    print "Rescued $rescued items\n";
} 

sub LiftLOHDatapoints {
	my $table = 'datapoints';
	print "\n";
	print "###############################\n";
	print "## Processing LOH DATAPOINTS ## \n";
	print "###############################\n";
	## get content and other needed info on cnvs
	$sth = $dbh->prepare("SELECT a.chr,  a.id, p.chiptypeid, a.nrsnps, a.start, a.stop FROM `$longsourcedb`.aberration_LOH a JOIN `$longsourcedb`.project p ON p.id = a.idproj ");
	my $rowcache = ();
	my $rows = $sth->execute();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	## create list of failed cnv lifts for autocorrection.
	my %faileditems = ();
	if ($autocorrect == 1) { 
		print "Getting failed items\n";
		my $failed = $dbh->prepare("SELECT itemID,ID, Reason FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$sourcedb' AND tobuild = '$targetdb' AND tablename = 'aberration_LOH'");
		my $srows = $failed->execute();
		my $scache = ();
		if ($srows < 10000) {
			$smax = $srows;
		}
		else {
		     $smax = 10000;
		}
		while (my $sresult = shift(@$scache) || shift( @{$scache = $failed->fetchall_arrayref(undef,$smax)|| []})) {
            # itemID (aberration) => failure_id (ID) 
			$faileditems{$sresult->[0]}{'id'} = $sresult->[1];
            $faileditems{$sresult->[0]}{'reason'} = $sresult->[2];
		}
	}
	print "nr of items to do: $rows\n";
    print "nr of failed items to process: ".keys(%faileditems)."\n";

	## open file output
	open OUT, ">$maintenancedir/LiftOver/Files/DataPointsLOH.lift.tmp" or die("could not open output file $maintenancedir/LiftOver/Files/DataPointsLOH.lift.tmp\n");
	print "processing actual aberrations datapoints\n";
	my $counter = 0;
	my $tresh = 10;
    # "SELECT a.chr,  a.id, p.chiptypeid, a.nrsnps, a.start, a.stop FROM `$longsourcedb`.aberration_LOH a JOIN `$longsourcedb`.project p ON p.id = a.idproj "
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		$counter++;
		if ($counter > ($tresh * ($rows / 100))) {
		#	print "#####################\n";
			print "$tresh%... ";
		#	print "#####################\n";
			$tresh = $tresh + 10;
		}
		my $chr = $result->[0];
		my $lf = $sourcedb;
		my $id = $result->[1];
		#if (!exists($faileditems{$id})) {
		#	next;
		#}
		my $chipid = $result->[2];
		my $nrsnps = $result->[3];
		my $astart = $result->[4];
		my $astop = $result->[5];
		# get datapoints 
		my $dsth = $dbh->prepare("select content, LiftedFrom,structure FROM `$longsourcedb`.datapoints_LOH WHERE id = $id");
		$dsth->execute();
		my @dr = $dsth->fetchrow_array();
		$dsth->finish();
		my $content = $dr[0];
		if ($dr[1] != '') {
			$lf = $dr[1];
		}
		my $struct = $dr[2];
		#my $dtype = $dr[3];
		# no content, print out and skip
		if ($content eq '') {
			#print "$id : No datapoints available\n";
			print OUT "$id\t\t$struct\t$lf\n";
			next;
		}
		# split content
		my @items = split(/_/,$content);
		my $nr = scalar(@items);
		my $newcontent = '';
		my @probes = ();
		push @probes, $chipid;
		push @probes, $chr;
		#push @probes, $nrsnps;
		push @probes, $astart;
		push @probes, $astop;
		my $sum = 0;
		my $nrinregion = 0;
		my $nrtotal = 0;
		my $sourcenr = 0;
        # Illumina data is either 1110 or 1111 as stucture.
        my $increment = 3;
        if ($struct eq '1111') {
            $increment = 4;
        }
		for ($i = 0; $i < $nr ; $i = $i + $increment) {
			$sourcenr++; # count all
			my $pos = $items[$i];
			my $lr = $items[$i+1];
			my $baf = $items[$i+2];
			my $newpos = $poshash{"$chr-$pos"};
			if ($newpos eq '') {
				## probe missing in new coordinates
				next;
			}  
            #my ($newchr,$newpos) = split(/-/,$newpos);
			$newpos =~ m/(\d+)-(\d+)/;
			$newchr = $1;
			$newpos = $2;
			if ($newchr != $chr) {
				## probe remapped to other chr. 
				next;
			}
			if ($pos >= $astart && $pos <= $astop) {
				$sum = $sum + $lr;
				$nrinregion++;
			}
			$nrtotal++;
			$newcontent .= "$newpos"."_".$lr."_"."$baf"."_";
            if ($increment == 4) {
                $newcontent .= $items[$i+3]."_";
            }
			push @probes, $newpos;
		}
		my $avglr = 0;
		if ($nrinregion > 0) {
			$avglr = $sum / $nrinregion;
		}
		## store last items		
		unshift @probes, $avglr;
		unshift @probes, $nrtotal;
		unshift @probes, $sourcenr;
        
		print OUT "$id\t$newcontent\t$struct\t$lf\n";	
		## if this cnv failed to lift, store probes for further analysis.
		if (exists $faileditems{$id}) {
			my $nrpr = scalar(@probes);
			#print "Storing $nrpr probes for aid = $id\n";
			$faileditems{$id}{'probes'} = [ @probes ] ;
		}
			
	}
	$sth->finish();
	close OUT;
	## load output file to database
	print "\nLoading to database\n";
	$dbh->do("TRUNCATE TABLE `$db`.`datapoints_LOH`");
	$dbh->do("LOAD DATA LOCAL INFILE '$maintenancedir/LiftOver/Files/DataPointsLOH.lift.tmp' INTO TABLE `$db`.`datapoints_LOH`");
    unlink("$maintenancedir/LiftOver/Files/DataPointsLOH.lift.tmp");

    # store to file. for debug
    #store \%faileditems, "/home/webstoredev/faileditems.hash";
    # restore:
    #%faileditems = %{retrieve( "/home/webstoredev/faileditems.hash")};
	## check failed items
	print "Processing failed items\n";
    my $rescued = 0;
	foreach (keys(%faileditems)) {
		my $aid = $_;
		my $failedid = $faileditems{$aid}{'id'};
		if (!exists $faileditems{$aid}{'probes'}) {
			#print "No probes present for $aid\n";
			# no probes present, skip
			next;
		}
		## array with probe locations.
		my @probes = @{ $faileditems{$aid}{'probes'} };
		#print "aid $aid => " . scalar(@probes) . " items in probes array\n";
		# get non-probe info from front of array
		my $sourcenr = shift(@probes);
		my $nrp = shift(@probes);
		my $avglr = shift(@probes);
		my $chiptypeid = shift(@probes);
		my $chr = shift(@probes);
		#my $nrp = shift(@probes);
		my $astart = shift(@probes);
		my $astop = shift(@probes);
		# sort and get min-max position
		@probes = sort(@probes);
		my $first = $probes[0];
		my $last = $probes[$#probes];
		## if to be accepted, all probes retrieved should be in the @probes array, in the same order. 
		## this would mean that the region is still covered by just these probes, in the same order, on the new array.
		## if probes were deleted, they are deleted both in the @probes and database. 
		## in this case, check how many are deleted (accepted maximum : 5). (this is checked first for performance)
		## NOTE: this check is over the probes in actual cnv region + 250Kb 5' and 3' . 
        
        ## REVISED 
        ##   - if region is within 20% deviation from original size; accept lift based on start/end probe lift. 
        
        # get start/end position of the lifted probes
        my $newstart = $poshash{"$chr-$astart"};
        my $newstart_full = $poshash{"$chr-$astart"};
        my $newstop = $poshash{"$chr-$astop"};
        my $newstop_full = $poshash{"$chr-$astop"};
        $newstart =~ s/(\d+)-(\d+)/$2/;
		$newstop =~ s/(\d+)-(\d+)/$2/;
        # missing probes on border : move in
        if ($newstart eq "") {
            ## get probes. 
            #print("Border probe did not lift, querying region for first lifted position.")
		    my $psth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND chiptype = '$chiptypeid' AND position BETWEEN $first AND $last ORDER BY position LIMIT 1");
		    my $prows = $psth->execute();
            my @prow = $psth->fetchrow_array();
            $newstart = $prow[0];
        }
        if ($newstop eq "") {
            #print("Border probe did not lift, querying region for last lifted position.")
		    my $psth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND chiptype = '$chiptypeid' AND position BETWEEN $first AND $last ORDER BY position DESC LIMIT 1");
		    my $prows = $psth->execute();
            my @prow = $psth->fetchrow_array();
            $newstop = $prow[0];
        }
        if ($astop == $astart) {
            # increment to prevent division by zero
            print("zero size cnv ? : $astart - $astop ; aid = $aid\n");
            $astop++;
        }
        if (($newstop-$newstart) / ($astop-$astart) >= 0.8 && ($newstop-$newstart) / ($astop-$astart) <= 1.2) {
            $rescued++;
            #print("rescueing $aid : $chr:$astart-$astop as $chr:$newstart-$newstop  : reason was : ".$faileditems{$aid}{'reason'}."\n");

        }
        else {
            print("Size difference is too big : $chr:$astop-$astart vs $chr:$newstop-$newstart   : ". ($newstop-$newstart) / ($astop-$astart) . " \n");
            next;
        }

        # get largestart based on lifted probe positions.
		my $newlstart = 0;
		my $newlstop = 'ter';
		my $lsth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND position < '$newstart' AND chiptype = '$chiptypeid' ORDER BY position DESC LIMIT 1");
		my $nr = $lsth->execute();
		if ($nr != 0) {
			my @r = $lsth->fetchrow_array();
			$newlstart = $r[0];
		}
		$lsth->finish();
		$lsth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND position > '$newstart' AND chiptype = '$chiptypeid' ORDER BY position ASC LIMIT 1");
		$nr = $lsth->execute();
		if ($nr != 0) {
			my @r = $lsth->fetchrow_array();
			$newlstop = $r[0];
		}
		$lsth->finish();

		## got needed info. move and update from deleted to cnv
        ## data for loh is not stored in deleted_loh, get straight from source db.
		my $dsth = $dbh->prepare("SELECT cn, sample, idproj, inheritance, class, seenby, validation, validationdetails, pubmed FROM `$longsourcedb`.`aberration_LOH` WHERE id = '$aid'");
		$dsth->execute();
		my @r = $dsth->fetchrow_array();
		my $cn = $r[0];
		my $sample = $r[1];
		my $pid = $r[2];
		my $inh = $r[3];
		my $dc = $r[4];
		my $sb = $r[5];
		my $val = $r[6];
		my $vald = $r[7];
		my $pm = $r[8];
		my $size = $newstop - $newstart + 1;
		$dsth->finish();
        print("Inserting into aberration LOH : $aid with start $newstart and stop $newstop\n");
		my $isth = $dbh->prepare("INSERT INTO `$db`.`aberration_LOH` (id, cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, seenby, nrsnps, size, LiftedFrom, validation, validationdetails, pubmed, avgLogR) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		$isth->execute($aid,$cn,$newstart,$newstop,$chr,$newlstart,$newlstop,$sample,$pid,$inh,$dc,$sb,$nrp,$size,$sourcedb,$val,$vald,$pm,$avglr);
		$isth->finish(); 
		#$dbh->do("DELETE FROM `$db`.`deleted_LOH` WHERE id = $aid");
		$dbh->do("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE ID = '$failedid'");  
		#print "Rescued Region chr$chr:$astart-$astop as chr$chr:$newstart-$newstop\n";
	}
    print "Rescued $rescued items\n";
} 

sub LiftBAFSEG {
	print "\n";
	print "#######################\n";
	print "## Processing BAFSEG ## \n";
	print "#######################\n";
	## copy contents to new database
	$dbh->do("TRUNCATE TABLE `$db`.BAFSEG");
	$dbh->do("INSERT INTO `$db`.BAFSEG SELECT * FROM `$longsourcedb`.BAFSEG");
	## get content
	my $sth = $dbh->prepare("SELECT idsamp, idproj, resultlist,LiftedFrom FROM `$db`.BAFSEG");
	my $rowcache = ();
	my $rows = $sth->execute();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	print "nr of items to do: $rows\n";
	## open file output
	my $counter = 0;
	my $tresh = 10;
	my $usth = $dbh->prepare("UPDATE `$db`.BAFSEG SET resultlist = ? , LiftedFrom = ? WHERE idsamp = ? AND idproj = ?");
    my $psth = $dbh->prepare("SELECT chromosome, position FROM `$db`.probelocations WHERE `name` = ?");
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		$counter++;
		if ($counter > ($tresh * ($rows / 100))) {
			#print "#######################\n";
			print "$tresh% ... ";
			#print "#######################\n";
			$tresh = $tresh + 10;
		}
		my $sid = $result->[0];
		my $pid = $result->[1];
		my $data = $result->[2];
		my $lf = $result->[3];
		if ($lf eq '') {
			$lf = $sourcedb;
		}
		## split data to array
		my @lines = split(/\n/,$data);
        
		if (scalar(@lines) < 2) {
			## just header line or no lines. 
			$dbh->do("UPDATE `$db`.BAFSEG SET LiftedFrom = '$lf' WHERE idsamp = '$sid' AND idproj = '$pid'");
			next;
		}
		my $newout = shift(@lines) . "\n";
		foreach (@lines) {
			my @p = split(/\t/,$_);
            my $pnr = $psth->execute($p[4]);
            if ($pnr == 0) {
                next;
            }
            my @prow = $psth->fetchrow_array();
            my $newchr = $prow[0];
            my $newstart = $prow[1];
			#my $newstart = $poshash{$p[1]."-".$p[2]};
			if ($newstart eq '') {
				#print "Start probe not present in new build ($p[4] : $p[1]-$p[2]\n";
				next;
			}
			#$newstart =~ m/(\d+)-(\d+)/;
			#$newchr = $1;
			#$newstart = $2;
			if ($newchr != $p[1] ) {
				#print "Start Probe mapped to new Chromosome  ($p[4] : $p[1]-$p[2]\n";
				next;
			}
			#my $newstop = $poshash{$p[1]."-".$p[3]};
            $pnr = $psth->execute($p[5]);
            if ($pnr == 0) {
                next;
            }
            @prow = $psth->fetchrow_array();
            $newchr = $prow[0];
            my $newstop = $prow[1];
			if ($newstop eq '') {
				#print "Stop probe not present in new build ($p[5] : $p[1]-$p[3]\n";
				next;
			}
			#$newstop =~ m/(\d+)-(\d+)/;
			#$newchr = $1;
			#$newstop = $2;
			if ($newchr != $p[1] ) {
				#print "Stop Probe mapped to new Chromosome  ($p[4] : $p[1]-$p[3]\n";
				next;
			}
			## add to update string
			$size = $newstop - $newstart + 1;
			$newout .= "$p[0]\t$newchr\t$newstart\t$newstop\t$p[4]\t$p[5]\t$p[6]\t$p[7]\t$p[8]\t$size\n";
		}	
		$usth->execute($newout,$lf,$sid,$pid);	
	}
	$usth->finish();
	$sth->finish();
	print "Done\n";
}



sub LiftParentsDatapoints {
	my $table = 'parents_datapoints' ;
	print "\n";
	print "###################################\n";
	print "## Processing PARENTS DATAPOINTS ## \n";
	print "###################################\n";
	## get content and other needed info on cnvs
	## need to get chiptype...
	$sth = $dbh->prepare("SELECT a.chr, a.id, a.avgLogR, a.sid, a.start, a.stop, p.chiptypeid FROM `$longsourcedb`.parents_regions a JOIN `$longsourcedb`.projsamp ps JOIN `$longsourcedb`.project p ON p.id = ps.idproj AND ps.idsamp = a.sid");
		#$esth = $dbh->prepare("SELEC T");
	my $rowcache = ();
	my $rows = $sth->execute();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	## create list of failed cnv lifts for autocorrection.
	my %faileditems = ();
	if ($autocorrect == 1) { 
		print "Getting failed items\n";
		my $failed = $dbh->prepare("SELECT f.itemID,f.ID,p.sid FROM `GenomicBuilds`.`FailedLifts` f JOIN `$longsourcedb`.parents_regions p ON f.itemID = p.id WHERE f.frombuild = '$sourcedb'  AND f.tobuild = '$targetdb' AND f.tablename = 'parents_regions' ");
		my $srows = $failed->execute();
		my $scache = ();
		if ($srows < 10000) {
			$smax = $srows;
		}
		else {
		     $smax = 10000;
		}
		while (my $sresult = shift(@$scache) || shift( @{$scache = $failed->fetchall_arrayref(undef,$smax)|| []})) {
			$faileditems{$sresult->[0]}{'id'} = $sresult->[1];
			$faileditems{$sresult->[0]}{'sid'} = $sresult->[2];
		}
		$failed->finish();
	}
	print "nr of items to do: $rows\n";
	## open file output
	open OUT, ">$maintenancedir/LiftOver/Files/parents_datapoints.lift.tmp" or die("Could not open file for writing: $maintenancedir/LiftOver/Files/parents_datapoints.lift.tmp");
	print "processing actual parents_regions datapoints\n";
	my $counter = 0;
	my $tresh = 10;
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		$counter++;
		if ($counter > ($tresh * ($rows / 100))) {
			#print "#######################\n";
			print "$tresh%... ";
			#print "#######################\n";
			$tresh = $tresh + 10;
		}
        
	 	# a.chr, a.id, a.avgLogR, a.sid, a.start, a.stop
		my $chr = $result->[0];
		my $lf = $sourcedb;
		my $id = $result->[1];
        
		my $sid = $result->[3];
		my $astart = $result->[4];
		my $astop = $result->[5];
        my $chipid = $result->[6];
		## get chiptype
		#my $chiptype = '';
		#my $substh = $dbh->prepare("SELECT father, father_project, mother, mother_project FROM `$db`.parents_relations WHERE father = '$sid' OR mother = '$sid'");
		#$substh->execute();
		#while (my @subrow = $substh->fetchrow_array()) {
		#	
		#}
		# get datapoints 
		my $dsth = $dbh->prepare("select content, LiftedFrom FROM `$longsourcedb`.parents_datapoints WHERE id = $id");
		$dsth->execute();
		my @dr = $dsth->fetchrow_array();
		$dsth->finish();
		my $content = $dr[0];
		if ($dr[1] != '') {
			$lf = $dr[1];
		}
		# no content, print out and skip
		if ($content eq '' || $content eq 'NA') {
			#print "$id : No datapoints available\n";
			print OUT "$id\tNA\t$lf\n";
			next;
		}
		# split content
		my @items = split(/_/,$content);
		my $nr = scalar(@items);
		my $newcontent = '';
		my @probes = ();
		push @probes, $chipid;
		push @probes, $chr;
		#push @probes, $nrsnps;
		push @probes, $astart;
		push @probes, $astop;
		my $sum = 0;
		my $nrinregion = 0;
		my $nrtotal = 0;
		my $sourcenr = 0;
		for ($i = 0; $i < $nr ; $i = $i + 3) {
			$sourcenr++; # count all
			my $pos = $items[$i];
			my $lr = $items[$i+1];
			my $baf = $items[$i+2];
			my $newpos = $poshash{"$chr-$pos"};
			if ($newpos eq '') {
				print "Probe chr$chr:$pos not present in new build (from aid : $id)\n";
				## probe missing in new coordinates
				next;
			}  
			$newpos =~ m/(\d+)-(\d+)/;
			$newchr = $1;
			$newpos = $2;
			if ($newchr != $chr) {
				#print "probe chr$chr:$pos mapped to chr$newchr:$newpos (different chromosome)\n";
				## probe remapped to other chr. 
				next;
			}
			if ($pos >= $astart && $pos <= $astop) {
				$sum = $sum + $lr;
				$nrinregion++;
			}
			$nrtotal++;
			$newcontent .= "$newpos"."_".$lr."_"."$baf"."_";
			push @probes, $newpos;
		}
		#if ($nrtotal + 5 < $sourcenr) {
		#	my $diff = $sourcenr - $nrtotal;
		#	#print "$id : chr$chr:$astart-$astop : $diff probes missing\n";
		#}
		my $avglr = 0;
		if ($nrinregion > 0) {
			$avglr = $sum / $nrinregion;
		}
		## store last items		
		unshift @probes, $avglr;
		unshift @probes, $nrtotal;
		unshift @probes, $sourcenr;
		print OUT "$id\t$newcontent\t$lf\n";	
		## if this cnv failed to lift, store probes for further analysis.
		if (exists $faileditems{$id}) {
			my $nrpr = scalar(@probes);
			#print "Storing $nrpr probes for aid = $id\n";
			$faileditems{$id}{'probes'} = [ @probes ];
		}
			
	}
	$sth->finish();
	print "Done\n";
	close OUT;
	## load output file to database
	print "Loading to database\n";
    print("   - truncate\n");
	$dbh->do("TRUNCATE TABLE `$db`.`parents_datapoints`");
    print("   - load\n");
	$dbh->do("LOAD DATA LOCAL INFILE '$maintenancedir/LiftOver/Files/parents_datapoints.lift.tmp' INTO TABLE `$db`.`parents_datapoints`");
    unlink("$maintenancedir/LiftOver/Files/parents_datapoints.lift.tmp");
    #store \%faileditems, "/home/webstoredev/failedparents.hash";
    #%faileditems = %{retrieve("/home/webstoredev/failedparents.hash")};
    my $rescued = 0;
	## check failed items
	print "Processing failed items\n";
	foreach (keys(%faileditems)) {
		my $aid = $_;
		my $failedid = $faileditems{$aid}{'id'};
		if (!exists $faileditems{$aid}{'probes'}) {
			#print "No probes present for $aid\n";
			# no probes present, skip
			next;
		}
        
		## array with probe locations.
		my @probes = @{$faileditems{$aid}{'probes'}};
		# get non-probe info from front of array
		my $sid = $faileditems{$aid}{'sid'};
		my $sourcenr = shift(@probes);
		my $nrp = shift(@probes);
		my $avglr = shift(@probes);
		my $chiptypeid = shift(@probes);
		my $chr = shift(@probes);
		#my $nrp = shift(@probes);
		my $astart = shift(@probes);
		my $astop = shift(@probes);
		# sort and get min-max position
		@probes = sort(@probes);
		my $first = $probes[0];
		my $last = $probes[$#probes];
        ## REVISED 
        ##   - if region is within 10% deviation from original size, with a max of 250kb ; accept lift based on start/end probe lift. 
        
        # get start/end position of the lifted probes
        my $newstart = $poshash{"$chr-$astart"};
        my $newstart_full = $poshash{"$chr-$astart"};
        my $newstop = $poshash{"$chr-$astop"};
        my $newstop_full = $poshash{"$chr-$astop"};
        $newstart =~ s/(\d+)-(\d+)/$2/;
		$newstop =~ s/(\d+)-(\d+)/$2/;

        # missing probes on border : move in
        if ($newstart eq "") {
            
            ## get probes. 
            #print("Border probe did not lift, querying region for first lifted position.");
		    my $psth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND chiptype = '$chiptypeid' AND position BETWEEN $first AND $last ORDER BY position LIMIT 1");
		    my $prows = $psth->execute();
            my @prow = $psth->fetchrow_array();
            $newstart = $prow[0];
        }
        if ($newstop eq "") {
            #print("Border probe did not lift, querying region for last lifted position.");
		    my $psth = $dbh->prepare("SELECT position FROM `$db`.`probelocations` WHERE chromosome = '$chr' AND chiptype = '$chiptypeid' AND position BETWEEN $first AND $last ORDER BY position DESC LIMIT 1");
		    my $prows = $psth->execute();
            my @prow = $psth->fetchrow_array();
            $newstop = $prow[0];
        }
        if ($astop == $astart) {
            # increment to prevent division by zero
            print("zero size cnv ? : $astart - $astop ; aid = $aid\n");
            $astop++;
        }
        if (($newstop-$newstart) / ($astop-$astart) >= 0.9 && ($newstop-$newstart) / ($astop-$astart) <= 1.1  && abs(($newstop-$newstart) - ($astop-$astart)) <= 250000) {
            $rescued++;
            #print("rescueing $aid : $chr:$astart-$astop as $chr:$newstart-$newstop  : reason was : ".$faileditems{$aid}{'reason'}."\n");

        }
        else {
            #print("Size difference is too big : $chr:$astop-$astart vs $chr:$newstop-$newstart   : ". ($newstop-$newstart) / ($astop-$astart) . " \n");
            next;
        }
        
		my $isth = $dbh->prepare("INSERT INTO `$db`.`parents_regions` (id, chr, start, stop, sid, avgLogR, LiftedFrom) VALUES (?,?,?,?,?,?,?)");
		$isth->execute($aid,$chr,$newstart,$newstop,$sid,$avglr,$sourcedb);
		$isth->finish(); 
		$dbh->do("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE ID = '$failedid'");  
		#print "Rescued aid $aid (chr$chr:$astart-$astop => chr$chr:$newstart-$newstop)\n";
	}
    print "Rescued $rescued items\n";
} 

sub LiftCusDatapoints {
	my $table = 'cus_datapoints';
	print "\n";
	print "##################################\n";
	print "## Processing Custom DATAPOINTS ## \n";
	print "##################################\n";
	## get content and other needed info on cnvs
	$sth = $dbh->prepare("SELECT id, content, LiftedFrom, structure FROM `$longsourcedb`.`cus_datapoints`");
	#$sth = $dbh->prepare("SELECT a.chr,  a.id, p.chiptypeid, a.nrsnps, a.start, a.stop FROM `$longsourcedb`.aberration a JOIN `$longsourcedb`.project p ON p.id = a.idproj");
	my $rowcache = ();
	my $rows = $sth->execute();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	#	print "nr of items to do: $rows\n";
	## open file output
	open OUT, ">$maintenancedir/LiftOver/Files/cus_datapoints.lift.tmp";
	print "processing actual custom aberrations datapoints\n";
	my $counter = 0;
	my $tresh = 10;
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		$counter++;
		if ($counter > ($tresh * ($rows / 100))) {
			#print "#####################\n";
			print "$tresh%... ";
			#print "#####################\n";
			$tresh = $tresh + 10;
		}
		my $aid = $result->[0];
		my $content = $result->[1];
		my $lf = $result->[2];
		if ($lf eq '') {
			$lf = $sourcedb;
		}
		my $struct = $result->[3];
		
		## cnv details
		my $csth = $dbh->prepare("SELECT chr, start, stop, nrprobes FROM cus_aberration WHERE id = '$aid'"); 
		$csth->execute();
		my @crow = $csth->fetchrow_array();
		$csth->finish();
		
		my $chr = $crow[0];
		#my $chipid = $result->[2];
		my $nrsnps = $result->[3];
		my $astart = $crow[1];
		my $astop = $crow[2];
		# no content, print out and skip
		if ($content eq '') {
			#print "$id : No datapoints available\n";
			print OUT "$id\t\t$lf\n";
			next;
		}
		# split content
		my @items = split(/_/,$content);
		my $nr = scalar(@items);
		my $newcontent = '';
		my @probes = ();
		#push @probes, $chipid;
		push @probes, $chr;
		push @probes, $nrsnps;
		push @probes, $astart;
		push @probes, $astop;
		my $sum = 0;
		my $nrinregion = 0;
		my $nrtotal = 0;
		my $sourcenr = 0;
		# define data structure
		my @structure = split(//,$struct);
		my $cols = 0;
		$cols += $_ for @structure;
		$structure[1] == 1 ? $takeavg = 1 : $takeavg = 0;
		for ($i = 0; $i < $nr ; $i = $i + $cols) {
			$sourcenr++; # count all
			my $pos = $items[$i];
			## need lifting !
			my ($liftres,$reason,$newchr, $newpos, $newstop) = &Lift($chr, $pos, ($pos + 1),$aid,'cus_datapoints');  
			if ($liftres == 0) {
				#lift failed.
				next;
			}
			if ($newchr != $chr) {
				#print "probe chr$chr:$pos mapped to chr$newchr:$newpos (different chromosome)\n";
				## probe remapped to other chr. 
				next;
			}
			if ($pos >= $astart && $pos <= $astop) {
				if ($takeavg == 1) {
					$sum = $sum + $lr;
				}
				$nrinregion++;
			}
			$nrtotal++;
			## add to content
			$newcontent .= $newpos . "_";
			for ($j = 1; $j < $cols; $j++) {
				$newcontent .= $items[($i + $j)] ."_";
			}
			push @probes, $newpos;
		}
		my $avglr = 0;
		if ($takeavg == 1) {
			my $avglr = 0;
			if ($nrinregion > 0) {
				$avglr = $sum / $nrinregion;
			}
		}
		## store last items		
		unshift @probes, $avglr;
		unshift @probes, $nrtotal;
		unshift @probes, $sourcenr;
		print OUT "$id\t$newcontent\t$lf\n";	
		## if this cnv failed to lift, store probes for further analysis.
		if (exists $faileditems{$id}) {
			my $nrpr = scalar(@probes);
			$faileditems{$id}{'probes'} = [ @probes ] ;
		}
			
	}
	$sth->finish();
	close OUT;
	print "Done\n";
	## load output file to database
	print "Loading to database\n";
	$dbh->do("TRUNCATE TABLE `$db`.`cus_datapoints`");
	$dbh->do("LOAD DATA LOCAL INFILE '$maintenancedir/LiftOver/Files/cus_datapoints.lift.tmp' INTO TABLE `$db`.`cus_datapoints`");
    unlink("$maintenancedir/LiftOver/Files/cus_datapoints.lift.tmp");

}
 

sub LiftUPDDatapoints {
	my $table = 'parents_upd_datapoints' ;
	print "\n";
	print "###################################\n";
	print "## Processing UPD DATAPOINTS ## \n";
	print "###################################\n";
	## get content and other needed info on cnvs
	## need to get chiptype...
	$sth = $dbh->prepare("SELECT d.id,d.sid,d.content,d.LiftedFrom, u.chr, u.start, u.stop, u.nrprobes  FROM `$longsourcedb`.parents_upd_datapoints d JOIN `$longsourcedb`.parents_upd u ON d.id = u.id");
	#$esth = $dbh->prepare("SELEC T");
	my $rowcache = ();
	my $rows = $sth->execute();
	if ($rows < 10000) {
		$max = $rows;
	}
	else {
	     $max = 10000;
	}
	print "nr of items to do: $rows\n";
	## open file output
	open OUT, ">$maintenancedir/LiftOver/Files/upd_datapoints.lift.tmp";
	print "processing actual parents_upd datapoints\n";
	my $counter = 0;
	my $tresh = 10;
	while (my $result = shift(@$rowcache) || shift( @{$rowcache = $sth->fetchall_arrayref(undef,$max)|| []})) {
		$counter++;
		if ($counter > ($tresh * ($rows / 100))) {
			#print "#######################\n";
			print "$tresh%... ";
			#print "#######################\n";
			$tresh = $tresh + 10;
		}
		#d.id,d.sid,d.content,d.LiftedFrom, u.chr, u.start, u.stop, u.nrprobes
		my $aid = $result->[0];
		my $chr = $result->[4];
		my $lf = $result->[3];
		if ($lf eq '') {
			$lf = $sourcedb;
		}
		my $content = $result->[2];
		my $sid = $result->[1];
		my $astart = $result->[5];
		my $astop = $result->[6];
		my $nrprobes = $result->[7];
		# no content, print out and skip
		if ($content eq '' || $content eq 'NA') {
			#print "$id : No datapoints available\n";
			print OUT "$aid\t$sid\tNA\t$lf\n";
			next;
		}
		# split content
		my @items = split(/_/,$content);
		my $nr = scalar(@items);
		my $newcontent = '';
		my @probes = ();
		push @probes, $chipid;
		push @probes, $chr;
		#push @probes, $nrsnps;
		push @probes, $astart;
		push @probes, $astop;
		my $sum = 0;
		my $nrinregion = 0;
		my $nrtotal = 0;
		my $sourcenr = 0;
        # structure is pos ; logR ; GT : 1101
		for ($i = 0; $i < $nr ; $i = $i + 3) {
			$sourcenr++; # count all
			my $pos = $items[$i];
			my $lr = $items[$i+1];
			my $gt = $items[$i+2];
			my $newpos = $poshash{"$chr-$pos"};
			if ($newpos eq '') {
				#print "Probe chr$chr:$pos not present in new build\n";
				## probe missing in new coordinates
				next;
			}  
			$newpos =~ m/(\d+)-(\d+)/;
			$newchr = $1;
			$newpos = $2;
			if ($newchr != $chr) {
				#print "probe chr$chr:$pos mapped to chr$newchr:$newpos (different chromosome)\n";
				## probe remapped to other chr. 
				next;
			}
			if ($pos >= $astart && $pos <= $astop) {
				$sum = $sum + $lr;
				$nrinregion++;
			}
			$nrtotal++;
			$newcontent .= "$newpos"."_".$lr."_"."$gt"."_";
			push @probes, $newpos;
		}
		#if ($nrtotal + 5 < $sourcenr) {
		#	my $diff = $sourcenr - $nrtotal;
		#	#print "$id : chr$chr:$astart-$astop : $diff probes missing\n";
		#}
		my $avglr = 0;
		if ($nrinregion > 0) {
			$avglr = $sum / $nrinregion;
		}
		## store last items		
		unshift @probes, $avglr;
		unshift @probes, $nrtotal;
		unshift @probes, $sourcenr;
		print OUT "$aid\t$sid\t$newcontent\t1101\t$lf\n";	
		## if this cnv failed to lift, store probes for further analysis.
		if (exists $faileditems{$id}) {
			my $nrpr = scalar(@probes);
			#print "Storing $nrpr probes for aid = $id\n";
			$faileditems{$id}{'probes'} = @probes;
		}
			
	}
	$sth->finish();
	print "Done\n";
	close OUT;
	## load output file to database
	print "Loading to database\n";
	$dbh->do("TRUNCATE TABLE `$db`.`parents_upd_datapoints`");
	$dbh->do("LOAD DATA LOCAL INFILE '$maintenancedir/LiftOver/Files/upd_datapoints.lift.tmp' INTO TABLE `$db`.`parents_upd_datapoints`");
    unlink("$maintenancedir/LiftOver/Files/upd_datapoints.lift.tmp");
	## check failed items
	
}

sub Lift {
	# returns array of STATUS(0=failed,1=OK),REASON(if failed) or Chr,Start,Stop(if OK)
   	my($chr, $start, $stop,$id,$table) = @_;
	my $infile = "$maintenancedir/LiftOver/Files/LiftOver_$table.region.in";
	my $outOK = "$maintenancedir/LiftOver/Files/LiftOver_$table.region.outOK";
	my $outBAD = "$maintenancedir/LiftOver/Files/LiftOver_$table.region.outBAD";
	my $outMerged = "$maintenancedir/LiftOver/Files/LiftOver_$table.region.outMerged";
	open OUT, ">$infile";
	print OUT "chr$chr\t$start\t$stop\t$id";
	close OUT;
	system("cd  $maintenancedir/LiftOver/ && ./liftOver -minMatch=0.9 -multiple '$infile' '$chainfile' '$outOK' '$outBAD' > /dev/null 2>&1");
	if (-z $outOK) {
		# lift failed
		my $reason = `head -n 1 $outBAD`;
		chomp($reason);
		$reason =~ s/#//;
		$reason = "LiftOver Failed: $reason";
		#print "chr$chr:$start-$stop : $reason\n";
		return (0,$reason,'','','');
	}
	else {
		# lift ok
		system("cd  $maintenancedir/LiftOver/ && ./liftOverMerge -mergeGap=$Gap '$outOK' '$outMerged' && mv '$outMerged' '$outOK' > /dev/null 2>&1");
		open IN, "$outOK";
		my @lines = <IN>;
		if (scalar(@lines) > 1) {
			# region is split => Failed
			return(0,'LiftOver Failed: Region is split in new build','','','');
		}
		else {
			my $line = $lines[0];
			chomp($line);
			my @parts = split(/\t/,$line);
			$parts[0] =~ s/chr//;
			return(1,'',$parts[0],$parts[1],$parts[2]);
		}		
	}
}	


