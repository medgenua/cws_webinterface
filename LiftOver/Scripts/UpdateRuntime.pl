#!/usr/bin/perl
use Getopt::Std;
use DBI;
# read credentials
use Cwd 'abs_path';
$credpath = abs_path("../../.LoadCredentials.pl");
require($credpath);


$|++;
# DEFINE VARIABLES
my %chromhash = ();
my @chroms = (1..22);
foreach (@chroms)  {
	$chromhash{ $_ } = $_;
}
$chromhash{ "23" } = "X";
$chromhash{ "24" } = "Y";
$chromhash{ "25" } = "M";
$chromhash{ "X" } = "23";
$chromhash{ "XY" } = "23";
$chromhash{ "Y" } = "24";
$chromhash {"M" } = "25";
##########################
# COMMAND LINE ARGUMENTS #
##########################
# d = Source Database, mandatory.
# D = Target Database, mandatory.
# U = platform userid, needed for logfiles
# G = run the GC creation for QuantiSNP (
getopts('D:d:GU:', \%opts);  # option are in %opts

########################### 
# CHECK MANDATORY OPTIONS #
###########################
if (!$opts{'D'} || $opts{'D'} eq '') {
	die('Target Database is mandatory');
}
if (!$opts{'d'} || $opts{'d'} eq '') {
	die('Source Database is mandatory');
}
$sourcedb = "CNVanalysis" . $opts{'d'};
$targetdb = "CNVanalysis" . $opts{'D'};
$sourceshortname = $opts{'d'};
$targetshortname = $opts{'D'};
$sourceshortname =~ s/^-//;
$targetshortname =~ s/^-//;




############################## 
# CONNECT TO SOURCE DATABASE #
##############################
print "1/ Connecting to the Target database ($targetdb)\n";
$connectionInfocnv="dbi:mysql:$targetdb:$host"; 
$dbh = DBI->connect($connectionInfocnv,$userid,$userpass) or die('Could not connect to database') ;
print "\t=> Done\n";


print " THIS IS NOT SUPPORTED USING UI ROUTINES\n";
print " => MANUALLY CHECK ALL FILES UNDER $scriptdir/Analysis_Methods/Runtime/\n";
print " => UPDATE ALL that need to be updated (some code available in this script)\n";
print "\n\n";
$dbh->do("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step = '4a' WHERE frombuild ='-$sourceshortname' AND tobuild = '-$targetshortname'");
# change status for website
system("echo 0 > $sitedir/status/LiftOver.status");
exit();

###################
## GET chiptypes ##
###################
my %chips;
my $sth = $dbh->prepare("SELECT name, ID FROM `$sourcedb`.`chiptypes`");
$sth->execute();
while (my @row = $sth->fetchrow_array()) {
	print "found chip: $row[0]\n";
	$chips{$row[0]} = $row[1];
}

###################
## PennCNV Files ##
###################

## .hmm files => build independent, leave as is.

## .pfb files => update based on updated probelists. delete missing items. 
%links;
%deleted;
my %allprobes; # hash containing position of ALL probes in new build.
print "Loading ALL probes present in database into hash...\n";
my $sthp = $dbh->prepare("SELECT name,chromosome,position FROM probelocations");
$sthp->execute();
my $rows = $sthp->rows();
my $rowcache;
if ($rows < 10000) {
	$max = $rows;
}
else {
	$max = 10000;
}
while (my $aref = shift(@$rowcache) || shift( @{$rowcache = $sthp->fetchall_arrayref(undef,$max)|| []})) {
	$allprobes{$aref->[0]} = $chromhash{$aref->[1]} ."\t" . $aref->[2];
}
# process the chips
foreach(keys(%chips)) {
	my $chipname = $_;
	my $chipid = $chips{$chipname};
	## check if pfb file is a real file or a link
	my $filename = "$chipname.$sourceshortname.pfb";
	my $targetfile = "$chipname.$targetshortname.pfb";
	print "Processing $filename\n";
	if (!-l "$scriptdir/PennCNV/lib/$filename") {
		# is file, does target exist?
		if (-e "$scriptdir/PennCNV/lib/$targetfile") {
			print "  => $targetfile already exists, skipping this file.";
			next;
		}
	}
	else {	
		# should be link, read the link for target file.
		my $target = readlink "$scriptdir/PennCNV/lib/$filename" ;
		## store links, process last.
		$links{$filename} = $target;
		print "  => Link detected, processing last\n";
		next;
	}
	## update file
	open IN, "$scriptdir/PennCNV/lib/$filename";
	open OUT, ">$scriptdir/PennCNV/lib/$targetfile";
	my $head = <IN>;
	print OUT $head;
	print "  => Updating file\n";
	while (<IN>) {
		chomp($_);
		my @p = split(/\t/,$_);
		if (!exists($allprobes{$p[0]})) {
			# probe was deleted in lift (is missing in new build...)
			$deleted{$p[0]} = "($chipname) $_";
			next;
		}
		my $line = $p[0] ."\t".$allprobes{$p[0]}."\t".$p[3]."\n";
		print OUT $line;
	}
	close IN;
	close OUT;
	print "  => $filename Done.\n";
}


## Pfb LINKS 
my %doublelinks;

foreach(keys(%links)) {
	my $filename = $_;
	my $liftedfilename = $filename;
	$liftedfilename =~ s/$sourceshortname/$targetshortname/;
	print "Processing Link $filename\n";
	my $targetfile = $links{$_};
	if (-l "$scriptdir/PennCNV/lib/$targetfile") {
		print "  => Target is also link, process last";
		my $target = readlink "$scriptdir/PennCNV/lib/$targetfile";
		$doublelinks{$filename} = $target;
		next;
	}
	## target of link is file. Check if already updated.
	$liftedtarget = $targetfile;
	$liftedtarget =~ s/$sourceshortname/$targetshortname/;
	if (-e "$scriptdir/PennCNV/lib/$liftedtarget") {
		print "  => Link target is already lifted. Creating Link.\n";
		system("cd $scriptdir/PennCNV/lib && ln -s $liftedtarget $liftedfilename");
	}
	else {
		## update the target filename. 
		open IN, "$scriptdir/PennCNV/lib/$targetfile";
		open OUT, ">$scriptdir/PennCNV/lib/$liftedtarget";
		my $head = <IN>;
		print OUT $head;
		print "  => Updating the link target file ($targetfile)\n";
		while (<IN>) {
			chomp($_);
			my @p = split(/\t/,$_);
			if (!exists($allprobes{$p[0]})) {
				# probe was deleted in lift (is missing in new build...)
				$deleted{$p[0]} =  "($targetfile) $_";
				next;
			}
			my $line = $p[0] ."\t".$allprobes{$p[0]}."\t".$p[3]."\n";
			print OUT $line;
		}
		close IN;
		close OUT;
		## and create link
		system("cd $scriptdir/PennCNV/lib  && ln -s $liftedtarget $liftedfilename");
		print "  => $filename Done.\n";
	}

}
## clear variables and print overview for .pfb update.
%allprobes = ();
%links = ();
$nrdel = keys(%deleted)	;
print " ==> ALL .pfb FILES DONE\n";
print " Checking deleted items\n";
## check deleted items, were they used ?
open OUT, ">Failed.txt";
$sth = $dbh->prepare("SELECT chiptype FROM `$sourcedb`.`probelocations` WHERE name = ?");
my $realdeleted = 0;
foreach(keys(%deleted)) {
	$sth->execute($_);
	if ($sth->rows() > 0) {;
		print OUT "$_ =>$deleted{$_} \n";
		$realdeleted++;
	}
}
close OUT;
print "      => $nrdel items were deleted from the .pfb files, from which $realdeleted probes were actually used.\n";
%deleted = ();

## gc files. update using perl script.
foreach(keys(%chips)) {
	my $chipname = $_;
	my $chipid = $chips{$chipname};
	my $pfbfile = "$chipname.$targetshortname.pfb";
	## check if gc file is a real file or a link
	my $filename = "$chipname.$sourceshortname.gcmodel";
	my $targetfile = "$chipname.$targetshortname.gcmodel";
	print "Processing $targetfile\n";
	if (!-l "$scriptdir/PennCNV/lib/$filename") {
		# is file, does target exist?
		if (-e "$scriptdir/PennCNV/lib/$targetfile") {
			print "  => $targetfile already exists, skipping this file.";
			next;
		}
	}
	else {	
		# should be link, read the link for target file.
		my $target = readlink "$scriptdir/PennCNV/lib/$filename" ;
		## store links, process last.
		$links{$filename} = $target;
		print "  => Link detected, processing last\n";
		next;
	}
	## first check (and download) the UCSC gc-content file.
	my $file = "/tmp/gc5Base.$targetshortname.txt";
	if (!-e "$file") {
		print "Downloading UCSC GC-content file to: $file\n";
		# download
		system("wget -q -O '/tmp/gc5Base.txt.gz' http://hgdownload.cse.ucsc.edu/goldenPath/$targetshortname/database/gc5Base.txt.gz");
		# extract
		system("cd /tmp/ && gunzip -f gc5Base.txt.gz");
		# sort
		system("cd /tmp/ && sort -k 2,2 -k 3,3n <gc5Base.txt > tmp.txt && mv tmp.txt $file");
	} 
	## update file
	system("perl cal_gc_snp.pl --output '$scriptdir/PennCNV/lib/$targetfile' $file $scriptdir/PennCNV/lib/$pfbfile");
	print "  => $targetfile Done.\n";
}
## gc LINKS 
my %doublelinks;

foreach(keys(%links)) {
	my $filename = $_;
	my $liftedfilename = $filename;
	$liftedfilename =~ s/$sourceshortname/$targetshortname/;
	print "Processing Link $liftedfilename\n";
	my $targetfile = $links{$_};
	my $pfbfile = $targetfile;
	$pfbfile =~ s/gcmodel/pfb/;
	if (-l "$scriptdir/PennCNV/lib/$targetfile") {
		print "  => Target is also link, process last";
		my $target = readlink "$scriptdir/PennCNV/lib/$targetfile";
		$doublelinks{$filename} = $target;
		next;
	}
	## target of link is file. Check if already updated.
	$liftedtarget = $targetfile;
	$liftedtarget =~ s/$sourceshortname/$targetshortname/;
	if (-e "$scriptdir/PennCNV/lib/$liftedtarget") {
		print "  => Link target is already lifted. Creating Link.\n";
		system("cd $scriptdir/PennCNV/lib && ln -s $liftedtarget $liftedfilename");
	}
	else {
		## update the target filename. 
		my $file = "/tmp/gc5Base.$targetshortname.txt";
		if (!-e "$file") {
			print "Downloading UCSC GC-content file to: $file\n";
			# download
			system("wget -q -O '/tmp/gc5Base.txt.gz' http://hgdownload.cse.ucsc.edu/goldenPath/$targetshortname/database/gc5Base.txt.gz");
			# extract
			system("cd /tmp/ && gunzip -f gc5Base.txt.gz");
			# sort
			system("cd /tmp/ && sort -k 2,2 -k 3,3n <gc5Base.txt > tmp.txt && mv tmp.txt $file");
		} 
		system("perl cal_gc_snp.pl --output '$scriptdir/PennCNV/lib/$liftedtarget' $file $scriptdir/PennCNV/lib/$pfbfile");
		## and create link
		system("cd $scriptdir/PennCNV/lib  && ln -s $liftedtarget $liftedfilename");
		print "  => $liftedfilename Done.\n";
	}
}
system("cd /tmp/ && rm -f /tmp/gc5Base.$targetshortname.txt");

###############
## QUANTISNP ##
###############
## GC-content data
## this can be created with a easy script, but it takes a 6.6G file that needs to be parsed (eg hg19.gc5Base.txt). 
## this is why it is recommended to NOT generate these files on a production server.
## however, if not specified, run it from here.
if($opts{'G'}) {
	print "QuantiSNP GC data were specified by the site.\n";
}
else {
	system("cd /tmp/ && perl $sitedir/LiftOver/Scripts/create_1K.pl -b $targetshortname ");
	if (-d "/opt/QuantiSNP") {
		system("cp -Rf /tmp/gc.$targetshortname /opt/QuantiSNP/ && chmod -R 777 /opt/QuantiSNP/gc.$targetshortname");
	}
	if (-d "/opt/QuantiSNP2") {
		system("cp -Rf /tmp/gc.$targetshortname /opt/QuantiSNP2/ && chmod -R 777 /opt/QuantiSNP2/gc.$targetshortname");  
	}
	system("rm -Rf /tmp/gc.$targetshortname");
}
## no other variables need to be included. 

#################
## VANILLA ICE ##
#################
## Zeroed SNPs files contain only probe names => build independent
## no other variables need to be loaded. 


###############################  
## RUNTIME FILES ARE UPDATED ## 
###############################
# store this in database !
$dbh->do("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step = '4a' WHERE frombuild ='-$sourceshortname' AND tobuild = '-$targetshortname'");
# change status for website
system("echo 0 > $sitedir/status/LiftOver.status");

