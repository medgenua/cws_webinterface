<?php
#################################################
#  LIFTOVER STEP 1 : CREATING NEW DB		#
#	- ASK FOR NEW BUILD VERSION		#
#	- WHEN PROVIDED : CREATE DB		#
#	- COPY STRUCTURE OF ALL TABLES		#
#	- COPY CONTENT OF ALL STATIC TABLES	#
#						#
#	=> ACCESS TO OLD BUILD IN READ-ONLY	#
#################################################
echo "<div class=sectie>\n";
echo "<h3>LiftOver Step 1: Preparing the Database</h3>\n";
echo "<h4>And fill with build independent data</h4>\n";

// ASK FOR NEW BUILD VERSION 
if (!isset($_POST['newbuild']) ) {
	mysql_select_db('GenomicBuilds');
	$query = mysql_query("SELECT name, StringName, Description, size FROM CurrentBuild");
	$row = mysql_fetch_array($query);
	echo "<p>You will first need some information on the new genomebuild. This information will be used to give information back to the user (like the top of the menu on the left), and to point all scripts and programs to the right directories.The current values are listed below as a guideline.</p>\n";
	echo "<p><form action='index.php?page=admin_pages&amp;type=lift&amp;step=1' method=POST>\n";
	echo "<table cellspacing=0>\n";
	echo "<tr>\n";
	echo "<th class=topcellalt $firstcell>Item</th>\n";
	echo "<th class=topcellalt >Info</th>\n";
	echo "<th class=topcellalt>Old Value</th>\n";
	echo "<th class=topcellalt>New Value</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell class=specalt>Build (UCSC)</td>\n";
	echo "<td>This value should contain the UCSC notation of the new build. It is used to name the new database. No spaces or special characters, except for a hyphen as first character.</td>\n";
	echo "<td>".$row['name']."</td>\n";
	echo "<td><input type=text name=shortname size=30></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell class=specalt>Build (all)</td>\n";
	echo "<td>A Compact string containing the new Build name. It is used on reports and in the notation on the top left of the menu.</td>\n";
	echo "<td>".$row['StringName']."</td>\n";
	echo "<td><input type=text name=stringname size=30></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell class=specalt>Description</td>\n";
	echo "<td>Description is shown as tooltip when a users hovers the build-version on the left.</td>\n";
	echo "<td>".$row['Description']."</td>\n";
	echo "<td><input type=text name=description size=30></td>\n ";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell>ChainFile URL</td>\n";
	echo "<td>Chainfiles are provided by UCSC and are needed by the LiftOver tool. Provide the link to the correct file here.</td>\n";
	echo "<td><a href='http://hgdownload.cse.ucsc.edu/goldenPath/hg18/liftOver/' target='_blank'>Example files</a></td>\n";
	echo "<td><input type=text name=chainfile size=30></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell>Genomic size</td>\n";
	echo "<td>Provide the total size of this genome build. <a href='https://genomewiki.ucsc.edu/index.php?search=size+statistics&title=Special%3ASearch&profile=all&fulltext=1' target='_blank'>Try searching here</a></td>\n";
	echo "<td>".$row['size']."</td>\n";
	echo "<td><input type=text name=genomesize size=30></td>\n";
	echo "</tr>\n";

	echo "</table></p>\n";
	echo "<p class=bold>Proceeding here will put CNV-WebStore in a READ-ONLY mode !</p>\n";
	echo "<p><input type=submit name=newbuild class=button value=Next></form></p>";
	
	# POSSIBLY, THERE IS A LIFT IN PROGRESS, SHOW LINKS TO CONTINUE WITH THEM.
	$liftquery = mysql_query("SELECT frombuild, tobuild, step, statictables FROM `GenomicBuilds`.`LiftsInProgress`  WHERE frombuild = '".$_SESSION['dbname']."' ORDER BY step DESC");
	if (mysql_num_rows($liftquery) > 0) {
		echo "<p>LiftOver processes were started but left unfinished. You can continue them from the table below. You can only continue lifts that were initiated from the current build.</p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr><th class=topcellalt $firstcell>Source Build</th><th class=topcellalt>Target Build</th><th class=topcellalt>Current Phase</th><th class=topcellalt>Proceed</th></tr>\n";
		while ($row = mysql_fetch_array($liftquery)) {
			$from = $row['frombuild'];
			$to = $row['tobuild'];
			$step = $row['step'];
			$statictables = $row['statictables'];
			echo "<tr><td $firstcell>$from</td>";
			echo "<td>$to</td>";
			switch($step) {
				case "1":
					$stepstring = "1: Database and tables created.";
					$form ="<input type=submit class=button name=newbuild value='Go'><input type=hidden name=from value='$from'><input type=hidden name=to value='$to'><input type=hidden name=nextsubstep value=1>"; 
					$linkstep = 1;
					break;
				case "2":
					$stepstring = "2a: Static tables specified and filled";
					$form ="<input type=submit class=button name=newbuild value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=1><input type=hidden name=staticsfilled value=1>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}
					$linkstep = "2";
					break;
				case "2b":
					$stepstring = "2b: New Annotation URLs specfied";
					$form ="<input type=submit class=button name=filltables value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=2>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}
					$linkstep = "2";
					break;
				case "3":
					$stepstring = "3a: Annotation Tables Filled and Ready for Curation";
					$form ="<input type=submit class=button name='GoCurateTables' value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=3>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}

					$linkstep = 3;
					break;
				case "3b":
					$stepstring = "3b: Curating Lifted Probelocations";
					$form ="<input type=submit class=button name='LiftProbeLocPart3' value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=3>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}

					$linkstep = 3;
					break;
				case "3c":
					$stepstring = "3c: Adapting Runtime Files to new build";
					$form ="<input type=submit class=button name='LiftRunTimeFiles' value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=3>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}

					$linkstep = 3;
					break;
				case "4a":
					$stepstring = "4a: Lifting CNV regions to new build";
					$form ="<input type=submit class=button name='LiftCNVs' value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=4>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}

					$linkstep = 4;
					break;
				case "4b":
					$stepstring = "4b: Lifting Datapoints to new build";
					$form ="<input type=submit class=button name='LiftDataPoints' value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=4>";
					$tables = explode(',',$statictables);
					foreach($tables as $key => $tablename) {
						$form .= "<input type=hidden name='statictables[]' value='$tablename'>";
					}

					$linkstep = 4;
					break;
                case "5":
                        $stepstring = "5: Finalize";
                        $form ="<input type=submit class=button name='ToFinal' value='Go'><input type=hidden name=from value='$from'><input type=hidden name=shortname value='$to'><input type=hidden name=nextsubstep value=5>";
                        $tables = explode(',',$statictables);
                        foreach($tables as $key => $tablename) {
                            $form .= "<input type=hidden name='statictables[]' value='$tablename'>";
                        }
    
                        $linkstep = 5;
                        break;
			}
			echo "<td>$stepstring</td>";
			echo "<td><form action='index.php?page=admin_pages&amp;type=lift&step=$linkstep' method=POST>$form</form></td>";
			echo "</tr>";
		}
		
	}

}
// CREATE THE NEW DATABASE 
elseif (!isset($_POST['statictables'])) {
   if ($_POST['nextsubstep'] != '1') {
	# function to check chainfile url
	function is_valid_url ( $url )
	{
		$url = @parse_url($url);
		if ( ! $url) {
			echo "No URL specified... => ";
			return false;
		}
		$url = array_map('trim', $url);
		if ($url['scheme'] == 'http') {
			$url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
		}
        elseif($url['scheme'] == 'https') {
			$url['port'] = (!isset($url['port'])) ? 443 : (int)$url['port'];
		}
		elseif($url['scheme'] == 'ftp') {
			$url['port'] = (!isset($url['port'])) ? 21 : (int)$url['port'];
		}
		$path = (isset($url['path'])) ? $url['path'] : '';
		if ($path == ''){
			$path = '/';
		}
		$path .= ( isset ( $url['query'] ) ) ? "?$url[query]" : '';
		# case http link
		if ( ($url['scheme'] == 'http' || $url['scheme'] == 'https' ) && isset($url['host']))
		{
			$headers = get_headers("$url[scheme]://$url[host]:$url[port]$path");
			$headers = ( is_array ( $headers ) ) ? implode ( "\n", $headers ) : $headers;
			if (preg_match ( '#^HTTPS{0,1}/.*\s+[(200|301|302)]+\s#i', $headers )) {
				return true;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(404)+\s#i', $headers )) {
				echo "File Not Found on server... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(401)+\s#i', $headers )) {
				echo "Request needs username/password... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(402)+\s#i', $headers )) {
				echo "Request Denied... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(500|503)+\s#i', $headers )) {
				echo "Hostname unavailable... => ";
				return false;
			}

			else {
				echo "Failed for unclear reason... => ";
				return false;
			}
		}
		# case ftp link
		elseif ($url['scheme'] == 'ftp' && isset($url['host'])) {

	        	$server = $url['host']; 
	            	$user = $url['user']; 
	            	$pass = $url['pass']; 
       	            	if ((!$server) || (!$path)) { echo "No Host specified... => ";return false; }
	            	if (!$port) { $port = 21; }
	        	if (!$user) { $user = "anonymous"; }
            		if (!$pass) { $pass = "phpaccess@medgen.ua.ac.be"; }
            		switch ($url['scheme']) {
                		case "ftp":
                    			$ftpid = ftp_connect($server, $port);
                    			break;
                		case "ftps":
                    			$ftpid = ftp_ssl_connect($server, $port);
                    			break;
            		}
            		if (!$ftpid) { echo "Could not connect to server... => ";return false; }
            		$login = ftp_login($ftpid, $user, $pass);
            		if (!$login) { echo "Could not login...";return false; }
            		$ftpsize = ftp_size($ftpid, $path);
            		ftp_close($ftpid);
            		if ($ftpsize == -1) { 
				echo "File not found... => ";
				return false; 
			}
			else {
            			return true;
			}
        	}
		return false;
	}
	
	$shortname = $_POST['shortname'];
	$stringname = $_POST['stringname'];
	$descr = $_POST['description'];
	$chainfile = $_POST['chainfile'];
	$genomesize = $_POST['genomesize'];
	if ($shortname == '' || $stringname == '' || $descr == '' || $chainfile == '' || $genomesize == '') {
		echo "<h3>Insufficient information</h3>\n";
		echo "<p>You did not provide all needed information. Go back and try again</p>";
		exit(1);
	}
	if (!is_valid_url($chainfile)) {
		echo "<h3>Invalid ChainFile URL</h3>\n";
		echo "<p>The provided Chainfile URL could not be fetched. Please go back and check your link.</p>";
		exit(1);
	}
	
	if (!is_numeric($genomesize)) {
		echo "<h3>Invalid Genomic size</h3>\n";
		echo "<p>This should be an integer value. Please go back and try again.</p>";
		exit(1);
	}


	$dbselected = mysql_select_db("CNVanalysis$shortname");
	if ($dbselected) {
		echo "<h3>New Build Already exists</h3>\n";
		echo "<p>There already is a database for Build $shortname. Please check this or go back and specify the right name.</p>";
		exit(1);
	}
	flush();	
	# ok, we can proceed, the new database does not exist yet, store status
	mysql_select_db('GenomicBuilds');
	
	# try to fetch the chain file
	print "<span class=nadruk>Fetch Chainfile:</span> ";
	flush();
	$chainfilename = basename($chainfile);
	if (preg_match('/.gz$/',$chainfilename)) {
		$chainfilename = basename($chainfile,'.gz');
		$out = system("cd $maintenancedir/LiftOver && wget -q --no-check-certificate -O '$chainfilename.gz' '$chainfile' && gunzip '$chainfilename.gz' --force");
	}
	else {
		$out = system("cd $maintenancedir/LiftOver && wget -q -O '$chainfilename' '$chainfile'");
	}
	if (file_exists("$maintenancedir/LiftOver/$chainfilename")) {
		echo "OK<br/>";
		flush();
	}
	else {
		echo "<h3>ChainFile Could not be fetched</h3>\n";
		echo "<p>The download of the specified chainfile failed. Please check the URL and try again.</p>";
		exit(1);
	}
	# all seems good to go...

	# store info on new build db
	$descr = addslashes($descr);
	mysql_query("INSERT INTO NewBuild (name, StringName, Description, size) VALUES ('$shortname','$stringname', '$descr', '$genomesize') ");
	# create new db
	echo "<p><span class=nadruk>Creating Database: </span>";
	$createsql = "CREATE DATABASE IF NOT EXISTS `CNVanalysis$shortname`";
	mysql_query("$createsql");
	echo " OK<br>";
	echo "<span class=nadruk>Creating Tables:</span>";
	flush();
	###################################
	## RENDER CNV-WEBSTORE READ-ONLY ##
	###################################

	## backup permissions
	$currentdb = "CNVanalysis" . $_SESSION['dbname'];
	$sourcebuild = $_SESSION['dbname'];
	$sourcebuild = substr($sourcebuild,1);
	$targetbuild = substr($shortname,1);
	mysql_query("CREATE TABLE `GenomicBuilds`.`$sourcebuild"."To$targetbuild"."UserPermissions` LIKE `$currentdb`.`projectpermission`");
	mysql_query("INSERT INTO `GenomicBuilds`.`$sourcebuild"."To$targetbuild"."UserPermissions` SELECT * FROM `$currentdb`.`projectpermission`");
	mysql_query("CREATE TABLE `GenomicBuilds`.`$sourcebuild"."To$targetbuild"."cus_UserPermissions` LIKE `$currentdb`.`cus_projectpermission`");
	mysql_query("INSERT INTO `GenomicBuilds`.`$sourcebuild"."To$targetbuild"."cus_UserPermissions` SELECT * FROM `$currentdb`.`cus_projectpermission`");

	mysql_query("UPDATE `$currentdb`.`projectpermission` SET editcnv=0,editclinic=0,editsample=0 WHERE 1=1");
	mysql_query("UPDATE `$currentdb`.`cus_projectpermission` SET editcnv=0,editclinic=0,editsample=0 WHERE 1=1");
	mysql_query("UPDATE `GenomicBuilds`.`SiteStatus` SET status = 'LiftOver' WHERE 1 = 1");
	
	#######################
	## CREATE ALL TABLES ##
	#######################
	# get list of existing tables
	mysql_select_db($currrentdb);
	$query = mysql_query("SHOW TABLES IN `$currentdb`");
	## prepare the table to print
	$formstring = '';
	$formstring .= "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=1' method=POST>\n";
	$formstring .= "<input type=hidden name=newbuild value=1>\n";
	$formstring .= "<input type=hidden name=shortname value='$shortname'>\n";
	$formstring .= "<p><table cellspacing=0>\n";
	$formstring .= "<tr><th class=topcellalt $firstcell>Fill</th>\n";
	$formstring .= "<th class=topcellalt>Table Name</th>\n";
	$formstring .= "<th class=topcellalt>Info?</th>\n";
	$formstring .= "</tr>\n";
	# loop tables, create them.
	while($row = mysql_fetch_array($query)) {
		$tablename = $row[0];
		$sql = "SHOW CREATE TABLE `$currentdb`.`$tablename`";
		$subquery = mysql_query("$sql");
		$v = mysql_result($subquery,0,1);
		$comment = '';
		if($v) {
			$p = strpos($v,"COMMENT=");
			if($p) {
				$q = strpos($v,"/*",$p+8);
				if ($q) {
					$length = $q - $p -11;
					$comment = substr($v, $p + 9,$length);
				}
				else {
					$comment = substr($v,$p+9,-1);
				}
			}
		}
		if ($comment == '') {
			$comment = 'Table description not found';
		}
		if (preg_match('/independent/i',$comment)) {
			$checked = 'CHECKED';
		}
		else {
			$checked = '';
		}
		$createsql = "CREATE TABLE `CNVanalysis$shortname`.`$tablename` LIKE `$currentdb`.`$tablename`";
		mysql_query("$createsql");
		$formstring .= "<tr>\n";
		$formstring .= "<td $firstcell><input type=checkbox name=statictables[] value='$tablename' $checked></td>\n";
		$formstring .= "<td>$tablename</td>\n";
		$formstring .= "<td>$comment</td>\n";
		$formstring .= "</tr>\n";
	}
	$formstring .= "</table>";
	echo " OK<br>";
	# store chainfile url & name
	mysql_query("INSERT INTO LiftFiles (frombuild, tobuild, url, filename) VALUES ('".$_SESSION['dbname']."','$shortname','$chainfile','$chainfilename')");
	 
	# ok, all tables and database created, store status
	mysql_query("INSERT INTO LiftsInProgress (frombuild, tobuild, step) VALUES ('".$_SESSION['dbname']."','$shortname','1')"); 

	flush();
	echo "</p>\n";
	echo "<p>All table structures are created. Next, select the tables below that contain information independent of genomic build. These data will be transferred to the new database directly, the remaining tables will be be filled by Lifting Over the current data, or by supplying new sources for the data (in case of annotation tables).</p>";
	echo "$formstring";
	echo "</p><p><input type=submit class=button value=Next name=submit></form></p>\n";
   }
   # CONTINUE FROM PREVIOUS LIFT PROCESS
   else {
	$from = $_POST['from'];
	$to = $_POST['to'];
	$currentdb = "CNVanalysis" . $from;
	mysql_select_db($currrentdb);
	$query = mysql_query("SHOW TABLES IN `$currentdb`");
	$formstring = '';
	$formstring .= "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=1' method=POST>\n";
	$formstring .= "<input type=hidden name=newbuild value=1>\n";
	$formstring .= "<input type=hidden name=shortname value='$to'>\n";
	$formstring .= "<p><table cellspacing=0>\n";
	$formstring .= "<tr><th class=topcellalt $firstcell>Fill</th>\n";
	$formstring .= "<th class=topcellalt>Table Name</th>\n";
	$formstring .= "<th class=topcellalt>Info?</th>\n";
	$formstring .= "</tr>\n";
	while($row = mysql_fetch_array($query)) {
		$tablename = $row[0];
		$sql = "SHOW CREATE TABLE `$currentdb`.`$tablename`";
		$subquery = mysql_query("$sql");
		$v = mysql_result($subquery,0,1);
		$comment = '';
		if($v) {
			$p = strpos($v,"COMMENT=");
			if($p) {
				$q = strpos($v,"/*",$p+8);
				if ($q) {
					$length = $q - $p -11;
					$comment = substr($v, $p + 9,$length);
				}
				else {
					$comment = substr($v,$p+9,-1);
				}
			}
		}
		if ($comment == '') {
			$comment = 'Table description not found';
		}
		if (preg_match('/independent/i',$comment)) {
			$checked = 'CHECKED';
		}
		else {
			$checked = '';
		}
		$formstring .= "<tr>\n";
		$formstring .= "<td $firstcell><input type=checkbox name=statictables[] value='$tablename' $checked></td>\n";
		$formstring .= "<td>$tablename</td>\n";
		$formstring .= "<td>$comment</td>\n";
		$formstring .= "</tr>\n";
	}
	$formstring .= "</table>";
	echo "<p>The database and tables were prepared previously. Now select the tables below that contain information independent of genomic build. These data will be transferred to the new database directly, the remaining tables will be be filled by Lifting Over the current data, or by supplying new sources for the data (in case of annotation tables).</p>";
	echo "$formstring";
	echo "</p><p><input type=submit class=button value=Next name=submit></form></p>\n";
	
   }
			
}
# Fill static tables
elseif (isset($_POST['statictables'])) {
	# get needed variables
	$tables = $_POST['statictables'];
	$shortname = $_POST['shortname'];
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$newdb = "CNVanalysis".$shortname;
	if ($shortname == '') {
		echo "<h3>Insufficient information</h3>\n";
		echo "<p>New db name was lost, I can not proceed.</p>";
		exit(1);
	}
	# fill tables
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
	$nrtables = count($tables);
	echo "<p>Data are being copied from the old to the new database for all $nrtables static tables you have specified. Afterwards, proceed to the next step (providing update info for build-dependent tables).</p>";
	$currentidx = 0;
	if ($nrtables == 0) {
		echo "<p><span class=nadruk>No tables specified:</span> Continuing with the next step. </p>\n";
	}
	else {
		echo "<p>";
		$statics = '';
		foreach($tables as $key => $tablename) {
			$currentidx++;
			echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
			$statics .= $tablename.",";
			echo "<span class=bold>$currentidx / $nrtables : $tablename:</span> ";
			mysql_query("ALTER TABLE `$newdb`.`$tablename` DISABLE KEYS");
			mysql_query("INSERT INTO `$newdb`.`$tablename` SELECT * FROM `$currentdb`.`$tablename`");
			mysql_query("ALTER TABLE `$newdb`.`$tablename` ENABLE KEYS");
			echo "done.<br>\n";
			flush();
		}
		$statics = substr($statics,0,-1);
		mysql_query("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step='2',statictables='$statics' WHERE frombuild = '".$_SESSION['dbname']."' AND tobuild = '$shortname'");
		echo "</p>";
	}
	echo "<p>\n";
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	echo "<input type=submit class=button value=Proceed name=staticsfilled>\n";
	echo "</form></p>\n";
}
else {
	echo "<h3> No action provided</h3>\n";
	echo "<p>No info on what to do, this should not happen.</p>";
}
echo "</div>";
?>
