<?php
#################################################
#  LIFTOVER STEP 2 : Fill Annotation Tables	#
#	- PROVIDE NEW TABLE SOURCES		#
#	- TEST URLS FOR VALIDITY		#
#	- WHEN PROVIDED : UPDATE THE TABLES	#
#						#
#	=> ACCESS TO CURRENT BUILD REMAINS OPEN	#
#################################################
echo "<div class=sectie>\n";
echo "<h3>LiftOver Step 2: Fill Annotation Tables</h3>\n";
echo "<h4>By updating the scheduled scripts</h4>\n";

# First provide new table sources 
if (!isset($_POST['newsources']) && isset($_POST['staticsfilled'])) {
	$shortname = $_POST['shortname'];
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
	# pass on static tables
	$tables = $_POST['statictables'];	
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	# get urls to update
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$newdb = "CNVanalysis".$shortname;
	mysql_select_db($currentdb);
	$query = mysql_query("SELECT item, url, description, UpdateScript FROM Administration ORDER BY UpdateScript");
	$currentscript = '';
	echo "<p>Multiple annotation tracks on the platform are regularly updated automatically. To achieve this, the data are fetched from public servers. Please provide the new urls to these tables below for the new genomic build.</p>";
	echo "<p><span class=nadruk>Note:</span>If a specified table is not yet available for the new build, specify the url as 'LIFT' (without quotes). In this case, the old data will be lifted (so there is some info available for this annotation in the new build) but the table will NOT be updated untill you manually specify the new url when it comes available. Empty or invalid urls will be considered as unavailable and will be lifted.</p>\n";
	echo "<p><table cellspacing=0>\n";
		
	while ($row = mysql_fetch_array($query)) {
		$item = $row['item'];
		$url = $row['url'];
		$values = $row['values'];
		$description = $row['description'];
		$script = $row['UpdateScript'];
		if ($script != $currentscript) {
			if ($currentscript != '') {
				echo "</tr>\n";
			}
			echo "<tr>\n";
			echo "<th colspan=2 $firstcell>Script: $script ";
			echo "<span style='float:right'><input type=checkbox name='disable_$script' /> DISABLE</span>";
            echo "</th></tr>\n";
			$currentscript = $script;
		}
		echo "<tr>\n";
		echo "<th class=specalt $firstcell>Table name</th>\n";
		echo "<td ><span class=bold>$item</span></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=specalt $firstcell>Description</th>\n";
		echo "<td>$description</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=specalt $firstcell>Current URL</th>\n";
		echo "<td>$url</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=specalt $firstcell>New URL</th>\n";
		echo "<td><input type=text name='newurl_$item' size=70></td>\n";
		echo "</tr>\n";
	}
	echo "</table></p>\n";
	echo "<p><input type=submit class=button name=newsources value=Proceed>\n";
	echo "</form>\n";
	echo "</p>\n";

}

elseif (isset($_POST['newsources']) && !isset($_POST['filltables'])) {
	// new function to check urls later on...
	
	function is_valid_url ( $url )
	{
		if (preg_match('/lift/i',$url)) {
			echo "LIFT command specified... => ";
			return false;
		}
		$url = @parse_url($url);
		
		if ( ! $url) {
			echo "No URL specified... => ";
			return false;
		}
		$url = array_map('trim', $url);

		if ($url['scheme'] == 'http') {
			$url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
		}
        elseif ($url['scheme'] == 'https') {
			$url['port'] = (!isset($url['port'])) ? 443 : (int)$url['port'];
		}
		elseif($url['scheme'] == 'ftp') {
			$url['port'] = (!isset($url['port'])) ? 21 : (int)$url['port'];
		}
		$path = (isset($url['path'])) ? $url['path'] : '';
		if ($path == ''){
			$path = '/';
		}
		$path .= ( isset ( $url['query'] ) ) ? "?$url[query]" : '';
		# case http link
		if ( ($url['scheme'] == 'http' || $url['scheme'] == 'https') && isset($url['host']))
		{
			$headers = get_headers("$url[scheme]://$url[host]:$url[port]$path");
			$headers = ( is_array ( $headers ) ) ? implode ( "\n", $headers ) : $headers;
			if (preg_match ( '#^HTTPS{0,1}/.*\s+[(200|301|302)]+\s#i', $headers )) {
				return true;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(404)+\s#i', $headers )) {
				echo "File Not Found on server... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(401)+\s#i', $headers )) {
				echo "Request needs username/password... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(402)+\s#i', $headers )) {
				echo "Request Denied... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(500|503)+\s#i', $headers )) {
				echo "Hostname unavailable... => ";
				return false;
			}

			else {
				echo "Failed for unclear reason... => ";
				return false;
			}
		}
		# case ftp link
		elseif ($url['scheme'] == 'ftp' && isset($url['host'])) {

	        	$server = $url['host']; 
	            	$user = $url['user']; 
	            	$pass = $url['pass']; 
       	            	if ((!$server) || (!$path)) { echo "No Host specified... => ";return false; }
	            	if (!$port) { $port = 21; }
	        	if (!$user) { $user = "anonymous"; }
            		if (!$pass) { $pass = "phpaccess@medgen.ua.ac.be"; }
            		switch ($url['scheme']) {
                		case "ftp":
                    			$ftpid = ftp_connect($server, $port);
                    			break;
                		case "ftps":
                    			$ftpid = ftp_ssl_connect($server, $port);
                    			break;
            		}
            		if (!$ftpid) { echo "Could not connect to server... => ";return false; }
            		$login = ftp_login($ftpid, $user, $pass);
            		if (!$login) { echo "Could not login...";return false; }
            		$ftpsize = ftp_size($ftpid, $path);
            		ftp_close($ftpid);
            		if ($ftpsize == -1) { 
				echo "File not found... => ";
				return false; 
			}
			else {
            			return true;
			}
        	}
		return false;
	}
    
	# get posted variables
	$shortname = $_POST['shortname'];
	$tables = $_POST['statictables'];
	# set database names	
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$newdb = "CNVanalysis".$shortname;
	mysql_select_db($currentdb);
	# process entries
	echo "<p><span class=bold>Checking URLs:</span><ul id=ul-simple style='padding-left:15px'>\n";
	$query = mysql_query("SELECT item, url, description, UpdateScript FROM Administration ORDER BY UpdateScript");
	$formstring = '';
	$numrows = mysql_num_rows($query);
	$currrow = 0;
	while ($row = mysql_fetch_array($query)) {
		$currow++;
		$item = $row['item'];
		$itemrep = str_replace('.','_',$item);
		$description = stripslashes($row['description']);
		$script = $row['UpdateScript'];
		$url = $_POST["newurl_$itemrep"];
		echo "<li>$currow/$numrows: $item: ";
		flush();
        if (isset($_POST["disable_".str_replace(".","_",$script)])) {
            echo " disabled</li>";
            continue;
        }
        elseif (!is_valid_url($url)) {
			$oldurl = $row['url'];
			$formstring .= "<tr><th $firstcell colspan=2>$script : $item</th></tr>\n";
			$formstring .= "<tr><th $firstcell class=specalt>LiftOver</th><td><input type=checkbox name='lift_$item'><input type=hidden name='toconfirm[]' value='$item'><input type=hidden name='script_$item' value='$script'></td></tr>\n";
			$formstring .= "<tr><th $firstcell class=specalt>Old URL</th><td>$oldurl</td></tr>\n";
			$formstring .= "<tr><th $firstcell class=specalt>New URL</th><td><input type=text name='newurl_$item' size=70 value='$url'></td></tr>\n";
			echo " invalid</li>\n";
		}
		else {
			echo " valid</li>\n";
		}
		flush();
		$description = addslashes($description);
		mysql_query("INSERT INTO `$newdb`.`Administration` (item, description, UpdateScript, url) VALUES ('$item', '$description', '$script', '$url')");
	}
	
	if ($formstring != '') {
		# print form to confirm the incorrect urls.
		echo "<p>Some url were found to be invalid or missing. You <span class=nadruk>MUST</span> confirm for each of those invalid urls that you want them to be invalid. Otherwise correct the urls and try again. The data will be lifted from the previous build and automatic updates will be disabled for this track !</p>\n";
		echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
		# pass on static tables
		foreach($tables as $key => $tablename) {
			echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
		}
		# pass on password & build name
		echo "<input type=hidden name=shortname value='$shortname'></p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr><th style='text-align:center' $firstcell colspan=2>INVALID URLS FOUND</th></tr>\n";
		echo $formstring;
		echo "</table></p>\n";
		echo "<p><input type=submit class=button name=confirminvalid value='Retry'></p>";
		echo "</form>\n";
		echo "</p>";
	}
	else {
		// print form to continue to next stage: filling annotation tables
		echo "<p>All URLs were successfully validated. You can now proceed to loading the annotation tables.</p>\n";
		echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
		# pass on static tables
		foreach($tables as $key => $tablename) {
			echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
		}
		# UPDATE STAGE IN LiftsInProgress TABLE
		mysql_query("UPDATE `GenomicBuilds`.`LiftsInProgress` set step='2b' WHERE frombuild = '".$_SESSION['dbname'] ."' AND tobuild = '$shortname'");
		# pass on password & build name
		echo "<input type=hidden name=shortname value='$shortname'></p>\n";
		echo "<p><input type=submit class=button name=filltables value='Retry'></p>";
		echo "</form>\n";
		echo "</p>";
	}
}
//////////////////////////// 
// RECHECK URLs IF NEEDED //
////////////////////////////
elseif(isset($_POST['confirminvalid']) && !isset($_POST['filltables'])) {
	// new function to check urls later on...
	
	function is_valid_url ( $url )
	{
		if (preg_match('/lift/i',$url)) {
			echo "LIFT command specified... => ";
			return false;
		}
		$url = @parse_url($url);
		
		if ( ! $url) {
			echo "No URL specified... => ";
			return false;
		}
		$url = array_map('trim', $url);
		if ($url['scheme'] == 'http') {
			$url['port'] = (!isset($url['port'])) ? 80 : (int)$url['port'];
		}
        elseif ($url['scheme'] == 'https') {
			$url['port'] = (!isset($url['port'])) ? 443 : (int)$url['port'];
		}
		elseif($url['scheme'] == 'ftp') {
			$url['port'] = (!isset($url['port'])) ? 21 : (int)$url['port'];
		}
		$path = (isset($url['path'])) ? $url['path'] : '';
		if ($path == ''){
			$path = '/';
		}
		$path .= ( isset ( $url['query'] ) ) ? "?$url[query]" : '';
		# case http link
		if ( ( $url['scheme'] == 'http' || $url['scheme'] == 'https' ) && isset($url['host']))
		{
			$headers = get_headers("$url[scheme]://$url[host]:$url[port]$path");
			$headers = ( is_array ( $headers ) ) ? implode ( "\n", $headers ) : $headers;
			if (preg_match ( '#^HTTPS{0,1}/.*\s+[(200|301|302)]+\s#i', $headers )) {
				return true;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(404)+\s#i', $headers )) {
				echo "File Not Found on server... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(401)+\s#i', $headers )) {
				echo "Request needs username/password... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(402)+\s#i', $headers )) {
				echo "Request Denied... => ";
				return false;
			}
			elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(500|503)+\s#i', $headers )) {
				echo "Hostname unavailable... => ";
				return false;
			}
            // omim exception : 
            elseif (preg_match ( '#^HTTPS{0,1}/.*\s+(400)+\s#i', $headers ) && $url['host'] == 'api.omim.org' ) {
				trigger_error("OMIM api returns 400 without a valid symbol. this is allowed for ".$url['host']);
				return true;
			} 
			else {
				echo "Failed for unclear reason... => ";
				return false;
			}
		}
		# case ftp link
		elseif ($url['scheme'] == 'ftp' && isset($url['host'])) {
	        	$server = $url['host']; 
	            	$user = $url['user']; 
	            	$pass = $url['pass']; 
       	            	if ((!$server) || (!$path)) { echo "No Host specified... => ";return false; }
	            	if (!$port) { $port = 21; }
	        	if (!$user) { $user = "anonymous"; }
            		if (!$pass) { $pass = "phpaccess@medgen.ua.ac.be"; }
            		switch ($url['scheme']) {
                		case "ftp":
                    			$ftpid = ftp_connect($server, $port);
                    			break;
                		case "ftps":
                    			$ftpid = ftp_ssl_connect($server, $port);
                    			break;
            		}
            		if (!$ftpid) { echo "Could not connect to server... => ";return false; }
            		$login = ftp_login($ftpid, $user, $pass);
            		if (!$login) { echo "Could not login...";return false; }
            		$ftpsize = ftp_size($ftpid, $path);
            		ftp_close($ftpid);
            		if ($ftpsize == -1) { 
				echo "File not found... => ";
				return false; 
			}
			else {
            			return true;
			}
        	}
		return false;
	}

	# get posted variables
	$shortname = $_POST['shortname'];
	$tables = $_POST['statictables'];
	$toconfirm = $_POST['toconfirm'];
	# set database names	
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$newdb = "CNVanalysis".$shortname;
	mysql_select_db($currentdb);
	# process entries
	echo "<p><span class=bold>Re-Checking URLs:</span><ul id=ul-simple style='padding-left:15px'>\n";
	$query = mysql_query("SELECT item, url, description, UpdateScript FROM Administration ORDER BY UpdateScript");
	$formstring = '';
	$numrows = mysql_num_rows($query);
	$currrow = 0;
	$formstring = '';
	$nritems = count($toconfirm);
	$curitem = 0;
	foreach ($toconfirm as $key => $item) {
		$curitem++;
		$itemrep = str_replace('.','_',$item);
		//$item = $row['item'];
		$newurl = $_POST["newurl_$itemrep"];
		$script = $_POST["script_$itemrep"];
		echo "<li>$curitem/$nritems: ";
		# lift is confirmed
		if (isset($_POST["lift_$itemrep"])) {
			echo "LiftOver confirmed => OK";
			mysql_query("UPDATE `$newdb`.`Administration` SET url='LIFT' WHERE item = '$item'");
		}
		else{
			echo "New URL supplied : ";
			if (!is_valid_url($newurl)) {
				//$oldurl = $row['url'];
				$subquery = mysql_query("SELECT url FROM Administration WHERE item = '$item'");
				$row = mysql_fetch_array($query);
				$oldurl = $row['url'];
				$formstring .= "<tr><th $firstcell colspan=2>$script : $item</th></tr>\n";
				$formstring .= "<tr><th $firstcell class=specalt>LiftOver</th><td><input type=checkbox name='lift_$item'><input type=hidden name='toconfirm[]' value='$item'><input type=hidden name='script_$item' value='$script'></td></tr>\n";
				$formstring .= "<tr><th $firstcell class=specalt>Old URL</th><td>$oldurl</td></tr>\n";
				$formstring .= "<tr><th $firstcell class=specalt>New URL</th><td><input type=text name='newurl_$item' size=70 value='$newurl'></td></tr>\n";
				echo " invalid</li>\n";
			}
			else {
				echo " valid => OK</li>\n";
				mysql_query("UPDATE `$newdb`.`Administration` SET url='$newurl' WHERE item = '$item'");
			}
		}
		flush();
	}
	if ($formstring != '') {
		# print form to confirm the incorrect urls.
		echo "<p>Some url were still found to be invalid or missing. You <span class=nadruk>MUST</span> confirm for each of those invalid urls that you want them to be invalid. Otherwise correct the urls and try again. When lifted from the previous build, automatic updates will be disabled for this track !</p>\n";
		echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
		# pass on static tables
		foreach($tables as $key => $tablename) {
			echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
		}
		# pass on password & build name
		echo "<input type=hidden name=shortname value='$shortname'></p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr><th style='text-align:center' $firstcell colspan=2>INVALID URLS FOUND</th></tr>\n";
		echo $formstring;
		echo "</table></p>\n";
		echo "<p><input type=submit class=button name=confirminvalid value='Retry'></p>";
		echo "</form>\n";
		echo "</p>";
	}
	else {
		// print form to continue to next stage: filling annotation tables
		echo "<p>All URLs were successfully validated. You can now proceed to loading the annotation tables.</p>\n";
		echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=2' method=POST>\n";
		# pass on static tables
		foreach($tables as $key => $tablename) {
			echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
		}
		# UPDATE STAGE IN LiftsInProgress TABLE
		mysql_query("UPDATE `GenomicBuilds`.`LiftsInProgress` set step='2b' WHERE frombuild = '".$_SESSION['dbname'] ."' AND tobuild = '$shortname'");
		# pass on password & build name
		echo "<input type=hidden name=shortname value='$shortname'></p>\n";
		echo "<p><input type=submit class=button name=filltables value='Proceed'></p>";
		echo "</form>\n";
		echo "</p>";
	}
	
}
elseif (isset($_POST['filltables'])) {
	# get posted variables
	$shortname = $_POST['shortname'];
	$tables = $_POST['statictables'];
	# set database names	
	$targetdb = $shortname;
	$sourcedb = $_SESSION['dbname'];
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$newdb = "CNVanalysis".$shortname;
	mysql_select_db($currentdb);

	echo "<p><span id='runningspan'><span class=nadruk>Running updates:</span> ";
	echo "All the annotation tracks are now being updated. Once finished, there should appear a button here to continue.</span></p>";
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
	# pass on static tables
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'></p>\n";
	echo "<p><span id=continuebutton style='display:none;'><input type=submit class=button name=GoCurateTables value=Proceed></span></p>\n";
	echo "</form>";
	$query = mysql_query("SELECT DISTINCT(UpdateScript) FROM Administration");
	$rows = mysql_num_rows($query);
	$rownr = 0;
	while ($row = mysql_fetch_array($query)) {
		$rownr++;
		$script = $row['UpdateScript'];
		$spanname = str_replace('.pl','',$script);
		echo "<script type='text/javascript'>interval$rownr = setInterval(\"reloadspan('$spanname')\",10000)</script>\n";
		echo "<p><span class=bold>$rownr/$rows:  $script:</span><pre id='$spanname' class=scrollbarbox>Starting $script...</pre><hr>\n";
	}
	// get path.
    
	$output  = system("(echo 1 > status/LiftOver.status && sleep 5 && cd $maintenancedir/LiftOver/ && ./ToNewBuild.pl -d '$sourcedb' -D '$targetdb' -u $userid &) > $maintenancedir/LiftOver/Files/ToNewBuild.out 2>&1");
	flush();
	$rows = $rows + 1;
	echo "<script type='text/javascript'>interval$rows = setInterval(\"checkstatus($rows)\",10000)</script>\n";
	# print complete hidden form
	# proceed button is shown above the scroll boxes when status becomes 0.
}
echo "</div>";

?>
