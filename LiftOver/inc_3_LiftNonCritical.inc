<?php
#################################################
#  LIFTOVER STEP 3 : Lift Non Critical TABLES	#
#	- Lifting and curatation		#
#	- Will cost you some time !		#
#						#
#	=> ACCESS TO CURRENT BUILD REMAINS OPEN	#
#################################################
echo "<div class=sectie>\n";
echo "<h3>LiftOver Step 3: Lifting non Critical Tables</h3>\n";
echo "<h4>And Manually Curate failed lifts</h4>\n";
ob_end_flush();


## chromosome hash
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["25"] = "M";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["M"] = "25";


# Step 1: Were there any failed lifts from the annotation tables (step 2) 
# these will be presented on a per-table basis, one at a time, coming back here untill none remain.
if (isset($_POST['GoCurateTables'])) {
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
	# pass on static tables
	$tables = $_POST['statictables'];	
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	//////////////////////////////////
	// first process remapped items //
	//////////////////////////////////
	if (isset($_POST['remapped'])) {
		$remapped = $_POST['remapped'];
		$tablename = $_POST['workingtable'];
		foreach($remapped as $key => $itemid) {
			$newchr = $_POST["chr_$itemid"];
			$newchr = $chromhash[$newchr];
			$newstart = $_POST["start_$itemid"];
			$newstart = str_replace(",",'',$newstart);
			$newstop = $_POST["stop_$itemid"];
			$newstop = str_replace(",",'',$newstop);
			$query = mysql_query("SELECT column_name from information_schema.columns where table_name='$tablename' AND table_schema = '$currentdb'");
			$qstring = "INSERT INTO `$newdb`.`$tablename` (";
			$qstringbis = "VALUES (";
			while ($row = mysql_fetch_array($query)) {
				$col = $row['column_name'];
				if ($col == 'id' || $col == 'ID' || $col == 'Id') {
					continue;
				}
				if ($col == 'Start' || $col == 'start') {
					$qstring .= "$col,";
					$qstringbis .= "'$newstart',";
					continue;
				}
				if ($col == 'Stop' || $col == 'stop' || $col == 'End' || $col == 'end') {
					$qstring .= "$col,";
					$qstringbis .= "'$newstop',";
					continue;
				}
				if ($col == 'chr' || $col == 'Chr' || $col == 'chromosome') {
					$qstring .= "$col,";
					$qstringbis .= "'$newchr',";
					continue;
				}
				$qstring .= "$col,";
				$qs = mysql_query("SELECT $col FROM `$currentdb`.`$tablename` WHERE id = '$itemid'");
				$srow = mysql_fetch_array($qs);
				$qstringbis .= "'".$srow[$col]."',";
			} 
			$qstring = substr($qstring,0,-1);
			$qstringbis = substr($qstringbis,0,-1);
			$qstring = $qstring . ") $qstringbis)";
			#echo "qs: $qstring<br/>";
			$insq = mysql_query($qstring);
			$delq = mysql_query("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND uid = '$userid' AND itemID = '$itemid' AND tablename = '$tablename'"); 
		}	
	}	

	///////////////////////////////////
	// second process Curated items ///
	///////////////////////////////////
	if (isset($_POST['tabledone'])) {
		$tablename = $_POST['tabledone'];
		# get ids from current table
		$query = mysql_query("SELECT itemID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tablename' AND frombuild ='$oldshortname' AND tobuild = '$shortname' AND uid = '$userid'");
		$instring = '';
		while ($row = mysql_fetch_array($query)) {
			$instring .= $row['itemID'].",";
		}
		echo "tablename: $tablename<br/>";

		$instring = substr($instring,0,-1);
		#echo "instring $instring<br/>";	
		if ($tablename == 'encode') {
			$query = mysql_query("SELECT id, symbol as name, chr, start, end AS stop, coding as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
		}
		elseif ($tablename == 'genes') {
			$query =  mysql_query("SELECT id, g.symbol as name, g.chr, g.start, g.end AS stop, gs.synonyms as extra FROM `$currentdb`.`$tablename` g JOIN `$currentdb`.`genesum` gs ON g.symbol = gs.symbol WHERE g.id IN ($instring) ORDER BY g.chr, g.start");
		}
		elseif ($tablename == 'cytoBand')  {
			$query = mysql_query("SELECT id, name, chr, start, stop, gieStain as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
		}
		elseif ($tablename == 'segdup') {
			$query = mysql_query("SELECT id, id as name, chr, start, stop, CONCAT('Chr',otherchr,':',otherstart,'-',otherstop) as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
		}
		elseif ($tablename = 'DGV_full') {
			$query = mysql_query("SELECT id, study as name, chr, start, stop, CONCAT('gain:',gain,'-loss:',loss,'-inv:',inv) as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
		}
		while ($row = mysql_fetch_array($query)) {
			$itemid = $row['id'];
			$item = $row['name'];
			$chr = $row['chr'];
			$chrtxt = $chromhash[$chr];
			$start = $row['start'];
			$stop = $row['stop'];
			$region = "Chr".$chrtxt.":".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
			$extra = $row['extra'];
			#echo "testing $itemid $item $region<br/>";
			if ($_POST[$tablename."_".$itemid] == 'D') {
				#echo "in D<br/>";
				$squery = mysql_query("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND uid = '$userid' AND itemID = '$itemid' AND tablename = '$tablename'");
			}
			elseif ($_POST[$tablename."_".$itemid] == 'E') {
				#echo "in E<br/>";
				$toedit .= "<tr>";
				$toedit .= "<td $firstcell>$item</td><td>$region</td><td><input type=hidden name='remapped[]' value='$itemid'>Chr<input type=text name='chr_$itemid' size=2 value='$chrtxt'>:<input type=text name='start_$itemid' size=9 value='$start'>-<input type=text name='stop_$itemid' size=9 value='$stop'></td>";
				$toedit .= "</tr>";
			}
		}
		if ($toedit != '') {
			echo "<p>Please provide the updated coordinates for the selected items and click 'proceed'.</p>";
			echo "<p><table cellspacing=0>";
			echo "<tr><th $firstcell>Item</th><th>Original Coordinates ($oldshortname)</th><th>New Coordinates ($shortname)</th></tr>"; 
			echo "$toedit</table></p>";
			echo "<p><input type=submit class=button name='GoCurateTables' value='Proceed'><input type=hidden name='workingtable' value='$tablename'></form></p></div>";
			exit();
		}
		#echo "<input type=hidden name='tabledone' value='$tablename'>";
		#echo "<p><input type=submit class=button name='GoCurateTables' value='Proceed'></form></p>";
	}
	## SET DATABASE NAMES
 	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$newdb = "CNVanalysis".$shortname;
	mysql_select_db($currentdb);
	# update lift status to 'step 3'
	$query = mysql_query("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step = 3 WHERE frombuild ='$oldshortname' AND tobuild = '$shortname'");
	## get the tables containing failed lifts.
	$query = mysql_query("SELECT DISTINCT(tablename) FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND uid = '$userid'");
	$nrtables = mysql_num_rows($query);
	if ($nrtables == 0) {
		echo "<p><span class=nadruk>All annotation tables were successfully lifted or filled with new data</span></p>";
		echo "<p>Click Proceed to continue with lifting or providing new probelocation files.</p>";
		echo "<p><input type=submit class=button name='LiftProbeLoc' value='Proceed'></form></p>";
		echo "</div>";
		exit();
	}
	echo "<p>There are $nrtables tables left that failed to lift without errors completely. You will be presented with the failed items from one table at a time.</p>";
	$row = mysql_fetch_array($query);
	$tablename = $row['tablename'];
	## if table = probelocations, present button to continue to next block of code.
	if ($tablename == 'probelocations') {
		echo "<p>Proceed to Curating the Failed Probelocation lifts.</p>";
		echo "<p><input type=submit class=button name='LiftProbeLocPart3' value='Proceed'></form></p>";
		echo "</div>";
		exit();
	}
	echo "<p><span class=nadruk>Curating failed lifts from '$tablename'</span></p>";
	# get ids from current table
	$query = mysql_query("SELECT itemID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tablename' AND frombuild ='$oldshortname' AND tobuild = '$shortname' AND uid = '$userid'");
	$instring = '';
	while ($row = mysql_fetch_array($query)) {
		$instring .= $row['itemID'].",";
	}
	$instring = substr($instring,0,-1);
	# print details from those items and set options => delete, edit,
	if ($tablename == 'encode') {
		$query = mysql_query("SELECT id, symbol as name, chr, start, end AS stop, coding as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
	}
	elseif ($tablename == 'genes') {
		$query =  mysql_query("SELECT id, g.symbol as name, g.chr, g.start, g.end AS stop, gs.synonyms as extra FROM `$currentdb`.`$tablename` g JOIN `$currentdb`.`genesum` gs ON g.symbol = gs.symbol WHERE g.id IN ($instring) ORDER BY g.chr, g.start");
	}
	elseif ($tablename == 'cytoBand')  {
		$query = mysql_query("SELECT id, name, chr, start, stop, gieStain as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
	}
	elseif ($tablename == 'segdup') {
		$query = mysql_query("SELECT id, id as name, chr, start, stop, CONCAT('Chr',otherchr,':',otherstart,'-',otherstop) as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
	}
	elseif ($tablename = 'DGV_full') {
		$query = mysql_query("SELECT id, study as name, chr, start, stop, CONCAT('gain:',gain,'-loss:',loss,'-inv:',inv) as extra FROM `$currentdb`.`$tablename` WHERE id IN ($instring) ORDER BY chr, start");
	}
	echo "<p><table cellspacing=0>";
	echo "<tr><th $firstcell>Item</th><th>Original Region</th><th>Extra Info</th><th>Action</th></tr>";
	while ($row = mysql_fetch_array($query)) {
		$item = $row['name'];
		$region = "Chr". $row['chr'] . ":".number_format($row['start'],0,'',',')."-".number_format($row['stop'],0,'',',');
		$extra = $row['extra'];
		$radioname = $tablename."_".$row['id'];
		echo "<tr><td $firstcell>$item</td><td>$region</td><td>$extra&nbsp;</td><td NOWRAP><input type=radio name='$radioname' value='D'> Delete / <input type=radio name='$radioname' value='E'>Map Manually</td></tr>";
	}
	echo "</table></p>";	
	echo "<input type=hidden name='tabledone' value='$tablename'>";
	echo "<p><input type=submit class=button name='GoCurateTables' value='Proceed'></form></p>";
	echo "</div>";
	exit();	

}

/////////////////////////////////////////////////////////////////////
// NO TABLES FROM STEP 2 LEFT => PROCEED WITH PROBE LOCATION FILES //
/////////////////////////////////////////////////////////////////////
if (isset($_POST['LiftProbeLoc'])) {
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form enctype='multipart/form-data' action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
	## pass on static tables
	$tables = $_POST['statictables'];	
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	## pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";

	echo "<p><span class=nadruk>Updating Probe Locations</span></p>";
	echo "<p>There are two options to update the probelocations. You can either provide full data tables with all probes for all supported chiptypes, or we can try to Lift the probes automatically to the new genome build.  Providing Illumina-generated position maps is the most reliable, but unfortunately they do not provide them with backward compatibility. The new probelocation files should contain the following columns: <br/><br/> - Name<br/> - Chromosome<br/> - Position</p>";
	echo "<p>Select the chiptypes you have the new annotation files for. Next click 'Load Probelocations'. Chiptypes that were not provided will be filled in automatically afterwards in two steps: First, check if a certain probe (by name) was provided for another chiptype. If so, use these coordinates. If not, Lift the coordinates for using the UCSC LiftOver tool.</p>";
	$query = mysql_query("SELECT ID, name FROM `$currentdb`.chiptypes ORDER BY name");
	echo "<table cellspacing=0><tr><th $firstcell>ChipType</th><th>New Build Probe Locations</th></tr>";
	while ($row = mysql_fetch_array($query)) {
		$id = $row['ID'];
		$name = $row['name'];
		$fieldname = "ProbeFile_$id";
		echo "<tr><td $firstcell>$name</td><td><input type=file size=40 name='$fieldname'></td></tr>";
	}
	echo "</table></p><p>";
	echo "<input type=submit class=button name='LiftProbeLocPart2' value='Load Probelocations'></form>";
	echo "</p></div>";  
	exit();
}

if (isset($_POST['LiftProbeLocPart2'])) {
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;

	echo "<p>Processing provided files...</p>";
	flush();
	$query = mysql_query("SELECT ID, name FROM `$currentdb`.chiptypes ORDER BY name");
	$providedfiles = '';
	while ($row = mysql_fetch_array($query)) {
		$id = $row['ID'];
		$name = $row['name'];
		$fieldname = "ProbeFile_$id";
		echo "<span class=bold>$name :</span>";
		flush();
		if (basename( $_FILES[$fieldname]['name']) != "") {
			echo " File Provided: ";
			$file = basename( $_FILES[$fieldname]['name']);
		 	$file = str_replace(' ','_',$file);
			$location = "/tmp/chip_$id.$file";
			// check filetype
			$filetype = $_FILES[$fieldname]['type'];
			if (!($filetype == "text/plain") && !($filetype == "text/csv") ) {
				echo "File Format not supported, skipping (only .TXT/.CSV files are supported) => filetype = $filetype<br/>";
				continue;
			}
			// move to /tmp
			if(move_uploaded_file($_FILES[$fieldname]['tmp_name'], $location) ) {
   			}
		 	else {
  				echo "Upload Failed, skipping<br/>";
				continue;
 			}
			// check columns
		  	$fh = fopen("$location","r");
		  	$line = fgets($fh);
		  	$line = rtrim($line);
		  	#$pieces = preg_split("/\t|,|;/",$line);
			if (!preg_match("/name/i",$line) || !preg_match("/chr/i",$line) || !preg_match("/pos|mapinfo/i",$line)) {	
			#if (!in_array("Name",$pieces) || !in_array("Chromosome", $pieces) || !in_array("Position",$pieces)) {
				echo "Provided file does not contain the required columns, skipping<br/>";
				unlink($location);
				continue;
			}
			echo "File stored for processing<br/>";
			$providedfiles .= "$location@@";
		}
		else {
			echo " No File Provided.<br/>";
		}
	}
	if ($providedfiles != '') {
		$providedfiles = substr($providedfiles,0,-2);
		$provstring = "-p '$providedfiles'";
	}
	else {
		$provstring = '';
	}
	echo "</p><p>Now starting with the Lifting process: </p>";
	echo "<script type='text/javascript'>interval1 = setInterval(\"reloadspan('ProbeLiftOut')\",10000)</script>\n";
	echo "<p><span class=bold>ProbeLift Output:</span><pre id='ProbeLiftOut' class=scrollbarbox>Starting Lifting Process...</pre><hr>\n";
	$output  = system("(echo 1 > status/LiftOver.status && sleep 5 && cd $maintenancedir/LiftOver/ && ./LiftOver.pl -d '$oldshortname' -D '$shortname' -U '$userid' -T 'probelocations' $provstring &) > $maintenancedir/LiftOver/Files/ProbeLiftOut.out 2>&1");
	flush();
	echo "<script type='text/javascript'>interval2 = setInterval(\"checkstatus(2)\",10000)</script>\n";
	// print CONTINUE FORM
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
	# pass on static tables
	$tables = $_POST['statictables'];	
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	echo "<p><span id=continuebutton style='display:none;'><input type=submit class=button name='LiftProbeLocPart3' value=Proceed></span></p>\n";
	echo "</form>";
	echo "</div>";
	exit();
}

if (isset($_POST['LiftProbeLocPart3'])) {
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=3' method=POST>\n";
	# pass on static tables
	$tables = $_POST['statictables'];	
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	//////////////////////////////////
	// first process remapped items //
	//////////////////////////////////
	if (isset($_POST['remapped'])) {
		$remapped = $_POST['remapped'];
		$tablename = 'probelocations';
		foreach($remapped as $key => $itemid) {
			$name = $_POST["name_$itemid"];
			$newchr = $_POST["chr_$name"];
			$newchr = $chromhash[$newchr];
			$newstart = $_POST["pos_$name"];
			$newstart = str_replace(",",'',$newstart);
			#$newstop = $_POST["stop_$itemid"];
			#$newstop = str_replace(",",'',$newstop);
			#print "remap $itemid ($name) to $newchr:$newstart<br/>";
			$qstring = "INSERT INTO `$newdb`.`probelocations` (name, chromosome, position, chiptype, zeroed, filtered, LiftedFrom)";
			$query = mysql_query("SELECT chiptype, zeroed, filtered FROM `$currentdb`.`probelocations` WHERE ID = '$itemid'");
			$row = mysql_fetch_array($query);
			$qstring .= " VALUES ('$name','$newchr','$newstart','".$row['chiptype']."','".$row['zeroed']."','".$row['filtered']."','$oldshortname')";
			#echo "qs: $qstring<br/>";
			$insq = mysql_query($qstring);
			$delq = mysql_query("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND uid = '$userid' AND itemID = '$itemid' AND tablename = 'probelocations'"); 
		}	
	}	
	///////////////////////////////////
	// second process Curated items ///
	///////////////////////////////////
	if (isset($_POST['chrdone'])) {
		$chrdone = $_POST['chrdone'];
        
		# get ids from current table
		$query = mysql_query("SELECT itemID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = 'probelocations' AND frombuild ='$oldshortname' AND tobuild = '$shortname' AND uid = '$userid'");
		$instring = '';
		while ($row = mysql_fetch_array($query)) {
			$instring .= $row['itemID'].",";
		}
		#echo "tablename: Chromsome <br/>";

		$instring = substr($instring,0,-1);
		#echo "instring $instring<br/>";	
		$query = mysql_query("SELECT ID, name, chromosome, position FROM `$currentdb`.`probelocations` WHERE id IN ($instring) AND chromosome = '$chrdone' ORDER BY position, name");
		
		$currentname = '';
		while ($row = mysql_fetch_array($query)) {
			$itemid = $row['ID'];
			$item = $row['name'];
			$chr = $row['chromosome'];
			$chrtxt = $chromhash[$chr];
			$pos = $row['position'];
			$region = "Chr".$chrtxt.":".number_format($pos,0,'',',');
            if (array_key_exists($item,$_POST['probelocations'])) {
			    if ($_POST["probelocations"][$item] == 'D') {
			    	$squery = mysql_query("DELETE QUICK FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND uid = '$userid' AND itemID = '$itemid' AND tablename = 'probelocations'");
			    }
			    elseif ($_POST["probelocations"][$item] == 'E') {
			    	$hiddenfields .= "<input type=hidden name='name_$itemid' value='$item'><input type=hidden name='remapped[]' value='$itemid'>\n";
			    	if ($currentname != $item) {
			    		$toedit .= "<tr><td $firstcell>$item</td><td>$region</td><td>Chr<input type=text name='chr_$item' size=2 value='$chrtxt'>:<input type=text name='pos_$item' size=12 value='$pos'></td></tr>";
			    		$currentname = $item;
			    	}
			    }
            }
		}
		if ($toedit != '') {
			echo "<p>Please provide the updated coordinates for the selected items and click 'proceed'.</p>";
			echo $hiddenfields;
			echo "<p><table cellspacing=0>";
			echo "<tr><th $firstcell>Item</th><th>Original Coordinates ($oldshortname)</th><th>New Coordinates ($shortname)</th></tr>"; 
			echo "$toedit</table></p>";
			echo "<p><input type=submit class=button name='LiftProbeLocPart3' value='Proceed'><input type=hidden name='workingtable' value='probelocations'></form></p></div>";
			exit();
		}
		#echo "<input type=hidden name='tabledone' value='$tablename'>";
		#echo "<p><input type=submit class=button name='GoCurateTables' value='Proceed'></form></p>";
	}
	# update lift status to 'step 3b'
	$query = mysql_query("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step = '3b' WHERE frombuild ='$oldshortname' AND tobuild = '$shortname'");
	## get the tables containing failed lifts.
	$query = mysql_query("SELECT itemID FROM `GenomicBuilds`.`FailedLifts` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname' AND tablename = 'probelocations' AND uid = '$userid'");
	$nrprobes = mysql_num_rows($query);
	if ($nrprobes == 0) {
		echo "<p><span class=nadruk>All probe locations were successfully lifted</span></p>";
		echo "<p>Click Proceed to continue with adapting analysis runtime files.</p>";
		echo "<p><input type=submit class=button name='LiftRunTimeFiles' value='Proceed'></form></p>";
		echo "</div>";
		exit();
	}
	#echo "<p>There are $nrprobes probes left to lift.  You will be presented with the failed items from one chromosome at a time.</p>";
	#echo "<p><span class=nadruk>Curating failed lifts from 'probelocations'</span></p>";
	$query = mysql_query("SELECT p.ID, p.name, p.chromosome, p.position, f.Reason FROM `$currentdb`.`probelocations` p JOIN `GenomicBuilds`.`FailedLifts` f ON f.itemID = p.ID  WHERE f.tablename = 'probelocations' AND f.frombuild = '$oldshortname' AND f.tobuild = '$shortname' AND f.uid = '$userid' ORDER BY p.chromosome, p.position, p.name ");
	#echo "<p>SELECT p.ID, p.name, p.chromosome, p.position, f.Reason FROM `$currentdb`.`probelocations` p JOIN `GenomicBuilds`.`FailedLifts` f ON f.itemID = p.ID WHERE f.tablename = '$probelocations' AND f.frombuild = '$oldshortname' AND f.tobuild = '$shortname' AND f.uid = '$userid' ORDER BY p.chromosome, p.position, p.name </p>";
	// print results
	$currentname = '';
	$currchr = '';
	while ($row = mysql_fetch_array($query)) {
		$itemid = $row['ID'];
		$item = $row['name'];
		$chr = $row['chromosome'];
		$failureinfo = $row['Reason'];
		if ($chr != $currchr) {
			if ($currchr == '') {
				echo "<p><span class=nadruk>Curating failed lifts from 'probelocations' on chromosome ".$chromhash[$chr]."</span></p>";
				echo "<p><table cellspacing=0>";
				echo "<tr><th $firstcell>Item</th><th>Original Region</th><th>Failure Info</th><th><span title='Click to delete all' style='cursor:pointer' onclick=\"check('D');\">Delete</span> / <span title='Click to manually curate all' style='cursor:pointer' onclick=\"check('E')\">Remap</span></th></tr>";
			}
			else {
				break;
			}
		}
		$currchr = $chr;
		$chrtxt = $chromhash[$chr];
		$pos = $row['position'];
		$region = "Chr".$chrtxt.":".number_format($pos,0,'',',');
		#$hiddenfields .= "<input type=hidden name='name_$itemid' value='$item'>\n";
		if ($currentname != $item) {
			$radioname = "probelocations[".$row['name']."]";
			echo "<tr><td $firstcell>$item</td><td>$region</td><td>$failureinfo</td><td NOWRAP><input type=radio name='$radioname' value='D' > Delete / <input type=radio name='$radioname' value='E' >Map Manually</td></tr>";
			$currentname = $item;
		}
	}
	echo "</table></p>";	
	echo "<input type=hidden name='chrdone' value='$currchr'>";
	echo "<p><input type=submit class=button name='LiftProbeLocPart3' value='Proceed'></form></p>";
	echo "</div>";
	exit();	



}

if (isset($_POST['LiftRunTimeFiles'])) {
	echo "<p>We are now ready to lift some runtime files.  Based on the provided or lifted probe locations, the .pfb and .gcmodel files of PennCNV will be created for the new build. ";
    echo "GC-model files for QuantiSNP are also created automatically, but this takes a lot of time and bandwidth, so please be patient.</p>";

	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=4' method=POST>\n";
	echo "<p><span id=continuebutton style='display:none;'>Runtime files have been successfully lifted. You may now proceed to lifting the actual CNV data. <br/><br/><input type=submit class=button name='LiftCNVs' value=Proceed></span></p>\n";
	# pass on static tables
	$tables = $_POST['statictables'];	
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	// UPDATE STATUS
	$query = mysql_query("UPDATE `GenomicBuilds`.`LiftsInProgress` SET step = '3c' WHERE frombuild ='$oldshortname' AND tobuild = '$shortname'");
	
	$output  = system("(echo 1 > status/LiftOver.status && sleep 5 && cd $sitedir/LiftOver/Scripts && ./UpdateRuntime.pl -d '$oldshortname' -D '$shortname' -U '$userid' &) > $maintenancedir/LiftOver/Files/RuntimeLiftOut.out 2>&1");
	flush();
	echo "<script type='text/javascript'>interval1 = setInterval(\"reloadspan('RuntimeLiftOut')\",10000)</script>\n";
	echo "<p><span class=bold>Runtime Lift Output:</span><pre id='RuntimeLiftOut' class=scrollbarbox style='height:400px;'>Starting Lifting Process...</pre><hr>\n";
	flush();
	echo "<script type='text/javascript'>interval2 = setInterval(\"checkstatus(2)\",10000)</script>\n";
	
	echo " </form>";	
	echo "</div>";

}

?>
