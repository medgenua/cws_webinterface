<?php
#################################################
#  LIFTOVER STEP 4 : Lift Critical TABLES	#
#	- Lifting and curatation		#
#	- Will cost you some time !		#
#						#
#	=> ACCESS TO CURRENT BUILD REMAINS OPEN	#
#################################################
echo "<div class=sectie>\n";
echo "<h3>LiftOver Step 4: Lifting Critical Tables</h3>\n";
echo "<h4>And Manually Curate failed lifts</h4>\n";
ob_end_flush();


## chromosome hash
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["25"] = "M";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["M"] = "25";


###########################
## define tables to lift ##
###########################
$tablestring = 'aberration,cus_aberration,parents_regions,parents_upd,Classifier_x_Regions,aberration_LOH, aberration_mosaic, Filters_x_Regions ';
$cnvtables = explode(',',$tablestring); 
#$datapointtables = array( 'datapoints',  'BAFSEG', 'cus_datapoints', 'parents_datapoints', 'parents_upd_datapoints');
$datatablestring = 'datapoints,BAFSEG,cus_datapoints,parents_datapoints,parents_upd_datapoints,datapoints_LOH';
$datatables = explode(',',$datatablestring);

############################
## LIFT CNV REGION TABLES ##
############################
if (isset($_POST['LiftCNVs'])) {
	echo "<p>We will now be lifting CNV region information. You will not be asked to manually curate all this, since the results will be sent to the project owners for curating.  After CNV region info, all stored raw datapoints will be lifted.</p>";

	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=4' method=POST>\n";
	echo "<p><span id=continuebutton style='display:none;'>CNV region info was successfully lifted. You may now proceed to lifting the CNV probelevel datapoints. You may select the option below to automatically fix failed region lifts based on the probelevel lifting. This means that if a region is split in the new build, but the probelocations still constitute a contiguous stretch without extra probes between them, the region will be lifted based on this information. Probe coordinates are taken from the probe lift performed earlier.<br/><br/><input type=checkbox name=autocorrection value=yes> Autocorrect Failed CNV lifts based on probe info.<br/><br/><input type=checkbox name=autoparents value=yes> Autocorrect Failed Parental Regions based Corresponding offspring regions. <br/><br/><input type=submit class=button name='LiftDataPoints' value=Proceed></span></p>\n";
	# pass on static tables
	$tables = $_POST['statictables'];
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	// UPDATE STATUS
	## start the update script for Region tables	
	$output  = system("(echo 1 > status/LiftOver.status && sleep 5 && cd $sitedir/LiftOver/Scripts && ./LiftCNVs.pl -d '$oldshortname' -D '$shortname' -u '$userid' -t '$tablestring' &) > $maintenancedir/LiftOver/Files/CNVlift.out 2>&1");
	flush();
	$row = 1;
	foreach ($cnvtables as $key => $tablename) {
		echo "<script type='text/javascript'>interval$row = setInterval(\"reloadspan('$tablename')\",10000)</script>\n";
		echo "<p><span class=bold>$tablename Lift Output:</span><pre id='$tablename' class=scrollbarbox >Starting Lifting Process...</pre><hr>\n";
		flush();
		$row++;
	}

	
	echo "<script type='text/javascript'>interval$row = setInterval(\"checkstatus('$row')\",10000)</script>\n";
	echo " </form>";	
	echo "</div>";
}
###############################
## LIFT CNV DATAPOINT TABLES ##
###############################
if (isset($_POST['LiftDataPoints'])) {
	echo "<p>We will now be lifting probe level data from several tables. You will not be asked to manually curate all this. Because manual curation would be too much work, all missing datapoints are simply deleted. </p>";
	if (isset($_POST['autocorrection'])) {
		$auto = '-a';
	}
	else {
		$auto = '';
	}
	if (isset($_POST['autoparents'])) {
		$autoparents = '-p';
	}
	else {
		$autoparents = '';
	}
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=5' method=POST>\n";
	echo "<p><span id=continuebutton style='display:none;'>ProbeLevel Datapoints were successfully lifted. You may now proceed to final configuration steps. <br/><br/><input type=submit class=button name='ToFinal' value=Proceed></span></p>\n";
	# pass on static tables
	$tables = $_POST['statictables'];
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	## start the update script for DATApoint tables	
	$output  = system("(echo 1 > status/LiftOver.status && sleep 5 && cd $sitedir/LiftOver/Scripts && ./LiftDataPoints.pl -d '$oldshortname' -D '$shortname' -u '$userid' -t '$datatablestring' $auto $autoparents &) > $maintenancedir/LiftOver/Files/DataLift.out 2>&1");
	flush();
	$row = 1;
	echo "<p><span class=bold>DataPoints Lift Output:</span><pre id='DataLift' class=scrollbarbox style='height:350px;' >Starting Lifting Process...</pre><hr>\n";
	echo "<script type='text/javascript'>interval1 = setInterval(\"reloadspan('DataLift')\",10000)</script>\n";
	flush();
	echo "<script type='text/javascript'>interval2 = setInterval(\"checkstatus(2)\",10000)</script>\n";
	
}
