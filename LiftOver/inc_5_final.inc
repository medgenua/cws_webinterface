<?php
#################################################
#  LIFTOVER STEP 4 : Lift Critical TABLES	#
#	- List items to do manually		# 
#	- send out emails to users		#
#	- restore project permissions		#
#	- set new build to active.		#
#################################################
echo "<div class=sectie>\n";
echo "<h3>LiftOver Step 5: Final Configurations</h3>\n";
echo "<h4>You will need commandline access</h4>\n";
ob_end_flush();


## chromosome hash
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["25"] = "M";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["M"] = "25";

if (isset($_POST['ToFinal'])) {
	// PRINT DIRECTIONS FOR MANUAL CURATION.
	echo "<p>You must now review the links provided by users to public resources. Double check each link for references to Genome builds. If present you can replace ucsc build (eg hg19) by '%u'. If you can not update the link appropriately, unckeck the Resources to not include it in the new build.</p>";
	
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=5' method=POST>\n";
	#echo "<p><span id=continuebutton style='display:none;'>ProbeLevel Datapoints were successfully lifted. You may now proceed to final configuration steps. <br/><br/><input type=submit class=button name='ToFinal' value=Proceed></span></p>\n";
	# pass on static tables
	$tables = $_POST['statictables'];
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";
	$query = mysql_query("SELECT Resource, link, Description, type FROM `$currentdb`.db_links ORDER BY type ASC, Resource ASC");
	if (mysql_num_rows($query) == 0) {
		echo "<p>No public resources present for the current build.</p>";
	}
	else {
		echo "<p><table cellspacing=0>";
		$types = array('1' => 'Gene', '2' => 'Region');
		echo "<tr><th $firstcell>Keep</th><th>Resource</th><th>link</th><th>Description</th><th>Query Basis</th></tr>";
		while ($row = mysql_fetch_array($query)) {
			echo "<tr>";
			echo "<td $firstcell><input type=checkbox CHECKED name='keep_".$row['Resource']."'></td>";
			echo " <td NOWRAP >".$row['Resource']."</td>";
			echo " <td><input type=text name='link_".$row['Resource']."' size=60 value='".$row['link']."'></td>";
			echo " <td>".$row['Description']."</td>";
			echo " <td>".$types[$row['type']]."</td>";
			echo "</tr>";
		}
		echo "</table></p>";
	}
	
	echo "<p>If all links are corrected, click proceed to store the new values.</p><p><input type=submit class=button name='StoreLinks' value=Proceed></form></p>";
	exit();	

}

if (isset($_POST['StoreLinks'])) {
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	// process posted items
	$query = mysql_query("SELECT Resource, link, Description, type FROM `$currentdb`.db_links ORDER BY type ASC, Resource ASC");
	if (mysql_num_rows($query) == 0) {
		echo "<p>No public resources were reviewed.</p>";
	}
	else {
		echo "Storing Public Resources:</ul>";
		while ($row = mysql_fetch_array($query)) {
			$resourcepost = str_replace(' ','_',$row['Resource']);
			$resource = $row['Resource'];
			if (!isset($_POST["keep_$resourcepost"])) {
				echo "<li>$resource => discarded</li>";
				continue;
			}
			$link = $_POST["link_$resourcepost"];
			$desc = $row['Description'];
			$type = $row['type'];
			mysql_query("INSERT INTO `$newdb`.`db_links` (Resource,link,Description,type,LiftedFrom) VALUES ('$resource','$link','$desc','$type','$oldshortname')");
			echo "<li>$resource => Stored</li>";
		}
		echo "</ul></p>";
	}
	echo "<form action='index.php?page=admin_pages&amp;type=lift&amp;step=5' method=POST>\n";
	# pass on static tables
	$tables = $_POST['statictables'];
	foreach($tables as $key => $tablename) {
		echo "<input type=hidden name='statictables[]' value='$tablename'>\n";
	}
	# pass on password & build name
	echo "<input type=hidden name=shortname value='$shortname'>\n";

	echo "<p> All 'CNV-WebStore' related data have now been updated to the new genomic build. Please not that other services that make use of the GenomeBuild Tables to determine their target build should be manually checked. These services might include all processes involing Blast/BLAT/exonerate components.</p>";
	echo "<p>By proceeding below, the system will be restored to active state and all permissions on the projects will be restored to the pre-lifting permissions. After that, the old Genome Build will remain accessible in a read-only mode and all access to CNV-WebStore will default to the new genome build.</p>";
	echo "<p><input type=submit class=button name='RestoreSystem' value=Proceed></p>\n";
	echo "</form>";
	exit();

}


if (isset($_POST['RestoreSystem'])) {
	// db to use:
	$currentdb = "CNVanalysis".$_SESSION['dbname'];
	$oldshortname = $_SESSION['dbname'];
	$shortname = $_POST['shortname'];
	$newdb = "CNVanalysis".$shortname;
	// 0. PREPARE
	// details
	$query = mysql_query("SELECT StringName, Description, size FROM `GenomicBuilds`.NewBuild WHERE name = '$shortname'");
	$row = mysql_fetch_array($query);
	$longname = $row['StringName'];
	$descr = $row['Description'];
    $size = $row['size'];
	// send out emails
	$subject = "CNV-WebStore Genome Build Update";
	$query = mysql_query("select COUNT(f.uid) as Aantal, u.FirstName, u.LastName, u.email FROM `GenomicBuilds`.FailedLifts f JOIN `$currentdb`.users u ON f.uid = u.id GROUP BY f.uid");
	echo "<p>Sending out emails for manual curation of failed lifts:<ul>";
	
	while ($row = mysql_fetch_array($query)){
		$email = $row['email'];
		$fname = $row['FirstName'];
		$lname = $row['LastName'];
		$todo = $row['Aantal'];
		$name = "$fname $lname";
		$message = "Dear $fname,\r\rCNV-WebStore has been converted to the new reference genomebuild ($longname). In this process, as many data as possible has been converted automatically. However, $todo regions remain that should be manually curated. To do so, log in on CNV-WebStore and follow the directions on the main page.\r\rBest Regards,\rCNV-WebStore System Administrator\r";
		$headers = "From: $adminemail\r\n";
		#$headers .= "To: $email\r\n";
		$headers .= "To: $adminemail\r\n";
		// Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. 
		//if (mail($adminemail,"$subject",$message,$headers)) {
		#if (mail($email,"$subject",$message,$headers)) {
			echo "<li>$name => Email successfully sent : $todo items to review</li>";
		//} 
		//else {
		//	echo "<li>$name => Sending Failed</li>";
		//}
	}
	
	echo "</ul></p>";

	
	// 1. Move Current Build to previous builds
	mysql_query("INSERT IGNORE INTO `GenomicBuilds`.PreviousBuilds SELECT * FROM `GenomicBuilds`.CurrentBuild");
		
	// 2. Set New Build to Current 
	mysql_query("TRUNCATE TABLE `GenomicBuilds`.CurrentBuild");
	mysql_query("INSERT INTO `GenomicBuilds`.CurrentBuild VALUES ('$shortname', '$longname', '$descr',$size)");
	
	// 3. Restore Permissions
	$sourcebuild = substr($oldshortname,1);
	$targetbuild = substr($shortname,1);
	mysql_query("TRUNCATE TABLE `$newdb`.projectpermission");
	mysql_query("INSERT INTO `$newdb`.projectpermission SELECT * FROM `GenomicBuilds`.`$sourcebuild"."To$targetbuild"."UserPermissions`");
	mysql_query("TRUNCATE TABLE `$newdb`.cus_projectpermission");
	mysql_query("INSERT INTO `$newdb`.cus_projectpermission SELECT * FROM `GenomicBuilds`.`$sourcebuild"."To$targetbuild"."cus_UserPermissions`");
	
	// 4. Remove new build from 'in progress'
	mysql_query("DELETE FROM `GenomicBuilds`.`LiftsInProgress` WHERE frombuild = '$oldshortname' AND tobuild = '$shortname'");	
	// 5. Restore Platform status
	mysql_query("UPDATE `GenomicBuilds`.`SiteStatus` SET status = 'Operative' WHERE 1=1");
	
	// 6. Print Redirection.
	echo "<p>Congratulations, CNV-WebStore has now been lifted to $longname.</p>";
	echo "<p><a href='index.php?setbuild=$shortname'>Reload CNV-WebStore in build $longname</a></p>";
	exit();
}







?>
