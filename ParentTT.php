
<?php
ob_start();
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

$aid = $_GET['q'];
$userid = $_GET['u'];
$psid = $_GET['psid'];
$ppid = $_GET['ppid'];

#$result = mysql_query("SELECT p.chiptype, a.cn, a.start, a.stop, a.chr, s.chip_dnanr, s.gender, a.seenby, a.nrsnps, a.nrgenes, a.sample, a.idproj, a.class, a.inheritance FROM aberration a JOIN sample s JOIN project p ON a.sample = s.id AND a.idproj = p.id WHERE a.id = '$aid'");
$result = mysql_query("SELECT a.inheritance, a.class, a.cn, a.start, a.stop, a.chr, s.chip_dnanr, a.sample FROM aberration a JOIN sample s ON a.sample = s.id WHERE a.id = '$aid'");

#$fromimage = $_GET['fi'];
$row = mysql_fetch_array($result);
$sid = $row['sample'];
$cn = $row['cn'];
$start = $row['start'];
$stop = $row['stop'];
$size = $stop - $start + 1;
$chr = $row['chr'];
$inheritance = $inh[$inh[$row['inheritance']]];
$class = $row['class'];
$chrtxt = $chromhash[ $chr];
$sample = $row['chip_dnanr'];
$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');



// PRINT TITLE 
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
echo "<h3 style='margin-top:5px'>$region</h3>";
echo "</div>\n";
// PRINT CNV DETAILS (for cnv from child)
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
echo "<span class=nadruk>Offspring CNV Details</span>";
echo " <ul id=ul-simple>";
echo "<li>- Sample: $sample</li>";
echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
echo "<li>- Copy Number : $cn</li>";
echo "</ol></li>";
echo "</ul>";
// PRINT CLASSIFICATION DETAILS
echo "<span class=nadruk>Offspring CNV Classification</span>";
echo "<ul id=ul-simple>";
echo "<li>- Inheritance: ". $inheritance . "</li>\n";
$nrlines = 0;
if ($class != '') {
	$logq = mysql_query("SELECT uid, entry,arguments FROM log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
			$arguments = $logrow['arguments'];
			$logentry = $logrow['entry'];
			break;
		}
	}
	echo "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
}
else {
	echo "<li>- Diagnostic Class: Not Defined ";
}
if ($arguments != '') {
	echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
	$nrlines--;
}
echo "</li>\n";
echo "</ul>\n";



// PRINT PARENT DETAILS

$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate FROM projsamp WHERE idsamp = '$psid' AND idproj = '$ppid'");
$row = mysql_fetch_array($query);
$lsd = $row['LRRSD'];
$bsd = $row['BAFSD'];
$wave = $row['WAVE'];
$iniwave = $row['INIWAVE'];
$callrate = $row['callrate'];
$remark = "<div class=nadruk>Quality Remarks:</div><ul id=ul-simple>";
$remarkstring = $remark;

if ($callrate < 0.994) {
	//$cbg = 'style="background:red;color:black"';
	$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
	$nrlines++;
}
if ($lsd > 0.2 && $lsd < 0.3) {
	$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
}
if ($lsd >=0.3) {
	$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
	$nrlines++;
}
if ($bsd >= 0.6) {
	$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
	$nrlines++;
} 
if ($iniwave != 0) {
	$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
	$nrlines++;
}
# fill some whitespace
for ($idx = 1; $idx<=$nrlines;$idx++) {
	echo "<br/>";
}
echo "</div>";
$query = mysql_query("select COUNT(id) as aantal FROM aberration WHERE sample = '$psid' AND idproj = '$ppid'");
$row = mysql_fetch_array($query);
$ncnvs = $row['aantal'];

$query = mysql_query("select chip_dnanr, gender FROM sample WHERE id = '$psid'");
$row = mysql_fetch_array($query);
$parentname = $row['chip_dnanr'];
$gender = $row['gender'];

$query = mysql_query("SELECT chiptype FROM project WHERE id = '$ppid'");
$row = mysql_fetch_array($query);
$chiptype = $row['chiptype'];
echo "<div style='position:absolute;left:215px;top:50px;'>";
echo "<div class=nadruk>Parental Details</div>";
echo "<ul id=ul-simple>";
echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$ppid&sample=$psid' target=new>$parentname</a></li>\n";
echo "<li>- Chiptype: $chiptype</li>";
echo "<li>- Gender : $gender</li>\n";
echo "<li>- #CNV's : $ncnvs</li>\n";
if ($remarkstring != $remark) {
	echo "</ul>\n";
	echo "<p>";
	echo $remarkstring;
	echo "</ul></p>";
}
else {
	echo "<li>- Quality : OK</li>\n";
	echo "</ul>\n";
}

echo "</div>";
ob_flush();
flush();

include('inc_create_parent_plot.inc');

?>
