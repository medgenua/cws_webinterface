#!/usr/bin/perl


# GET VARS
$samplename = $ARGV[0];
$includeplots = $ARGV[1];


# Compile main report
my $command = "( pdflatex $samplename.tex && pdflatex $samplename.tex && rm $samplename.aux && rm $samplename.log && rm $samplename.tex ) > /dev/null 2>&1";
system($command);
