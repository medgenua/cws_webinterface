<?php
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$lddb = "LNDB";
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$cnvdb");

if (isset($_GET['s']) && isset($_GET['pc']) && !isset($_GET['delpheno'])) {
	$sid = $_GET['s'];
	$phenocode = $_GET['pc'];
	if ($_GET['custom'] == 1) {
		$pid = $_GET['p'];
		mysql_query("INSERT INTO cus_sample_phenotypes (sid, pid, lddbcode) VALUES ('$sid', '$pid', '$phenocode') ON DUPLICATE KEY UPDATE sid = sid");
	}
	else {
		mysql_query("INSERT INTO sample_phenotypes (sid, lddbcode) VALUES ('$sid', '$phenocode') ON DUPLICATE KEY UPDATE sid = sid"); 
	}
}
elseif (isset($_GET['s']) && $_GET['delpheno'] == 1) {
	$sid = $_GET['s'];
	$phenocode = $_GET['pc'];
	if ($_GET['custom'] == 1) {
		$pid = $_GET['p'];
		mysql_query("DELETE FROM cus_sample_phenotypes WHERE sid = '$sid' AND pid = '$pid' AND lddbcode = '$phenocode'");	
	}
	else {
		mysql_query("DELETE FROM sample_phenotypes WHERE sid = '$sid' AND lddbcode = '$phenocode'");	
	}
}
if (isset($_GET['filldiv'])) {
	
	echo "<h3>Associated Phenotypes for $sid</h3>\n";
	$sid = $_GET['s'];
	$pid = $_GET['p'];
	$parray = array();
	$linkpart = '';
	if ($_GET['custom'] == 1) {
		$query = mysql_query("SELECT lddbcode FROM cus_sample_phenotypes WHERE sid = '$sid' AND pid = '$pid' ORDER BY lddbcode");
	}
	else {
		$query = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
	}
	while ($row = mysql_fetch_array($query)) {
		$code = $row['lddbcode'];
		$pieces = explode('.',$code);
		$parray[$pieces[0]][$pieces[1]][] = $pieces[2];
	}
	mysql_select_db($lddb);
	echo "<p><table cellspacing=0>";
	echo "<tr>\n";
	echo "<th $firstcell class=topcellalt>&nbsp;</th><th class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
	echo "</tr>\n";
	$fts = '&ft=1';
	foreach($parray as $first => $sarray) {
		$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
		$row = mysql_fetch_array($query);
		$firstlabel = $row['LABEL'];
		foreach ($sarray as $second => $tarray) {
			if ($second == '00') {
				echo "<tr><td $firstcell><img onclick=\"delphenotype('$sid','$pid','$first.00.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
			}
			else {
				$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
				$row = mysql_fetch_array($query);
				$secondlabel = $row['LABEL'];
				foreach ($tarray as $third) {
					if ($third == '00') {
						echo "<tr><td $firstcell><img onclick=\"delphenotype('$sid','$pid','$first.$second.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>\n";	
					}
					else {
						$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
						$row = mysql_fetch_array($query);
						$thirdlabel = $row['LABEL'];
					
						echo "<tr><td $firstcell><img onclick=\"delphenotype('$sid','$pid','$first.$second.$third')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>\n";
					}	
				}
			}
		}
	}
	echo "</table>\n";

}
?>
