<?php
session_start();
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$lddb = "LNDB";
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$cnvdb");
//settings
if (!isset($_SESSION['maxclass'] )) {
	$_SESSION['maxclass'] = 3;
}
$classpart = '';
if ($_SESSION['maxclass'] != '') {
	$classpart = "AND class IN (";
	for ($c = 1; $c <= $_SESSION['maxclass'];$c++) {
		$classpart .= "$c,";
	}
	$classpart = substr($classpart,0,-1);
	$classpart .= ")";
}
if (!isset($_SESSION['andor'])) {
	$_SESSION['andor'] = 'OR';
}
$andor = $_SESSION['andor'];
$userid = $_SESSION['userID'];
// process
if (!isset($_GET['delpheno']) && isset($_GET['pc'])) {
	$phenocode = $_GET['pc'];
	#mysql_query("INSERT INTO sample_phenotypes (sid, lddbcode) VALUES ('$sid', '$phenocode') ON DUPLICATE KEY UPDATE sid = sid"); 
	$_SESSION['searchpheno'][$phenocode] = 1;
}
elseif (isset($_GET['pc']) && $_GET['delpheno'] == 1) {
	#$sid = $_GET['s'];
	$phenocode = $_GET['pc'];
	#mysql_query("DELETE FROM sample_phenotypes WHERE sid = '$sid' AND lddbcode = '$phenocode'");	
	unset($_SESSION['searchpheno'][$phenocode]);
}
if (isset($_GET['filldiv'])) {
	echo "<div class=sectie>";
	echo "<h3>Selected Phenotypes </h3>\n";
	$parray = array();
	$instring = '';	
	foreach ($_SESSION['searchpheno'] as $code => $set) {
		if ($set != 1) {
			continue;
		}
		$instring .= "'$code',";
		$pieces = explode('.',$code);
		$parray[$pieces[0]][$pieces[1]][] = $pieces[2];
	}

	mysql_select_db($lddb);
	echo "<p><table cellspacing=0>";
	echo "<tr>\n";
	echo "<th $firstcell class=topcellalt>&nbsp;</th><th class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
	echo "</tr>\n";
	$fts = '&ft=1';
	foreach($parray as $first => $sarray) {
		$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
		$row = mysql_fetch_array($query);
		$firstlabel = $row['LABEL'];
		foreach ($sarray as $second => $tarray) {
			if ($second == '00') {
				echo "<tr><td $firstcell><img onclick=\"delphenotype('$first.00.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
			}
			else {
				$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
				$row = mysql_fetch_array($query);
				$secondlabel = $row['LABEL'];
				foreach ($tarray as $third) {
					if ($third == '00') {
						echo "<tr><td $firstcell><img onclick=\"delphenotype('$first.$second.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>\n";	
					}
					else {
						$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
						$row = mysql_fetch_array($query);
						$thirdlabel = $row['LABEL'];
					
						echo "<tr><td $firstcell><img onclick=\"delphenotype('$first.$second.$third')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>\n";
					}	
				}
			}
		}
	}
	echo "</table>\n";
	echo "</p>";
	//////////////////////////////////
	// CHANGE CRITERIA COMBINATIONS //
	//////////////////////////////////
	echo "<p><form action='index.php?page=results&type=pheno' method=POST>";
	echo "<table style='width:100%'>";
	if ($andor == 'AND') {
		echo "<tr><td class=clear>Show samples matching: <input type=radio name=andor value='AND' checked> All / <input type=radio name=andor value='OR'> Any</td>"; 
	}
	else{
		echo "<tr><td class=clear>Show samples matching: <input type=radio name=andor value='AND'> All / <input type=radio name=andor value='OR' checked> Any</td>";
	}

	echo "<td class=clear>Show aberrations from diagnostic class: <select name=maxclass>";
	if ($_SESSION['maxclass'] == '') {
		echo "<option value='' selected>All (including non-specified)</option>";
		echo "<option value=1>Only Class 1</option>";
		echo "<option value=2>Class 1-2</option>";
		echo "<option value=3>Class 1-3</option>";
		echo "<option value=4>Class 1-4</option>";
	}
	else {
		echo "<option value=''>All (including non-specified)</option>";
		if ($_SESSION['maxclass'] == 1) {
			echo "<option value=1 selected>Only Class 1</option>";
		}
		else {
			echo "<option value=1>Only Class 1</option>";
		}
		for ($j = 2;$j<=4;$j++) {
			if ($_SESSION['maxclass'] == $j) {
				echo "<option value=$j selected>Class 1-$j</option>";
			}
			else {
				echo "<option value=$j >Class 1-$j</option>";
			}
		}
	}
	echo "</select></td></tr>";
	echo "<tr><td class=clear><input type=submit class=button name=setAndOr value='Change'>&nbsp;&nbsp;<input type=submit class=button name='clearall' value='Clear All'></td></tr></table></form></p>";

	echo "</div>";
	echo "<div class=sectie>";
	echo "<h3>Matching Samples</h3>";
	if (count($_SESSION['searchpheno']) == 0) {
		echo "<p>At least one phenotypic characteristics has to be selected first.</p>";
	}
	else {
		$criteria = count($_SESSION['searchpheno']);
		echo "<p>";	
		mysql_select_db($cnvdb);
		$instring = substr($instring,0,-1);
		$query = mysql_query("SELECT s.chip_dnanr, s.gender, COUNT(sp.lddbcode) as matching, s.id AS sid, p.id AS pid, p.naam FROM projsamp ps JOIN sample s JOIN project p JOIN projectpermission pp JOIN sample_phenotypes sp ON ps.idsamp = s.id AND ps.idproj = p.id AND p.id = pp.projectid AND s.id = sp.sid WHERE sp.lddbcode IN ($instring) AND s.intrack = 1  AND pp.userid = '$userid' GROUP BY pid,sid ORDER BY matching DESC, s.chip_dnanr");
		echo "<table cellspacing=0>";
		$nrsamples = mysql_num_rows($query);
		$rowidx = 0;
		$found = 0;
		while ($row = mysql_fetch_array($query)) {
			$rowidx++;
			$sample = $row['chip_dnanr'];
			$gender = $row['gender'];
			$matching = $row['matching'];
			if ($andor == 'AND' && $matching < $criteria) {
				continue;
			}
			$found++;
			$sid = $row['sid'];
			$pid = $row['pid'];
			$project = $row['naam'];
			echo "<tr><th $firstcell id='nrsamples$found'>$rowidx/</th><th>$sample</th><th>Gender: $gender</th><th>Project:$project</th></tr>";
			#echo "sample $sample ($gender) has $matching items<br/>";
			$subarray = array();
			$subquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
			while ($subrow = mysql_fetch_array($subquery)) {
				$code = $subrow['lddbcode'];
				$pieces = explode('.',$code);
				$subarray[$pieces[0]][$pieces[1]][] = $pieces[2];
			}
			mysql_select_db($lddb);
			echo "<tr>";
			echo "<td colspan=4 class=clear><div style='padding-left:7px;'>"; #global div for phenotypes
			$subtablestring = '';
			$match = 0;
			foreach($subarray as $first => $sarray) {
				$ssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
				$ssubrow = mysql_fetch_array($ssubquery);
				$firstlabel = $ssubrow['LABEL'];
				foreach ($sarray as $second => $tarray) {
					if ($second == '00') {
						if ($_SESSION['searchpheno']["$first.00.00"] == 1) {
							$firstcellclass =  "style='border-left: 1px solid #a1a6a4;font-weight:bold'";
							$match++;
							$class = "style='font-weight:bold'";
						}
						else {
							$class = "";
							$firstcellclass = $firstcell;
						}
						$subtablestring .= "<tr><td $firstcellclass>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
					}
					else {
						$sssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
						$sssubrow = mysql_fetch_array($sssubquery);
						$secondlabel = $sssubrow['LABEL'];
						foreach ($tarray as $third) {
							if ($third == '00') {
								if ($_SESSION['searchpheno']["$first.$second.00"] == 1) {
									$firstcellclass =  "style='border-left: 1px solid #a1a6a4;font-weight:bold'";
									$match++;
									$class = "style='font-weight:bold'";
								}
								else {
									$firstcellclass = $firstcell;
									$class = "";
								}
	
								$subtablestring .= "<tr><td $firstcellclass>$firstlabel</td><td $class>$secondlabel</td><td>&nbsp;</td></tr>";	
							}
							else {
								$ssssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
								$ssssubrow = mysql_fetch_array($ssssubquery);
								$thirdlabel = $ssssubrow['LABEL'];
								if ($_SESSION['searchpheno']["$first.$second.$third"] == 1) {
									$class = "style='font-weight:bold;'";
									$firstcellclass =  "style='border-left: 1px solid #a1a6a4;font-weight:bold'";
									$match++;
								}
								else {
									$class = "";
									$firstcellclass = $firstcell;
	
								}
								$subtablestring.= "<tr><td $firstcellclass>$firstlabel</td><td $class>$secondlabel</td><td $class>$thirdlabel</td></tr>";
							}	
						}
					}
				}
			}
			echo "<a href='index.php?page=details&amp;sample=$sid&amp;project=$pid' target='_blank' class=nadruk>Go To Sample Details</a><br/>";
			echo "<span class=nadruk>Associated phenotypes:</span> ($match/$criteria query items found)";
			echo "<table cellspacing=0 style='padding-left:10px'>";
			echo "<tr><th $firstcell class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
			echo "</tr>";
			echo $subtablestring;
			echo "</table></div>"; # global div for phenotypes
			echo "</td>";
			mysql_select_db($cnvdb);
			echo "</tr>";
			echo "<tr>";
			echo "<td colspan=4 class=clear><div style='padding-left:7px;'>"; # global div for results
			$abquery = mysql_query("SELECT chr, start, stop, cn, class, inheritance FROM aberration WHERE sample = '$sid' AND idproj = '$pid' $classpart ORDER BY chr, start");
			$nrabs = mysql_num_rows($abquery);
			if ($nrabs == 0) {
				echo "<span class=nadruk>This sample does not have any aberrations passing classification restrictions.</span>";
				echo "</div></td></tr>";
				continue;
			}
			
			echo "<span class=nadruk>Classified Aberrations:</span>";
			echo "<table cellspacing=0>";
			#echo "<tr><th $firstcell class=topcellalt>Overview</th><th class=topcellalt>List</th>\n";
			echo "<tr>";
			echo "<td class=clear><img src='karyo.php?sid=$sid&amp;pid=$pid'></td>";
			echo "<td class=clear><div>"; # nested div for results 
			echo "<table cellspacing=0><tr><th class=topcellalt $firstcell>Region</th><th class=topcellalt>CN</th><th class=topcellalt>DC</th><th class=topcellalt>Inheritance</th></tr>";
					
			while ($abrow = mysql_fetch_array($abquery)) {
				$chr = $chromhash[$abrow['chr']];
				$start = $abrow['start'];
				$stop = $abrow['stop'];
				$region = "Chr$chr:".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
				$cn = $abrow['cn'];
				$class = $abrow['class'];
				$inheritance = $inh[$inh[$abrow['inheritance']]];
				#$inhe = $abrow['inheritance'];
				echo "<tr><td $firstcell>$region</td><td>$cn</td><td>$class</td><td>$inheritance</td></tr>";
			}
			echo "</table></div>";  # nested div for results
			echo "</td>";
			echo "</tr>";
			echo "</table></div>"; #global div for results
			echo "</td></tr>";
				
		
			
	
		}
		echo "</table>";
		if ($found == 0 ) {
			echo "<p> No matching samples found. Make your criteria less stringent and try again.</p>";
		}
		else {
			echo "<span id=nrfound style='display:none;'>$found</span>";
			#echo "<script type='text/javascript'>for(i=1;i<=$found;i++){document.getElementById('nrsamp'+i).innerHTML = document.getElementById('nrsamp'+i).innerHTML + '$found';};</script>";
		}	
	}
	mysql_select_db($cnvdb);
	
		
}
?>
