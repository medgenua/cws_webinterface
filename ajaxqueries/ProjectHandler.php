<?php

#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "CNVanalysis".$_SESSION['dbname'];
mysql_select_db("$db");

$userid = $_GET['uid'];
$action = $_GET['a'];
$to_delete = $_GET['s'];


// DELETE
if ($action == 'd') {
	$pids = explode(",",$_GET['s']);
	$pidstr = '';
	$error = '';
	$ok = '';
	$warning = '';
	// double check permissions.

	//echo "<p>DEBUG</p>";
	//exit;

	foreach($pids as $pid) {
		if (!is_numeric($pid)) {continue;}
		$query = mysql_query("SELECT p.naam, pm.editsample FROM projectpermission pm JOIN project p ON p.id = pm.projectid WHERE pm.projectid = '$pid' AND pm.userid = '$userid'");
		if (mysql_num_rows($query) == 0) {
			$error .= "<li>Invalid ProjectID: $pid</li>";
			continue;
		}
		$row = mysql_fetch_array($query,MYSQL_ASSOC);

		$countquery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS number FROM aberration WHERE idproj = $pid");
		$qres = mysql_fetch_array($countquery);
		$samples = $qres['number'];

		if ($row['editsample'] == 1) {
			$pidstr .= "$pid,";
			if ($samples > 0) {
				$warning .= "<li style='color:#fff'>".$row['naam']." (".$samples." samples)</li>";
			}
			else {
				$ok .= "<li style='color:#fff'>".$row['naam']." (".$samples." samples)</li>";
			}

		}
		else {
			$error .= "<li style='color:#fff'>".$row['naam']." (".$samples." samples)</li>";
		}
	}
	if ($pidstr == '') {
		echo "<div class='sectie' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
		echo "<p>Invalid ProjectIDs provided, or no access to the selected projects.</p>";
		echo "<input type=button name='Close' value='Close' onClick=\"document.getElementById('overlay').style.display='none'\">";
		echo "</div>";
		exit;
	}
	$pidstr = substr($pidstr,0,-1);

	echo "<div class='sectie' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
	echo "<h3 style='text-align:left;'>Confirmation Needed</h3>";
	echo "<p  style='text-align:left;'>Please confirm that the following projects should be deleted?</p>";
	echo "<p  style='text-align:left;'><ol class=twocol>";
	echo "$ok";
	echo "</ol></p>";

	if ($warning != '') {
		echo "<p  style='color: #FF4500;text-align:left;'>WARNING: the following projects still contain samples:<ol class=twocol>";
		echo "$warning";
		echo "</ol></p>";
	}
	if ($error != '') {
		echo "<p  style='text-align:left;'>The following projects cannot be deleted due to insufficient permissions:<ol class=twocol>";
		echo "$error";
		echo "</ol></p>";
	}
	echo "<form action='' method=GET>";
	echo "<input type=hidden id='todelete' value='$pidstr'>";
	echo "<p  style='text-align:left;'><input type=button name='DeleteYes' value='Yes, Delete Projects' OnClick='ConfirmedDeleteProjects($userid)'> &nbsp; <input type=button name='DeleteNo' value='No, Keep Them' onClick=\"document.getElementById('overlay').style.display='none'\"></form></p>";
	echo "</div>";
	exit;

}


// confirmed deletion => actual deletion !
if ($action == 'cd') {
	$pids = explode(",", $_GET['s']);
	$pidstr = '';
	$error = '';
	$ok = '';

	// DOUBLE CHECK PERMISSIONS.
	foreach($pids as $pid) {
		if (!is_numeric($pid)) {continue;}
		$query = mysql_query("SELECT p.naam, pm.editsample FROM projectpermission pm JOIN project p ON p.id = pm.projectid WHERE pm.projectid = '$pid' AND pm.userid = '$userid'");
		if (mysql_num_rows($query) == 0) {
			$error .= "<li>Invalid ProjectID: $pid</li>";
			continue;
		}
		$row = mysql_fetch_array($query,MYSQL_ASSOC);
		$pname = $row['naam'];
		$editsample = $row['editsample'];

		if ($editsample == 1) {
			#echo "Deleting '$pname'<br/>";
			
			# datapoints
			$abs = mysql_query("SELECT id FROM aberration WHERE idproj = '$pid'");
			while ($row = mysql_fetch_array($abs)) {
				$aid = $row['id'];
				$del = mysql_query("DELETE FROM datapoints WHERE id = '$aid'");
	
			}
			$abs = mysql_query("SELECT id FROM aberration_LOH WHERE idproj = '$pid'");
			while ($row = mysql_fetch_array($abs)) {
				$aid = $row['id'];
				$del = mysql_query("DELETE FROM datapoints_LOH WHERE id = '$aid'");
	
			}
	
			# plots (and delete the files themselve)
			$files = mysql_query("SELECT filename FROM plots WHERE idproj = '$pid'");
			// you really need filenames from table, because joining and renaming projects can change pid for a file.
			while (list($filename) = mysql_fetch_array($files)) {
				unlink("Whole_Genome_Plots/$filename");
			}
			$del = mysql_query("DELETE FROM plots WHERE idproj = '$pid'");
			$files = mysql_query("SELECT filename FROM BAFSEG WHERE idproj = '$pid'");
			while (list($filename) = mysql_fetch_array($files)) {
				unlink("Whole_Genome_Plots/$filename");
			}
			$del = mysql_query("DELETE FROM BAFSEG WHERE idproj = '$pid'");
			# aberrations
			$del = mysql_query("DELETE FROM aberration WHERE idproj = '$pid'");
			$del = mysql_query("DELETE FROM aberration_LOH WHERE idproj = '$pid'");
			#Prioritizations
			$prs = mysql_query("SELECT id FROM prioritize WHERE project = '$pid'");
			while ($row = mysql_fetch_array($prs)) {
				$prid = $row['id'];
				$del = mysql_query("DELETE FROM priorabs WHERE prid = '$prid'");
	
			}
			$del = mysql_query("DELETE FROM prioritize WHERE project = '$pid'");
			#project permissions
			$del = mysql_query("DELETE FROM projectpermission WHERE projectid = '$pid'");
			# group permissions
			$del = mysql_query("DELETE FROM projectpermissiongroup WHERE projectid = '$pid'");
			# from UPD tables
			$sel = mysql_query("SELECT id FROM parents_upd WHERE spid = '$pid'");
			$ids = "";
			while ($row = mysql_fetch_array($sel)) {
				$ids .= $row['id'] . ",";
			}
			$ids = substr($ids,0,-1);
			$del = mysql_query("DELETE FROM parents_upd WHERE spid = '$pid'");
			$del = mysql_query("DELETE FROM parents_upd_datapoints WHERE id IN ($ids)");
			# project samples & samples
			$samples = mysql_query("SELECT idsamp FROM projsamp WHERE idproj = '$pid'");
			while ($row = mysql_fetch_array($samples)) {
				$sid = $row['idsamp'];
				$details = mysql_query("SELECT intrack, trackfromproject, chip_dnanr FROM sample WHERE id = '$sid'");
				$detrow = mysql_fetch_array($details);
				$intrack = $detrow['intrack'];
				$tfp = $detrow['trackfromproject'];
				$sample = $detrow['chip_dnanr'];
				// delete family relations and all related data.
				$q = mysql_query("SELECT id FROM parents_relations WHERE father = '$sid' AND father_project = '$pid'");
				if (mysql_num_rows($q) > 0) {
					mysql_query("UPDATE parents_relations SET father = 0, father_project = 0 WHERE father = '$sid' AND father_project = '$pid'");
					$r = mysql_query("SELECT id FROM parents_regions WHERE sid = '$sid'");
					while ($rr = mysql_fetch_array($r)) {
						mysql_query("DELETE FROM parents_datapoints WHERE id = '".$rr['id']."'");
					}
					mysql_query("DELETE FROM parents_regions WHERE sid = '$sid'");
					mysql_query("DELETE FROM parent_offspring_cnv_relations WHERE parent_sid = '$sid'");
				}
				$q = mysql_query("SELECT id FROM parents_relations WHERE mother = '$sid' AND mother_project = '$pid'");
				if (mysql_num_rows($q) > 0) {
					mysql_query("UPDATE parents_relations SET mother = 0, mother_project = 0 WHERE mother = '$sid' AND mother_project = '$pid'");
					$r = mysql_query("SELECT id FROM parents_regions WHERE sid = '$sid'");
					while ($rr = mysql_fetch_array($r)) {
						mysql_query("DELETE FROM parents_datapoints WHERE id = '".$rr['id']."'");
					}
					mysql_query("DELETE FROM parents_regions WHERE sid = '$sid'");
					mysql_query("DELETE FROM parent_offspring_cnv_relations WHERE parent_sid = '$sid'");
				}
				// determine if the sample entry can be fully deleted (present in other samples?
				if ($intrack == 1 && $tfp == $pid) {
					$alternatives = mysql_query("SELECT ps.idproj, p.naam, p.created FROM project p JOIN projsamp ps ON ps.idproj = p.id WHERE ps.idsamp = '$sid'  AND  NOT ps.idproj = '$pid' ORDER BY ps.idproj DESC");
					if (mysql_num_rows($alternatives) > 0) {
						$formstring .= "Select an alternative for $sample :<select name=$sid>";
						$sidstring .= "$sid"."_";
						$needsupdate = 1;
						#echo "$sample needs alternative : <br>";
						# delete from projsamp, and update table sample to most recent project by default,  and create form to update table if other project is wanted
						$selected = "selected";
						while ($prow  = mysql_fetch_array($alternatives)) {
							$altid =$prow['idproj'];
							$altname = $prow['naam'];
							$altcreated = $prow['created'];
							#echo " - $altid : $altname<br>";
							if ($selected != "") {
								// update track_from_project
								$update = mysql_query("UPDATE sample SET trackfromproject = '$altid' WHERE id = '$sid'");
								$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
								$formstring .="<option value='$sid-$altid' $selected>$altname ($altcreated)</option>";
								$selected = '';
							}
							else {
								$formstring .= "<option value='$sid-$altid'>$altname ($altcreated)</option>";
							}
	
						}
						$formstring .= "</select><br>\n";
	
					}
					else {
						//echo "$sample will be deleted (no other projects<br>";
						$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
						$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
						$delete = mysql_query("DELETE FROM parents_relations WHERE id = '$sid'");
						# delete from projsamp &amp; sample
					}
	
				}
				elseif ($intrack == 1) {
					# delete from projsamp ?
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
					continue;
					# in track from different project (assume it exists)
	
				}
				else{
					# not in track, check if it is included in a different project
					$checkproj = mysql_query("SELECT idproj FROM projsamp WHERE idsamp = '$sid' AND NOT idproj = '$pid'");
					if (mysql_num_rows($checkproj) > 0) {
						#echo "$sample not in track but in other projects, not deleting completely<br>";
						#only delete from projsamp
						$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
					}
					else {
						#echo "$sample not in track nor other projects, deleting completely<br>";
						#delete from projsamp &amp; sample
						$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
						$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
						$delete = mysql_query("DELETE FROM parents_relations WHERE id = '$sid'");
	
					}
				}
			}
	
			$delete = mysql_query("DELETE FROM project WHERE id = '$pid'");
		}
	


	}

	// give feedback.
	echo "<div class='sectie' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff;text-align:left;'>";
	echo "<h3 style='text-align:left;'>Projects deleted</h3>";
	echo "<p>The projects were deleted. You can now close this screen</p>";
	echo "<p  style='text-align:left;'><input type=button name='close' value='Close' onClick=\"document.getElementById('overlay').style.display='none';location.reload()\"></form></p>";
	echo "</div>";
	exit;




}

?>
