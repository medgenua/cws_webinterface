<?php

#######################
# CONNECT TO DATABASE #
#######################
include('../.LoadCredentials.php');
$db = "CNVanalysis".$_SESSION['dbname'];
mysql_select_db("$db");

 // data = { cts: cts, classes: classes, snps: snps, prefix: prefix }

$cts = $_POST['cts'];
$classes = $_POST['classes'];
$minsnps = $_POST['minsnps'];
$maxsnps = $_POST['maxsnps'];
$prefix = $_POST['prefix'];
$uids = $_POST['uids'];
$sourcedb = $_POST['sourcedb'];

$select =  "SELECT f.ID ";
$from = " FROM `GenomicBuilds`.`FailedLifts` f JOIN `$sourcedb`.`aberration` a ";
$on = " ON f.itemID = a.id ";
$where = "WHERE f.tablename = 'aberration' AND f.uid IN ($uids) ";

$query = mysql_query("$select $from $on $where");
$nr_ini = mysql_num_rows($query);


// chiptypes set ? 
if ($cts !== "" ) {
    $from .= " JOIN `$sourcedb`.`project` p ";
    $on .= " AND p.id = a.idproj";
    $where .= " AND p.chiptypeid IN ($cts)";
}
// classes set ? 
if ($classes !== "") {
    $classes_array = explode(",",$classes);
    if (in_array('0',$classes_array)) {
        $where .= " AND ( a.class IN ($classes) OR a.class IS NULL) ";
    }
    else {
        $where .= " AND a.class IN ($classes)";

    }
}
// snps set ? 
if ($minsnps > 0) {
    $where .= " AND a.nrsnps >= '$minsnps'";
}
if ($maxsnps > 0) {
    $where .= " AND a.nrsnps <= '$maxsnps'";
}
// prefix set ? 
if ($prefix !== "") {
    $from .= " JOIN `$sourcedb`.`sample` s ";
    $on .= " AND a.sample = s.id " ;
    $where .= " AND s.chip_dnanr LIKE '$prefix%'";
}

// full query 
$query = "$select $from $on $where LIMIT 1000";
//error_log($query);
$q = mysql_query($query);
while (mysql_num_rows($q) > 0) {
    $ids = array();
    while ($r = mysql_fetch_array($q)) {
        array_push($ids,$r['ID']);
    }
    $dq = "DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE ID in (".implode(',',$ids).")";
    mysql_query($dq);
    $q = mysql_query($query);
}


// report : final remaining:
$select =  "SELECT f.ID AS result ";
$from = " FROM `GenomicBuilds`.`FailedLifts` f JOIN `$sourcedb`.`aberration` a ";
$on = " ON f.itemID = a.id ";
$where = "WHERE f.tablename = 'aberration' AND f.uid IN ($uids) ";

$query = mysql_query("$select $from $on $where");
$nr_after = mysql_num_rows($query);

echo "Nr Variants to curated reduced from $nr_ini to $nr_after";
