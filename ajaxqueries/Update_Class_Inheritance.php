<?php

header('Content-Type: application/json');

include('../.LoadCredentials.php');


$db = "CNVanalysis".$_SESSION['dbname'];
mysql_select_db("$db");

$userid = $_SESSION['userID'];

// chromhash
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

// inheritance array
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

// diagnostic classes from db
$dc_query = mysql_query("SELECT id, name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
}




$data = json_decode($_POST['data'], true);
$sid = $_POST['sid'];
$pid = $_POST['pid'];

// SET class & inheritance
$toconfirm = '';
$query = mysql_query("SELECT id, class, chr, start, stop, inheritance FROM aberration WHERE sample = '$sid' AND idproj = '$pid' ORDER BY chr ASC , start ASC ");
while ($row = mysql_fetch_array($query)){
    $aid = $row['id'];
    $origclass = $row['class'];
    if ($origclass == '') {
        $origclass = 0;
    }

    $setchr = $row['chr'];
    $setchrtxt = $chromhash[$setchr];
    $setstart = $row['start'];
    $setstop = $row['stop'];		
    $previnh = $row['inheritance'];
    // new posted class 
    $class = $data[$aid]['class'];
    if ($class != $origclass) {
        /*
         needs confirmation when:
          - changing previous set class
          - changing to FP
          - resetting class
        */
        if ($origclass > 0) {
            $toconfirm .= "$aid-$origclass-$class,";
        }
        elseif ($class == 5) {
            $toconfirm .= "$aid-$origclass-$class,";
        }
        elseif ($class == 0) {
            $toconfirm .= "$aid-$origclass-$class,";
        }

        else {
            if ($origclass == 0) {
                $logentry = "Diagnostic Class set to ".$dc[$class];
            }
            else {
                $logentry = "Diagnostic Class changed from ".$dc[$origclass]." to ".$dc[$class];
            }
    
            mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
            $update = mysql_query("UPDATE aberration SET class=$class WHERE id = '$aid'");
        }
    }
    $inheritance = $data[$aid]['inheritance'];
    if ($inheritance != $previnh) {
            $logentry = "Inheritance Changed From ". $inh[$inh[$previnh]]." to ". $inh[$inh[$inheritance]];
            mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
            $update = mysql_query("UPDATE aberration SET inheritance=$inheritance WHERE id = '$aid'");
    }

}

if ($toconfirm != '') {
    $toconfirm = substr($toconfirm,0,-1);
    error_log("Need confirmation: $toconfirm");
}

echo json_encode( array("status" => "ok", "toconfirm" => $toconfirm));


