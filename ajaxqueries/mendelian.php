<?php
session_start();
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
include('../.LoadCredentials.php');
## always log in to the default current build.          
if ($_SESSION['dbname'] == '') {
        // this seems to exlude some sort of race condition, where session is not starting correctly (i think, I had missing SESSION vars)
        mysql_select_db('GenomicBuilds');
        $query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
        $row = mysql_fetch_array($query);
        $_SESSION['dbname'] = $row['name'];
        $_SESSION['dbstring'] = $row['StringName'];
        $_SESSION['dbdescription'] = stripslashes($row['Description']);
}
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

$sid = $_GET['s'];
$chr = $_GET['c'];
$famq = mysql_query("SELECT mendelian_errors FROM parents_relations WHERE id = '$sid'");
// get sample ids & projects
$frow = mysql_fetch_array($famq);
$merr = $frow['mendelian_errors'];
$merrs = explode('@@@',$merr);
echo "<p>The prediction is based on $merrs[1] mendelian inconsistencies, in combination with $merrs[2] putative Maternal UPD regions versus $merrs[3] putative Paternal UPD regions (these regions were not tested for significance yet). An overview of all mendelian errors is listed below.</p>";
echo "<p>Show Mendelian errors for chromosome: ";
for ($i = 1; $i <= 22; $i++) {
	echo "<a href='javascript:void()' onClick=\"LoadMerr('$sid','$i')\">$i</a> ";
}
echo "</p>";
$errtable = "<p><table cellspacing=0>";
$errtable .= "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>Type</th><th class=topcellalt>Child</th><th class=topcellalt>Father</th><th class=topcellalt>Mother</th><th class=clear>&nbsp;&nbsp;</th>";
$errtable .= "<th class=topcellalt $firstcell>Position</th><th class=topcellalt>Type</th><th class=topcellalt>Child</th><th class=topcellalt>Father</th><th class=topcellalt>Mother</th></tr>";
$errgts = explode("\n",$merrs[4]);
$lines = array();
$item = 0;
foreach ($errgts as $key => $line) {
       	$p = explode(";",$line);
	if ($p[0] != $chr) {
		continue;
	}
	$lines[$item] = "<td $firstcell>Chr$p[0]:$p[1]</td><td>$p[2]</td><td>$p[3]</td><td>$p[4]</td><td>$p[5]</td>";
	$item++;
       	#$errtable .= "<tr><td $firstcell>$p[0]</td><td>$p[1]</td><td>$p[2]</td><td>$p[3]</td><td>$p[4]</td><td>$p[5]</td></tr>";
}
if (count($lines) & 1) {
	$lines[$item] = "<td $firstcell>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
}
$half = count($lines)/2;
for ($i = 0; $i < $half; $i++) {
	$errtable .= "<tr>".$lines[$i]."<td class=clear>&nbsp;</td>".$lines[($half+$i)]."</tr>";
} 
$errtable .= "</table>";
echo $errtable;
?>
