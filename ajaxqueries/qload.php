<?php
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
include('../.LoadCredentials.php');
## always log in to the default current build.          
if ($_SESSION['dbname'] == '') {
        // this seems to exlude some sort of race condition, where session is not starting correctly (i think, I had missing SESSION vars)
        mysql_select_db('GenomicBuilds');
        $query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
        $row = mysql_fetch_array($query);
        $_SESSION['dbname'] = $row['name'];
        $_SESSION['dbstring'] = $row['StringName'];
        $_SESSION['dbdescription'] = stripslashes($row['Description']);
}
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

## user id 
$userid = $_SESSION['userID'];
$level = $_SESSION['level'];

// get the currently running projects. 
$result = `ps aux | grep perl | grep MainProgram`;
$result = explode("\n",$result);
$runningpids = array();
foreach ($result as $string) {
	if (!preg_match('/MainProgram\.pl/',$string)) {
		continue;
	}
	$random = preg_replace('/(.*pl\s-r\s)(.{5})/','$2',$string);
	$fh = fopen("$scriptdir/output/$random.pid",'r');
	$pid = fgets($fh);
	rtrim($pid);
	$runningpids[$pid] = 1;
}
// During startup : print the currently running projects. 
$query = mysql_query("SELECT p.id, p.chiptype, p.created, p.userID, COUNT(ps.idsamp) AS samples, u.LastName, u.FirstName FROM project p JOIN projsamp ps JOIN users u ON p.id = ps.idproj AND p.userID = u.id WHERE p.finished = 0 GROUP BY ps.idproj ORDER BY p.created ASC");
echo "<span style='float:right'><a href='javascript:void(0);' onClick=\"divHide('qoverview')\">Hide</a> | <a href='javascript:void(0);' onClick=\"reloadqueue('qoverview')\">Refresh</a></span>";
echo "<div class=sectie>";
echo "<h3>Analysis Queue Overview</h3>";
if (mysql_num_rows($query) == 0) {
	echo "<p>There are no running projects. Your analysis will be able to start immediately.</p>";
}
else {
	echo "<p><table cellspacing=0 style='margin-left:3em;width:90%'>";
	echo "<tr>";
	echo "  <th class=light>Started</th>";
	if ($level > 2) {
		echo "<th class=light>Owner</th>";
	}
	echo "  <th class=light>ChipType</th>";
	echo "  <th class=light># Samples</th>";
	
	echo "</tr>";
	$printed = 0;
	while ($row = mysql_fetch_array($query)) {
		$chip = $row['chiptype'];
		$created = $row['created'];
		$user = $row['userID'];
		$samples = $row['samples'];
		$uname = $row['LastName'] . ' ' . $row['FirstName'];
		$bold = '';
		$pid = $row['id'];
		if (!array_key_exists($pid,$runningpids)) {
			#project is not running, update db.
			mysql_query("UPDATE project SET finished = 1 WHERE id = '$pid'");
			continue;
		}
		$printed++;
		if ($user == $userid) {
			$bold = "style='font-weight:bold;'";
		}
		echo "<tr>";
		echo "  <td class=light $bold>$created</td>";
		if ($level > 2) {
			echo "<td class=light $bold>$uname</td>";
		}
		echo "  <td class=light $bold>$chip</td>";
		echo "  <td class=light $bold>$samples</td>";
		echo "</tr>";
	}
	if ($level <= 2) {
		echo "<tr><td class=last colspan=3>&nbsp;</td></tr>";
	}
	else {
		echo "<tr><td class=last colspan=4>&nbsp;</td></tr>";
	}
	echo "</table></p>";
	if ($printed == 0) {
		echo "<p>There are no running projects. Your analysis will be able to start immediately.</p>";
	}
}
echo "</div>";



?>
