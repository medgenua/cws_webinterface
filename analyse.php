<?php
include('.LoadCredentials.php');
ob_start();
#########################
## universal variables ##
#########################
$projectname = $_POST["TxtName"];
$projectname = str_replace(' ', '_', $projectname); 
$username = $_POST["username"];
$chiptype = $_POST["chiptype"];

/////////////////
// MULTI SETUP //
/////////////////
if ($_GET['type'] == 'multiple') {
	# posted variables
	$rand = $_POST['rand'];
	$usepedigree = $_POST['usepedigree'] || 0;
	if ($usepedigree == '') {
		$usepedigree = 0;
	}
 	$group = $_POST['group'];
	# asymetric filter settings
 	$asym = $_POST['asym'];
	$asymminconf = $_POST['asymminconf'];
	$asymmethod = $_POST['asymmethod'];
	$asymtype = $_POST['asymtype'];
	$plink = $_POST['plink'];
  	$doY = $_POST['doY'];
	$sharegroup = $_POST['sharegroup'];
	$genderzip = $_POST['genderzip'];
	$pedigreezip = $_POST['pedzip'];
	$datazips = explode(',',$_POST['datazips']);
	$nrsup = $_POST['nrsupdata'];
	$triPOD = $_POST['triPOD'];
	$datafile = file_get_contents('php://input');
 	$variables = explode("&", $datafile);
	## preprocess	
	//$target="$scriptdir/datafiles/CNV-WebStore/runtime/";
	$target = $config['DATADIR'];
 	$minsnp = $_POST['minsnp'];
 	$num = count($variables);
 	$fh = fopen("$scriptdir/output/"."$projectname" ."_fortrack.txt", 'w');
 	#for ($i = 0; $i<$num;$i+=1) {
 	#	if (preg_match("/=on$/", $variables[$i])) {
	#		$sampleid = preg_replace('/(.+)=on/', "$1",$variables[$i]);
    	#		fwrite($fh, "$sampleid\n");
    	#	}
 	#}
	$trackcheck = $_POST['track'];
	foreach($trackcheck as $sname => $value) {
		//echo "$sname => $value<br/>";
		fwrite($fh, "$sname\n");
	}
 	fclose($fh);
	## write out zipcodings
	$fh = fopen("$target$rand.zipcoding.txt",'w');
	fwrite($fh,"gender\t$genderzip\n");
	fwrite($fh,"pedigree\t$pedigreezip\n");
	foreach ($datazips as $key => $value) {
		fwrite($fh,"data.$key\t$value\n");
	}
	fclose($fh);
	## set file permissions
	system("chmod 777 $target$rand.*");
	$checkupd = 1;
	$fh = fopen("$scriptdir/output/$rand.out",'w');
	fwrite($fh,"Starting up\n");
	fclose($fh);
	system("chmod 777 $scriptdir/output/$rand.out");
	############################
	## WRITE OUT SETTINGS XML ##
	############################
	$fh = fopen("$target$rand.settings.xml",'w');
	fwrite($fh,"<settings>\n");
	$domain = $_SERVER['HTTP_HOST'];
	$baselink = "https://$domain/$basepath/";
	fwrite($fh,"<webhost>$baselink</webhost>\n");
	# general settings
	fwrite($fh,"<general>\n");
	fwrite($fh,"\t<projectname>$projectname</projectname>\n");
	fwrite($fh,"\t<chiptype>$chiptype</chiptype>\n");
	fwrite($fh,"\t<minsnp>$minsnp</minsnp>\n");
	fwrite($fh,"\t<group>$group</group>\n");
	fwrite($fh,"\t<username>$username</username>\n");
	fwrite($fh,"\t<asym>$asym</asym>\n");
	fwrite($fh,"\t<asymminconf>$asymminconf</asymminconf>\n");
	fwrite($fh,"\t<asymmethod>$asymmethod</asymmethod>\n");
	fwrite($fh,"\t<asymtype>$asymtype</asymtype>\n");
	fwrite($fh,"\t<usepedigree>$usepedigree</usepedigree>\n");
	fwrite($fh,"\t<checkupd>$checkupd</checkupd>\n");
	fwrite($fh,"\t<plink>$plink</plink>\n");
	fwrite($fh,"\t<triPOD>$triPOD</triPOD>\n");
	fwrite($fh,"\t<doY>$doY</doY>\n");
	fwrite($fh,"\t<sharegroup>$sharegroup</sharegroup>\n");
	fwrite($fh,"\t<uminsnp>3</uminsnp>\n");  // minimal informative snps for upd
	fwrite($fh,"\t<datafiles>$nrsup</datafiles>\n");
	fwrite($fh,"\t<methods>QuantiSNPv2;PennCNVas;PennCNVx;VanillaICE</methods>\n");
	fwrite($fh,"\t<inputcols>".$_POST['neededcols']."</inputcols>\n");
	fwrite($fh,"</general>\n");
	# multi setup : include full settings for default algorithms
	$methods = array("QuantiSNPv2", "PennCNVas", "PennCNVx", "VanillaICE");
	foreach ($methods as $key => $method) {
		$xmlfile = "$scriptdir/Analysis_Methods/Configuration/$method.xml";
		fwrite($fh,"<$method>\n");
		$xmlstring = file_get_contents($xmlfile);
		fwrite($fh,$xmlstring);
		fwrite($fh,"</$method>\n");
	}
	if ($plink == 1) {
		$method = 'PlinkHomoz';
		$xmlfile = "$scriptdir/Analysis_Methods/Configuration/$method.xml";
		fwrite($fh,"<$method>\n");
		$xmlstring = file_get_contents($xmlfile);
		fwrite($fh,$xmlstring);
		fwrite($fh,"</$method>\n");
	}
	if ($triPOD == 1) {
		$method = 'triPOD';
		$xmlfile = "$scriptdir/Analysis_Methods/Configuration/$method.xml";
		fwrite($fh,"<$method>\n");
		$xmlstring = file_get_contents($xmlfile);
		fwrite($fh,$xmlstring);
		fwrite($fh,"</$method>\n");
	}

	fwrite($fh,"</settings>\n");
	fclose($fh);
	#####################
	## LAUNCH ANALYSIS ##
	#####################
	if ($usedrmaa == 1) {
		$command = "(COLUMNS=1024 && echo \"1\" > status/status.$rand && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir && $scriptdir/Bin/MainProgram.pl -r '$rand' >> $scriptdir/output/$rand.out\" 2>&1 && echo \"0\" > status/status.$rand && chmod a+rw status/status.$rand) > /dev/null & ";

		#echo "$command<br/>";
		system($command);
		
	}
	else {
		echo "I have to adapt the code, stay tuned.";
		exit();
	}
	
	header("Location: index.php?page=result&type=multiple&r=$rand");

}

/*
/////////////////////
// QUANTISNP SETUP //
/////////////////////
if ($_GET['type'] == 'quantisnp') {
 $data = $_POST["data"];
 $datafilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$data);
 $gender = $_POST["gender"];
 $genderfilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$gender);
 $config = $_POST["config"];
 $emiters = $_POST["emiters"];
 $lsetting = $_POST["lsetting"];
 $maxcopy = $_POST["maxcopy"];
 $dogccorrect = $_POST["dogccorrect"];
 $gcdir = $_POST["gcdir"];
 $printrs = $_POST["printrs"];
 $minsnp = $_POST["minsnp"];
 $minlbf = $_POST["minlbf"];
 $datafile = file_get_contents('php://input');
 $variables = explode("&", $datafile);
 $num = count($variables);
 ## local installation uses QuaniSNP2
 if ($username == $username) {
 	system("(chmod 777 $data && chmod 777 $gender && echo \"1\" > status/status.$projectname && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd /var/tmp/datafiles && dos2unix \"$data\" && dos2unix \"$gender\" && sed '1n; s/,/\./g' \"$data\" > tmp.txt && mv tmp.txt \"$data\" && sed '1n; s/,/\./g' \"$gender\" > tmp.txt && mv tmp.txt \"$gender\" && cd $scriptdir/QuantiSNPv2 && ./QuantiSNP_V2.pl -d \"$data\" -g \"$gender\" -p \"$projectname\" -C \"$config\" -e \"$emiters\" -L \"$lsetting\" -M \"$maxcopy\" -G \"$dogccorrect\" -l \"$gcdir\" -R \"$printrs\" -s \"$minsnp\" -S \"$minlbf\" -u \"$username\" -c \"$chiptype\" > $projectname.out\" && rm $data && rm $gender && echo \"0\" > status/status.$projectname && chmod a+rw status/status.$projectname) > /dev/null & ");
 }
 else {
	system("(chmod 777 $data && chmod 777 $gender && echo \"1\" > status/status.$projectname && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd /var/tmp/datafiles && dos2unix \"$data\" && dos2unix \"$gender\" && sed '1n; s/,/\./g' \"$data\" > tmp.txt && mv tmp.txt \"$data\" && sed '1n; s/,/\./g' \"$gender\" > tmp.txt && mv tmp.txt \"$gender\" && cd $scriptdir/QuantiSNP && ./QuantiSNP_build_dep.pl -d \"$data\" -g \"$gender\" -p \"$projectname\" -C \"$config\" -e \"$emiters\" -L \"$lsetting\" -M \"$maxcopy\" -G \"$dogccorrect\" -l \"$gcdir\" -R \"$printrs\" -s \"$minsnp\" -S \"$minlbf\" -u \"$username\" -c \"$chiptype\" > $projectname.out\" && rm $data && rm $gender && echo \"0\" > status/status.$projectname && chmod a+rw status/status.$projectname) > /dev/null & ");
 }
  header("Location: index.php?page=result&project=$projectname&type=quantisnp");
}

///////////////////
// PENNCNV SETUP //
///////////////////
if ($_GET['type'] == 'penncnv' ) {
 $data = $_POST["data"];
 $datafilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$data);
 $gender = $_POST["gender"];
 $genderfilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$gender);
 $minsnp = $_POST['minsnp'];
 $minconf = $_POST['minconf'];
 $datafile = file_get_contents('php://input');
 $variables = explode("&", $datafile);
 $num = count($variables);
 system("(chmod 777 $data && chmod 777 $gender && echo \"1\" > status/status.$projectname && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd /var/tmp/datafiles/ && dos2unix \"$data\" && dos2unix \"$gender\" && sed '1n; s/,/\./g' \"$data\" > tmp.txt && mv tmp.txt \"$data\" && sed '1n; s/,/\./g' \"$gender\" > tmp.txt && mv tmp.txt \"$gender\" && cd $scriptdir/PennCNV/ && ./PennCNV_build_dep.pl -d \"$data\" -g \"$gender\" -p \"$projectname\" -s \"$minsnp\" -S \"$minconf\" -u \"$username\" -c \"$chiptype\" > $projectname.out\" && rm $data && rm $gender && echo \"0\" > status/status.$projectname && chmod a+rw status/status.$projectname) > /dev/null & ");
  header("Location: index.php?page=result&project=$projectname&type=penncnv");

}
/////////////////
// FASEG SETUP //
/////////////////
if ($_GET['type'] == 'faseg') {
 $data = $_POST["data"];
 $datafilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$data);
 $gender = $_POST["gender"];
 $genderfilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$gender);
 $minsnp = $_POST['minsnp'];
 $span = $_POST['span'];
 $sig = $_POST['sig'];
 $delta = $_POST['delta'];
 $datafile = file_get_contents('php://input');
 $variables = explode("&", $datafile);
 $num = count($variables);
 $fh = fopen("$scriptdir/FASeg/"."$projectname" ."_analyze.txt", 'w');
 for ($i = 0; $i<$num;$i+=1) {
    if (preg_match("/=on$/", $variables[$i])) {
	$sampleid = preg_replace('/(.+)=on/', "$1",$variables[$i]);
    	fwrite($fh, "$sampleid\n");
    }
 }
 fclose($fh);
 system("(chmod 777 $data && chmod 777 $gender && echo \"1\" > status/status.$projectname && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $scriptdir/FASeg/ && dos2unix \"$data\" && dos2unix \"$gender\" && sed '1n; s/,/\./g' \"$data\" > tmp.txt && mv tmp.txt \"$data\" && sed '1n; s/,/\./g' \"$gender\" > tmp.txt && mv tmp.txt \"$gender\" && ./faseg_mt.pl \"$data\" \"$gender\" \"$projectname\" \"$span\" \"$sig\" \"$delta\" \"$minsnp\" \"$username\" > $projectname.out\" && rm $data && rm $gender && echo \"0\" > status/status.$projectname && chmod a+rw status/status.$projectname) > /dev/null & ");
  header("Location: index.php?page=result&project=$projectname&type=faseg");


}

//////////////////////
// VANILLAICE SETUP //
//////////////////////
if ($_GET['type'] == 'vanillaice') {
 $data = $_POST["data"];
 $datafilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$data);
 $gender = $_POST["gender"];
 $genderfilename = preg_replace(''.$scriptdirreg.'\/datafiles\/(.+)/','$1',$gender);
 $minsnp = $_POST['minsnp'];
 $taufactor = $_POST['taufactor'];
 $hmm = $_POST['hmm'];
 $variance = $_POST['variance'];
 $datafile = file_get_contents('php://input');
 $variables = explode("&", $datafile);
 $num = count($variables);
  system("(chmod 777 $data && chmod 777 $gender && echo \"1\" > status/status.$projectname && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd /var/tmp/datafiles/ && dos2unix \"$data\" && dos2unix \"$gender\" && sed '1n; s/,/\./g' \"$data\" > tmp.txt && mv tmp.txt \"$data\" && sed '1n; s/,/\./g' \"$gender\" > tmp.txt && mv tmp.txt \"$gender\" && cd $scriptdir/VanillaICE/ && ./VanillaICE_build_dep.pl -d \"$data\" -g \"$gender\" -p \"$projectname\" -s \"$minsnp\" -t \"$taufactor\" -m  \"$hmm\" -v \"$variance\" -c \"$chiptype\" -u \"$username\" > $projectname.out\" && rm $data && rm $gender && echo \"0\" > status/status.$projectname && chmod a+rw status/status.$projectname) > /dev/null & ");
  header("Location: index.php?page=result&project=$projectname&type=vanillaice");

}
*/
?> 
