<?php
abstract class API
{
    /**
     * Property: method
     * The HTTP method this request was made in, either GET, POST, PUT or DELETE
     */
    protected $method = '';
    /**
     * Property: endpoint
     * The Model requested in the URI. eg: /files
     */
    protected $endpoint = '';
    /**
     * Property: verb
     * An optional additional descriptor about the endpoint, used for things that can
     * not be handled by the basic methods. eg: /files/process
     */
    protected $verb = '';
    /**
     * Property: args
     * Any additional URI components after the endpoint and verb have been removed, in our
     * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
     * or /<endpoint>/<arg0>
     */
    protected $args = array();
    /**
     * Property: file
     * Stores the input of the PUT request
     */
    protected $file = Null;

    /**
     * User details
     */
    public $uid = "";
    protected $userlevel = '';
    protected $apikey = '';
    protected $email = '';

    /*
     * config data
     */
    protected $config = array();

    /**
     * Constructor: __construct
     * Allow for CORS, assemble and pre-process the data
     */

    public function __construct($request)
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");
        $this->args = explode('/', rtrim($request, '/'));
        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }


        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        switch ($this->method) {
            case 'DELETE':
            case 'POST':
            case 'GET':
                $this->request = $this->_cleanInputs($_REQUEST);
                break;
            case 'PUT':
                $this->request = $this->_cleanInputs($_GET);
                $this->file = file_get_contents("php://input");
                break;
            default:
                $this->_response('Invalid Method', 405);
                break;
        }

        // Read credentials
        global $config;
        $config = $this->_ReadCredentials();
        $this->path = $_SERVER['HTTP_HOST'] . "/" . $config['WEBPATH'] . "/api/";

        // initialize logger
        // TODO
        // require('../includes/inc_logging.inc');
        // initialize the DB.
        $mysqli = $this->_ConnectToDb($config['DBUSER'], $config['DBPASS'], $config['DBHOST']);

        // Check authentication: either internal via UID or external via API key
        $v = $this->verifyAuth();
        if (! $v['status']) {
            throw new Exception($v['reason']);
        }
    }


    public function processAPI()
    {        
        if ((int)method_exists($this, $this->endpoint) > 0) {
            $response = $this->{$this->endpoint}($this->args);
            if ($response['status'] == 'error') {
                return $this->_response($response, $response['err_code']);
            }
            else {
                return $this->_response($response);
            }
        }
        return $this->_response("No Endpoint: $this->endpoint", 404);
    }

    private function _response($data, $status = 200)
    {
        http_response_code($status);
        return json_encode($data);
    }

    private function _cleanInputs($data)
    {
        $clean_input = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                $clean_input[$k] = $this->_cleanInputs($v);
            }
        } else {
            $clean_input = trim(strip_tags($data));
        }
        return $clean_input;
    }

    private function _requestStatus($code)
    {
        $status = array(
            200 => 'OK',
            400 => 'Bad Request',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code]) ? $status[$code] : $status[500];
    }

    private function _ReadCredentials()
    {
        $file = realpath(".credentials");
        $lines = file($file) or die("Could not read $file"); //return(0);
        $config = array();
        foreach ($lines as $line_num => $line) {
            # Comment?
            if (!preg_match("/#.*/", $line)) {
                # Contains non-whitespace?
                if (preg_match("/\S/", $line) && preg_match("/=/", $line)) {
                    list($key, $value) = explode("=", trim($line), 2);
                    $config[$key] = $value;
                }
            }
        }
        // set a few general config items for usage elseswhere.
        $this->config['WEBPATH'] = $config['WEBPATH'];
        $this->config['SITEDIR'] = $config['SITEDIR'];
        $this->config['SCRIPTUSER'] = $config['SCRIPTUSER'];
        $this->config['SCRIPTPASS'] = $config['SCRIPTPASS'];
        $this->config['SCRIPTDIR'] = $config['SCRIPTDIR'];
        $this->config['DATADIR'] = $config['DATADIR'];
        $this->config['DOMAIN'] = $config['DOMAIN'];
        $this->config['DBUSER'] = $config['DBUSER'];
        $this->config['DBPASS'] = $config['DBPASS'];
        $this->config['DBHOST'] = $config['DBHOST'];

        // set the path
        $PATH = '';
        if (array_key_exists('CONDA_ENV', $config)) {
            $this->config['CONDA_ENV'] = $config['CONDA_ENV'];
            $command = "echo \"{$config['SCRIPTPASS']}\" | sudo -i -S -u {$config['SCRIPTUSER']} conda run -n {$config['CONDA_ENV']} which python";
            $python_exec = shell_exec($command);
            $python_exec = trim($python_exec);
            $python_bin = rtrim($python_exec, 'python');
            $PATH .= $python_bin . ":";
        }
        if (array_key_exists('PATH', $config)) {
            $PATH .= $config['PATH'] . ":";
        }
        $PATH .= getenv('PATH');
        
        // Update the .htaccess file cgi-bin that sets path.
        file_put_contents($config['SITEDIR'] . "/python/.htaccess", "SetEnv PATH $PATH");
        
        return $config;
    }


    public function verifyAuth() {
        // check if it's an internal call
        if (isset($this->request['uid']) && !empty($this->request['uid'])) {
            $this->uid = $this->request['uid'];
            $row = $this->_runQuery("SELECT LastName, FirstName, email, apikey, level FROM `users` WHERE id = '$this->uid'", False);
            if (empty($row)) {
                return array('status' => FALSE, 'reason' => 'Invalid UID');
            }
            $this->email = $row['email'];
            $this->userlevel = $row['level'];
            $this->apikey = $row['apikey'];
            return array('status' => TRUE);
        }
        // else check API key
        if (isset($this->request['apikey']) && !empty($this->request['apikey'])) {
            $this->apikey = $this->request['apikey'];
        }
        else {
            return array('status' => FALSE, 'reason' => 'No API Key provided');
        }

        $row = $this->_runQuery("SELECT id, LastName, FirstName, email, level FROM `users` WHERE apikey = '$this->apikey'", False);
        if (empty($row)) {
            return array('status' => FALSE, 'reason' => 'Invalid API Key');
        }
        // validate key : user is active
        if ($row['level'] < 1) {
            return array('status' => FALSE, 'reason' => "API-key belongs to disabled user: " . $row['level']);
        }
        $this->uid = $row['id'];
        $this->email = $row['email'];
        $this->userlevel = $row['level'];
        return array('status' => TRUE); 

    }

    public function CheckPlatformStatus()
    {
        //$this->ConnectToDB() or die("Could not connect to database");
        ## check platform status.
        $query = "SELECT status FROM `GenomicBuilds`.`SiteStatus`";
        $row = $this->_runQuery($query, False);
        return ($row);
    }

    ///////////////////////
    // DATABASE ROUTINES //
    ///////////////////////
    private function _ConnectToDB($dbuser, $dbpass, $dbhost)
    {
        if (!isset($this->mysqli)) {
            // get build.
            $mysqli = new mysqli($dbhost, $dbuser, $dbpass, 'GenomicBuilds');
            if ($mysqli->connect_errno) {
                echo json_encode("Failed to connect to MySQL: " . $mysqli->connect_error);

                error_log("Failed to connect to MySQL: " . $mysqli->connect_error);
                exit();
            }
            $result = $mysqli->query("SELECT name, StringName, Description FROM `CurrentBuild` LIMIT 1");
            $row = $result->fetch_assoc();
            $dbname = $row['name'];
            $result->free();
            $selectdb = $mysqli->select_db("CNVanalysis$dbname");
            if ($selectdb) {
                $this->mysqli = $mysqli;
            } else {
                echo json_encode("Failed to connect to Database CNVanalysis$dbname");
                error_log("Failed to connect to MySQL: " . $mysqli->connect_error);
                exit;
            }
        }
        return $mysqli;
    }

    // run query without feedback (create / drop /truncate)
    protected function _doQuery($query)
    {
        $sth = $this->mysqli->query($query);
        if (!$sth) {
            trigger_error($this->mysqli->error, E_USER_ERROR);
            return FALSE;
        } else {
            return TRUE;
        }
    }

    protected function _runQuery($query, $type = 'assoc', $aoa = True)
    {
        //workaround because php does not support named args: if called as rq($query,False)
        if (!$type) {
            // set aoa to false, reset type to def
            $aoa = False;
            $type = 'assoc';
        }
        // type "assoc" return key/value arrays (associative) ; type "row" returns anonymous array (numeric keys)
        $sth = $this->mysqli->query($query);
        if (!$sth) {
            trigger_error($this->mysqli->error, E_USER_ERROR);
            return FALSE;
        }
        // one result, and not requesting an array of arrays as result
        if ($sth->num_rows == 1 && !$aoa) {
            if ($type != 'row') {
                $result = $sth->fetch_assoc();
            } else {
                $result = $sth->fetch_row();
            }
        } elseif ($sth->num_rows == 0) {
            $result = array();
        } else {
            $result = array();
            if ($type != 'row') {
                while ($row = $sth->fetch_assoc()) {
                    array_push($result, $row);
                }
            } else {
                while ($row = $sth->fetch_row()) {
                    array_push($result, $row);
                }
            }
        }
        return $result;
    }

    protected function _insertQuery($query)
    {

        // run query
        if ($this->mysqli->query($query)) {
            $id = $this->mysqli->insert_id;
        } else {
            trigger_error($this->mysqli->error, E_USER_ERROR);
            $id = -1;
        }
        return ($id);
    }
    // create AoA from array in case of a single result
    protected function _OneToMulti($a)
    {
        $result = array();
        if (empty($a)) {
            return ($a);
        }
        array_push($result, $a);
        return ($result);
    }

    // escape values for MYSQL
    protected function _SqlEscapeValues($data)
    {
        if (is_array($data)) {
            for ($i = 0; $i < count($data); $i = $i + 1) {
                $data[$i] = $this->mysqli->real_escape_string($data[$i]);
            }
        } else {
            $data = $this->mysqli->real_escape_string($data);
        }
        return ($data);
    }

    // get the latest sql error
    protected function _GetSqlError()
    {
        return ($this->mysqli->error);
    }

}