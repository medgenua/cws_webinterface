<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
//
// STRUCTURE OF EACH RESPONSE IS ASSOC ARRAY WITH: 
//   - IF SUCCESS: 
//      - help => some_documentation
//      - status => "ok" (lowercase)
//      - results => the actual response structure
//   - IF FAILURE:
//      - help => some_documentation
//      - status => "error" (lowercase)
//      - msg => "error message"
//
require_once 'API.class.php';

class MyAPI extends API
{
    protected $User;

    public function __construct($request)
    {
        parent::__construct($request);
    }
    /////////////////////////
    // AVAILABLE ENDPOINTS //
    /////////////////////////
    // list user details
    // list all stored filters 			ok
    // list details of a specific filter		ok
    // list all stored annotation sets		ok
    // list details of a specific annotation set	ok
    // list all samples				ok
    // list details of a specific sample		ok
    // Load Annotations using a POSTed JSON file	ok
    // Phenotypes : details by ID, search by term
    // run query, for sample/region (mandatory), using a specific filter (just one) and one or more annotation sets ok
    // download data : sample ID, vcf.gz, bam, bai, vcf.gz.tbi
    // gene panel info, make&get bedfiles		ok
    // set sample relations.

    ///////////////////////////////////
    //                               //
    // SECTION 1 :: RETRIEVING DATA  //
    //                               //
    ///////////////////////////////////

    /****************************************************/
    /** GET VARIOUS STATUS-DETAILS: queries/system/... **/
    /****************************************************/

    // just for testing purposes
    protected function stats() {

        $result = array();
        if ($this->method == 'GET') {
            $result['Test'] = 'test';
        }
        return($result);
    }

    protected function GetStatus($args)
    {
        // documentation.
        $doc = array(
            "goal" => "Get various status reports",
            "method" => "GET",
            "sub-call" => 'status item. Values: variantdb, annotation, revision, stats, query, queue, import, summarystatus',
            "arguments" => array(
                'variantdb' => "none",
                'annotation' => array("None" => 'overall annotation status', "sample_ID" => "single sample annotation status"),
                'revision' => "none",
                'stats' => "none",
                'query' => 'query_key',
                'import' => 'import_key',
                'queue' => 'none',
                'summarystatus' => 'user_id or project_id'
            ),
            "parameters" => array("type" => "Required for 'summarystatus', values (u)ser, (p)roject"),

            "return" => array(
                "variantdb" => "Platform status : Operative, Construction, LiftOver",
                "annotation" => array(
                    'no_sample_given' => 'array(running/pending annotations (strings)',
                    'sample_given' => 'Annotation status (string)'
                ),
                "revision" => array("Site" => "git revision of UI", "Database" => "git revision of Database"),
                "stats" => array(
                    "size_database" => array("database" => "size"),
                    "size_datafiles" => "Total size of stored bam/vcf files",
                    "system_df" => "Dump of 'df -h' command on the server"
                ),
                "query" => array(
                    "status" => "queued/running/finished/error",
                    "message" => "Error message if status == error",
                    "queue_position" => "position in the queue if status == queued"
                ),
                "queue" => "number of queued items",
                "import" => array(
                    "status" => "finished/error/running",
                    "runtime_output" => "Output from import script",
                    "ProjectID" => "ID of the project where samples where imported",
                    "id_map" => "Map between sample names and sample_ids"
                ),
                'summarystatus' => array(
                    "SummaryStatus" => "0/1/2",
                    "SummaryTextStatus" => "Out-Of-Date / Running / Up-To_Date",
                )

            )
        );
        if ($this->method == 'GET') {
            // PlatForm Status.
            if (strtolower($this->verb) == 'cnvwebstore') {
                $result = $this->_runQuery("SELECT status FROM `GenomicBuilds`.`SiteStatus` WHERE 1", False);
                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            
            // System status : revision
            elseif (strtolower($this->verb) == 'revision') {
                // list current system revisions: database + web interface.
                $revision = array();
                $cmd = "(echo " . $this->config['SCRIPTPASS'] . " | sudo -u " . $this->config['SCRIPTUSER'] . " -S bash -c \"cd " . $this->config['SITEDIR'] . " && git rev-parse --short HEAD \" 2>/dev/null)";
                exec($cmd, $output_ui, $exit);
                if ($exit) {
                    return array("help" => $doc, "status" => "error", "msg" => 'Could not get UI revision : ' . implode(" ", $output_ui));
                }
                $revision['Site'] = $output_ui[0];
                $cmd = "(echo " . $this->config['SCRIPTPASS'] . " | sudo -u " . $this->config['SCRIPTUSER'] . " -S bash -c \" cd " . $this->config['SCRIPTDIR'] . "/../Database_Revisions && git rev-parse --short HEAD \" 2>/dev/null)";
                //trigger_error($cmd);

                exec($cmd, $output_db, $exit);
                if ($exit) {
                    return array("help" => $doc, "status" => "error", "msg" => 'Could not get DB revision : ' . implode(" ", $output_db));
                }
                $revision['Database'] = $output_db[0];
                return array("help" => $doc, "status" => "ok", "results" => $revision);
            }
            // System status : disk usage
            elseif (strtolower($this->verb) == 'stats') {
                // only for admin users. (check on level)
                if ($this->userlevel <= 1) {
                    return array("help" => $doc, "status" => "error", "msg" => 'Access denied');
                }
                $result = array();
                // database size
                $rows = $this->_runQuery("SELECT table_schema 'Data Base Name',sum( data_length + index_length ) / 1024 / 1024 'Data Base Size in MB' FROM information_schema.TABLES GROUP BY table_schema HAVING table_schema LIKE 'CNVanalysis%'");
                $result['size_database'] = array();
                foreach ($rows as $row) {
                    $result['size_database'][] = $row;
                }
                // storage size
                exec("cd " . $this->config['DATADIR'] . " && du -hs", $output, $exit);
                $output = explode("\t", $output[0]);
                $result['size_datafiles'] = $output[0];
                // system free disk space
                $output = array();
                exec("df -h | grep -v none ", $output, $exit);
                $result['system_df'] = $output;

                return array("help" => $doc, "status" => "ok", "results" => $result);
            }
            
            else {
                return array("help" => $doc, "status" => "error", "msg" =>  "Verb '" . $this->verb . "' not supported.");
            }
        } else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    }

    // DENUG remove this
    protected function test () {
        // return array("status" => "ok");
        return array("status" => "error", "msg" => "test", "err_code" => 505);
    }

    protected function importSample($data, $apikey, $library, $sharegroup, $infofile) {
        // documentation.
        $doc = array(
            "goal" => "Import a sWGS sample",
            "method" => "GET",
            "arguments" => array(
                "sample" => "dnanr of the sample",
                "gender" => "gender (Male/Female)",
                "projectname" => "If this project exists, the sample will be added to the project. Else the project will be created",
                "resolution" => "sequencing depth",
                "datafile" => "raw data file",
                "cnvfile" => "called cnvs/mosaicisms"
            ),
            "return" => array(
                "sid" => "DB sample id",
                "pid" => "DB project id",
                "result" => "succes/error",
                "error" => "optional: message if import failed"
            )
        );        

        // get arguments from direct API call (if arguments are given, it's an internal function call)
        
        if (empty($data)) {
            $data = $this->request;
        }
            
        if ($this->method == 'GET') {
            // get url
            $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/python/import_sample.py";
            $poststring  = "sample=" . $data['sample'] . "&gender=" . $data['gender'];
            $poststring .= "&datafile=" . $data['datafile'] . "&cnvfile=" . $data['cnvfile'] . "&infofile=" . $infofile;
            $poststring .= "&projectname=" . $data['projectname'] . "&resolution=" . $data['resolution'];
            $poststring .= "&macon=" . $data['macon'] . "&n_reads=". $data['n_reads'] . "&gendertest=".$data['gendertest'];
            $poststring .= "&upd_indication=" . $data['upd_indication'];
            $poststring .= "&apikey=".$apikey;
            $poststring .= "&dbuser=".$this->config['DBUSER'];
            $poststring .= "&dbpass=".$this->config['DBPASS'];
            $poststring .= "&dbhost=".$this->config['DBHOST'];
            if ($library) {
                $poststring .= "&library=".$library;
            }
            if ($sharegroup) {
                $poststring .= "&share=".$sharegroup;
            }

            $result = $this->_InternalCall($url, $poststring);
            $result = json_decode(json_encode($result), true);
            
            if (isset($result->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $result->ERROR);
            } else {
                return array("help" => $doc, "status" => "ok", 'results' => $result);

            }
        }

        else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }
    
    }
    protected function mergeCNVs(){
        if (! $this->method == 'POST') {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts POST requests");
        }
        else {
            $cnvs = $this->request['cnvs'];
            $sid = $this->request['sid'];
            $pid = $this->request['pid'];

            // get sample name
            $samplerow = $this->_runQuery("SELECT chip_dnanr as sample FROM sample WHERE id = $sid", FALSE);
            $sample = $samplerow['sample'];

            // gather all cnvs
            $interval = filter_var($this->request['interval'], FILTER_VALIDATE_BOOLEAN);
            if ($interval) {
                if (count($cnvs) != 2) {
                    return array("status" => "error", "msg" => "Please select the first and last CNV to merge. You provided ".count($cnvs)." CNV's", "err_code" => 400);
                }

                $first_cnv = $this->_runQuery("SELECT id, chr, start, stop  FROM aberration WHERE id='".$cnvs[0]."'", FALSE);
                $second_cnv = $this->_runQuery("SELECT id, chr, start, stop FROM aberration WHERE id='".$cnvs[1]."'", FALSE);
                if ($first_cnv['chr'] != $second_cnv['chr']) {
                    return array("status" => "error", "msg" => "Chromosomes of CNVs don't match: ".$first_cnv['chr'].", ".$second_cnv['chr'], "err_code" => 400);
                }

                // check which cnv comes first, and get inner interval between them
                if ($first_cnv['start'] < $second_cnv['start']) {
                    $interval_start = $first_cnv['stop'];
                    $interval_end = $second_cnv['start'];
                }
                else {
                    $interval_start = $second_cnv['stop'];
                    $interval_end = $first_cnv['start'];
                }

                $cnvs_interval = $this->_runQuery("SELECT id FROM `aberration` WHERE sample = '$sid' and chr = '".$first_cnv['chr']."' AND start > '$interval_start' AND stop < '$interval_end'");
				$cnvs = array();
				foreach ($cnvs_interval AS $cnv) {
					array_push($cnvs, $cnv['id']);
				}
            }

            else {
                if (count($cnvs) < 2) {
                    return array("status" => "error", "msg" => "Please select at least 2 CNV's to merge. You provided ".count($cnvs)." CNV's", "err_code" => 400);
                }

            }

            if (count($cnvs) == 0) {
                return array("status" => "ok", "results" => "NA");
            }
            // START MERGING
            $firstaid = $cnvs[0];
            $row = $this->_runQuery("Select chr, start, stop, largestart, largestop, cn, class, inheritance FROM aberration WHERE id='$firstaid'", FALSE);
            $chr = $row['chr'];
            $firststart = $row['start'];
            $laststop = $row['stop'];
            $largestart = $row['largestart'];
            $largestop = $row['largestop'];
            $cn = $row['cn'];
            $inheritance = $row['inheritance'];
            $class = $row['class'];

            $qsum = 0;
            $passum = 0;
            $pxsum = 0;
            $qnumber = 0;
            $pasnumber = 0;
            $pxnumber = 0;
            $vice = 0;
            $contenthash = array();
            $finalstructure = 0;
            $fullcontent = '';
            $fullremark = '';
            $avglogrs = array();
            $avgzscores = array();
            $segmentzscores = array();
            $likelihood_ratios = array();
            $datatype = null;
            $snps = 0;
            $totalsnps = 0;
            $seenbyavg = "";
            $avgzscore = null;
            $segmentzscore = null;
            $likelihood_ratio = null;
            
            foreach ($cnvs as $key => $aid) {
                $row = $this->_runQuery("Select a.chr, a.start, a.stop, a.largestart, a.largestop, a.cn, a.nrsnps, a.total_nrsnps, a.seenby, a.Remarks, a.avgLogR, a.avgZscore, a.segmentZscore, a.likelihood_ratio, a.datatype, a.class, a.inheritance, d.structure, d.content FROM aberration a LEFT JOIN datapoints d ON a.id = d.id WHERE a.id='$aid'", FALSE);
                $cchr = $row['chr'];
                $cstart = $row['start'];
                $cstop = $row['stop'];
                $clargestart = $row['largestart'];
                $clargestop = $row['largestop'];
                $ccn = $row['cn'];
                $seenby = $row['seenby'];
                $content = $row['content'];
                $structure = $row['structure'];
                $fullremark .= $row['Remarks'];
                $datatype = $row['datatype'];
                $avglogr = $row['avgLogR'];
                $avgzscore = $row['avgZscore'];
                $segmentzscore = $row['segmentZscore'];
                $likelihood_ratio = $row['likelihood_ratio'];
                $snps = $snps + $row['nrsnps'];
                $cclass = $row['class'];
                $cinheritance = $row['inheritance'];

                if ($ccn != $cn) {
                    return array("status" => "error", "msg" => "The copy numbers of the CNVs doesnt match: $ccn versus $cn. Cant merge.", "err_code" => 400);
                }
                elseif ($cchr != $chr) {
                    return array("status" => "error", "msg" => "The chromsomes of the CNVs dont match: $cchr versus $chr. Cant merge.", "err_code" => 400);
                }

                // update region
                if ($clargestart < $largestart) {
                    $largestart = $clargestart;
                }
                if ($clargestop > $largestop) {
                    $largestop = $clargestop;
                }
                if ($cstart < $firststart) {
                    $firststart = $cstart;
                }
                if ($cstop > $laststop) {
                    $laststop = $cstop;
                }
            
                // update class / inheritance only if it the same for all
                if ($cclass != $class) {
                    $class = '';
                }
                if ($cinheritance != $inheritance) {
                    $inheritance = '';
                }


                // update scores
                array_push($avgzscores, $avgzscore);
                if ($datatype == 'swgs') {
                    array_push($segmentzscores, $segmentzscore);
                    array_push($likelihood_ratios, $likelihood_ratio);
                    array_push($avglogrs, $avglogr);
                }
                else {
                    if (preg_match('/VanillaICE/',$seenby)) {
                        $vice = 1;
                    }
                    if (preg_match('/QuantiSNPv2\s\((\S+)\)/', $seenby, $match)) {
                        $score = $match[1];
                        $qsum = $qsum + $score;
                        $qnumber++;
                    }
                    if (preg_match('/PennCNVas\s\((\S+)\)/', $seenby, $match)) {
                        $score = $match[1];
                        $passum = $passum + $score;
                        $pasnumber++;
                    }
                    elseif (preg_match('/PennCNVx\s\((\S+)\)/', $seenby, $match)) {
                        $score = $match[1];
                        $pxsum = $pxsum + $score;
                        $pxnumber++;
                    }
            
                }
            
                // DETERMINE DATA STRUCTURE
                $structure_nr = substr_count($structure, '1');
                if ($structure_nr > $finalstructure) {
                    $finalstructure = $structure_nr;
                }

                // GET DATA POINTS FOR NON-SWGS DATA (per CNV)
                if ($datatype != 'swgs') {
                    $content = trim($content, "_");
                    $content = explode("_", $content);
                    $contentelements = count($content);
                    for ($i = 0; $i < $contentelements; $i += $structure_nr) {
                        $position = $content[$i];
                        $logr = $content[$i + 1];
                        $baf = $content[$i + 2];
                        if ($structure_nr == 4) {
                            $baf_ab = $content[$i + 3];
                        }
                        else {
                            $baf_ab = "NC";
                        }
                        $contenthash[$position] = array("logr" => $logr, "baf" => $baf, "baf_ab" => $baf_ab);
                    }
                }
            }


            // GENE CONTENT & SIZE
            $genrow = $this->_runQuery("SELECT Count(DISTINCT symbol) as nrgenes FROM genes WHERE chr='$chr' AND (((start BETWEEN $firststart AND $laststop) OR (end BETWEEN $firststart AND $laststop)) OR (start <= $firststart AND end >= $laststop))", FALSE);
            $nrgenes = $genrow['nrgenes'];
            $size = $laststop - $firststart +1;

            // GET DATA POINTS FOR SWGS-DATA (from raw data)
            if ($datatype == 'swgs') {
                // parse sample info file
                $projsamprow = $this->_runQuery("SELECT `project`.naam AS project, `projsamp`.sampleinfo_file  AS sampleinfo, `projsamp`.swgs_resolution AS resolution FROM `projsamp` JOIN `project` ON `projsamp`.idproj = `project`.id WHERE idsamp = '$sid' AND idproj = '$pid'", FALSE);
                $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/python/parse_sampleinfo.py";
                $poststring = "sampleinfo=" . $projsamprow['sampleinfo'] . "&projectname=" . $projsamprow['project'];
                $parsed_samples = $this->_InternalCall($url, $poststring);
                $parsed_samples = json_decode(json_encode($parsed_samples), true);
                $sample_data = $parsed_samples[$sample];
                $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/python/get_cnv_datapoints.py";
                $poststring = "data=" . $sample_data['datafile'] . "&chromosome=" . $chr . "&start=" . $firststart . "&stop=" . $laststop . "&resolution=" . $projsamprow['resolution'] ."&maxstart=" . $largestart . "&maxstop=" . $largestop;
                $data = $this->_InternalCall($url, $poststring);
                $data = json_decode(json_encode($data), true);
                
                $fullcontent = $data['datapoints'];
                $fullcontent = trim($fullcontent, "_");
                $structure = "1110";
                $totalsnps = $data['nrbins'];

            }

            else {
                // FINALIZE DATA POINTS
                if ($finalstructure == 4) {
                    foreach ($contenthash as $position => $arr) {
                        $fullcontent .= $position."_".$arr["logr"]."_".$arr["baf"]."_".$arr["baf_ab"]."_";
                    }
                    $structure = "1111";
                }
                else {
                    foreach ($contenthash as $position => $arr) {
                        $fullcontent .= $position."_".$arr["logr"]."_".$arr["baf"]."_";
                    }
                    $structure = "1110";
                }
                $fullcontent = trim($fullcontent, "_");
            }
                

            
            // GET AVERAGE SCORES
            $avglogr = $this->_calculateAverage($avglogrs);
            if ($datatype == 'swgs') {
                // is average the best approximation, or take the highest?
                $avgzscore = $this->_calculateAverage($avgzscores);
                $segmentzscore = $this->_calculateAverage($segmentzscores);
                $likelihood_ratio = $this->_calculateAverage($likelihood_ratios);
            }
            else {
                //find number of snps
                $snprow = $this->_runQuery("SELECT Count(name) as nrsnps FROM probelocations WHERE chiptype='$chiptypeid' AND chromosome='$chr' AND  position>='$firststart' AND position<='$laststop'", FALSE);
                $snps = $snprow['nrsnps'];
                if ($qnumber > 0) {
                    $qavg = round(($qsum / $qnumber),2);
                    $seenbyavg = $seenbyavg . "- QuantiSNPv2 ($qavg) ";
                }
                if ($pasnumber > 0) {
                    $pavg = round(($passum / $pasnumber),2);
                    $seenbyavg = $seenbyavg . "- PennCNVas ($pavg) ";
                }
                elseif ($pxnumber > 0) {
                    $pavg = round(($pxsum / $pxnumber),2);
                    $seenbyavg = $seenbyavg . "- PennCNVx ($pavg) ";
                }
                if ($vice == 1) {
                    $seenbyavg = $seenbyavg . "- VanillaICE (NaN)";
                }
                # FIND next 5' probe
                $smaller = "SELECT position FROM probelocations WHERE chromosome = '$chr' AND chiptype = $chiptypeid AND position < $firststart ORDER BY position DESC LIMIT 1";
                #echo "$smaller<br/>";
                $smallrows = $this->_runQuery($smaller, FALSE);
                $smallrows = mysql_num_rows($smallq);
                
                if (count($smallrows) > 0) {
                    $largestart = $smallrow['position'];
                }
                else {
                    $largestart = 'ter';
                }
                # FIND next 3' probe
                $larger = "SELECT position FROM probelocations WHERE chromosome = '$chr' AND chiptype = $chiptypeid AND position > $laststop ORDER BY position ASC LIMIT 1";
                $largerows = $this->_runQuery($larger, FALSE);
                if (count($largerows) > 0) {
                    $largestop = $largerow['position'];
                }
                else {
                    $largestop = 'ter';
                }
            }
            
            // UPDATE DB
            $new_aid = $this->_insertQuery("INSERT INTO aberration (chr, start, stop, cn, sample, idproj, nrsnps, total_nrsnps, nrgenes, seenby, largestart, largestop, size, Remarks, avgLogR, avgZscore, segmentZscore, likelihood_ratio, datatype, class, inheritance) VALUES ('$chr', '$firststart', '$laststop', '$cn', '$sid', '$pid', '$snps', '$totalsnps', '$nrgenes', '$seenbyavg', '$largestart', '$largestop', '$size','$fullremark', '$avglogr', '$avgzscore', '$segmentzscore', '$likelihood_ratio', '$datatype', '$class', '$inheritance')");
            $new_datapoints_id = $this->_insertQuery("INSERT INTO datapoints (id, content, structure, datatype) VALUES ('$new_aid', '$fullcontent', '$structure', '$datatype')");
            $logentry = "Added as merging result";
            $new_log_id = $this->_insertQuery("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$new_aid','$this->uid', '$logentry')");
            $new_log_id = 'dummy';
            foreach ($cnvs as $key => $aid) {
                if ($aid != '') {
                    $subr = $this->_runQuery("SELECT id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby FROM aberration WHERE id = $aid", FALSE);
                    $delid = $subr['id'];
                    $delcn = $subr['cn'];
                    $delstart = $subr['start'];
                    $delstop = $subr['stop'];
                    $delchr = $subr['chr'];
                    $delsid = $subr['sample'];
                    $delpid = $subr['idproj'];
                    $delinh = $subr['inheritance'];
                    $delclass = $subr['class'];
                    $delsb = $subr['seenby'];
                    $new_deletedcnvs_id = $this->_insertQuery("INSERT INTO deletedcnvs (id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby) VALUES ('$delid','$delcn','$delstart','$delstop','$delchr','$delsid','$delpid','$delinh','$delclass','$delsb')");
                    $logentry = "Removed after merging";
                    $new_log_deleted_id = $this->_insertQuery("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$delsid', '$delpid', '$aid', '$this->uid', '$logentry')");
                    $query = $this->_doQuery("DELETE FROM aberration WHERE id = '$aid'");
                }
            }
                       
            return array("status" => "ok", "results" => "NA");
        }
    }

    protected function importSamples() {
        // get default library name
        $row = $this->_runQuery("SELECT name FROM `chiptypes` where datatype = 'sWGS' ORDER BY `chiptypes`.`ID`  DESC LIMIT 1", FALSE);
        $default_library = $row['name'];
        // documentation.
        $doc = array(
            "goal" => "Import multiple sWGS samples",
            "method" => "GET",
            "arguments" => array(
                "sampleinfo"    => "the full path to file with the sample info",
                "projectname"   => "If this project exists, the samples will be added to the project. Else the project will be created",
                "library"       => "[Optional] The name of the sequencing library that was used. This has to be a known 'chiptype' in CNV-WebStore. The current default is '$default_library'",
                "share"         => "[Optional] A CNV-WebStore usergroup to share the data with. When given, all users can edit the details of the sample in this project"
            ),
            "return" => array(
                "status" => "ok/error",
                "index" => array(
                    "input" => "array of sample input details",
                    "results" => array (
                        "result"    => "succes/error",
                        "sid"       => "DB sample id",
                        "pid"       => "DB project id",
                        "error"     => "optional: message if import failed"
                    ) 
                ) 
            )
        );

        if ($this->method == 'GET') {
            $results = array();
            $apikey = $this->request['apikey'];
            $library = null;
            $sharegroup = null;
            if (isset($this->request['library'])) {
                $library = $this->request['library'];
            }
            else {
                $library = $default_library;
            }
            if (isset($this->request['share'])) {
                $sharegroup = $this->request['share'];
            }
            
            $url = "https://" . $this->config['DOMAIN'] . "/" . $this->config['WEBPATH'] . "/python/parse_sampleinfo.py";
            $poststring = "sampleinfo=" . $this->request['sampleinfo'] . "&projectname=" . $this->request['projectname'];
            $parsed_samples = $this->_InternalCall($url, $poststring);
            $parsed_samples = json_decode(json_encode($parsed_samples), true);

            if (isset($parsed_samples->ERROR)) {
                return array("help" => $doc, "status" => "error", "msg" => $result->ERROR);
            } 
            else {
                // import each sample individually
                foreach ($parsed_samples as $sample_name => $sampleinfo) {
                    $imported_sample = $this->importSample($sampleinfo, $apikey, $library, $sharegroup, $this->request['sampleinfo']);
                    $imported_sample = json_decode(json_encode($imported_sample), true);
                    array_push($results, $imported_sample['results']);
                }
                return array("help" => $doc, "status" => "ok", "results" => $results);
            }
        }    
        else {
            return array("help" => $doc, "status" => "error", "msg" => "Only accepts GET requests");
        }

    }


    ////////////////////////////////////
    // SECTION 5 :: PRIVATE FUNCTIONS //
    ////////////////////////////////////

    private function _calculateAverage($array) {
        $array = array_filter($array);
        if(count($array)) {
            $average = array_sum($array)/count($array);
            return $average;
        }
        else {
            return null;
        }
        
    }


    private function _InternalCall($url, $post_string = NULL, $json_decode = true)
    {
        // initialize   
        $curl_connection = curl_init();
        curl_setopt($curl_connection, CURLOPT_URL, $url);
        curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_connection, CURLOPT_FAILONERROR, true);
        if (!is_null($post_string)) {
            curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);
        }
        $result = curl_exec($curl_connection);

        if ($result === false) {
            trigger_error("Call to url failed: '$url' : " . curl_errno($curl_connection) . " : " . curl_error($curl_connection), E_USER_ERROR);
            $result = array();
            $result['ERROR'] = "Call to url failed: '$url' : error code was " . curl_errno($curl_connection);
            // make it compatible with other results
            $result = json_encode($result);
            if ($json_decode) {
                $result = json_decode($result);
            }
            return $result;
        }

        //close the connection
        curl_close($curl_connection);
        if ($json_decode) {
            $result = json_decode($result);
        }
        return $result;
    }


}



// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

try {
    $API = new MyAPI($_REQUEST['request']);
    echo $API->processAPI();
} 
catch (Exception $e) {
    $result = array("help" => "High level api error occured. Please report", "status" => "error", "msg" => $e->getMessage());
    //echo json_encode(Array('error' => $e->getMessage()));
    echo json_encode($result);
}

?>
