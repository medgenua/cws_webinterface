<?php
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$uid = $_SESSION['userID'];

if (isset($_POST['changedef'])) {
	$sidstring = $_POST['sids'];
	$sids = explode('_',$sidstring);
	foreach ($sids as $s) {
		$value = $_POST[$s];
		$parts = explode('-',$value);
		$currsid = $parts[0];
		$altpid = $parts[1];
		$update = mysql_query("UPDATE sample SET trackfromproject = '$altpid' WHERE id = '$currsid'");
	}
	#header("Location: index.php?page=profile&amp;type=projects");	 
	echo "<meta http-equiv='refresh' content='0;URL=index.php?page=projects&amp;type=illumina'>\n";	
}

// JOIN PROJECTS
if (isset($_POST['join'])) {
	$in = '';
	for ($i=0; $i<count($_POST['joinarray']); $i++){
		$pid = $_POST['joinarray'][$i];
		$in .= $pid . ',';
	}
	$in = substr($in, 0,-1);
	$ok = 1;
	// check overlap in samples
	$query = mysql_query("SELECT COUNT(idproj) AS aantal FROM projsamp where idproj IN ($in) GROUP BY idsamp ORDER BY aantal DESC LIMIT 1");
	$row = mysql_fetch_array($query);
	if ($row['aantal'] > 1) {
		echo "<div class=sectie>\n";
		echo "<h3>Overlap Detected</h3>\n";
		echo "<p>Projects can not be joined when they contain the same samples.</p>\n";
		echo "</div>\n";
		$ok = 0;
	}
	$query = mysql_query("SELECT COUNT(DISTINCT chiptypeid) AS aantal, chiptype, chiptypeid FROM project where id IN ($in)");
	$row = mysql_fetch_array($query);
	if ($row['aantal'] > 1) {
		echo "<div class=sectie>\n";
		echo "<h3>Multiple Chiptypes Detected</h3>\n";
		echo "<p>Projects can not be joined when they are run on different chiptypes.</p>\n";
		echo "</div>\n";
		$ok = 0;
	}
	$squery = mysql_query("SELECT minsnp,asymFilter,minmethods,pipeline,plink FROM project WHERE id in ($in)");
	$ms = array();
	$asym = array();
	$minmethods = array();
	$pipeline = array();
	echo "<form action=change_permissions.php method=POST>\n";
	
	while ($srow = mysql_fetch_array($squery)) {
		
		$ms[$srow['minsnp']] = 1;
		$asym[$srow['asymFilter']] = 1;
		$pipeline[$srow['pipeline']] = 1;
		$minmethods[$srow['minmethods']] = 1;
		$plink[$srow['plink']] = 1;
	} 
	if (count($pipeline) > 1 || count($minmethods) > 1) {
		echo "<div class=sectie>\n";
		echo "<h3>Multiple Analysis Pipelines Detected</h3>\n";
		echo "<p>Projects can not be joined when they are run using different analysis pipelines. This might lead to misinterpretation !</p>\n";
		echo "</div>\n";
		$ok = 0;
	}
	else {
		
		echo "<input type=hidden name=pipeline value='".key($pipeline)."'/>";
		echo "<input type=hidden name=minmethods value='".key($minmethods)."'/>";
	
	}

	if ($ok == 1) {
		$chiptype = $row['chiptype'];
		$chiptypeid = $row['chiptypeid'];
		echo "<div class=sectie>\n";
		
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=pids value='$in'>\n";
		echo "<input type=hidden name=chiptype value='$chiptype'>\n";
		echo "<input type=hidden name=chiptypeid value='$chiptypeid'>\n";
		echo "<h3>Set New Project Details</h3>\n";
		echo "<p>Please note that users with access to only a subset of the selected projects will gain access to the complete project !</p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr>\n";
		echo "  <td>Project Name:</td>\n";
		echo "  <td><input type=text size=30 name=pname></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "  <td>Collection:</td>\n";
		echo "  <td> <select name=collection>";
		$cquery = mysql_query("SELECT name FROM collections ORDER BY name");
		while ($crow = mysql_fetch_array($cquery)) {
			echo "<option value='".$crow['name']."'>".$crow['name']."</option>";
		}
		echo "</select></td>";
		echo "</tr>\n";
		echo "</table>\n";
		echo "</p>\n";
		if (count($asym) > 1) {
			echo "<p><span class=nadruk>ATTENTION:</span><br/>";
			echo "Multiple Asymetric filter settings were found:<ol>";
			foreach ($asym as $value => $dummy) {
				$asymf = explode('|',$value);
				if ($asymf[0] == 0) {
					echo "<li><input type=radio name=asymFilter value='0'> Disabled</li>";
				}
				else {
					$reptype = array("dup" => "Duplication", "del" => "Deletion", "any" => "Both deletion and duplication");
					echo "<li><input type=radio name=asymFilter value='$value'>".$reptype[$asymf[3]] . " calls made by " . $asymf[1] . " , with a minimal confidence of ". $asymf[2] ."</li>";
				}
			}
			echo "</ol>";
			echo "Are you sure you want to continue? Only one can (and must) be passed on to the resulting project, and will be reported to all samples. This can lead to misinterpretation.";
		}
		else {
			echo "<input type=hidden name=asymFilter value='".key($asym)."'/>";
		}
		if (count($plink) > 1) {
			echo "<p><span class=nadruk>ATTENTION:</span><br/>";
			echo "Plink (LOH) analysis was not performed for all source projects.<ol>";
			foreach ($plink as $value => $dummy) {
				if ($value == 0) {
					echo "<li><input type=radio name=plink value='0'> Disabled</li>";
				}
				else {
					echo "<li><input type=radio name=plink value='1'> Enabled</li>";

				}
			}
			echo "</ol>";
			echo "Are you sure you want to continue? Only one setting can (and must) be passed on to the resulting project, and will be reported to all samples. This can lead to misinterpretation.";
		}
		else {
			echo "<input type=hidden name=plink value='".key($plink)."'/>";
		}

		if (count($ms) > 1) {
			echo "<p><span class=nadruk>ATTENTION:</span><br/>";
			echo "Multiple values were specified at runtime for the minimal number of snps:<ul>";
			foreach ($plink as $value => $dummy) {
				echo "<li><input type=radio name=minsnp value='$value'> $value snps</li>";
			}
			echo "</ul>";
			echo "Are you sure you want to continue? Only one setting can (and must) be passed on to the resulting project, and will be reported to all samples. This can lead to misinterpretation.";
		}
		else {
			echo "<input type=hidden name=minsnp value='".key($ms)."'/>";
		}

		echo "<input type=submit class=button value=Submit name=joinsubmit>\n";
	
	}	
}
if (isset($_POST['joinsubmit'])) {
	$userid = $uid;
	$pids = explode(',',$_POST['pids']);
	$in = '';
	foreach($pids AS $value) {
		$in .= $value . ',';
	}
	$in = substr($in, 0,-1);
	$pname = $_POST['pname'];
	$collection = $_POST['collection'];
	$chiptype = $_POST['chiptype'];
	$chiptypeid = $_POST['chiptypeid'];
	$plink = $_POST['plink'];
	$minsnp = $_POST['minsnp'];
	$asym = $_POST['asymFilter'];
	$minmethods = $_POST['minmethods'];
	$pipeline = $_POST['pipeline'];
	$created = date('j/n/Y - G/i/s');
	ob_start();
	echo "Joining Projects: $in<br>";
	ob_flush();
	flush();
	// create new project
	$qs = "INSERT INTO project (naam, chiptype, created, collection, userID, chiptypeid,pipeline,minmethods,minsnp,plink,asymFilter) VALUES ('$pname', '$chiptype', '$created','$collection','$userid', '$chiptypeid','$pipeline','$minmethods','$minsnp','$plink','$asym')";
	$query = mysql_query("$qs");
	$newpid = mysql_insert_id();
	if ($newpid == '') {
		echo "Project creation failed. please contact system administrator.";
		exit;
	}
	echo "created project with id $newpid<br>";
	//update aberration
	echo "Updating aberrations table <br>\n";
	ob_flush();
	flush();
	mysql_query("UPDATE aberration SET idproj = $newpid WHERE idproj IN ($in)");
	mysql_query("UPDATE aberration_LOH SET idproj = $newpid WHERE idproj IN ($in)");	
	//update BAFSEG
	echo "Updating bafseg plots <br>\n";
	ob_flush();
	flush();
	mysql_query("UPDATE BAFSEG SET idproj = $newpid WHERE idproj IN ($in)");
	//update plots
	echo "Updating plots table<br>\n";
	ob_flush();
	flush();
	mysql_query("UPDATE plots SET idproj = $newpid WHERE idproj IN ($in)");
	//update prioritize
	echo "Updating prioritize table <br>\n";
	ob_flush();
	flush();
	mysql_query("UPDATE prioritize SET project = $newpid WHERE project IN ($in)");
	// delete from projects
	echo "Delete old projects<br>\n";
	ob_flush();
	flush();
	mysql_query("DELETE FROM project WHERE id IN ($in)");
	# update UPD tables
	$upd = mysql_query("UPDATE parents_upd SET spid = $newpid WHERE spid IN ($in)");
	$upd = mysql_query("UPDATE parents_upd SET fpid = $newpid WHERE fpid IN ($in)");
	$upd = mysql_query("UPDATE parents_upd SET mpid = $newpid WHERE mpid IN ($in)");

	// update projectpermissions
	
	$query = mysql_query("SELECT userid, MAX(editcnv) AS 'ecnv', MAX(editclinic) AS 'eclin', MAX(editsample) AS 'esamp' FROM projectpermission WHERE projectid IN ($in) GROUP BY userid");
	$userline = '';
	while ($row = mysql_fetch_array($query)) {
		$curruser = $row['userid'];
		$editcnv = $row['ecnv'];
		$editclinic = $row['eclin'];
		$editsample = $row['esamp'];
		mysql_query("INSERT INTO projectpermission (userid, projectid, editcnv, editclinic, editsample) VALUES ('$curruser', '$newpid', '$editcnv', '$editclinic', '$editsample')");
	}
		mysql_query("DELETE FROM projectpermission WHERE projectid IN ($in)");

	// update projectpermissionsgroup
	$query = mysql_query("SELECT DISTINCT groupid FROM projectpermissiongroup WHERE projectid IN ($in)");
	$groupline = '';
	while ($row = mysql_fetch_array($query)) {
		$currgroup = $row['groupid'];
		mysql_query("INSERT INTO projectpermissiongroup (groupid, projectid) VALUES ('$currgroup', '$newpid')");
	}
	mysql_query("DELETE FROM projectpermissiongroup WHERE projectid IN ($in)");
	
	ob_flush();
	flush();
	// update trackfromproject
	echo "update trackfromprojects<br>\n";
	mysql_query("UPDATE sample SET trackfromproject = $newpid WHERE trackfromproject IN ($in)");
	// update projsamp
	echo "update projsamp table<br>";
	mysql_query("UPDATE projsamp SET idproj = $newpid WHERE idproj IN ($in)");
	// update logs
	echo "update log table<br/>";
	mysql_query("UPDATE log SET pid = '$newpid' WHERE pid IN($in)");
	// update deletedcnvs
	echo "update deleted cnvs table<br/>";
	mysql_query("UPDATE deletedcnvs SET idproj = '$newpid' WHERE idproj IN ($in)");
	// update project methods
	echo "update project methods table<br/>";
	mysql_query("UPDATE `project_x_method` SET pid = '$newpid' WHERE pid IN ($in)");

	// redirect to projects.
	echo "<br/><br/>";
	echo "Update Succeeded. You will be redirected to your projects table in 1 seconds.<br/>";
	 echo "<meta http-equiv='refresh' content='1;URL=index.php?page=projects&amp;type=illumina'>\n";



}
elseif(isset($_POST['protectsubmit'])) {
	$userid = $uid;
	// the checked
	$newprotected = $_POST['protectarray'];
	// from db.
	$query = mysql_query("SELECT p.id,p.protected FROM project p JOIN projectpermission pp ON pp.projectid = p.id WHERE p.userID = '$userid' OR (pp.editsample = 1 AND pp.userid = ' $userid') GROUP BY p.id ");
	$oldprotected = array();
	while ($row = mysql_fetch_array($query)) {
		if ($row['protected'] == 1) {
			$oldprotected[$row['id']] = $row['id'];
		}
	}
	// activate the new ones
	foreach($newprotected as $key => $pid) {
		if (in_array($pid,$oldprotected)) {
			// ok. delete from old
			unset($oldprotected[$pid]);
			continue;
		}
		// new one
		mysql_query("UPDATE `project` SET protected = 1 WHERE id = '$pid'");
	}
	// deactivate
	foreach ($oldprotected as $key => $pid) {
		mysql_query("UPDATE `project` SET protected = 0 WHERE id = '$pid'");
	}
	// redirect to projects.
	echo "<br/><br/>";
	echo "Update Succeeded. You will be redirected to your projects table in 1 seconds.<br/>";
	echo "<meta http-equiv='refresh' content='1;URL=index.php?page=projects&amp;type=illumina'>\n";
	
}
?>

