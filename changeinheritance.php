<?php

# POSTED VARS
$aid = $_POST['aid'];
$inh = $_POST['inh'];
$uid = $_POST['uid'];
$pid = $_POST['pid'];
$sid = $_POST['sid'];
$logentry = $_POST['logentry'];
$setbyid = $_POST['setbyid'];
$arguments = $_POST['arguments'];

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inhtypes = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
if ($arguments != '') {
	$close = 1;
	// update LOG
	mysql_query("INSERT INTO log (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry','$arguments')");
	$logid = mysql_insert_id();
	// update Aberrations
	$query = "UPDATE aberration SET inheritance='$inh' WHERE id = '$aid'";
	mysql_query($query);
	// update scantable
	$query = "UPDATE scantable SET inheritance='$inh' WHERE aid = '$aid'";
	mysql_query($query);
	// notify user (for all overrules but validation) if you're not overruling your own annotation
	if ($arguments != 'Validated' && $uid != $setbyid) {
		$subject = "CNV Annotation Overruled";
		$insquery = mysql_query("INSERT INTO inbox (inbox.from, inbox.to, inbox.subject, inbox.body, inbox.type, inbox.values, inbox.date) VALUES ('$uid','$setbyid','$subject','','cnvannotation','$logid',NOW())");
	}
}
else {
	$query = mysql_query("SELECT sample, idproj, inheritance, chr, start, stop, cn FROM aberration WHERE id = $aid ");
	$row = mysql_fetch_array($query);
	$originh = $row['inheritance'];
	$sid = $row['sample'];
	$pid = $row['idproj'];
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$cn = $row['cn'];
	$close = 0;
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$logq = mysql_query("SELECT uid, entry FROM log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Inheritance/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" . $usrow['FirstName'] .' '.$usrow['LastName'];
			break;
		}
	}
	echo "<h3>Empty explanation is not allowed!</h3>";
	echo "<p>Please argument your decisision to change the inheritance of the following CNV:</p>";
	echo "<ul>\n";
	echo "<li>Region: $region</li>";
	echo "<li>CopyNumber: $cn</li>";
	echo "<li>Previous inheritance: ".$inhtypes[$inhtypes[$originh]]."</li>";
	echo "<li>New inheritance: ".$inhtypes[$inhtypes[$class]]."</li>";
	echo "<li>Previously set by: $setby</li>";
	echo "</ul></p>";
	$close = 0;
	$logentry = "Inheritance Changed From ".$inhtypes[$inhtypes[$inh]]." to ".$inhtypes[$inhtypes[$inh]];
	echo "<p><form action=changeinheritance.php method=POST>";
	echo "<input type=hidden name=logentry value='$logentry'>\n";
	echo "<input type=hidden name=sid value='$sid'>\n";
	echo "<input type=hidden name=pid value='$pid'>\n";
	echo "<input type=hidden name=aid value='$aid'>\n";
	echo "<input type=hidden name=uid value='$uid'>\n";
	echo "<input type=hidden name=inh value='$inh'>\n";

	echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
	echo "<input type=submit name=submit value='Change Inheritance'>\n";
	echo" </form>\n";
}

if ($close == 1) {
	echo "<script type='text/javascript'>";
	echo "var parenturl = window.opener.location.href;\n";
	echo "if (parenturl.indexOf('details') > -1 ){\n";
	echo "	 window.opener.location = parenturl + '&sample=$sid&project=$pid';\n";
	echo "}\n";
	echo "else {\n";
	echo "	 window.opener.location.reload();\n";
	echo "}\n";
	echo "</script>";
	//echo "<script type='text/javascript'>window.opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
?>
