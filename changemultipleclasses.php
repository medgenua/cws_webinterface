<?php
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

echo "<html>";
echo "<head>";
echo "<!-- Bundled General Stylesheet file -->";
echo "<link rel='stylesheet' type='text/css' href='stylesheets/tables.css'/>";
echo" </head>";
echo "<body>";
# POSTED VARS
$toconfirm = $_GET['tc'];
if (isset($_GET['u']) && $_GET['u'] != '') {
	$uid = $_GET['u'];
}
else {
	$uid = $_SESSION['userID'];
}
$sid = $_GET['s'];
$pid = $_GET['p'];
if (isset($_POST['submitted'])) {
	$submitted = $_POST['submitted'];
}
$stripcomma = 0;
$close = 1;
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

$dc = array();
$dc_query = mysql_query("SELECT id, name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
}

## PROCESS SUBMITTED ARGUMENTS ##
if (isset($_POST['submitted'])) {
   foreach($submitted as $key => $aid) {
	$arguments = $_POST['arguments'][$aid];
	if ($arguments != '') {
		$sid = $_POST['sid'][$aid];
		$pid = $_POST['pid'][$aid];
		$logentry = $_POST['logentry'][$aid];
		$prevclass = $_POST['prevclass'][$aid];
		$setbyid = $_POST['setbyid'][$aid];
		$newclass = $_POST['newclass'][$aid];
		// update LOG
		mysql_query("INSERT INTO log (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry','$arguments')");
		$logid = mysql_insert_id();
		// update Aberrations
		$query = "UPDATE aberration SET class='$newclass' WHERE id = '$aid'";
		mysql_query($query);
		// update scantable
		$query = "UPDATE scantable SET class='$newclass' WHERE aid = '$aid'";
		mysql_query($query);
		// notify user (for all overrules but validation) if you're not overruling your own annotation
		if ($arguments != 'Validated' && $uid != $setbyid) {
			$subject = "CNV Annotation Overruled";
			$insquery = mysql_query("INSERT INTO inbox (inbox.from, inbox.to, inbox.subject, inbox.body, inbox.type, inbox.values, inbox.date) VALUES ('$uid','$setbyid','$subject','','cnvannotation','$logid',NOW())");
		}
	}
	else {
		$prevclass = $_POST['prevclass'][$aid];
		$newclass = $_POST['newclass'][$aid];
		$toconfirm .= "$aid-$prevclass-$newclass,";
		$stripcomma = 1;
	}
  }
}
if ($stripcomma == 1) {
	$toconfirm = substr($toconfirm,0,-1);
}
# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";


// split toconfirm
$items = explode(',',$toconfirm);

echo "<h3>The Following CNV Annotation Changes Need Confirmation</h3>";
echo "<p>Please argument your decision to change the diagnostic class of the following CNV:</p>";
echo "<p><form action='changemultipleclasses.php?u=$uid&amp;p=$pid&amp;s=$sid' method=POST>";
$nritems = count($items);
foreach ($items as $key => $content) {
	if ($content == '') {
		continue;
	}
	$close = 0;
	$parts = explode('-',$content);
	$aid = $parts[0];
	$origclass = $parts[1];
	$class = $parts[2];
	$query = mysql_query("SELECT sample, idproj, class, chr, start, stop, cn FROM aberration WHERE id = $aid ");
	$row = mysql_fetch_array($query);
	$origclass = $row['class'];
	$sid = $row['sample'];
	$pid = $row['idproj'];
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$cn = $row['cn'];
	$close = 0;
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$logq = mysql_query("SELECT uid, entry FROM log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setbyid = $logrow['uid'];
			$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
			break;
		}
	}
	
	echo "<table cellspacing=0>\n";
	echo "<tr><th $firstcell colspan=2>$region</th></tr>";
	echo "<tr><th $firstcell class=specalt>CopyNumber</th><td>$region</td></tr>";
	echo "<tr><th $firstcell class=specalt>Previous Class</th><td>$dc[$origclass]</td></tr>";
	echo "<tr><th $firstcell class=specalt>New Class</th><td>$dc[$class]</td></tr>";
	echo "<tr><th $firstcell class=specalt>Previously set by</th><td>$setby&nbsp;</td>";
	echo "<tr><th $firstcell class=specalt>Reason</th><td>";
	if ($class == 'null') {
		$logentry = "Diagnostic Class Changed From $dc[$origclass] to $dc[$class]";
		$arguments = '';
	}
	elseif ($class == $origclass) {
		$logentry = "Diagnostic Class Confirmed As Class $dc[$class]";
		$arguments = 'Validated';
	}
	else {
		$logentry = "Diagnostic Class Changed From $dc[$origclass] to $dc[$class]";
		$arguments = '';
	}
	echo "<input type=hidden name='prevclass[$aid]' value='$origclass'>\n";
	echo "<input type=hidden name='setbyid[$aid]' value='$setbyid'>\n";
	echo "<input type=hidden name='newclass[$aid]' value='$class'>\n";
	echo "<input type=hidden name='logentry[$aid]' value='$logentry'>\n";
	echo "<input type=hidden name='sid[$aid]' value='$sid'>\n";
	echo "<input type=hidden name='pid[$aid]' value='$pid'>\n";
	echo "<input type=hidden name='submitted[]' value=$aid>";
	echo "<input type=text name='arguments[$aid]' value='$arguments' maxsize=255 size=35></td></tr>";
	#echo "<input type=submit name=submit value='Change Class'>\n";
	#echo" </form>\n";
}



if ($close == 1) {
	echo "<script type='text/javascript'>window.opener.location='index.php?page=class&sid=$sid&pid=$pid'</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
else {

	echo "</table></p><p><input type=submit class=button name=submit value='Submit'></p></form>";
}
?>
</body>
</html>
