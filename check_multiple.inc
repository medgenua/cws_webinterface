<?php
 // location of files 
 $dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username/"; 
 $target = $config['DATADIR'];
 // start form
 echo "<form action=\"analyse.php?type=multiple\" method=\"post\">\n";

 // select database
 $db = "CNVanalysis" . $_SESSION["dbname"];
 mysql_select_db("$db");
 ob_end_flush();
 // posted variables
 $projectname = $_POST["TxtName"];
 $group = $_POST['group'];
 $chiptype = $_POST['chiptype'];
 $minsnp = $_POST['minsnp'];
 if (isset($_POST['plink'])) {
	$plink = 1;
 }
 else {
	$plink = 0;
 }
 if (isset($_POST['doY'])) {
	$doY = 1;
 }
 else {
	$doY = 0;
 }
 if (isset($_POST['triPOD'])) {
	$triPOD = 1;
 }
 else {
	$triPOD = 0;
 }
 // page output
 echo "<div class=sectie>\n";
 echo " <h2>New Project Data Overview: $projectname</h2>\n";
 echo "<h4>$chiptype chips</h4>\n";
 echo " <p>Please check and confirm everything below before submitting the analysis</p>\n";
 echo "</div>\n";
 flush();
 // print out hidden form fields
 echo "<input type=hidden value=\"$chiptype\" name=chiptype>\n";
 echo "<input type=hidden value=\"$group\" name=group>\n";
 echo "<input type=hidden value=\"$username\" name=username>\n";
 echo "<input type=hidden value=\"$minsnp\" name=minsnp>\n";
 echo "<input type=hidden value='".$_POST['sharegroup']."' name=sharegroup>\n";
 //###############################
 // GET A RANDOM DATA ID string ##
 //###############################
 $rand = createRandomString(); 
 while (file_exists("$target$rand.lock")) {
	$rand = rand(1,1500);
 }
 // create lock file
 touch("$target$rand.lock");
 echo "<div class=sectie><h3>Input file analysis</h3>";
 echo "<p>Checking Files: <ol>";
 //######################
 //## START DATA CHECK ##
 //######################
 $projectname = str_replace(' ', '_', $projectname); 
 $uploadedgender = 0;
 $uploadeddata = 0;
 $uploadedpedigree = 0;
 $ok = 1;
 // file uploaded by form
 if (basename( $_FILES['genders']['name']) != "") {
	if ($_POST['uploadedgender'] != "") {
		echo "<li style='color:red'>You can only specify one source for the Gender table!</li>\n";
		$ok = 0;
	}
	echo "<li>Using uploaded gender table</li>";
	$genderfile = basename( $_FILES['genders']['name']);
 	$genderfile = str_replace(' ','_',$genderfile);
	$gender_type = $_FILES['genders']['type'];
 }
 // file available on ftp
 elseif ($_POST['uploadedgender'] != "") {
	$genderfile = $_POST['uploadedgender'];
	$sgenderfile = $genderfile;
	$uploadedgender = 1;
	$gender_type = "text/plain";
	echo "<li>Using gender table from FTP</li>";
 }
 else {
	echo "<li style='color:red'>You must specify a gender/callrate table !</li>\n";
	$ok = 0;
 }	
 flush();
 // file uploaded by form
 if (basename( $_FILES['data']['name']) != "") {
	if ($_POST['uploadeddata'] != "") {
		echo "<li style='color:red'>You can only specify one source for the Main Data table !</li>\n"; 
		$ok = 0;
	}
	echo "<li>Using uploaded datafile</li>";
	$datafile = basename( $_FILES['data']['name']);
	$datafile = str_replace(' ','_',$datafile);
	$data_type = $_FILES['data']['type'];
 }
 // file uploaded by ftp
 elseif ($_POST['uploadeddata'] != "") {
		$datafile = $_POST['uploadeddata'];
		$sdatafile = $datafile;
		$uploadeddata = 1;
		$data_type = "text/plain";
		echo "<li>Using datafile from FTP</li>";
 }
 else {
	echo "<li style='color:red'>You must specify a LogR/BAF/GT table !</li>\n";
	$ok = 0;
}
######################
## CHECK FILE TYPES ##
###################### 
 if($data_type == 'application/x-gzip') {
	$datazip = 1;
	$datazips[0] = 1;
	$data = "$target$rand.datafile.0.gz";
 }
 elseif($data_type == 'text/plain') {
	$datazip = 0;
	$datazips[0] = 0;
	$data = "$target$rand.datafile.0.txt";
 }
 else {
	echo "<li style='color:red'>Only .txt or .gz (NOT tar.gz) files allowed for datafiles ";
	echo "(data: $data_type)</li>\n";
	$ok = 0;
 }
 if ($gender_type == 'application/x-gzip') {
	$genderzip = 1;
	$gender = "$target$rand.genderfile.gz";
 }
 elseif ($gender_type == 'text/plain') {
	$genderzip = 0;
	$gender = "$target$rand.genderfile.txt";
 }
 else {
 	echo "<li style='color:red'>Only .txt or .gz (NOT tar.gz) gender-tables allowed ";
	echo "(gender : $gender_type)</li>\n";
   	$ok = 0;
 }

##################################
## GET PED BASE AND TARGET NAME ##
##################################
 // file uploaded by form
 if (basename( $_FILES['pedigree']['name']) != "") {
	if ($_POST['uploadedpedigree'] != "") {
		echo "<li style='color:red'>You can only specify one source for the Pedigree file !</li>\n"; 
		$ok = 0;
	}
	echo "<li>Using uploaded pedigree file</li>";
	$pedigreefile = basename( $_FILES['pedigree']['name']);
	$pedigreefile = str_replace(' ','_',$pedigreefile);
	$pedigree_type = $_FILES['pedigree']['type'];
	$usepedigree = 1 ;
 }
 // file uploaded by ftp
 elseif ($_POST['uploadedpedigree'] != "") {
	$pedigreefile = $_POST['uploadedpedigree'];
	$spedigreefile = $pedigreefile;
	$uploadedpedigree = 1;
	$pedigree_type = "text/plain";
	$usepedigree = 1;
	echo "<li>Using pedigree file from FTP</li>";
 }
 else {
	$usepedigree = 0;
 }
 if ($usepedigree == 1 && $pedigree_type == 'text/plain') {
	$pedzip = 0;
	$pedigree = "$target$rand.pedigree.txt";
	$usepedigree = 1;
 }
 elseif ($usepedigree == 1 && $pedigree_type == 'application/x-gzip') {
	$pedzip = 1;
	$pedigree = "$target$rand.pedigree.gz";
	$usepedigree = 1;
 }
 elseif ($usepedigree == 1)  {
	echo "<li style='color:red'>Only .txt or .gz (NOT tar.gz/rar/zip/) files allowed ";
	echo "(pedigree file : $pedigree_type</li>\n";
	$usepedigree = 0;
 }
 if ($usepedigree == 0 && $triPOD == 1) {
	echo "<li style='color:red'>triPOD analysis disabled, no pedigree file was provided.</li>";
 } 
echo "</ol></p>";
//####################################
//## MOVE FILES TO TARGET POSITIONS ##
//####################################
flush();
 if ($ok == 1) {
	echo "<p>Moving files to the Analysis location:<ol>";
	// gender file
	echo "<li>Gender File:</li>";
	flush();
	if ($uploadedgender == 1) {
		#if(copy($dir.$genderfile, $gender)) {
		if(symlink($dir.$genderfile, $gender)) {
		}
		else {
			echo "<li style='color:red'>Copying $genderfile to $gender failed</li>\n";
			$ok = 0;
		}
		flush();
	}
	else {
		if(move_uploaded_file($_FILES['genders']['tmp_name'], $gender) ) {
   		}
	 	else {
			echo "<li style='color:red'>Copying ".$_FILES['genders']['tmp_name']." to $gender failed</li>";
  			$ok = 0;
 		}
	}
	flush();
	// data
	echo "<li>Data File:</li>";
	flush();
	if ($uploadeddata == 1) {
		#if(copy($dir.$datafile,$data)) {
		if (symlink($dir.$datafile,$data)) {
		}
		else {
			echo "<li style='color:red'>Linking $datafile to $data failed</li>\n";
			$ok = 0;
		}
	}
	else {
		if(move_uploaded_file($_FILES['data']['tmp_name'], $data)) {
		}
 		else {
			echo "<li style='color:red'>Copying ".$_FILES['data']['tmp_name']." to $data failed</li>";
  			$ok = 0;
 		}
 	}
	// pedigree
	echo "<li>Pedigree File</li>";
	flush();
	if ($usepedigree == 1) {
	    if ($uploadedpedigree == 1) {
		#if(copy($dir.$pedigreefile,$pedigree)) {
		if(symlink($dir.$pedigreefile,$pedigree)) {
		}
		else {
			echo "<li style='color:red'>Copying $pedigreefile to $pedigree failed";
			echo "Pedigree information will *NOT* be used</li>\n";
			$usepedigree = 0;
		}
	    }
	    else {
		if (move_uploaded_file($_FILES['pedigree']['tmp_name'],$pedigree)) {
		}
		else {
			echo "<li style='color:red'>Copying ".$_FILES['pedigree']['tmp_name']." to $pedigree failed. ";
			echo "Pedigree information will *NOT* be used</li>\n";
			$usepedigree = 0;

		}
	    }
	}
 }
 echo "</ol></p>";
 flush();
 echo "</div>";
 if ($ok == 1 ) {
	////////////////////
  	//scan genderfile //
	////////////////////
  	$nrsamples = 0;
	if ($genderzip == 1) {
		$fh = gzopen("$gender","r");
		$line = gzgets($fh);
	}
	else {
  		$fh = fopen("$gender","r");
	  	$line = fgets($fh);
	}
  	$line = rtrim($line);
  	$header = explode("\t",$line);
  	if (in_array("Sample ID",$header) && in_array("Gender",$header) && in_array("Call Rate",$header) && in_array("Index",$header)) {
   		$idpos = array_search("Sample ID", $header);
   		$genpos = array_search("Gender",$header);
   		$crpos = array_search("Call Rate",$header);
  		$inpos = array_search("Index",$header);
   		$genderok = 1;
   		echo "<div class=sectie><h3>Samples:</h3>\n<h4>File ok...</h4>";
   		echo "<table id=mytable cellspacing=0>\n <tr>\n  <th $firstcell>Index</td>\n  <th>Sample ID</td>\n  <th>Gender</td>\n  <th>Call Rate</td>\n  <th>In Track</td>\n  <th>For Track</td></tr>\n";
 		if ($genderzip == 1) {
   		   while(!gzeof($fh)) {
    			$line = gzgets($fh);
    			$line = rtrim($line);
    			$pieces = explode("\t",$line);
    			if ($pieces[0] != "") {
      				$nrsamples += 1;
      				$query = mysql_query("Select intrack from sample where chip_dnanr = \"$pieces[$idpos]\""); 
      				$row = mysql_fetch_array($query, MYSQL_ASSOC);
      				$nrrows = mysql_num_rows($query);	 
      				$intrack = "";
      				$fortrack = "";
      				if ($nrrows == 1 && $row['intrack'] == 1) {
        				$intrack = "<img src=\"images/bullets/accept.png\">";
					$fortrack = "DISABLED";
      				}
      				else { 
					$fortrack = "checked";
					$intrack = "<img src=\"images/bullets/no.png\">";
				}
      				echo " <tr>\n";
				echo "  <td $firstcell>$pieces[$inpos]</td>\n";
				echo "  <td>$pieces[$idpos]</td>\n";
				echo "  <td>$pieces[$genpos]</td>\n";
				echo "  <td>$pieces[$crpos]</td>\n";
				echo "  <td><center>$intrack</center></td>\n";
				echo "  <td><center><input type=\"checkbox\" name=\"$pieces[$idpos]\" $fortrack></center></td>\n";
				echo " </tr>\n";
      				$samples[$nrsamples] = $pieces[$idpos];
    			}
   		   }
		}
		else {
		   while(!feof($fh)) {
    			$line = fgets($fh);
    			$line = rtrim($line);
    			$pieces = explode("\t",$line);
    			if ($pieces[0] != "") {

      				$nrsamples += 1;
				$style='';
				$sname = $pieces[$idpos];
				// check sample name syntax
				if (!preg_match("/^[a-zA-Z0-9\-_]+$/",$pieces[$idpos])){
					$sname = preg_replace("/[^a-zA-Z0-9\-_]/","_",$pieces[$idpos]);
					$style="style='background-color:#FFFF6F;' title='Illegal samplename. Will be replaced by \"$sname\"'";
				}
      				$query = mysql_query("Select intrack from sample where chip_dnanr = '$sname'"); 
      				$row = mysql_fetch_array($query, MYSQL_ASSOC);
      				$nrrows = mysql_num_rows($query);	 
      				$intrack = "";
      				$fortrack = "";
      				if ($nrrows == 1 && $row['intrack'] == 1) {
        				$intrack = "<img src=\"images/bullets/accept.png\">";
					$fortrack = "DISABLED";
      				}
      				else { 
					$fortrack = "checked";
					$intrack = "<img src=\"images/bullets/no.png\">";
				}
      				echo " <tr>\n";
				echo "  <td $firstcell>$pieces[$inpos]</td>\n";
				echo "  <td $style>$pieces[$idpos]</td>\n";
				echo "  <td>$pieces[$genpos]</td>\n";
				echo "  <td>$pieces[$crpos]</td>\n";
				echo "  <td><center>$intrack</center></td>\n";
				echo "  <td><center><input type='checkbox' name='track[$sname]' $fortrack></center></td>\n";
				echo " </tr>\n";
      				$samples[$nrsamples] = $pieces[$idpos];
    			}
   		   }

		}
   		echo "</table></div>";
 	}
 	else {
    		$genderok = 0;
    		echo "<div class=sectie><h3>Problem with gender file</h3>";
    		echo "<p>Go back and check the needed columns in the Gender-table please.</p></div>";  
  	}
	if ($genderzip == 1) {
	  	gzclose($fh);
	}
	else {
		fclose($fh);
	}
	////////////////////
  	// check datafile //
	////////////////////
	if ($datazip == 1) {
	  	$fh = gzopen("$data","r");
  		$line = gzgets($fh);
	}
	else {
		$fh = fopen("$data","r");
  		$line = fgets($fh);
	}
  	$line = rtrim($line);
  	$pieces = explode("\t",$line);
  	$dataok = 1;
	// what columns do i need?
	$neededcols = explode('|',$_POST['neededcols']);
	$neededstring = $_POST['neededcols'];
	if ($plink == 1 && !in_array('%samplegt%',$neededcols)) {
		echo "<p>Adding Sample_Genotype to needed columns. This column is needed by Plink homozygosity mapping.</p>";
		array_push($neededcols, '%samplegt%');
		$neededstring .= '|%samplegt%';
	}
	$missingcols = array();
  	// check columns in datafile.
	for ($i=1;$i<=$nrsamples;$i++) {
		$colreplace = array('%probename%' => 'Name', '%chromosome%' => 'Chr', '%position%' => 'Position', '%samplelogr%' => $samples[$i].'.Log R Ratio', '%samplebaf%' => $samples[$i].'.B Allele Freq', '%samplegt%' => $samples[$i].'.GType');
   		foreach ($neededcols as $key => $colname) {
			if (!in_array($colreplace[$colname],$pieces)) {
				$missingcols[$colreplace[$colname]] = 1;
				$dataok = 0;
			}
		}
  	}   
  	if ($dataok == 1) {
    		echo "<div class=sectie><h3>Intensity Values:</h3>\n<h4>File ok...</h4>\n";
    		echo "<p>All the needed columns corresponding to the samples above are found.</p>\n</div>\n";
		echo "<input type=hidden name='neededcols' value='$neededstring'>";
  	}
  	else { 
    		echo "<div class=sectie><h3>Problem with data file</h3>\n<p>Make sure the correct columns are in your data table, and that the samples correspond to the samples specified in the genders table. The following columns are missing:</p><ul class=ul-nodisc>";
		foreach ($missingcols as $missing => $dummy) {
			echo "<li>- $missing </li>";
		}	
		echo "</ul></div>\n";
  	}

	$chrok = 1;
	$chromosomes = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "X", "Y", "XY");
	//$chromosomes_file = array();
        while (($line = fgets($fh)) != false) {
		$columns = explode("\t", $line);
		$chrom = $columns[1];
		if (($key = array_search($chrom, $chromosomes)) !== false) {
    			unset($chromosomes[$key]);
		}
	}

	if ($datazip == 1) {
		gzclose($fh);
	}
	else {
		fclose($fh);
	}

	//print_r($chromosomes);
	if (! empty($chromosomes)) {
		$chrok = 0;
	}

	echo "<div class=sectie><h3>Chromosome check:</h3>\n";
	if ($chrok == 1) {
		 echo "<h4>File ok...</h4>\n";
		 echo "<p>Data for all chromosomes is found in the data file.</p>\n</div>\n";
	}
	else {
		echo "<h4>Warning!</h4>\n";
		echo "<p>Data for following chromosomes was not found in the input data file:</p><ul class=ul-nodisc>";
		foreach ($chromosomes as $missing) {
			echo "<li>- $missing </li>";
		}
		echo "</ul></div>\n";
	}
 }
 else {
 	echo "<div class=sectie><h3>There was an uploading error</h3>\n<p>Please go back and check that the files are plain text (.txt) files. </p><p>Then try again, if it failes again, contact the webmaster</p>\n</div>\n";
 }

// print the from to continue if all is ok
if ($dataok == 1 && $genderok == 1 ) {
    	echo "<div class=sectie>\n";
	// print remaining entries to form
  	echo "<input type=hidden name=\"usepedigree\" value=\"$usepedigree\">\n";
	echo "<input type=hidden name=pedzip value=$pedzip>\n";
	echo "<input type=hidden name=\"TxtName\" value=\"$projectname\">\n";
	echo "<input type=hidden name=rand value=\"$rand\">";
    	echo "<input type=hidden value=\"$username\" name=username>\n";
	echo "<input type=hidden name=genderzip value=$genderzip>\n";
	echo "<input type=hidden name=triPOD value=$triPOD>\n";
	if (isset($_POST['asym'])) {
		echo "<input type=hidden value=1 name=asym>\n";
	}
	else {
		echo "<input type=hidden value=0 name=asym>\n";
	}
	echo "<input type=hidden value='".$_POST['asymminconf']."' name=asymminconf>";
	echo "<input type=hidden value='".$_POST['asymmethod']."' name=asymmethod>";
	echo "<input type=hidden value='".$_POST['asymtype']."' name=asymtype>";
	if (isset($_POST['plink'])) {
		echo "<input type=hidden value=1 name=plink>\n";
	}
	else {
		echo "<input type=hidden value=0 name=plink>\n";
	}
	if (isset($_POST['doY'])) {
		echo "<input type=hidden value=1 name=doY>\n";
	}
	else {
		echo "<input type=hidden value=0 name=doY>\n";
	}

    	
 }

//#############################################
//## next check the supplementary data files ##
//#############################################

// FIRST: files provided by the form.
$datafiles = $_FILES['datafiles'];
$nrfiles = count($datafiles['name']);
$data = '';
echo "<div class=sectie>";
echo "<h3>Scanning for Supplementary datafiles</h3><p>";
$datacounter = 0;
for($idx = 0; $idx < $nrfiles ; $idx++) {
	flush();
	#echo "Idx $idx: ";
	$datacounter++; // = $idx+1;
	if (basename( $datafiles['name'][$idx]) != "") {
		$thefile = basename( $datafiles['name'][$idx]);
		$thefile = str_replace(' ','_',$thefile);
		$thetype = $datafiles['type'][$idx];
		if ($thetype == 'application/x-gzip') {
			$datazips[$datacounter] = 1;
			$data = "$target$rand.datafile.$datacounter.gz";
		}
		elseif ($thetype == 'text/plain') {
			$datazips[$datacounter] = 0;
			$data = "$target$rand.datafile.$datacounter.txt";
		}
		else {
			echo "Ignoring Supplementary file '$thefile' : Not in .txt/.gz format<br/>";
			unset($datazips[$datacounter]);
			$datacounter--;
			continue;
		}
		if(move_uploaded_file($datafiles['tmp_name'][$idx], $data)) {
			echo "Supplementary file '$thefile' uploaded.<br/>";
		}
 		else {
			echo "Supplementary file '$thefile' failed to upload! (will be discarded)<br/>";
			unset($datazips[$datacounter]); 
			$datacounter--;
			continue;
 		}
	}
	else {
		echo "Supplementary file $datacounter : empty basename, will be discarded.<br/>";
		$datacounter--;
		continue;
	}
}

// SECOND : files provided by ftp
$uploadeddatafiles = $_POST['uploadeddatafiles'];
$nruplfiles = count($uploadeddatafiles);
// added to data array
#echo "<div class=sectie>";
#echo "<h3>Scanning for Supplementary datafiles</h3><p>";
for($idx = 0; $idx < $nruplfiles ; $idx++) {
	flush();
	$datacounter++;
	$thefile = $uploadeddatafiles[$idx];
	$datazips[$datacounter] = 0;
	$data = "$target$rand.datafile.$datacounter.txt";
	if(symlink($dir.$thefile, $data)) {
	#if(copy($dir.$thefile, $data)) {
		echo "Supplementary file '$thefile' from ftp location included in analysis.<br/>";
	}
	else {
		echo "Supplementary file '$thefile' failed to copy from ftp location! (will be discarded)<br/>";
		unset($datazips[$datacounter]); 
		$datacounter--;
		continue;
 	}
}
echo "</p></div>";
#########################################################
## print out total number of data files and file types ##
#########################################################
echo "<input type=hidden name=nrsupdata value=$datacounter>\n";
if ($datacounter > 0) {
	ksort($datazips);
	$zipval = implode(',',$datazips);
	echo "<input type=hidden name=\"datazips\" value='$zipval'>\n";
}
else {
	echo "<input type=hidden name=\"datazips\" value=''>\n";
}

## close form and print submit button if data is ok.
if ($dataok == 1 && $genderok == 1 ) {
	echo "<p><input type=submit class=button value=\"Start Analysis\"></p>\n</div>\n";
    	echo "</div>\n";
    	echo "</form>\n";
}




##############
## FUNCTION ##
##############
function createRandomString() { 

    $chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHIFKLMNOPQRSTUVWXYZ"; 
    $length = strlen($chars) - 1;
    $i = 0; 
    $string = '' ; 

    while ($i <= 4) { 
        $num = rand(0,$length); 
        $string .= substr($chars, $num, 1); 
        $i++; 
    } 

    return $string; 

} 

?>
