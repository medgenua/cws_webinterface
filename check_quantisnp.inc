
<?php 
 echo "<form action=\"analyse.php?type=quantisnp\" method=\"post\">\n";
 $db = "CNVanalysis" . $_SESSION["dbname"];
 ob_start();
 $projectname = $_POST["TxtName"];
 
 echo "<div class=sectie>\n";
 echo " <h2>QuantiSNP Settings overview: $projectname</h2>\n";
 echo " <p>Please check and confirm everything below before submitting the analysis</p>\n";
 echo "</div>\n";
 $projectname = str_replace(' ', '_', $projectname); 
 
 $config = $_POST["config"];
 $emiters = $_POST["emiters"];
 $lsetting = $_POST["lsetting"];
 $maxcopy = $_POST["maxcopy"];
 $dogccorrect = $_POST["dogccorrect"];
 $gcdir = $_POST["gcdir"];
 $printrs = $_POST["printrs"];
 $minsnp = $_POST["minsnp"];
 $minlbf = $_POST["minlbf"];
 $chiptype = $_POST["chiptype"];
 $target = $config['DATADIR']; //"$scriptdir/datafiles/";
 $genderfile = basename( $_FILES['genders']['name']);
 $genderfile = str_replace(' ','_',$genderfile);
 $gender = $target . $projectname . "_" . $genderfile ;
 $datafile = basename( $_FILES['data']['name']);
 $datafile = str_replace(' ','_',$datafile);
 $data = $target . $projectname . "_" . $datafile;
 $ok=1;
 $gender_type = $_FILES['genders']['type'];
 $data_type = $_FILES['data']['type'];
 if (!($gender_type == "text/plain") || !($data_type == "text/plain")) {
 // echo "Enkel .txt bestanden toegelaten<br>";
   $ok = 0;
 }
 if ($ok == 1) {
  if(move_uploaded_file($_FILES['genders']['tmp_name'], $gender) )
   {
   }
 else {
  $ok = 0;
 }
 if(move_uploaded_file($_FILES['data']['tmp_name'], $data)) 
 {
 }
 else {
  $ok = 0;
 }
 }
 if ($ok == 1 ) { 
  //check genderfile
  $nrsamples = 0;
  $fh = fopen("$gender","r");
  $line = fgets($fh);
  $line = rtrim($line);
  $header = explode("\t",$line);
  if (in_array("Sample ID",$header) && in_array("Gender",$header) && in_array("Call Rate",$header) && in_array("Index",$header)) {
   $idpos = array_search("Sample ID", $header);
   $genpos = array_search("Gender",$header);
   $crpos = array_search("Call Rate",$header);
   $inpos = array_search("Index",$header);
   $genderok = 1;
   echo "<div class=sectie><h3>Samples:</h3>\n<h4>File ok...</h4>";
   echo "<table id=mytable cellspacing=0>\n <tr>\n  <th $firstcell>Index</td>\n  <th>Sample ID</td>\n  <th>Gender</td>\n  <th>Call Rate</td>\n</tr>\n";
   while(!feof($fh)) {
    $line = fgets($fh);
    $line = rtrim($line);
    $pieces = explode("\t",$line);
    if ($pieces[0] != '') {
      $nrsamples += 1;
      echo " <tr>\n  <td $firstcell>$pieces[$inpos]</td>\n  <td>$pieces[$idpos]</td>\n  <td>$pieces[$genpos]</td>\n  <td>$pieces[$crpos]</td>\n  </tr>\n";
      $samples[$nrsamples] = $pieces[$idpos];
    }  
   }
   echo "</table></div>";
  }
  else {
    $genderok = 0;
    echo "<div class=sectie><h3>Problem with gender file</h3>";
    echo "<p>Go back and check the columns in the Gender-table please.</p></div>";  
  }
  fclose($fh);
  //check datafile
  $fh = fopen("$data","r");
  $line = fgets($fh);
  $line = rtrim($line);
  $pieces = explode("\t",$line);
  $dataok = 1;
  for ($i=1;$i<=$nrsamples;$i++) {
   $log = $samples[$i] . ".Log R Ratio";
   $ball = $samples[$i] . ".B Allele Freq"; 
   //if ($pieces[3+($i-1)*2] == $log && $pieces[4+($i-1)*2] == $ball) {
   if (in_array($log,$pieces) && in_array($ball, $pieces)) {
   }
   else {$dataok = 0;}
  }   
  if (in_array("Name",$pieces) && in_array("Chr",$pieces) && in_array("Position", $pieces)) {
  }
  else {$dataok = 0;}
  if ($dataok == 1) {
    echo "<div class=sectie><h3>Intensity Values:</h3>\n<h4>File ok...</h4>\n";
    echo "<p>All the needed columns corresponding to the samples above are found.</p>\n</div>\n";
  }
  else { 
    echo "<div class=sectie><h3>Problem with intensities</h3>\n<p>Make sure the correct columns are in your intensity/b-allele table, and that the samples correspond to the samples specified in the genders table</p></div>\n";
  }
  echo "<div class=sectie>\n";
  echo "<h3>Parameters:</h3>\n";
  echo "<table id=mytable cellspacing=0>\n";
  echo "<tr><th $firstcell>Parameter</td><th>Value</td></tr>\n";
  echo "<tr><td $firstcell>Project Name: </td><td>$projectname <input type=hidden name=\"TxtName\" value=\"$projectname\"></td></tr>\n";
  echo "<tr><td $firstcell>Config File: </td><td>$config <input type=hidden name=\"config\" value=\"$config\"></td></tr>\n";
  echo "<tr><td $firstcell>EM Iterations:</td><td>$emiters <input type=hidden name=\"emiters\" value=\"$emiters\"></td></tr>\n";
  echo "<tr><td $firstcell>LSetting: </td><td>$lsetting <input type=hidden name=\"lsetting\" value=\"$lsetting\"></td></tr>\n";
  echo "<tr><td $firstcell>Max Copy Number: </td><td>$maxcopy <input type=hidden name=\"maxcopy\" value=\"$maxcopy\"></td></tr>\n";
  echo "<tr><td $firstcell>GC Correction: </td><td>$dogccorrect <input type=hidden name=\"dogccorrect\" value=\"$dogccorrect\"></td></tr>\n";
  echo "<tr><td $firstcell>GC-Data: </td><td>$gcdir <input type=hidden name=\"gcdir\" value=\"$gcdir\"></td></tr>\n";
  echo "<tr><td $firstcell>Use Probenames: </td><td>$printrs <input type=hidden name=\"printrs\" value=\"$printrs\"></td></tr>\n";
  echo "<tr><td $firstcell>Min. nr. SNP's: </td><td>$minsnp <input type=hidden name=\"minsnp\" value=\"$minsnp\"></td></tr>\n";
  echo "<tr><td $firstcell>Min. Log Bayes: </td><td>$minlbf <input type=hidden name=\"minlbf\" value=\"$minlbf\"></td></tr>\n";
  echo "<tr><td $firstcell>Chiptype: </td><td>$chiptype <input type=hidden name=chiptype value=\"$chiptype\"></td></tr>\n";
  echo "</table>\n";
  echo "<input type=hidden name=\"data\" value=\"$data\">\n<input type=hidden name=\"gender\" value=\"$gender\">\n";
  echo "</div>\n"; 

 }
 else {
  echo "<div class=sectie><h3>There was an uploading error</h3>\n<p>Please go back and check that the files are plain text (.txt) files. </p><p>Then try again, if it failes again, contact the webmaster</p>\n</div>\n";
 }
  echo "<div class=sectie>\n";
  echo "<input type=hidden value=\"$username\" name=username>\n";
  
  echo "<p><input type=submit class=button value=\"Start Analysis\"></p>\n</div>\n";
  echo "</div>\n";
  echo "</form>\n";
?>
