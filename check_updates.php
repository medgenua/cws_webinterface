<?php
// load credentials
include(".LoadCredentials.php");

// variable to hold if updates
$updates = 0;

// random value for output files
$rand = rand(1,1500);

// Check for updates for Web-Interface
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $sitedir && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") >> /tmp/output 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! stristr($content,'no changes found')) {
	$updates = 1;
}
if ($webonly != 1) {
	// Check for updates for CNV Analysis scripts
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") >> /tmp/output 2>&1"; 
	system($command);
	$content = file_get_contents("/tmp/$rand.git.out");
	system("rm -f /tmp/$rand.git.out");
	if (! stristr($content,'no changes found')) {
		$updates = 1;
	}
}
// Check for updates for Credentials scripts
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $maintenancedir/../.Credentials && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if ( !stristr($content,'no changes found')) {
	$updates = 1;
}


	
// Check for updates for Maintenance scripts
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $maintenancedir && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! $content) {
	$updates = 1;
}

// Check for updates for Database 
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir/../Database_Revisions && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! $content) {
	$updates = 1;
}

// return the result. 
echo $updates;
?>
