<?php
//Tell the browser what kind of file is coming in
header("Content-Type: image/png");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET POSTED VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$plotwidth = $_GET['pw'] ;
// CHECK WHAT TO SHOW IN PLOT

// DEFINE SOME VARS for IMAGE SIZE
$result =  mysql_query("SELECT start FROM cytoBand WHERE chr = '$chr' ORDER BY start LIMIT 1");
$row = mysql_fetch_array($result);
$chrstart = $row['start'];
$result =  mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$chrstop = $row['stop'];
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];
$window = $chrstop-$chrstart+1;

# DEFINE IMAGE PROPERTIES
$height = 30 ;
$scalef = ($plotwidth-1)*0.80/($window);

//Specify constant values
$width = ($plotwidth -1) * 0.80 +5; //Image width in pixels

//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

# CREATE SCALE
# CREATE KARYO BANDS
$y = 10;  //start here

$arm = 'p';
$query = mysql_query("SELECT start, stop, name, gieStain FROM cytoBand WHERE chr= '$chr' ORDER BY start");

while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$name = $row['name'];
	$gieStain = $row['gieStain'];
	$scaledstart = intval(round(($cstart)*$scalef));
	$scaledstop = intval(round(($cstop)*$scalef));
	if ($cstop == $lastp) {
		if ($gieStain != 'gneg') {
			imagefilledpolygon($image, array($scaledstart,$y,$scaledstop,($y+5),$scaledstart,($y+10)),3, $colors[$gieStain]);
		}
		imagepolygon($image, array($scaledstart,$y,$scaledstop,$y+5,$scaledstart,$y+10),3,$black);
		$nexttriang = 1;
	}
	elseif (isset($nexttriang) && $nexttriang == 1) {
		if ($gieStain != 'gneg') {
			imagefilledpolygon($image, array($scaledstart,$y+5,$scaledstop,$y,$scaledstop,$y+10),3,$colors[$gieStain]);
		}
		imagepolygon($image, array($scaledstart,$y+5,$scaledstop,$y,$scaledstop,$y+10),3,$black);
		$nexttriang = 0;
	}
	else{
		if ($gieStain != 'gneg') {
			imagefilledrectangle($image, $scaledstart, $y, $scaledstop, $y+10, $colors[$gieStain]);
		}
		imagerectangle($image, $scaledstart, $y, $scaledstop, $y+10, $black);
	}
	$fontwidth = imagefontwidth(1);
	$fullname = $chrtxt . $name;
	$txtwidth = strlen($fullname)*$fontwidth;
	if ($txtwidth+2 < ($scaledstop-$scaledstart)) {
		$txtx = ($scaledstop+$scaledstart)/2 - $txtwidth/2+1;
		if ($gieStain != "gpos100" && $gieStain != "gpos75") {
			imagestring($image,1,$txtx,$y+1,$fullname,$black);		
		}
		else {
			imagestring($image,1,$txtx,$y+1,$fullname,$white);
		}

	}

}

// PLOT BOX FOR CURRENT REGION

$scaledstart = intval(round(($start)*$scalef));
$scaledstop = intval(round(($stop)*$scalef));
imagerectangle($image,$scaledstart,7,$scaledstop,23,$red);	

//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

