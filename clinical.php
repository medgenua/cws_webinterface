<?php

## POSTED VARS
$sid = $_GET['sid'];
$uid = $_GET['uid'];
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
$lddb = "LNDB";
mysql_select_db("$cnvdb");
// PRINT THE STRUCTURED PHENOTYPES
echo "<div class=nadruk style='margin-left:7px'>Structured Phenotypic Data:</div>";
$parray = array();
if (!isset($_GET['cstm']) || $_GET['cstm'] != 1) {
	$query = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
}
else {
	$pid = $_GET['pid'];
	$query = mysql_query("SELECT lddbcode FROM cus_sample_phenotypes WHERE sid = '$sid' AND pid = '$pid' ORDER BY lddbcode");
}
$codes = array();
while ($row = mysql_fetch_array($query)) {
	$code = $row['lddbcode'];
	$codes[] = $code;
	$pieces = explode('.',$code);
	$parray[$pieces[0]][$pieces[1]][] = $pieces[2];
}
mysql_select_db($lddb);
if (count($codes) == 0) {
	echo "<p style='margin-left:15px'>Not Available</p>";
}
else {
	echo "<p style='margin-left:15px'>";
	echo "<table cellspacing=0 style='margin-left:15px'>";
	echo "<tr>\n";
	echo "<th $firstcell class=compact>Primary</th><th class=compact>Secondary</th><th class=compact>Tertiary</th>\n";
	echo "</tr>\n";
	foreach($parray as $first => $sarray) {
		$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
		$row = mysql_fetch_array($query);
		$firstlabel = $row['LABEL'];
		foreach ($sarray as $second => $tarray) {
			if ($second == '00') {
				echo "<tr><td $firstcell class=compact>$firstlabel</td><td class=compact>&nbsp;</td><td class=compact>&nbsp;</td></tr>\n";
			} 
			else {
				$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
				$row = mysql_fetch_array($query);
				$secondlabel = $row['LABEL'];
				foreach ($tarray as $third) {
					if ($third == '00') {
						echo "<tr><td $firstcell class=compact>$firstlabel</td><td class=compact>$secondlabel</td><td class=compact>&nbsp;</td></tr>\n";	
					}
					else {
						$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
						$row = mysql_fetch_array($query);
						$thirdlabel = $row['LABEL'];
					
						echo "<tr><td $firstcell class=compact>$firstlabel</td><td class=compact>$secondlabel</td><td class=compact>$thirdlabel</td></tr>\n";
					}	
				}
			}
		}
	}
	echo" </table></p>";
}
## check freetext
echo "<div class=nadruk style='margin-left:7px'>Freetext Clinical Info</div>";

mysql_select_db($cnvdb);
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
if ($_GET['cstm'] != 1) {
	$clinquery = mysql_query("SELECT DECODE(clinical, '$encpass') as clindata FROM sample WHERE id = $sid");
}
else {
	$clinquery = mysql_query("SELECT DECODE(clinical, '$encpass') as clindata FROM cus_sample WHERE idsamp = '$sid' AND idproj = '$pid'");
}
$row = mysql_fetch_array($clinquery);
$clindata = $row['clindata'];
if ($clindata != '') {
	$clindata = str_replace(array("\r\n", "\r", ),"\n",$clindata);
	$nrlines = substr_count($clindata,"\n");
	echo "<div class=clinical style='margin-left:15px'>";
	echo "$clindata</div>\n";

}
else {
	echo "<p style='margin-left:15px;'>Not Available</p>";
}

?>
