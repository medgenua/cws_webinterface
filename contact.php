<html>
<head><title>PHP Mail Sender</title></head>
<body>
<?php
/* All form fields are automatically passed to the PHP script through the array $_POST. */
$name = $_POST['name'];
$email = $_POST['email'];
$affiliation = $_POST['affiliation'];
$text = $_POST['text'];
$subject = $_POST['subject'];
$check = $_POST['check'];

/* PHP form validation: the script checks that the Email field contains a valid email address and the Subject field isn't empty. preg_match performs a regular expression match. It's a very powerful PHP function to validate form fields and other strings - see PHP manual for details. */
if (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email)) 
 {
  echo "<div class=sectie><h3>Invalid email address</h3><p>Please correct your emailadres to a valid one.</p></div>";
  include('page_contact.php');
 }
elseif ($affiliation == "affiliation") 
 {
  echo "<div class=sectie><h3>No affiliation specified</h3><p>Please enter the university or company you work for.</p></div>";
  include('page_contact.php');
 }
 
elseif ($name == "name") 
 {
  echo "<div class=sectie><h3>No name specified</h3><p>Please enter your name.</p></div>";
  include('page_contact.php');
 }
elseif ($text == "message")
 {
  echo "<div class=sectie><h3>No message specified</h3><p>Please type a message, since that's what this form is meant for.</p></div>";
  include('page_contact.php');
 }
elseif ($subject == "subject")
 {
  echo "<div class=sectie><h3>No subject specified</h3><p>Please enter a subject for your message</p></div>";
  include('page_contact.php');

 }
elseif ($check != "G" && $check != "g")
 {
  echo "<div class=sectie><h3>Wrong answer</h3><p>Answer the question please. This is a simple anti-spam check. If you don't know the answer, it's \"G\". :-) </p></div>";
  include('page_contact.php');
  }
else 
 {
  //compose message, etc
  $domain = $_SERVER['HTTP_HOST'];
  $message = "Message sent from http://$domain\r";  
  $message .= "Sent by: $email\r";
  $message .= "Subject: $subject\r";
  $message .= "Message: \r\r";
  $message .= "$text\r";
  $headers = "From: $email\r\n";
  $headers .= "To: $adminemail\r\n";
  /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
if (mail($email,"$subject",$message,$headers)) {
	 
	echo "<h4>Thank you for contacting us.</h4>";
	echo "<p>The email has also been sent your own specified address.</p>";
	echo "<p><a href=\"index.php?page=main\">Back to Homepage</a></p>";
} 
else {
  echo "<h4>Can't send email </h4>";
}

}
?>
</body>
</html>

