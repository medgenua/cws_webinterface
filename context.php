<?php
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
// get list of links to show
$uid = $_GET['uid'];
$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$uid'");
$row = mysql_fetch_array($query);
$db_links = $row['db_links'];


$type = $_GET['type'];
$aid = $_GET['aid'];

## loh ?
$fromcontext = $_GET['fc'];
if (isset($_GET['cstm']) && $_GET['cstm'] == 1) {
	$tbl = 'cus_';
}
else {
	$tbl = '';
}
// type 1: from ajax image context
if ($type == 'a') {
	if (substr($aid,0,4) == 'loh-') {
		$loh = 1;
		$mos = 0;
		$aid = substr($aid,4);
		$table = 'aberration_LOH';
		$fields = '';
	}
	elseif (substr($aid,0,4) == 'mos-') {
		$mos = 1;
		$loh = 0;
		$aid = substr($aid,4);
		$table = 'aberration_mosaic';
		$fields = ',a.cn';
	}
	else {
		$loh = 0;
		$mos = 0;
		$table = 'aberration';
		$fields = ', a.cn';
	}

	echo "<div id=contextpop>\n";
	#########################
	## GET CNV INFORMATION ##
	#########################
	$result = mysql_query("SELECT a.start, a.stop, a.chr, a.cn, a.sample, a.pubmed, a.idproj, s.gender $fields FROM $table a JOIN `sample` s  ON a.sample = s.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$start = $row['start'];
	$stop = $row['stop'];
	$chr = $row['chr'];
	$sid = $row['sample'];
	$gender = $row['gender'];

	if ($loh != 1) {
		$cn = $row['cn'];
		// if($mos == 1) {
		// 	$cn = round($cn);
		// }
		if ($gender == 'Male' && ($chr == 23 || $chr == 24)) {
			$normal_cn = 1;
		}
		else {
			$normal_cn = 2;
		}

		if ($cn > $normal_cn) {
			$cntype = 'DUP';
		}
		elseif ($cn < $normal_cn) {
			$cntype = 'DEL';
		}

		error_log($cntype);
		
	}

	$pid = $row['idproj'];
	$pubmed = $row['pubmed'];
	$size = $stop - $start +1;
	$chrtxt = $chromhash[ $chr];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	##################
	## get CHIPTYPE ##
	##################
	$result = mysql_query("SELECT chiptypeid FROM project WHERE id = $pid");
	$row = mysql_fetch_array($result);
	$chiptypeid = $row['chiptypeid'];
	#######################################
	## TIP ORIGIN DEPENDENT PAGE HEADERS ##
	#######################################
	if ($fromcontext != 1) {
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
		echo "<h3 style='margin-top:5px'>$region</h3></div>\n";
	}
	else {
		echo "<div style='margin-left:7px'>";
	}
	echo "<div class=sectie style='position:relative;border-bottom-style:none;'> ";
	echo "<span style='position:absolute;right:2px;top:1px;'>";
	echo "<a class=tt style='font-style:italic;font-size:0.8em;' href='javascript:void(0)' onclick=\"LoadPreferences('PickDBlinks')\">Set Resources</a>";
	echo "</span>";
	echo "<div class=nadruk>Public Resources: </div>";
	echo "<ul id=ul-simple>\n";
	############################
	## fetch user preferences ##
	############################
	$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=2 ORDER BY Resource");
	$nrlinks = mysql_num_rows($query);
	if ($nrlinks == 0) {
	echo "<li> - No resources specified </li>\n";
	}
	else {
		while ($row = mysql_fetch_array($query)) {
			$dbname = $row['Resource'];
			$link = $row['link'];
			$title = $row['Description'];
			if ($dbname == 'Ensembl') {
				$linkparts = explode('@@@',$link) ;
				if ($size < 1000000) {
					$link = $linkparts[0];
				}
				else {
					$link = $linkparts[1];
				}
			}
			$link = str_replace('%c',$chrtxt,$link);
			$link = str_replace('%s',$start,$link);
			$link = str_replace('%f',$start,$link);
			$link = str_replace('%e',$stop,$link);
			$link = str_replace('%t',$stop,$link);
			$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
			$link = str_replace('%u',$ucscdb,$link);
			$link = str_replace('%d',$cntype,$link);
			$link = str_replace('%g',$_SESSION['dbname'],$link);

			// specialties
			if ($dbname == 'BeadChip Coverage') {
				$link .= "&ct=$chiptypeid&loh=$loh";
			}
			// skip some /// this code should be updated to be more robust. 
			if (strstr($link,'%D') || strstr($link,'%d')) {continue;} // decipher syndrome/case
			if (strstr($link,'%E')) {continue;} // ecaruca case. 
			echo "<li> - <a class=tt title='$title' href='$link' target=new>$dbname</a></li>\n";
		}
	}
 	echo "</ul>\n";
	if ($loh != 1) {
		#########################
		## CHECK PARENTAL INFO ##
		#########################
		$parq = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
		$parents = mysql_query("$parq");
		$parrow = mysql_fetch_array($parents);
		$father = $parrow['father'];
		$ppid = $parrow['father_project'];
		$mother = $parrow['mother'];
		$mpid = $parrow['mother_project'];

		if ($mother != 0 && $father != 0 && $cn >= 1 && $cn <= 3) {
			echo "<div class=nadruk>Parent Of Origin</div>\n";
			echo "<ul id=ul-simple>\n";
			echo "<li> - <a class=tt href='index.php?page=PoO&cn=$cn&aid=$aid' target='_blank'>Check PoO</li>";
			echo "</ul>\n"; 
		}
	}
	############################
	## ADD PUBLICATION TO CNV ##
	############################
	echo "<div class=nadruk>Associated Literature</div>";
	echo "<ul id=ul-simple>";
	if ($pubmed != '') {
		echo "<li> - <a class=tt href='index.php?page=literature&type=read&aid=$aid&loh=$loh' >Browse associated publications</a></li>"; 
	}
	echo "<li> - <a class=tt href='index.php?page=literature&type=add&aid=$aid&loh=$loh' >Add a publication </a></li>";
	echo "</ul>";
	#################
	## qPCR design ##
	#################
	//echo "<div class=nadruk>qPCR Validation</div>\n";
	//echo "<ul id=ul-simple>\n";
	//echo "  <li> - <a class=tt href='index.php?page=primerdesign&amp;type=search&amp;location=$region' target='_blank'>Search qPCR Primers</a></li>\n";
	//echo "  <li> - <a class=tt href='findprimers2.php?aid=$aid&amp;uid=$uid' target='_blank'>Design qPCR Primers</a></li>\n";
	//echo "</ul>\n";
	if ($fromcontext == 1) {
		echo "</div>";
	}
	//echo "</div>";
	

}

# CUSTOM PROJECTS	
elseif ($type == 'c') {
	echo "<div id=contextpop>\n";
	#########################
	## GET CNV INFORMATION ##
	#########################
	$result = mysql_query("SELECT a.start, a.stop, a.chr, a.pubmed, a.sample FROM cus_aberration a WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$start = $row['start'];
	$stop = $row['stop'];
	$chr = $row['chr'];
	$size = $stop - $start +1;
	$sample = $row['sample'];
	$pubmed = $row['pubmed'];
	$chrtxt = $chromhash[ $chr];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	#######################################
	## TIP ORIGIN DEPENDENT PAGE HEADERS ##
	#######################################
	if ($fromcontext != 1) {
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
		echo "<h3 style='margin-top:5px'>$region</h3></div>\n";
	}
	else {
		echo "<div style='margin-left:7px'>";
	}
	echo "<div class=sectie style='position:relative;border-bottom-style:none;'> ";
	echo "<span style='position:absolute;right:2px;top:1px;'>";
	echo "<a class=tt style='font-style:italic;font-size:0.8em;' href='javascript:void(0)' onclick=\"LoadPreferences('PickDBlinks')\">Set Resources</a>";
	echo "</span>";

	echo "<div class=nadruk>Public Resources: </div>";
	echo "<ul id=ul-simple>\n";
	############################
	## fetch user preferences ##
	############################
	$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=2 ORDER BY Resource");
	$nrlinks = mysql_num_rows($query);
	if ($nrlinks == 0) {
	echo "<li> - No resources specified </li>\n";
	}
	else {
		while ($row = mysql_fetch_array($query)) {
			$dbname = $row['Resource'];
			$link = $row['link'];
			$title = $row['Description'];
			if ($dbname == 'Ensembl') {
				$linkparts = explode('@@@',$link) ;
				if ($size < 1000000) {
					$link = $linkparts[0];
				}
				else {
					$link = $linkparts[1];
				}
			}
			$link = str_replace('%c',$chrtxt,$link);
			$link = str_replace('%s',$start,$link);
			$link = str_replace('%e',$stop,$link);
			$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
			$link = str_replace('%u',$ucscdb,$link);
			if ($dbname == 'PubMed Fetcher') {
				$link .= "&cstm=1";
				$link = str_replace('%a',$aid,$link);
			}
			echo "<li> - <a class=tt title='$title' href='$link' target=new>$dbname</a></li>\n";
		}
	}
	echo "</ul>\n";
	#########################
	## CHECK PARENTAL INFO ##
	#########################
	// not available yet for custom data
	/*
	$parq = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
	$parents = mysql_query("$parq");
	$parrow = mysql_fetch_array($parents);
	$father = $parrow['father'];
	$ppid = $parrow['father_project'];
	$mother = $parrow['mother'];
	$mpid = $parrow['mother_project'];

	if ($mother != 0 && $father != 0 && $cn >= 1 && $cn <= 3) {
		echo "<div class=nadruk>Parent Of Origin</div>\n";
		echo "<ul id=ul-simple>\n";
		echo "<li> - <a class=tt href='index.php?page=PoO&cn=$cn&aid=$aid' target='_blank'>Check PoO</li>";
		echo "</ul>\n"; 
	}
	*/
	############################
	## ADD PUBLICATION TO CNV ##
	############################
	echo "<div class=nadruk>Associated Literature</div>";
	echo "<ul id=ul-simple>";
	if ($pubmed != '') {
		echo "<li> - <a class=tt href='index.php?page=literature&type=read&aid=$aid&cstm=1' >Browse associated publications</a></li>"; 
	}
	echo "<li> - <a class=tt href='index.php?page=literature&type=add&aid=$aid&cstm=1'>Add a publication</a> </li>";
	echo "</ul>";
	#################
	## qPCR design ##
	#################
	echo "<div class=nadruk>qPCR Validation</div>\n";
	echo "<ul id=ul-simple>\n";
	echo "  <li> - <a class=tt href='index.php?page=primerdesign&amp;type=search&amp;location=$region' target='_blank'>Search qPCR Primers</a></li>\n";
	echo "  <li> - <a class=tt href='findprimers2.php?aid=$aid&amp;uid=$uid&cstm=1' target='_blank'>Design qPCR Primers</a></li>\n";
	echo "</ul>\n";
	echo "</div>";
	if ($fromcontext == 1) {
		echo "</div>";
	}

}
else {
	echo "no type found";
}

?>
