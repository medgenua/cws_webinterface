<?php
session_start();
//Tell the browser what kind of file is coming in
header("Content-Type: image/png");
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["X"] = 23;
$chromhash["Y"] = 24;
ob_start();

$font = 'arial';
$maxsize = 248000000 ;// rounded from chromosome 1 
$maxheight = 250; // max height of a chromosome, corresponds to 1
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET POSTED VARS

$pid = $_GET['uid']; # kept original name from generic script. 
$sid = $_GET['sid'];
# get chromosomes to plot
$classpart = '';
if ($_GET['cstm'] == 1) {
	$table = 'cus_aberration';
}
else {
	$table = 'aberration';
}
## GET LOH SETTINGS
$lohshow = $_SESSION['LOHshow'];
$lohsize = $_SESSION['LOHminsize'];
$lohsnp = $_SESSION['LOHminsnp'];
if ($lohshow == 1) {
	if ($_GET['cstm'] == 1) {
		$lohpart = "AND ((NOT cn = '2') OR (cn = '2' AND size >= $lohsize))";
		$plink = '';
	}
	else {
		$lohpart = "AND ((NOT cn = 2) OR (cn = 2 AND size >= $lohsize AND nrsnps >= $lohsnp))";
		$plink = "SELECT DISTINCT(chr) as chr FROM aberration_LOH WHERE (class <> '5' OR class IS NULL) AND idproj = '$pid' AND sample = '$sid' AND size >= '$lohsize' AND nrsnps >= '$lohsnp' ORDER BY chr ASC";
	}	
}
else {
	$lohpart = "AND NOT cn = '2'";
	$plink = '';
}
$query = mysql_query("SELECT DISTINCT(chr) as chr FROM $table WHERE (class <> '5' OR class IS NULL) AND idproj = '$pid' AND sample = '$sid' $lohpart ORDER BY chr ASC");
$toplot = array();
while ($row = mysql_fetch_array($query)) {
	array_push($toplot,$row['chr']); 	
}

if ($plink != '') {
	$query = mysql_query($plink);
	while ($row = mysql_fetch_array($query)) {
		if (!in_array($row['chr'],$toplot)) {
			array_push($toplot,$row['chr']);
		}
	}
}

sort($toplot);
$idx = 0;
$rowheight = 0;
$imgheight = 0;
$rowidx = 0;
$rows = array();
foreach ($toplot as $chr) {
	$subquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
	$subrow = mysql_fetch_array($subquery);
	$chrstop = $subrow['stop'];
	$idx++;
	if ($idx > 12) {
		$idx = 1;
		$imgheight += $rowheight + 20;
		$rowidx++;
		$rows[$rowidx] = $rowheight;
		$rowheight = 0;
	}	
	$chrsize = 250*($chrstop/$maxsize);
	if ($chrsize > $rowheight) {
		$rowheight = $chrsize;
	}
}
$imgheight += $rowheight + 20;
$nrtoplot = count($toplot);

# DEFINE IMAGE PROPERTIES
$height = $imgheight ;

//Specify constant values
#$width = ($plotwidth -1) * 0.80 +5; //Image width in pixels
$width = 35*12 + 5;  // 10 per chr, 6 aan elke kant voor cnvs, 12 spacing tussen elk + marge
$scalef = 250/$maxsize;
//Create the image resource
$image = ImageCreate($width,$height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);
$blue  = imageColorallocate($image,0,0,255);
$darklightblue = ImageColorAllocate($image,122,189,255);
$lightblue = ImageColorAllocate($image,235,235,255);
$green = ImageColorAllocate($image,0,190,0);
$lightgreen = ImageColorAllocate($image,156,255,56);
$purple = ImageColorAllocate($image,136,34,135);
$lightpurple = ImageColorAllocate ($image, 208, 177, 236);
$orange = ImageColorAllocate($image,255, 179,0);
$lightorange = ImageColorAllocate($image,255,210,127);
$lightpink = ImageColorAllocate($image,205,183,181);
$pink = ImageColorAllocate($image,255,182,193);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue, '9' => $pink);
$lightcns = array('0' => $lightred, '1' => $lightred, '2' => $lightorange, '3' => $lightblue, '4' => $lightblue, '9' => $lightpink);

#Fill background
imagefill($image,0,0,$white);

# CREATE SCALE
# CREATE KARYO BANDS
$y = 10;  //start here
$xoff = -18;  // start here
$idx = 0;
$rowidx = 1;
$nexttriang = 0; 
for ($i=1;$i<=24;$i++) {
	if (!in_array($i,$toplot) ) {
		continue;
	}
	$idx++;
	if ($idx > 12) {
		$y = $y + $rows[$rowidx] + 10 ;
		$rowidx++;
		$idx = 1;
		$xoff = 16;
	}
	else {
		$xoff += 34;
	} 
	// get last p
	$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$i' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
	$row = mysql_fetch_array($result);
	$lastp = $row['stop'];

	// first draw chromosome
	$query = mysql_query("SELECT start, stop, name, gieStain FROM cytoBand WHERE chr = '$i' ORDER BY start");
	$arm = 'p';
	while ($row = mysql_fetch_array($query)) { 
		$cstart = $row['start'];
		$cstop = $row['stop'];
		$name = $row['name'];
		$gieStain = $row['gieStain'];
		$scaledstart = intval(round(($cstart)*$scalef));
		$scaledstop = intval(round(($cstop)*$scalef));
		if ($cstop == $lastp) {
			if ($gieStain != 'gneg') {
				imagefilledpolygon($image, array($xoff,$y+$scaledstart,$xoff+5,$y+$scaledstop,$xoff+10,$y+$scaledstart),3, $colors[$gieStain]);
			}
			imagepolygon($image, array($xoff,$y+$scaledstart,$xoff+5,$y+$scaledstop,$xoff+10,$y+$scaledstart),3,$black);
			$nexttriang = 1;
		}
		elseif ($nexttriang == 1) {
				if ($gieStain != 'gneg') {
				imagefilledpolygon($image, array($xoff+5,$y+$scaledstart,$xoff,$y+$scaledstop,$xoff+10,$y+$scaledstop),3,$colors[$gieStain]);
			}
			imagepolygon($image, array($xoff+5,$y+$scaledstart,$xoff,$y+$scaledstop,$xoff+10,$y+$scaledstop),3,$black);
			$nexttriang = 0;
		}
		else{
			if ($gieStain != 'gneg') {
				imagefilledrectangle($image, $xoff,$y+$scaledstart, $xoff+10,$y + $scaledstop, $colors[$gieStain]);
			}
			imagerectangle($image, $xoff,$y+$scaledstart, $xoff+10, $y+$scaledstop, $black);
		}
		

	}
	$fontwidth = imagefontwidth(1);
	$str = "Chr".$chromhash[$i];
	$txtwidth = strlen($str)*$fontwidth;
	$txtx = $xoff+ 5 - ($txtwidth/2);
	imagestring($image,1,$txtx,$y-8,$str,$black);
	// PLOT ABERRATIONS
	
	$query = mysql_query("SELECT start, stop, cn, class FROM $table WHERE (class <> '5' OR class IS NULL) AND sample = '$sid' AND idproj = '$pid' AND chr = '$i' $lohpart ORDER BY start");
	while ($row = mysql_fetch_array($query)) {
		$ccn = $row['cn'];
		if (preg_match("/gain/i",$ccn)) {
			$ccn = 3;
		}
		elseif (preg_match("/los/i",$ccn)) {
			$ccn = 1;
		}
		elseif (preg_match("/loh/i",$ccn)) {
			$ccn = 2;
		}

		$cstart = $row['start'];
                $cstop = $row['stop'];
		$class = $row['class'];
                $scaledstart = intval(round(($cstart)*$scalef));
                $scaledstop = intval(round(($cstop)*$scalef));
		if ($scaledstop - $scaledstart < 1) {
			$scaledstop = $scaledstart + 1;
		}
		if ($ccn <= 2) {
			$xpos = $xoff - 6;
			$ax = $xoff - 12;				
		}
		else {
			$xpos = $xoff + 12;
			$ax = $xoff + 18;
		}
		if ($class == 4) {
			$ccolor = $lightcns[$ccn];
		}
		elseif ($class == 1 || $class == 2) {
			## place red asterix
			$ay = $y + (($scaledstop + $scaledstart) / 2) - 2;
			imagestring($image,1,$ax,$ay,"*",$red);
			$ccolor = $cns[$ccn];
		}
		elseif ($class == 3) {
			## place black asterix 
			$ay = $y + (($scaledstop + $scaledstart) / 2) - 2;
			imagestring($image,1,$ax,$ay,"*",$gpos75);
			$ccolor = $cns[$ccn];

		}	
		else {
			$ccolor = $cns[$ccn];
		}
		imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+4,$y+$scaledstop,$ccolor);
		
	}
	// PLOT LOH ABERRATIONS
	if ($plink != '' ) {
 	  $query = mysql_query("SELECT start, stop, class FROM aberration_LOH WHERE (class <> '5' OR class IS NULL) AND sample = '$sid' AND idproj = '$pid' AND chr = '$i' AND size >= '$lohsize' AND nrsnps >= '$lohsnp' ORDER BY start");
	  while ($row = mysql_fetch_array($query)) {
		$ccn = 2;
		$cstart = $row['start'];
                $cstop = $row['stop'];
		$class = $row['class'];
                $scaledstart = intval(round(($cstart)*$scalef));
                $scaledstop = intval(round(($cstop)*$scalef));
		if ($scaledstop - $scaledstart < 1) {
			$scaledstop = $scaledstart + 1;
		}
		$xpos = $xoff - 6;
		$ax = $xoff - 12;				
		if ($class == 4) {
			$ccolor = $lightcns[$ccn];
		}
		elseif ($class == 1 || $class == 2) {
			## place red asterix
			$ay = $y + (($scaledstop + $scaledstart) / 2) - 2;
			imagestring($image,1,$ax,$ay,"*",$red);
			$ccolor = $cns[$ccn];
		}
		elseif ($class == 3) {
			## place black asterix 
			$ay = $y + (($scaledstop + $scaledstart) / 2) - 2;
			imagestring($image,1,$ax,$ay,"*",$gpos75);
			$ccolor = $cns[$ccn];

		}	
		else {
			$ccolor = $cns[$ccn];
		}
		imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+4,$y+$scaledstop,$ccolor);
	  }	
	}
	

	
}
// PLOT BOX FOR CURRENT REGION


//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 

?>


