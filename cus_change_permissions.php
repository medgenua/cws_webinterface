<?php
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$uid = $_POST['uid'];
// CHANGE PERMISSIONS 
if (isset($_POST['submit'])) {
	$del;
	$add;	
	foreach($_POST as $k => $v) {
		if (strpos($k, "add") !== false && $v != "") {
			$add["$k"] = $v;
		}
		if (strpos($k, "del") !== false && $v != "") {
			$del["$k"] = $v;
		}
	}
	if (count($del) > 0) {
		foreach ($del as $k => $v) {
			$k = substr($k, strpos($k,"_")+1);
			$result = mysql_query("DELETE FROM cus_projectpermission WHERE userid = '$v' AND projectid = '$k'"); 
		}
	}
	if (count($add) > 0) {
		foreach ($add as $k => $v) {
			$k = substr($k, strpos($k,"_")+1);
			$result = mysql_query("INSERT INTO cus_projectpermission (userid, projectid) VALUES ('$v', '$k') ON DUPLICATE KEY UPDATE userid=userid");
		}
	}	
#header("Location: index.php?page=profile&amp;type=projects&amp;result=$result");
echo "<meta http-equiv='refresh' content='0;URL=index.php?page=profile&amp;type=custom&amp;result=$result'>\n";		

}

// DELETE PROJECTS
if (isset($_POST['delete'])) {
	$needsupdate = 0;
	$formstring = "";
	$sidstring = "";
	for ($i=0; $i<count($_POST['deletearray']); $i++){
		$pid = $_POST['deletearray'][$i];
		#echo "pid : $pid<br>";
		# datapoints
		$abs = mysql_query("SELECT id FROM cus_aberration WHERE idproj = '$pid'");
		#while ($row = mysql_fetch_array($abs)) {
		#	$aid = $row['id'];
		#	$del = mysql_query("DELETE FROM datapoints WHERE id = '$aid'");
		#}
		# plots (and delete the files themselve)
		#$files = mysql_query("SELECT filename FROM plots WHERE idproj = '$pid'");
		// you really need filenames from table, because joining and renaming projects can change pid for a file. 
		#while (list($filename) = mysql_fetch_array($files)) {
		#	unlink("Whole_Genome_Plots/$filename");
		#}
		#$del = mysql_query("DELETE FROM plots WHERE idproj = '$pid'");
		#$files = mysql_query("SELECT filename FROM plots WHERE idproj = '$pid'");
		#while (list($filename) = mysql_fetch_array($files)) {
		#	unlink("Whole_Genome_Plots/$filename");
		#}
		#$del = mysql_query("DELETE FROM BAFSEG WHERE idproj = '$pid'");
		# aberrations
		$del = mysql_query("DELETE FROM cus_aberration WHERE idproj = '$pid'");
		#Prioritizations
		#$prs = mysql_query("SELECT id FROM prioritize WHERE project = '$pid'");
		#while ($row = mysql_fetch_array($prs)) {
		#	$prid = $row['id'];
		#	$del = mysql_query("DELETE FROM priorabs WHERE prid = '$prid'");
		#}
		#$del = mysql_query("DELETE FROM prioritize WHERE project = '$pid'");
		#project permissions
		$del = mysql_query("DELETE FROM cus_projectpermission WHERE projectid = '$pid'");
		# project samples &amp; samples
		/*
		$samples = mysql_query("SELECT idsamp FROM projsamp WHERE idproj = '$pid'");
		while ($row = mysql_fetch_array($samples)) {
			$sid = $row['idsamp'];
			$details = mysql_query("SELECT intrack, trackfromproject, chip_dnanr FROM sample WHERE id = '$sid'");
			$detrow = mysql_fetch_array($details);
			$intrack = $detrow['intrack'];
			$tfp = $detrow['trackfromproject'];
			$sample = $detrow['chip_dnanr'];
			if ($intrack == 1 && $tfp == $pid) { 
				$alternatives = mysql_query("SELECT ps.idproj, p.naam, p.created FROM project p JOIN projsamp ps ON ps.idproj = p.id WHERE ps.idsamp = '$sid'  AND  NOT ps.idproj = '$pid' ORDER BY ps.idproj DESC");
				if (mysql_num_rows($alternatives) > 0) {
					$formstring .= "Select an alternative for $sample :<select name=$sid>";
					$sidstring .= "$sid"."_";
					$needsupdate = 1;
					#echo "$sample needs alternative : <br>";
					# delete from projsamp, and update table sample to most recent project by default,  and create form to update table if other project is wanted
					$selected = "selected";
					while ($prow  = mysql_fetch_array($alternatives)) {
						$altid =$prow['idproj'];
						$altname = $prow['naam'];
						$altcreated = $prow['created'];
						#echo " - $altid : $altname<br>";
						if ($selected != "") {
							$update = mysql_query("UPDATE sample SET trackfrompoject = '$altid' WHERE id = '$sid'");
							$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
							$formstring .="<option value='$sid-$altid' $selected>$altname ($altcreated)</option>";
							$selected = '';
						}
						else { 
							$formstring .= "<option value='$sid-$altid'>$altname ($altcreated)</option>";
						}

					}
					$formstring .= "</select><br>\n";
				}
				else {
					//echo "$sample will be deleted (no other projects<br>";
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
					$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
					# delete from projsamp &amp; sample
				}
		
			}
			elseif ($intrack == 1) {
				# delete from projsamp ?
				$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
				next;
				# in track from different project (assume it exists)
				
			}
			else{ 
				# not in track, check if it is included in a different project 
				$checkproj = mysql_query("SELECT idproj FROM projsamp WHERE idsamp = '$sid' AND NOT idproj = '$pid'");
				if (mysql_num_rows($checkproj) > 0) {
					#echo "$sample not in track but in other projects, not deleting completely<br>";
					#only delete from projsamp
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
				}
				else {
					#echo "$sample not in track nor other projects, deleting completely<br>";
					#delete from projsamp &amp; sample
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
					$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
					
				}	
			}
		}
		*/
		
	}
	/*
	if ($formstring != '') {
		$sidstring = substr($sidstring,0,-1);
		echo "<p>Following samples are present in other projects. If you want them to be accessed by default from another project than the one selected, change it here and press submit. Otherwise, you can close this window.</p>";
		echo "<p><form action=cus_change_permissions.php method=POST>\n";
		echo "<input type=hidden name=sids value='$sidstring'>\n";
		echo $formstring;
		echo "<input type=submit class=button name=changedef value=Submit>";
		echo "</form>\n";
	}
	*/
	#finally delete project 
	$delete = mysql_query("DELETE FROM cus_project WHERE id = '$pid'");
	if ($needsupdate == 0) {
		echo "<meta http-equiv='refresh' content='0;URL=index.php?page=profile&amp;type=custom'>\n";
	}

}
/*
if (isset($_POST['changedef'])) {
	$sidstring = $_POST['sids'];
	$sids = explode('_',$sidstring);
	foreach ($sids as $s) {
		$value = $_POST[$s];
		$parts = explode('-',$value);
		$currsid = $parts[0];
		$altpid = $parts[1];
		$update = mysql_query("UPDATE sample SET trackfromproject = '$altpid' WHERE id = '$currsid'");
	}
	#header("Location: index.php?page=profile&amp;type=projects");	 
	echo "<meta http-equiv='refresh' content='0;URL=index.php?page=profile&amp;type=projects'>\n";	
}
*/
// JOIN PROJECTS
if (isset($_POST['join'])) {
	$in = '';
	for ($i=0; $i<count($_POST['joinarray']); $i++){
		$pid = $_POST['joinarray'][$i];
		$in .= $pid . ',';
	}
	$in = substr($in, 0,-1);
	$ok = 1;
	// check overlap in samples
	#$query = mysql_query("SELECT COUNT(idproj) AS aantal FROM projsamp WHERE idproj IN ($in) GROUP BY idsamp ORDER BY aantal DESC LIMIT 1");
	#$row = mysql_fetch_array($query);
	#if ($row['aantal'] > 1) {
	#	echo "<div class=sectie>\n";
	#	echo "<h3>Overlap Detected</h3>\n";
	#	echo "<p>Projects can not be joined when they contain the same samples.</p>\n";
	#	echo "</div>\n";
	#	$ok = 0;
	#}
	#$query = mysql_query("SELECT COUNT(DISTINCT chiptypeid) AS aantal, chiptype, chiptypeid FROM project where id IN ($in)");
	#$row = mysql_fetch_array($query);
	#if ($row['aantal'] > 1) {
	#	echo "<div class=sectie>\n";
	#	echo "<h3>Multiple Chiptypes Detected</h3>\n";
	#	echo "<p>Projects can not be joined when they are run on different chiptypes.</p>\n";
	#	echo "<p>query was = SELECT COUNT(DISTINCT chiptypeid) AS aantal, chiptype FROM project where id IN ($in) <br>";
	#	echo "</div>\n";
	#	$ok = 0;
	#}
	if ($ok == 1) {
		#$chiptype = $row['chiptype'];
		#$chiptypeid = $row['chiptypeid'];
		echo "<div class=sectie>\n";
		echo "<form action=cus_change_permissions.php method=POST>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=pids value='$in'>\n";
		#echo "<input type=hidden name=chiptype value='$chiptype'>\n";
		#echo "<input type=hidden name=chiptypeid value='$chiptypeid'>\n";
		echo "<h3>Set New Project Details</h3>\n";
		echo "<p>Please note that users with access to only a subset of the selected projects will gain access to the complete project !</p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr>\n";
		echo "  <td>Project Name:</td>\n";
		echo "  <td><input type=text size=30 name=pname></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "  <td>Collection:</td>\n";
		echo "  <td <select name=collection><option value=Patient selected>Patients</option><option value=Control>Healthy Controls</option></select></td>\n";
		echo "</tr>\n";
		echo "</table>\n";
		echo "</p>\n";
		echo "<input type=submit class=button value=Submit name=joinsubmit>\n";
	}	
}
if (isset($_POST['joinsubmit'])) {
	$userid = $uid;
	$pids = explode(',',$_POST['pids']);
	$in = '';
	foreach($pids AS $value) {
		$in .= $value . ',';
	}
	$in = substr($in, 0,-1);
	$pname = $_POST['pname'];
	$collection = $_POST['collection'];
	#$chiptype = $_POST['chiptype'];
	#$chiptypeid = $_POST['chiptypeid'];
	$created = date('j/n/Y - G/i/s');
	ob_start();
	echo "Joining Projects: $in<br>";
	ob_flush();
	flush();
	// create new project
	$qs = "INSERT INTO cus_project (naam, created, collection, userID) VALUES ('$pname', '$created','$collection','$userid')";
	$query = mysql_query("$qs");
	$newpid = mysql_insert_id();
	echo "created project with id $newpid<br>";
	//update aberration
	echo "Updating aberrations table <br>\n";
	ob_flush();
	flush();
	mysql_query("UPDATE cus_aberration SET idproj = $newpid WHERE idproj IN ($in)");	
	//update BAFSEG
	#echo "Updating bafseg plots <br>\n";
	#ob_flush();
	#flush();
	#mysql_query("UPDATE BAFSEG SET idproj = $newpid WHERE idproj IN ($in)");
	//update plots
	#echo "Updating plots table<br>\n";
	#ob_flush();
	#flush();
	#mysql_query("UPDATE plots SET idproj = $newpid WHERE idproj IN ($in)");
	//update prioritize
	#echo "Updating prioritize table <br>\n";
	#ob_flush();
	#flush();
	#mysql_query("UPDATE prioritize SET project = $newpid WHERE project IN ($in)");
	// delete from projects
	echo "Delete old projects<br>\n";
	ob_flush();
	flush();
	mysql_query("DELETE FROM cus_project WHERE id IN ($in)");
	// update projectpermissions
	$query = mysql_query("SELECT DISTINCT userid FROM cus_projectpermission WHERE projectid IN ($in)");
	$userline = '';
	while ($row = mysql_fetch_array($query)) {
		$curruser = $row['userid'];
		mysql_query("INSERT INTO cus_projectpermission (userid, projectid) VALUES ('$curruser', '$newpid')");
	}
	mysql_query("DELETE FROM cus_projectpermission WHERE projectid IN ($in)");
	ob_flush();
	flush();
	// update trackfromproject
	#echo "update trackfromprojects<br>\n";
	#mysql_query("UPDATE sample SET trackfromproject = $newpid WHERE trackfromproject IN ($in)");
	// update projsamp
	#echo "update projsamp table<br>";
	#mysql_query("UPDATE projsamp SET idproj = $newpid WHERE idproj IN ($in)");
	// update logs
	#echo "update log table<br/>";
	#mysql_query("UPDATE log SET pid = '$newpid' WHERE pid IN($in)");
	// update deletedcnvs
	#echo "update deleted cnvs table<br/>";
	#mysql_query("UPDATE deletedcnvs SET idproj = '$newpid' WHERE idproj IN ($in)");
	echo "done!<br>";
}

?>

