<?php

# POSTED VARS
$aid = $_POST['aid'];
$class = $_POST['class'];
$uid = $_POST['uid'];
$pid = $_POST['pid'];
$sid = $_POST['sid'];
$logentry = $_POST['logentry'];
$arguments = $_POST['arguments'];

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
if ($arguments != '') {
	$close = 1;
	mysql_query("INSERT INTO cus_log (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry','$arguments')");
	$query = "UPDATE cus_aberration SET class='$class' WHERE id = '$aid'";
	mysql_query($query);
#	$query = "UPDATE scantable SET class='$class' WHERE aid = '$aid'";
#	mysql_query($query);
}
else {
	$query = mysql_query("SELECT sample, idproj, class, chr, start, stop, cn FROM cus_aberration WHERE id = $aid ");
	$row = mysql_fetch_array($query);
	$origclass = $row['class'];
	$sid = $row['sample'];
	$pid = $row['idproj'];
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$cn = $row['cn'];
	$close = 0;
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$logq = mysql_query("SELECT uid, entry FROM cus_log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" . $usrow['FirstName'] .' '.$usrow['LastName'];
			break;
		}
	}
	echo "<h3>Empty explaination is not allowed!</h3>";
	echo "<p>Please argument your decisision to change the diagnostic class of the following CNV:</p>";
	echo "<ul>\n";
	echo "<li>Region: $region</li>";
	echo "<li>CopyNumber: $region</li>";
	echo "<li>Previous Class: $origclass</li>";
	echo "<li>New Class: $class</li>";
	echo "<li>Previously set by: $setby</li>";
	echo "</ul></p>";
	$close = 0;
	$logentry = "Diagnostic Class Changed From $origclass to $class";
	echo "<p><form action=cus_changeclass.php method=POST>";
	echo "<input type=hidden name=logentry value='$logentry'>\n";
	echo "<input type=hidden name=sid value='$sid'>\n";
	echo "<input type=hidden name=pid value='$pid'>\n";
	echo "<input type=hidden name=aid value='$aid'>\n";
	echo "<input type=hidden name=uid value='$uid'>\n";
	echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
	echo "<input type=submit name=submit value='Change Class'>\n";
	echo" </form>\n";
}

if ($close == 1) {
	echo "<script type='text/javascript'>window.opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
?>
