<?php

for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
$ucscdb = str_replace('-','',$_SESSION['dbname']);
mysql_select_db("$db");
$aid = $_GET['aid'];
$fromcontext = $_GET['fc'];
$type = $_GET['type'];
$uid = $_GET['uid'];
// get list of links to show
$uid = $_GET['uid'];
$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$uid'");
$row = mysql_fetch_array($query);
$db_links = $row['db_links'];
if ($type == 'a') {
	echo "<div id=contextpop>\n";
	#########################
	## GET CNV INFORMATION ##
	#########################
	$result = mysql_query("SELECT a.start, a.stop, a.chr, a.pubmed FROM cus_aberration a WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$start = $row['start'];
	$stop = $row['stop'];
	$chr = $row['chr'];
	$pubmed = $row['pubmed'];
	$size = $stop - $start +1;
	$chrtxt = $chromhash[ $chr];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	if ($fromcontext != 1) {
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
		echo "<h3 style='margin-top:5px'>$region</h3></div>\n";
	}
	else {
		echo "<div style='margin-left:7px'>";
	}
	#echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
	#echo "<h3 style='margin-top:5px'>$region</h3></div>\n";
	echo "<div class=sectie style='border-bottom-style:none;'> ";
	echo "<div class=nadruk>Public Resources: <a class=ttsmall href=\"index.php?page=settings&type=plots&st=publ&v=p\" target='_blank'>(change)</a> </div>";
	echo "<ul id=ul-simple>\n";
	############################
	## fetch user preferences ##
	############################
	$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=2 ORDER BY Resource");
	$nrlinks = mysql_num_rows($query);
	if ($nrlinks == 0) {
	echo "<li> - No resources specified </li>\n";
	}
	else {
		while ($row = mysql_fetch_array($query)) {
			$dbname = $row['Resource'];
			$link = $row['link'];
			$title = $row['Description'];
			if ($dbname == 'Ensembl') {
				$linkparts = explode('@@@',$link) ;
				if ($size < 1000000) {
					$link = $linkparts[0];
				}
				else {
					$link = $linkparts[1];
				}
			}
			$link = str_replace('%c',$chrtxt,$link);
			$link = str_replace('%s',$start,$link);
			$link = str_replace('%e',$stop,$link);
			$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
			$link = str_replace('%u',$ucscdb,$link);
			echo "<li> - <a class=tt title='$title' href='$link' target=new>$dbname</a></li>\n";
		}
	}
 	echo "</ul>\n";
	############################
	## ADD PUBLICATION TO CNV ##
	############################
	echo "<div class=nadruk>Associated Literature</div>";
	echo "<ul id=ul-simple>";
	if ($pubmed != '') {
		echo "<li> - <a class=tt href='index.php?page=literature&type=read&cstm=1&aid=$aid' >Browse associated publications</a></li>"; 
	}
	echo "<li> - <a class=tt href='index.php?page=literature&type=add&cstm=1&aid=$aid' >Add a publication</a> </li>";
	echo "</ul>";
	#################
	## qPCR design ##
	#################
	echo "<div class=nadruk>qPCR Validation</div>\n";
	echo "<ul id=ul-simple>\n";
	echo "  <li> - <a class=tt href='index.php?page=primerdesign&amp;type=search&amp;location=$region' target='_blank'>Search qPCR Primers</a></li>\n";
	echo "  <li> - <a class=tt href='findprimers2.php?aid=$aid&amp;uid=$uid' target='_blank'>Design qPCR Primers</a></li>\n";
	echo "</ul>\n";
	if ($fromcontext == 1) {
		echo "</div>";
	}
	echo "</div>";
}
elseif ($type == 'c') {
	echo "<div id=contextpop>\n";
	$chr = $_GET['chr'];
	$start = $_GET['start'];
	$stop = $_GET['stop'];
	$chrtxt = $chromhash[ $chr];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	#######################################
	## TIP ORIGIN DEPENDENT PAGE HEADERS ##
	#######################################
	if ($fromcontext != 1) {
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
		echo "<h3 style='margin-top:5px'>$region</h3></div>\n";
	}
	else {
		echo "<div style='margin-left:7px'>";
	}
	echo "<div class=sectie style='border-bottom-style:none;'> ";
	echo "<div class=nadruk>Public Resources: </div>";
	echo "<ul id=ul-simple>\n";
	############################
	## fetch user preferences ##
	############################
	$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=2 ORDER BY Resource");
	$nrlinks = mysql_num_rows($query);
	if ($nrlinks == 0) {
	echo "<li> - No resources specified </li>\n";
	}
	else {
		while ($row = mysql_fetch_array($query)) {
			$dbname = $row['Resource'];
			$link = $row['link'];
			$title = $row['Description'];
			if ($dbname == 'Ensembl') {
				$linkparts = explode('@@@',$link) ;
				if ($size < 1000000) {
					$link = $linkparts[0];
				}
				else {
					$link = $linkparts[1];
				}
			}
			$link = str_replace('%c',$chrtxt,$link);
			$link = str_replace('%s',$start,$link);
			$link = str_replace('%e',$stop,$link);
			$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
			$link = str_replace('%u',$ucscdb,$link);
			echo "<li> - <a class=tt title='$title' href='$link' target=new>$dbname</a></li>\n";
		}
	}
	echo "</ul>\n";
	############################
	## ADD PUBLICATION TO CNV ##
	############################
	echo "<div class=nadruk>Associated Literature</div>";
	echo "<ul id=ul-simple>";
	if ($pubmed != '') {
		echo "<li> - <a class=tt href='index.php?page=literature&type=read&aid=$aid&cstm=1' >Browse associated publications</a></li>"; 
	}
	echo "<li> - <a class=tt href='index.php?page=literature&type=add&aid=$aid&cstm=1'>Add a publication</a> </li>";
	echo "</ul>";
	#################
	## qPCR design ##
	#################
	echo "<div class=nadruk>qPCR Validation</div>\n";
	echo "<ul id=ul-simple>\n";
	echo "  <li> - <a class=tt href='index.php?page=primerdesign&amp;type=search&amp;location=$region' target='_blank'>Search qPCR Primers</a></li>\n";
	echo "  <li> - <a class=tt href='findprimers2.php?aid=$aid&amp;uid=$uid&cstm=1' target='_blank'>Design qPCR Primers</a></li>\n";
	echo "</ul>\n";
	echo "</div>";
	if ($fromcontext == 1) {
		echo "</div>";
	}


}
else {
	echo "no type found";
}

?>
