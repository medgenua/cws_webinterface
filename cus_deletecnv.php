<?php

# POSTED VARS
$aid = $_GET['aid'];
//$class = $_GET['class'];
$uid = $_GET['u'];
$ftt = $_GET['ftt'];

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inharray = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$query = mysql_query("SELECT cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, confidence, platform, chiptype, sgender, sdescr, nrprobes FROM `cus_aberration` a WHERE a.id = $aid ");
$row = mysql_fetch_array($query);
$class = $row['class'];
$samplename = $row['sample'];
$sid = $samplename;
$pid = $row['idproj'];
$chr = $row['chr'];
$start = $row['start'];
$stop = $row['stop'];
$cn = $row['cn'];
$inh = $row['inheritance'];
$lstart = $row['largestart'];
$lstop = $row['largestop'];
$confidence = $row['confidence'];
$platform = $row['platform'];
$chiptype = $row['chiptype'];
$gender = $row['sgender'];
$sdescr = $row['sdescr'];
$nrprobes = $row['nrprobes'];
$close = 1;


# DELETE !
if (isset($_GET['delete']) && $_GET['arguments'] != '') {
	$arguments = $_GET['arguments'];
	mysql_query("INSERT INTO `cus_deletedcnvs` (id, cn, start, stop, chr, sample, idproj, inheritance, class,largestart, largestop, confidence, platform, chiptype, sgender, sdescr,nrprobes) VALUES ('$aid','$cn','$start','$stop','$chr','$sid','$pid','$inh','$class','$lstart','$lstop','$confidence','$platform','$chiptype','$gender','$sdescr','$nrprobes')");
	$logentry = "Deleted";
	mysql_query("INSERT INTO `cus_log` (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry', '$arguments')");
	$query = mysql_query("DELETE FROM `cus_aberration` WHERE id = '$aid'");
	$close = 1;
}
else {
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');

	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM cus_log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" .$usrow['FirstName'] .' '.$usrow['LastName'] . ")";
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	echo "<h3>Deleting a CNV Needs Confirmation</h3>";
	echo "<p>You are about to delete the following region. To do so, specify a clear reason and press 'Delete'</p>\n";
	echo "<p>";
	echo "<ul>\n";
	echo "<li>Region: $region</li>";
	echo "<li>CopyNumber: $cn</li>";
	echo "<li>Sample: $samplename</li>";
	echo "<li>Diagnostic Class: $class $setby</li>";
	echo "<li>Inheritance: ".$inharray[$inharray[$inh]]."</li>";
	echo "</ul></p>";


	echo "<p><form action='cus_deletecnv.php' method=GET>";
//	echo "<input type=hidden name=sid value='$sid'>\n";
//	echo "<input type=hidden name=pid value='$pid'>\n";
	echo "<input type=hidden name=aid value='$aid'>\n";
	echo "<input type=hidden name=u value='$uid'>\n";
//	echo "<input type=hidden name=class value='$class'>\n";
	echo "Reason: <br/><input type=text value='' name=arguments maxsize=255 size=35></p>";
	echo "<input type=submit name=delete value='Delete'>\n";
	echo" </form>\n";
}

if ($close == 1) {
	echo "<script type='text/javascript'>";
	echo "var parenturl = window.opener.location.href;\n";
	echo "if (parenturl.indexOf('details') > -1 ){\n";
	echo "	 window.opener.location = parenturl + '&sample=$sid&project=$pid';\n";
	echo "}\n";
	echo "else {\n";
	echo "	 window.opener.location.reload();\n";
	echo "}\n";
	echo "</script>";
	//echo "<script type='text/javascript'>window.opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
//echo "done.<br/>Refresh page to see update.<br/>";
//echo "<a href='javascript:window.close()'>Close</a>";
//echo "<center>";
?>
