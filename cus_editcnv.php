<?php
# copy get variables around if needed
# GETED VARS
$aid = $_GET['aid'];
//$class = $_GET['class'];
$uid = $_GET['u'];
$ftt = $_GET['ftt'];

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inharray = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
// get details on cnv
$query = mysql_query("SELECT a.sample, a.idproj, a.class, a.chr, a.start, a.stop,  a.cn, a.inheritance FROM `cus_aberration` a WHERE a.id = $aid ");
$row = mysql_fetch_array($query);
$class = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$chrtxt = $chromhash[$chr];
$start = $row['start'];
$stop = $row['stop'];
$cn = $row['cn'];
$inh = $row['inheritance'];
$samplename = $row['sample'];
$close = 1;
$region = "Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',');

$firstcell =  "style=\"border-left: 1px solid black;\"";

## SET HTML HEADERS 
?>
<html>
<head><title>EDIT CNV DETAILS: <?php echo $region; ?></title>
<style type="text/css">
	td {
	font-size:12;
	border-right: 1px solid black;
	border-bottom: 1px solid black;
	text-align: left;
	padding: 6px 6px 6px 12px;
	padding: 3px 3px 3px 12px; 
}
td.grey {
	font-size:12;
	border-right: 1px solid black;
	border-top: 1px solid black;
	text-align: left;
	padding: 6px 6px 6px 12px;
	background:#eeeeee;
	padding: 3px 3px 3px 12px; 
	
}

</style>
<script type=text/javascript>
  	function clearfield(fieldid) {
		document.getElementById(fieldid).value = "";
	}
</script>
</head>
<body>
<?php
if (isset($_GET['cancel'])) {
	$close = 1;
}
# UPDATE DATA !
elseif (isset($_GET['save']) && $_GET['arguments'] != '') {
	$arguments = $_GET['arguments'];
	# Get new values
	$query = "";
	$regionlog = 0;
	// start was adapted
	if ($_GET['start'] != $start) {
		$newstart = $_GET['start'];
		$query .= " start = '$newstart',";
		$regionlog = 1;
	}
	else {
		$newstart = $start;
	}
	
	// stop was adapted
	if ($_GET['stop'] != $stop) {
		$newstop = $_GET['stop'];
		$query .= " stop = '$newstop',";
		$regionlog = 1;
		
	}
	else {
		$newstop = $stop;
	}
	
	if ($regionlog == 1) {
		$entry = "Region Adapted From $region";
		mysql_query("INSERT INTO `cus_log` (sid, aid, pid, uid, entry, arguments) VALUES ('$sid', '$aid', '$pid', '$uid', '$entry', '$arguments')");
	}
	// CopyNumber was adapted
	if ($_GET['copynumber'] != $cn) {
		$newcn = $_GET['copynumber'];
		$query .= " cn = '$newcn',";
		$entry = "CopyNumber Changed From $cn to $newcn";
		
		mysql_query("INSERT INTO `cus_log` (sid, aid, pid, uid, entry, arguments) VALUES ('$sid', '$aid', '$pid', '$uid', '$entry', '$arguments')");
	}
	# update aberration table
	if ($query != '') {
		$query = substr($query,0,-1);
		mysql_query("UPDATE `cus_aberration` SET $query WHERE id = '$aid'");
	}
	# set close-variable 
	$close = 1;
}
else {
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM `cus_log` WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" .$usrow['FirstName'] .' '.$usrow['LastName'] . ")";
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	echo "<h3>Edit CNV Details : $region</h3>";
	echo "<p>You are editing CNV details. Please specify a clear reason for this !</p>\n";
	echo "<p>";
	

	echo "<p><form action='cus_editcnv.php' method=GET>";
	echo "<table cellspacing=0>";
	echo "<tr><td class=grey $firstcell>Item</td><td class=grey>New Value</td><td class=grey>Remark</td></tr>";
	# region
	echo "<tr><td $firstcell>Chromosome</td><td>$chrtxt</td><td>Chr can't be changed</td></tr>";
	echo "<tr><td $firstcell>Start</td><td><input name=start type=text size=15 value='$start' onchange=\"clearfield('lstart')\"></td><td>Only Plain numbers</td></tr>";
	echo "<tr><td $firstcell>Stop</td><td><input name=stop type=text size=15 value='$stop' onchange=\"clearfield('lstop')\"></td><td>Only Plain numbers</td></tr>";
	# CopyNumber
	echo "<tr><td $firstcell>CopyNumber</td><td><input type=text size=15 value='$cn' name=copynumber ></td><td>Integer, string or logR value</td></tr>";
		
	echo "</table>";
	echo "<input type=hidden name=aid value='$aid'>\n";
	echo "<input type=hidden name=u value='$uid'>\n";
	echo "Reason: <br/><input type=text value='' name=arguments maxsize=255 size=35></p>";
	echo "<input type=submit name=save value='Save'> &nbsp; &nbsp; <input type=submit name=cancel value='Cancel'>\n";
	echo" </form>\n";
}

## CLOSE POPUP & GO BACK TO CALLING PAGE
if ($close == 1) {
	$_SESSION['setfromeditcnv'] = 1;
	$_SESSION['sample'] = $sid;
	$_SESSION['project'] = $pid;
	//echo "<script type='text/javascript'>opener.location.reload()</script>";
	//echo "<script type='text/javascript'>window.close()</script>";
	echo "<script type='text/javascript'>";
	echo "var parenturl = window.opener.location.href;\n";
	echo "if (parenturl.indexOf('details') > -1 ){\n";
	echo "	 window.opener.location = parenturl + '&sample=$sid&project=$pid';\n";
	echo "}\n";
	echo "else {\n";
	echo "	 window.opener.location.reload();\n";
	echo "}\n";
	echo "</script>";
	//echo "<script type='text/javascript'>window.opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";

}
?>

</body>
</html>
