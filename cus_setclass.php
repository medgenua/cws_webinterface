<?php

# POSTED VARS
$aid = $_GET['aid'];
$class = $_GET['class'];
$uid = $_GET['u'];
$ftt = $_GET['ftt'];

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$query = mysql_query("SELECT sample, idproj, class, chr, start, stop, cn FROM cus_aberration WHERE id = $aid ");
$row = mysql_fetch_array($query);
$origclass = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$start = $row['start'];
$stop = $row['stop'];
$cn = $row['cn'];
$close = 1;

if ($class == $origclass && $class != 'null') {
	
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM cus_log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	
	if ($setbyuid != $uid) {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Updating 'Set By User'</h3>";
		echo "<p>The diagnostic class of the following CNV was previously set by a diffent user, if you are validating this region, pleas use 'Validated' as the reason below:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>Previous Class: $origclass</li>";
		echo "<li>New Class: $class</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Confirmed As Class $class";
		echo "<p><form action=cus_changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "Reason: <br/><input type=text value='Validated' name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Update'>\n";
		echo" </form>\n";
	}
	else {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Confirming Diagnostic Class</h3>";
		echo "<p>The diagnostic class of the following CNV was previously set by you, if you are validating this region, pleas use 'Validated' as the reason below:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>Previous Class: $origclass</li>";
		echo "<li>New Class: $class</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Confirmed As Class $class";
		echo "<p><form action=cus_changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "Reason: <br/><input type=text value='Validated' name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Update'>\n";
		echo" </form>\n";
	}
}

if ($class != $origclass && $class != 'null' ) {
	if ($origclass == '') {
		$logentry = "Diagnostic Class Set to $class";
		mysql_query("INSERT INTO cus_log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry')");
	}
	else {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		$logq = mysql_query("SELECT uid, entry FROM cus_log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
				break;
			}
		}
		echo "<h3>Changing Diagnostic class needs Explaination!</h3>";
		echo "<p>Please argument your decisision to change the diagnostic class of the following CNV:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>Previous Class: $origclass</li>";
		echo "<li>New Class: $class</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Changed From $origclass to $class";
		echo "<p><form action=cus_changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Class'>\n";
		echo" </form>\n";
	}
}
elseif ($origclass > 0 && $class == 'null') {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		$logq = mysql_query("SELECT uid, entry FROM cus_log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.$usrow['LastName'];
				break;
			}
		}
		echo "<h3>Changing Diagnostic class needs Explaination!</h3>";
		echo "<p>Please argument your decisision to change the diagnostic class of the following CNV:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $region</li>";
		echo "<li>Previous Class: $origclass</li>";
		echo "<li>New Class: Undefined</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Changed From $origclass to Undefined";
		echo "<p><form action=cus_changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";

		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Class'>\n";
		echo" </form>\n";

	
	
	//mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
}

if ($close == 1) {
	$query = "UPDATE cus_aberration SET class='$class' WHERE id = '$aid'";
	mysql_query($query);
	#$query = "UPDATE scantable SET class='$class' WHERE aid = '$aid'";
	#mysql_query($query);

	echo "<script type='text/javascript'>window.close()</script>";
}
//echo "done.<br/>Refresh page to see update.<br/>";
//echo "<a href='javascript:window.close()'>Close</a>";
//echo "<center>";
?>
