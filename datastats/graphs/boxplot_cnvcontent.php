<?php
// include the graphing class 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_stock.php');
require_once ('../jpgraph/src/jpgraph_log.php');
// connect to the database
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];
// function 
function mypercentile($data,$percentile){ 
    if( 0 < $percentile && $percentile < 1 ) { 
        $p = $percentile; 
    }else if( 1 < $percentile && $percentile <= 100 ) { 
        $p = $percentile * .01; 
    }else { 
        return ""; 
    } 
    $count = count($data); 
    $allindex = ($count-1)*$p; 
    $intvalindex = intval($allindex); 
    $floatval = $allindex - $intvalindex; 
    sort($data); 
    if(!is_float($floatval)){ 
        $result = $data[$intvalindex]; 
    }else { 
        if($count > $intvalindex+1) 
            $result = $floatval*($data[$intvalindex+1] - $data[$intvalindex]) + $data[$intvalindex]; 
        else 
            $result = $data[$intvalindex]; 
    } 
    return $result; 
} 

// get chiptype
$chip = $_GET['ct'];
$query = mysql_query("Select name FROM chiptypes WHERE ID = '$chip'");
$row = mysql_fetch_array($query);
$chiptype = $row['name'];
// size of cnvs per sample controles
$query = mysql_query("SELECT SUM(size) as size,a.sample, a.idproj FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id  AND a.sample = s.id WHERE pp.userid = $userid AND p.collection IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND a.cn <> 2 GROUP BY a.sample, a.idproj ");
$csizes = array();
while ($row = mysql_fetch_array($query) ) {
	$sid = $row['sample'];
	$pid = $row['idproj'];
	#$totalsize = $row['size'];
	// check mosaicism
	$sq = mysql_query("SELECT idsamp FROM BAFSEG WHERE idsamp = '$sid' AND idproj = '$pid'");
	if (mysql_num_rows($sq) > 0) {
		## mosaicism suspected, exclude from summary !
		continue;
	}
	$csizes[] = $row['size'];

}
if (count($csizes) == 0) {
	$cnodata =1;
}
$cmin = round(mypercentile($csizes,5)/1000);
$cmax = round(mypercentile($csizes,95)/1000);
$cmed = round(mypercentile($csizes,50)/1000);
$copen = round(mypercentile($csizes,25)/1000);
$cclose = round(mypercentile($csizes,75)/1000);

// nr cnvs per sample patients
$query = mysql_query("SELECT SUM(a.size) as size FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id AND a.sample = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND s.trackfromproject = a.idproj and a.cn <> 2 GROUP BY a.sample, a.idproj ");
$psizes = array();
while ($row = mysql_fetch_array($query)) {
	$psizes[] = $row['size'];
}
if (count($psizes) == 0) {
	$pnodata =1;
}
$pmin = round(mypercentile($psizes,5)/1000);
$pmax = round(mypercentile($psizes,95)/1000);
$pmed = round(mypercentile($psizes,50)/1000);
$popen = round(mypercentile($psizes,25)/1000);
$pclose = round(mypercentile($psizes,75)/1000);



$labels = array("","Controls","Patients","");
// Create the graph. These two calls are always required
$graph = new Graph(325,325);    
$graph->SetScale("textlin");
$graph->xaxis->SetTickLabels($labels);
$graph->xaxis->SetLabelAngle(50);
$graph->xaxis->SetLabelMargin(5);
$graph->SetShadow();
$graph->img->SetMargin(100,30,30,50);

// add text if no data was found.
if ($cnodata == 1) {
	$txt = new Text('No Controls Available');
	// ( (0,0) is the upper left corner )
	$txt->SetPos(0.5, 0.45,'center','center');
	$txt->SetColor('red');
	$txt->SetFont(FF_FONT2,FS_BOLD); 
	$txt->SetBox('yellow','navy','gray');
	$graph->AddText($txt); 
}
if ($pnodata == 1) {
	$txt = new Text('No Patients Available');
	// ( (0,0) is the upper left corner )
	$txt->SetPos(0.5, 0.45,'center','center');
	$txt->SetColor('red');
	$txt->SetFont(FF_FONT2,FS_BOLD); 
	$txt->SetBox('yellow','navy','gray');
	$graph->AddText($txt); 
}

#else {
#	$txt = new Text("min: $min ; max: $max\nbinsize: $binsize");
#	// ( (0,0) is the upper left corner )
#	$txt->SetPos(0.5, 0.45,'center','center');
#	$txt->SetColor('red');
#	$txt->SetFont(FF_FONT2,FS_BOLD); 
#	$txt->SetBox('yellow','navy','gray');
#	$graph->AddText($txt); 
#}
// Create the bar plots
$datay = array(0.2,0.3,0.1,0.4,0.25,$copen,$cclose,$cmin,$cmax,$cmed,$popen,$pclose,$pmin,$pmax,$pmed,0.2,0.3,0.1,0.4,0.25);
$b1plot = new BoxPlot($datay,$datax);
$b1plot->SetWidth(9);
$graph->Add($b1plot);
$graph->title->Set("Total CNV Content Per Sample");
$graph->subtitle->Set("Chiptype : $chiptype");
$graph->yaxis->title->Set("Total CNV size (kb)");
#if ($max < 100) {
#	$graph->xaxis->title->SetMargin(18);
#	$graph->legend->SetPos(0.32,0.99,'center','bottom');
#}
#else {
#	$graph->xaxis->title->SetMargin(25);
#	$graph->legend->SetPos(0.32,0.99,'center','bottom');
#}
#$graph->yaxis->title->Set("Nr. of Samples");
$graph->yaxis->title->SetMargin(50);
 
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
#// legend layout
// Display the graph
$graph->Stroke();
?>
