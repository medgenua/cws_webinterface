<?php
// include the graphing class 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_bar.php');
require_once ('../jpgraph/src/jpgraph_log.php');
// connect to the database
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];
// function 
function mypercentile($data,$percentile){ 
    if( 0 < $percentile && $percentile < 1 ) { 
        $p = $percentile; 
    }else if( 1 < $percentile && $percentile <= 100 ) { 
        $p = $percentile * .01; 
    }else { 
        return ""; 
    } 
    $count = count($data); 
    $allindex = ($count-1)*$p; 
    $intvalindex = intval($allindex); 
    $floatval = $allindex - $intvalindex; 
    sort($data); 
    if(!is_float($floatval)){ 
        $result = $data[$intvalindex]; 
    }else { 
        if($count > $intvalindex+1) 
            $result = $floatval*($data[$intvalindex+1] - $data[$intvalindex]) + $data[$intvalindex]; 
        else 
            $result = $data[$intvalindex]; 
    } 
    return $result; 
} 

// get chiptype
$chip = $_GET['ct'];
$query = mysql_query("Select name FROM chiptypes WHERE ID = '$chip'");
$row = mysql_fetch_array($query);
$chiptype = $row['name'];
// nr cnvs per sample controles
$query = mysql_query("SELECT COUNT(a.id) as nr, a.sample, a.idproj FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id  AND a.sample = s.id WHERE pp.userid = $userid AND p.collection IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 GROUP BY a.sample, a.idproj ");
$cnrs = array();
while ($row = mysql_fetch_array($query) ) {
	$sid = $row['sample'];
	$pid = $row['idproj'];
	// check mosaicism
	$sq = mysql_query("SELECT idsamp FROM BAFSEG WHERE idsamp = '$sid' AND idproj = '$pid'");
	if (mysql_num_rows($sq) > 0) {
		## mosaicism suspected, exclude from summary !
		continue;
	}
	$cnrs[] = $row['nr'];

}
// nr cnvs per sample patients
$query = mysql_query("SELECT COUNT(a.id) as nr FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id AND a.sample = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND s.trackfromproject = a.idproj GROUP BY a.sample, a.idproj ");
$pnrs = array();
while ($row = mysql_fetch_array($query)) {
	$pnrs[] = $row['nr'];
}

// get bin sizes => from smalles to 95% percentile.
$allnrs = array_merge($cnrs,$pnrs);
if (count($allnrs) == 0) {
	$min = 0;
	$max = 11;
	$nodata = 1;
}
else {
	$min = min($allnrs);
	$max = round(mypercentile($allnrs,95));
	#$max = max($allnrs);
}
$binsize = round(($max - $min + 1)/10);
if ($min == 0) {
	$min = 1;
}
//initiate bins
$cbins = array();
$pbins = array();
$lastbin = 0;
for ($i = ($min - 1); $i < $max; $i = $i + $binsize) {
	$cbins[$i] = 0;
	$pbins[$i] = 0;
	$lastbin = $i;
}
$cbins[$lastbin + $binsize];
$pbins[$lastbin + $binsize];
ksort($cbins);
ksort($pbins);
// fill bins controls
foreach ($cnrs as $key => $nr) {
	$prevbin = 0;
	foreach($cbins as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$cbins[$prevbin] = $cbins[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$cbins[$lastbin+$binsize] = $cbins[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
// fill bins patients
foreach ($pnrs as $key => $nr) {
	$prevbin = 0;
	foreach($pbins as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$pbins[$prevbin] = $pbins[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$pbins[$lastbin+$binsize] = $pbins[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
// scale the bins to percentage.
$csum = array_sum($cbins);
foreach ($cbins as $key => $value) {
	$cbins[$key] = ($value/$csum)*100;
}
$psum = array_sum($pbins);
foreach ($pbins as $key => $value) {
	$pbins[$key] = ($value/$psum)*100;
}

// get labels 
$labels = array();
foreach($cbins as $label => $value) {
	if ($label != ($lastbin + $binsize)) {
		$labels[] = "$label - ".($label+$binsize-1);
	}
	else {
		$labels[] = ">".($lastbin + $binsize - 1); # label = max + 1 => need : label - 1 + binsize - 1 = $max + $binsize - 1
	}
}
// fill datarows 
$data1 = array();
foreach($cbins as $bin => $value) {
	$data1[] = $value;
}
$data2 = array();
foreach($pbins as $bin => $value) {
	$data2[] = $value;
}
// Create the graph. These two calls are always required
$graph = new Graph(325,325);    
$graph->SetScale("textint");
$graph->xaxis->SetTickLabels($labels);
$graph->xaxis->SetLabelAngle(50);
$graph->xaxis->SetLabelMargin(5);
$graph->SetShadow();
$graph->img->SetMargin(45,30,30,50);

// add text if no data was found.
if ($nodata == 1) {
	$txt = new Text('No Data Available');
	// ( (0,0) is the upper left corner )
	$txt->SetPos(0.5, 0.45,'center','center');
	$txt->SetColor('red');
	$txt->SetFont(FF_FONT2,FS_BOLD); 
	$txt->SetBox('yellow','navy','gray');
	$graph->AddText($txt); 
}
#else {
#	$txt = new Text("min: $min ; max: $max\nbinsize: $binsize");
#	// ( (0,0) is the upper left corner )
#	$txt->SetPos(0.5, 0.45,'center','center');
#	$txt->SetColor('red');
#	$txt->SetFont(FF_FONT2,FS_BOLD); 
#	$txt->SetBox('yellow','navy','gray');
#	$graph->AddText($txt); 
#}
// Create the bar plots
$b1plot = new BarPlot($data1);
$b1plot->SetFillColor("orange");
$b1plot->SetLegend('Control/Parents');
$b2plot = new BarPlot($data2);
$b2plot->SetFillColor("blue");
$b2plot->SetLegend('Patients');
 
// Create the grouped bar plot
$gbplot = new GroupBarPlot(array($b1plot,$b2plot));
 
// ...and add it to the graPH
$graph->Add($gbplot);

$graph->title->Set("Nr. Of CNVs Per Sample");
$graph->subtitle->Set("Chiptype : $chiptype");
$graph->xaxis->title->Set("Nr. Of CNVs");
if ($max < 100) {
	$graph->xaxis->title->SetMargin(18);
	$graph->legend->SetPos(0.32,0.99,'center','bottom');
}
else {
	$graph->xaxis->title->SetMargin(25);
	$graph->legend->SetPos(0.32,0.99,'center','bottom');
}
$graph->yaxis->title->Set("Nr. of Samples");
$graph->yaxis->title->SetMargin(5);
 
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
// legend layout
$graph->legend->SetShadow('gray@0.4',5); 
// Display the graph
$graph->Stroke();
?>
