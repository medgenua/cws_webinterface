<?php
// include the graphing class 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_bar.php');

// connect to the database
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];

// function 
function mypercentile($data,$percentile){ 
    if( 0 < $percentile && $percentile < 1 ) { 
        $p = $percentile; 
    }else if( 1 < $percentile && $percentile <= 100 ) { 
        $p = $percentile * .01; 
    }else { 
        return ""; 
    } 
    $count = count($data); 
    $allindex = ($count-1)*$p; 
    $intvalindex = intval($allindex); 
    $floatval = $allindex - $intvalindex; 
    sort($data); 
    if(!is_float($floatval)){ 
        $result = $data[$intvalindex]; 
    }else { 
        if($count > $intvalindex+1) 
            $result = $floatval*($data[$intvalindex+1] - $data[$intvalindex]) + $data[$intvalindex]; 
        else 
            $result = $data[$intvalindex]; 
    } 
    return $result; 
} 

// get chiptype
$chip = $_GET['ct'];
$query = mysql_query("Select name FROM chiptypes WHERE ID = '$chip'");
$row = mysql_fetch_array($query);
$chiptype = $row['name'];

############################
## GET DATA FROM DATABASE ##
############################

// size of cnvs in controles : hom.del
$query = mysql_query("SELECT size, a.sample, a.idproj FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id  AND a.sample = s.id WHERE pp.userid = $userid AND p.collection IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND a.cn = 0");
$czero = array();
while ($row = mysql_fetch_array($query) ) {
	$sid = $row['sample'];
	$pid = $row['idproj'];
	// check mosaicism
	$sq = mysql_query("SELECT idsamp FROM BAFSEG WHERE idsamp = '$sid' AND idproj = '$pid'");
	if (mysql_num_rows($sq) > 0) {
		## mosaicism suspected, exclude from summary !
		continue;
	}
	$czero[] = $row['size'];

}
// size of cnvs in controles : het.del
$query = mysql_query("SELECT size, a.sample, a.idproj FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id  AND a.sample = s.id WHERE pp.userid = $userid AND p.collection IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND a.cn = 1");
$cone = array();
while ($row = mysql_fetch_array($query) ) {
	$sid = $row['sample'];
	$pid = $row['idproj'];
	// check mosaicism
	$sq = mysql_query("SELECT idsamp FROM BAFSEG WHERE idsamp = '$sid' AND idproj = '$pid'");
	if (mysql_num_rows($sq) > 0) {
		## mosaicism suspected, exclude from summary !
		continue;
	}
	$cone[] = $row['size'];

}
// size of cnvs in controles : dup
$query = mysql_query("SELECT size, a.sample, a.idproj FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id  AND a.sample = s.id WHERE pp.userid = $userid AND p.collection IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND a.cn = 3");
$cthree = array();
while ($row = mysql_fetch_array($query) ) {
	$sid = $row['sample'];
	$pid = $row['idproj'];
	// check mosaicism
	$sq = mysql_query("SELECT idsamp FROM BAFSEG WHERE idsamp = '$sid' AND idproj = '$pid'");
	if (mysql_num_rows($sq) > 0) {
		## mosaicism suspected, exclude from summary !
		continue;
	}
	$cthree[] = $row['size'];
}
// size of cnvs in controles : amp 
$query = mysql_query("SELECT size, a.sample, a.idproj FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id  AND a.sample = s.id WHERE pp.userid = $userid AND p.collection IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND a.cn = 4");
$cfour = array();
while ($row = mysql_fetch_array($query) ) {
	$sid = $row['sample'];
	$pid = $row['idproj'];
	// check mosaicism
	$sq = mysql_query("SELECT idsamp FROM BAFSEG WHERE idsamp = '$sid' AND idproj = '$pid'");
	if (mysql_num_rows($sq) > 0) {
		## mosaicism suspected, exclude from summary !
		continue;
	}
	$cfour[] = $row['size'];
}

//size of cnvs in patients : hom del 
$query = mysql_query("SELECT size FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id AND a.sample = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND s.trackfromproject = a.idproj AND cn = 0");
$pzero = array();
while ($row = mysql_fetch_array($query)) {
	$pzero[] = $row['size'];
}
//size of cnvs in patients : het del 
$query = mysql_query("SELECT size FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id AND a.sample = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND s.trackfromproject = a.idproj AND cn = 1");
$pone = array();
while ($row = mysql_fetch_array($query)) {
	$pone[] = $row['size'];
}
//size of cnvs in patients : dup 
$query = mysql_query("SELECT size FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id AND a.sample = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND s.trackfromproject = a.idproj AND cn = 3");
$pthree = array();
while ($row = mysql_fetch_array($query)) {
	$pthree[] = $row['size'];
}
//size of cnvs in patients : amp 
$query = mysql_query("SELECT size FROM aberration a JOIN project p JOIN projectpermission pp JOIN sample s ON a.idproj = pp.projectid AND a.idproj = p.id AND a.sample = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls', 'Parents') AND p.chiptypeid = '$chip' AND s.intrack = 1 AND s.trackfromproject = a.idproj AND cn = 4");
$pfour = array();
while ($row = mysql_fetch_array($query)) {
	$pfour[] = $row['size'];
}

// get bin sizes => from smalles to 95% percentile.
$allnrs = array_merge($czero,$cone,$cthree,$cfour,$pzero,$pone,$pthree,$pfour);
if (count($allnrs) == 0) {
	$min = 0;
	$max = 11;
	$nodata = 1;
}
else {
	$min = min($allnrs);
	$max = round(mypercentile($allnrs,95));
	#$max = max($allnrs);
}
$binsize = round(($max - $min + 1)/10);
if ($min == 0) {
	$min = 1;
}
//initiate bins
$cbinzero = array();
$cbinone = array();
$cbinthree = array();
$cbinfour = array();
$pbinzero = array();
$pbinone = array();
$pbinthree = array();
$pbinfour = array();
$lastbin = 0;
for ($i = ($min - 1); $i < $max; $i = $i + $binsize) {
	$cbinzero[$i] = 0;
	$cbinone[$i] = 0;
	$cbinthree[$i] = 0;
	$cbinfour[$i] = 0;
	$pbinzero[$i] = 0;
	$pbinone[$i] = 0;
	$pbinthree[$i] = 0;
	$pbinfour[$i] = 0;
	$lastbin = $i;
}
$cbinzero[$lastbin + $binsize] = 0;
$cbinone[$lastbin + $binsize] = 0;
$cbinthree[$lastbin + $binsize] = 0;
$cbinfour[$lastbin + $binsize] = 0;
$pbinzero[$lastbin + $binsize] = 0;
$pbinone[$lastbin + $binsize] = 0;
$pbinthree[$lastbin + $binsize] = 0;
$pbinfour[$lastbin + $binsize] = 0;

#ksort($cbins);
#ksort($pbins);

// fill bins controls
foreach ($czero as $key => $nr) {
	$prevbin = 0;
	foreach($cbinzero as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$cbinzero[$prevbin] = $cbinzero[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$cbinzero[$lastbin+$binsize] = $cbinzero[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
foreach ($cone as $key => $nr) {
	$prevbin = 0;
	foreach($cbinone as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$cbinone[$prevbin] = $cbinone[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$cbinone[$lastbin+$binsize] = $cbinone[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
foreach ($cthree as $key => $nr) {
	$prevbin = 0;
	foreach($cbinthree as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$cbinthree[$prevbin] = $cbinthree[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$cbinthree[$lastbin+$binsize] = $cbinthree[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
foreach ($cfour as $key => $nr) {
	$prevbin = 0;
	foreach($cbinfour as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$cbinfour[$prevbin] = $cbinfour[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$cbinfour[$lastbin+$binsize] = $cbinfour[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}

// fill bins patients
foreach ($pzero as $key => $nr) {
	$prevbin = 0;
	foreach($pbinzero as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$pbinzero[$prevbin] = $pbinzero[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$pbinzero[$lastbin+$binsize] = $pbinzero[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
foreach ($pone as $key => $nr) {
	$prevbin = 0;
	foreach($pbinone as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$pbinone[$prevbin] = $pbinone[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$pbinone[$lastbin+$binsize] = $pbinone[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
foreach ($pthree as $key => $nr) {
	$prevbin = 0;
	foreach($pbinthree as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$pbinthree[$prevbin] = $pbinthree[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$pbinthree[$lastbin+$binsize] = $pbinthree[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}
foreach ($pfour as $key => $nr) {
	$prevbin = 0;
	foreach($pbinfour as $bin => $value) {
		if ($nr <= $max && $nr < $bin) {
			$pbinfour[$prevbin] = $pbinfour[$prevbin] + 1;
			break;
		}
		elseif ($nr > $max) {
			$pbinfour[$lastbin+$binsize] = $pbinfour[$lastbin+$binsize] + 1;
			break;
		}
		$prevbin = $bin;
	}
}

// scale the bins to percentage.
$csum = array_sum(array_merge($cbinzero,$cbinone,$cbinthree,$cbinfour));
$psum = array_sum(array_merge($pbinzero,$pbinone,$pbinthree,$pbinfour));
foreach ($cbinzero as $key => $value) {
	$cbinzero[$key] = ($cbinzero[$key]/$csum)*100;
	$cbinone[$key] = ($cbinone[$key]/$csum)*100;
	$cbinthree[$key] = ($cbinthree[$key]/$csum)*100;
	$cbinfour[$key] = ($cbinfour[$key]/$csum)*100;
	$pbinzero[$key] = ($pbinzero[$key]/$psum)*100;
	$pbinone[$key] = ($pbinone[$key]/$psum)*100;
	$pbinthree[$key] = ($pbinthree[$key]/$psum)*100;
	$pbinfour[$key] = ($pbinfour[$key]/$psum)*100;
}
#$cbins = array_map( function($val,$factor) { return ($val/$factor); }, $cbins, array_fill(0, count($cbins), $csum));
#$pbins = array_map( function($val,$factor) { return ($val/$factor); }, $pbins, array_fill(0, count($pbins), $psum));
// get labels 
$labels = array();
foreach($cbinzero as $label => $value) {
	if ($label != ($lastbin + $binsize)) {
		$labels[] = intval($label/1000)."-".intval(($label+$binsize-1)/1000);
	}
	else {
		$labels[] = ">".intval(($lastbin + $binsize - 1)/1000); # label = max + 1 => need : label - 1 + binsize - 1 = $max + $binsize - 1
	}
}
// fill datarows 
$data1zero = array();
$data1one = array();
$data1three = array();
$data1four = array();
foreach($cbinzero as $bin => $value) {
	$data1zero[] = $value;
}
foreach($cbinone as $bin => $value) {
	$data1one[] = $value;
}
foreach($cbinthree as $bin => $value) {
	$data1three[] = $value;
}
foreach($cbinfour as $bin => $value) {
	$data1four[] = $value;
}
$data2zero = array();
$data2one = array();
$data2three = array();
$data2four = array();
foreach($pbinzero as $bin => $value) {
	$data2zero[] = $value;
}
foreach($pbinone as $bin => $value) {
	$data2one[] = $value;
}
foreach($pbinthree as $bin => $value) {
	$data2three[] = $value;
}
foreach($pbinfour as $bin => $value) {
	$data2four[] = $value;
}


// Create the graph. These two calls are always required
$graph = new Graph(325,325);    
$graph->SetScale("textint");
$graph->xaxis->SetTickLabels($labels);
$graph->xaxis->SetLabelAngle(50);
$graph->xaxis->SetLabelMargin(5);
$graph->SetShadow();
$graph->img->SetMargin(45,30,30,50);

// add text if no data was found.
if ($nodata == 1) {
	$txt = new Text('No Data Available');
	// ( (0,0) is the upper left corner )
	$txt->SetPos(0.5, 0.45,'center','center');
	$txt->SetColor('red');
	$txt->SetFont(FF_FONT2,FS_BOLD); 
	$txt->SetBox('yellow','navy','gray');
	$graph->AddText($txt); 
}
#else {
#	$txt = new Text("csum: $csum ; psum: $psum");
#	// ( (0,0) is the upper left corner )
#	$txt->SetPos(0.5, 0.45,'center','center');
#	$txt->SetColor('red');
#	$txt->SetFont(FF_FONT2,FS_BOLD); 
#	$txt->SetBox('yellow','navy','gray');
#	$graph->AddText($txt); 
#}
// Create the bar plots
$b1plot = new BarPlot($data1zero);
$b1plot->SetLegend('Control/Parents : cn 0');
$b2plot = new BarPlot($data1one);
$b2plot->SetLegend('Control/Parents : cn 1');
$b3plot = new BarPlot($data1three);
$b3plot->SetLegend('Control/Parents : cn 3');
$b4plot = new BarPlot($data1four);
$b4plot->SetLegend('Control/Parents : cn 4');

$b5plot = new BarPlot($data2zero);
$b5plot->SetLegend('Patients : cn 0');
$b6plot = new BarPlot($data2one);
$b6plot->SetLegend('Patients : cn 1');
$b7plot = new BarPlot($data2three);
$b7plot->SetLegend('Patients : cn 3');
$b8plot = new BarPlot($data2four);
$b8plot->SetLegend('Patients : cn 4');

// Create the accumulated bar plots
$ab1plot = new AccBarPlot(array($b1plot,$b2plot,$b3plot,$b4plot));
$ab2plot = new AccBarPlot(array($b5plot,$b6plot,$b7plot,$b8plot));

// Create the grouped bar plot
$gbplot = new GroupBarPlot(array($ab1plot,$ab2plot));
// ...and add it to the graph
$graph->Add($gbplot);

// set colors 
$b1plot->SetFillColor("darkred");
$b2plot->SetFillColor("purple");
$b3plot->SetFillColor("darkgreen");
$b4plot->SetFillColor("darkblue");
$b5plot->SetFillColor("red");
$b6plot->SetFillColor("violet");
$b7plot->SetFillColor("green");
$b8plot->SetFillColor("blue");


$graph->title->Set("CNV Size Distribution");
$graph->subtitle->Set("Chiptype : $chiptype");
$graph->xaxis->title->Set("Size (kb)");
#if ($max < 100) {
#	$graph->xaxis->title->SetMargin(18);
#	$graph->legend->SetPos(0.32,0.99,'center','bottom');
#}
#else {
	$graph->xaxis->title->SetMargin(25);
	$graph->legend->SetPos(0.05,0.15,'right','top');
#}
$graph->yaxis->title->Set("% of CNVs");
$graph->yaxis->title->SetMargin(5);
 
$graph->title->SetFont(FF_FONT1,FS_BOLD);
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
// legend layout
$graph->legend->SetReverse();
$graph->legend->SetColumns(1);
$graph->legend->SetShadow('gray@0.4',5); 
$graph->legend->SetLineSpacing(-1);
$graph->legend->SetFont(FF_DEFAULT,FS_NORMAL,7);
// Display the graph
$graph->Stroke();
?>
