<?php
// include the graphing class 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_pie.php');
require_once ('../jpgraph/src/jpgraph_pie3d.php');

// connect to the database
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = '';
if (isset($_SESSION['userID'])) {
	$userid = $_SESSION['userID'];
}
else {
	// not logged in? set uid to -1
	$userid = -1;
}

// get data
$biggestidx = 0;
$biggestvalue = 0;
$sum = 0;
$query = mysql_query("SELECT ps.idsamp, p.chiptype FROM project p JOIN sample s JOIN projectpermission pp JOIN projsamp ps ON p.id = pp.projectid  AND ps.idproj = p.id AND ps.idsamp = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls','Parents') AND s.intrack = 1 AND s.trackfromproject = p.id");
$title = 'Unique Sample Distribution by Chiptype';
$samples = array();
while ($row = mysql_fetch_array($query)) {
	$samples[$row['idsamp']] = $row['chiptype'];
}

// set labels if items are found.
if (count($samples) == 0) {
	$data = array(1);
	$legends = array('No Data Found');
}
else {
	$sum = count($samples);
	$tmp = array();
	foreach ($samples as $id => $chip) {
		$tmp[$chip] = $tmp[$chip] + 1;
	}
}

// set labels
$data = array();
ksort($tmp);
$idx = -1;
if ($sum > 0) {
	foreach ($tmp as $chip => $value) {
		$idx++;
		$data[$idx] = $value;
		$legends[$idx] =  $chip . " : " . $value;
		if ($value > $biggestvalue) {
			$biggestvalue = $value;
			$biggestidx = $idx;
		}
		if ($value/$sum >= 0.03) {
			$labels[$idx] = "%.0f%%";
		}
		else {
			$labels[$idx] = '';
		}
	}
}


// Create the Pie Graph. 
$graph = new PieGraph(665,325);
$theme_class= new UniversalTheme;
$graph->SetTheme($theme_class);
if (function_exists('imageantialias')) {
    $graph->SetAntiAliasing();
}

// Set A title for the plot
$graph->title->Set($title);
$sum = number_format($sum,0,'',',');
$graph->subtitle->Set("Total : $sum Samples");
// Create
$p1 = new PiePlot3D($data);
$p1->SetSize(0.5);
$p1->SetLegends($legends);
$graph->legend->SetPos(0.63,0.2,'left','top');
$graph->legend->SetColumns(1);
$graph->legend->SetShadow('gray@0.4',5);
$p1->SetCenter(0.32,0.5);

// labels ?
#$p1->SetGuideLines();
#$p1->SetGuideLinesAdjust(1.4);
$p1->ExplodeSlice($biggestidx);
#$p1->value->Show();
$p1->SetLabels($labels);
$p1->SetLabelPos(1);
// add to graph field
$graph->Add($p1);

// draw the plot
$graph->Stroke();


?>

