<?php
// include the graphing class 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_pie.php');
require_once ('../jpgraph/src/jpgraph_pie3d.php');

// connect to the database
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];

$title = 'Unique Sample Distribution by Diagnostic Class';
// get data   => number of samples in each class, without parents & controls
$biggestidx = 0;
$biggestvalue = 0;
$sum = 0;

// get samples for each class.
$classes = array(4,3,2,1);
$samples = array();
foreach($classes as $key => $thisclass) {
	$query = mysql_query("SELECT a.sample FROM aberration a JOIN projectpermission pp JOIN project p JOIN sample s ON a.sample = s.id AND a.idproj = pp.projectid AND p.id = pp.projectid WHERE pp.userid = $userid AND p.collection NOT IN ('Controls','Parents') AND a.class = $thisclass AND s.intrack = 1 AND s.trackfromproject = a.idproj GROUP BY a.sample");
	while ($row = mysql_fetch_array($query)) {
		// this assigns as class to a sample. if it was already specified, it will be overwritten with a lower class (1 comes last).
		$samples[$row['sample']] = $thisclass;
	}
}

// now no specified classes / FP.
$query = mysql_query("SELECT a.sample FROM aberration a JOIN sample s JOIN projectpermission pp JOIN project p ON a.sample = s.id AND a.idproj = pp.projectid AND p.id = pp.projectid WHERE pp.userid = $userid AND p.collection NOT IN ('Controls','Parents') AND ( a.class NOT IN (1,2,3,4) OR a.class IS NULL OR a.class = 5 OR a.class = 0 OR a.class = '') AND s.intrack = 1 AND s.trackfromproject = a.idproj GROUP BY a.sample");
while ($row = mysql_fetch_array($query)) {
	if (!isset($samples[$row['sample']])) {
		$samples[$row['sample']] = 0;
	}
}

// we now have most significant class per sample. Count samples per class and store in data array.
if (count($samples) == 0) {
	$data = array(1);
	$legends = array('No Data Found');
}
else {
	$sum = count($samples);
	$data = array();
	foreach($samples as $id => $class) {
		$data[$class] = $data[$class] +1 ;
	}
}
// get samples without CNVs
$query = mysql_query("SELECT ps.idsamp FROM project p JOIN projectpermission pp JOIN projsamp ps JOIN sample s ON ps.idsamp = s.id AND p.id = pp.projectid  AND ps.idproj = p.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls','Parents') AND s.intrack = 1 AND s.trackfromproject = p.id ");
while ($row = mysql_fetch_array($query)) {
	if (!isset($samples[$row['idsamp']])) {
		$nocnvs[$row['idsample']] = 5;
	}
}
$sum = $sum + count($nocnvs);	

$tmp = array();
// set labels & legends if items are found.
$classdesc = array('5' => 'No Class', '1' => 'Syndrome', '2' => 'Causative', '3' => 'Unclear', '4' => 'Not Causative');
if ($sum > 0) {
	for ($idx = 1; $idx <= 4 ; $idx++) { // each ($data as $idx => $value) {
		$value = $data[$idx];
		$tmp[] = $data[$idx];
		$legends[] = "$idx. ". $classdesc[$idx] . " : " . $value;
		if ($value > $biggestvalue) {
			$biggestvalue = $value;
			$biggestidx = $idx -1 ;
		}
		if ($value/$sum >= 0.03) {
			$labels[] = "%.0f%%";
		}
		else {
			$labels[] = '';
		}
	}
}

$tmp[] = $data[0];
$legends[] = $classdesc[5] . " : " . $data[0];
if ($data[0] / $sum >= 0.03) {
	$labels[] = "%.0f%%";
} 
else {
	$labels[] = '';
}
$tmp[] = count($nocnvs);
$legends[] = "No CNVs : ". count($nocnvs);
if (count($nocnvs) / $sum >= 0.03) {
	$labels[] =  "%.0f%%";
}
else {
	$labels[] = "";
}

// re-assign data[]
$data = $tmp;
// Create the Pie Graph. 
$graph = new PieGraph(325,325);
$theme_class= new UniversalTheme;
$graph->SetTheme($theme_class);
if (function_exists('imageantialias')) {
	$graph->SetAntiAliasing();
}
// Set A title for the plot
$graph->title->Set($title);
$sum = number_format($sum,0,'',',');
$graph->subtitle->Set("Total : $sum Samples");
// Create
$p1 = new PiePlot3D($data);
$p1->SetSize(0.38);
$p1->SetLegends($legends);
$graph->legend->SetPos(0.5,0.97,'center','bottom');
$graph->legend->SetColumns(2);
$graph->legend->SetShadow('gray@0.4',5);
$p1->SetCenter(0.5,0.42);

// labels ?
#$p1->SetGuideLines();
#$p1->SetGuideLinesAdjust(1.4);
$p1->ExplodeSlice($biggestidx);
#$p1->value->Show();
$p1->SetLabels($labels);
$p1->SetLabelPos(1);
// add to graph field
$graph->Add($p1);

// draw the plot
$graph->Stroke();


?>

