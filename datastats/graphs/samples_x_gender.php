<?php
// include the graphing class 
require_once ('../jpgraph/src/jpgraph.php');
require_once ('../jpgraph/src/jpgraph_pie.php');
require_once ('../jpgraph/src/jpgraph_pie3d.php');

// connect to the database
include('../../.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$userid = $_SESSION['userID'];


// get data
$biggestidx = 0;
$biggestvalue = 0;
$sum = 0;
$query = mysql_query("SELECT s.gender, COUNT(DISTINCT(s.id)) as nr FROM project p JOIN sample s JOIN projectpermission pp JOIN projsamp ps ON p.id = pp.projectid  AND ps.idproj = p.id AND ps.idsamp = s.id WHERE pp.userid = $userid AND p.collection NOT IN ('Controls','Parents') AND s.intrack = 1 AND s.trackfromproject = p.id GROUP BY s.gender ORDER BY s.gender DESC");
$title = 'Unique Sample Distribution by Gender';
$data = array();
$legends = array();
if (mysql_num_rows($query) == 0) {
	$data = array(1);
	$legends = array('No Data Found');
}
else {
	$idx = -1;
	while ($row = mysql_fetch_array($query)) {
		$sum = $sum + $row['nr'];
		$idx++;
		$data[] = $row['nr'];
		$legends[] = $row['gender'] . " : " . $row['nr'];
		#$labels[] = "#" . $row['nr'];
		if ($row['nr'] > $biggestvalue) {
			$biggestvalue = $row['nr'];
			$biggestidx = $idx;
		}
	}
}
// set labels if items are found.
if ($sum > 0) {
	foreach ($data as $idx => $value) {
		if ($value/$sum >= 0.03) {
			$labels[$idx] = "%.0f%%";
		}
		else {
			$labels[$idx] = '';
		}
	}
}

// Create the Pie Graph. 
$graph = new PieGraph(325,325);
$theme_class= new UniversalTheme;
$graph->SetTheme($theme_class);
if (function_exists('imageantialias')) {
    $graph->SetAntiAliasing();
}


// Set A title for the plot
$graph->title->Set($title);
$sum = number_format($sum,0,'',',');
$graph->subtitle->Set("Total : $sum Samples");
// Create
$p1 = new PiePlot3D($data);
$p1->SetSize(0.38);
$p1->SetLegends($legends);
$graph->legend->SetPos(0.5,0.97,'center','bottom');
$graph->legend->SetColumns(2);
$graph->legend->SetShadow('gray@0.4',5);
$p1->SetCenter(0.5,0.42);

// labels ?
#$p1->SetGuideLines();
#$p1->SetGuideLinesAdjust(1.4);
$p1->ExplodeSlice($biggestidx);
#$p1->value->Show();
$p1->SetLabels($labels);
$p1->SetLabelPos(1);
// add to graph field
$graph->Add($p1);

// draw the plot
$graph->Stroke();


?>

