<?php
//syntax
?>
<!--
////////////////////////////////////////////////////////////////
// CREATE GRAPHICAL OVERVIEWS OF THE DATA PRESENT IN WEBSTORE //
////////////////////////////////////////////////////////////////

// general intro. 
-->
<div class=sectie>
<h3>CNV Summary Statistics</h3>
<p>These graphs represent some CNV oriented summary information on the data present in you CNV-WebStore projects. Please select the data to display from the list below.</p>

<form action="index.php?page=datastats&type=2" method=POST>
<p><select name=query>
<option value="nr">Number of CNVs per sample, split up by chiptype</option>
<option value="size">Ratio between Del/Dup and the size distribution, split up by chiptype</option>
<option value="persample">Average CNV content per sample, split up by chiptype</option>
</select></p>
<input type=submit class=button value=Show name=Show>
</form></p>
<p></p>
<!-- order the plots three columns wide, with caption below. ? -->
</div>

<!-- NR of CNVs per sample BY CHIPTYPE -->
<?php
if ($_POST['query'] == 'nr' || !isset($_POST['query'])) {
  echo "<div class=sectie>\n";
  echo "<h3>Amount of CNVs per sample, by Chiptype</h3>\n";
  $idx = 0;
  $column = 0;
  $query = mysql_query("SELECT ID, name FROM chiptypes ORDER BY name");
  while ($row = mysql_fetch_array($query)) {
	$column++;
	if ($column > 3) {
		$column = 1;
		echo "<br style=clear:both />";
	}
	$idx++;
	$chip = $row['ID'];
	$chiptype = $row['name'];
	echo "<div class=graph>";
  	echo "<p><img class=graphimg src='datastats/graphs/histogram_cnvs_x_samples.php?ct=$chip&rnd=".rand(1,1000)."'></p>";
  	echo "<p class=imgcap><span class=nadruk>Plot $idx.</span> Number of CNVs per sample on the $chiptype chip. X-axis: number of CNVs found in each sample. Y-axis: percentage of samples in each bin. </p>";
	echo "</div> ";
  }

  echo "<!-- CLEAR THE LINE -->\n";
  echo "<br style='clear:both;' />\n";
  echo "</div>\n";
}
elseif ($_POST['query'] == 'size') {
  echo "<div class=sectie>\n";
  echo "<h3>Deletion/Duplication Ratio, ordered by size, per chiptype</h3>\n";

  $query = mysql_query("SELECT ID, name FROM chiptypes ORDER BY name");
  $column = 0;
  while ($row = mysql_fetch_array($query)) {
	$column++;
	if ($column > 3) {
		$column = 1;
		echo "<br style=clear:both />";
	}
	$idx++;
	$chip = $row['ID'];
	$chiptype = $row['name'];
	echo "<div class=graph>";
  	echo "<p><img class=graphimg src='datastats/graphs/histogram_cnvs_x_size.php?ct=$chip&rnd=".rand(1,1000)."'></p>";
  	echo "<p class=imgcap><span class=nadruk>Plot $idx.</span> Size of CNVs on the $chiptype chip. X-axis: Size of CNVs. Y-axis: Percentage of events in each bin. For each bin, first column are control data, second are patient data.</p>";
	echo "</div> ";
  }
  echo "<br style='clear:both;' />\n";
  echo "</div> \n";
}
elseif($_POST['query'] == 'persample') {

  echo "<div class=sectie>\n";
  $query = mysql_query("SELECT ID, name FROM chiptypes ORDER BY name");
  $column = 0;
  while ($row = mysql_fetch_array($query)) {
	$column++;
	if ($column > 3) {
		$column = 1;
		echo "<br style=clear:both />";
	}
	$idx++;
	$chip = $row['ID'];
	$chiptype = $row['name'];
	if ($chip == 4) {
		$note = "<p class=imgcap><span class=nadruk>NOTE:</span> : The quality of the HapMap data on the HumanCyto2.0 is of extremely low quality. Everywhere else on this website, the data of the HumanCyto2.1 HapMap set is used as reference. Be sure to compare your CNV content with that population !</p>";
	}
	else {
		$note = "";
	}
	echo "<div class=graph>";
  	echo "<p><img class=graphimg src='datastats/graphs/boxplot_cnvcontent.php?ct=$chip&rnd=".rand(1,1000)."'></p>";
  	echo "<p class=imgcap><span class=nadruk>Plot $idx.</span> CNV content on the $chiptype chip. X-axis: Population. Y-axis: Nr of CNVs per sample. Boxplot shows range up to 95%. </p>$note";
	echo "</div> ";
  }
  echo "<br style='clear:both;' />\n";
  echo "</div> \n";
}
?>


