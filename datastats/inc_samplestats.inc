<!--
////////////////////////////////////////////////////////////////
// CREATE GRAPHICAL OVERVIEWS OF THE DATA PRESENT IN WEBSTORE //
////////////////////////////////////////////////////////////////

// general intro. 
-->
<div class=sectie>
<h3>Sample Summary Statistics</h3>
<p>These graphs represent some sample oriented summary information on the data present in you CNV-WebStore projects.</p>
<p></p>
<!-- order the plots three columns wide, with caption below. ? -->

<div class=graph>
  <p><img class=graphimg src='datastats/graphs/samples_x_class.php?rnd=<?php echo rand(1,1000);?>'></p>
  <p class=imgcap><span class=nadruk>Plot 1.</span> Number of samples classified by their most significant diagnostic class.  Control samples, healthy parents and double entries were excluded. </p>
</div> 

<div class=biggraph>
  <p><img class=biggraphimg src='datastats/graphs/samples_x_collection.php?type=total&rnd=<?php echo rand(1,1000);?>'></p>
  <p class=imgcap><span class=nadruk>Plot 2.</span> Total Sample Distribution by Collection: Number of samples assigned to each of the available collections. Samples analysed multiple times are included multiple times.</p>
</div> 


<br style='clear:both;' />
<div class=graph>
  <p><img class=graphimg src='datastats/graphs/samples_x_gender.php?rnd=<?php echo rand(1,1000)?>'></p>
  <p class=imgcap><span class=nadruk>Plot 3.</span> Number of samples classified by gender. Control samples, healthy parents and double entries were excluded. </p>
</div> 

<div class=biggraph>
  <p><img class=biggraphimg src='datastats/graphs/samples_x_collection.php?type=unique&rnd=<?php echo rand(1,1000);?>'></p>
  <p class=imgcap><span class=nadruk>Plot 4.</span> Unique Sample Distribution by Collection: Number of samples assigned to each of the available collections. Double entries were excluded.</p>
</div> 
<br style='clear:both;' />
<!-- CLEAR THE LINE -->
<br style='clear:both;' />

<div class=biggraph>
  <p><img class=biggraphimg src='datastats/graphs/samples_x_chiptype.php?rnd=<?php echo rand(1,1000);?>'></p>
  <p class=imgcap><span class=nadruk>Plot 5.</span> Number of samples classified by chiptype.  Control samples, healthy parents and double entries were excluded. </p>
</div> 
<!-- CLEAR THE LINE -->
<br style='clear:both;' />
<!-- CLEAR THE LINE -->
<br style='clear:both;' />
</div>
