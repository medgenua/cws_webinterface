<?php

# POSTED VARS
$aid = $_GET['aid'];
$uid = $_GET['u'];
$ftt = $_GET['ftt'];
if (isset($_GET['loh'])) {
	$loh = $_GET['loh'];
}
else {
	$loh = 0;
}

if (isset($_GET['mos'])) {
	$mos = $_GET['mos'];
}
else {
	$mos = 0;
}

error_log($loh);

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inharray = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
if ($loh == 1) {
	$atable = 'aberration_LOH';
	$ltable = 'log_loh';
	$dtable = 'deleted_loh';
	$fields = '';
}
elseif ($mos == 1) {
	$atable = 'aberration_mosaic';
	$ltable = 'log_mos';
	$dtable = 'deletedmos';
	$fields = ', a.cn, a.datatype';
}

// rest: regular cnvs (array / swgs)
else {
	$atable = 'aberration';
	$ltable = 'log';
	$dtable = 'deletedcnvs';
	$fields = ', a.cn, a.inheritance, a.datatype';
}

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$retrievequery = "SELECT a.sample, a.idproj, a.class, a.chr, a.start, a.stop, a.seenby, s.chip_dnanr $fields FROM $atable a JOIN sample s ON s.id = a.sample WHERE a.id = $aid ";
error_log($retrievequery);
$query = mysql_query($retrievequery);
$row = mysql_fetch_array($query);
$class = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$start = $row['start'];
$stop = $row['stop'];
$cn = '';
$inh = '';
$datatype = '';
if ($mos == 1) {
	$cn = $row['cn'];
	$datatype = $row['datatype'];
}
elseif ($loh == 1) {
	$cn = 2;
}
else {
	$cn = $row['cn'];
	$datatype = $row['datatype'];
	$inh = $row['inheritance'];
}

$sb = $row['seenby'];
$samplename = $row['chip_dnanr'];
$close = 1;


# DELETE !
if (isset($_GET['delete']) && $_GET['arguments'] != '') {
	$deletedquery = '';
	if ($mos == 1) {
		$deletedquery = "INSERT INTO $dtable (id, cn, start, stop, chr, sample, idproj, class, seenby, datatype) VALUES ('$aid','$cn','$start','$stop','$chr','$sid','$pid','$class','$sb','$datatype')";
	}
	elseif ($loh == 1) {
		$deletedquery = "INSERT INTO $dtable (id, cn, start, stop, chr, sample, idproj, class, seenby) VALUES ('$aid','$cn','$start','$stop','$chr','$sid','$pid','$class','$sb')";
	}
	else {
		$deletedquery = "INSERT INTO $dtable (id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby, datatype) VALUES ('$aid','$cn','$start','$stop','$chr','$sid','$pid','$inh','$class','$sb','$datatype')";
	}
	error_log($deletedquery);
	mysql_query($deletedquery);
	$arguments = $_GET['arguments'];
	$logentry = "Deleted";
	mysql_query("INSERT INTO $ltable (sid, pid, aid, uid, entry, arguments) VALUES ('$sid', '$pid', '$aid', '$uid', '$logentry', '$arguments')");
	$query = mysql_query("DELETE FROM $atable WHERE id = '$aid'");
	$close = 1;
}
else {
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" .$usrow['FirstName'] .' '.$usrow['LastName'] . ")";
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	echo "<h3>Deleting a CNV Needs Confirmation</h3>";
	echo "<p>You are about to delete the following region. To do so, specify a clear reason and press 'Delete'</p>\n";
	echo "<p>";
	echo "<ul>\n";
	echo "<li>Region: $region</li>";
	echo "<li>CopyNumber: $cn</li>";
	echo "<li>Sample: $samplename</li>";
	echo "<li>Diagnostic Class: $class $setby</li>";
	if ($mos == 0 and $loh == 0) {
		echo "<li>Inheritance: ".$inharray[$inharray[$inh]]."</li>";
	}
	echo "</ul></p>";


	echo "<p><form action=deletecnv.php method=GET>";
	echo "<input type=hidden name=aid value='$aid'>\n";
	echo "<input type=hidden name=u value='$uid'>\n";
	echo "<input type=hidden name=loh value='$loh'>\n";
	echo "<input type=hidden name=mos value='$mos'>\n";
	echo "Reason: <br/><input type=text value='' name=arguments maxsize=255 size=35></p>";
	echo "<input type=submit name=delete value='Delete'>\n";
	echo" </form>\n";
}

if ($close == 1) {
	echo "<script type='text/javascript'>window.opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
?>
