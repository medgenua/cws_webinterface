<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
###################
# GET FORM PARAMS #
###################
$uid = $_GET['uid'];
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$type = $_GET['type'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
# GET CONTROL DATA FROM DATABASE
$dgvtypelong = array('gain' => 'Copy Number Gains', 'loss' => 'Copy Number Losses', 'inv' => 'Inversion');
$dgvlong = $dgvtypelong[$type];
echo "<div class=nadruk>Toronto DGV $dgvlong: </div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Region: Chr$chr:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
echo "<li> - Size : $sizeformat bp<li>";
echo "<li> - Type : $dgvlong</li>";
echo "<li> - <a class=tt href='http://projects.tcag.ca/cgi-bin/variation/gbrowse/$ucscdb/?start=$start;stop=$stop;ref=chr$chrtxt;width=760;version=100;label=chromosome:overview-cytoband-RefGene-super_Duplication-Variation-Inversion-InDel;grid=on' target='_blank'>Show in DGV-Browser</a></li>\n";
echo "</ul></p>";
echo "<p>";
echo "<img src='dgvplot.php?&amp;u=$uid&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;type=$type' border=0>\n";
echo "</p>";



?>
