<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$uid = $_GET['u'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$window = $stop-$start+1;
$type = $_GET['type'];

# GET included studies
//$query = mysql_query("SELECT DGVinclude FROM usersettings WHERE id = '$uid'");
//$row = mysql_fetch_array($query);
//$instring = $row['DGVinclude']; 
# DEFINE IMAGE PROPERTIES
$querystring = "SELECT id FROM DGV_gold WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) AND vsubtype = '$type' ";
$result = mysql_query($querystring);
$nrlines = mysql_num_rows($result);
$height = $nrlines*9 + 20;

// scale
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
}
else {
	$scale = 500000;
	$stext = "500 kb";
} 


$width = 400; //Image width in pixels
$xoff = 130;

// set scaling factor
$scalef = ($width-$xoff)/($window);
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];


//Specify constant values
//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$orange = ImageColorAllocate($image,255, 179,0);

$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

// dgv types
$dgvcolors = array('Gain' => $blue, 'Loss' => $red, 'Inv' => $gpos50);
$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
//Fill background
imagefill($image,0,0,$white);

// get data
$querystring = "SELECT id, frequency, start, stop, max_start, max_stop FROM DGV_gold WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) AND vsubtype = '$type' ORDER BY start";
$result = mysql_query($querystring);
$y = 8;
$cy = $y;
$cstudid = '';

while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
    $cmstart = $row['max_start'];
    $cmstop = $row['max_stop'];
	
	
	// next line
    $y = $y + 9;
    // cnv id
	imagestring($image,1,10,$y,$row['id'],$black);
	// cnv frequency
	imagestring($image,1,90,$y,$row['frequency'],$black);	
	$larrow = 0;
	if ($cstart < $start) {
		$larrow = 1;
		$cstart = $start;
	}
	$rarrow = 0;
	if ($cstop > $stop) {
		$rarrow = 1;
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	imagefilledrectangle($image,$xoff+$scaledstart,$y+1,$xoff+$scaledstop,$y+4,$dgvcolors[$type]);
	if ($larrow == 1) {
		imageline($image,$xoff+$scaledstart+1,$y+2,$xoff+$scaledstart+6,$y+2,$white);
		imageline($image,$xoff+$scaledstart+1,$y+2,$xoff+$scaledstart+4,$y+4,$white);
	}
	if ($rarrow == 1) {
		imageline($image,$xoff+$scaledstop-6,$y+2,$xoff+$scaledstop-1,$y+2,$white);
		imageline($image,$xoff+$scaledstop-1,$y+2,$xoff+$scaledstop-4,$y+4,$white);
	}

	
}


# Draw the table borders
imagestring($image,2,10,1,"cnv_id",$gpos75);
imagestring($image,2,95,1,"Freq",$gpos75);
imagefilledrectangle($image,0,0,0,$y+10,$gpos50);
imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
imagefilledrectangle($image,$width-1,0,$width-1,$y+10,$gpos50);
imagefilledrectangle($image,85,0,85,$y+10,$gpos50);
imagefilledrectangle($image,128,0,128,$y+10,$gpos50);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);		

#draw position indications: LEFT
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,130,7,134,4,$gpos75);
imageline($image,130,7,134,10,$gpos75);
imageline($image,130,7,140,7,$gpos75);
#imageline($image,$xstop,18,$xstop,14,$black);
#imageline($image,$xstart,16,$xstop,16,$black);
imagestring($image,2,144,1,$formatstart,$gpos75);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
// RIGHT
imageline($image, 400,7,390,7,$gpos75);
imageline($image, 400,7,396,4,$gpos75);
imageline($image, 400,7,396,10,$gpos75);
imagestring($image,2,388-$txtwidth,2,$formatstop,$gpos75);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

