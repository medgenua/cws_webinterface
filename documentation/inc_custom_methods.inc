<?php

if ($_GET['t'] == 'main' || $_GET['t'] == '') {
/*
	echo "<div class=sectie>";
	echo "<h3>Browsing and Managing Your Data</h3>\n";
	echo "<p>The data are stored in a central database, and can be represented or browsed in multiple ways. Each way has a specific goal, and is briefly discussed here</p>\n";
	echo "<p><span class=nadruk>Browse Projects</span></p>\n";
	echo "Each sample is contained in a 'project', most likely corresponding to one lab-experiment. These projects are bundled on the type of lab or research topic. Using a Lab-project-sample representation, you can easily browse through all results, if you known what your looking for. Results from a specific project can then be visualised graphically, or can be further browsed on a per-sample basis.</p>\n";
	echo "<p><span class=nadruk>Search on Sample</span></p>\n";
	echo "<p>If you know the sample ID, but not the project, or you want to check if a certain sample is analyzed in multiple projects, you can retrieve this information here. From the search results, a directly link to the sample details is provided\n</p>";
	echo "<p><span class=nadruk>Search on Region/Gene</span></p>\n";
	echo "<p>Searching is possible on UCSC-style region (chr7:21,741,335-32,487,687), cytoband notation (12q13-q14) or on RefSeq gene symbol (ATRX). When a cytoband is not found, an educated guess is made for corresponding bands. Also, if multiple transcripts exist for a single gene, or when wildcarts (*) are used, the user will be prompted to select one. \n</p> ";
	echo "<p><span class=nadruk>Search on phenotype</span></p>\n";
	echo "<p>Using the London Neurogenetics Database Ontology, phenotypic information can be associated to the analysed samples. Searching on these phenotypes provides a powerfull tool to group samples and check if there are common aberrant regions.</p>";
	echo" <p><span class=nadruk>Permissions</span></p>\n";
	echo "Manage your projects, share or unshare them with other users, or usergroups. You can specify specific permissions when sharing with single users. Multiple projects can also be grouped into a single new one, and obsolete projects can be deleted.</p>\n";
	echo "<p><span class=nadruk>Bookmark Files</span></p>\n";
	echo "<p>Bookmark files are xml files that can be imported into BeadStudio/GenomeStudio. These files are kept for about 6 months on the server and can be downloaded from here. (section only accessible from the menu on the left)\n</p> ";
*/
}
else{
	include('documentation/inc_custom_methods_'.$_GET['t'].'.inc');
}	
