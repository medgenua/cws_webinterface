<div class=sectie>
<h3>Creating a parser for pre-analysed data files</h3>

<p><span class=nadruk>Step 1. Provide a sample datafile</span><p>
<p><a href='images/content/createparser1.png' rel=lightbox><img src="images/content/createparser1.png" class="kader_links" style='width:45%'/></a><br/>If you open the "manage parsers" page, it will show your previously created parsers, and at the bottom a field to provide a datafile.
<br/>
<br/>
Povide as input file the tab-separated file with variants you want to add to CNV-WebStore and press 'Next'.
<br/><br/>
To delete old parsers, click the delete button next to the item in the top table.
<br style="clear:both;">

<p><span class=nadruk>Step 2. Select Headers.</span><p>
 <p><a href='images/content/createparser2.png' rel=lightbox><img src="images/content/createparser2.png" class="kader_links" style='width:35%'/></a>If the file contains column headers, select the checkbox. The parser supports multiline-per entry format. This means that data from multiple, but fixed number, lines will be combined while parsing. If your file contains such data, provide the number of lines per entry.
<br/>
Press 'Submit' if you changed any of these settings.</p>
<br style="clear:both;">

<p><a href='images/content/createparser3.png' rel=lightbox><img src="images/content/createparser3.png" class="kader_links" style='width:45%'/></a> <br/>Select for each column the type of information it contains. Select 'Discard' to ignore a column.
<br/>
<br/>
The 'Refine' checkbox allows to further subdivide a column in the following step.</p>

<br style="clear:both;">

<p><span class=nadruk>Step 3. Refine Column Parsing.</span><p>
<p><a href='images/content/createparser4.png' rel=lightbox><img src="images/content/createparser4.png" class="kader_links" style='width:45%'/></a> <br/>Using the php-perl regex function, substrings can be extracted from columns and assigned to specific fields. Some examples are given  (note that no hyphens are used, and no slashes in the regex):
<br/>
<br/>
&nbsp; entry: chr1:24,214,350-27,555,666<br/>
&nbsp; regex: chr([\dXM]+):([\d,]+)-([\d,]+)<br/>
&nbsp; keep: 1 2 3 <br/><br/>
  This will extract three fields, to assign to chromosome, start and end position.
<br/>
<br/>
<a href='http://php.net/manual/en/function.preg-match.php'>More info</a>. 
</p>
<br style="clear:both;">

<p><span class=nadruk>Step 4. Check the parser</span><p>
<p><a href='images/content/createparser5.png' rel=lightbox><img src="images/content/createparser5.png" class="kader_links" style='width:45%'/></a> <br/> After all columns are assigned to database fields, check the settings before saving it. If anything went wrong, use the browser back buttons to go back. 
</p>
<br style="clear:both;">

<p><span class=nadruk>Step 5. Save the parser</span><p>
<p><a href='images/content/createparser6.png' rel=lightbox><img src="images/content/createparser6.png" class="kader_links" style='width:45%'/></a> <br/> Finally, provide a name and some additional information and select 'Submit Parser' to store. 

<br/>
<br/>
The parser is now available for <a href='index.php?page=custom_methods&type=st'>storing data into CNV-WebStore </a>
</p>
<br style="clear:both;">


