<div class=sectie>
<h3>Storing pre-analysed data files</h3>

<p><span class=nadruk>Step 1. Provide a sample datafile and parser to use</span><p>
<p><a href='images/content/storecustom1.png' rel=lightbox><img src="images/content/storecustom1.png" class="kader_links" style='width:45%'/></a><br/>Adding pre-analysed data to CNV-WebStore is achieved by uploading a tab-delimited file using the selection field, and parsing it using a predefined parser, which is selected from the dropdown list. If you select a parser, some information about it will be shown. 
<br/>
<br/>
<br style="clear:both;">

<p><span class=nadruk>Step 2. Check the output of the parser</span><p>
<p><a href='images/content/storecustom1b.png' rel=lightbox><img src="images/content/storecustom1b.png" class="kader_links" style='width:45%'/></a><br/>A few sample rows are shown to check if the parser is correct for the provided data. If all looks ok, select 'Continue'. 
<br/>
<br/>
<br style="clear:both;">

<p><span class=nadruk>Step 3. Provide General Details</span><p>
<p><a href='images/content/storecustom2.png' rel=lightbox><img src="images/content/storecustom2.png" class="kader_links" style='width:45%'/></a><br/> Provide general details about the new samples. If they should be added to a new project, provide a name. If they must be added to an existing project, select one of the projects available to the current useraccount from the drop-down menu (not shown in this picture).
<br/>
<br/>
Platform and chiptype info can be added if it was not available in the provided datafile columns. 
<br style="clear:both;">

<p><span class=nadruk>Step 4. Provide Sample Details</span><p>
<p><a href='images/content/storecustom3.png' rel=lightbox><img src="images/content/storecustom3.png" class="kader_links" style='width:45%'/></a><br/> Provide sample genders (if not specified in the data), and clinical information. This info will be added to the free-text clinical information. 
<br/>
<br/>
<br style="clear:both;">

<p><span class=nadruk>Step 5. Missing information</span><p>
<p><a href='images/content/storecustom4.png' rel=lightbox><img src="images/content/storecustom4.png" class="kader_links" style='width:45%'/></a><br/> Supply missing information. When a row contains empty cells, these rows will be provided for completion. If you do not now the info, and leave it blank, the row will be discarded. 
<br/>
<br/>
After completion of the insert, your data are available in <a href='index.php?page=custom_methods&type=tree'>the project browser</a>.
<br style="clear:both;">

