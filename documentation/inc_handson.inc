<?php
//SYNTAX	
?>

<div class=sectie>
<h3>Hands-On Demo With Sample Data </h3>
<p style='padding-left:5px;'>This overview will provide a short overview of the capabilities of this platform, guided by a small demo dataset. You will analyse the data yourself and complete the annotation and interpretation process all the way down to reporting. Just follow the steps below to explore the platform.
</p>

<!-- GET A USERNAME -->
<p><span class=nadruk>0: Register a username:</span></p>
<p style='padding-left:5px;'>=> <a href='index.php?page=request_user'>REQUEST USERNAME</a> <=</p>
<p style='padding-left:5px;'>Requesting a username will result in full access to the platform. All data ananlysed will be coupled to that username and will not be visible by other users, unless you allow them to see the data.</p>


<!-- DOWNLOAD DATA -->
<p><span class=nadruk>1: Download Demo Data:</span></p>
<p style='padding-left:5px;'>=> <a href='getDemoData.php'>DOWNLOAD</a> <=</p>
<p style='padding-left:5px;'>The data are provided as a compressed archive, consisting of BeadArray data from two chromosomes for a single family with three members. For detailed information on how to create similar files yourself, or on the exact content of the files, look <a href='index.php?page=documentation&s=run&t=multiple'> here</a></p>

<!-- START ANALYSIS -->
<p><span class=nadruk>2:  Setup The Analysis:</span></p>
<p style='padding-left:5px;'>=> <a href='index.php?page=run&type=multiple&v=r' target='_blank'>GO HERE</a> <=</p>
<p style='padding-left:5px;'>Follow the link above the the analysis setup page. On the lower end of the page, you can set some details for the analysis.</p>
<p style='padding-left:5px;'><table cellspacing=0>
<tr><th class=topcellalt style="border-left: 1px solid #a1a6a4;">Item</th><th class=topcellalt>Description</th><th class=topcellalt>Example Value</th></tr>
<tr><th class=specalt>Project Name</td><td>Free Name for the experiment. Used for browsing experiments later</td><td>MyDemoProject</td></tr>
<tr><th class=specalt>Collection</td><td>Grouping Variable to keep large projects together</td><td>Take 'Guest Data' for now</td></tr>
<tr><th class=specalt>Table of Genders</td><td>Contains Info on Samples (gender, sample names, callrates)</td><td>Demotrio.genders.txt</td></tr>
<tr><th class=specalt>Table of LogR</td><td>Contains Probe Data (LogR/Baf/Gtype)</td><td>Demotrio.data.txt</td></tr>
<tr><th class=specalt>Pedigree Info</td><td>Contains Parents/Offspring Relations</td><td>Demotrio.pedigree.txt</td></tr>
<tr><th class=specalt>Chiptype</td><td>Set this to the correct BeadChip Version</td><td>Use HumanCyto12v2.0</td></tr>
<tr><th class=specalt>Assymmetric Filter</td><td>Over-rule the majority vote for PennCNV called duplications</td><td>Set To Yes</td></tr>
</table></p>
<p style='padding-left:5px;'>Next, click '...Go...' in the lower right corner and continue with step 3.</p>

<!-- CHECK DATA -->
<p class=nadruk>3: Found Data Overview</p>
<p style='padding-left:5px;'>This screen shows some details on the samples found in the provided files. If mandatory columns are missing, or samples are listed in one file and not in another, you will be notified. The 'In Track' and 'For Track' checks control if a sample will be included in future statistics and search results. When you notice here that a sample has a low call-rate, it can be excluded from the search results here. The sample will be analysed however and you will get results (but they might be unreliable). Samples can also be added or excluded from search results later on.</p>
<p style='padding-left:5px;'>For now, leave all checkboxes checked, as the Demo Data is of high quality. Click 'Start Analysis' and continue with step 4</p>

<!-- PROGRESS OVERVIEW -->
<p class=nadruk>4: Analysis Progress Monitor</p>
<p style='padding-left:5px;'>This page monitors the programs actions and provides the user with some feedback. On top there is one button 'Results So Far' that will lead you to an graphical overview of the (incomplete) results. This is explained in step 5.</p>
<p style='padding-left:5px;'>When analysis is finished, extra items show up. First there are download links for the XML bookmarks that can be imported back into GenomeStudio, and tab-delimted TXT files containing the same information. These files are available for both the majority vote and each of the seperate methods. The 'details' links leads to the tabular project browser, explained in step 6. The 'Create Custom Bookmarks' tools allows you to filter CNVs on several criteria and export the result in XML format. This is explained in step 15.</p>
<p style='padding-left:5px;'>The lower half shows the algorithm output. If you feel that anything went wrong, you can check here if any errors showed up, and for larger projects the progress can be followed live. </p>

<!-- GRAPHICAL RESULTS BROWSER -->
<p class=nadruk>5: Graphical Projects Overview</p>
<p style='padding-left:5px;'>For details information on using this page, click <?php echo "<a href='https://$domain/$basepath/index.php?page=documentation&s=overview&t='>here</a>"; ?>.</p>
<p style='padding-left:5px;'>Since the DemoData contains only data for chromosome 9 and 10, browse immediately to chromosome 9 by clicking on the link on top. Now you see for the sample 'Demo_Data_Child' that there is a small purple bar on the right border of the plot. This indicates a deletion. Hover the mouse over the purple box and a pop-up with detailed information will be shown. Hovering over 'Father' and 'Mother' entries will show the corresponding region for parental data. You can easily see that the deletion detected in the child is of <span class=italic>De Novo</span> origin. The sample name listed on the right is a hotlink to a tabular details browser, discussed in section 7. We will come back to the tooltips themselves in section 10, further explaining options for functional interpretation.</p>
<p style='padding-left:5px;'>From This graphical overview, the overlap of the chromosome 9 deletion with the 9q-Deletion syndrome from DECIPHER should be clear. To evaluatie it in more detail, click on the 9q-Ter cytoband, in the karyogram above the main plot. After zooming in, it is clear that none of the over 200 HapMap Samples shown as controls contains this deletions, and just one sample from a control study by Iafrate et al contains a significant overlap. When looking at the gene content on top however, we can see that this deletion does not contain the EHMT1-gene, while the deletion in the demo sample does contain that gene.</p>
<p style='padding-left:5px;'>To further explore our data, we will go to the tabular representation. </p>

<!-- PROJECT TREE BROWSER -->
<p class=nadruk>6: Tabular Projects Browser</p>
<p style='padding-left:5px;'>=> <a href='index.php?page=results&type=tree&v=b' target='_blank'>GO HERE</a> <=</p>
<p style='padding-left:5px;'>This browser is the recommended way to find a specific experiment and go back to the graphical overview explained above. When looking for a single sample or region however, there are other options, available under 'Browse Results' on the left hand menu. </p>
<p style='padding-left:5px;'>From the drop-down menus, pick the 'Demo_Data_Child' sample, optionally picking the correct collection and project to narrow the choices down. AFter selecting the sample, the table below the forms will show all experiments containing the sample. This makes it possible to locate replicate samples.</p>
<p style='padding-left:5px;'>Now click 'Show Sample Details' to go to the tabular Samples Details Browser. The 'Show Project Overview' button leads to the graphical browser explained above, while the 'Share this Project' button allows you to share your data with collaborators, as explained in section 21. 

<!-- SAMPLE TABLE DETAILS -->
<p class=nadruk>7: Tabular Samples Details Browser</p> 
<p style='padding-left:5px;'>For further annotating and examining the data, all information regarding a sample is presented in a single page. Each of the information blocks is described below.</p>
<p style='padding-left:5px;'><span class=italic>a) Sample Details</span></p>
<p style='padding-left:15px;'>Information about the sample, you should double check the Quality Controls (derived from PennCNV). When failed, remarks will be shown below the actions table. Also check the gender, as incorrect genders might indicate sample errors. </p>
<p style='padding-left:5px;'><span class=italic>b) Actions</span></p>
<p style='padding-left:15px;'>Several tools for to use on the sample under review. For now, try the 'Whole Genome Plots', as they provide the visual data overview without the need to go back to GenomeStudio. As mentioned before, sampels can be removed from or added to search results with the 'Remove/Add sample from search results option'. Finally, click on 'Add Clinical Information' and read the information under section 8 (otherwise section c will not show up). We will come back on the other options later on.</p>
<p style='padding-left:5px;'><span class=italic>c) Clinical Information</span></p>
<p style='padding-left:15px;'>The clinical synopsis is shown after the details are added as shown in section 8.</p>
<p style='padding-left:5px;'><span class=italic>d) Sorting Options</span></p>
<p style='padding-left:15px;'>By default, detected CNVs are ordered by chromosomal position. Other options are ranking by size and number of genes, or by gene Prioritisation. Select your method of preference from the dropdown list and click 'change'. For gene prioritization (using Endeavour), there are currently two trainingsets available, Mental Retardation and Epilepsy. Please contact the webmaster if you have trainingsets for additional phenotypes.</p>
<p style='padding-left:5px;'><span class=italic>e) Observed Aberrations</span></p>
<p style='padding-left:15px;'>This table gives the details on all CNVs detected in the sample under review. From left to right there is an indication of importance (based on size, number of genes, overlap with decipher cases/syndromes/occurrece), the genomic region, the predicted copynumber, genomic size, number of affected probes, diagnostic classification, inheritance, statistic significances, reccurence estimations, number of affected genes and some extra links such as decipher cases or attached publications. A few actions are available are covered below.</p>  
<p style='padding-left:15px;'>Just above the table is a link taking you to a page where the Diagnostic relevance and inheritance can be defined, together with some options to alter CNV details. This is described in section 9. Go there after exploring the current CNV table as described below.</p>
<p style='padding-left:20px;'><span class=italic style='text-decoration:underline;'>I) Right Clicking a table row</span></p>
<p style='padding-left:30px;'>This brings up the 'actions' context menu. On top a list of links to public resources is presented, which can be adapted to suite your own needs by clicking 'change' (see <a href='index.php?page=settings&type=plots&st=publ&v=p'>here</a> under 'queries by genomic region'). When both parents are availabe, the next option is to investigate the 'Parent of Origin' of the CNV. This is particularly usefull in case of <span class=italic>De Novo</span> events. Next, there is an option to add or show associated publications. This might be usefull when sharing annotated samples with other users, as they can quickly locate the relevant literature. Adding and browsing literature is explained in section 13.  Finally, a quick link to the qPCR primerdesign pipeline is presented, providing an easy way to set up experimental validation. Click the 'Add a publication' link for the 9qter deletion and add the pubmedID '15264279'.</p>
<p style='padding-left:20px;'><span class=italic style='text-decoration:underline;'>II) Hovering the Copy Number Value</span></p>
<p style='padding-left:30px;'>When the mouse is placed over the predicted copy number value, a CNV-based popup is presented containing more information on this CNV. The exact details are discussed in section 10.</p>
<p style='padding-left:20px;'><span class=italic style='text-decoration:underline;'>III) Affected Genes</span></p>
<p style='padding-left:30px;'>When the CNVs have been prioritized, CNVs containing significantly associated genes will be colored red. Placing you mouse on the link will show the significant genes. Click on the link to go to gene-based details page, discussed in section 14.</p>
<p style='padding-left:20px;'><span class=italic style='text-decoration:underline;'>IV) Additional Links</span></p>
<p style='padding-left:30px;'>If available, icons will indicate overlapping Decipher patients (<img src='images/bullets/patient.ico' alt='' height='12'/>), syndromes (<img src='images/bullets/syndrome.ico' alt='' height='12' />), or associated literature (<img src='images/content/pubmed.png' height='12'/>).</p>
<p style='padding-left:5px;'><span class=italic>f) Uniparental Disomy Analysis</span></p>
<p style='padding-left:15px;'>UPD can be detected using SNP arrays based on the provided genotype of offspring and parents. The analysis is performed when all family members are available during analysis, or by providing full data for all members later on with the 'New Analysis'->'Add Family info' option. Here you see that the 9qter deletion is picked up as a uniparental isodisomy of maternal origin, which is normal since the paternal allel is deleted. The remarks column shows that this region overlaps with the already picked up deletion. Since this region is a duplicate, click 'Delete This Region' to discard it. If it would be a true UPD event, it an be added to the general CNV table for inclusion in reports and search results.
<p style='padding-left:5px;'><span class=italic>g) B-Allele-Frequency Segmentation Results</span></p>
<p style='padding-left:15px;'>During analysis, the Standard Deviation on the B-Allele Frequence (taken from QuantiSNP) is checked for outliers. Outliers might indicate mosaicism, and if present, the sample is analysed using BAFsegment, presenting the results on this page. There are no mosaic samples in the demo project, but to check a mosaic sample, join the usergroup 'Control Samples' (from <a href='index.php?page=group&type=join'>here</a>) and then check out HapMap sample 'NA10856' on the 'HumanOmniExpress12v1.0' chip. This sample contains a mosaic duplicaton on 8qter.</p>
<p style='padding-left:5px;'><span class=italic>h) References</span></p>
<p style='padding-left:15px;'>Aftar adding associated literature to a CNV, the References section contains a bibliography with full titles and authors. Click the '[PubMed'] link to read the article or abstract.</p>
<p style='padding-left:5px;'><span class=italic>i) History</span></p>
<p style='padding-left:15px;'>Finally, all annotation changes to CNVs are listed for traceability. 

<!-- CLINICAL DATA -->
<p class=nadruk>8: Adding Clinical Information</p> 
<p style='padding-left:5px;'><span class=italic>a) Free Text Information</span></p>
<p style='padding-left:15px;'>Some random information can be added in a free text field, for example when you can't find the exact ontology term. Add the term 'Short Fingers' in the text field and click 'Update'.</p>
<p style='padding-left:5px;'><span class=italic>b) Ontology Based Information</span></p>
<p style='padding-left:15px;'>Adding clinical information using the London NeuroGenetics Database nomenclature allows to group samples based on phenotype (see <a href='index.php?page=documentation&s=results&t=pheno'>here</a>). Add the following terms by either typing into the text box and selecting form the dropdown list, or by browsing the ontology tree: 'Mid-face hypoplasia', 'Hypotonia (non-myopathic)' and 'microcephaly'. Switching between the tree and the dropdown list can be done by the 'alternative' links below the form. To remove a phenotypic trait, click the trashbin on the left of the items in the table below the form.</p>

<!-- Assigning Diagnostic Classes -->
<p class=nadruk>9: Annotationg CNVs</p> 
<p style='padding-left:5px;'>CNVs can be annotated from two different places. First, from quicklinks in the CNV-Context popups described in section 10, and second, from the dedicated page accessible from the link just on top of the CNV table as described in section 7. There are a few options when adding annotations to CNVs, each summarised here. After setting everything as listed below, go back to the sample by following the 'go back to sample details' link.</p>
<p style='padding-left:5px;'><span class=italic>a) Diagnostic Class</span></p>
<p style='padding-left:15px;'>There are five different classes available to assing to a CNV. A proposed system is : 
	<ul id=ul-simple>
		<li> &nbsp; 1. Corresponding to a described syndrome. Pathogenic.</li>
		<li> &nbsp; 2. Non-Described but most likely to be pathogenic (eg. Very large <span class=italic>De Novo</span> deletion)</li>
		<li> &nbsp; 3. Unclear significance. Should be correlated to the phenotype (eg. Inherited large duplication, not described before)</li>
		<li> &nbsp; 4. Benign Variant</li>
		<li> &nbsp; FP. False Positive Call. </li>
	</ul></p>
<p style='padding-left:15px;'>Set the 9qter deletion to class 1, and the 10qter duplication to class 4 (present in multiple HapMap samples and DGV) and click 'Submit'</p>
<p style='padding-left:5px;'><span class=italic>b) Inheritance</span></p>
<p style='padding-left:15px;'>As can be seen from the CNV-Context popups, the 9qter deletion is <span class=italic>de novo</span> and the 10qter duplication is inherited from the father. Set these inheritance patterns and press 'submit'.</p>
<p style='padding-left:5px;'><span class=italic>b) CNV editing</span></p>
<p style='padding-left:15px;'>As no method is perfect, called CNVs may be edited, merged or deleted. If you do so, a reason will be required and stored.</p>
	
<!-- CNV TOOLTIPS -->
<p class=nadruk>10: CNV Context Pop-Ups</p>
<p style='padding-left:5px;'> When placing your mouse over a CNV-bar in the graphical browser, or over the copynumber in the tabular browser, a popup is shown grouping usefull info on the sample, and CNV together. To fix its position click once. to hide it, click again. Follow the guidelines below to explore all the information contained in these popups. Fix the tooltip containing information on the 9qter deletion.</p>
<p style='padding-left:5px;'><span class=italic>a) Family Panel</span></p>
<p style='padding-left:15px;'>Just under the region, links to the parents of the current sample are shown (if available). Move the mouse over the links to show the same region in the parental data. This is usefull for checking inheritance. The region, as detected in the offspring, is indicated by red bars on the parental plot. When showing parental data, the details listed below are replaced by a summary of the CNV/sample details.</p>
<p style='padding-left:5px;'><span class=italic>b) CNV and Sample details</span></p>
<p style='padding-left:15px;'>Details on the current CNV are listed on the left, including size, region, copyNumber and Classification details. Diagnostic classification can be changed from the quicklinks present here. If parental info is available, there will also be a 'Check Parent of Origin' option. Another quicklink leads to an overview of the affected genes. Click on this link and read section 16.</p> 
<p style='padding-left:15px;'>Sample details, listed on the right also include a QC-summary. If the sample failed quality control, this will be mentioned here, suggesting caution on believing the CNV call. 
<p style='padding-left:5px;'><span class=italic>c) Sub Data Menu</span></p>
<p style='padding-left:15px;'>Select what needs to be shown on the bottom half of the pop-up.</p>
<p style='padding-left:5px;'><span class=italic>d) Plots</span></p>
<p style='padding-left:15px;'>These plots show the probelevel data for the CNV and surrounding 500Kb. Use these plots for visual inspection of the CNV to exclude putative false postive calls. The top panel shows the LogR values, the bottom one shows B-Allele Frequency. At the bottom, affected genes are shown, and regions that ware also detected in the HapMap Panel analysed on the same chiptype.
<p style='padding-left:5px;'><span class=italic>e) Clinical</span></p>
<p style='padding-left:15px;'>If available, this option shows the associated clinical characteristics of the current sample. These characteristics can be changed from the sample details page, as described in section 8</p>
<p style='padding-left:5px;'><span class=italic>f) Karyogram</span></p>
<p style='padding-left:15px;'>Visual summary of all CNVs detected in the sample under review. Look here to check if other CNVs were annotated as relevant for the phenotype before.</p>
<p style='padding-left:5px;'><span class=italic>g) Public Tools</span></p>
<p style='padding-left:15px;'>The presented links to public resources can be adapted to suite your own needs by clicking 'change' (see <a href='index.php?page=settings&type=plots&st=publ&v=p'>here</a> under 'queries by genomic region'). For now click (or add first) the 'PubMed Fetcher' link. This tool tries to find a link between the associated clinical characteristics, and the genes in the region under review. The tool is further explained under section 12. </p>

<!-- REPORTING -->
<p class=nadruk>11: Creating Reports</p>
<p style='padding-left:5px;'>After fully annotating the CNVs for the sample, go back to the tabular sample details page and press 'Create Patient Report' at the top of the page.</p>
<p style='padding-left:5px;'>On the resulting page, select or deselect the items to include in the report. For now, exclude the additional comments section and include the History (or 'Log'). If needed, CNVs can be filtered on class, size or number of probes. Set the filter to only show classified CNVs ('Class 1-4 + False Positives'). Next, click 'Create Report'.</p>
<p style='padding-left:5px;'>A pdf file will be presented for download. This file contains some experimental data on the sample under review (name on top, gender, project, date, genomic build and QC-values below). Next, the clinical data is included, followed by a ISCN notation of the karyotype. Note that only class 1 and 2 CNVs are included in the ISCN notation! Next statistics on included CNVs and filter settings are shown, and a table listing the gene content. A full karyogram showing affected regions is shown next, followed by a list of MORBID descriptions if genes are affected that are listed in the MORBID map. Finally a table is presented with all regions, details and gene content. Bold genes are listed in the MORBID map, while slanted gene symbols represent genes that are in the maximal genomic region of the CNV (meaning between the last non-affected, and the first affected probe). </p> 

<!-- PubMed Fetcher -->
<p class=nadruk>12: Search PubMed</p>
<p style='padding-left:5px;'>The PubMed-Fetcher tool can be accessed from both the Region specific public tools, or by using the hotlink 'Search PubMed' at the top of the sample details page. Go to the tool from the context menu of the 9qter deletion. </p>
<p style='padding-left:5px;'><span class=italic>a) Setup</span></p>
<p style='padding-left:15px;'>If  you come from the CNV-WebStore pages, the regions and query terms will already be filled in. Add terms if needed, and choose settings at the bottom. For now, leave everything as is and press 'Submit'. If you use the tool on its own, you can specify regions yourself, or add a list of genes. 

<p style='padding-left:5px;'><span class=italic>b) Results</span></p>
<p style='padding-left:15px;'>There are two association found using these settings and phenotypic information, both based on the affected chromosome band. Click 'Abstracts' to read the abstracts. All query terms and gene/cytoband symbols will have their own color code. For each abstract, there is a 'To PubMed' link to get the publication. Now go back to the setup page and add 'Mental Retardation' as an extra query term and rerun the analysis. Now the expected assocations with EHMT1 are retrieved, one of the genes responsible for the 9qter deletion phenotype.</p>

<!-- MANAGING LITERATURE -->
<p class=nadruk>13: Adding And Browsing Literature</p>
<p style='padding-left:15px;'>Associating literature to certain CNVs might be usefull when other users hava access to your data, and might not have the same knowledge on a certain phenotype-genotype correlation. Publications are linked to CNVs based on the PubMed-ID, since these numbers are unique and stable. </p>
<p style='padding-left:15px;'>Add the publication 'Subtelomeric deletions of chromosome 9q: a novel microdeletion syndrome' to the 9qter deletion. To do so, go to <a href='http://www.ncbi.nlm.nih.gov/pubmed' target='_blank'>PubMed</a> and search for publications on the 9qter deletion. Use for example the terms '9q AND subtelomeric AND deletion AND mental retardation'. Click on the title to read the abstract. The PubMed-ID is located below the abstract, formed by the numbers after 'PMID:', in this case '15264279'. Place this number in the input field on the CNV-WebStore page and click 'Add Publication'. The publication will now be shown the input field. </p>
<p style='padding-left:15px;'>To remove a publication, go to the 'Browse Associated publications' link from a region context menu and click 'Remove' for the relevant publication. 

<!-- GENES -->
<p class=nadruk>14: Browsing Gene Details</p>
<p style='padding-left:15px;'>By clicking the number of affected genes for a specific CNV, the list of affected genes is presented coupled with further links for downstream analysis. The page consists of a table with the Gene Symbol, a P-Value in case the genes are prioritised, the full gene name, OMIM/MORBID data if available and finally links to public databases. Hover the mouse over the database links to get some information on each resource.</p>
<p style='padding-left:15px;'>To change this list of resources, go to '<a href='index.php?page=settings&type=plots&st=publ&v=p' target='_blank'>Profile->Plot Settings->Public Databases</a>'. Additional resources can be added by using the 'NEW' link on this page, for the relevant section (symbol/region based).  

<!-- CUSTOM BOOKMARKS -->
<p class=nadruk>15: Creating Custom Bookmarks</p>
<p style='padding-left:15px;'>Bookmarks for use in GenomeStudio can be recreated by going to '<a href='index.php?page=projects&type=Accessible' target='_blank'>Profile->My Projects->All Illumina Projects</a>'. This page contains a list of all projects you have access to. Click the 'XML' link for the 'DemoProject'. Here fill in any criteria the CNVs should conform to and click 'Create Bookmarks'. For example, set the minimal number of SNPs to 20. This will cause the 10qter deletion to be absent for in the resulting XML file. Once created, click the link presented above the table to download the file.</p>

<!--
step 21: sharing
-->
</div> 

