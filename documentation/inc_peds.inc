<div class=sectie>
<h3>Provide Family Information for CNV analysis</h3>
<h4>During of after analysis</h4>

<p>Providing Family information allows easy checking of observed copy number variant regions in parental data. The process is fairly simple and the information can be added during or after initial CNV analysis. One file is mandatory, a second input file is optional but provides very useful information.</p>

<span class=nadruk>File 1: Family Information</span>
<p>
<a href='images/content/pedfile.png' rel=lightbox><img src="images/content/pedfile.png" class='kader_links' style='width:25%' /></a>
<br/>
This file should contain three columns seperated by tabs, specifying the child in the first column, and parents in column 2 and 3. The order is arbitrary, gender will be retrieved from the database. The exact same sampleID's should be used that were used during CNV-analysis. The headerline is also mandatory.
<br/>
<br/>
An example is shown on the left, provding info on two siblings (C_id1 & C_id2), having the same parents, and a second trio on the last line. Another example can be found <a href='files/pedigree.example.txt'>here</a>.
<br/>
<br/>
When all samples are present in the same GenomeStudio project, these files can be extracted using the 'Analysis'->'Edit Parental Information' menu. Insert the relations here, and export the table.  
<br style='clear:both;' /> 

<p>
<span class=nadruk>File 2: Genotype Information</span>
</p>
<p>
This file contains the raw data used for CNV-analysis. If available, provide the file with data for the parents. In this case, probe level data will be stored for regions called as aberrant in the child, even if it was not called in the parent. For documentation on preparing these files, see <a href='index.php?page=documentation&s=run'>here</a>.
</p>
<p>
<span class=nadruk>Result: Example</span>
<p>
<a href='images/content/pedoffspring.png' rel=lightbox><img src="images/content/pedoffspring.png" class='kader_links' style='width:25%' /></a>
<a href='images/content/pedparent.png' rel=lightbox><img src="images/content/pedparent.png" class='kader_links' style='width:25%' /></a>
<br/>
Shown on the left is an example on how this parental information is presented.  For All CNV's detected in the child, the parental data is checked.  When visualising a CNV this data is accessible through extra links on top of the popup windows (red boxes).  When parental data is showed, the boundaries as known in the child are plotted over the actual data (red bars, indicated by the extra red hook), together with summarised data on the child and parent experimental data. <br/>
<br/>
Besides graphical overview, it also becomes possible to determine the parent of origin of <span class=italic>De Novo</span> aberrations.  To do so follow th link on the CNV details plot (not shown here), or from context menus when browsing in tabular mode.  The link will only appear for aberrations with CopyNumber one to three, where parental data is available. <br/>
<br style='clear:both;' />
</p>  
</div>
