<?php

if ($_GET['t'] == 'main' || $_GET['t'] == '') {
	echo "<div class=sectie>";
	echo "<h3>Browsing and Managing Your Data</h3>\n";
	echo "<p>The data are stored in a central database, and can be represented or browsed in multiple ways. Each way has a specific goal, and is briefly discussed here</p>\n";
	echo "<p><span class=nadruk>Browse Projects</span> : <a href='index.php?page=documentation&s=results&t=tree'>Documentation</a></p>\n";
	echo "Each sample is contained in a 'project', most likely corresponding to one lab-experiment. These projects are bundled on the type of lab or research topic. Using a Lab-project-sample representation, you can easily browse through all results, if you known what your looking for. Results from a specific project can then be visualised graphically, or can be further browsed on a per-sample basis.</p>\n";
	echo "<p><span class=nadruk>Search on Sample</span> : <a href='index.php?page=documentation&s=results&t=sample'>Documentation</a></p>\n";
	echo "<p>If you know the sample ID, but not the project, or you want to check if a certain sample is analyzed in multiple projects, you can retrieve this information here. From the search results, a directly link to the sample details is provided\n</p>";
	echo "<p><span class=nadruk>Search on Region/Gene</span> : <a href='index.php?page=documentation&s=results&t=region'>Documentation</a></p>\n";
	echo "<p>Searching is possible on UCSC-style region (chr7:21,741,335-32,487,687), cytoband notation (12q13-q14) or on RefSeq gene symbol (ATRX). When a cytoband is not found, an educated guess is made for corresponding bands. Also, if multiple transcripts exist for a single gene, or when wildcarts (*) are used, the user will be prompted to select one. \n</p> ";
	echo "<p><span class=nadruk>Search on phenotype</span> : <a href='index.php?page=documentation&s=results&t=pheno'>Documentation</a></p>\n";
	echo "<p>Using the London Neurogenetics Database Ontology, phenotypic information can be associated to the analysed samples. Searching on these phenotypes provides a powerfull tool to group samples and check if there are common aberrant regions.</p>";
	echo" <p><span class=nadruk>Permissions</span> : <a href='index.php?page=documentation&s=results&t=illumina'>Documentation</a></p>\n";
	echo "Manage, delete and join your projects, share or unshare them with other users, or usergroups. You can specify specific permissions when sharing with single users. Multiple projects can also be grouped into a single new one, and obsolete projects can be deleted.</p>\n";
	echo "<p><span class=nadruk>Bookmark Files</span> : <a href='index.php?page=documentation&s=results&t=xml'>Documentation</a></p>\n";
	echo "<p>Bookmark files are xml files that can be imported into BeadStudio/GenomeStudio. These files are kept for about 6 months on the server and can be downloaded from here. (section only accessible from the menu on the left)\n</p> ";

	echo "</div>";
	echo "<div class=sectie>";
	echo "<h3>General Overview</h3>";
?>
<p class=indent>Results that have been analysed on this website can be browsed and searched in multiple ways. Browsing 'majority vote' results will be explained first, since these data can be visualised in the largest variety of ways. </p>

<p class=indent>Other projects, like single algorithm analyses or uploaded custom CNV-reports will be discussed below. These methods are less advanced and consist in general of a tabular listing of results with links to some downstream tools.</p>
<p><span class=nadruk>1: Accessing The Graphical Project Overview</span></p>
<p><span class=italic>a) From the runtime progress monitor</span></p>
<p class=indent><a href='images/content/runstatus.png' rel=lightbox><img src="images/content/runstatus.png" class="kader_links" style='width:45%'/></a>
<br />
Even while a project is not yet finished, it is already possible to check the preliminary results by clicking on the 'Results So Far' button, shown in the image on the left. A similar button. <br/>
<br />
Using the links at the top labeled 'Progress Monitor' and 'Project Overview', toggling between the graphical overview and following progress using the program output log, is possible. <br />
<br style="clear:both;">
</p>
<p><span class=italic>b) From the project tree</span></p>
<p class=indent><a href='images/content/browsetree.png' rel=lightbox><img src="images/content/browsetree.png" class="kader_links" style='width:45%'/></a>
<br />
The project tree is accessible from the menu on the left, under CNV-results. You will be presented with dropdown menu where you can select a project or sample you have access to. Options can be narrowed down by selecting a collection. <br />
<br />
If a project is selected, a button appears labeled 'Show Project Overview'. This will lead you to the graphical representation of the projects results.<br />
<br style="clear:both;">
</p>
<p><span class=nadruk>2: Using The Graphical Project Overview</span></p>
<p class=indent><a href='images/content/graphicoverview.png' rel=lightbox><img src="images/content/graphicoverview.png" class="kader_links" style='width:45%'/></a>
<br />
The graphical project overview presents multiple data sources in a easy to interpret way.  Each of the labeled components gives access to specific functions and tools and is explained below.<br/>

<br style='clear:both;'>
</p>
<p><span class=italic>a) Chromosome picker</span></p>
<p class=indent>Results are displayed in a per-chromosome way. Switch chromosomes using these links.</p>
<p><span class=italic>b) Navigation tools</span></p>
<p class=indent>Basic navigation tools are provided, allowing to zoom in and out and sliding along the chromosome when the image is zoomed in.</p>
<p><span class=italic>c) Genes Panel</span></p>
<p class=indent><a href='images/content/geneTT.png' rel=lightbox><img src="images/content/geneTT.png" class="kader_links" style='width:45%'/></a>
<br />
Genes are represented by rectangles corresponding to the genomic positions. The color codes are red for genes listed in the MORBID database, blue for genes listed in OMIM and black for genes that are listed in neither of the two. When you hover a specific gene, you will be presented with a popup window as shown on the left.  <br />
<br />
These popups provide you with the gene name, strand or transcription orientation and some external links. To access those links, single click with your mouse, when a popup is shown, to fix this popup. <br />
<br />
When a gene is listed in the MORBID map, the identifier will be presented as link to the relevant MORBID page, together with a short description of the associated phenotype.<br />
<br style='clear:both;'></p>
<p><span class=italic>d) Sample Level CNVs</span></p>
<p class=indent><a href='images/content/cnvTT.png' rel=lightbox><img src="images/content/cnvTT.png" class="kader_links" style='width:45%' /></a>
<br />
Below the genes panel, an overview of the called CNVs is presented. The color codes are as follows: <br />
&nbsp;&nbsp; - CopyNumber = 0 : Red<br/>
&nbsp;&nbsp; - CopyNumber = 1 : Purple<br />
&nbsp;&nbsp; - CopyNumber = 2 : Orange (only on X)<br />
&nbsp;&nbsp; - CopyNumber = 3 : Green<br/>
&nbsp;&nbsp; - CopyNumber = 4 : Blue<br />
<br/>
Hovering the mouse over a CNV shows the main context information of this CNV. An example is shown on the left. You can fixate the position and access links in this popup by left-clicking once. <br />
<br />
The genomic position of the CNV is shown on top. Below on the left are characteristics of the CNV itself,: size, copynumber, diagnostic class and inheritance (if specified), number of probes affected, the number of affected genes and the scores of the calling algorithms. <br/>
<br/>
On the right, some information on the sample is presented, like the sample identifier and gender. The used chiptype is also listed, together with the total number of called CNVs. When lower data quality was detected, this will also be mentioned. 
<br/>
<br/> 
On the bottom half of the popup, the LogR and B-allele frequency data are plotted for visual examination of the CNV. Red probes are non-polymorphic probes, while black probes are true SNP-probes. For inspection of the boundaries, 250 Kb in both 5' and 3' direction is added to the plots. <br />
<br />
For further inspection, links to the full sample results and gene content are provided by the information on top. To access these links, fixate the popup with a single left-click of the mouse.<br/>
<br style='clear:both;'></p>
<p><span class=italic>e) Decipher</span></p>
<p class=indent><a href='images/content/DecipherSyndrome.png' rel=lightbox><img src="images/content/DecipherSyndrome.png" class="kader_links" style='width:45%' /></a>
<br />
The publicly available information from DECIPHER is split into two seperate entries. First, known microdeletion/duplication syndromes are aligned against the region in focus. The same color codes as for sample data apply here. Hovering an entry provides some phenotype information on the syndrome, together with a link to the DECIPHER entry page. <br/>
<br/>
Overlapping case level data from DECIPHER are grouped together in grey location indication bars. Hovering these will show a table with the number of occurences for different types of entries, together with a link to phenotype info. <br/>
<br/>
The different types of events are coded as: 
&nbsp;&nbsp; - del: Deletion <br/>
&nbsp;&nbsp; - ins: Insertion/Duplication<br/>
&nbsp;&nbsp; - de_novo_balanced : De Novo Balanced Translocation<br/>
&nbsp;&nbsp; - familial_balanced : Familial Balalnced Translocation<br/>
&nbsp;&nbsp; - de_novo_unbalanced : De Novo Unbalanced Translocation<br/>
&nbsp;&nbsp; - familial_unbalanced : Unbalanced From Familial Balanced<br/>
&nbsp;&nbsp; - inversion : Inversion <br/>
&nbsp;&nbsp; - unknown: Not Specified<br/>
<br/>
Inheritance is classified as: 
&nbsp;&nbsp; - DN: De Novo<br/>
&nbsp;&nbsp; - IFA : Inherited From Affected<br/>
&nbsp;&nbsp; - IFU : Inherited From Unaffected<br/>
&nbsp;&nbsp; - UI : Unknown Inheritance<br/>
&nbsp;&nbsp; - TR : Contains all translocation entries (see above)<br/>
<br style='clear:both;'/>
<?php
}
else{
	include('documentation/inc_results_'.$_GET['t'].'.inc');
}	
