<div class=sectie>
<h3>Manage Illumina Data Projects</h3>
<h4>Share, Merge and Delete</h4>
<p>To keep your data structured, it might be neccessary once in a while to rename, join or delete projects.  To make the most out your data on the other hand, it is often beneficial to allow multiple users to annotate the data.  From the 'Permissions' pages these actions are performed and each is explained below. </p>

</div>

<?php
// include sharing projects doc
include('inc_shareprojects.inc');
?>

<div class=sectie>
<h3>Join Projects</h3>
<p>When joining multiple projects, the following conditions need to be taken into account: 
<ol>
<li>All project must be performed using the same Chiptype
<li>Samples can not be included in more than one of the projects to join (double samples in a project are not allowed)
<li>Be aware that users with access to a subset of the projects, will gain access to the complete new project !
</ol>
When the criteria are met, you will need to specify a new collection and projectname.</p>

</div>

<div class=sectie>
<h3>Deleting projects</h3>
<p>When deleting a project, the system checks if the same sample is present in different projects. If the sample is found and results from the current project were used in searching, you will be presented with the option to include the sample again in the search results using the alternative project as a source.  Be carefull when deleting projects, this process can not be turned back !</p>

</div>
