<div class=sectie>
<h3>Search Sample by phenotype</h3>
<p>Phenotypic information can be associated to samples using the London Neurogenetics Database ontology. Because this is a very structured methods, it allows for powerfull filtering and searching based on phenotype. The different methods to add and bundle criteria are discussed below.</p>

<p><span class=nadruk>1: Adding And Removing Criteria</span></p>
<p><span class=italic>1.A) Adding criteria from a textfield</span></p>
<p class=indent><a href='images/content/searchaddphenotext.png' rel=lightbox><img src="images/content/searchaddphenotext.png" class="kader_links" style='width:45%'/></a>
<br />
The default method to add criteria to the search is by typing them into the textfield. When at least 2 characters are typed, a drop-down list with suggestions appears. The number of suggestions using the currently supplied text is limited to eight. Keep typing to narrow the options down, the list will be updated while you type.
<br/>
<br/>
If the item you are looking for appears, select it by using the mouse, or go down with the arrow keys. Add the item you need by clickingit and selecting 'Add' afterwards, or by using the enter key after selecting it from the list. 
<br/>
<br/>
If there is only one suggestion appearing, it is allowed to press enter without selecting or completing the text. If multiple items remain when submitting the form, you will be notified that this is not allowed.
<br/>
<br style='clear:both;'>

<p><span class=italic>1.B) Adding criteria from the Ontology Tree</span></p>
<p class=indent><a href='images/content/searchaddphenotree.png' rel=lightbox><img src="images/content/searchaddphenotree.png" class="kader_links" style='width:45%'/></a>
<br />
Another option is browsing the ontology tree top-down. To access the tree, follow the 'Alternative: ...' link. The two methods can be switched without losing information. 
<br/>
<br/>
To add criteria, click a node in the tree. This will show subnodes, together with a green 'plus' sign. Click this sign to add the item, or click the item itself to show the next level of items.
<br/>
<br style='clear:both;'/>
<p><span class=italic>1.C) Removing Criteria from the selection</span></p>
<p class=indent><a href='images/content/searchphenofilled.png' rel=lightbox><img src="images/content/searchphenofilled.png" class="kader_links" style='width:45%'/></a>
<br />
Selected criteria are shown in a table under the selection forms.  To remove items, simply press the little trash bin on the left side. 
<br/>
<br/>
There is also a 'Clear All' Button provided to remove all items at the same time.
<br/>
<br/>
Search results are updated each time you add or remove an item.
<br/>
<br style='clear:both;'>
</p>

<!-- Combining the data -->
<p><span class=nadruk>2: Combining Criteria and filtering results</span></p>
<p><span class=italic>2.A) Combining Criteria</span></p>
<p class=indent>There are two options to combine the criteria. If you pick 'ANY', samples matching any of the criteria will show up, ordered according to a descending number of matching criteria. For 'ALL', only samples matching all criteria will show up, ordered according to a descending ratio of matched over total phenotypic items. </p>
<p><span class=italic>2.B) Filter CNVs</span></p>
<p class=indent>For samples matching the criteria, an overview is produced of the associated CNVs. It is then possible to select the diagnostic classes of CNVs that should be included.  Only including class 1 to 3 limits the output and makes comparing the CNVs more straightforward and relevant.</p>

<!-- Results -->
<p><span class=nadruk>3: Results</span></p>
<p class=indent>Results are presented confirming to the settings described above. For each matching sample the sampleID is shown together with the gender and the project it's included in.  Next the phenotypic description is shown, where matching criteria are shown in bold.  If there are CNVs present for the sample that passed filtering on class, these will be shown on a karyogram and in a table showing region, class and inheritance. A link is also provided to go to the sample details page.
</p>
</div>
