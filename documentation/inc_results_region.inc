<div class=sectie>
<?php
if ($_GET['s'] == 'results') {
	echo"<h3>Search on Region / Gene </h3>";
}
else {
	echo "<h3>Graphical Project Overview</h3>";

?>
<p><span class=nadruk>1: Accessing The Graphical Overview</span></p>
<p><span class=italic>a) From the runtime progress monitor</span></p>
<p class=indent><a href='images/content/runstatus.png' rel=lightbox><img src="images/content/runstatus.png" class="kader_links" style='width:45%'/></a>
<br />
While a project is still running, it is already possible to check the preliminary results by clicking on the 'Results So Far' button, shown in the image on the left.This button will also be present when the analysis is finished.<br/>
<br />
Using the links at the top labeled 'Progress Monitor' and 'Project Overview', toggling between the graphical overview and following progress using the program output log, is possible. <br />
<br style="clear:both;">
</p>
<p><span class=italic>b) From the project tree</span></p>
<p class=indent><a href='images/content/browsetree.png' rel=lightbox><div style='margin-left:15px;margin-right:5px;float:left;overflow:hidden;height:150px;width:45%;border:solid 1px black'><img src="images/content/browsetree.png" class="kader_links" style='width:100%'/></div></a>
<br />
The project tree is accessible from the menu on the left, under CNV-results. You will be presented with dropdown menu where you can select a project or sample you have access to. Options can be narrowed down by selecting a collection. <br />
<br />
If a project is selected, a button appears labeled 'Show Project Overview'. This will lead you to the graphical representation of the projects results. This is further explained <a href='index.php?page=documentation&s=results&t=tree'>here</a><br />
<br style="clear:both;">
</p>
<!-- DOCs on using the graphical interface !-->
<p><span class=nadruk>2: Using The Graphical Project Overview</span></p>
<?php
}
?>
<p></p><p class=indent>
<a href='images/content/graphicalbrowser.png' rel=lightbox><img src="images/content/graphicalbrowser.png" class="kader_links" style='width:45%'/></a>
<br/>
The graphical project overview presents multiple data sources in a easy to interpret way.  Each of the labeled components gives access to specific functions and tools and is explained below. Depending on whether you are visualising results from a single project, or from a region/gene bases search, different content will be shown on the top half of the page. Browsing a single project offers a 'chromosome picker', a list of all chromosomes, for switching to another chromosome overview for this project. Browsing region / gene search results presents the search box on top, together with some query information.  If a provided cytoBand was invalid for example, the guessed corrections will be shown.
<br/>
<br/>
Many of the tracks described below can be customised by the user. To do so, go to 'preferences', in the top right corner of the image. All settings are documented <a href='index.php?page=documentation&s=settings&t=plots'>here</a> 
</p>
<p class=indent>
<br style='clear:both;' />
<p><span class=italic>A) Navigation tools</span></p>
<p class=indent>
Basic navigation tools are provided, allowing to zoom in and out and sliding along the chromosome when the image is zoomed in.
Clicking on the chromosome plot allow a direct jump to the select cytoband.
</p>
<p><span class=italic>B) Genes Panel</span></p>
<p class=indent><a href='images/content/geneTT.png' rel=lightbox><img src="images/content/geneTT.png" class="kader_links" style='width:45%'/></a>
Genes are represented by lines corresponding to the genomic positions. On these lines, arrows indicate the transcription direction and exons are marked by solid rectangles. The color codes are red for RefSeq genes listed in the MORBID database, blue for genes listed in OMIM and black for genes that are listed in neither of the two. The next two tracks represent Coding and non-coding transcripts from the Encode project. 
<br/>
When you hover a specific gene, you will be presented with a popup window as shown on the left.  
These popups provide you with the gene name, strand or transcription orientation, links to public databases and some phenotype information when this is present (from MORBID). To access the links, single click with your mouse, when a popup is shown, to fix this popup. Each user can select the public resources to be shown, and can make new resources available to all users. This is described <a href='index.php?page=documentation&s=settings&t=plots&st=publ'>here</a>.<br />
<br />
<br style='clear:both;'></p>
<p><span class=italic>C) Segmental Duplications Panel</span></p>
<p class=indent><a href='images/content/segdup.png' rel=lightbox><img src="images/content/segdup.png" class="kader_links" style='width:45%'/></a>
<br />
Segmental duplications are regions with high sequence similarity dispersed around the genome, often mediating recombination events. They are represented in the browser by black rectangles and hovering them provides the user with a popup as shown on the left. 
<br/>
<br/>
The original region, currenlty under selection is shown on top.  The table below this shows all regions with similar sequence, with the similarity percentage and a relative position plot. The darker this plot, the higher the similarity.  All data are extracted from the GenomicSuperDups table in the UCSC browser. 
<br/>
<br style='clear:both;'>   

<p><span class=italic>D) Sample Level CNVs</span></p>
<p class=indent><a href='images/content/cnvTT.png' rel=lightbox><img src="images/content/cnvTT.png" class="kader_links" style='width:45%' /></a>
<br />
Below the genes panel, an overview of the called CNVs is presented. The color codes are as follows: <br />
&nbsp;&nbsp; - CopyNumber = 0 : Red<br/>
&nbsp;&nbsp; - CopyNumber = 1 : Purple<br />
&nbsp;&nbsp; - CopyNumber = 2 : Orange (only on X)<br />
&nbsp;&nbsp; - CopyNumber = 3 : Green<br/>
&nbsp;&nbsp; - CopyNumber = 4 : Blue<br />
<br/>
Hovering the mouse over a CNV shows the main context information of this CNV. An example is shown on the left. You can fix the position and access links in this popup by left-clicking once. <br />
<br />
The genomic position of the CNV is shown on top. Below on the left are characteristics of the CNV itself,: size, copynumber, diagnostic class and inheritance (if specified), number of probes affected, the number of affected genes and the scores of the calling algorithms. <br/>
<br/>
On the right, some information on the sample is presented, like the sample identifier and gender. The used chiptype is also listed, together with the total number of called CNVs. When lower data quality was detected, this will also be mentioned. 
<br/>
<br/> 
On the bottom half of the popup, the LogR and B-allele frequency data are plotted for visual examination of the CNV. Red probes are non-polymorphic probes, while black probes are true SNP-probes. For inspection of the boundaries, 250 Kb in both 5' and 3' direction is added to the plots. <br />
<br />
For further inspection, links to the full sample results and gene content are provided by the information on top. To access these links, fix the popup with a single left-click of the mouse.<br/>
<br/>When familial information was provided, extra buttons will appear to show the parental data for the region under review. See <a href='index.php?page=documentation&s=peds'>here</a> for more details on adding familial information.
<br/>
<br style='clear:both;'></p>
<p><span class=italic>E) Collapsed Results</span></p>
<p class=indent>When a lot of data is stored on the platform, results might become cluttered in polymorphic regions. To avoid this, a user might decide to group data together based on diagnostic class.  In most cases it is recommended to group class 4 (known benign variants) together, and sometimes also CNVs that have not (yet) been classified. When hovering these data, a popup will show the exact regions, samples and classes of the CNVs included in the summary plots.</p>
<p><span class=italic>F) Custom Dataset Results</span></p>
<p class=indent>Custom data can be parsed and stored on the platform (see <a href=index.php?page=documentation&s=custom_methods'>here</a>). Hovering these results in the graphical overview will show similar popups as the ones illustrated in section D, but without the probelevel data plots.</p>

<p><span class=italic>G) Decipher</span></p>
<p class=indent><a href='images/content/DecipherSyndrome.png' rel=lightbox><img src="images/content/DecipherSyndrome.png" class="kader_links" style='width:45%' /></a>
<br />
The publicly available information from DECIPHER is split into two seperate entries. First, known microdeletion/duplication syndromes are aligned against the region in focus. The same color codes as for sample data apply here. Hovering an entry provides some phenotype information on the syndrome, together with a link to the DECIPHER entry page. <br/>
<br/>
Overlapping case level data from DECIPHER are grouped together in grey location indication bars. Hovering these will show a table with the number of occurences for different types of entries, together with a link to phenotype info. <br/>
<br/>
The different types of events are coded as: 
&nbsp;&nbsp; - del: Deletion <br/>
&nbsp;&nbsp; - ins: Insertion/Duplication<br/>
&nbsp;&nbsp; - de_novo_balanced : De Novo Balanced Translocation<br/>
&nbsp;&nbsp; - familial_balanced : Familial Balalnced Translocation<br/>
&nbsp;&nbsp; - de_novo_unbalanced : De Novo Unbalanced Translocation<br/>
&nbsp;&nbsp; - familial_unbalanced : Unbalanced From Familial Balanced<br/>
&nbsp;&nbsp; - inversion : Inversion <br/>
&nbsp;&nbsp; - unknown: Not Specified<br/>
<br/>
Inheritance is classified as: 
&nbsp;&nbsp; - DN: De Novo<br/>
&nbsp;&nbsp; - IFA : Inherited From Affected<br/>
&nbsp;&nbsp; - IFU : Inherited From Unaffected<br/>
&nbsp;&nbsp; - UI : Unknown Inheritance<br/>
&nbsp;&nbsp; - TR : Contains all translocation entries (see above)<br/>
<br style='clear:both;'/>
<p><span class=italic>H) HapMap Data</span></p>
<p class=indent>All HapMap samples analysed by Illumina to generate the cluster files, are publicly available. We analysed these data and included them in the project as control populations. Two tracks are included in the browser, one containing all CNVs observed in the same set of chips used to generate the user's results in the region under review, and one containing CNVs observed using ANY of the available chiptypes. 
<br />
<br /> 
Hovering provides a similar popup as the one illustrated in section C.  For each HapMap sample, the samplename is provided, the chiptype on which the CNV was detected, and a relative position plot.</p>
<p><span class=italic>I) Toronto Database Of Genomic Variants</span></p>
<p class=indent>The Toronto DGV is a large public collection of CNVs observed in normal cohorts. Similar to the HapMap samples, this dataset is visualised in a summarised way, as a control cohort. <br />
<br /> 
If wanted, the user can specify which studies should be included in the track (see <a href='index.php?page=documentation&s=settings&t=plots&st=con'>here</a>).
</p>
</div>


