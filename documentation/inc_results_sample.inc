<div class=sectie>
<h3>Search Results by Sample ID</h3>
<p>To optimise search results, we strongly recommend naming all your samples using a standard convention of 'XXXX_YYYYYY', where XXXX are the four last digits of the array barcode, and YYYYYY is a custom identifier (eg DNA-number). </p>
<p>
<a href='images/content/searchsample.png' rel=lightbox><img src='images/content/searchsample.png' class='kader_links' style='width:45%' /></a> 
<br/>
To start, enter a sample identifier in the text field and press search. Wildcards are automatically added. If appropiate, select the 'include custom datasets' checkbox. This will enable searching in custom data projects. By default, only samples analysed on the platform itself are searched. 
<br/>
<br/>
Results are presented grouped by Sample ID. If a specific sample is present in multiple projects, these will be listed together, as shown on the left. As can be seen from the example, we searched for a sampleID, that was repeated in a second experiment (different chiptype-part).  By selecting the 'Details' link, the full overview of the sample in the selected project is presented. </p>
<br/>
<br style='clear:both' />
</p>
</div> 
