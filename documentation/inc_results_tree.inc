<div class=sectie>
<h3>Browse Projects</h3>
<p>
All the analysis results are ordered in sort of an hierarchical tree-structure.  By narrowing down the criteria, samples from a specific experiment can be browsed and some summary statistics are shown on the projects or samples under review. Below, some of the key items to use this representation are described.
</p>

<p>
<span class=nadruk>Initial display: no selection made</span></p>
<p>
<a href='images/content/browsetreeclear.png' rel=lightbox><img src='images/content/browsetreeclear.png' class='kader_links' style='width:45%' /></a>
To start browsing, select a collection.  This narrows down the projects in the dropdown menu for further selection. Alternatively, click on one of the rows in the table on the lower half of the page. This table represents the ten most recent projects you have access to, together with the number and type of samples in the experiment.
<br style='clear:both'>
</p>
<p>
<span class=nadruk>After Selecting a project</span></p>
<p>
<a href='images/content/browsetree.png' rel=lightbox><img src='images/content/browsetree.png' class='kader_links' style='width:45%' /></a>
<br />
After selecting a project, some extra functions are shown.  First of all, the list of samples in the drop down menu is further restricted. Select one and press 'Show Sample Details' to browse that sample's details in a tabular fashion. More on that can be found <a href='index.php?page=documentation&s=details'>here</a>.
<br/>
<br/>
'Show Project Overview' leads to a graphical summary of the project. This overview contains all CNV's on a per chromosome basis, in the context of gene content, control samples, known pathogenic regions etc. From here links lead to multiple public resources, sample details pages, and more. See <a href='index.php?page=documentation&s=results&t=region'>here</a> for more details, since the interface is equal to the inferface for region/gene based searching. 
<br/>
<br/>
'Share This Project' is only visible if the selected project was created by the current user. The link leads to the project management site described <a href='index.php?page=documentation&s=shareprojects'>here</a>.
<br/>
<br />
An alternative to selecting a sample from the presented dropdown list, is selecting them from the table presented on the lower half of the page. This table also gives the number of CNVs in each sample, and the analysis call rate, which is an indication for the quality. <br/>
<br style='clear:both'>
</p>
<p><span class=nadruk>When a Sample is selected</span></p>
<p>When a samples is selected, the table on the lower half of the page shows other projects that contain this specific sample. This might mean the sample was analysed more than once, or that the sample was repeated on a second array. </p>
</div> 
