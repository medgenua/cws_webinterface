<div class=sectie>
<h3>XML File Browser</h3>
<p>This list is mainly a method to simply redownload XML files if they were not downloaded after analysing the data.  For importing XML files into the Illumina Genome Viewer, open the Bookmark viewer and select import. </p>
<p>The details pages give a very basic overview of the regions listed in the XML files, sorted by sample, with links to UCSC and decipher (if cases are present).
</div>
