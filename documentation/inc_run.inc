<div class=sectie>
<h3>Setting Up and Running a CNV-analysis</h3>

<p> Running a CNV analysis on this platform takes roughly 3 steps. We will go through all of them here. The instructions are similar for running the seperate algorithms or for running a standard combined analysis.</p>

<span class=nadruk>Step 1: Generating the Input Files</span>

<p>There are two files that need to be created. First, a file containing sample genders, identifiers, and some other information, and second, a file containing the actual intensity, and genotype data. When you have access to BeadStudio or GenomeStudio, these files can easily be extracted. When you only have Full Call Reports available, you will need to reformat those using provided scripts.<p>

<p>NOTICE: samplenames should only contain alphanumeric symbols (AZ09_) !</p>

<span class=italic>a) Extract From BeadStudio / GenomeStudio</span>
<p>
<a href='images/content/BeadStudio_Main.png' rel=lightbox><img src="images/content/BeadStudio_Main.png" class="kader_links" style='width:45%'/></a>
<br />
Open the column chooser (<a href='images/content/columnFDT.png' rel=lightbox>Screenshot</a>) for the 'Full Data Table' ('1' in the image on the left). <br />Retain only the following 'Displayed Columns': <br /> - Name <br> - Chr <br /> - Position <br /> - Sample Names to be included <br /> Next set the following 'Displayed Subcolumns': <br /> - Log R Ratio <br /> - B Allele Freq <br /> - GType <br />
<br />
Finally, set the following 'Displayed Columns' for the 'Samples Table' using the column chooser (<a href='images/content/columnST.png' rel=lightbox>Screenshot</a>) (under '2') : <br />
- SampleID<br />
- Gender<br />
- Call Rate<br />
- Index<br />
- Array Info.Sentrix ID<br/>
- ArrayInfo.Sentrix Position<br/>
<br />

<br style="clear:both;">
</p>
<br />
<p>
<a href='images/content/BeadStudio_step2.png' rel=lightbox><img src="images/content/BeadStudio_step2.png" class='kader_links' style='width:45%' /></a>
<br />
We suggest to use a uniform sampleID constitution, consisting of the 4 last digits of the chip barcode, followed by an underscore and a sample/DNA identifier. The sample genders can be estimated from the context menu of the Samples table. Call Rates can be calculated from the calculator icon in the Samples Table toolbar.<br />
<br />
Once you have tables like the one shown on the left, use buttons '1' and '2' to select the entire table and export it to a flat text file. Do this for both the Samples Table and the Full Data Table. When prompted to export the entire table instead of only the selected rows and columns, select 'No'. <br />
<br />
Make sure no single columns are selected, since this might lead to incomplete files.  When a single column is selected, this is indicated by a darkened column header and yellow cells (<a href='images/content/colselected.png' rel=lightbox>Screenshot</a>). 

<br style='clear:both;' />
</p>
<p>
<br />
<span class=italic >b) Compile from Full Call Reports</span>
</p>
<a href='images/content/reformat.png' rel=lightbox><img src='images/content/reformat.png' class='kader_links' style='width:45%' /></a>
<br /> 
When you have recieved Full Call Reports (FCR's) from a core facility, you can reformat these results using a few simple scripts using a UNIX commanline, as illustrated in the image on the left.<br />
<br />
First, download the scripts <a href=files/FCR2BS.zip>here</a>. Extract them into a folder containing the FCR's. Next reformat each FCR to a tabular format using the step1 script (see syntax below). The 'NON-POLY.txt' file is a file containing non-polymorphic probes for the used chiptype. These lists can be obtained <a href='index.php?page=chiptypes' target='_blank'>here</a>.
<br />
<br />
We will now combine these output files with the probe details.  If this information is not provided with the FCR's, you can obtain it <a href='index.php?page=chiptypes' target='_blank'>here</a> . Use the step2 script (syntax below), where infiles are the output files from step 1:<br />
<br />
Finally, extract the samples table using the step3 script:
<br />
<br /> 
Notice: All input files should be converted during the process to UNIX-format. If your output files seem incorrect please use dos2unix command or similar to achieve this manually!
</p>
<br style='clear:both;' />
<center><span class=italic>perl step1_create_tabfile.pl NON-POLY.txt INPUTfile OUTPUTfile</span></center>
<center><span class=italic>perl step2_combine_files.pl positions.txt INfiles OUTfile</span></center>
<center><span class=italic>perl step3_estimate_genders.pl INfile OUTfile</span></center>
<br />
<br />
</p>
<p>
<div class=nadruk>Step 2: Uploading Your Data</div>
</p>
<p>
To start an analysis, the data has to be sent to the server. If you have a high speed internet connection and the project is small, this can be achieved by the standard analysis form, described below. When you have a slower connection or are uploading large datasets, then you should first upload your data using the seperate upload page, as described <a href='index.php?page=documentation&s=upload'>here</a>.
</p>
<p>
<span class=nadruk>Step 3: Setting Up Analysis Details</span>
</p>
<p> To start an analysis, click the 'New Analysis' entry in the left panel. You will then be presented with the option to run a combined analysis with preset parameter settings, or to run a QuantiSNP, PennCNV or VanillaICE seperately, which allows you to change certain parameters. We will briefly describe each option here.</p>
<p>
It is important to note that only the results from the combined analysis are stored in the database for future reference and will allow searching and sharing of the results. Since the results of each seperate algorithm are also presented as an individual file, we recommend to use this method.
</p>
<p>
<span class=italic>a) Combined analysis</span>
</p>

<p>
<a href='images/content/setup_multi.png' rel=lightbox><img src='images/content/setup_multi.png' class='kader_links' style='width:45%' /></a>
First you need a projectname, which is set by default to the current date and time.  Next, define a group. The options for this originated from some projects we were working on when designing this page: Diagnostics (daily hospital routine), Research_MR (Mental Retardation), Research_EP (Epilepsy), Control (healthy / HapMap samples) and Guest (sporadic users with). If need another option, please feel free to suggest.  
<br />
<br />
Thirdly, the files created in step 1 need to be attached to the project. You can upload data from the 'choose file' fields, or select a previously uploaded datafile from the dropdown lists.  Pedigree information is optional and can be added in a later stage. More information on this can be found <a href='index.php?page=documentation&s=peds'>here.</a>
<br />
<br />
Finally you need to select the correct chiptype.  It is important to select the correct type, because chip-specific lists of non-polymorphic probes and population BAF tables are used.  The option of the asymmetric filter allows you to retain duplications only called by PennCNV but with high confidence (>20) in the majority vote.
<br />
<br style='clear:both;' />
</p>
<!-- 
<p>
<span class=italic>b) QuantiSNP analysis</span>
</p>
<p>
<a href='images/content/setup_qsnp.png' rel=lightbox><img src='images/content/setup_qsnp.png' class='kader_links' style='width:45%' /></a>
<br />
Like described above, you first need to choose a projectname, attach data files and select the correct chiptype. Since searching the results is not possible here, and browsing the results is based on feeding the results file to a parser, you can not select a group here. 
<br />
<br />
Next you can change the QuantiSNP default parameter settings, although it is not recommended.   For reference about the meaning of each parameter you are refered to the QuantiSNP publication (<a href='http://medgen.ua.ac.be/cnv/files/Qsnp_publication.pdf'>here</a>). 
<br />
<br />
In short the EM-iters setting is the number of iterations used in training the model on the experimental data, L-setting is the expected typical number of basepairs in a CNV-region (sort of smoothing factor) and the maxcopy setting is the maximal copy number state value in the markov model. GC-correction is applied by default to smooth out genomic waving and probenames are printed in the results file. 
<br />
<br />
By default a minimal coverage of 10 probes and a Log Bayes Factor (confidence) of 10 is needed for a valid call.
<br />
<br style='clear:both;' />
</p>
<p>
<span class=italic>c) PennCNV analysis</span>
</p>
<p>
<a href='images/content/setup_pcnv.png' rel=lightbox><img src='images/content/setup_pcnv.png' class='kader_links' style='width:45%' /></a>
<br />
Configuration of PennCNV is slightly less complicated because it is a pretrained model that takes few runtime parameters. 
<br />
<br />
As usual, pick a username, attach datafiles and set the correct chiptype. After that, you only need to set the minimal coverage and confidence levels for a valid call. 
<br />
<br />
For more information click <a href='http://www.openbioinformatics.org/penncnv/' target='_blank'>here</a>
<br />
<br style='clear:both;' />
</p>
<p>
<span class=italic>d) VanillaICE analysis</span>
</p>
<p>
<a href='images/content/setup_vice.png' rel=lightbox><img src='images/content/setup_vice.png' class='kader_links' style='width:45%' /></a>
<br />
VanillaICE is an algorithm that was originally written for Affymetrix data. We have tried to costumize it as good as possible to cope with the typical Illumina data (non polymorphic probes, different expected intensity values, etc). 
<br />
<br />
Regular requirements are a project name, datafiles and a chiptype. Next you need to set a smoothing factor and a minimal coverage for default calls. The HMM states type should be left on 'experimental', since the experimental intensity values observed for deletions and duplications differ significantly from the theoretically expected values (eg log2{1/2} for deletion), due to the non-affine normalisations performed by the illumina preprocessing algorithms. 
</br />
<br style='clear:both;' />
<br />
</p>
-->
<p>
<span class=nadruk>Step 4: Checking Submitted Data</span>
</p>
<p>
<a href='images/content/check_data.png' rel=lightbox><img src='images/content/check_data.png' class='kader_links' style='width:45%' /></a>
<br />
When the data are successfully submitted, a page with an overview of the found samples is presented.  Here you can specify for new samples if they should be included in search results (for combined analysis only !).  We recommend to exclude samples with low callrates, since they will contain many false positive calls due to the low data quality. 
<br />
<br />
If a seperate algorithm was started, an overview of the parameters settings will also be shown.<br />
<br />
If there were problems detected with the input files you will recieve an error with some hints on what might be wrong. If all is allright, press 'start analysis' to proceed.
<br />
<br style='clear:both;' />
<br />
</p>
<p>
<span class=nadruk>Step 5: Monitor Progress</span>
</p>
<p>
<a href='images/content/progress.png' rel=lightbox><img src='images/content/progress.png' class='kader_links' style='width:45%' /></a>
<br />
When a project is running you can follow the progress as shown on the left (outdated image).  On top you can press the 'Results so far' button to go to a graphical overview of the results for finished samples. There will also be a link to check the output files for individual jobs for each sample.  
<br />
<br />
The output of the program itself is printed below.  Here you can see which samples are finished, needed GC-correction (which is applied automatically when genomic waving is detected), an how many CNV's were found in each sample.
<br />
<br />
Until the analysis is finished, this page will be refreshed every 15 seconds. Once it's finished, you will be presented with links to the files containing the results and to pages containing detailed overviews of the results. These links will show up at the top of the page.  
<br /><br/>
You can close CNV-WebStore while the analysis is running. An email will be sent to you once the analysis is finished. 
<br /><br/>
Detailed tips and instructions on browsing the results can be found <a href='index.php?page=documentation&s=results&t=main'>here</a>. 
<br />
<br style='clear:both;' />
</p>  
 
</div>
 
