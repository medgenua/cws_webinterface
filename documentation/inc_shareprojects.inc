<div class=sectie>
<h3>Sharing Projects</h3>

<p>
<a href='images/content/sharing.png' rel=lightbox><img src="images/content/sharing.png" class='kader_links' style='width:35%' /></a>
<br/>
The project sharing page is divided in two parts. On top users/usergroups can be selected to allow them access. Below, the list of current users/usergroups with access is shown, with a link to remove their access.
<br/>
<br/>
Projects can be shared in two ways: giving single users access, or giving entire usergroups access. You can only give access to groups that you are a member of. These groups are listed on the left, with a distinction of public an private groups. On the right, single users are listed grouped by affiliation. Multiple users and groups can be selected at once. After selection press 'Share' to continue. 
<br/>
<br/>
When selecting single users, the exact permissions can be set for each user added.  Permissions for usergroups are set by the usergroup administrator. See <a href='index.php?page=documentation&s=group&t=mine'>here </a> for more information.
<br/>
<br/>
To remove access for a certain user, use the trash bin next to the name of the user/usergroup.  

<br style='clear:both'/>
</p>

<p>
<a href='images/content/share_user.png' rel=lightbox><img src="images/content/share_user.png" class='kader_links' style='width:35%' /></a>
<br/>
Three permissions have to be defined when sharing a project to specific users:<br/>
&nbsp; &nbsp; - Edit CNVs: This allows the user to edit CNV annotation: Class, Inheritance, Merging etc.<br/>
&nbsp; &nbsp; - Edit Clinic: This allows the user to change Clinical information (freetext/LNDB)<br/>
&nbsp; &nbsp; - Project Control: Allow a user to share, join or delete projects.<br/>
<br/>
Setting all to 'NO' equals to sharing with read-only access. 

<br style='clear:both'/>
</p>


</div>

