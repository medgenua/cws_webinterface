<div class=sectie>
<h3>Uploading Large Datasets</h3>
<p>When analysing Illumina data, there is a possibility to upload and store your datafiles on the server for future analysis. This is done using FTP, making it less prone to connection failures. 
<p>No further documentation is provided, as the page itself contains all instructions. </p>
<br/>
There is no garantuee that uploaded files are kept longer than 6 months on the server. So make sure you keep a local copy ! Uploading files is also usefull if you plan to analyse your data multiple times. 
<br />
<br style='clear:both;' />
</p>

