<?php
session_start();
if(isset($_GET['sid']) && isset($_GET['pid']))
{
	// if id is set then get the file with the id from database
	#######################
	# CONNECT TO DATABASE #
	#######################
	include('.LoadCredentials.php');
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");
	#echo "db: $db<br/>";
	// GET REQUESTED ITEM DETAILS
	$sid = $_GET['sid'];
	$pid = $_GET['pid'];
	$type= $_GET['type'];
    $path = $plotdir;

    if ($type == 'swgs') {
		$table = 'projsamp';
		$query = "SELECT swgs_plots FROM $table WHERE idsamp = '$sid' AND idproj = '$pid'";
		$result = mysql_query($query) or die('Error, query failed');
		list( $fullfile) =  mysql_fetch_array($result);
	}
	if ($type == 'CNV') {
		$table = 'plots';
		//$path = 'Whole_Genome_Plots/WGP/';
		$query = "SELECT filename FROM $table WHERE idsamp = '$sid' AND idproj = '$pid'";
		$result = mysql_query($query) or die('Error, query failed');
		list( $file) =  mysql_fetch_array($result);
		// COMPOSE FULL FILENAME
		$fullfile = "$path/$file";

	}
	if ($type == 'BS') {
		$table = 'BAFSEG';
		$query = "SELECT filename FROM $table WHERE idsamp = '$sid' AND idproj = '$pid'";
		$result = mysql_query($query) or die('Error, query failed');
		list( $file) =  mysql_fetch_array($result);
		// COMPOSE FULL FILENAME
		$fullfile = "$path/$file";
	}
	if ($type == 'UPD') {
		$table = 'upd_analyses_swgs';
		$query = "SELECT pdf_path FROM `$table` WHERE iid = '$sid' AND ipid = '$pid' ORDER BY `$table`.`id` DESC LIMIT 1";
		$result = mysql_query($query) or die('Error, query failed');
		list($file) =  mysql_fetch_array($result);
		// COMPOSE FULL FILENAME
		$fullfile = "$path/$file";
	}
	if ($type == 'tp') {
		$table = 'triPOD';
	}
	// GET SAMPLENAME FOR USER-FRIENDLY DOWNLOAD FILE NAME
	$squery = "SELECT chip_dnanr FROM sample WHERE id = '$sid' LIMIT 1";
	$result = mysql_query($squery) or die('Error, query failed');
	list($samplename) = mysql_fetch_array($result);
	// GET SYSTEM FILENAME TO UPLOAD
	// $query = "SELECT filename FROM $table WHERE idsamp = '$sid' AND idproj = '$pid'";
	//$result = mysql_query($query) or die('Error, query failed');
	//list( $file) =  mysql_fetch_array($result);
	// COMPOSE FULL FILENAME
	//$fullfile = "$path/$file";
	// UPLOAD
	if (file_exists("$fullfile") && is_readable("$fullfile")) {
		$size = filesize("$fullfile");
		if ($fp = @fopen("$fullfile",'r')) {
			//send headers
			header("Content-type: application/pdf");
			header("Content-Length: $size");
			header("Content-Disposition: attachment; filename=$samplename.pdf");
			// send file contents
			fpassthru($fp);
			//close file
			fclose($fp);
			sleep(1);
			exit;
		}
	}
	else {
		echo "Plot File Not Found. ($fullfile)";
	}
}
?>
