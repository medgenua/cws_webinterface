<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

$ecarucaSize = $_SESSION['ecarucaSize'];
# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$userid = $_GET['u'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$window = $stop-$start+1;


# DEFINE IMAGE PROPERTIES
$querystring = "SELECT COUNT(a.id) AS aantal FROM ecaruca a WHERE a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) AND (stop - start) < $ecarucaSize GROUP BY a.caseid";

$result = mysql_query($querystring);
$nrlines = mysql_num_rows($result);

$height = $nrlines*10 + 20;
	
//$height = 300;
//$height = 60 + $nrabs*10;
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
}
else {
	$scale = 500000;
	$stext = "500 kb";
} 


$scalef = 341/($window);
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];


//Specify constant values
$width = 400; //Image width in pixels
//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$orange = ImageColorAllocate($image,255, 179,0);

$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

/*
# CREATE SCALE
$scaledscale = intval(round($scale*$scalef));
imagerectangle($image, 0, 0, 60, 20, $black);
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($stext)*$fontwidth;
$txtx = 30 -($txtwidth/2);
imagestring($image,3,$txtx,1,$stext,$black);
$xstart = 30-($scaledscale/2);
$xstop = 30+($scaledscale/2);
imageline($image,$xstart,18,$xstart,14,$black);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
*/
$querystring = "SELECT a.start, a.stop, a.caseid FROM ecaruca a WHERE a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop')) AND (stop - start) < $ecarucaSize ORDER BY a.caseid, a.start";
//echo "<div class=nadruk>Currently Reformatting !</div>\n";
$result = mysql_query($querystring);
$csample = "";
$cct = "";
//echo "<ul id=ul-simple>\n";
$y = 5;
$cy = $y;
$xoff = 51;
while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$caseid = $row['caseid'];
	if ($caseid != $csample) {
		if ($csample != '') {
			imagestring($image,1,6,($cy+$y)/2-1,"$csample",$black);
			imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
		}
		
		//echo "</ul>\n$chip_dnanr<ul id=ul-simple>\n";
		$csample = $caseid;
		$cy = $y+14;
		$y = $y +10;
		$cct = '';
	}
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	imagefilledrectangle($image,$xoff+$scaledstart,$y+4,$xoff+$scaledstop,$y+6,$gpos50);
}

imagestring($image,1,6,($cy+$y)/2-1,$csample,$black);

# Draw the table borders
imagestring($image,2,4,1,"Case ID",$gpos75);
#imagestring($image,2,50,1,"ChipType",$gpos75);
imagefilledrectangle($image,0,0,0,$height-5,$gpos50);
imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,$height-5,$width-1,$height-5,$gpos50);
imagefilledrectangle($image,$width-1,0,$width-1,$height-5,$gpos50);
imagefilledrectangle($image,47,0,47,$height-5,$gpos50);
#imagefilledrectangle($image,108,0,108,$height-1,$gpos50);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);		

#draw position indications: 
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,51,7,55,4,$gpos75);
imageline($image,51,7,55,10,$gpos75);
imageline($image,51,7,61,7,$gpos75);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
imagestring($image,2,65,1,$formatstart,$gpos75);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
imageline($image, 390,7,380,7,$gpos75);
imageline($image, 390,7,386,4,$gpos75);
imageline($image, 390,7,386,10,$gpos75);
imagestring($image,2,378-$txtwidth,2,$formatstop,$gpos75);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

