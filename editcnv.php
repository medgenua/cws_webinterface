<?php

function findInformativeBin($aid, $position, $largeposition, $nrbins, $mode, $dtable) {
	// mode can be start or stop
	$sq = mysql_query("SELECT content, structure FROM $dtable WHERE id = '$aid'");
	$row = mysql_fetch_row($sq);
	$content = $row[0];
	$structure = $row[1];
	$increment = substr_count($structure, '1');
	$counter = 0;
	$nrbins_absolute = abs($nrbins);
	$content = trim($content, "_");
	$content = explode("_", $content);
	$nrelements = count($content);
	$datapoints = array();
	for ($i=0; $i<$nrelements; $i+=$increment) {
		// non-informative bin
		if ($content[$i+1] == '.' || $content[$i+2] == '.' ) {
			$datapoints[$content[$i]] = 0;
		}
		// rest is informative
		else {
			$datapoints[$content[$i]] = 1;
		}
	}
    
	// sort the datapoint in the appropriate direction
	// scenario 1: add bins 5' or remove bins 3' -> traverse datapoints 3'-> 5' (high to low position)
	if (($mode == 'up' && $nrbins > 0) || ($mode == 'down' && $nrbins < 0)) {
		krsort($datapoints);
	}
	// scenario 2: add bins 3' or remove bins 5' -> traverse datapoints 5'-> 3' (low to high position)
	elseif (($mode == 'down' && $nrbins > 0) || ($mode == 'up' && $nrbins < 0)) {
		ksort($datapoints);
	}
	
	$newboundary = $newlargeboundary = false;
	// find 5' or 3' n-th informative bin (depending on the mode)
	foreach ($datapoints as $currentpos => $informative) {
		if ($newboundary) {
			$newlargeboundary = $currentpos;
			return array(intval($newboundary), intval($newlargeboundary));
		}
		// count the number of bins after the original start/end position
		if (($nrbins > 0 && (($mode == 'up' && $currentpos < $position) || ($mode == 'down' && $currentpos > $position))) ||
			($nrbins < 0 && (($mode == 'up' && $currentpos > $position) || ($mode == 'down' && $currentpos < $position))))
		{
			if ($informative == 1) {
				$counter++;
				if ($counter == $nrbins_absolute) {
					$newboundary = $currentpos;
				}
			}
		}
	}

	// not found, return the original values
	return array(intval($position), intval($largeposition));

}

# copy get variables around if needed
# GETED VARS

$aid = $_GET['aid'];
//$class = $_GET['class'];
$uid = $_GET['u'];
//$ftt = $_GET['ftt'];
if (isset($_GET['loh'])) {
	$loh = $_GET['loh'];
}
else {
	$loh = 0;
}
if (isset($_GET['mos'])) {
	$mos = $_GET['mos'];
}
else {
	$mos = 0;
}

if (isset($_GET['swgs'])) {
	$swgs = $_GET['swgs'];
}
else {
	$swgs = 0;
}


# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inharray = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
if ($loh == 1) {
	$atable = 'aberration_LOH';
	$ltable = 'log_loh';
	$dtable = 'datapoints_loh';
	$fields = '';
}
elseif ($mos == 1) {
	$atable = 'aberration_mosaic';
	$ltable = 'log_mos';
	$dtable = 'datapoints_mosaic';
	$fields = ',a.cn,a.inheritance';
}
else {
	$atable = 'aberration';
	$ltable = 'log';
	$dtable = 'datapoints';
	$fields = ',a.cn,a.inheritance';
}


#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
// get details on cnv
$query = mysql_query("SELECT a.sample, a.idproj, a.class, a.chr, a.start, a.stop, a.largestart, a.largestop,  a.seenby, s.chip_dnanr, a.datatype $fields FROM $atable a JOIN sample s ON s.id =a.sample WHERE a.id = $aid ");
$row = mysql_fetch_array($query);
$class = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$datatype = $row['datatype'];  // array or sWGS
$chrtxt = $chromhash[$chr];
$start = $row['start'];
$stop = $row['stop'];
if ($loh != 1) {
	$cn = $row['cn'];
	$inh = $row['inheritance'];
}
$sb = $row['seenby'];
$samplename = $row['chip_dnanr'];
$largestart = $row['largestart'];
$largestop = $row['largestop'];
$close = 1;
$region = "Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',');

$firstcell =  "style=\"border-left: 1px solid black;\"";

if ($cn == 9) {
	# get upd type
	$updq = mysql_query("select type from parents_upd WHERE sid = '$sid' AND chr = '$chr' AND start = '$start' AND stop = '$stop'");
	$updr = mysql_fetch_array($updq);
	$updtype = $updr['type'];
}

## SET HTML HEADERS 
?>
<html>
<head><title>EDIT CNV DETAILS: <?php echo $region; ?></title>
<style type="text/css">
	td {
	font-size:12;
	border-right: 1px solid black;
	border-bottom: 1px solid black;
	text-align: left;
	padding: 6px 6px 6px 12px;
	padding: 3px 3px 3px 12px; 
}
td.grey {
	font-size:12;
	border-right: 1px solid black;
	border-top: 1px solid black;
	text-align: left;
	padding: 6px 6px 6px 12px;
	background:#eeeeee;
	padding: 3px 3px 3px 12px; 
	
}

</style>
<script type=text/javascript>
	function clearfield(fieldid) {
		document.getElementById(fieldid).value = "";
	}
</script>
</head>
<body>
<?php
if (isset($_GET['cancel'])) {
	$close = 1;
}
# UPDATE DATA !
elseif (isset($_GET['save']) && $_GET['arguments'] != '') {
	$arguments = $_GET['arguments'];
	# Get new values
	$query = "";
	$regionlog = 0;
	if (isset($_GET['swgs']) && $_GET['swgs'] == 1) {
		error_log("SWGS EDIT: $start, $largestart, $stop, $largestop");
		// error_log(print_r($_GET, true));
		// sWGS data
		$regionlog = 1;
		$edit = false;
		$upbins = $downbins = 0;
		$newstart = $start;
		$newstop = $stop;
		$newlargestart = $largestart;
		$newlargestop = $largestop;

		// extend with nr 5' bins
		if (isset($_GET['upbins'])) {
			$upbins = $_GET['upbins'];
			list($newstart, $newlargestart) = findInformativeBin($aid, $newstart, $newlargestart, $upbins, "up", $dtable);
			$edit = true;
		}
		// extend with nr 3' bins
		if (isset($_GET['downbins'])) {
			$downbins = $_GET['downbins'];
			list($newstop, $newlargestop) = findInformativeBin($aid, $newstop, $newlargestop, $downbins, "down", $dtable);
			$edit = true;
		}

		if ($edit) {
			$size = $newstop - $newstart;
			error_log("EDIT: $upbins, $downbins, $start, $newstart,  $stop, $newstop, $size");
			$query .= " start = '$newstart', largestart = '$newlargestart',  stop = '$newstop', largestop = '$newlargestop',";
		}


	}
	else {
		// Array date: legacy
		// start was adapted
		if ($_GET['start'] != $start) {
			$newstart = $_GET['start'];
			$query .= " start = '$newstart',";
			$regionlog = 1;
			// check largestart
			if ($_GET['lstart'] != $largestart) {
				$largestartdone = 1;
				if ($_GET['lstart'] != '' && $_GET['lstart'] <= $newstart) {
					$newlargestart = $_GET['lstart'];				
				}
				else {
					# get new largestart value.
					$sq = mysql_query("SELECT chiptypeid FROM project WHERE id = '$pid'");
					$sqr = mysql_fetch_array($sq);
					$chip = $sqr['chiptypeid'];
					$sq = mysql_query("SELECT position FROM probelocations WHERE chiptype = '$chip' AND chromosome = '$chr' AND position < $newstart ORDER BY position DESC limit 1");
					if (mysql_num_rows($sq) == 0) {
						$newlargestart = 0;
					}
					else {
						$sqr = mysql_fetch_array($sq);
						$newlargestart = $sqr['position'];
					}
					
				}
				$query .= " largestart = '$newlargestart',";
				
			}
		}
		else {
			$newstart = $start;
		}
		// check largestart (without start edit)
		if ($_GET['lstart'] != $largestart && $largestartdone != 1) {
	
			if ($_GET['lstart'] != '' && $_GET['lstart'] <= $newstart) {
				$newlargestart = $_GET['lstart'];
			} 
			else {
				# get new largestart value.
				$sq = mysql_query("SELECT chiptypeid FROM project WHERE id = '$pid'");
				$sqr = mysql_fetch_array($sq);
				$chip = $sqr['chiptypeid'];
				$sq = mysql_query("SELECT position FROM probelocations WHERE chiptype = '$chip' AND chromosome = '$chr' AND position < $newstart ORDER BY position DESC limit 1");
				if (mysql_num_rows($sq) == 0) {
					$newlargestart = 0;
				}
				else {
					$sqr = mysql_fetch_array($sq);
					$newlargestart = $sqr['position'];
				}
			}
			$query .= " largestart = '$newlargestart',";
		}
	
		// stop was adapted
		if ($_GET['stop'] != $stop) {
			$newstop = $_GET['stop'];
			$query .= " stop = '$newstop',";
			$regionlog = 1;
			// check largestop
			if ($_GET['lstop'] != $largestop) {
				$largestopdone = 1;
	
				if ($_GET['lstop'] != '' &&  $_GET['lstop'] >= $newstop) {
					$newlargestop = $_GET['lstop'];
				}
				else {
					# get new largestop value.
					$sq = mysql_query("SELECT chiptypeid FROM project WHERE id = '$pid'");
					$sqr = mysql_fetch_array($sq);
					$chip = $sqr['chiptypeid'];
					$sq = mysql_query("SELECT position FROM probelocations WHERE chiptype = '$chip' AND chromosome = '$chr' AND position > $newstop ORDER BY position ASC limit 1");
					if (mysql_num_rows($sq) == 0) {
						$newlargestop = 'ter';
					}
					else {
						$sqr = mysql_fetch_array($sq);
						$newlargestop = $sqr['position'];
					}
				}
				$query .= " largestop = '$newlargestop',";
			}
	
		}
		else {
			$newstop = $stop;
		}
		// largestop edit without stop edit
		if ($_GET['lstop'] != $largestop && $largestopdone != 1) {
			if ($_GET['lstop'] != '' && $_GET['lstop'] >= $newstop) {
				$newlargestop = $_GET['lstop'];
			} 
			else {
				# get new largestop value.
				$sq = mysql_query("SELECT chiptypeid FROM project WHERE id = '$pid'");
				$sqr = mysql_fetch_array($sq);
				$chip = $sqr['chiptypeid'];
				$sq = mysql_query("SELECT position FROM probelocations WHERE chiptype = '$chip' AND chromosome = '$chr' AND position > $newstop ORDER BY position ASC limit 1");
				if (mysql_num_rows($sq) == 0) {
					$newlargestop = 'ter';
				}
				else {
					$sqr = mysql_fetch_array($sq);
					$newlargestop = $sqr['position'];
				}
			}
			$query .= " largestop = '$newlargestop',";
	
			$size = $newstop - $newstart + 1;
		}
	}

	if ($regionlog == 1) {
		$entry = "Region Adapted From $region";
		mysql_query("INSERT INTO $ltable (sid, aid, pid, uid, entry, arguments) VALUES ('$sid', '$aid', '$pid', '$uid', '$entry', '$arguments')");
	}

	if ($loh != 1) {
	   // CopyNumber was adapted
	   if ($_GET['copynumber'] != $cn) {
		$newcn = $_GET['copynumber'];
		$query .= " cn = '$newcn',";
		if ($cn == 9) {
			include('inc_Gaussian.inc');	
			$entry = "CopyNumber Changed From $updtype to $newcn";
			# move datapoints !
			$uq = mysql_query("SELECT id, fid, mid FROM parents_upd WHERE sid = '$sid' AND chr = '$chr' AND start = '$start' AND stop = '$stop'");
			$ur = mysql_fetch_array($uq);
			$uaid = $ur['id'];
			$ufid = $ur['fid'];
			$umid = $ur['mid'];
			// index
			$sdq = mysql_query("SELECT content FROM parents_upd_datapoints WHERE sid = '$sid' AND id = '$uaid'");
			$sdqr = mysql_fetch_array($sdq);
			$siddata = explode('_',$sdqr['content']);
			
			$nrsid = count($siddata);
			$sidstring = "";
			$gts = array( "AA" => '0', "BB" => '1', "AB" => '0.5');
			for ($i = 0; $i < $nrsid; $i = $i+3){
				#if ($siddata[$i] < $newstart || $siddata[$i] > $newstop) {
				#	continue;
				#}
				$gtval = $gts[$siddata[($i+2)]] + gauss_ms(0,0.01);
				if ($gtval > 1) {
					$gtval = 2 - $gtval ;
				}
				elseif ($gtval < 0) {
					$gtval = abs($gtval);
				}

				$sidstring .= $siddata[$i] . "_" . $siddata[($i+1)] . "_" . $gtval ."_";
			}
			$sidstring = substr($sidstring,0,-1);
			mysql_query("INSERT INTO $dtable (id, content) VALUES ('$aid', '$sidstring')");
			// father
			$fdq = mysql_query("SELECT content FROM parents_upd_datapoints WHERE sid = '$ufid' AND id = '$uaid'");
			$fdqr = mysql_fetch_array($fdq);
			$fiddata = explode('_',$fdqr['content']);
			$nrfid = count($fiddata);
			$fidstring = "";
			$slogr = 0; 
			for ($i = 0; $i < $nrfid; $i = $i+3){
				if ($fiddata[$i] < $newstart || $fiddata[$i] > $newstop) {
					$nrfid--;
					continue;
				}
				$fidstring .= $fiddata[$i] . "_" . $fiddata[($i+1)] . "_" . $gts[$fiddata[($i+2)]] ."_";
				$slogr += $fiddata[($i+1)];
			}
			if ($nrfid > 0) {
				$fal = $slogr/$nrfid;
			}
			else {
				$fal = '';
			}
			$fidstring = substr($fidstring,0,-1);
			mysql_query("INSERT INTO parents_datapoints (content) VALUES ('$fidstring')");
			$newid = mysql_insert_id();
			mysql_query("INSERT INTO parents_regions (id, chr, start, stop, sid , avgLogR) VALUES ('$newid','$chr', '$newstart', '$newstop', '$ufid', '$fal')");
			mysql_query("INSERT INTO parent_offspring_cnv_relations (aid, parent_seen, parent_aid, parent_sid) VALUES ('$aid', '0', '$newid', '$ufid')");	

			// mother
			$mdq = mysql_query("SELECT content FROM parents_upd_datapoints WHERE sid = '$umid' AND id = '$uaid'");
			$mdqr = mysql_fetch_array($mdq);
			$middata = explode('_',$mdqr['content']);
			$nrmid = count($middata);
			$midstring = "";
			$slogr = 0; 
			for ($i = 0; $i < $nrmid; $i = $i+3){
				if ($middata[$i] < $newstart || $middata[$i] > $newstop) {
					$nrmid--;
					continue;
				}
				$midstring .= $middata[$i] . "_" . $middata[($i+1)] . "_" . $gts[$middata[($i+2)]] ."_";
				$slogr += $middata[($i+1)];
			}
			if ($nrmid > 0) {
				$mal = $slogr/$nrmid;
			}
			else {
				$mal = '';
			}
			$midstring = substr($midstring,0,-1);
			mysql_query("INSERT INTO parents_datapoints (content) VALUES ('$midstring')");
			$newid = mysql_insert_id();
			mysql_query("INSERT INTO parents_regions (id, chr, start, stop, sid , avgLogR) VALUES ('$newid','$chr', '$newstart', '$newstop', '$umid', '$fal')");
			mysql_query("INSERT INTO parent_offspring_cnv_relations (aid, parent_seen, parent_aid, parent_sid) VALUES ('$aid', '0', '$newid', '$umid')");	
		
		}
		elseif ($newcn == 9) {
			$entry = "CopyNumber Changed From $cn to UPD";
			// THIS OPTION IS NOT AVAILABLE IN DROPDOWN !
		}
		else {
			$entry = "CopyNumber Changed From $cn to $newcn";
		}
		mysql_query("INSERT INTO log (sid, aid, pid, uid, entry, arguments) VALUES ('$sid', '$aid', '$pid', '$uid', '$entry', '$arguments')");
	   }
	}
	$query .= " size = '$size',";

	// update number of SNPs based on dataponts
	$sq = mysql_query("SELECT content, structure FROM $dtable WHERE id = '$aid'");
	$row = mysql_fetch_row($sq);
	$content = $row[0];
	$structure = $row[1];
	$increment = substr_count($structure, '1');

	$content = trim($content, "_");
	$content = explode("_", $content);
	$nrelements = count($content);
	$datapoints = array();
	for ($i=0; $i<$nrelements; $i+=$increment) {
		// non-informative bin
		if ($content[$i+1] == '.' || $content[$i+2] == '.' ) {
			$datapoints[$content[$i]] = 0;
		}
		// rest is informative
		else {
			$datapoints[$content[$i]] = 1;
		}
	}

	ksort($datapoints);
	$nrsnps = 0;
	$totalnrsnps = 0;
	foreach ($datapoints as $position => $informative) {
		if ($newstart <= $position && $position <= $newstop) {
			// non-informative bin
			if ($informative == 1) {
				error_log("I bin");
				$nrsnps++;
				$totalnrsnps++;
			}
			else {
				error_log("NI bin");
				$totalnrsnps++;

			}
		}
	}
	$query .= " nrsnps = '$nrsnps', total_nrsnps  = '$totalnrsnps', ";

	// update number of genes
	$sq = mysql_query("SELECT Count(DISTINCT symbol) as nrgenes FROM genes WHERE chr='$chr' AND (((start BETWEEN $newstart AND $newstop) OR (end BETWEEN $newstart AND $newstop)) OR (start <= $newstart AND end >= $newstop))");
	$row = mysql_fetch_array($sq);
	$nrgenes = $row['nrgenes'];
	$query .= " nrgenes = '$nrgenes',";

	#echo "$query<br/>";

	# update aberration table
	if ($query != '') {
		$query = substr($query,0,-1);
		error_log($query);
		mysql_query("UPDATE $atable SET $query WHERE id = '$aid'");
	}
	# set close-variable 
	$close = 1;
}
else {
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" .$usrow['FirstName'] .' '.$usrow['LastName'] . ")";
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	echo "<h3>Edit CNV Details : $region</h3>";
	echo "<p>You are editing CNV details. Please specify a clear reason for this !</p>\n";
	echo "<p>";
	

	echo "<p><form action=editcnv.php method=GET>";
	echo "<input type=hidden name=loh value='$loh'>";
	echo "<input type=hidden name=swgs value='$swgs'>";
	echo "<input type=hidden name=mos value='$mos'>";
	echo "<table cellspacing=0>";
	echo "<tr><td class=grey $firstcell>Item</td><td class=grey>New Value</td><td class=grey>Remark</td></tr>";
	# region
	if ($swgs == 1) {
		echo "<tr><td $firstcell>Chromosome</td><td>$chrtxt</td><td>Chr can't be changed</td></tr>";
		echo "<tr><td $firstcell>Extra bins 5'</td><td><input name=upbins type=text size=15 placeholder='0'></td><td>Only Plain numbers</td></tr>";
		echo "<tr><td $firstcell>Extra bins 3'</td><td><input name=downbins type=text size=15 placeholder='0'></td><td>Only Plain numbers</td></tr>";
	}
	else {
		echo "<tr><td $firstcell>Chromosome</td><td>$chrtxt</td><td>Chr can't be changed</td></tr>";
		echo "<tr><td $firstcell>Start</td><td><input name=start type=text size=15 value='$start' onchange=\"clearfield('lstart')\"></td><td>Only Plain numbers</td></tr>";
		echo "<tr><td $firstcell>Stop</td><td><input name=stop type=text size=15 value='$stop' onchange=\"clearfield('lstop')\"></td><td>Only Plain numbers</td></tr>";
		echo "<tr><td $firstcell>Large Start</td><td><input name=lstart id=lstart type=text size=15 value='$largestart'></td><td>Set to empty for auto value</td></tr>";
		echo "<tr><td $firstcell>Large Stop</td><td><input name=lstop id=lstop type=text size=15 value='$largestop'></td><td>Set to empty for auto value</td></tr>";
	}
	# CopyNumber
	if ($mos == 1) {
		echo "<tr><td $firstcell>CopyNumber</td><td><input name=copynumber type=number value='$cn' step='0.1'></td><td></td></tr>";
	}
	elseif ($loh != 1) {
		echo "<tr><td $firstcell>CopyNumber</td><td><select name=copynumber>";
		$found = 0;
		for ($i = 0;$i <=4;$i++) {
			if ($i == $cn) {
				$selected = "SELECTED";
				$found = 1;
			}
			else {
				$selected = '';
			}
			echo "<option value=$i $selected>$i</option>";
		}
		if ($found == 0 && $cn == 9) {
			echo "<option value=9 SELECTED>$updtype</option>";
		}
		echo "</select></td><td>&nbsp;</td></tr>";
	}	
	echo "</table>";
	echo "<input type=hidden name=aid value='$aid'>\n";
	echo "<input type=hidden name=u value='$uid'>\n";
	echo "Reason: <br/><input type=text value='' name=arguments maxsize=255 size=35></p>";
	echo "<input type=submit name=save value='Save'> &nbsp; &nbsp; <input type=submit name=cancel value='Cancel'>\n";
	echo" </form>\n";
}

## CLOSE POPUP & GO BACK TO CALLING PAGE
if ($close == 1) {
	$_SESSION['setfromeditcnv'] = 1;
	$_SESSION['sample'] = $sid;
	$_SESSION['project'] = $pid;
	echo "<script type='text/javascript'>opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
?>

</body>
</html>
