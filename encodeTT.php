<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$evidence["1"] = "Validated";
$evidence["2"] = "Manual Annotation";
$evidence["3"] = "Automatic Annotation";
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
$gID = $_GET['q'];
//$type = $_GET['t'];

$query = mysql_query("SELECT chr, start, stop, strand, symbol, level, entries, coding FROM encodesum WHERE ID = '$gID'");

$row = mysql_fetch_array($query);
$chr = $row['chr'];
$chrtxt = $chromhash[ $chr ];
$start = $row['start'];
$stop = $row['stop'];
$symbol = $row['symbol'];
$entries = $row['entries'];
$level = $row['level'];
$coding = $row['coding'];
if ($coding == 0) {
	echo "<div class=nadruk>$symbol : Non Coding Transcripts</div>\n";
}
else {
	echo "<div class=nadruk>$symbol : Coding Transcripts</div>\n";
}
echo "<p></p><span class=italic>General Information:</span><br/>\n";
echo "<ul id=ul-simple>\n";
echo "<li> - Region: Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',')."</li>\n";
echo "<li> - Evidence: ". $evidence[$level] . "</li>\n";
if ($coding == 0) {
	echo "<li> - Transcripts Overview:<br/>\n";
}
else {
	echo "<li> - Transcripts Overview: <br/>\n";
}
$subquery = mysql_query("SELECT start, end, strand, etID, class, ottgid FROM encode WHERE chr = '$chr' AND coding = '$coding' AND ( (start BETWEEN $start AND $stop) OR (end BETWEEN $start AND $stop ) OR (start <= $start AND end >= $stop)) ORDER BY start");
echo "<table cellspacing=0 align=center>\n";
echo "<tr>\n";
echo "<th class=compact $firstcell>Region (link)</th>\n";
echo "<th class=compact>Strand</th>\n";
echo "<th class=compact>Class</th>\n";
echo "</tr>\n";
while ($subrow = mysql_fetch_array($subquery)) {
	$substart = $subrow['start'];
	$substop = $subrow['end'];
	$strand = $subrow['strand'];
	$etID = $subrow['etID'];
	$class= $subrow['class'];
	//$ottgid = $subrow['ottgid'];
	echo "<tr>";
	echo "<td class=compact $firstcell><a class=tt style='font:9px;' target='_blank' href='http://genome.ucsc.edu/cgi-bin/hgTracks?clade=mammal&org=Human&db=$ucscdb&position=$etID'>chr$chrtxt:".number_format($substart,0,'',',')."-".number_format($substop,0,'',',')."</a></td>";
	echo "<td class=compact>$strand</td>";
	echo "<td class=compact>$class</td>\n";
	echo "</tr>\n";
}
echo "</table>\n";
echo "</li></ul>\n";

echo "<br/>";
echo "<span class=italic>External Links:</span><br/>\n";
echo "<ul id=ul-simple>\n";
echo "<li> - <a class=tt href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&amp;cmd=search&amp;term=$symbol\" target=new> Search PubMed</a></li>\n";
echo "<li> - <a class=tt href=\"http://scholar.google.com/scholar?hl=en&amp;lr=&amp;btnG=Search&amp;q=$symbol\" target=new>Search Google Scholar</a></li>\n";
echo "<li> - <a class=tt href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=unigene&amp;cmd=search&amp;term=$symbol%20AND%20Homo%20Sapiens\" target=new>Search UniGene</a></li>\n";
echo " <li> - <a class=tt href=\"http://www.pubgene.org/tools/Ontology/BioAssoc.cgi?mode=simple&amp;terms=$symbol&amp;organism=hs&amp;termtype=gap\" target=new>Search PubGene</a></li>\n";
echo " <li> - <a class=tt href=\"index.php?page=results&amp;type=region&amp;region=$symbol\" target=new>Search Local Database</a></li>\n";
echo "</ul>";
echo "<br />\n";
?>
