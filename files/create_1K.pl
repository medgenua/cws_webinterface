#!/usr/bin/perl

##################
## LOAD MODULES ##
##################
use Getopt::Std;
# b : Genome Build in UCSC notation (e.g. hg19)
getopts('b:', \%opts);
my $build ;
if ($opts{'b'}) {
	$build = $opts{'b'};
}
else {
	die('You need to specify the genome build (eg : -b hg19)');
}

$|++;
###################
## DOWNLOAD FILE ## 
###################
print "Downloading Needed data file\n";
my $url = "http://hgdownload.cse.ucsc.edu/goldenPath/$build/gc5Base/$build.gc5Base.txt.gz";
## axel is a highly recommended download tool.
my $out = system("which axel");
if ($out ne '') {
	system("axel -a -o $build.gc5Base.txt.gz $url");
}
else {
	system("wget -O $build.gc5Base.txt.gz $url");
}
##################
## EXTRACT FILE ##
##################
print "Extracting Data File (resulting file is over 6Gb!)\n";
system("gunzip -f $build.gc5Base.txt.gz");


open IN, "$build.gc5Base.txt";
if (!-d "gc.$build") {
	system("mkdir gc.$build");
}
$currchr = '';
my $kbstart = 0;
my $tresh = $kbstart+1000;
my $currpos = "";
open OUT, ">/dev/null"; ## this is temporary
my $sum = 0;
my $items = 0;
while (<IN>) {
	# new chromosome detected?
	if ($_ =~ m/chrom=chr(\S+)\s/) {
		my $chr = $1;
		if ($sum > 0) {
			my $average = $sum / $items;
			print OUT "$kbstart\t$currpos\t$average\n";
			print "  - chr$currchr: $kbstart-$currpos => $average\n";
		}
		close OUT;
		my $file = "gc.$build/$chr"."_1k.txt";
		if (length($chr) > 2) {
			# redirect output of random and Un_gl... chromosomes to trash
			$file = "/dev/null";
		}
		open OUT, ">$file";
		$currchr = $chr;
		print "Processing chr $chr\n";
		$sum = 0;
		$items = 0;
		$kbstart = 0;
		$tresh = $kbstart + 1000;
		next;
	}
	# split line
	chomp($_);
	my @p = split(/\t/,$_);
	# passed 1Kb treshold?
	if ($p[0] > $tresh) {
		if ($sum > 0) {
			my $average = $sum / $items;
			print OUT "$kbstart\t$tresh\t$average\n";
			#print "  - chr$currchr: $kbstart-$tresh => $average\n";
		}
		$kbstart = $tresh + 5;
		$tresh = $kbstart + 1000;
		$sum = 0;
		$items = 0;
	}
	$items++;
	$sum = $sum + $p[1];
}
## clean up
system("rm -f $build.gc5Base*");
