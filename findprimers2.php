<?php

// Connect to the database
include('.LoadCredentials.php');
$db = "primerdesign".$_SESSION['dbname'];
mysql_select_db("$db");

for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["X"] = 23;
$chromhash["Y"] = 24;

if ($_POST['region']) {
	$region = $_POST['region'];
	$pieces=preg_split("/[:\-_+(\.\.)]/",$region); 
	$chr = preg_replace("/chr(\w+)/i", "$1", $pieces[0]);
	$chr = str_replace(" ","",$chr);
	$start = $pieces[1];
	$start = str_replace(",","",$start);
	$start = str_replace(" ","",$start);
	$end = $pieces[2];
	$end = str_replace(",","",$end);
	$end = str_replace(" ","",$end);
	$tablename = "chr".$chr."_".$start."_".$end;
	$result = mysql_query("Select ID, submitter FROM Succeeded WHERE region='$tablename'");
	//$reg = preg_replace("/:/", 's', $region);
	//$reg = preg_replace("/[\+-_]/",'e',$reg); 
	if ($end <= $start ) {
		echo "chromosome: $chr <br>Start: $start <br>End: $end<br>\n";
		echo "Your starting position has to be bigger than the end position !<br>Go back and try again\n";
	}
	if (!$_POST['operator']) {
		echo "Operator is mandatory!!<br>Go back and try again<br>";
	}
	else if ( mysql_num_rows($result)>0) {
		$operator = $_POST['operator'];
		$userid = $_POST['submitter'];
		$row = mysql_fetch_array($result);
		$submitter = $row['submitter'];
		if ($submitter == 0) {
			$regionid = $row['ID'];
			$mysqldate = date( 'Y-m-d H:i:s' );
			#$phpdate = strtotime( $mysqldate );
			$query = mysql_query("UPDATE Succeeded SET submitter = '$userid', created = '$mysqldate' WHERE ID = '$regionid'");
			header("Location: index.php?page=primerdesign&type=result&region=$tablename&operator=$operator&existed=auto&uid=$userid");

		}
		header("Location: index.php?page=primerdesign&type=result&region=$tablename&operator=$operator&existed=true&uid=$userid");
	}
	else {
		$operator = $_POST['operator'];
		$submitter = $_POST['submitter'] ;
		$statusfile = "status/chr".$chr."_".$start."_".$end.".$operator.$submitter.pd.status";
		$outfile = "status/chr".$chr."_".$start."_".$end.".output";
		system("(echo 1 > $statusfile && chmod 777 $statusfile && echo Starting up...  > /opt/mkprimer/$outfile && chmod 777 /opt/mkprimer/$outfile && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd /opt/mkprimer && ./mkprimer.pl $tablename $operator $submitter 8 >> $outfile \" &&  echo 0 > $statusfile ) > /dev/null & ");
		header("Location: index.php?page=primerdesign&type=result&region=$tablename&operator=$operator&uid=$submitter");
	}
}
elseif ($_POST['batch'] ) {
	$problem = 0;
	if (!$_POST['operator']) {
		echo "Operator is mandatory!!<br>Go back and try again<br>";
	}
	else {
		$operator = $_POST['operator'];
		$userid = $_POST['submitter'];
		$regions = $_POST['batch'];
		#echo "under construction\n";
		$text = str_replace("\r", "", $regions);//to process the windows new line
		$arrtext = explode("\n", $text);// create array of keywords
		$problems = "";
		$goods = "";
		$finished = "";
		foreach($arrtext as $region) {
			
			$pieces=preg_split("/[:\-_+(\.\.)]/",$region); 
			$chr = preg_replace("/chr(\w+)/i", "$1", $pieces[0]);
			$chr = str_replace(" ","",$chr);
			$start = $pieces[1];
			$start = str_replace(",","",$start);
			$start = str_replace(" ","",$start);
			$end = $pieces[2];
			$end = str_replace(",","",$end);
			$end = str_replace(" ","",$end);
			$tablename = "chr".$chr."_".$start."_".$end;
			$statusfile = "status/chr".$chr."_".$start."_".$end.".$operator.$userid.pd.status";
			$result = mysql_query("Select ID, submitter FROM Succeeded WHERE region='$tablename'");
			if ($end <= $start || $chr = "" || $start = "" || $end = "") { 
				$problems = $problems ."<li>". $region . "</li>\n";
				$problem = 1;	
			} 
			elseif  ( mysql_num_rows($result)>0) {   // this one is analysed before, echo 0 to statusfile 
				$row = mysql_fetch_array($result);
				$submitter = $row['submitter'];
				if ($submitter == 0) {
					$mysqldate = date( 'Y-m-d H:i:s' );
					$regionid = $row['ID'];
					$query = mysql_query("UPDATE Succeeded SET submitter = '$userid', created = '$mysqldate' WHERE ID = '$regionid'");
				}
				system("(echo 0 > $statusfile && chmod 777 $statusfile ) > /dev/null & ");
				$finished = $finished ."<li><a href=index.php?page=primerdesign&amp;type=result&amp;region=$tablename&amp;operator=$operator&amp;uid=$userid>$region</a></li>\n";
			}
			else { // New analysis, echo -1 to statusfile
				system("(echo -1 > $statusfile && chmod 777 $statusfile ) > /dev/null & ");
				$goods = $goods. "<li>".$region ."</li>\n";
			}	
		}	
	} 
	$data = "good=$goods&bad=$problems&finished=$finished";
	$file = rand(1,1000) . "pd.batch";
	$fh = fopen("data/$file", 'w');
	fwrite($fh, $data);
	fclose($fh);
	header("Location: index.php?page=primerdesignbatchresult&data=$file");


	

}
elseif ($_GET['aid'] && $_GET['uid']) {
	// GET DETAILS
	$db = "CNVanalysis" . $_SESSION['dbname']; 
	mysql_select_db("$db");
	$aid = $_GET['aid'];
	$uid = $_GET['uid'];
	if ($_GET['cstm'] != 1) {
		$result = mysql_query("Select chr, start, stop FROM aberration WHERE id='$aid'");
	}
	else {
		$result = mysql_query("Select chr, start, stop FROM cus_aberration WHERE id = '$aid'");
	}
	$row = mysql_fetch_array($result);
	$chr = $row['chr'];
	$chrtxt = $chromhash[$chr];
	
	$start = $row['start'];
	$end = $row['stop'];
	$region = "$chrtxt:$start-$end";
	$result = mysql_query("SELECT FirstName FROM users WHERE id = '$uid'");
	$row = mysql_fetch_array($result);
	$operator = $row['FirstName'];
	
	$problems = "";
	$goods = "";
	$finished = "";
	$tablename = "chr".$chrtxt."_".$start."_".$end;
	$statusfile = "status/chr".$chrtxt."_".$start."_".$end.".$operator.$uid.pd.status";
	// SUBMIT REGION
	mysql_select_db("$db");
	$result = mysql_query("Select ID, submitter FROM Succeeded WHERE region='$tablename'");
	if ($end <= $start || $chr = "" || $start = "" || $end = "") { 
		$problems = $problems ."<li>". $region . "</li>\n";
		$problem = 1;	
	} 
	elseif  ( mysql_num_rows($result)>0) {   // this one is analysed before, echo 0 to statusfile 
		$row = mysql_fetch_array($result);
		$submitter = $row['submitter'];
		if ($submitter == 0) {
			$mysqldate = date( 'Y-m-d H:i:s' );
			$regionid = $row['ID'];
			$query = mysql_query("UPDATE Succeeded SET submitter = '$uid', created = '$mysqldate' WHERE ID = '$regionid'");
		}
		system("(echo 0 > $statusfile && chmod 777 $statusfile ) > /dev/null & ");
		$finished = $finished ."<li><a href=index.php?page=primerdesign&amp;type=result&amp;region=$tablename&amp;operator=$operator&amp;uid=$uid>$region</a></li>\n";
	}
	else { // New analysis, echo -1 to statusfile
		system("(echo -1 > $statusfile && chmod 777 $statusfile ) > /dev/null & ");
		$goods = $goods. "<li>".$region ."</li>\n";
	}
	
	$data = "good=$goods&bad=$problems&finished=$finished";
	$file = rand(1,1000) . "pd.batch";
	$fh = fopen("data/$file", 'w');
	fwrite($fh, $data);
	fclose($fh);
	header("Location: index.php?page=primerdesignbatchresult&data=$file");


}
else {
echo "some problem\n";
}
?>
