<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
$gID = $_GET['q'];
//$type = $_GET['t'];
$uid = $_GET['u'];
$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$uid'");
$row = mysql_fetch_array($query);
$db_links = $row['db_links'];

$query = mysql_query("SELECT chr, start, stop, strand, symbol, omimID, morbidID, geneID, morbidTXT FROM genesum WHERE ID = '$gID'");

$row = mysql_fetch_array($query);
$chr = $row['chr'];
$chrtxt = $chromhash[ $chr ];
$start = $row['start'];
$stop = $row['stop'];
$strand = $row['strand'];
$symbol = $row['symbol'];
$omimID = $row['omimID'];
$morbidID = $row['morbidID'];
$morbidtxt = $row['morbidTXT'];
$geneid = $row['geneID'];
echo "<div class=nadruk>$symbol : RefSeq Entry</div>\n";
echo "<span class=italic>Gene Information:</span><br/>\n";
echo "<ul id=ul-simple>\n";
echo "<li> - Position : Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',')."</li>\n";
echo "<li> - Strand : $strand</li>\n";
if ($morbidID != '') {
	echo "<li> - <a class=tt href=\"http://www.omim.org/entry/$morbidID\" target=new>MORBID id: $morbidID</a></li>\n";
}
elseif ($omimID != '') {
	echo "<li> - <a class=tt href=\"http://www.omim.org/entry/$omimID\" target=new>OMIM id: $omimID</a></li>\n";
}
echo "</ul>\n";
echo "<br/>";
echo "<span class=italic>External Links: <a class=ttsmall href=\"index.php?page=settings&type=plots&st=publ&v=p\" target='_blank'>(change)</a></span><br/>\n";
echo "<ul id=ul-simple>\n";
$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=1 ORDER BY Resource");
$nrlinks = mysql_num_rows($query);
if ($nrlinks == 0) {
	echo "<li> - No resources specified </li>\n";
}
else {
	while ($row = mysql_fetch_array($query)) {
		$dbname = $row['Resource'];
		$link = $row['link'];
		$title = $row['Description'];
		$link = str_replace('%s',$symbol,$link);
		$link = str_replace('%o',$omimID,$link);	
		$link = str_replace('%g',$geneid,$link);
		$link = str_replace('%u',$ucscdb,$link);
		
		echo "<li> - <a class=tt title='$title' href='$link' target=new>$dbname</a></li>\n";
	}
}
//echo "<li> - <a class=tt href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&amp;cmd=search&amp;term=$symbol\" target=new> Search PubMed</a></li>\n";
#echo "<li> - <a class=tt href=\"http://scholar.google.com/scholar?hl=en&amp;lr=&amp;btnG=Search&amp;q=$symbol\" target=new>Search Google Scholar</a></li>\n";
#echo "<li> - <a class=tt href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=unigene&amp;cmd=search&amp;term=$symbol%20AND%20Homo%20Sapiens\" target=new>Search UniGene</a></li>\n";
#echo " <li> - <a class=tt href=\"http://www.pubgene.org/tools/Ontology/BioAssoc.cgi?mode=simple&amp;terms=$symbol&amp;organism=hs&amp;termtype=gap\" target=new>Search PubGene</a></li>\n";
#echo " <li> - <a class=tt href=\"index.php?page=results&amp;type=region&amp;region=$symbol\" target=new>Search Local Database</a></li>\n";
echo "</ul>";
echo "<br />\n";
if ($morbidID != ''){
	echo "<span class=italic>MORBID Description</span><br/>\n";
	$morbidtxt = str_replace('|',"\n<li> - ",$morbidtxt);
	$morbidtxt = str_replace(array('{','}','[',']'),'',$morbidtxt);
	echo "<ul id=ul-simple>\n";
	echo "$morbidtxt";
	echo "\n</ul>";

}
?>
