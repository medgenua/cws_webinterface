<?php
// this should keep the session alive
//$sessionid = session_id();
//if (empty($sessionid)) {
//	session_start();
//}
// get file output
$filename = $_GET['filename'];
if (filesize($filename) > 1024*1024*5) {
	echo "There seems to be a problem with the output file.\n It has grown over 5Mb in size. As this is not normal, it will most likely keep growing and eventually crash your browser. Please report this, together with the full URL of the page your are currently browsing. \n";
}
else {
	$filecontent = file_get_contents($filename);
	echo $filecontent;
}
?>
