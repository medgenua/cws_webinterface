<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$uid = $_GET['u'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (end BETWEEN '$start' AND '$stop') OR (end <= '$start' AND end >= '$stop'))";
$window = $stop-$start+1;
$type = $_GET['type'];

$types = array(1 => "= 'DUP'", 2 => "= 'DEL'", 3 => "NOT IN ('DEL','DUP')");


# DEFINE IMAGE PROPERTIES
$querystring = "SELECT id FROM `gnomAD_SV_CNV` WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (end BETWEEN '$start' AND '$stop') OR (start <= '$start' AND end >= '$stop')) AND type $types[$type] ";
$result = mysql_query($querystring);
$nrlines = mysql_num_rows($result);
$height = $nrlines*9 + 20;

// scale
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
}
else {
	$scale = 500000;
	$stext = "500 kb";
} 

//Specify constant values
$width = 400; //Image width in pixels
$xoff = 130;
// set scaling factor
$scalef = ($width-$xoff)/($window);
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];



//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$orange = ImageColorAllocate($image,255, 179,0);

$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

// dgv types
$dgvcolors = array(1 => $blue, 2 => $red, 3 => $gpos50);
$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
//Fill background
imagefill($image,0,0,$white);

// get data
$querystring = "SELECT id, af_all, popmax_af, start, end FROM `gnomAD_SV_CNV` WHERE chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (end BETWEEN '$start' AND '$stop') OR (start <= '$start' AND end >= '$stop')) AND type $types[$type] ORDER BY start";
$result = mysql_query($querystring);
$y = 8;
$cy = $y;

$cstudid = '';

while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['end'];
    
	trigger_error("Drawing $cstart - $cstop");
	// next line
    $y = $y + 9;
    // cnv af
	imagestring($image,1,10,$y,$row['af_all'],$black);
	// cnv max frequency
	imagestring($image,1,77,$y,$row['popmax_af'],$black);	
	$larrow = 0;
	if ($cstart < $start) {
		$larrow = 1;
		$cstart = $start;
	}
	$rarrow = 0;
	if ($cstop > $stop) {
		$rarrow = 1;
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	imagefilledrectangle($image,$xoff+$scaledstart,$y+1,$xoff+$scaledstop,$y+4,$dgvcolors[$type]);
	if ($larrow == 1) {
		imageline($image,$xoff+$scaledstart+1,$y+2,$xoff+$scaledstart+6,$y+2,$white);
		imageline($image,$xoff+$scaledstart+1,$y+2,$xoff+$scaledstart+4,$y+4,$white);
	}
	if ($rarrow == 1) {
		imageline($image,$xoff+$scaledstop-6,$y+2,$xoff+$scaledstop-1,$y+2,$white);
		imageline($image,$xoff+$scaledstop-1,$y+2,$xoff+$scaledstop-4,$y+4,$white);
	}

	
}


# Draw the table borders
imagestring($image,2,10,1,"Overall AF",$gpos75);
imagestring($image,2,76,1,"MaxPop AF",$gpos75);
imagefilledrectangle($image,0,0,0,$y+10,$gpos50);
imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
imagefilledrectangle($image,$width-1,0,$width-1,$y+10,$gpos50);
imagefilledrectangle($image,72,0,72,$y+10,$gpos50);
imagefilledrectangle($image,128,0,128,$y+10,$gpos50);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);		

#draw position indications: LEFT
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,130,7,134,4,$gpos75);
imageline($image,130,7,134,10,$gpos75);
imageline($image,130,7,140,7,$gpos75);
#imageline($image,$xstop,18,$xstop,14,$black);
#imageline($image,$xstart,16,$xstop,16,$black);
imagestring($image,2,144,1,$formatstart,$gpos75);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
// RIGHT
imageline($image, $width,7,$width-10,7,$gpos75);
imageline($image, $width,7,$width-4,4,$gpos75);
imageline($image, $width,7,$width-4,10,$gpos75);
imagestring($image,2,$width-12-$txtwidth,2,$formatstop,$gpos75);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

