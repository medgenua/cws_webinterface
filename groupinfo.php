<?php
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


//vars
$uid = $_GET['u'];
$gid = $_GET['q'];
// get usergroup information
$query = mysql_query("SELECT description, affiliation, administrator, editcnv, editclinic, opengroup FROM usergroups WHERE id = '$gid'");
$row = mysql_fetch_array($query);
$descr = $row['description'];
$allowedaffi = $row['affiliation'];
$admin = $row['administrator'];
$editcnv = $row['editcnv'];
$editclinic = $row['editclinic'];
$opengroup = $row['opengroup'];
if ($editcnv == 1 && $editclinic == 1) {
	$permission = 'Edit CNV and Clinical information';
}
elseif ($editcnv == 1) {
	$permission = 'Edit CNV information';
}
elseif ($editclinic == 1) {
	$permission = 'Edit Clinical information';
}
else {
	$permission = 'Read Only Access';
}  

// get affiliations information
if ($opengroup != 1) {
	
	$query = mysql_query("SELECT id,name FROM affiliation WHERE id IN ($allowedaffi) ORDER BY name");
	$affis = array();
	while ($row = mysql_fetch_array($query)) {
		$affis[$row['id']] = $row['name'];
	}
}

// get admin information
$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$admin'");
$row = mysql_fetch_array($query);
$adminname = $row['LastName'] ." ".$row['FirstName'];

echo "<ul id=ul-simple style='margin-left:10px'>\n";
echo "<li><span class=italic>- Description:</span> $descr</li>\n";
echo "<li><span class=italic>- Administrator:</span> $adminname</li>";
echo "<li><span class=italic>- Permissions:</span> $permission</li>"; 
echo "<li><span class=italic>- Institutions:</span> ";
if ($opengroup == 1) {
	echo " All</li>";
}
else {
	echo "<ul id=ul-simple style='margin-left:15px'>";
	foreach($affis as $id => $name) {
		echo "<li>- $name</li>\n";
	}
	echo "</ul>\n";
	echo "</li>\n";
}
echo "<li><span class=italic>- Projects:</span><ul id=ul-simple style='margin-left:15px'>";
// get projects shared with this group:
$query = mysql_query("SELECT p.id, p.naam, p.chiptype FROM project p JOIN projectpermissiongroup ppg ON p.id = ppg.projectid WHERE ppg.groupid = '$gid'");
$numrowsp = mysql_num_rows($query);
if ($numrowsp == 0) {
	echo "<li>None</li>\n";
}
else {
	while ($row = mysql_fetch_array($query)) {
		$pname = $row['naam'];
		$pid = $row['id'];
		$pchip = $row['chiptype'];
		echo "<li>- $pname, $pchip : <a href='index.php?page=results&type=tree&collection=&project=$pid&sample=' target='_blank'>browse</a> / <a href='index.php?page=overviewbrowse&p=$pid' target='_blank'>overview</a></li>\n";
	}
}
echo "</ul>\n";
echo "</li>\n";
echo "<li> => <a href='index.php?page=group&type=mine&action=leave&gid=$gid' class=italic>Leave This Group</li>\n";
echo "</ul>\n";
?>
