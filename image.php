<?php
//Tell the browser what kind of file is coming in
header("Content-Type: image/jpeg");
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
#ob_start();
$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include(".LoadCredentials.php");
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
###################
# GET POSTED VARS #
###################
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$project = $_GET['p'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$userid = $_GET['u'];
$level = $_GET['l'];
$plotwidth = $_GET['pw'] ;
$custom = $_GET['cstm'] ;

##############################
# CHECK WHAT TO SHOW IN PLOT #
##############################
$prefq = mysql_query("SELECT DGVshow, DGVinclude, HapSame, HapAll,RefSeq, GCcode, GCncode, SegDup, LOHshow, LOHminsize, LOHminsnp, condense, condense_classes, condense_null, ecaruca,ecarucaSize,DECsyn,DECpat,annotations FROM usersettings WHERE id = '$userid'");
$prefrow = mysql_fetch_array($prefq);
$DGVshow = $prefrow['DGVshow'];
$DGVinc = $prefrow['DGVinclude'];
$HapSame = $prefrow['HapSame'];
$HapAll = $prefrow['HapAll'];
$RefSeq = $prefrow['RefSeq'];	
$GCcode = $prefrow['GCcode'];
$GCncode = $prefrow['GCncode'];
$SegDup = $prefrow['SegDup'];
$LOHshow = $prefrow['LOHshow'];
$LOHminsize = $prefrow['LOHminsize'];
$LOHminsnp = $prefrow['LOHminsnp'];
$Condense = $prefrow['condense'];
$ToCondense = $prefrow['condense_classes'];
$NullCondense = $prefrow['condense_null'];
$ecaruca = $prefrow['ecaruca'];
$ecarucaSize = $prefrow['ecarucaSize'];
$DECsyn = $prefrow['DECsyn'];
$DECpat = $prefrow['DECpat'];
$annotations = $prefrow['annotations'];
###################################
# DEFINE SOME VARS for IMAGE SIZE #
###################################
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$window = $stop-$start+1;
$nrcon = 12;

#mysql_select_db("decipher");
if ($DECsyn == 1) {
	$dquery = mysql_query("SELECT COUNT(ID) AS 'aantal' FROM decipher_syndromes WHERE Chr = '$chrtxt' AND ((Start BETWEEN '$start' AND '$stop') OR (END BETWEEN '$start' AND '$stop') OR (Start <= '$start' AND END >= '$stop'))");
	$result = mysql_fetch_array($dquery);
	$nrd = $result['aantal'];
}
else {
	$nrd = 0;
}
if ($DECpat == 1) {
	$pquery = mysql_query("SELECT COUNT(id) AS 'aantal' FROM decipher_patsum WHERE chr = '$chr' AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))");
	$result = mysql_fetch_array($pquery);
	$patrows = $result['aantal'];
	if ($patrows > 0) {
		$nrd = $nrd+1;
	}
}
#mysql_select_db("$db");

###########################
# DEFINE IMAGE PROPERTIES #
###########################
$condensepart = '';
if ($project != '' && $custom != 1) {
	$samplequery = mysql_query("SELECT COUNT(s.id) AS 'aantal' FROM projsamp ps JOIN sample s ON ps.idsamp = s.id WHERE ps.idproj = '$project'");
	$result = mysql_fetch_array($samplequery);
	$nrsamples = $result['aantal'];
	$height = 110 + $RefSeq*10 + $GCcode*10 + $GCncode*10 + $ecaruca*10 + $DECsyn*10 + $DECpat*10 + $HapSame*(6*10) + $HapAll*(6*10) + 6*10*$Condense + $nrd*10 + $nrsamples*10 + $DGVshow*50 + $SegDup*22;
	$scale = 3000000 ; // 3mb
	$stext = "3 Mb";

}
elseif ($project != '' && $custom == 1) {
	$samplequery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS 'aantal' FROM cus_aberration WHERE idproj = '$project'");
	$result = mysql_fetch_array($samplequery);
	$nrsamples = $result['aantal'];
	$height = 130 + $RefSeq*10 + $GCcode*10 + $GCncode*10 + $ecaruca*10 + $DECsyn*10 + $DECpat*10 +$HapSame*(6*10) + $HapAll*(6*10) + 6*10*$Condense + $nrd*10 + $nrsamples*10 + $DGVshow*50 + $SegDup*22;
	$scale = 3000000 ; // 3mb
	$stext = "3 Mb";
}
else {
	if ($Condense == 0) {
		//illumina results
		$abquery = mysql_query("SELECT COUNT(DISTINCT(a.sample)) AS 'aantal' FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON a.idproj = p.id AND s.id = a.sample AND pp.projectid = a.idproj WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id");
		$result = mysql_fetch_array($abquery);
		$nrabs = $result['aantal'];
		#$nrabs = 9;
		// custom results
		$abquery = mysql_query("SELECT COUNT(DISTINCT(a.sample)) AS 'aantal' FROM cus_aberration a JOIN cus_project p JOIN cus_projectpermission pp ON a.idproj = p.id AND a.idproj = pp.projectid WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Control'");
		$result = mysql_fetch_array($abquery);
		$cusabs = $result['aantal'];
		#$cusabs = 0;
		$height = 100 + $RefSeq*10 + $GCcode*10 + $GCncode*10 + $ecaruca*10 + $nrd*10 + $nrabs*10 + $cusabs*10 + 50 + $HapSame*(5*10) + $HapAll*(5*10) + $DGVshow*(5*10)+ $SegDup*22 ;
		#$height = 100 + $RefSeq*10 + $GCcode*10+  $GCncode*10 + $ecaruca*10;
	}
	elseif ($Condense == 1) {
		$NC = array('OR a.class IS NULL','AND a.class IS NOT NULL');
		$condensepart = "AND (NOT a.class IN ($ToCondense) $NC[$NullCondense])";
		$abquery = mysql_query("SELECT COUNT(DISTINCT(a.sample)) AS 'aantal' FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON a.idproj = p.id AND s.id = a.sample AND pp.projectid = a.idproj WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id $condensepart");
		$result = mysql_fetch_array($abquery);
		$nrabs = $result['aantal'];
		// custom results
		
		$abquery = mysql_query("SELECT COUNT(s.idsamp) AS 'aantal' FROM cus_aberration a JOIN cus_sample s JOIN cus_project p JOIN cus_projectpermission pp ON s.idsamp = a.sample AND s.idproj = a.idproj AND a.idproj = p.id AND a.idproj = pp.projectid WHERE pp.userid = '$userid' AND s.intrack = 1 AND a.chr = '$chr' $locquery AND p.collection <> 'Control' ");
		$result = mysql_fetch_array($abquery);
		$cusabs = $result['aantal'];
		$height = 100 + $RefSeq*10 + $GCcode*10 + $GCncode*10 + $ecaruca*10 + $nrd*10 + $DECsyn*10 + $DECpat*10 + $nrabs*10 + $cusabs*10 + 6*10*$Condense + 50 + $HapSame*(5*10) + $HapAll*(5*10) + $DGVshow*(5*10)+ $SegDup*22 ;
	}
	#if (($userid == 1 || $userid == 16) && $chr == 1) {
	#	$height = $height + 500;
	#}
	//$height = 60 + $nrabs*10;
	if ($window > 47000000) {
		$scale = 3000000;
		$stext = "3 Mb";
	}
	elseif ($window > 32000000) {
		$scale = 2000000;
		$stext = "2 Mb";
	}
	elseif ($window > 16000000) {
		$scale = 1000000;
		$stext = "1 Mb";

	}
	else {
		$wintresh = 8000000;
		$scale = 500000;
		$stext = "500";
		while ($window < $wintresh) {
			$scale = $scale / 2;
			$stext = $stext / 2;
			$wintresh = $wintresh / 2;
		}
		$stext = number_format($stext,1,'.','') . " Kb";
	} 
}
$xoff = 75; 
$scalef = ($plotwidth-$xoff-1)/($window);

##############################################
## add heigth for user provided annotations ##
##############################################
if ($userid == 1) {


}
##########################
# GET CENTROMER POSITION #
##########################
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];


#############################
# Create the image resource #
#############################
$width = $plotwidth; //Image width in pixels
$image = ImageCreate($width, $height);

#################
# CREATE COLORS #
#################
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$blue  = imageColorallocate($image,0,0,255); 
$darklightblue = ImageColorAllocate($image,122,189,255);
$lightblue = ImageColorAllocate($image,235,235,255);
$green = ImageColorAllocate($image,0,190,0);
$lightgreen = ImageColorAllocate($image,156,255,56);
$purple = ImageColorAllocate($image,136,34,135);
$lightpurple = ImageColorAllocate ($image, 208, 177, 236);
$orange = ImageColorAllocate($image,255, 179,0);
$lightorange = ImageColorAllocate($image,255,210,127);
$cyan = ImageColorAllocate($image,0,255,255);
$lightpink = ImageColorAllocate($image,205,183,181);
$pink = ImageColorAllocate($image,255,182,193);
$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);


$cnslight = array('0' => $lightred, '1' => $lightred, '2' => $lightorange, '3' => $darklightblue, '4' => $darklightblue,'9' => $lightpink);




$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue, '9' => $pink);
#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

// OUTPUT settings link
$fontwidth = imagefontwidth(2);
$pref = 'Preferences';
$txtwidth = strlen($pref)*$fontwidth;
$txtx = $width - $txtwidth - 3;
imagestring($image,2,$txtx,1,$pref,$gpos50);

# CREATE SCALE
$scaledscale = intval(round($scale*$scalef));
imagerectangle($image, 0, 0, 60, 20, $black);
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($stext)*$fontwidth;
$txtx = 30 -($txtwidth/2);
imagestring($image,3,$txtx,1,$stext,$black);
$xstart = 30-($scaledscale/2);
$xstop = 30+($scaledscale/2);
imageline($image,$xstart,18,$xstart,14,$black);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
#imagestring($image,3,$txtx,20,$project,$black);
# CREATE KARYO BANDS
$y = 25;  //start here

$arm = 'p';
$query = mysql_query("SELECT start, stop, name, gieStain FROM cytoBand WHERE chr= '$chr' $locquery ORDER BY start");

while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$name = $row['name'];
	$gieStain = $row['gieStain'];
	$scaledstart = intval(round(($cstart-$start)*$scalef));
	$scaledstop = intval(round(($cstop-$start)*$scalef));
	//$xend = $x + $scaled;
	if ($cstop == $lastp) {
		if ($gieStain != 'gneg') {
			imagefilledpolygon($image, array($xoff+$scaledstart,$y,$xoff+$scaledstop,($y+10),$xoff+$scaledstart,($y+20)),3, $colors[$gieStain]);
		}
		imagepolygon($image, array($xoff+$scaledstart,$y,$xoff+$scaledstop,$y+10,$xoff+$scaledstart,$y+20),3,$black);
		$nexttriang = 1;
	}
	elseif ($nexttriang == 1) {
		if ($gieStain != 'gneg') {
			imagefilledpolygon($image, array($xoff+$scaledstart,$y+10,$xoff+$scaledstop,$y,$xoff+$scaledstop,$y+20),3,$colors[$gieStain]);
		}
		imagepolygon($image, array($xoff+$scaledstart,$y+10,$xoff+$scaledstop,$y,$xoff+$scaledstop,$y+20),3,$black);
		$nexttriang = 0;
	}
	else{
		if ($gieStain != 'gneg') {
			imagefilledrectangle($image, $xoff+$scaledstart, $y, $xoff+$scaledstop, $y+20, $colors[$gieStain]);
		}
		imagerectangle($image, $xoff+$scaledstart, $y, $xoff+$scaledstop, $y+20, $black);
	}
	$fontwidth = imagefontwidth(3);
	$fullname = $chrtxt . $name;
	$txtwidth = strlen($fullname)*$fontwidth;
	if ($txtwidth+5 < ($scaledstop-$scaledstart)) {
		$txtx = $xoff + ($scaledstop+$scaledstart)/2 - $txtwidth/2;
		if ($gieStain != "gpos100") {
			imagestring($image,3,$txtx,$y+4,$fullname,$black);		
		}
		else {
			imagestring($image,3,$txtx,$y+4,$fullname,$white);
		}

	}
	
	
	//$x = $xend;

}
$y=50;

#############
# DRAW GRID #
#############
$scaledstop = intval(round(($cstop-$start)*$scalef));
$xline1 = intval(round(($start-$start)*$scalef));
$xline2 = intval(round((($window / 10))*$scalef));
$xline3 = intval(round((($window / 10)*2)*$scalef));
$xline4 = intval(round((($window / 10)*3)*$scalef));
$xline5 = intval(round((($window / 10)*4)*$scalef));
$xline6 = intval(round((($window / 10)*5)*$scalef));
$xline7 = intval(round((($window / 10)*6)*$scalef));
$xline8 = intval(round((($window / 10)*7)*$scalef));
$xline9 = intval(round((($window / 10)*8)*$scalef));
$xline10 = intval(round((($window / 10)*9)*$scalef));
$xline11 = intval(round(($stop-$start)*$scalef));

imageline($image,$xoff+$xline1,$y,$xoff+$xline1,$height,$lightblue);
imageline($image,$xoff+$xline2,$y,$xoff+$xline2,$height,$lightblue);
imageline($image,$xoff+$xline3,$y,$xoff+$xline3,$height,$lightblue);
imageline($image,$xoff+$xline4,$y,$xoff+$xline4,$height,$lightblue);
imageline($image,$xoff+$xline5,$y,$xoff+$xline5,$height,$lightblue);
imageline($image,$xoff+$xline6,$y,$xoff+$xline6,$height,$lightblue);
imageline($image,$xoff+$xline7,$y,$xoff+$xline7,$height,$lightblue);
imageline($image,$xoff+$xline8,$y,$xoff+$xline8,$height,$lightblue);
imageline($image,$xoff+$xline9,$y,$xoff+$xline9,$height,$lightblue);
imageline($image,$xoff+$xline10,$y,$xoff+$xline10,$height,$lightblue);
imageline($image,$xoff+$xline11,$y,$xoff+$xline11,$height,$lightblue);


# DRAW REFSEQ GENES
$y = 48;
imagestring($image,3,0,$y-5,"Genes",$black);
imagefilledrectangle($image,$xoff,$y+2,$plotwidth,$y+3,$gpos75);

if ($RefSeq == 1) {
  $y = $y+10;
  $genequery = mysql_query("SELECT start, stop, strand, omimID, morbidID,exonstarts,exonends FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");
  imagestring($image,2,0,$y-5,'RefSeq',$black);
  while ($generow = mysql_fetch_array($genequery)) {
	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$gomim = $generow['omimID'];
	$gmorbid = $generow['morbidID'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($gmorbid != '') {
		$gcolor = $red;
		$garrow = $black;
	}
	elseif ($gomim != '') {
		$gcolor = $blue;
		$garrow = $white;
	}
	else {
		$gcolor = $black;
		$garrow = $white;
	}
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y+2,$hooktip-2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y+2,$hooktip+2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $start) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $stop ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$start) * $scalef));
		$gexscaledstop = intval(round(($gexe-$start) * $scalef));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-1,$xoff+$gexscaledstop,$y+5,$gcolor);
	}

  }
}
# DRAW ENCODE CODING GENES	
if ($GCcode == 1) {
  $y = $y + 10;
  $genequery = mysql_query("SELECT start, stop, strand, level,exonstarts,exonends FROM encodesum WHERE coding = 1 AND chr = '$chr' $locquery ORDER BY start");
  imagestring($image,2,0,$y-5,'Enc.Coding',$black);
  while ($generow = mysql_fetch_array($genequery)) {
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$glevel = $generow['level'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($glevel == 1 ) {
		$gcolor = $black;
		$garrow = $black;
	}
	elseif ($glevel == 2) {
		$gcolor = $gpos50;
		$garrow = $white;
	}
	elseif ($glevel == 3) {
		$gcolor = $gpos25;
	}
	else {
		$gcolor = $black;
		$garrow = $white;
	}
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y+2,$hooktip-2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y+2,$hooktip+2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $start) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $stop ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$start) * $scalef));
		$gexscaledstop = intval(round(($gexe-$start) * $scalef));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-1,$xoff+$gexscaledstop,$y+5,$gcolor);
	}
  }
}
# DRAW ENCODE NON-CODING GENES
if ($GCncode == 1) {
  $y = $y + 10;
  $genequery = mysql_query("SELECT start, stop, strand, level,exonstarts,exonends FROM encodesum WHERE coding = 0 AND chr = '$chr' $locquery ORDER BY start");
  imagestring($image,2,0,$y-5,'Enc.N.Coding',$black);
  while ($generow = mysql_fetch_array($genequery)) {
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$glevel = $generow['level'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($glevel == 1 ) {
		$gcolor = $black;
		$garrow = $black;
	}
	elseif ($glevel == 2) {
		$gcolor = $gpos50;
		$garrow = $white;
	}
	elseif ($glevel == 3) {
		$gcolor = $gpos25;
	}
	else {
		$gcolor = $black;
		$garrow = $white;
	}
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y+2,$hooktip-2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y+2,$hooktip+2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $start) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $stop ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$start) * $scalef));
		$gexscaledstop = intval(round(($gexe-$start) * $scalef));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-1,$xoff+$gexscaledstop,$y+5,$gcolor);
	}
  }
}

# DRAW SEGMENTAL DUPLICATIONS
if ($SegDup == 1) {
	$y = $y +12;
	imagestring($image,3,0,$y-7,"SegDups",$black);
	imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
	$query = mysql_query("SELECT start, stop FROM segdup_sum WHERE chr = '$chr' $locquery");
	$nrows = mysql_num_rows($query);
	$y = $y+10;
	imagestring($image,2, 0,$y-5,'UCSC SegDups',$black);
	while ($row = mysql_fetch_array($query)) {
		$cstart = $row['start'];
		$cstop = $row['stop'];
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
	}
		

}

$y = $y + 12;
$chiptypes = array();
# DRAW SAMPLES FOR SINGLE PROJECT OVERVIEW
if ($project != "" && $custom != 1) {
	imagestring($image,3,0,$y-7,"Samples",$black);
	imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
	$samplequery = mysql_query("SELECT s.id, s.chip_dnanr, p.chiptypeid FROM projsamp ps JOIN sample s JOIN project p ON ps.idsamp = s.id AND ps.idproj = p.id WHERE ps.idproj = '$project' ORDER BY s.chip_dnanr");
	$nrrows = mysql_num_rows($samplequery);
	while($srow = mysql_fetch_array($samplequery)){
		$y = $y+10;
		$samplename = $srow['chip_dnanr'];
		$sid = $srow['id'];
		$chiptypeid = $srow['chiptypeid'];
		$chiptypes[$chiptypeid] = 1;
		$fontwidth = imagefontwidth(2);
		$txtwidth = strlen($samplename)*$fontwidth;
		$txtx = 10;
		imagestring($image,2,0,$y-5,$samplename,$black);
		$abquery = mysql_query("SELECT cn, start, stop, class, nrsnps,largestart,largestop FROM aberration WHERE sample = '$sid' AND idproj = '$project' AND chr='$chr' $locquery");
	
		while ($abrow = mysql_fetch_array($abquery)) {
			$cn = $abrow['cn'];
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			$cclass = $abrow['class'];
			$cnrsnps = $abrow['nrsnps'];
			$clstart = $abrow['largestart'];
			$clstop = $abrow['largestop'];
			$csize = $cstop - $cstart +1;
			if ($cn == 2 && ($LOHshow == 0 || $csize < $LOHminsize || $cnrsnps < $LOHminsnp )) {
				continue;
			}
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			if ($cclass == 5) {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos25);
			}
			elseif ($cclass == 4) {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnslight[$cn]);
			}
			else {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$cn]);
			}
			if ($cclass == 1 || $cclass == 2) {
				imagerectangle($image,$xoff+$scaledstart-2,$y-2,$xoff+$scaledstop+2,$y+7,$black);
			}
			## max region 'error' bars
			if ($clstart != '' && $clstop != '') {
				if ($clstart < $start) {
					$vstart = $start;
				}
				else {
					$vstart = $clstart;
				}
				if ($clstop > $stop) {
					$vstop = $stop;
				}
				else {
					$vstop = $clstop;
				}
				$scaledvstart =  intval(round(($vstart-$start) * $scalef));
				$scaledvstop = intval(round(($vstop-$start) * $scalef));
				# 5' if $cstart > $start
				if ($cstart > $start) {
					# horizontal
					imagefilledrectangle($image,$xoff+$scaledvstart,$y+2,$xoff+$scaledstart,$y+3,$cnslight[$cn]);
					# vertical
					if ($clstart >= $start) {
						imageline($image,$xoff+$scaledvstart,$y,$xoff+$scaledvstart,$y+5,$cnslight[$cn]);
					}
				}
				# 3' horizontal if $cstop < $stop
				if ($cstop < $stop) {
					# horizontal 
					imagefilledrectangle($image,$xoff+$scaledstop,$y+2,$xoff+$scaledvstop,$y+3,$cnslight[$cn]);
					# vertical 5'
					if ($clstop <= $stop) {
						imageline($image,$xoff+$scaledvstop,$y,$xoff+$scaledvstop,$y+5,$cnslight[$cn]);
					}
				}
			}
		}
	
	}
	
	
}

# DRAW SAMPLES FOR SINGLE CUSTOM PROJECT OVERVIEW
elseif ($project != "" && $custom == 1) {
	imagestring($image,3,0,$y-7,"Samples",$black);
	imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
	$samplequery = mysql_query("SELECT DISTINCT(sample) as sample FROM cus_aberration WHERE idproj = '$project' ORDER BY sample");
	while($srow = mysql_fetch_array($samplequery)){
		$y = $y+10;
		$samplename = $srow['sample'];
		$fontwidth = imagefontwidth(2);
		$txtwidth = strlen($samplename)*$fontwidth;
		$txtx = 10;
		imagestring($image,2,0,$y-5,$samplename,$black);
		$abquery = mysql_query("SELECT cn, start, stop, class FROM cus_aberration WHERE sample = '$samplename' AND idproj = '$project' AND chr='$chr' $locquery");
	
		while ($abrow = mysql_fetch_array($abquery)) {
			$cn = $abrow['cn'];
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			$cclass = $abrow['class'];
			$csize = $cstop - $cstart +1;
			if (preg_match("/gain/i",$cn)) {
				$cn = 3;
			}
			elseif (preg_match("/los/i",$cn)) {
				$cn = 1;
			}
			elseif (preg_match("/loh/i",$cn)) {
				$cn = 2;
			}
			if ($cn == 2 && ($LOHshow == 0 || $csize < $LOHminsize )) {
				continue;
			}
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			if ($cclass == 5) {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos25);
			}
			elseif ($cclass == 4) {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnslight[$cn]);
			}
			else {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$cn]);
			}
			if ($cclass == 1 || $cclass == 2) {
				//imagerectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
				imagerectangle($image,$xoff+$scaledstart-2,$y-2,$xoff+$scaledstop+2,$y+7,$black);
			}

		}
	
	}
}
// SEARCH RESULTS BY GENE or REGION	
else {
	// ILLUMINA results
	imagestring($image,3,0,$y-7,"Samples",$black);
	imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);	
	$querystring = "SELECT s.chip_dnanr, a.start, a.stop, a.cn, a.nrsnps, p.chiptypeid, a.class, a.largestart, a.largestop FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON a.idproj = p.id AND s.id = a.sample AND pp.projectid = a.idproj WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id $condensepart ORDER BY s.chip_dnanr ";	
	$abquery = mysql_query("$querystring");
	$prevsample = "";
	while ($abrow = mysql_fetch_array($abquery)) {
		$cn = $abrow['cn'];
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		$csize = $cstop - $cstart +1;
		$sample = $abrow['chip_dnanr'];
		$chiptypeid = $abrow['chiptypeid'];
		$cnrsnps = $abrow['nrsnps'];
		$cclass = $abrow['class'];
		$clstart = $abrow['largestart'];
		$clstop = $abrow['largestop'];		
		if ($cn == 2 && ($LOHshow == 0 || $csize < $LOHminsize || $cnrsnps < $LOHminsnp )) {
				continue;
			}

		$chiptypes[$chiptypeid] = 1;

		if ($sample != $prevsample) {
			$y = $y+10;
			$prevsample = $sample;
		}
		imagestring($image,2,0,$y-5,$sample,$black);
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		if ($cclass == 5) {
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos25);
		}
		elseif ($cclass == 4) {
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnslight[$cn]);	
		}
		else {
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$cn]);
		}
		if ($cclass == 1 || $cclass == 2) {
			//imagerectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
			imagerectangle($image,$xoff+$scaledstart-2,$y-2,$xoff+$scaledstop+2,$y+7,$black);
				
		}
		## max region 'error' bars
		if ($clstart != '' && $clstop != '') {
			if ($clstart < $start) {
					$vstart = $start;
				}
				else {
					$vstart = $clstart;
				}
				if ($clstop > $stop) {
					$vstop = $stop;
				}
				else {
					$vstop = $clstop;
				}

			$scaledvstart =  intval(round(($vstart-$start) * $scalef));
			$scaledvstop = intval(round(($vstop-$start) * $scalef));
			# 5' if $cstart > $start
			if ($cstart > $start) {
				# horizontal
				imagefilledrectangle($image,$xoff+$scaledvstart,$y+2,$xoff+$scaledstart,$y+3,$cnslight[$cn]);
				# vertical
				if ($clstart >= $start) {
					imageline($image,$xoff+$scaledvstart,$y,$xoff+$scaledvstart,$y+5,$cnslight[$cn]);
				}
			}
			# 3' horizontal if $cstop < $stop
			if ($cstop < $stop) {
				# horizontal 
				imagefilledrectangle($image,$xoff+$scaledstop,$y+2,$xoff+$scaledvstop,$y+3,$cnslight[$cn]);
				# vertical 5'
				if ($clstop <= $stop) {
					imageline($image,$xoff+$scaledvstop,$y,$xoff+$scaledvstop,$y+5,$cnslight[$cn]);
				}
			}
		}
		//$y = $y+10;
	}
	// Condensed Results
	if ($Condense == 1) {
		$NC = array('AND a.class IS NOT NULL','OR a.class IS NULL');
		$condensepart = "AND (a.class IN ($ToCondense) $NC[$NullCondense])";
		#imagestring($image,2,50,$y+20,$condensepart,$black);
		$y = $y+12;
		$string = "Class $ToCondense";
		$fontwidth = imagefontwidth(3);
		$txtwidth = strlen($string)*$fontwidth;
		if ($NullCondense == 1) {
			$string .= ",NA";
		}
		imagestring($image,3,0,$y-7,"$string",$black);
		if ($txtwidth > $xoff) {
			$add = $txtwidth - $xoff;
		}
		else {
			$add = 0;
		}
		imagefilledrectangle($image,$xoff+$add,$y,$plotwidth,$y+1,$gpos75);	
		$arr = array(0,1,2,3,4,5);
		foreach ($arr as $i) {
			$cn = $i;
			$y = $y+10;
			if ($i == 5) {
				$string = "Cond.-FP";
			}
			else {
				$string = "Cond.-CN:$i";
			}
			imagestring($image,2,0,$y-5,$string,$black);
			$querystring = "SELECT a.start, a.stop, a.nrsnps, p.chiptypeid, a.class FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON a.idproj = p.id AND s.id = a.sample AND pp.projectid = a.idproj WHERE a.cn = '$i' AND pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id $condensepart";
			
			$abquery = mysql_query("$querystring");
			$nrrows = mysql_num_rows($abquery);
			$prevsample = "";
			while ($abrow = mysql_fetch_array($abquery)) {
				#$cn = $abrow['cn'];
				$cstart = $abrow['start'];
				$cstop = $abrow['stop'];
				$csize = $cstop - $cstart +1;
				#$sample = $abrow['chip_dnanr'];
				$chiptypeid = $abrow['chiptypeid'];
				$cnrsnps = $abrow['nrsnps'];
				#$cclass = $abrow['class'];		
				if ($cn == 2 && ($LOHshow == 0 || $csize < $LOHminsize || $cnrsnps < $LOHminsnp )) {
					continue;
				}
	
				$chiptypes[$chiptypeid] = 1;
		
				#if ($sample != $prevsample) {
				#	$y = $y+10;
				#	$prevsample = $sample;
				#}
				#imagestring($image,2,0,$y-5,$sample,$black);
				if ($cstart < $start) {
					$cstart = $start;
				}
				if ($cstop > $stop) {
					$cstop = $stop;
				}
				$scaledstart = intval(round(($cstart-$start) * $scalef));
				$scaledstop = intval(round(($cstop-$start) * $scalef));
				#if ($cclass == 4) {
				#	imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnslight[$cn]);	
				#}
				#else {
				if ($cclass == 5) {
					imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos25);
				}
				else {
					imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$cn]);
				}
				#}
				#if ($cclass == 1 || $cclass == 2) {
					//imagerectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
					#imagerectangle($image,$xoff+$scaledstart-2,$y-2,$xoff+$scaledstop+2,$y+7,$black);
				
				#}
			}
		}
	}	
	// CUSTOM results
	if ($cusabs > 0) {
		$y = $y+10;
		imagestring($image,3,0,$y-5,"Custom Results",$black);
		imagefilledrectangle($image,$xoff+23,$y+1,$plotwidth,$y+2,$gpos75);	
		$querystring = "SELECT s.idsamp, a.start, a.stop, a.cn, a.class FROM cus_aberration a JOIN cus_sample s JOIN cus_project p JOIN cus_projectpermission pp ON s.idsamp = a.sample AND a.idproj = s.idproj AND a.idproj = p.id AND a.idproj = pp.projectid WHERE pp.userid = '$userid' AND s.intrack = 1 AND a.chr = '$chr' $locquery AND p.collection <> 'Control' ORDER BY s.idsamp ";	
		$abquery = mysql_query("$querystring");
		$prevsample = "";
		$nrabs = mysql_num_rows();
		while ($abrow = mysql_fetch_array($abquery)) {
			$cn = $abrow['cn'];
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			$csize = $cstop - $cstart +1;
			$sample = $abrow['idsamp'];
			$cclass = $abrow['class'];		
			if (preg_match("/gain/i",$cn)) {
				$cn = 3;
			}
			elseif (preg_match("/los/i",$cn)) {
				$cn = 1;
			}
			elseif (preg_match("/loh/i",$cn)) {
				$cn = 2;
			}
	
			if ($cn == 2 && ($LOHshow == 0 || $csize < $LOHminsize )) {
				continue;
			}
	
			if ($sample != $prevsample) {
				$y = $y+10;
				$prevsample = $sample;
			}
			imagestring($image,2,0,$y-5,$sample,$black);
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			if ($cclass == 5) {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos25);
			}
			elseif ($cclass == 4) {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnslight[$cn]);	
			}
			else {
				imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$cn]);
			}
			if ($cclass == 1 || $cclass == 2) {
				//imagerectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
				imagerectangle($image,$xoff+$scaledstart-2,$y-2,$xoff+$scaledstop+2,$y+7,$black);
			}
		}
	}
	
	
}

#imagefilledrectangle($image,771,181,785,188,$black);
// DECIPHER 
if ($DECsyn == 1 || $DECpat == 1) {
	$y = $y+10;
	imagestring($image,3,0,$y-5,"DECIPHER",$black);
	imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
	 #mysql_select_db("decipher");
  	// syndromes
  	#$decquery = str_replace('stop', 'End', "$locquery");

}
if ($DECsyn == 1) {
  $abquery = mysql_query("SELECT Start, End, Variance, Name FROM decipher_syndromes WHERE Chr = '$chrtxt' $decquery ORDER BY Start");
  while ($abrow = mysql_fetch_array($abquery)) {
	$y = $y+10;
	$cstart = $abrow['Start'];
	$cstop = $abrow['End'];
	$variance = $abrow['Variance'];
	$name = $abrow['Name'];
	$namesub = substr($name,0,8) . "...";
	imagestring($image,2,0,$y-5,$namesub,$black);
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	if ($variance == 'ins') {
		$color = $blue;
	}
	else {
		$color = $red;
	}
	imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$color);
  }
}
if ($DECpat == 1) {
  // patients
  $abquery = mysql_query("SELECT start, stop FROM decipher_patients WHERE chr = '$chr' $locquery ORDER BY start");
  $patrows = mysql_num_rows($abquery);
  if ($patrows > 0) {
   $y = $y + 10;
   imagestring($image,2,0,$y-5,"Patients",$black);
   while ($abrow = mysql_fetch_array($abquery)) {
	//$y = $y+10;
	$cstart = $abrow['start'];
	$cstop = $abrow['stop'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos50);
   }
  }
}
#mysql_select_db($db);	

/////////////
// ECARUCA //
/////////////
if ($ecaruca == 1) {
	$y = $y+10;
	imagestring($image,3,0,$y-5,"ECARUCA",$black);
	imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
	$abquery = mysql_query("SELECT start, stop FROM ecaruca WHERE chr = '$chr' $locquery AND (stop - start ) < $ecarucaSize ORDER BY start");
	$patrows = mysql_num_rows($abquery);
	if ($patrows > 0) {
	   $y = $y + 10;
	   imagestring($image,2,0,$y-5,"Patients",$black);
	   while ($abrow = mysql_fetch_array($abquery)) {
		//$y = $y+10;
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos50);
	   }
	}
}
/////////////
// PARENTS //
/////////////
$y = $y+10;
imagestring($image,3,0,$y-5,"Healthy Parents",$black);
imagefilledrectangle($image,$xoff+35,$y+2,$plotwidth,$y+3,$gpos75);
if ($chr == 23) {
	$arr = array(0,1,2,3,4);
}
else {
	$arr = array(0,1,3,4);
}
foreach ($arr as $i) {
	$y = $y+10;
	$string = "Parent-CN:$i";
	imagestring($image,2,0,$y-5,$string,$black);
	$querystring = "SELECT a.start, a.stop FROM aberration a JOIN sample s JOIN project p JOIN projectpermission pp ON s.id = a.sample AND a.idproj = p.id AND a.idproj = pp.projectid WHERE  s.intrack = 1 AND s.trackfromproject = p.id AND pp.userid = $userid AND p.collection = 'Parents' AND chr = '$chr' AND cn = '$i' $locquery ";
	$abquery = mysql_query("$querystring");
	while ($abrow = mysql_fetch_array($abquery)) {
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$i]);
	}

}


// CONTROLS OVERVIEW
ksort($chiptypes);
$chipstring = '';
$chiparray = array();
foreach($chiptypes as $key => $value) {
	if ($key == 4) {
		$key = 10; # Cyto12v2.0 failed, substitute with 2.1
	}
	$chipq = mysql_query("SELECT Controles from chiptypes WHERE ID = '$key'");
	$row = mysql_fetch_array($chipq);
	if ($row['Controles'] == '') {
		continue;
	}
	$chiparray[$key] = $key;
}
sort($chiparray);
$chipstring = implode('-',$chiparray);
if ($HapSame == 1) {
	$y = $y+10;
	imagestring($image,3,0,$y-5,"Same Chip Control",$black);
	imagefilledrectangle($image,$xoff+42,$y+2,$plotwidth,$y+3,$gpos75);
	if ($chr == 23) {
		$arr = array(0,1,2,3,4);
	}
	else {
		$arr = array(0,1,3,4);
	}
	foreach ($arr as $i) {
		$y = $y+10;
		$string = "Contr-CN:$i";
		imagestring($image,2,0,$y-5,$string,$black);
		$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
		$abquery = mysql_query("$querystring");
		while ($abrow = mysql_fetch_array($abquery)) {
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$i]);
		}
	
	}
}
if ($HapAll == 1) {
	$chipq = mysql_query("SELECT ID,Controles FROM chiptypes");
	$chipstring = '';
	while ($row = mysql_fetch_array($chipq)) {
		if ($row['Controles'] == '') {
			continue;
		}
		$chipstring .= $row['ID'] . '-';
	}
	$chipstring = substr($chipstring,0,-1);
	$y = $y+10;
	imagestring($image,3,0,$y-5,"All Chip Control",$black);
	imagefilledrectangle($image,$xoff+35,$y+2,$plotwidth,$y+3,$gpos75);
	if ($chr == 23) {
		$arr = array(0,1,2,3,4);
	}
	else {
		$arr = array(0,1,3,4);
	}
	foreach ($arr as $i) {
		$y = $y+10;
		$string = "Contr-CN:$i";
		imagestring($image,2,0,$y-5,$string,$black);
		$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
		$abquery = mysql_query("$querystring");
		while ($abrow = mysql_fetch_array($abquery)) {
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cns[$i]);
		}
	}
}
if ($DGVshow == 1) {
	$y = $y+10;	
	$string = "Toronto DGV";
	imagefilledrectangle($image,$xoff+1,$y+2,$plotwidth,$y+3,$gpos75);
	imagestring($image,3,0,$y-5,$string,$black);

	# LOAD DGV TRACK for selected studies
	$dgvtypes = array(1 => "gain", 2 => "loss", 3 => "inv");
	$dgvcolors = array(1 => $blue, 2 => $red, 3 => $gpos50);
	$dgvtitles = array(1 => "CN Gain", 2 => "CN Loss", 3 => "Inversion");
	for ($i = 1; $i <= 3; $i++) {
		$y = $y+10;
		$string = "$dgvtitles[$i]";
		imagestring($image,2,0,$y-5,$string,$black);

		$query = mysql_query("SELECT start, stop FROM DGV_full WHERE chr = '$chr' AND $dgvtypes[$i] > 0 AND study IN ($DGVinc) $locquery ORDER BY start, stop");
		#imagestring($image,2,$xoff,$y-5,$qstring,$black);	
		while ($abrow = mysql_fetch_array($query)) {
			#$y = $y+10;
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$dgvcolors[$i]);
		}
	}
	
}
// enable antialias
if (function_exists('imageantialias')) {
	imageantialias($image,true);
}

//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

