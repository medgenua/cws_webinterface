<?php
//syntax
?>
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>

<?php
if ($loggedin != 1) {
	include('login.php');
	exit;
}
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_end_flush();
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


######################
## POSTED VARIABLES ##
######################
$aid = $_GET['aid'];


// tmp part
if ($aid == '') {
	$aid = $_POST['aid'];
}
echo "<div class=sectie><h3>Check LOH regions for UPD</h3>";

/*
if ($level == 3) {
$query = mysql_query("select a.id, chr, start, stop, size, chip_dnanr from aberration a JOIN sample s ON a.sample = s.id where cn = 2 AND a.sample IN (select id from parents_relations) ORDER BY size desc");
echo "<p>select your region: <form action='index.php?page=PoO&cn=2' method=POST><select name=aid>";
$next = 0;
while ($row = mysql_fetch_array($query)) {
	if ($next == 1) {
		$next = 0;
		$link = "index.php?page=PoO&cn=2&aid=".$row['id'];
	}
	if ($row['id'] == $aid) {
		$selected = 'selected';
		$next = 1;
	}
	else {
		$selected ='';
	}
	echo "<option value=".$row['id']." $selected>".$row['chip_dnanr']." : chr".$row['chr'] .":".$row['start'].'-'.$row['stop'].' ('.$row['size'].'bp)</option>';
	
}

echo "</select> : <input type=submit class=button name=submit value='SELECT'>  </form></p>";
echo "<p>(<a href='$link'>Next</a>)</p>";
}
*/
##########################
## GET DETAILS AND DATA ##
##########################
# cnv details
$query = mysql_query("SELECT a.start, a.stop, a.chr, a.sample, a.idproj, s.chip_dnanr FROM aberration a JOIN sample s ON a.sample = s.id WHERE a.id = $aid");
$arow = mysql_fetch_array($query);
$start = $arow['start'];
$stop = $arow['stop'];
$chr = $arow['chr'];
$pid = $arow['idproj'];
$sid = $arow['sample'];
$sample = $arow['chip_dnanr'];
echo "<p>Currenly looking at sample $sample, region chr".$chromhash[$chr] .":$start-$stop</p>";
#datapoints
$query = mysql_query("SELECT content FROM datapoints WHERE id = $aid");
$drow = mysql_fetch_array($query);
$content = $drow['content'];
unset($drow);
# parental info
$query = mysql_query("SELECT father, mother FROM parents_relations where id = $sid");
$row = mysql_fetch_array($query);
$fid = $row['father'];
$mid = $row['mother'];
#paternal datapoints
$query = mysql_query("SELECT parent_seen, parent_aid FROM parent_offspring_cnv_relations WHERE aid = $aid AND parent_sid = $fid");
$row = mysql_fetch_array($query);
$fseen = $row['parent_seen'];
$faid = $row['parent_aid'];
if ($fseen == 1) {
	$table = 'datapoints';
}
else {
	$table = 'parents_datapoints';
}
$query = mysql_query("SELECT content FROM $table WHERE id = $faid");
$drow = mysql_fetch_array($query);
$fcontent = $drow['content'];
unset($drow);
#maternal datapoints
$query = mysql_query("SELECT parent_seen, parent_aid FROM parent_offspring_cnv_relations WHERE aid = $aid AND parent_sid = $mid");
$row = mysql_fetch_array($query);
$mseen = $row['parent_seen'];
$maid = $row['parent_aid'];
if ($mseen == 1) {
	$table = 'datapoints';
}
else {
	$table = 'parents_datapoints';
}
$query = mysql_query("SELECT content FROM $table WHERE id = $maid");
$drow = mysql_fetch_array($query);
$mcontent = $drow['content'];
unset($drow);
## PROCESS DATAPOINTS
$idata = explode('_',$content);
$inrelements = count($idata);
$ibaf = array();
for ($i = 0;$i < ($inrelements - 1);$i+=3) {
	if ($idata[$i] >= $start && $idata[$i] <= $stop) {
		$ibaf[$idata[$i]] = $idata[$i+2];
	}
}
$inrelements = count($ibaf);
unset($content);
$fdata = explode('_',$fcontent);
$nrelements = count($fdata);
$fbaf = array();
for ($i = 0;$i < ($nrelements - 1);$i+=3) {
	
	$fbaf[$fdata[$i]] = $fdata[$i+2];
}
unset($fcontent);
$mdata = explode('_',$mcontent);
$nrelements = count($mdata);
$mbaf = array();
for ($i = 0;$i < ($nrelements - 1);$i+=3) {
	$mbaf[$mdata[$i]] = $mdata[$i+2];
}
unset($mcontent);

## COMPARE DATAPOINTS
ksort($ibaf);
# working with baf values: suppose (including): 
#  - BAF < 0.25  => AA
#  - BAF > 0.75  => BB
#  - 0.375 < BAF <  0.625   => AB
echo "<p>Below is an overview of the informative genotypes for UPD detection (if any). In total $inrelements datapoints were considered.</p><p>";
echo "<table cellspacing=0>";
echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>Child</th><th class=topcellalt>Father</th><th class=topcellalt>Mother</th><th class=topcellalt>Status</th></tr>";
foreach ($ibaf as $pos => $ibval) {
	$status = 1;
	$igt = '';
	$fgt = '';
	$mgt = '';
	$fbval = $fbaf[$pos];
	$mbval = $mbaf[$pos];
	if ($fbval == '' || $mbval == '') {
		#$status = 'parental data missing';
		continue;
	}
	if ($ibval >= 0.375 && $ibval <= 0.625) {
		$igt = 'AB';
		# heterozygous index, parental check
		if (($fbval <= 0.25 && $mbval <= 0.25) && ($fbval >= 0.75 && $mbval >= 0.75)) {
			# parents homozygous
			$status = 'PARENTAL CHECK';
			if ($fbval <= 0.25) {
				$fgt = 'AA';
			}
			else {
				$fgt = 'BB';
			}
			if ($mbval <= 0.25) {
				$mgt = 'AA';
			}
			else {
				$mgt = 'BB';
			} 
		}
		elseif ($fbval <= 0.25 && $mbval >= 0.75) {
			$fgt = "AA";
			$mpgt = "BB";
			$status = 'Biparental Inheritance';
		}
		elseif ($fbval >= 0.75 && $mbval <= 0.25) {
			$fgt = 'BB';
			$mgt = 'AA';
			$status = 'Biparental Inheritance';
		}
		else {
			# ok
			continue;
		}
	}
	else {
		if ($ibval <= 0.25) {
			$igt = 'AA';
			if ($fbval <= 0.25 && $mbval >= 0.75) {
				$fgt = 'AA';
				$mgt = 'BB';
				$status = 'UPD-Paternal';
			}
			elseif ($fbval >= 0.375 && $fbval <= 0.625 && $mbval >= 0.75) {
				$fgt = 'AB';
				$mgt = 'BB';
				$status = 'UPD-Paternal';
			}
			elseif ($fbval >= 0.75 && $mbval >= 0.375 && $mbval <= 0.625) {
				$fgt = 'BB';
				$mgt = 'AB';
				$status = 'UPD-Maternal';
			}
			elseif ($fbval >= 0.75 && $mbval <= 0.25) {
				$fgt = 'BB';
				$mgt = 'AA';
				$status = 'UPD-Maternal';
			}
			else {
				# is ok
				/*
				if ($fbval >= 0.75) {
					$fgt = 'BB';
				}
				elseif ($fbval <= 0.25) {
					$fgt = 'AA';
				}
				elseif( $fbval >= 0.375 && $fbval <= 0.625) {
					$fgt = 'AB';
				}
				if ($mbval >= 0.75) {
					$mgt = 'BB';
				}
				elseif ($mbval <= 0.25) {
					$mgt = 'AA';
				}
				elseif( $mbval >= 0.375 && $mbval <= 0.625) {
					$mgt = 'AB';
				}
				*/
				continue;
			}
		}
		elseif ($ibval >= 0.75) {
			$igt = 'BB';
			if ($fbval <= 0.25 && $mbval >= 0.75) {
				$fgt = 'AA';
				$mgt = 'BB';
				$status = 'UPD-Maternal';
			}
			elseif ($fbval >= 0.375 && $fbval <= 0.625 && $mbval <= 0.25) {
				$fgt = 'AB';
				$mgt = 'AA';
				$status = 'UPD-Paternal';
			}
			elseif ($fbval <= 0.25 && $mbval >= 0.375 && $mbval <= 0.625) {
				$fgt = 'AA';
				$mgt = 'AB';
				$status = 'UPD-Maternal';
			}
			elseif ($fbval >= 0.75 && $mbval <= 0.25) {
				$fgt = 'BB';
				$mgt = 'AA';
				$status = 'UPD-Paternal';
			}
			else {
				/*
				if ($fbval >= 0.75) {
					$fgt = 'BB';
				}
				elseif ($fbval <= 0.25) {
					$fgt = 'AA';
				}
				elseif( $fbval >= 0.375 && $fbval <= 0.625) {
					$fgt = 'AB';
				}
				if ($mbval >= 0.75) {
					$mgt = 'BB';
				}
				elseif ($mbval <= 0.25) {
					$mgt = 'AA';
				}
				elseif( $mbval >= 0.375 && $mbval <= 0.625) {
					$mgt = 'AB';
				}
				*/
				# is ok. non informative
					
				#continue;
			}
		}
		else {
			# unclear values
			$status = 'UNCLEAR VALUES';
			$igt = "baf:'$ibval'";
			$fgt = "baf:$fbval";
			$mgt = "baf:$mbval";
			#continue;
		}
	}
	if ($status != '1') {
		echo "<tr><td $firstcell onmouseover=\"Tip(ToolTip('$aid','1','i',0,event))\" onmouseout=\"UnTip()\">$pos</td><td>$igt</td><td>$fgt</td><td>$mgt</td><td>$status</td></tr>";
		flush;
	}
}
echo "</table>";
echo" </p>";
#print "index datapoints:<br/><br/>$content<br/><br/>";
#print "father datapoints:<br/><br/>$fcontent<br/><br/>";
#print "mother datapoints:<br/><br/>$mcontent<br/><br/>";
?>
<div id="txtHint">something must appear here</div>
<!-- context menu loading -->

