<?php
//syntax
?>
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>

<?php
if ($loggedin != 1) {
	include('login.php');
}
else {
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_end_flush();
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


######################
## POSTED VARIABLES ##
######################
$aid = $_GET['aid'];


// tmp part
if ($aid == '') {
	$aid = $_POST['aid'];
}
echo "<div class=sectie><h3>Parent of Origin : Deletion</h3>";


##########################
## GET DETAILS AND DATA ##
##########################
# cnv details
$query = mysql_query("SELECT a.start, a.stop, a.chr, a.sample, a.idproj, s.chip_dnanr FROM aberration a JOIN sample s ON a.sample = s.id WHERE a.id = $aid");
$arow = mysql_fetch_array($query);
$start = $arow['start'];
$stop = $arow['stop'];
$chr = $arow['chr'];
$pid = $arow['idproj'];
$sid = $arow['sample'];
$sample = $arow['chip_dnanr'];
#echo "<p>Currenly looking at sample $sample, region chr".$chromhash[$chr] .":$start-$stop</p>";
#datapoints
$query = mysql_query("SELECT content FROM datapoints WHERE id = $aid");
$drow = mysql_fetch_array($query);
$content = $drow['content'];
unset($drow);
# parental info
$query = mysql_query("SELECT father, mother FROM parents_relations where id = $sid");
$row = mysql_fetch_array($query);
$fid = $row['father'];
$mid = $row['mother'];

##################################
## DEFAULT BOTH PARENTS PRESENT ##
##################################
if ($fid != 0 && $mid != 0) {
	# parental samplenames
	$query =  mysql_query("SELECT chip_dnanr FROM sample WHERE id = $fid");
	$row = mysql_fetch_array($query);
	$fname = $row['chip_dnanr'];
	$query =  mysql_query("SELECT chip_dnanr FROM sample WHERE id = $mid");
	$row = mysql_fetch_array($query);
	$mname = $row['chip_dnanr'];
	echo "<p><span class=nadruk>Region:</span> chr".$chromhash[$chr] .":".number_format($start,0,'',',') . "-". number_format($stop,0,'',',')."<br/>";
	echo "<span class=nadruk>Child:</span> $sample<br/>";
	echo "<span class=nadruk>Father:</span> $fname<br/>";
	echo "<span class=nadruk>Mother:</span> $mname<br/>";
	echo "</p>";

	#paternal datapoints
	$query = mysql_query("SELECT parent_seen, parent_aid FROM parent_offspring_cnv_relations WHERE aid = $aid AND parent_sid = $fid");
	$row = mysql_fetch_array($query);
	$fseen = $row['parent_seen'];
	$faid = $row['parent_aid'];
	if ($fseen == 1) {
		$table = 'datapoints';
	}
	else {
		$table = 'parents_datapoints';
	}
	$query = mysql_query("SELECT content FROM $table WHERE id = $faid");
	$drow = mysql_fetch_array($query);
	$fcontent = $drow['content'];
	unset($drow);
	#maternal datapoints
	$query = mysql_query("SELECT parent_seen, parent_aid FROM parent_offspring_cnv_relations WHERE aid = $aid AND parent_sid = $mid");
	$row = mysql_fetch_array($query);
	$mseen = $row['parent_seen'];
	$maid = $row['parent_aid'];
	if ($mseen == 1) {
		$table = 'datapoints';
	}
	else {
		$table = 'parents_datapoints';
	}
	$query = mysql_query("SELECT content FROM $table WHERE id = $maid");
	$drow = mysql_fetch_array($query);
	$mcontent = $drow['content'];
	unset($drow);
	
	## PROCESS DATAPOINTS
	$idata = explode('_',$content);
	$inrelements = count($idata);
	$ibaf = array();
	for ($i = 0;$i < ($inrelements - 1);$i+=3) {
		if ($idata[$i] >= $start && $idata[$i] <= $stop) {
			$ibaf[$idata[$i]] = $idata[$i+2];
		}
	}
	$inrelements = count($ibaf);

	unset($content);
	$fdata = explode('_',$fcontent);
	$nrelements = count($fdata);
	$fbaf = array();
	for ($i = 0;$i < ($nrelements - 1);$i+=3) {
	
		$fbaf[$fdata[$i]] = $fdata[$i+2];
	}
	unset($fcontent);
	$mdata = explode('_',$mcontent);
	$nrelements = count($mdata);
	$mbaf = array();
	for ($i = 0;$i < ($nrelements - 1);$i+=3) {
		$mbaf[$mdata[$i]] = $mdata[$i+2];
	}
	unset($mcontent);

	## COMPARE DATAPOINTS
	ksort($ibaf);
	# working with baf values: suppose (including): 
	#  - BAF < 0.25  => AA
	#  - BAF > 0.75  => BB
	#  - 0.375 < BAF <  0.625   => AB
	echo "<p>Below is an overview of the mendelian errors that are considered informative genotypes for Parent of Origin detection (if any). In total $inrelements datapoints were considered.</p><p><span class=bold>Note:</span> The offspring genotypes are arbitrarily set to A or B instead of AA/BB because a deletion event is assumed.</p><p>";
	echo "<table cellspacing=0>";
	echo "<tr><th class=topcell $firstcell>Position</th><th class=topcell>Child</th><th class=topcell>Father</th><th class=topcell>Mother</th><th class=topcell>Status</th></tr>";
	foreach ($ibaf as $pos => $ibval) {
		$status = 1;
		$igt = '';
		$fgt = '';
		$mgt = '';
		$fbval = $fbaf[$pos];
		$mbval = $mbaf[$pos];
		if ($fbval == '' || $mbval == '') {
			#$status = 'parental data missing';
			continue;
		}
		# heterozygous is not possible for deletions !
		if ($ibval > 0.25 && $ibval < 0.75) {
			# unclear values
			continue;
		}
		else {
			if ($ibval <= 0.25 ) {
				$igt = 'A';
				if ($fbval <= 0.25 && $mbval >= 0.75) {
					$fgt = 'AA';
					$mgt = 'BB';
					#informative !
					$status = 'Maternal';
				}
				elseif ($fbval >= 0.375 && $fbval <= 0.625 && $mbval >= 0.75) {
					$fgt = 'AB';
					$mgt = 'BB';
					#informative !
					$status = 'Maternal';
				}
				elseif ($fbval >= 0.75 && $mbval >= 0.375 && $mbval <= 0.625) {
					$fgt = 'BB';
					$mgt = 'AB';
					# informative
					$status = 'Paternal';
				}
				elseif ($fbval >= 0.75 && $mbval <= 0.25) {
					$fgt = 'BB';
					$mgt = 'AA';
					#informative !
					$status = 'Paternal';
				}
				else {
					# non informative				
					continue;
				}
			}
			elseif ($ibval >= 0.75) {
				$igt = 'B';
				if ($fbval <= 0.25 && $mbval >= 0.75) {
					$fgt = 'AA';
					$mgt = 'BB';
					$status = 'Paternal';
				}
				elseif ($fbval >= 0.375 && $fbval <= 0.625 && $mbval <= 0.25) {
					$fgt = 'AB';
					$mgt = 'AA';
					$status = 'Maternal';
				}
				elseif ($fbval <= 0.25 && $mbval >= 0.375 && $mbval <= 0.625) {
					$fgt = 'AA';
					$mgt = 'AB';
					$status = 'Paternal';
				}
				elseif ($fbval >= 0.75 && $mbval <= 0.25) {
					$fgt = 'BB';
					$mgt = 'AA';
					$status = 'Maternal';
				}
				else {
					#non informative
						
					continue;
				}
			}
			else {
				# unclear values
				$status = 'UNCLEAR VALUES';
				$igt = "baf:'$ibval'";
				$fgt = "baf:$fbval";
				$mgt = "baf:$mbval";
				continue;
			}
		}
		if ($status != '1') {
			echo "<tr><td $firstcell onmouseover=\"Tip(ToolTip('$aid','1','i',0,event))\" onmouseout=\"UnTip()\">$pos</td><td>$igt</td><td>$fgt</td><td>$mgt</td><td>$status</td></tr>";
			flush;
		}
	}
	echo "</table>";
	echo" </p>";
}
###############################################
## EXPERIMENTAL : PREDICT WITH SINGLE PARENT ##
###############################################
else {
	# parental samplenames
	if ($fid != 0) {
		$query =  mysql_query("SELECT chip_dnanr FROM sample WHERE id = $fid");
		$row = mysql_fetch_array($query);
		$fname = $row['chip_dnanr'] ;
		$present = 'father';
		$mname = 'Not Present';
	}
	else {
		$query =  mysql_query("SELECT chip_dnanr FROM sample WHERE id = $mid");
		$row = mysql_fetch_array($query);
		$mname = $row['chip_dnanr'] ;
		$present = 'mother';
		$fname = 'Not Present';
	}
	echo "<p><span class=nadruk>Region:</span> chr".$chromhash[$chr] .":".number_format($start,0,'',',') . "-". number_format($stop,0,'',',')."<br/>";
	echo "<span class=nadruk>Child:</span> $sample<br/>";
	echo "<span class=nadruk>Father:</span> $fname<br/>";
	echo "<span class=nadruk>Mother:</span> $mname<br/>";
	echo "</p>";

	if ($present == 'father') {
		#paternal datapoints
		$query = mysql_query("SELECT parent_seen, parent_aid FROM parent_offspring_cnv_relations WHERE aid = $aid AND parent_sid = $fid");
		$row = mysql_fetch_array($query);
		$fseen = $row['parent_seen'];
		$faid = $row['parent_aid'];
		if ($fseen == 1) {
			$table = 'datapoints';
			$pseen = 1;
		}
		else {
			$table = 'parents_datapoints';
			$pseen = 0;
		}
		$query = mysql_query("SELECT content FROM $table WHERE id = $faid");
		$drow = mysql_fetch_array($query);
		$pcontent = $drow['content'];
		unset($drow);
	}
	else {
		#maternal datapoints
		$query = mysql_query("SELECT parent_seen, parent_aid FROM parent_offspring_cnv_relations WHERE aid = $aid AND parent_sid = $mid");
		$row = mysql_fetch_array($query);
		$mseen = $row['parent_seen'];
		$maid = $row['parent_aid'];
		if ($mseen == 1) {
			$table = 'datapoints';
			$pseen = 1;
		}
		else {
			$table = 'parents_datapoints';
			$pseen = 0;
		}
		$query = mysql_query("SELECT content FROM $table WHERE id = $maid");
		$drow = mysql_fetch_array($query);
		$pcontent = $drow['content'];
		unset($drow);
	}
	## PROCESS DATAPOINTS child
	$idata = explode('_',$content);
	$inrelements = count($idata);
	$ibaf = array();
	for ($i = 0;$i < ($inrelements - 1);$i+=3) {
		if ($idata[$i] >= $start && $idata[$i] <= $stop) {
			$ibaf[$idata[$i]] = $idata[$i+2];
		}
	}
	$inrelements = count($ibaf);
	unset($content);
	## PROCESS DATAPOINTS parent
	$pdata = explode('_',$pcontent);
	$nrelements = count($pdata);
	$pbaf = array();
	for ($i = 0;$i < ($nrelements - 1);$i+=3) {
		$pbaf[$pdata[$i]] = $pdata[$i+2];
	}
	unset($pcontent);

	## COMPARE DATAPOINTS
	ksort($ibaf);
	# working with baf values: suppose (including): 
	#  - BAF < 0.25  => AA
	#  - BAF > 0.75  => BB
	#  - 0.375 < BAF <  0.625   => AB
	echo "<p>Below is an overview of the mendelian errors that are considered informative genotypes for Parent of Origin detection (if any). In total $inrelements datapoints were considered.</p><p><span class=bold>Note:</span> The offspring genotypes are arbitrarily set to A or B instead of AA/BB because a deletion event is assumed.</p><p>";
	echo "<span class=nadruk style='color:red'>NOTE:</span> These predictions are made in absence of the second parent. The only prediction that can be done is a <span class=italic>De Novo</span> deletion of the allele from the available parent. This means a position where the allele left in the patient is not inherited from the presented parent (eg : child has A, available parent has BB). </p></p>";
	echo "<table cellspacing=0>";
	echo "<tr><th class=topcell $firstcell>Position</th><th class=topcell>Child</th><th class=topcell>Father</th><th class=topcell>Mother</th><th class=topcell>Status</th></tr>";
	foreach ($ibaf as $pos => $ibval) {
		$status = 1;
		$igt = '';
		$pgt = '';
		$pbval = $pbaf[$pos];
		if ($pbval == '' ) {
			#$status = 'parental data missing';
			$pgt = ' -- ';
			continue;
		}
		# heterozygous is not possible for deletions !
		if ($ibval > 0.25 && $ibval < 0.75) {
			#$igt = 'AB';
			#$pgt = ' / ';
			# unclear values
			continue;
		}
		else {
			if ($ibval <= 0.25 ) {
				$igt = 'A';
				if ($pbval <= 0.25 ) {
					$pgt = 'AA';
					# not informative !
					continue;
				}
				elseif ($pbval >= 0.375 && $pbval <= 0.625 ) {
					$pgt = 'AB';
					#not informative !
					continue;
				}
				elseif ($pbval >= 0.75 ) {
					$pgt = 'BB';
					#informative !
					$status = 'De Novo';
				}
				else {
					# non informative				
					continue;
				}
			}
			elseif ($ibval >= 0.75) {
				$igt = 'B';
				if ($pbval <= 0.25 ) {
					$pgt = 'AA';
					$status = 'De Novo';
				}
				elseif ($pbval >= 0.375 && $pbval <= 0.625 ) {
					$pgt = 'AB';
					# non informative 
					continue;
				}
				elseif ($pbval >= 0.75 ) {
					$pgt = 'BB';
					# non informative
					continue;
				}
				else {
					#non informative
					continue;
				}
			}
			else {
				# unclear values
				$status = 'UNCLEAR VALUES';
				$igt = "baf:'$ibval'";
				$fgt = "baf:$fbval";
				$mgt = "baf:$mbval";
				#continue;
			}
		}
		if ($status != '1') {
			if ($present == 'father') {
				if ($pseen == 1) { 
					$status = 'Paternally inherited';
				}
				else {
					$status = '<span class=italic>De Novo</span> (Paternal)';
				}
				echo "<tr><td $firstcell onmouseover=\"Tip(ToolTip('$aid','1','i',0,event))\" onmouseout=\"UnTip()\">$pos</td><td>$igt</td><td>$pgt</td><td> -- </td><td>$status</td></tr>";
			}
			else {
				if ($pseen == 1) {
					$status = 'Maternally inherited';
				}
				else {
					$status = '<span class=italic>De Novo</span> (Maternal)';
				}
				echo "<tr><td $firstcell onmouseover=\"Tip(ToolTip('$aid','1','i',0,event))\" onmouseout=\"UnTip()\">$pos</td><td>$igt</td><td> -- </td><td>$pgt</td><td>$status</td></tr>";
			}
			flush;
		}
	}
	echo "</table>";
	echo" </p>";

	}
}

?>
<div id="txtHint">something must appear here</div>
<!-- context menu loading -->

