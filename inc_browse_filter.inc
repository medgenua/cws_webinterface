<?
//syntax
?>
<div id="txtHint">something must appear here</div>
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>

<?php
ob_end_flush();
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style='border-left: 1px solid #a1a6a4;'";
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inh = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

#######################
# CONNECT TO DATABASE #
#######################
$db = "CNVanalysis" . $_SESSION['dbname'];
$ucscdb = str_replace('-','',$_SESSION['dbname']);
//////////////////////////////
// CONTENT FOR FILTER MENUs //
//////////////////////////////
$criteria = array(
	'autosome' => 'Chromosome = NOT X',
	'chr23' => 'Chromosome = X',
	'chrIN' => 'Specified Chromosome',
	'cn0' => 'CopyNumber = 0',
	'cn1' => 'CopyNumber = 1',
	'cn2' => 'LOH Region (cn = 2)',
	'cn3' => 'CopyNumber = 3',
	'cn4' => 'CopyNumber = 4',
	'del' => 'CopyNumber < 2',
	'dup' => 'CopyNumber > 2',
	'sizeLess' => 'Maximimal Size',
	'sizeMore' => 'Minimal Size',
	'nrsnpsMore' => 'Minimal Nr.Probes',
	'denovo' => 'Inheritance : De Novo',
	'pat' => 'Inheritance: Paternal',
	'mat' => 'Inheritance : Maternal',
	'class_path' => 'Diagnostic Class: Pathogenic',
	'class_pathrec' => 'Diagnostic Class: Pathogenic Recessive',
	'class_path_all' => 'Diagnostic Class: (Likely) Pathogenic (Recessive)',
	'class_vus' => 'Diagnostic Class: VUS',
	'class_benign' => 'Diagnostic Class: Benign',
	'class_benign_all' => 'Diagnostic Class: (Likely) Benign',
	'class_path_vus' => 'Diagnostic Class: Pathogenic & VUS',
	'class_path_vus_benign' => 'Diagnostic Class: Pathogenic & VUS & Benign',
	'class_fp' => 'False Positives',
	'inherited' => 'Inheritance : Inherited from any',
	'ResultLess' => 'Max ... Resulting CNVs',
	'ResultMore' => 'Min ... Resulting CNVs'
	);
$queryparts = array(
	'autosome' => 'chr < 23',
	'chr23' => 'chr = 23',
	'chrIN' => 'chr IN ('.str_replace(array('X','Y'),array('23','24'),$_POST['chrIN']).')',
	'cn0' => 'cn = 0',
	'cn1' => 'cn = 1',
	'cn2' => 'cn = 2',
	'cn3' => 'cn = 3',
	'cn4' => 'cn = 4',
	'del' => 'cn < 2',
	'dup' => 'cn > 2',
	'sizeLess' => 'size < '.$_POST['sizeLess'],
	'sizeMore' => 'size > '.$_POST['sizeMore'],
	'nrsnpsMore' => 'nrsnps >'.$_POST['nrsnpsMore'],
	'denovo' => 'inheritance = 3',
	'pat' => 'inheritance = 1',
	'mat' => 'inheritance = 2',
	'inherited' => 'inheritance IN (1,2)',
	'class_path' => 'class = 1',
	'class_pathrec' => 'class = 6',
	'class_path_all' => 'class IN (1,2,6)',
	'class_vus' => 'class = 3',
	'class_ben' => 'class = 4',
	'class_ben_all' => 'class IN (4,7)',
	'class_path_vus' => 'class IN (1,2,3,6)',
	'class_path_vus_benign' => 'class IN (1,2,3,4,6,7)',
	'class_fp' => 'class = 5',
	#'ResultLess' => '',
	#'ResultMore' => ''
	);

$toadd = "' AND <br/>' + fieldidx + ' : <select id=\'crit' + fieldidx +'\' name=\"criteria[' + fieldidx + ']\" onchange=\"showspan(' + fieldidx + ')\">";
$toadd .= "<option value=\'\'> </option>";
foreach ($criteria as $key => $descr) {
	$toadd .= "<option value=$key >$descr</option>";
}
$toadd .= "</select>";
$toadd .= "<span style=\'display:none;\' id=extra' + fieldidx + '> </span><span id=items' + (fieldidx + 1) + '>  <i>(<a href=\'javascript:void(0)\' onclick=\'additem('+ ( fieldidx + 1 ) +');\'>add item</a>)</i></span>'";
////////////////////////////////////
// script to add criteria entries //
////////////////////////////////////
echo "<script type='text/javascript'>\n";
echo "var fieldidx = 1;\n";
echo "function additem(fieldidx) {\n";
#echo "\tfieldidx++;\n";
#echo "\nalert(fieldidx);\n";
 echo "\tdocument.getElementById('items'+ fieldidx).innerHTML = $toadd;\n";
echo "}\n";
echo "</script>";	
?>
<script type='text/javascript'>
function showspan(counter) {
	var sel;
	var mylist = document.getElementById('crit' + counter);
	sel = mylist.value;
	if (sel == 'chrIN') {
		document.getElementById('extra' + counter).style.display='';
		document.getElementById('extra' + counter).innerHTML=' Set: <input type=text name=chrIN value=\'Comma Seperated List\' onfocus="if (this.value == \'Comma Seperated List\') {this.value = \'\';};" size=20>';
	}
	else if (sel == 'sizeLess') {
		document.getElementById('extra' + counter).style.display='';
		document.getElementById('extra' + counter).innerHTML=' Set: <input type=text name=sizeLess value=\'In basepairs\' onfocus="if (this.value == \'In basepairs\') {this.value = \'\';};" size=20>';
	}
	else if (sel == 'sizeMore') {
		document.getElementById('extra' + counter).style.display='';
		document.getElementById('extra' + counter).innerHTML=' Set: <input type=text name=sizeMore value=\'In basepairs\' onfocus="if (this.value == \'In basepairs\') {this.value = \'\';};" size=20>';
	}
	else if (sel == 'ResultLess') {
		document.getElementById('extra' + counter).style.display='';
		document.getElementById('extra' + counter).innerHTML=' Set: <input type=text name=ResultLess value=\'Nr.CNVs\' onfocus="if (this.value == \'Nr.CNVs\') {this.value = \'\';};" size=20>';
	}
	else if (sel == 'ResultMore') {
		document.getElementById('extra' + counter).style.display='';
		document.getElementById('extra' + counter).innerHTML=' Set: <input type=text name=ResultMore value=\'Nr.CNVs\' onfocus="if (this.value == \'Nr.CNVs\') {this.value = \'\';};" size=20>';
	}
	else if (sel == 'nrsnpsMore') {
		document.getElementById('extra' + counter).style.display='';
		document.getElementById('extra' + counter).innerHTML=' Set: <input type=text name=nrsnpsMore value=\'Minimal Nr.SNPs\' onfocus="if (this.value == \'Minimal Nr.SNPs\') {this.value = \'\';};" size=20>';

	}
	else {
		document.getElementById('extra' + counter).style.display='none';
	}
}
</script>
<?php
###################
## TOP INFO TEXT ##
###################
echo "<div class=sectie>";
echo "<h3>Filter Results Based on custom Criteria</h3>\n";
echo "<p>Select data from the database by custom filtering based on criteria that are manually set. Shown data when 'table' is selected, are the sample name, sample gender, CNV region, CNV size, CNV copynumber, diagnostic class, inheritance and nr genes. Results are sorted first on sample, then on region.  When 'Graphical Output' is selected, data are plotted along the chromosomes and details data is available by hovering the mouse over the regions.</p>";
echo "<p><span class=nadruk>GET cnvs WHERE : </p>";
echo "<p><form action='index.php?page=results&type=filter' METHOD=POST>";
$sortcount = 0;	
$intro = '';
foreach($_POST['criteria'] as $counter => $value) {
	$sortcount++;
	echo "$intro$sortcount : <select id='crit$counter' name=criteria[$counter] onchange='showspan($sortcount)'>";
	$intro = ' AND <BR/>';
	$selkey = '';
	echo "<option value=''> </option>";
	foreach ($criteria as $key => $descr) {
		if ($key == $value) {
			$sel = 'SELECTED';
			$selkey = $key;
		}
		else {
			$sel = '';
		}
		echo "<option value=$key $sel>$descr</option>";
	}
	echo "</select>";
	if ($selkey == 'sizeLess' || $selkey == 'sizeMore' || $selkey == 'chrIN' || $selkey == 'ResultLess' || $selkey == 'ResultMore' || $selkey == 'nrsnpsMore') {
		$selval = $_POST[$selkey];
		echo "<span id=extra$counter> Set: <input type=text name=$selkey value='$selval'></span>"; 
	}
	else {
		echo "<span style='display:none;' id=extra$counter> </span>";
	}
}
if ($sortcount == 0) {
	$nocriteria = 1;
	$sortcount++;
	echo "$sortcount : <select id='crit$sortcount' name=criteria[$sortcount] onchange='showspan($sortcount)'>";
	echo "<option value=''> </option>";
	foreach ($criteria as $key => $descr) {
		if ($key == $value) {
			$sel = 'SELECTED';
		}
		else {
			$sel = '';
		}
		echo "<option value=$key $sel>$descr</option>";
	}
	echo "</select>";
	echo "<span style='display:none;' id=extra$sortcount> </span>";

}
$sortcount++;
echo "<span id=items$sortcount> <i>(<a href='javascript:void(0)' onclick='additem($sortcount);'>add item</a>)</i></span>";
#echo " <i>(<a href='javascript:void(0)' onclick='additem($sortcount);'>add item</a>)</i> "; 
echo "</p><p>";
if (isset($_POST['controls'])) {
	echo "<input type=checkbox name=controls CHECKED> Include Control Samples</p><p>";
}
else {
	echo "<input type=checkbox name=controls> Include Control Samples</p><p>";
}	
echo "<input type=radio name=output value='table'> Table Output <input type=radio name=output value='plot' checked> Graphical output</p><p>";
echo "<input type=submit class=button value='Get Results'></form></p>"; 
echo "</div>";

#################
## GET RESULTS ##
#################
echo "<div class=sectie>";

$query = "SELECT s.chip_dnanr, s.gender, a.id,a.chr, a.start, a.stop, a.size, a.cn, a.class, a.inheritance, a.nrgenes, p.naam FROM aberration a JOIN sample s JOIN projsamp ps JOIN project p JOIN projectpermission pp ON a.sample = s.id AND a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND pp.projectid = a.idproj WHERE pp.userid = $userid ";
if (!isset($_POST['controls'])) {
	$query = $query . " AND s.intrack = 1 AND s.trackfromproject = a.idproj AND p.collection != 'Controls'";
}	
	
// process filters
$todo = array();
$subquery = '';
$nocriteria = 1;
foreach($_POST['criteria'] as $counter => $value) {
	if ($value == '') {
		continue;
	}
	$nocriteria = 0;
	if ($value == 'ResultLess' || $value == 'ResultMore') {
		$todo[] = $value;
		continue;
	}
	$query .= " AND ". $queryparts[$value];
	$subquery .= " AND ". $queryparts[$value];
	
}
foreach ($todo as $key => $value) {
	if ($value == 'ResultLess') {
		$query .= " AND (select COUNT(ab.id) FROM aberration ab WHERE a.idproj = ab.idproj AND a.sample = ab.sample $subquery) <= ".$_POST[$value];
	}
	elseif ($value == 'ResultMore') {
		$query .= " AND (select COUNT(ab.id) FROM aberration ab WHERE a.idproj = ab.idproj AND a.sample = ab.sample $subquery) >= ".$_POST[$value];
	}
}	
// set order:
if ($_POST['output'] != 'plot' && isset($_POST['output'])) {
	$query = $query . ' ORDER BY s.chip_dnanr ASC, a.chr ASC, a.start ASC, a.stop ASC';
}
else {
	$query = $query . ' ORDER BY a.chr ASC, a.cn ASC, a.start ASC, a.STOP ASC';
}
#echo "<p>QUERY: $query<br/><br/>";
if ($nocriteria == 1) {
	#echo "<div class=sectie>";
	echo "<h3>No criteria specified</h3>";
	echo "<p>Specify at lease one filter item.</p>";
	echo "</div>";
	exit();
}
####################
## OUTPUT RESULTS ##
####################
/////////////////////
// 1: TABLE OUTPUT //
/////////////////////
if ($_POST['output'] == 'table' || $_POST['output'] == 'table') {
	$results = mysql_query("$query");
	$nrrows = mysql_num_rows($results);
	#echo "rows found: $nrrows</p></div>";
	echo "<div class=sectie>";
	echo "<h3>Results</h3>\n";
	if ($nrrows == 0) {
		echo "<p>No Results. Maybe the filter criteria were too stringent. Try setting wider limits.</p></div>";
		exit();
	}
	echo "<p><table cellspacing=0>";
	echo "<tr><th>Sample</th><th>Region</th><th>Size</th><th>CopyNumber</th><th>DC/Inh</th><th>Nr.Genes</th></tr>";
	$currsample = '';
	$currgender = '';
	$currproject = '';
	$output = '';
	$rowcounter = 0;
	while ($row = mysql_fetch_array($results)) {
		$sample = $row['chip_dnanr'];
		if ($sample != $currsample && $currsample != '') {
			// output!
			$fc = "<tr><td $firstcell rowspan=$rowcounter>$currsample<br/>&nbsp; &nbsp; &nbsp;<span class=italic>($currgender)</span></td>";
			$output = $fc . $output;
			echo $output;
			$output = '';
			$rowcounter = 0;
		}
		$rowcounter++;
		$currsample = $sample;
		$currproject = $row['naam'];
		$currgender = $row['gender'];
		$aid = $row['id'];
		$chrtxt = $chromhash[$row['chr']];
		$start = $row['start'];
		$stop = $row['stop'];
		$region = "Chr$chrtxt:".number_format($start,0,'',','). "-".number_format($stop,0,'',',');
		$size = number_format($row['size'],0,'',',');
		$cn = $row['cn'];
		$class= $row['class'];
		if ($class == '') {
			$class = '-';
		}
		$in = $row['inheritance'];
		if ($in == '') {
			$in = '-';
		}
		else {
			$in = $inh[$in];
		}
		$nrgenes = $row['nrgenes'];
		$output .= "<td onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\">$region</td><td>$size</td><td>$cn</td><td>$class / $in</td><td>$nrgenes</td></tr>";
	}
	// final row !
	echo "<tr><td $firstcell rowspan=$rowcounter>$currsample<br/>&nbsp; &nbsp; &nbsp;<span class=italic>($currgender)</span></td>$output";
	echo "</table>";
}
////////////////////
// 2: PLOT OUTPUT //
////////////////////
elseif ($_POST['output'] == 'plot' || $_GET['output'] == 'plot' || !isset($_POST['output'])) {
	#echo "<div class=sectie>";
	echo "<h3>Results</h3>\n";
	$_SESSION['filterquery'] = $query;
	#$filterquery = $query;
	echo "<div id='loadingimage'><div style='text-align:center'><img src='images/content/ajax-loader.gif' width=50px /><br/>Filtering Results...</div></div>";
	echo "<img id='plotview' src='karyofilter.php' usemap='#results' border=0 onload=\"document.getElementById('loadingimage').innerHTML='<img src=\'images/content/ajax-loader.gif\' width=50px /><br/>Loading Image Map...</div>\'\">\n";
	flush();
	$results = mysql_query("$query");
	$nrrows = mysql_num_rows($results);
	if ($nrrows == 0) {
		echo "<p>No Results. Maybe the filter criteria were too stringent. Try setting wider limits.</p></div>";
		exit();
	}
	$map = '';
	$map .= "<map name='results'>";
	$maxsize = 249000000 ;// rounded from chromosome 1 (in build $ucscdb)
	$maxheight = 500; // max height of a chromosome, corresponds to 1
	$scalef = 500/$maxsize;
	$maxoffsets = array();
	$xoff = 0;
	$chromoffsets = array();
	$chromy = array();
	$y = 10;
	$addtox = -5;
	$maxy = array();
	while ($row = mysql_fetch_array($results)) {
		$chr = $row['chr'];
		$chromy[$chr] = $y;
		$start = $row['start'];
		$stop = $row['stop'];
		$cn = $row['cn'];
		if (!defined($maxy[$chr])) {
			// get size of chromosome
			$squery = mysql_query("SELECT stop FROM cytoBand WHERE chr IN ('$chr') ORDER BY stop DESC");
			$srow = mysql_fetch_array($squery);
			$maxy[$chr] = intval(round(($srow['stop'])*$scalef));
		}
		// (new chromosome )
		if ($currchr != $chr && $currchr != '') {
			if ($currcn < 2) {
				#echo "new chrom, currcn < 2";
				$maxoffsets[$currchr] = $addtox;
				$xoff = $xoff + $addtox + 30;
				$chromoffsets[$currchr] = $xoff;
				$offsets = array();
				$addtox = 0; 
				#echo "chr $currchr xoff $xoff, yoff: $chromy[$currchr] and maxoff $maxoffsets[$currchr]<br>";
			}
			else {
				#echo "new chrom, currcn > 2<br>";
				$xoff += 30;
				#$chromoffsets[$chr] = $xoff;
			}
			// if too wide, next line
			if ($xoff > 780) {
				$diff = $xoff - $chromoffsets[$currchr];
				#$y = $y + $maxy + 20;
				// clear size of two last chromosomes chromosome
				$currchrsize = $maxy[$currchr];
				$chrsize = $maxy[$chr];
				unset($maxy[$chr]);
				unset($maxy[$currchr]);
				// set Y-offset
				$y = $y + max($maxy) + 20;
				$maxy = array();
				$maxy[$chr] = $chrsize;
				$maxy[$currchr] = $currchrsize;
				$chromy[$currchr] = $y;
				$chromy[$chr] = $y;
				$chromoffsets[$currchr] = $maxoffsets[$currchr] + 10 ;
				$chromoffsets[$chr] = $chromoffsets[$currchr] + 10 + $diff;
				$xoff = $chromoffsets[$currchr] + $diff;
			}
		
		}
		elseif ($currchr == '' && $cn >= 2) {
			// get size of chromosome
			#$squery = mysql_query("SELECT stop FROM cytoBand WHERE chr IN ('$chr') ORDER BY stop DESC");
			#$srow = mysql_fetch_array($squery);
			#$maxy[$chr] = $srow['stop'];
			$xoff += 10;
			$chromoffsets[$chr] = $xoff;
		}
		// same chromosome, other side of karyo OR new chromosome but only cn > 2  =>both cases: plot karyo of current chrom
		if (($currchr == $chr && $currcn < 2 && $cn >= 2 ) || ($currchr != $chr && $cn >= 2)) {
			// plot the current chromosome
			if ($currchr == $chr) {
				#echo "here with addtox for $currchr = $addtox<br>";
				$maxoffsets[$currchr] = $addtox;
				$chromoffsets[$currchr] = $xoff + $addtox + 30;
			}
			else {
				#echo "here with chromoffset  for $chr = ".($addtox +$xoff + 5)."<br>";
				$chromoffsets[$chr] = $xoff + $addtox + 30;
			}
			$xoff = $xoff + $addtox + 5 ;
			$xoff += 7;
			$offsets = array();
			$addtox = 0;
		}
		// set current cn & chr values
		$currchr = $row['chr'];
		$currcn = $row['cn'];
		// plot the aberration
		$scaledstart = intval(round(($start)*$scalef));
	   	$scaledstop = intval(round(($stop)*$scalef));
		if ($scaledstop - $scaledstart < 2) {
			$scaledstop = $scaledstart + 2;
		}
		if ($scaledstop > $maxy) {
			$maxy = $scaledstop;
		}
		$plotted = 0;
		foreach ($offsets as $offset => $positions) {
			$ok = 1;
			#echo "chr $chr on offset $offset: ". count($offsets[$offset]) . " entries<br/>";
			foreach($offsets[$offset] as $begin => $end) {
				if ($scaledstop >= $begin - 2 && $scaledstop <= $end) {  $ok = 0; break;}
				elseif ($scaledstart <= $end -2 && $scaledstart >= $begin ) {$ok = 0; break;}
				elseif ($scaledstart <= $begin && $scaledstop >= $end) {$ok = 0; break;}
			}
			
			if ($ok == 1) {
				// plot into current offset value
				$xpos = $xoff + $offset;
				#imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);	
				$offsets[$offset][$scaledstart] = $scaledstop;
				$plotted = 1;
				break;
			}
			
		}
		
		if ($plotted == 0) {
			$addtox += 5;
			$xpos = $xoff + $addtox;
			#imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);
			$offsets[$addtox][$scaledstart] = $scaledstop;
			if ($cn < 2) {
				krsort($offsets);
			}
			else {
				ksort($offsets);
			}
			#echo "addtox : $addtox";
	
		}
			
	}
	if ($currcn < 2) {
		// if last cnv was cn < 2, last chromosome has to be placed.
		$maxoffsets[$currchr] = $addtox;
		$chromoffsets[$currchr] = $xoff + $addtox + 30;
	}
	
	// now set areas
	$xoff = 0;  // start here

	// START QUERY:
	$currchr = '';
	$currcn = '';
	$offsets = array();
	$addtox = -5;
	$row = 0;
	if(!mysql_data_seek($results,$row))continue;
	while ($row = mysql_fetch_array($results)) {
		$chr = $row['chr'];
		$start = $row['start'];
		$stop = $row['stop'];
		$cn = $row['cn'];
		$aid = $row['id'];
		$y = $chromy[$chr];
		// (new chromosome )
		if ($currchr != $chr ) {
			// plot the chromosome
			$xoff = $chromoffsets[$chr];
			#$xoff += 12;
			$offsets = array();
			$addtox = -5;
		}
		// same chromosome, other side of karyo OR new chromosome but only cn > 2  =>both cases: reset offsets 
		if ($currchr == $chr && $currcn < 2 && $cn >= 2 ) {
			#$xoff += 7;
			$offsets = array();
			$addtox = -5;
		}
		// set current cn & chr values²
		$currchr = $row['chr'];
		$currcn = $row['cn'];
		// plot the aberration
		$scaledstart = intval(round(($start)*$scalef));
	        $scaledstop = intval(round(($stop)*$scalef));
		if ($scaledstop - $scaledstart < 2) {
			$scaledstop = $scaledstart + 2;
		}
		$plotted = 0;
		foreach ($offsets as $offset => $positions) {
			$ok = 1;
			foreach($offsets[$offset] as $begin => $end) {
				if ($scaledstop >= $begin - 2 && $scaledstop <= $end) { $ok = 0; break;}
				elseif ($scaledstart <= $end -2 && $scaledstart >= $begin ) {$ok = 0; break;}
				elseif ($scaledstart <= $begin && $scaledstop >= $end) {$ok = 0; break;}
			}
			
			if ($ok == 1) {
				// plot into current offset value
				if ($cn < 2) {
					$xpos = $chromoffsets[$chr] - 5 - $offset;
				}
				else {
					$xpos = $chromoffsets[$currchr] + 12 + $offset;
				}
				$xstart = $xpos;
				$xstop = $xpos + 3;
				$ystart = $y+$scaledstart;
				$yend = $y+$scaledstop;
				$map .= "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\">";
				#onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\"
				#echo " plotted = 1 :AID: $aid : chr$chr:$start-$stop<br>";
				#imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);	
				$offsets[$offset][$scaledstart] = $scaledstop;
				$plotted = 1;
				break;
			}
			
		}
	
		if ($plotted == 0) {
			$addtox += 5;
			if ($cn < 2) {
				$xpos = $chromoffsets[$chr] - 5 - $addtox;
			}
			else {
				$xpos = $chromoffsets[$chr] + 12 + $addtox;
			}
	
			#$xpos = $xoff + $addtox;
			$xstart = $xpos;
			$xstop = $xpos + 3;
			$ystart = $y+$scaledstart;
			$yend = $y+$scaledstop;
			$map .= "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ToolTip('$aid','$userid','i',1,event))\" onmouseout=\"UnTip()\">";
			#echo "plotted = 0 (addtox: $addtox):AID: $aid : chr$chr:$start-$stop<br>";
			#imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);
			$offsets[$addtox][$scaledstart] = $scaledstop;
			#if ($cn < 2) {
			ksort($offsets);

		}
		
	}
	echo $map;
	echo "</map></div>";
	echo "<script type=text/javascript>";
	echo "document.getElementById('loadingimage').innerHTML='';";
	echo "</script>";
}	
		
			
