<?php
ob_end_flush();
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style='border-left: 1px solid #a1a6a4;'";
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

#######################
# CONNECT TO DATABASE #
#######################
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
$lddb = "LNDB";
mysql_select_db("$cnvdb");
# load javascript for lddb tree
echo '<script language="javascript" type="text/javascript" src="javascripts/clinicalsearch.js"></script> ';

// process new entries from textfield
if (isset($_POST['addpheno'])) {
	$phenocode = $_POST['featcode'];
	if ($phenocode == '') {
		$pheno = $_POST['traitfield'];
		mysql_select_db($lddb);
		$query = mysql_query("SELECT CODE FROM FEATURE WHERE LABEL LIKE '%$pheno%' LIMIT 2");
		$numrows = mysql_num_rows($query);
		if ($numrows == 1) {
			$row = mysql_fetch_array($query);
			$phenocode = $row['CODE'];
		}
		else {
			echo "<SCRIPT type='text/javascript'>alert('Non-Unique phenotype submitted (Multiple matching phenotypes). Please refine further, or select from list.')</script>";
		}
		mysql_select_db($cnvdb);
	}
	if ($phenocode != '') {
		// add to session variable
		#mysql_query("INSERT INTO sample_phenotypes (sid, lddbcode) VALUES ('$sid', '$phenocode') ON DUPLICATE KEY UPDATE sid = sid");
		$_SESSION['searchpheno'][$phenocode] = 1;
	}
}
// process delete entries
if (isset($_GET['dpheno'])) {
	$code = $_GET['dpheno'];
	// remove from session variable
	unset($_SESSION['searchpheno'][$code]);
	#mysql_query("DELETE FROM sample_phenotypes WHERE sid = '$sid' AND lddbcode = '$code'");
}
if (isset($_POST['clearall'])) {
	unset($_SESSION['searchpheno']);
}

// set criteria 
if (isset($_POST['setAndOr'])) {
	$_SESSION['andor'] = $_POST['andor'];
	$_SESSION['maxclass'] = $_POST['maxclass'];
	$_SESSION['maxres'] = $_POST['maxres'];
}
if (!isset($_SESSION['maxclass'] )) {
	$_SESSION['maxclass'] = 3;
}
if (!isset($_SESSION['maxres'])) {
	$_SESSION['maxres'] = 30;
}
if(!is_numeric($_SESSION['maxres'])) {
	$_SESSION['maxres'] = 30;
}
$maxres = $_SESSION['maxres'];
$classpart = '';
if ($_SESSION['maxclass'] != '') {
	$classpart = "AND class IN (";
	for ($c = 1; $c <= $_SESSION['maxclass'];$c++) {
		$classpart .= "$c,";
	}
	$classpart = substr($classpart,0,-1);
	$classpart .= ")";
}
/////////////////////////////////////////
// print form to add info from textbox //
/////////////////////////////////////////
if ($_GET['ft'] == 1) { // coming from tree
	$style = "style='display:none'";
}
else {
	$style = '';
}
echo "<div class=sectie id=textboxform $style>\n";
echo "<h3>Search Sample based on phenotype</h3>\n";
echo "<h4>Using the London NeuroGenetics Database nomenclature</h4>\n";
echo "<p>Type the clinical discription in the textbox below. While typing, suggestions will appear.  Select the one you need, or keep typing untill only one item remains. Next press enter or click 'Add'.</p>";
echo "<p>";
echo "<form action='index.php?page=results&type=pheno' method=POST>\n";
//echo "<input type=hidden name=sid value='$sid'>\n";
//echo "<input type=hidden name=pid value='$pid'>\n";
echo "<input type=hidden name=type value='HPO'>\n";
echo "<input type=hidden name=featcode id=traitcode value=''>\n";
echo "<p>Add new phenotype: <input type=text id=traitfield name='traitfield' size=40> &nbsp; <input type=submit class=button name='addpheno' value='Add'></p>";
echo "</form>";
	echo "<p><a href='javascript:void()' onclick=\"document.getElementById('treeform').style.display='block';document.getElementById('textboxform').style.display='none';loadChild('tree1','0');\">Alternative: Add phenotypes by browsing the ontology tree</a></p>";
echo "</div>";

////////////////////////////////////
// print form to add info by tree //
////////////////////////////////////
if ($_GET['ft'] == 1) { // coming from tree
	$style = "";
	echo "<script type='text/javascript'>loadChild('tree1','0');</script>";
}
else {
	$style = "style='display:none;'";
}
echo "<div class=sectie $style id='treeform'>\n";
echo "<h3>Search Sample based on phenotype</h3>\n";
echo "<h4>Using the London NeuroGenetics Database nomenclature</h4>\n";
echo "<p>";
// the tree 
echo "<div id='loadingbox'></div> \n";
echo ' <div id="tree1" class="nodecls"></div> ';
echo "</p>";
echo "<p><a href='javascript:void()' onclick=\"document.getElementById('treeform').style.display='none';document.getElementById('textboxform').style.display='block'\">Alternative: Add phenotypes by providing the clinical terms</a></p>";
echo "</div>";

////////////////////////////////////////////////
// print currently associated characteristics //
////////////////////////////////////////////////
echo "<div id='associated'>";  // this div is refilled by ajax when something changes
echo "<div class=sectie>";
echo "<h3>Selected Phenotypes </h3>\n";
if (isset($_SESSION['andor'])) {
	$andor = $_SESSION['andor'];
}
else {
	$andor = 'OR';
}
$parray = array();
$instring = '';
foreach ($_SESSION['searchpheno'] as $code => $set) {
	if ($set != 1) {
		continue;
	}
	$instring .= "'$code',";
	$pieces = explode('.',$code);
	$parray[$pieces[0]][$pieces[1]][] = $pieces[2];
}
mysql_select_db($lddb);
echo "<p><table cellspacing=0>";
echo "<tr>\n";
echo "<th $firstcell class=topcellalt>&nbsp;</th><th class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
echo "</tr>\n";
if ($_GET['ft'] == 1) {
	$fts = '&ft=1';
}
foreach($parray as $first => $sarray) {
	$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
	$row = mysql_fetch_array($query);
	$firstlabel = $row['LABEL'];
	foreach ($sarray as $second => $tarray) {
		if ($second == '00') {
			echo "<tr><td $firstcell><img onclick=\"delphenotype('$first.00.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
		}
		else {
			$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
			$row = mysql_fetch_array($query);
			$secondlabel = $row['LABEL'];
			foreach ($tarray as $third) {
				if ($third == '00') {
					echo "<tr><td $firstcell><img onclick=\"delphenotype('$first.$second.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>\n";	
				}
				else {
					$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
					$row = mysql_fetch_array($query);
					$thirdlabel = $row['LABEL'];
				
					echo "<tr><td $firstcell><img onclick=\"delphenotype('$first.$second.$third')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>\n";
				}	
			}
		}
	}
}
echo" </table>";
echo "</p>";
//////////////////////////////////
// CHANGE CRITERIA COMBINATIONS //
//////////////////////////////////
echo "<p><form action='index.php?page=results&type=pheno' method=POST>";
echo "<table style='width:100%'>";
if ($andor == 'AND') {
	echo "<tr><td class=clear>Show samples matching: <input type=radio name=andor value='AND' checked> All / <input type=radio name=andor value='OR'> Any</td>"; 
}
else {
	echo "<tr><td class=clear>Show samples matching: <input type=radio name=andor value='AND'> All / <input type=radio name=andor value='OR' checked> Any</td>";
}

echo "<td class=clear>Show aberrations from diagnostic class: <select name=maxclass>";
if ($_SESSION['maxclass'] == '') {
	echo "<option value='' selected>All (including non-specified)</option>";
	echo "<option value=1>Only Class 1</option>";
	echo "<option value=2>Class 1-2</option>";
	echo "<option value=3>Class 1-3</option>";
	echo "<option value=4>Class 1-4</option>";
}
else {
	echo "<option value=''>All (including non-specified)</option>";
	if ($_SESSION['maxclass'] == 1) {
		echo "<option value=1 selected>Only Class 1</option>";
	}
	else {
		echo "<option value=1>Only Class 1</option>";
	}
	for ($j = 2;$j<=4;$j++) {
		if ($_SESSION['maxclass'] == $j) {
			echo "<option value=$j selected>Class 1-$j</option>";
		}
		else {
			echo "<option value=$j >Class 1-$j</option>";
		}
	}
}
echo "</select></td></tr>";
echo "<tr><td class=clear>Maximal number of results: <input type=text name=maxres value='$maxres' size=10></td>";
echo "<td class=clear><input type=submit class=button name=setAndOr value='Change'>&nbsp;&nbsp;<input type=submit class=button name='clearall' value='Clear All'></td></tr></table></form></p>";

echo "</div>";
echo "<div class=sectie>";
echo "<h3>Matching Samples</h3>";
if (count($_SESSION['searchpheno']) == 0) {
	echo "<p>At least one phenotypic characteristics has to be selected first.</p>";
}
else {
	$criteria = count($_SESSION['searchpheno']);
	echo "<p>";	
	mysql_select_db($cnvdb);
	$instring = substr($instring,0,-1);
	$query = mysql_query("SELECT s.chip_dnanr, s.gender, COUNT(sp.lddbcode) as matching, s.id AS sid, p.id AS pid, p.naam FROM projsamp ps JOIN sample s JOIN project p JOIN projectpermission pp JOIN sample_phenotypes sp ON ps.idsamp = s.id AND ps.idproj = p.id AND p.id = pp.projectid AND s.id = sp.sid WHERE sp.lddbcode IN ($instring) AND s.intrack = 1  AND pp.userid = '$userid' GROUP BY pid,sid ORDER BY matching DESC, s.chip_dnanr");
	echo "<table cellspacing=0>";
	$nrsamples = mysql_num_rows($query);
	$rowidx = 0;
	$found = 0;
	while (($row = mysql_fetch_array($query)) && $found < $maxres) {
		$rowidx++;
		$sample = $row['chip_dnanr'];
		$gender = $row['gender'];
		$matching = $row['matching'];
		if ($andor == 'AND' && $matching < $criteria) {
			continue;
		}
		$found++;
		
		$sid = $row['sid'];
		$pid = $row['pid'];
		$project = $row['naam'];
		echo "<tr><th $firstcell id='nrsamples$found'>$rowidx/</th><th>$sample</th><th>Gender: $gender</th><th>Project: $project</tr>";
		$subarray = array();
		$subquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
		while ($subrow = mysql_fetch_array($subquery)) {
			$code = $subrow['lddbcode'];
			$pieces = explode('.',$code);
			$subarray[$pieces[0]][$pieces[1]][] = $pieces[2];
		}
		mysql_select_db($lddb);
		echo "<tr>";
		echo "<td colspan=4 class=clear><div style='padding-left:7px;'>"; #global div for phenotypes
		$subtablestring = '';
		$match = 0;
		foreach($subarray as $first => $sarray) {
			$ssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
			$ssubrow = mysql_fetch_array($ssubquery);
			$firstlabel = $ssubrow['LABEL'];
			foreach ($sarray as $second => $tarray) {
				if ($second == '00') {
					if ($_SESSION['searchpheno']["$first.00.00"] == 1) {
						$firstcellclass =  "style='border-left: 1px solid #a1a6a4;font-weight:bold'";
						$match++;
						$class = "style='font-weight:bold'";
					}
					else {
						$class = "";
						$firstcellclass = $firstcell;
					}
					$subtablestring .= "<tr><td $firstcellclass>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
				}
				else {
					$sssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
					$sssubrow = mysql_fetch_array($sssubquery);
					$secondlabel = $sssubrow['LABEL'];
					foreach ($tarray as $third) {
						if ($third == '00') {
							if ($_SESSION['searchpheno']["$first.$second.00"] == 1) {
								$firstcellclass =  "style='border-left: 1px solid #a1a6a4;font-weight:bold'";
								$match++;
								$class = "style='font-weight:bold'";
							}
							else {
								$firstcellclass = $firstcell;
								$class = "";
							}

							$subtablestring .= "<tr><td $firstcellclass>$firstlabel</td><td $class>$secondlabel</td><td>&nbsp;</td></tr>";	
						}
						else {
							$ssssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
							$ssssubrow = mysql_fetch_array($ssssubquery);
							$thirdlabel = $ssssubrow['LABEL'];
							if ($_SESSION['searchpheno']["$first.$second.$third"] == 1) {
								$class = "style='font-weight:bold;'";
								$firstcellclass =  "style='border-left: 1px solid #a1a6a4;font-weight:bold'";
								$match++;
							}
							else {
								$class = "";
								$firstcellclass = $firstcell;

							}
							$subtablestring.= "<tr><td $firstcellclass>$firstlabel</td><td $class>$secondlabel</td><td $class>$thirdlabel</td></tr>";
						}	
					}
				}
			}
		}
		echo "<a href='index.php?page=details&amp;sample=$sid&amp;project=$pid' target='_blank' class=nadruk>Go To Sample Details</a><br/>";
		echo "<span class=nadruk>Associated phenotypes:</span> ($match/$criteria query items found)";
		echo "<table cellspacing=0 style='padding-left:10px'>";
		echo "<tr><th $firstcell class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
		echo "</tr>";
		echo $subtablestring;
		echo "</table></div>"; # global div for phenotypes
		echo "</td>";
		mysql_select_db($cnvdb);
		echo "</tr>";
		echo "<tr>";
		echo "<td colspan=4 class=clear><div style='padding-left:7px;'>"; # global div for results
		$abquery = mysql_query("SELECT chr, start, stop, cn, class, inheritance FROM aberration WHERE sample = '$sid' AND idproj = '$pid' $classpart ORDER BY chr, start");
		$nrabs = mysql_num_rows($abquery);
		if ($nrabs == 0) {
			echo "<span class=nadruk>This sample does not have any aberrations passing classification restrictions.</span>";
			echo "</div></td></tr>";
			continue;
		}
		
		echo "<span class=nadruk>Classified Aberrations:</span>";
		echo "<table cellspacing=0>";
		echo "<tr>";
		echo "<td class=clear><img src='karyo.php?sid=$sid&amp;pid=$pid'></td>";
		echo "<td class=clear><div>"; # nested div for results 
		echo "<table cellspacing=0><tr><th class=topcellalt $firstcell>Region</th><th class=topcellalt>CN</th><th class=topcellalt>DC</th><th class=topcellalt>Inheritance</th></tr>";
				
		while ($abrow = mysql_fetch_array($abquery)) {
			$chr = $chromhash[$abrow['chr']];
			$start = $abrow['start'];
			$stop = $abrow['stop'];
			$region = "Chr$chr:".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
			$cn = $abrow['cn'];
			$class = $abrow['class'];
			$inheritance = $inh[$inh[$abrow['inheritance']]];
			echo "<tr><td $firstcell>$region</td><td>$cn</td><td>$class</td><td>$inheritance</td></tr>";
		}
		echo "</table></div>";  # nested div for results
		echo "</td>";
		echo "</tr>";
		echo "</table></div>"; #global div for results
		echo "</td></tr>";
	}
	echo "</table>";
	if ($found == 0 ) {
		echo "<p> No matching samples found. Make your criteria less stringent and try again.</p>";
	}
	else {
		echo "<script type='text/javascript'>for(i=1;i<=$found;i++){document.getElementById('nrsamples'+i).innerHTML = document.getElementById('nrsamples'+i).innerHTML + '$found';};</script>";
	}	
}
echo "</div>";
mysql_select_db($cnvdb);

?>
<script type='text/javascript'>

var options = {
		script:"lddb_suggestions.php?json=true&limit=10&",
		varname:"input",
		json:true,
		shownoresults:true,
		delay:200,
		cache:false,
		timeout:10000,
		minchars:2,
		callback: function (obj) { document.getElementById('traitfield').value = obj.value;document.getElementById('traitcode').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('traitfield', options);
	

</script>
