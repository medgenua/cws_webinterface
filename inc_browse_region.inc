<?php 
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$db = "CNVanalysis".$_SESSION['dbname'];
mysql_select_db($db);
for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}

$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["x"] = "23";
$chromhash["y"] = "24";

$chromhash["23"] = "X";
$chromhash["24"] = "Y";

if (isset($_POST['region'])) {
	$region = $_POST['region'];
	$fieldvalue = $_POST['region'];
}
elseif (isset($_GET['region'])) {
	$region = $_GET['region'];
	$fieldvalue = $_GET['region'];
}
else{
	$fieldvalue = 'Chromosomal Region';
}

if (isset($_POST['v'])) {
	$v = $_POST['v'];
}
elseif (isset($_GET['v'])) {
	$v = $_GET['v'];
}
else {
	$v = 'i';
}
?>
<div class=sectie>
<h3>Search detected aberrations based on chromosomal region / Gene Symbol</h3>
<p>Enter the chromosomal region in the field below en click 'search'. The region should be specified in UCSC-style, by chromosome band or by RefSeq gene symbol, e.g. chr7:7500000-9200000,  4q21.3, 16p11.2-p12.2 or SOX12.<p>

<p><form action='index.php' method=GET>
<input type=hidden name=page value='results'>
<input type=hidden name=type value='region'>
Search region: &nbsp;
<input type=text name=region width=20px value="<?php echo $fieldvalue ?>" onfocus="if (this.value == 'Chromosomal Region') {this.value = '';}"> &nbsp; &nbsp; 
Visualise by <input type=radio name=v value=i checked> image or <input type=radio name=v value=t> table.
<input type=submit class=button value=search>

</form>
</div>

<?php 

if (isset($_POST['region']) || isset($_GET['region'])) {
	$validfound =0;	
	// UCSC style
	$region = trim($region);
	if (preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $region)) {				
		$pieces = preg_split("/[:\-_+]/",$region);
		$length = count($pieces);
		# start was negative, '-' used as delimiter
		if ($length > 3) {
			$pieces[1] = -1 * abs($pieces[2]);
			$pieces[2] = $pieces[3];
		}
		$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
		if ($chrtxt == '23' || $chrtxt == '24'){
			$chrtxt = $chromhash[$chrtxt];
		}
		$chr = $chromhash[ $chrtxt ] ;
		$start = $pieces[1];
		$start = str_replace(",","",$start);
		if ($start < 0) {
			$start = 1;
		}
		$end = $pieces[2];
		$end = str_replace(",","",$end);
		// check end
		$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
		$nrow = mysql_fetch_array($nq);
		$chrstop = $nrow['stop'];
		if ($end > $chrstop) {
			$end = $chrstop;
		}
		
		$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
		$validfound = 1;
	}
	// CYTOBAND style
	elseif (preg_match("/(\d{1,2}|X|x|Y|y)(p|q)(\d+|ter)/",$region)) {
		$fiveprimevalid = 0;
		$threeprimevalid = 0;
		$invalidprinted = 0;
		$regiontxt = $region;
		$chrtxt = preg_replace("/(\w{1,2})(p|q)(\S+)/","$1",$region);
		$chr = $chromhash[$chrtxt];
		if (preg_match("/\-/",$region)) {  // cytoband range provided
			$startband = preg_replace("/(\w{1,2})(p|q)(\S+)\-(\S+)/","$2$3",$region);
			$stopband = preg_replace("/(\w{0,2})(p|q)(\S+)\-(\S+)/","$4",$region);
			$stopband = preg_replace("/(\w{0,2})(p|q)(\S+)/","$2$3",$stopband);
		}
		else {
			$startband = preg_replace("/(\w{1,2})(p|q)(\S+)/","$2$3",$region);
			$stopband = $startband;
		}
		if ($startband == 'pter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY start ASC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$startband = $nrow['name'];
		}
		if ($stopband == 'pter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY start ASC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$stopband = $nrow['name'];
		}
		if ($startband == 'qter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$startband = $nrow['name'];
		}
		if ($stopband == 'qter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$stopband = $nrow['name'];
		}

		$qstart = mysql_query("SELECT start, stop FROM cytoBand WHERE chr = '$chr' AND name = '$startband'");
		$qstop = mysql_query("SELECT start, stop FROM cytoBand WHERE chr= '$chr' AND name = '$stopband'");
		if (mysql_num_rows($qstart) == 0 ) {
			$invalidprinted = 1;
			echo "<div class=sectie>\n<h3>Invalid Band provided</h3>\n";
			echo "<p><span class=nadruk>5' Band</span></p>\n";
			echo "<p>According to ".$_SESSION['dbstring'].", there is no '$startband'-band for chromosome $chrtxt.\n";
			$qsquery = mysql_query("SELECT start, name FROM cytoBand WHERE chr = '$chr' AND name LIKE '$startband%' ORDER BY start ASC LIMIT 1");
			if (mysql_num_rows($qsquery) != 0) {
				$cstartarray = mysql_fetch_array($qsquery);
				$fivestart = $cstartarray['start'];
				$name1 = $cstartarray['name'];
				//if ($startband == $stopband) {
				$qssquery = mysql_query("SELECT stop, name FROM cytoBand WHERE chr= '$chr' AND name LIKE'$startband%' ORDER BY stop DESC LIMIT 1");	
				$cstartarray = mysql_fetch_array($qssquery);
				$fivestop = $cstartarray['stop'];
				$name2 = $cstartarray['name'];
				if ($name1 == $name2) {
					echo " $chrtxt$startband was converted to $chrtxt$name1.";
				}
				else {
					echo " $chrtxt$startband was converted to $chrtxt$name1-$chrtxt$name2.";
				}
				$fiveprimevalid = 1;
			}
			else {
				echo "Please check your query and try again.\n";	
			}

		}
		else {
			$cstartarray = mysql_fetch_array($qstart);
			$fivestart = $cstartarray['start'];
			$fivestop = $cstartarray['stop'];
			$fiveprimevalid = 1;
		}
		if (mysql_num_rows($qstop) == 0 ) {
			if ($invalidprinted == 0) {
				echo "<div class=sectie>\n<h3>Invalid Band provided</h3>\n";
			}
			echo "<p><span class=nadruk>3' Band</span></p>";
			echo "<p>According to ".$_SESSION['dbstring'].", there is no '$stopband'-band for chromosome $chrtxt. \n";
			$qsquery = mysql_query("SELECT start, name FROM cytoBand WHERE chr = '$chr' AND name LIKE '$stopband%' ORDER BY start ASC LIMIT 1");
			if (mysql_num_rows($qsquery) != 0) {
				$cstoparray = mysql_fetch_array($qsquery);
				$threestart = $cstoparray['start'];
				//echo "<h3>threestart $threestart</h3>";
				$name1 = $cstoparray['name'];
				$qssquery = mysql_query("SELECT stop, name FROM cytoBand WHERE chr= '$chr' AND name LIKE'$stopband%' ORDER BY stop DESC LIMIT 1");	
				$cstoparray = mysql_fetch_array($qssquery);
				$threestop = $cstoparray['stop'];
				$name2 = $cstoparray['name'];
				if ($name1 == $name2) {
					echo " $chrtxt$stopband was converted to $chrtxt$name1.";
				}
				else {
					echo " $chrtxt$stopband was converted to $chrtxt$name1-$chrtxt$name2.";
				}
				$threeprimevalid = 1;
			}
			else {
				echo "Please check your query and try again.\n";	
			}

		}
		else {
			$cstoparray = mysql_fetch_array($qstop);
			$threestop = $cstoparray['stop'];
			$threestart = $cstoparray['start'];
			$threeprimevalid = 1;
		}
		if ($fivestart >= $threestop) {
			$start = $threestart;
			$end = $fivestop;
		}
		else {
			$start = $fivestart;
			$end = $threestop;
		}
		if ($invalidprinted == 1) {
			echo "</div>";
		}
		if ($fiveprimevalid == 1 && $threeprimevalid == 1) {
			$validfound = 1;
		}
		$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";

	}
	// ASSUME THIS WAS A GENE SYMBOL
	else {				
		$symbol = $region;
		$regiontxt = $region;
		if (strpos($symbol,'*')) {
			$querysymbol = str_replace('*','%',$symbol);
			$querystring = "SELECT id, chr, start, end, symbol FROM genes WHERE symbol LIKE '$querysymbol' group by start, end ORDER BY symbol, chr, start, end ";
		}
		else {
			$querystring = "SELECT id, chr, start, end, symbol FROM genes WHERE symbol = '$symbol' group by start, end ";
		}
		$genquery = mysql_query($querystring);
		if (mysql_num_rows($genquery) > 1) {
			echo "<div class=sectie>\n";
			echo "<h3>Multiple hits found</h3>\n";
			echo "<p>Your query returned multiple chromosomal locations, please pick one. </p>\n<ul id=ul-simple>\n";
			while ($row = mysql_fetch_array($genquery)) {
				$chr = $row['chr'];
				$chrtxt = $chromhash[$chr];
				$start = $row['start'];
				$end = $row['end'];
				$gid = $row['id'];
				$symbol = $row['symbol'];
				
				echo "<li> - <span class=bold>$symbol</span> : <a href=\"index.php?page=results&amp;type=region&amp;region=chr$chrtxt:$start-$end&amp;v=$v&amp;gid=$symbol\">Chr$chrtxt:". number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</a></li>\n";
			}
			echo "</ul></p></div>\n";
		}
		elseif (mysql_num_rows($genquery) == 1) {
			$row = mysql_fetch_array($genquery);
			$chr = $row['chr'];
			$chrtxt = $chromhash[ $chr ];
			$start = $row['start'];
			$end = $row['end'];
			$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";
			$validfound = 1;
		}
		else {
			// check synonyms?
			$osym = $symbol;
			$genquery = mysql_query("SELECT id,chr, start, stop, symbol,synonyms FROM genesum WHERE synonyms REGEXP '(^|".'\\\\|)'.$symbol.'(\\\\||$)'."' GROUP BY symbol");
			if (mysql_num_rows($genquery) > 1) {
				echo "<div class=sectie>\n";
				echo "<h3>Multiple hits found</h3>\n";
				echo "<p>Your query ($osym) did not return an exact hit, but is present as a synonym for multiple genes. Please pick one. </p>\n<ul id=ul-simple>\n";
				while ($row = mysql_fetch_array($genquery)) {
					$chr = $row['chr'];
					$chrtxt = $chromhash[$chr];
					$start = $row['start'];
					$end = $row['stop'];
					$gid = $row['id'];
					$symbol = $row['symbol'];
					$synonyms = $row['synonyms'];
					$synonyms = str_replace('|',' ',$synonyms);
					echo "<li> - <span class=bold>$symbol</span> : <a href=\"index.php?page=results&amp;type=region&amp;region=chr$chrtxt:$start-$end&amp;v=$v&amp;gid=$symbol\">Chr$chrtxt:". number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</a> (Synonyms: $synonyms)</li>\n";
				}
				echo "</ul></p></div>\n";
			}
			elseif (mysql_num_rows($genquery)) {
				$row = mysql_fetch_array($genquery);
				$chr = $row['chr'];
				$chrtxt = $chromhash[$chr];
				$start = $row['start'];
				$end = $row['stop'];
				$gid = $row['id'];
				$symbol = $row['symbol'];
				$synonyms = $row['synonyms'];
				$synonyms = str_replace('|',' ',$synonyms);
				echo "<div class=sectie>";
				echo "<h3>Query Found as Synonym</h3>\n";
				echo "<p>Your query ($osym) was found as a synonym for $symbol on chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</p></div>";
				$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";
				$validfound = 1;
			}
			else {
				echo "<div class=sectie>";
				echo "<h3>No Results</h3>";
				echo "<p>Your query was not recognised as a region, and did not match any gene symbol or synonym. Please check your query and try again.</p>";
				echo "</div>";
			}
		}
	}
	if (isset($_GET['gid'])) {
		$regiontxt = $_GET['gid'];
	}

	if ($end < $start) {
		$tmp = $end;
		$end = $start;
		$start = $tmp;
	}

	/////////////////////////
	// LOAD IMAGE OVERVIEW //
	/////////////////////////
	if ($v == 'i' && $validfound == 1) {
		$stop = $end;
		$project = '';
		echo "<div class=sectie>\n";
		echo "<h3>Region overview: $regiontxt</h3>\n";
		echo $isregion;
		echo "<p>\n";
		include('Browser/LoadBrowser.php');
		echo "</p>";
		#echo "</div><div class=sectie>\n<h3>old style</h3><p>";
		#include('inc_create_image.inc');
		echo "</div>\n";
	}
	/////////////////////////
	// LOAD TABLE OVERVIEW //
	/////////////////////////
	elseif ($validfound == 1) {
		$querystring = "SELECT s.id, s.chip_dnanr, a.start, a.stop, a.cn, p.naam, p.chiptype, a.idproj  FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND p.id = a.idproj WHERE pp.userid = '$userid' AND (NOT (a.cn = 2 AND s.gender = 'Male' AND a.chr = 23 AND (((start BETWEEN 10001 AND 2781479) AND (stop BETWEEN 10001 AND 2781479)) OR ((start BETWEEN 89283596 AND 90982633) AND (stop BETWEEN 89283596 AND 90982633)) OR ((start BETWEEN 155701383 AND 156030895) AND (stop BETWEEN 155701383 AND 156030895))))) AND a.chr = '$chr' AND ((a.start <= '$start' AND a.stop >= '$start' ) OR (a.start BETWEEN '$start' AND '$end' ) OR (a.stop BETWEEN '$start' AND '$end')) AND p.collection <> 'Controls' ORDER BY a.start";
		error_log($querystring);
		$result = mysql_query($querystring);
		if (mysql_num_rows($result) == 0) {
			echo "<div class=sectie>\n";
			echo "<h3>Results for query \"$regiontxt\"</h3>\n";
			echo $isregion;
			echo "<p>No results found</p>\n";
			echo "</div>\n";
		}
		else {
			$numres = mysql_num_rows($result);
			echo "<div class=sectie>\n";
			echo "<h3>Samples containing CNV's in $regiontxt</h3>\n";
			echo "<h4>$numres CNV's were found in total</h4>\n";
			echo "<p>\n";
			echo "<table cellspacing=0>\n";
			$switch = 0;
			echo " <tr>\n";
			echo "  <th scope=col class=topcellalt $firstcell>Sample</td>\n";
			echo "  <th scope=col class=topcellalt>Region</td>\n";
			echo "  <th scope=col class=topcellalt>Copy Number</td>\n";
			echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
			echo "  <th scope=col class=topcellalt>Project</td>\n";

			echo "  <th scope=col class=topcellalt>Show</td>\n";
			echo " </tr>\n"; 

			$current = 1;
			while ($row = mysql_fetch_array($result)) {
				$sid = $row['id'];
				$pid = $row['idproj'];
				$sample = $row['chip_dnanr'];
				$start = $row['start'];
				$stop = $row['stop'];
				$cn = $row['cn'];
				$chiptype = $row['chiptype'];
				$pname = $row['naam'];
				$region = "chr" . $chromhash[ $chr ] . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
				$link = "index.php?page=details&amp;sample=$sid&amp;project=$pid";
				echo " <tr>\n";
				echo "  <td $tdtype[$switch] $firstcell>$sample</td>\n";
				echo "  <td $tdtype[$switch] >$region</td>\n";
				echo "  <td $tdtype[$switch] >$cn</td>\n";
				echo "  <td $tdtype[$switch] >$chiptype</td>\n";
				echo "  <td $tdtype[$switch] >$pname</td>\n";
				echo "  <td $tdtype[$switch] ><a href=$link>Details</a></td>\n";
				echo " </tr>\n";	
			}
			echo "</table>\n";

			echo "</div>\n";
		}
	}
}



?>		


