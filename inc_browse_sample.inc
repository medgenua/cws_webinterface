<?php 
####################
# LAYOUT VARIABLES #
####################
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
####################
# PRINT SEARCH BOX #
####################
?>
<div class=sectie>
<h3>Search Results By Sample ID</h3>
<p>Enter the sample identifier in the field below en click 'search'. All projects where this sample is included will be shown, and you can navigate to the results by clicking on the row of the project you need. <p>

<p><form action=index.php method=GET>
<input type=hidden name=page value=results>
<input type=hidden name=type value=sample>
<table>
<tr>
<td class=clear>Enter the sample DNA identifier: </td>
<td class=clear><input type=text name=id width=20px value=dnaID onfocus="if (this.value == 'dnaID') {this.value = '';}"></td>
</tr>
<tr>
<td class=clear>Include custom datasets: </td>
<?php
if ($webonly == 1) {
	echo "<td class=clear><input type=checkbox name=custom CHECKED></td>";
}
else {
	echo "<td class=clear><input type=checkbox name=custom></td>";
}
?>
</tr>
</table></p>
<p><input type=submit class=button value=search></p>
</form>
</div>

<?php 
$id = $_GET['id'];

if ($id != '') {
	if ($level < 3) {
		$result = mysql_query("SELECT DISTINCT(s.id), s.chip_dnanr FROM sample s JOIN projsamp ps JOIN projectpermission pp ON s.id = ps.idsamp AND pp.projectid = ps.idproj WHERE s.chip_dnanr LIKE \"%$id%\" AND pp.userid = '$userid' ORDER BY s.chip_dnanr");
	}
	else {
		$result = mysql_query("SELECT DISTINCT(s.id), s.chip_dnanr FROM sample s JOIN projsamp ps JOIN projectpermission pp ON s.id = ps.idsamp AND pp.projectid = ps.idproj WHERE chip_dnanr LIKE \"%$id%\" ORDER BY chip_dnanr");
	}	
	$current = 0;
	if (mysql_num_rows($result) == 0) {
		echo "<div class=sectie>\n";
		echo "<h3>Results for query \"$id\"</h3>\n";
		$query = mysql_query("SELECT COUNT(id) AS hits FROM sample WHERE chip_dnanr LIKE \"%$id%\"");
		$row = mysql_fetch_array($query);
		$nrhits = $row['hits'];
		if ($nrhits > 0) {
			echo "<p>$nrhits samples were found matching your query, but you don't have access to them.</p>\n";
		}
		else {
			echo "<p>No samples were found matching your query.</p>";
		}
		echo "</div>\n";
	}
	else {
		$numres = mysql_num_rows($result);
		echo "<div class=sectie>\n";
		while ($row = mysql_fetch_array($result)) {
			$current++;
			$sid = $row['id'];
			$name = $row['chip_dnanr'];
			echo "<h3>Result $current/$numres for \"$id\" : $name</h3>\n";
			echo "<p>\n";
			$pquery = "SELECT p.id, p.naam, p.chiptype, p.created, p.collection, u.LastName, u.FirstName ";
			$pquery = $pquery . "FROM project p JOIN users u JOIN projsamp ps JOIN sample s ";
			$pquery = $pquery . "ON p.userID = u.id AND p.id = ps.idproj AND s.id = ps.idsamp ";
			$pquery = $pquery . "WHERE s.id = '$sid'";
			$pres = mysql_query("$pquery");
			$numres = mysql_num_rows($pres);
			if ($numres == 0) {
				echo "No Projects available containing this sample.\n";
			}
			else {
				echo "<p><table cellspacing=0>\n";
				$switch = 0;
				echo " <tr>\n";
				echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
				echo "  <th scope=col class=topcellalt>Created</td>\n";
				echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
				echo "  <th scope=col class=topcellalt>Collection</td>\n";
				echo "  <th scope=col class=topcellalt>Owner</td>\n";	
				echo "  <th scope=col class=topcellalt>Show</td>\n";
				echo " </tr>\n"; 
				$notallow = '';
				$pidstring = '';
				while ($prow = mysql_fetch_array($pres)) {
					$pid = $prow['id'];
					$pidstring .= "$pid|";
					$pname = $prow['naam'];
					$pchiptype = $prow['chiptype'];
					$pcreated = $prow['created'];
					$pcreated = substr($pcreated, 0, strpos($pcreated, "-"));
					$pcollection = $prow['collection'];
					$uFirst = $prow['FirstName'];
					$uLast = $prow['LastName'];
					$line = '';
					$line .= " <tr>\n";
					$line .= "  <td  $firstcell>$pname</td>\n";
					$line .= "  <td  >$pcreated</td>\n";
					$line .= "  <td  >$pchiptype</td>\n";
					$line .= "  <td  >$pcollection</td>\n";
					$line .= "  <td  >$uFirst $uLast</td>\n";
					$permq = mysql_query("SELECT projectid FROM projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
					if (mysql_num_rows($permq) > 0) {
						$line .= "  <td ><a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\">Details</a></td>\n";
						$line .= "</tr>\n";
						echo $line;
					}
					else {
						if ($level < 3) {	
							$line .= " <td >Not Allowed</td>\n";
							$line .= "</tr>\n";
							$notallow .= $line;
						}
						else {
							$line .= " <td ><a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\" style='color:red'>Access</a></td>\n";
							$line .= "</tr>\n";
							$notallow .= $line;
						}
					}
				}
				echo $notallow;			
				echo "</table></p>\n";
				if ($numres > 1) {
					$pidstring = substr($pidstring,0,-1);
					echo "<p><a href=\"index.php?page=project_differences&sid=$sid&pids=$pidstring\">Click to cross-compare the results from the different projects</a>.</p>";
				}
			}
		}
		echo "</div>\n";
	}
	if (isset($_POST['custom']) || isset($_GET['custom'])) {
 	  $result = mysql_query("SELECT DISTINCT(sample),idproj FROM cus_aberration WHERE sample LIKE '%$id%' GROUP BY idproj ORDER BY sample");
	  if (mysql_num_rows($result) == 0) {
		echo "<div class=sectie>\n";
		echo "<h3>Custom Dataset Results for query \"$id\"</h3>\n";
		echo "<p>No results found</p>\n";
		echo "</div>\n";
	  }
	  else {
		$current = 0;
		$numres = mysql_num_rows($result);
		echo "<div class=sectie>\n";
		while ($row = mysql_fetch_array($result)) {
			$current++;
			$sample = $row['sample'];
			$pid = $row['idproj'];
			echo "<h3>Custom Dataset Result $current/$numres for \"$id\" : $sample</h3>\n";
			echo "<p>\n";
			$pquery = "SELECT p.id, p.naam, p.created, p.collection, u.LastName, u.FirstName FROM cus_project p JOIN cus_projectpermission pp JOIN users u ON p.id = pp.projectid AND u.id = p.userID WHERE pp.userid = '$userid' AND p.id = '$pid'";
			$pres = mysql_query("$pquery");
			if (mysql_num_rows($pres) == 0) {
				echo "No access to this sample.\n";
			}
			else {
				echo "<table cellspacing=0>\n";
				$switch = 0;
				echo " <tr>\n";
				echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
				echo "  <th scope=col class=topcellalt>Created</td>\n";
				echo "  <th scope=col class=topcellalt>Platform/ArrayType</td>\n";
				echo "  <th scope=col class=topcellalt>Collection</td>\n";
				echo "  <th scope=col class=topcellalt>Owner</td>\n";	
				echo "  <th scope=col class=topcellalt>Show</td>\n";
				echo " </tr>\n";
				$notallow = ''; 
				while ($prow = mysql_fetch_array($pres)) {
					$pid = $prow['id'];
					$pname = $prow['naam'];
					//$pchiptype = $prow['chiptype'];
					$pcreated = $prow['created'];
					$pcreated = substr($pcreated, 0, strpos($pcreated, " - "));
					$pcollection = $prow['collection'];
					$uFirst = $prow['FirstName'];
					$uLast = $prow['LastName'];
					$ctq = mysql_query("SELECT chiptype, platform FROM cus_sample WHERE idsamp = '$sample' AND idproj = '$pid'");
					$ctr = mysql_fetch_array($ctq);
					$pchiptype = $ctr['chiptype'];
					$pplatform = $ctr['platform'];
					if ($pchiptype == '') {
						$pchiptype = 'NA';
					}
					if ($pplatform == '') {
						$pplatform = 'NA';
					}
					$line = '';
					$line .= " <tr>\n";
					$line .= "  <td  $firstcell>$pname</td>\n";
					$line .= "  <td  >$pcreated</td>\n";
					$line .= "  <td  >$pplatform / $pchiptype</td>\n";
					$line .= "  <td  >$pcollection</td>\n";
					$line .= "  <td  >$uFirst $uLast</td>\n";
					$permq = mysql_query("SELECT projectid FROM cus_projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
					if (mysql_num_rows($permq) > 0) {
						$line .= "  <td><a href=\"index.php?page=cusdetails&amp;project=$pid&amp;sample=$sample\">Details</a></td>\n";
						$line .= "</tr>\n";
						echo $line;
						
					}
					else {
						$line .= " <td >Not Allowed</td>\n";
						$line .= "</tr>\n";
						$notallow .= $line;
					}
				}
				echo $notallow;		
				echo "</table>\n";
			}
		}
		echo "</div>\n";
	  }
	}
}



?>		


