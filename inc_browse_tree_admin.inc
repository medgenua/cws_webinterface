<?php 
$collection=$_GET['collection']; //This line is added to take care if your global variable is off
$projectid = $_GET['project'];
$sampleid = $_GET['sample'];

#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


##############################
# GET the COLLECTION options #
##############################
$collectionquery = mysql_query("SELECT DISTINCT collection FROM project order by collection");

###########################
# GET the PROJECT options #
###########################
if(isset($collection) and strlen($collection) > 0){
	$projectquery=mysql_query("SELECT naam, id FROM project where collection='$collection' order by naam");
}
else{
	$projectquery=mysql_query("SELECT naam, id FROM project order by naam"); 
}

##########################
# GET the SAMPLE options #
##########################
if (isset($collection) and strlen($collection) > 0){
	if (isset($projectid) and is_numeric($projectid)) {
		$samplequery = mysql_query("SELECT sa.chip_dnanr,sa.id, sa.gender, ps.callrate, pr.collection FROM sample sa JOIN projsamp ps JOIN project pr ON sa.id = ps.idsamp AND pr.id = ps.idproj where pr.collection = '$collection' AND pr.id = '$projectid' ORDER BY sa.chip_dnanr");  
	}
	else {
		$samplequery = mysql_query("SELECT sa.chip_dnanr,sa.id, sa.gender, ps.callrate, pr.collection FROM sample sa JOIN projsamp ps JOIN project pr ON sa.id = ps.idsamp AND pr.id = ps.idproj where pr.collection = '$collection' ORDER BY sa.chip_dnanr");
	} 
}
else {
	if (isset($projectid) and is_numeric($projectid)) {
		$samplequery = mysql_query("SELECT sa.chip_dnanr,sa.id, sa.gender, ps.callrate, pr.collection FROM sample sa JOIN projsamp ps JOIN project pr ON sa.id = ps.idsamp AND pr.id = ps.idproj where pr.id = '$projectid' ORDER BY sa.chip_dnanr");  
	}
	else {
		$samplequery = mysql_query("SELECT sa.chip_dnanr,sa.id, sa.gender, ps.callrate, pr.collection FROM sample sa JOIN projsamp ps JOIN project pr ON sa.id = ps.idsamp AND pr.id = ps.idproj ORDER BY sa.chip_dnanr");

	} 

}

# PRINT SELECTION TABLE #
echo "<div class=sectie>\n";
echo "<h3>Find your sample</h3>\n";
echo "<h4>... by narrowing down the criteria</h4>";
echo "<p>Pick your collection, project and sample below. After setting anything, the subchoices will be narrowed down.  When you have chosen your sample, click submit to go tho the details page. </p>\n";
echo "<form action=index.php?page=details method=POST>\n";
echo "<table  class=clear>\n";
echo " <tr>\n";
echo "  <th class=clear>Select a Collection:</td>\n";
echo "  <td class=clear><select name='collection' onchange=\"reload(this.form)\"><option value=''>Select one</option>";

while($collectionrow = mysql_fetch_array($collectionquery)) {
	if($collectionrow['collection']==@$collection){
		echo "<option selected value='".$collectionrow['collection']."'>".$collectionrow['collection']."</option>";
	}
	else{
		echo "<option value='".$collectionrow['collection']."'>".$collectionrow['collection']."</option>";
	}
}
echo "</select></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=clear> &amp;nbsp &amp;nbsp => Select a project:</td>\n";
echo "  <td class=clear><select name='project' onchange=\"reload(this.form)\"><option value=''>Select one</option>";

while($projectrow = mysql_fetch_array($projectquery)) {
	if($projectrow['id']==@$projectid){
		echo "<option selected value='".$projectrow['id']."'>".$projectrow['naam']."</option>";
	}
	else{
		echo "<option value='".$projectrow['id']."'>".$projectrow['naam']."</option>";
	}
}
echo "</select></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=clear> &amp;nbsp &amp;nbsp &amp;nbsp &amp;nbsp => Select a sample:</td>\n";
echo "  <td class=clear><select name='sample' onchange=\"reload(this.form)\"><option value=''>Select one</option>";

while($samplerow = mysql_fetch_array($samplequery)) {
	if($samplerow['id']==@$sampleid){
		echo "<option selected value='".$samplerow['id']."'>".$samplerow['chip_dnanr']."</option>";
	}
	else{
		echo "<option value='".$samplerow['id']."'>".$samplerow['chip_dnanr']."</option>";
	}
}
echo "</select></td>\n";
echo " </tr>\n";
echo " <tr><td class=clear><input type=submit class=button name=submit value='Show Details'></td></tr>";
echo "</table>\n";

echo "</form>";
echo "</div>\n";

##########################
# SHOW overview by table #
##########################
if (isset($sampleid) and is_numeric($sampleid)) {
	echo "<div class=sectie>\n";
	# Print information table on the selected sample
	$details = mysql_query("SELECT chip_dnanr, gender FROM sample WHERE id = $sampleid");
	$detrow = mysql_fetch_array($details);
	$samplename = $detrow['chip_dnanr'];
	echo "<h3>$samplename is available in the following projects</h3>\n";
	echo "<h4>... you have access to </h4>\n";
	echo "<p>You can view details of the projects listed below by clicking on the row. For details on the sample, use the submit button above.</p>\n";
	$samplegender = $detrow['gender'];
	$projidquery = mysql_query("SELECT pr.created, pr.naam, ps.idproj, ps.callrate, pr.chiptype, pr.collection FROM projsamp ps JOIN project pr ON pr.id = ps.idproj WHERE idsamp = $sampleid");
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th> project</td><th>Created</td><th>Collection</td><th>Chiptype</td><th>Callrate</td><th># Aberrations</td></tr>\n";	
	while($result = mysql_fetch_array($projidquery)) {
		
		$projid = $result['idproj'];
		$coll = $result['collection'];
		$projname = $result['naam'];
		$created = $result['created'];
		$callrate = $result['callrate'];
		$chiptype = $result['chiptype'];
		$countquery = mysql_query("SELECT COUNT(id) AS number FROM aberration WHERE sample = $sampleid AND idproj = $projid");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
		$url = "index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$projid&amp;sample="; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";	
		echo " <tr $trstyle>\n";
		echo "  <td>$projname</td>\n";
		echo "  <td>$created</td>\n";
		echo "  <td>$coll</td>\n";
		echo "  <td>$chiptype</td>\n";
		echo "  <td>$callrate</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	
	}
	echo "</table>\n";
	echo "</div>\n";	
}		
elseif (isset($projectid) and is_numeric($projectid)){
	echo "<div class=sectie>\n";
	# Print information table on the selected sample
	$details = mysql_query("SELECT naam, created, chiptype, collection FROM project WHERE id = $projectid");
	$detrow = mysql_fetch_array($details);
	$projname = $detrow['naam'];
	echo "<h3>Samples available in project '$projname'</h3>\n";
	echo "<h4>... and some project details</h4>\n";
	echo "<p>Collection: ". $detrow['collection']. "<br>Created: ".$detrow['created']. "<br>Chiptype: ". $detrow['chiptype']."</p>\n";
	echo "<p>\n";
	$samplegender = $detrow['gender'];
	$samplequery = mysql_query("SELECT sa.id, sa.chip_dnanr, sa.gender, ps.callrate FROM sample sa JOIN projsamp ps ON ps.idsamp = sa.id WHERE ps.idproj = $projectid");
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th>Sample</td><th>Gender</td><th>Callrate</td><th># Aberrations</td></tr>\n";	
	while($result = mysql_fetch_array($samplequery)) {
		$sid = $result['id'];
	 	$chipdnanr = $result['chip_dnanr'];	
		$callrate = $result['callrate'];
		$gender = $result['gender'];
		$countquery = mysql_query("SELECT COUNT(id) AS number FROM aberration WHERE sample = $sid AND idproj = $projectid");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
		$url = "index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$projectid&amp;sample=$sid"; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr $trstyle>\n";
		echo "  <td>$chipdnanr</td>\n";
		echo "  <td>$gender</td>\n";
		echo "  <td>$callrate</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	
	}
	echo "</table>\n";
	echo "</div>\n";	
}
elseif (isset($collection) and strlen($collection)>0){
	echo "<div class=sectie>\n";
	# Print information table on the selected sample
	$details = mysql_query("SELECT id, naam, created, chiptype FROM project WHERE collection='$collection'");
	echo "<h3>Projects available in collection '$collection'</h3>\n";
	echo "<h4>... and some project details</h4>\n";
	echo "<p>\n";
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th>Project</td><th>Created</td><th>Chiptype</td><th># Samples</td></tr>\n";	
	while($result = mysql_fetch_array($details)) {
		$projname = $result['naam'];
		$pid = $result['id'];
		$created = $result['created'];
		$chiptype = $result['chiptype'];
		$countquery = mysql_query("SELECT COUNT(idsamp) AS number FROM projsamp WHERE idproj = $pid");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
		$url = "index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$pid&amp;sample="; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr $trstyle>\n";
		echo "  <td>$projname</td>\n";
		echo "  <td>$created</td>\n";
		echo "  <td>$chiptype</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	
	}
	echo "</table>\n";
	echo "</div>\n";	
}

else {
	echo "<div class=sectie>\n";
	# Print information table on the selected sample
	$details = mysql_query("SELECT id, naam, created, chiptype, collection FROM project ORDER BY created desc LIMIT 10");
	echo "<h3>Ten Most Recent Projects</h3>\n";
	echo "<h4>... you have access to</h4>\n";
	echo "<p>\n";
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th>Project</td><th>Created</td><th>Chiptype</td><th>Collection></td><th># Samples</td></tr>\n";	

	while($result = mysql_fetch_array($details)) {
		$projname = $result['naam'];
		$pid = $result['id'];
		$created = $result['created'];
		$chiptype = $result['chiptype'];
		$coll = $result['collection'];
		$countquery = mysql_query("SELECT COUNT(idsamp) AS number FROM projsamp WHERE idproj = $pid");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
		$url = "index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$pid&amp;sample="; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr $trstyle>\n";
		echo "  <td>$projname</td>\n";
		echo "  <td>$created</td>\n";
		echo "  <td>$chiptype</td>\n";
		echo "  <td>$coll</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	}
	echo "</table>\n";
	echo "</div>\n"	;	


}

?>
