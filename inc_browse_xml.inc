<?php
 // FUNCTIONS 
// LOADED FROM inc_recent_inc !! needs to be loaded for this to work (inside menu part)	
if ($loggedin == 1) {
		// connect to db
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");
	// Load Bookmark Files
	$dir = 'bookmarks/';	
	$Files = LoadFiles($dir);
	SortByDate($Files);
	// Layout VARS
	$tdtype= array("","class=alt");
	$thtype= array("class=spec","class=specalt");
	$topstyle = array("class=topcell","class=topcellalt");
	$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
	$janee = array("nee","ja");
	$nrfiles = count($Files) - 1;
	// prepare page
	echo "<div class=sectie>\n";
	echo "<h3>Download bookmark XML files</h3>\n";
	echo "<h4>right-click, save as or click details for table view</h4>\n";
	echo "<p>Below is a list of all XML files, created from your Illumina Data projects. These files can be downloaded by right-clicking and selecting 'save as', and imported into The Illumina Bookmark Manager (from Genome Viewer), using the import feature. This allows visualising results in GenomeStudio. Alternatively, the bookmark can be parsed on-site for browsing for example the results of the seperate methods. To do so click 'Details'.</p>";
	echo "<p>To remove files you don't need anymore, select the checkboxes and click the 'Remove' button at the bottom of the page.</p>";
	echo "<p><form action=\"remove_bookmarks.php\" method=\"post\">\n";
	echo "<table id=mytable cellspacing=0>\n";
	echo "<tr>";
	echo " <th $firstcell>Remove</th>\n";
	echo " <th>Details</th>\n";
	echo " <th>Links to bookmark files</th>\n";
	echo "</tr>";
	 
	For($j=0;$j<=$nrfiles;$j += 1) {
		$filename = $Files[$j][0];
  		$pname = preg_replace('/(.+)_(multi|PennCNV|QuantiSNP|VanillaICE).xml/',"$1",$filename);
		$query = mysql_query("SELECT p.id FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE p.naam = '$pname' AND pp.userid = $userid");
		if (mysql_num_rows($query) > 0) {
			#$firstpart = substr($filename,0,14) . "...";
  			#echo "<li><a href=\"bookmarks/" ;
  			#echo $Files[$j][0];
			#$i = $i+1;
			echo "<tr><td align=\"center\" $firstcell><input type=\"checkbox\" name=\"";
			echo $filename;
  			echo "\"></td>\n";
  			echo "<td><a href=\"index.php?page=detailsxml&amp;project=";
  			echo $filename;
  			echo "\">Details</a></td>\n";
  			echo "<td><a href=\"bookmarks/";
  			echo $filename;
  			echo "\">";
  			echo $filename;
			echo "</a></td>\n";
  			echo "</tr>\n";
		}
		else {
			// check nonmulti database
			$subquery = mysql_query("SELECT ID FROM nonmulti WHERE project = '$pname' AND userID = '$userid'");
			if (mysql_num_rows($subquery) > 0) {
				#$firstpart = substr($Files[$j][0],0,14) . "...";
  				//echo the link html
  				#echo "<li><a href=\"bookmarks/" ;
  				#echo $Files[$j][0];
  				#echo "\" title=\"";
  				#echo $Files[$j][0];
  				#echo "\">$firstpart</a></li>\n";
				#$i = $i+1;
				echo "<tr><td align=\"center\" $firstcell><input type=\"checkbox\" name=\"";
				echo $filename;
  				echo "\"></td>\n";
  				echo "<td><a href=\"index.php?page=detailsxml&amp;project=";
  				echo $filename;
  				echo "\">Details</a></td>\n";
  				echo "<td><a href=\"bookmarks/";
  				echo $filename;
  				echo "\">";
  				echo $filename ;
				echo "</a></td>\n";
  				echo "</tr>\n";

			}
		}

	}
	
	echo "<td class=clear><input type=\"submit\" value=\"Remove\" class=button></td>\n";
	echo "</table>\n";
	echo "</form>\n";

	//echo "</ul>";
	echo "</div>";
}
?>
