<?php 
#####################
# GENERAL VARIABLES #
#####################
ob_start();
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
#######################
# CONNECT TO DATABASE #
#######################
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
$lddb = "LNDB";
mysql_select_db("$cnvdb");

# load javascript for lddb tree
echo '<script language="javascript" type="text/javascript" src="javascripts/clinicaltree.js"></script> ';


#################################
# GET Sample/project properties #
#################################

//$samplequery = mysql_query("SELECT sa.chip_dnanr, DECODE('clinical','tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF') AS data FROM sample sa WHERE sa.id='$sid'");
$samplequery = mysql_query("SELECT sa.chip_dnanr  FROM sample sa WHERE sa.id='$sid'");
$samplerow = mysql_fetch_array($samplequery);
$samplename = $samplerow['chip_dnanr'];

// process new entries from textfield
if (isset($_POST['addpheno'])) {
	$phenocode = $_POST['featcode'];
	if ($phenocode == '') {
		$pheno = $_POST['traitfield'];
		mysql_select_db($lddb);
		$query = mysql_query("SELECT CODE FROM FEATURE WHERE LABEL LIKE '%$pheno%' LIMIT 2");
		$numrows = mysql_num_rows($query);
		if ($numrows == 1) {
			$row = mysql_fetch_array($query);
			$phenocode = $row['CODE'];
		}
		else {
			echo "<SCRIPT type='text/javascript'>alert('Non-Unique phenotype submitted (Multiple matching phenotypes). Please refine further, or select from list.')</script>";
		}
		mysql_select_db($cnvdb);
	}
	if ($phenocode != '') {
		mysql_query("INSERT INTO sample_phenotypes (sid, lddbcode) VALUES ('$sid', '$phenocode') ON DUPLICATE KEY UPDATE sid = sid");
	}
}

// process delete entries
if (isset($_GET['dpheno'])) {
	$code = $_GET['dpheno'];
	mysql_query("DELETE FROM sample_phenotypes WHERE sid = '$sid' AND lddbcode = '$code'");
}

// print form to add info from textbox
if ($_GET['ft'] == 1) { // coming from tree
	$style = "style='display:none'";
}
else {
	$style = '';
}
echo "<div class=sectie id=textboxform $style>\n";
echo "<h3>Add Clinical Data for Sample $samplename</h3>\n";
echo "<h4>Using the London Dysmorphology Database nomenclature</h4>\n";
echo "<p>Type the clinical discription in the textbox below. While typing, suggestions will appear.  Select the one you need, or keep typing untill only one item remains. Next press enter or click 'Add'.</p>";
echo "<p>";
echo "<form action='index.php?page=clinical' method=POST>\n";
echo "<input type=hidden name=sid value='$sid'>\n";
echo "<input type=hidden name=pid value='$pid'>\n";
echo "<input type=hidden name=type value='HPO'>\n";
echo "<input type=hidden name=featcode id=traitcode value=''>\n";
echo "<p>Add new phenotype: <input type=text id=traitfield name='traitfield' size=40> &nbsp; <input type=submit class=button name='addpheno' value='Add'></p>";
echo "</form>";
	echo "<p><a href='javascript:void()' onclick=\"document.getElementById('treeform').style.display='block';document.getElementById('textboxform').style.display='none';loadChild('tree1','0','$sid','$pid');\">Alternative: Add phenotypes by browsing the ontology tree</a></p>";
echo "</div>";

// print form to add info by tree
if ($_GET['ft'] == 1) { // coming from tree
	$style = "";
	echo "<script type='text/javascript'>loadChild('tree1','0','$sid','$pid');</script>";
}
else {
	$style = "style='display:none;'";
}

echo "<div class=sectie $style id='treeform'>\n";
echo "<h3>Add Clinical Data for Sample $samplename</h3>\n";
echo "<h4>Using the London NeuroGenetics Database nomenclature</h4>\n";
echo "<p>";
// the tree 
echo "<div id='loadingbox'></div> \n";
echo ' <div id="tree1" class="nodecls"></div> ';
echo "</p>";
echo "<p><a href='javascript:void()' onclick=\"document.getElementById('treeform').style.display='none';document.getElementById('textboxform').style.display='block'\">Alternative: Add phenotypes by providing the clinical terms</a></p>";

echo "</div>";



// print currently associated characteristics
echo "<div class=sectie id='associated'>";
echo "<h3>Associated Phenotypes for $samplename</h3>\n";
$parray = array();
$query = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
while ($row = mysql_fetch_array($query)) {
	$code = $row['lddbcode'];
	$pieces = explode('.',$code);
	$parray[$pieces[0]][$pieces[1]][] = $pieces[2];
}
mysql_select_db($lddb);
echo "<p><table cellspacing=0>";
echo "<tr>\n";
echo "<th $firstcell class=topcellalt>&nbsp;</th><th class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
echo "</tr>\n";
if ($_GET['ft'] == 1) {
	$fts = '&ft=1';
}
foreach($parray as $first => $sarray) {
	$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
	$row = mysql_fetch_array($query);
	$firstlabel = $row['LABEL'];
	foreach ($sarray as $second => $tarray) {
		if ($second == '00') {
			echo "<tr><td $firstcell><img onclick=\"delphenotype('$sid','$pid','$first.00.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
		}
		else {
			$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
			$row = mysql_fetch_array($query);
			$secondlabel = $row['LABEL'];
			foreach ($tarray as $third) {
				if ($third == '00') {
					echo "<tr><td $firstcell><img onclick=\"delphenotype('$sid','$pid','$first.$second.00')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>\n";	
				}
				else {
					$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
					$row = mysql_fetch_array($query);
					$thirdlabel = $row['LABEL'];
				
					echo "<tr><td $firstcell><img onclick=\"delphenotype('$sid','$pid','$first.$second.$third')\" src='images/content/delete.gif'></td><td>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>\n";
				}	
			}
		}
	}
}
echo" </table>";
echo "</div>";
mysql_select_db($cnvdb);

?>
<!-- Start autocomplete function -->
<script type='text/javascript'>

var options = {
		script:"lddb_suggestions.php?json=true&limit=10&",
		varname:"input",
		json:true,
		shownoresults:true,
		delay:200,
		cache:false,
		timeout:10000,
		minchars:2,
		callback: function (obj) { document.getElementById('traitfield').value = obj.value;document.getElementById('traitcode').value = obj.id; }
	};
	var as_json = new bsn.AutoSuggest('traitfield', options);
	

</script>
