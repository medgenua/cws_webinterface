<?php
// for syntax
ob_end_flush();
// unset session vars from editing cnv
if (isset($_SESSION['setfromeditcnv'])) {
	unset($_SESSION['sample']);
	unset($_SESSION['project']);
	unset($_SESSION['setfromeditcnv']);
}


?>

<?php

################################
# CREATE CHR POSITION PLOT MAP #
################################
$result =  mysql_query("SELECT start FROM cytoBand WHERE chr = '$chr' ORDER BY start LIMIT 1");
$row = mysql_fetch_array($result);
$chrstart = $row['start'];
$result =  mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$chrstop = $row['stop'];
$chrwindow = $chrstop-$chrstart+1;
$scalechr = ($plotwidth-1)*0.80/($chrwindow);
echo "<map name='chr'>";
$query = mysql_query("SELECT start, stop, name FROM cytoBand WHERE chr= '$chr' ORDER BY start");
while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$name = $row['name'];
	$scaledstart = intval(round(($cstart)*$scalechr));
	$scaledstop = intval(round(($cstop)*$scalechr));
	$xstart = $scaledstart;
	$xend = $scaledstop;
	if ($xend - $xstart < 5) {
		$xend = $xend+1;
		$xstart = $start -1;
	}
	echo "<area shape='rect' coords='$xstart,10,$xend,20' title='". $chromhash[$chr] . "$name' href='index.php?$linkpart&amp;region=chr$chrtxt:$cstart-$cstop'>";
}
echo "</map>";

flush();

// INCLUDE RESULTS IMAGE

echo "<div id='loadingimage'><div style='text-align:center'><img src='images/content/ajax-loader.gif' width=50px /><br/>Loading Results...</div></div>";
flush();

echo "<img id='plotview' src='image.php?pw=$plotwidth&amp;l=$level&amp;u=$userid&amp;c=$chr&amp;start=$start&amp;stop=$stop$projpart' usemap='#results' border=0 onload=\"document.getElementById('loadingimage').innerHTML='';loadArea2()\">\n";
flush();

//CREATE MAP
$chrtxt = $chromhash[ $chr ];
$y = 25;
echo "<map name='results'>";
$xstart = $plotwidth-75;
$xstop = $plotwidth;
echo "<area shape='rect' coords='$xstart,1,$xstop,10' title='Set Plot Preferences' href='index.php?page=settings&amp;type=plots&amp;v=s' target='_blank'>";
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
// KARYOGRAM 
$query = mysql_query("SELECt start, stop, name FROM cytoBand WHERE chr = '$chr' $locquery ORDER BY start ");
while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$name = $row['name'];
	$scaledstart = intval(round(($cstart-$start)*$scalef));
	$scaledstop = intval(round(($cstop-$start)*$scalef));
	$xstart = $xoff + $scaledstart;
	$xend = $xoff + $scaledstop;
	$yend = $y+20;
	echo "<area shape='rect' coords='$xstart,$y,$xend,$yend' title='". $chromhash[$chr] . "$name' href='index.php?$linkpart&amp;region=chr$chrtxt:$cstart-$cstop'>";
}
flush();
$y = 48;
// REFSEQ MAP
if ($_SESSION['RefSeq'] == 1) {
  $y = $y+10; 
  $ystart = $y-1;
  $yend = $y+5;
  echo "<area shape='rect' href='index.php?page=genes&chr=$chr&start=$start&stop=$stop' target='_blank' coords='0,$ystart,60,$yend' title='RefSeq Entries: Blue: Omim; Red: Morbid; Black: Default; Hover for details; Click Here for All Gene Annotation'>";
  $genequery = mysql_query("SELECT start, stop, ID FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");
  while ($generow = mysql_fetch_array($genequery)) {
	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	//$gstrand = $generow['strand'];
	//$gsymbol = $gene
	//$gomim = $generow['omimID'];
	//$gmorbid = $generow['morbidID'];
	$gID = $generow['ID'];
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	$ystart = $y-1;
	$yend = $y+5;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(geneTT('$gID&u=$userid',event))\" onmouseout=\"UnTip()\">\n";
  }
  flush();
}
// GENCODE CODING MAP
if ($_SESSION['GCcode'] == 1) {
  $y = $y+10;
  $ystart = $y-1;
  $yend = $y+5;
  echo "<area shape='rect' coords='0,$ystart,60,$yend' title='ENCODE gencode Coding Transcripts ; Black: Validated ; Dark: Manual Annotation ; Light: Automatic Annotation ; Hover for Details'>";

  $genequery = mysql_query("SELECT start, stop, ID FROM encodesum WHERE coding = 1 AND chr = '$chr' $locquery ORDER BY start");
  while ($generow = mysql_fetch_array($genequery)) {
	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gID = $generow['ID'];
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	$ystart = $y-1;
	$yend = $y+5;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(encodeTT('$gID',event))\" onmouseout=\"UnTip()\">";
  }
  flush();
}
// GENCODE NON CODING MAP
if ($_SESSION['GCncode'] == 1) {
  $y = $y+10;
  $ystart = $y-1;
  $yend = $y+5;
  echo "<area shape='rect' coords='0,$ystart,60,$yend' title='ENCODE gencode Non-Coding Transcripts ; Black: Validated ; Dark: Manual Annotation ; Light: Automatic Annotation ; Hover for Details'>";

  $genequery = mysql_query("SELECT start, stop, ID FROM encodesum WHERE coding = 0 AND chr = '$chr' $locquery ORDER BY start");
  while ($generow = mysql_fetch_array($genequery)) {
	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gID = $generow['ID'];
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	$ystart = $y-1;
	$yend = $y+5;
	$xstart = $xoff + $scaledstart;
	$xstop = $xoff + $scaledstop;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(encodeTT('$gID',event))\" onmouseout=\"UnTip()\">";
  }
  flush();
}
// SEGMENTAL DUPLICATIONS MAP
if ($_SESSION['SegDup'] == 1) {
	$y = $y+12;
	$query = mysql_query("SELECT start, stop FROM segdup_sum WHERE chr = '$chr' $locquery ORDER BY start");
	$nrows = mysql_num_rows($query);
	$y = $y+10;
	while ($row = mysql_fetch_array($query)) {
		$cstart = $row['start'];
		$cstop = $row['stop'];
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		$ystart = $y-1;
		$yend = $y+6;
		$xstart = $xoff + $scaledstart;
		$xstop = $xoff + $scaledstop;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
		echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(segdupTT('1&amp;c=$chr&amp;start=$cstart&amp;stop=$cstop',event))\" onmouseout=\"UnTip()\">";
	}
	flush();
}


$chiptypes = array();
$y = $y+12;
// SINGLE PROJECT MAP
if ($project != "" && $custom != 1) {
	$samplequery = mysql_query("SELECT s.id, p.chiptypeid FROM projsamp ps JOIN sample s JOIN project p ON ps.idsamp = s.id AND ps.idproj = p.id WHERE ps.idproj = '$project' ORDER BY s.chip_dnanr");
	$nrrows = mysql_num_rows($samplequery);
	while($srow = mysql_fetch_array($samplequery)){
		$y = $y+10;
		#$samplename = $srow['chip_dnanr'];
		$sid = $srow['id'];
		$chiptypeid = $srow['chiptypeid'];
		$chiptypes[$chiptypeid] = 1;
		$abquery = mysql_query("SELECT id, cn, start, stop, nrsnps FROM aberration WHERE sample = '$sid' AND idproj = '$project' AND chr='$chr' $locquery ");
		
		while ($abrow = mysql_fetch_array($abquery)) {
			$id = $abrow['id'];
			$cn = $abrow['cn'];
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			#echo "REGIO: chr$chr:$cstart-$cstop<br/>";
			$csize = $cstop - $cstart +1;
			$cnrsnps = $abrow['nrsnps'];
			if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] || $cnrsnps < $_SESSION['LOHminsnp'] )) {
				continue;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			$ystart = $y-1;
			$yend = $y+6;
			$xstart = $xoff + $scaledstart;
			$xstop = $xoff + $scaledstop;
			#echo "MAP: $xstart - $xstop ; $ystart - $yend<br/>";
			if ($xstop - $xstart < 5) {
				$xstart = $xstart -1;
				$xstop = $xstop +1;
			}
			echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ToolTip('$id','$userid','i',1,event))\" onmouseout=\"UnTip()\">";
		}
	}
	flush();
}
// SINGLE CUSTOM PROJECT MAP
elseif ($project != "" && $custom == 1) {
	$samplequery = mysql_query("SELECT idsamp as sample FROM cus_sample WHERE idproj = '$project' ORDER BY sample");
	while($srow = mysql_fetch_array($samplequery)){
		$y = $y+10;
		$samplename = $srow['sample'];
		$abquery = mysql_query("SELECT id, cn, start, stop FROM cus_aberration WHERE sample = '$samplename' AND idproj = '$project' AND chr='$chr' $locquery ");
		
		while ($abrow = mysql_fetch_array($abquery)) {
			$id = $abrow['id'];
			$cn = $abrow['cn'];
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			$csize = $cstop - $cstart +1;
			$cnrsnps = $abrow['nrsnps'];
			if (preg_match("/gain/i",$cn)) {
				$cn = 3;
			}
			elseif (preg_match("/los/i",$cn)) {
				$cn = 1;
			}
			elseif (preg_match("/loh/i",$cn)) {
				$cn = 2;
			}

			if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize']  )) {
				continue;
			}
	
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			$ystart = $y-1;
			$yend = $y+6;
			$xstart = $xoff + $scaledstart;
			$xstop = $xoff + $scaledstop;
			if ($xstop - $xstart < 5) {
				$xstart = $xstart -1;
				$xstop = $xstop +1;
			}
			echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(CustomToolTip('$id','$userid','i',1,event))\" onmouseout=\"UnTip()\">";
		}
		
	}
	flush();
}
// SEARCH RESULTS MAP
else {  
	// illumina 
	if ($_SESSION['Condense'] == 0) {
		$querystring = "SELECT a.id, a.sample, a.start, a.stop, a.cn, a.nrsnps, p.chiptypeid FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id ORDER BY s.chip_dnanr ";
	}
	else {
		$NC = array('OR a.class IS NULL','AND a.class IS NOT NULL');
		$condensepart = "AND (NOT a.class IN (".$_SESSION['ToCondense'].") ".$NC[$_SESSION['NullCondense']].")";
		$querystring = "SELECT a.id, a.sample, a.start, a.stop, a.cn, a.nrsnps, p.chiptypeid FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id $condensepart ORDER BY s.chip_dnanr ";
	}
	$abquery = mysql_query("$querystring");
	$prevsample = "";
	while ($abrow = mysql_fetch_array($abquery)) {
		$id = $abrow['id'];
		$cn = $abrow['cn'];
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		$sample = $abrow['sample'];
		$chiptypeid = $abrow['chiptypeid'];
		$chiptypes[$chiptypeid] = 1;
		$csize = $cstop - $cstart +1;
		$cnrsnps = $abrow['nrsnps'];
		if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] || $cnrsnps < $_SESSION['LOHminsnp'] )) {
			continue;
		}

		if ($prevsample != $sample) {
			$y = $y+10;
			$prevsample = $sample;
		}
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		$ystart = $y-1;
		$yend = $y+6;
		$xstart = $xoff + $scaledstart;
		$xstop = $xoff + $scaledstop;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
		$id = "$id&amp;s=1";
		echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ToolTip('$id','$userid','i',1,event))\" onmouseout=\"UnTip()\">";
	}
	// condensed illumina results
	if ($_SESSION['Condense'] == 1) {
		$NC = array('AND a.class IS NOT NULL','OR a.class IS NULL');
		$condensepart = "AND (a.class IN (".$_SESSION['ToCondense'].") ". $NC[$_SESSION['NullCondense']].")";
		$y = $y+10;
		$arr = array(0,1,2,3,4);
		foreach ($arr as $i) {
			$cn = $i;
			$y = $y+10;
			#$NC = array('AND a.class IS NULL','AND a.class IS NOT NULL');
			$querystring = "SELECT a.start, a.stop, a.nrsnps, p.chiptypeid FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id WHERE pp.userid = '$userid' AND a.chr = '$chr' $locquery AND p.collection <> 'Controls' AND p.collection <> 'Parents' AND s.intrack = 1 AND s.trackfromproject = p.id AND a.cn = $i $condensepart ORDER BY a.start";
			#$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
			$abquery = mysql_query("$querystring");
			while ($abrow = mysql_fetch_array($abquery)) {
				$cstart = $abrow['start'];
				$cstop = $abrow['stop'];
				$csize = $cstop - $cstart +1;
				$cnrsnps = $abrow['nrsnps'];
				$chiptypeid = $abrow['chiptypeid'];
				$chiptypes[$chiptypeid] = 1;
				if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] || $cnrsnps < $_SESSION['LOHminsnp'] )) {
					continue;
				}
				if ($cstart < $start) {
					$cstart = $start;
				}
				if ($cstop > $stop) {
					$cstop = $stop;
				}
				$scaledstart = intval(round(($cstart-$start) * $scalef));
				$scaledstop = intval(round(($cstop-$start) * $scalef));
				$xstart = $xoff+$scaledstart;
				$xstop = $xoff+$scaledstop;
				$ystart = $y-1;
				$yend = $y+6;
				if ($xstop - $xstart < 5) {
					$xstart = $xstart -1;
					$xstop = $xstop +1;
				}
				echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(CondenseTT('1&amp;c=$chr&amp;start=$cstart&amp;stop=$cstop&amp;cn=$i','$userid',event))\" onmouseout=\"UnTip()\">";
			}
			
		}
		flush();
		$y = $y + 10;
	}
	// custom
	$querystring = "SELECT s.idsamp, a.id, a.start, a.stop, a.cn FROM cus_aberration a JOIN cus_sample s JOIN cus_project p JOIN cus_projectpermission pp ON s.idsamp = a.sample AND a.idproj = s.idproj AND a.idproj = p.id AND a.idproj = pp.projectid WHERE pp.userid = '$userid' AND s.intrack = 1 AND a.chr = '$chr' $locquery AND p.collection <> 'Control' ORDER BY s.idsamp ";
	$abquery = mysql_query("$querystring");
	$prevsample = "";
	$nrrows = mysql_num_rows($abquery);
	if ($nrrows > 0) {
		$y = $y+10;
	}
	while ($abrow = mysql_fetch_array($abquery)) {
		$id = $abrow['id'];
		$cn = $abrow['cn'];
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		$sample = $abrow['idsamp'];
		$csize = $cstop - $cstart +1;
		if (preg_match("/gain/i",$cn)) {
			$cn = 3;
		}
		elseif (preg_match("/los/i",$cn)) {
			$cn = 1;
		}
		elseif (preg_match("/loh/i",$cn)) {
			$cn = 2;
		}
		if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] )) {
			continue;
		}

		if ($prevsample != $sample) {
			$y = $y+10;
			$prevsample = $sample;
		}
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		$scaledstart = intval(round(($cstart-$start) * $scalef));
		$scaledstop = intval(round(($cstop-$start) * $scalef));
		$ystart = $y-1;
		$yend = $y+6;
		$xstart = $xoff + $scaledstart;
		$xstop = $xoff + $scaledstop;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
		$id = "$id&amp;s=1";
		echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(CustomToolTip('$id','$userid','i',1,event))\" onmouseout=\"UnTip()\">";
	}
	flush();

}

// DECIPHER
if ($_SESSION['DECsyn'] == 1 || $_SESSION['DECpat'] == 1) {
	$y = $y+10;
	#mysql_select_db("decipher");
	#$decquery = str_replace('stop', 'End', "$locquery");
}
if ($_SESSION['DECsyn'] == 1) {
  // syndromes
  $abquery = mysql_query("SELECT Start, End, ID FROM decipher_syndromes WHERE Chr = '$chrtxt' $locquery ORDER BY Start");
  while ($abrow = mysql_fetch_array($abquery)) {
	$y = $y+10;
	$cstart = $abrow['Start'];
	$cstop = $abrow['End'];
	$aid = $abrow['ID'];
	//$region = "chr$chrtxt:$cstart-$cstop";
	//echo "region $region<br>";
	if ($cstart < $start ) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	$xstart = $xoff+$scaledstart;
	$xstop = $xoff+$scaledstop;
	$ystart = $y-1;
	$yend = $y+6;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(DecipherTT('$aid&amp;t=s',event))\" onmouseout=\"UnTip()\">";
	flush();
  }
}
if ($_SESSION['DECpat'] == 1) {
  // patients
  #$abquery = mysql_query("SELECT id, start, stop FROM patsum WHERE chr = '$chr' $locquery ORDER BY Start");
  $abquery = mysql_query("SELECT start,stop FROM decipher_patients WHERE chr = '$chr' $locquery ORDER BY start");
  $patrows = mysql_num_rows($abquery) ;
  if ($patrows > 0) {
   $y = $y+10;
   $prevstart = 0;
   $prevstop = 0;
   while ($abrow = mysql_fetch_array($abquery)) {
	//$y = $y+10;
	$cstart = $abrow['start'];
	$cstop = $abrow['stop'];
	#$rowid = $abrow['id'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	if ($prevstop == 0) { // first region, initialize
		$prevstart = $cstart;
		$prevstop = $cstop;
	}
	elseif ($cstart >= $prevstop) { //region found, print map and initialize new region
		$scaledstart = intval(round(($prevstart-$start) * $scalef));
		$scaledstop = intval(round(($prevstop-$start) * $scalef));
		$xstart = $xoff+$scaledstart;
		$xstop = $xoff+$scaledstop;
		$ystart = $y-1;
		$yend = $y+6;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
		echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(DecipherTT('1&amp;t=p&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;u=$userid',event))\" onmouseout=\"UnTip()\">";
		$prevstart = $cstart;
		$prevstop = $cstop;
	}
	elseif ($cstop > $prevstop) {  // extension of previous region?
		$prevstop = $cstop;
	}

   }
   // Don't forget the last defined region!
	$scaledstart = intval(round(($prevstart-$start) * $scalef));
	$scaledstop = intval(round(($prevstop-$start) * $scalef));
	$xstart = $xoff+$scaledstart;
	$xstop = $xoff+$scaledstop;
	$ystart = $y-1;
	$yend = $y+6;
	if ($xstop - $xstart < 5) {
		$xstart = $xstart -1;
		$xstop = $xstop +1;
	}
 	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(DecipherTT('1&amp;t=p&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;u=$userid',event))\" onmouseout=\"UnTip()\">";
   flush();
  }
}

/////////////
// ECARUCA //
/////////////
if ($_SESSION['ecaruca'] == 1) {
	mysql_select_db($db);
	$y = $y+10;
	$abquery = mysql_query("SELECT start, stop FROM ecaruca WHERE chr = '$chr' $locquery AND (stop - start) < ". $_SESSION['ecarucaSize'] . " ORDER BY start");
	$patrows = mysql_num_rows($abquery) ;
	if ($patrows > 0) {
	   $y = $y+10;
	   $prevstart = 0;
	   $prevstop = 0;
	   while ($abrow = mysql_fetch_array($abquery)) {
		//$y = $y+10;
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		#$rowid = $abrow['id'];
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		if ($prevstop == 0) { // first region, initialize
			$prevstart = $cstart;
			$prevstop = $cstop;
		}
		elseif ($cstart >= $prevstop) { //region found, print map and initialize new region
			$scaledstart = intval(round(($prevstart-$start) * $scalef));
			$scaledstop = intval(round(($prevstop-$start) * $scalef));
			$xstart = $xoff+$scaledstart;
			$xstop = $xoff+$scaledstop;
			$ystart = $y-1;
			$yend = $y+6;
			if ($xstop - $xstart < 5) {
				$xstart = $xstart -1;
				$xstop = $xstop +1;
			}
			echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ecarucaTT('1&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;u=$userid',event))\" onmouseout=\"UnTip()\">";
			$prevstart = $cstart;
			$prevstop = $cstop;
		}
		elseif ($cstop > $prevstop) {  // extension of previous region?
			$prevstop = $cstop;
		}
	
	   }
	   // Don't forget the last defined region!
		$scaledstart = intval(round(($prevstart-$start) * $scalef));
		$scaledstop = intval(round(($prevstop-$start) * $scalef));
		$xstart = $xoff+$scaledstart;
		$xstop = $xoff+$scaledstop;
		$ystart = $y-1;
		$yend = $y+6;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
	 	echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ecarucaTT('1&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;u=$userid',event))\" onmouseout=\"UnTip()\">";
	   flush();
	}
}



/////////////////////
// HEALTHY PARENTS //
/////////////////////
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$y = $y+10;
if ($chr == 23) {
	$arr = array(0,1,2,3,4);
}
else {
	$arr = array(0,1,3,4);
}
foreach ($arr as $i) {
	// local vars
	$prevstop = 0;
	$prevstart = 0;
	$y = $y+10;
	$querystring = "SELECT a.start, a.stop FROM aberration a JOIN sample s JOIN project p JOIN projectpermission pp ON s.id = a.sample AND a.idproj = p.id AND a.idproj = pp.projectid WHERE  s.intrack = 1 AND s.trackfromproject = p.id AND pp.userid = $userid AND p.collection = 'Parents' AND chr = '$chr' AND cn = '$i' $locquery ORDER BY a.start, a.stop";
	$abquery = mysql_query("$querystring");
	while ($abrow = mysql_fetch_array($abquery)) {
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		if ($cstart < $start) {
			$cstart = $start;
		}
		if ($cstop > $stop) {
			$cstop = $stop;
		}
		if ($prevstop == 0) { // first region, initialize
			$prevstart = $cstart;
			$prevstop = $cstop;
		}
		elseif ($cstart >= $prevstop) { //region found, print map and initialize new region
			$scaledstart = intval(round(($prevstart-$start) * $scalef));
			$scaledstop = intval(round(($prevstop-$start) * $scalef));
			$xstart = $xoff+$scaledstart;
			$xstop = $xoff+$scaledstop;
			$ystart = $y-1;
			$yend = $y+6;
			if ($xstop - $xstart < 5) {
				$xstart = $xstart -1;
				$xstop = $xstop +1;
			}
			echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ParentPlotTT('1&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;cn=$i&amp;u=$userid',event))\" onmouseout=\"UnTip()\">";
			$prevstart = $cstart;
			$prevstop = $cstop;
		}
		elseif ($cstop > $prevstop) {  // extension of previous region?
			$prevstop = $cstop;
		}
	}
	// Don't forget the last defined region!
	if ($prevstop != 0) {
		$scaledstart = intval(round(($prevstart-$start) * $scalef));
		$scaledstop = intval(round(($prevstop-$start) * $scalef));
		$xstart = $xoff+$scaledstart;
		$xstop = $xoff+$scaledstop;
		$ystart = $y-1;
		$yend = $y+6;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
		echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ParentPlotTT('1&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;cn=$i&amp;u=$userid',event))\" onmouseout=\"UnTip()\">";
	}
}
flush();


// CONTROLS OVERVIEW
ksort($chiptypes);
$chipstring = '';
$chiparray = array();
foreach($chiptypes as $key => $value) {
	if ($key == 4) {
		$key = 10; # Cyto12v2.0 failed, substitute with 2.1
	}
	$chipq = mysql_query("SELECT Controles from chiptypes WHERE ID = '$key'");
	$row = mysql_fetch_array($chipq);
	if ($row['Controles'] == '') {
		continue;
	}
	$chiparray[$key] = $key;
}
sort($chiparray);
$chipstring = implode('-',$chiparray);

$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
if ($_SESSION['HapSame'] == 1) {
	$y = $y+10;
	if ($chr == 23) {
		$arr = array(0,1,2,3,4);
	}
	else {
		$arr = array(0,1,3,4);
	}
	foreach ($arr as $i) {
		$y = $y+10;
		$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
		$abquery = mysql_query("$querystring");
		while ($abrow = mysql_fetch_array($abquery)) {
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			$xstart = $xoff+$scaledstart;
			$xstop = $xoff+$scaledstop;
			$ystart = $y-1;
			$yend = $y+6;
			if ($xstop - $xstart < 5) {
				$xstart = $xstart -1;
				$xstop = $xstop +1;
			}
			echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ControlTT('1&amp;c=$chr&amp;start=$cstart&amp;stop=$cstop&amp;cn=$i&amp;chips=$chipstring',event))\" onmouseout=\"UnTip()\">";
		}
	}
	flush();
}
if ($_SESSION['HapAll'] == 1) {
	$y = $y+10;
	if ($chr == 23) {
		$arr = array(0,1,2,3,4);
	}
	else {
		$arr = array(0,1,3,4);
	}
	$chipq = mysql_query("SELECT ID,Controles from chiptypes");
	$chipstring = '';
	while ($row = mysql_fetch_array($chipq)) {
		if ($row['Controles'] == '') {
			continue;
		}
		$chipstring .= $row['ID'] . '-';
	}
	$chipstring = substr($chipstring,0,-1);	
	foreach ($arr as $i) {
		$y = $y+10;
		$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
		$abquery = mysql_query("$querystring");
		while ($abrow = mysql_fetch_array($abquery)) {
			$cstart = $abrow['start'];
			$cstop = $abrow['stop'];
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			$scaledstart = intval(round(($cstart-$start) * $scalef));
			$scaledstop = intval(round(($cstop-$start) * $scalef));
			$xstart = $xoff+$scaledstart;
			$xstop = $xoff+$scaledstop;
			$ystart = $y-1;
			$yend = $y+6;
			if ($xstop - $xstart < 5) {
				$xstart = $xstart -1;
				$xstop = $xstop +1;
			}
			echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(ControlTT('1&amp;c=$chr&amp;start=$cstart&amp;stop=$cstop&amp;cn=$i&amp;chips=$chipstring',event))\" onmouseout=\"UnTip()\">";

		}
	}
	flush();
}
if ($_SESSION['DGVshow'] == 1) {
	$y = $y+10;
	$DGVinc = $_SESSION['DGVinclude'];
	$dgvtypes = array(1 => "gain", 2 => "loss", 3 => "inv");
	for ($i = 1; $i <= 3; $i++) {
		$y = $y+10;
		$dgvtype = $dgvtypes[$i];
		$query = mysql_query("SELECT start, stop FROM DGV_full WHERE chr = '$chr' AND $dgvtype > 0 AND study IN ($DGVinc) $locquery ORDER BY start, stop");
		$prevstart = 0;
		$prevstop = 0;
		$cstart = 0;
		$cstop = 0;
		while ($row = mysql_fetch_array($query)) {
			$cstart = $row['start'];
			$cstop = $row['stop'];
			if ($cstart < $start) {
				$cstart = $start;
			}
			if ($cstop > $stop) {
				$cstop = $stop;
			}
			if ($prevstop == 0) { // first region, initialize
				$prevstart = $cstart;
				$prevstop = $cstop;
			}
			elseif ($cstart >= $prevstop) { //region found, print map and initialize new region
				$scaledstart = intval(round(($prevstart-$start) * $scalef));
				$scaledstop = intval(round(($prevstop-$start) * $scalef));
				$xstart = $xoff+$scaledstart;
				$xstop = $xoff+$scaledstop;
				$ystart = $y-1;
				$yend = $y+6;
				if ($xstop - $xstart < 5) {
					$xstart = $xstart -1;
					$xstop = $xstop +1;
				}
				echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(dgvTT('1&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;type=$dgvtype&amp;uid=$userid',event))\" onmouseout=\"UnTip()\">";
				$prevstart = $cstart;
				$prevstop = $cstop;
			}
			elseif ($cstop > $prevstop) {  // extension of previous region?
				$prevstop = $cstop;
			}
		}
		// Don't forget the last defined region!
		$scaledstart = intval(round(($prevstart-$start) * $scalef));
		$scaledstop = intval(round(($prevstop-$start) * $scalef));
		$xstart = $xoff+$scaledstart;
		$xstop = $xoff+$scaledstop;
		$ystart = $y-1;
		$yend = $y+6;
		if ($xstop - $xstart < 5) {
			$xstart = $xstart -1;
			$xstop = $xstop +1;
		}
		echo "<area shape='rect' coords='$xstart,$ystart,$xstop,$yend' onmouseover=\"Tip(dgvTT('1&amp;c=$chr&amp;start=$prevstart&amp;stop=$prevstop&amp;type=$dgvtype&amp;uid=$userid',event))\" onmouseout=\"UnTip()\">";

	}
	flush();
}


echo "</map>";

?>
</div>
<script type="text/javascript">
// passed variables from php
var plotstart = <?php echo $start; ?>;
var plotstop = <?php echo $stop; ?>;
var xoff = <?php echo $xoff; ?>;
var plotwidth = <?php echo $plotwidth; ?>;
var chrstart = <?php echo $chrstart; ?>;
var chrstop = <?php echo $chrstop; ?>;
var scalechr = <?php echo $scalechr; ?>;
var scalef = (plotwidth  - xoff - 1)/(plotstop - plotstart + 1);
var chrom = <?php echo $chr; ?>;
function loadArea2() {
     $('img#plotview').imgAreaSelect({
        handles: false,
	autoHide: true,
	fadeSpeed: 50,
	movable: false,
	minHeight: document.getElementById('plotview').height,
	onSelectEnd: function (img, selection) {
		// calculate selected coordinates;	
		var window = plotstop - plotstart + 1;
		var selstart = selection.x1;
		var selstop = selection.x2;
		if (selstart < xoff) {
			return;
		}
		if (selstop - selstart < 2) {
			return;
		}
        	var newstart = Math.round(plotstart + (selstart - xoff + 1)/scalef);
		var newstop = Math.round(plotstart + (selstop - xoff + 1)/scalef);
		//alert('selected width: ' + plotwidth + '; target from ' + newstart + ' to ' + newstop );
		location.href='index.php?<?php $linkpart = str_replace('amp;','',$linkpart);echo $linkpart; ?>&region=chr'+chrom+':'+newstart+'-'+newstop; 
   	} 
    });
}

</script>
<div id="txtHint">something must appear here</div>

