<?php
if ($loggedin != 1) {
	echo "<div class=sectie>\n<h3>You are not logged in.</h3>\n";
	include('login.php');
	echo "</div>\n";
	exit;
}

 
$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

$longoptions = array('Sample Identifier', 'Chromosome', 'Start Position', 'End Position', 'Size', 'Copy Number', 'Score', 'Nr. Probes', 'Sample Gender', 'Max Size Start Position', 'Max Size End Position', 'Platform', 'Chip Type','Inheritance','Class','Clinical Info');
$options = array('Sample', 'Chr', 'Start', 'End|Stop', 'Size', 'Value|CopyNumber|CNV|CN','Confidence|LBF|score', 'SNPS|Probes|NrSNP', 'Gender','MaxStart', 'MaxEnd', 'Platform|Array', 'Array|piattaforma|Chip|Chiptype','Origin|Inheritance|Inh', 'Class','Descr|Clinic');
$formoptions = array('sample', 'chr', 'start', 'stop', 'size', 'cn','confidence','nrprobes', 'sgender','largestart','largestop','platform','chiptype','inheritance','class','sdescr');

///////////////////////////////
// first delete if specified //
///////////////////////////////
if (isset($_POST['del'])) {
	$todel = $_POST['del'];
	$query = mysql_query("DELETE FROM parsers WHERE id = '$todel'");
}

####################################
## LIST CURRENT PARSERS TO DELETE ##
####################################
$query = mysql_query("SELECT id, Name, Array, Algorithm, Comments FROM parsers WHERE uid = '$userid'");
if (mysql_num_rows($query) > 0 && !isset($_POST['step'])) {
	echo "<div class=sectie>";
	echo "<h3>Previously created Parsers</h3>";
	echo "<p>Below are the parsers you have created on this platform. Press 'Delete' to remove items.</p>";
	echo "<p><table cellspacing=0>";
	echo "<tr><th $firstcell>Name</th><th>Platform</th><th>Algorithm</th><th>Comments</th><th>Delete</th></tr>";
	while ($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['Name'];
		$array = $row['Array'];
		$algo = $row['Algorithm'];
		$comment = $row['Comments'];
		echo "<tr>";
		echo "<td $firstcell>$name&nbsp;</td><td>$array&nbsp;</td><td>$algo&nbsp;</td><td>$comment&nbsp;</td>";
		echo "<td><form action='index.php?page=custom_methods&type=cp&v=c' method=POST><input type=submit class=button value='Delete'><input type=hidden name=del value='$id'></form></td>";
		echo "</tr>";
	}
	echo "</table>";
	echo "</div>";
}

##############################
# GET the COLLECTION options #
##############################
if (!isset($_POST['step'])) {
	echo "<div class=sectie>\n";
	echo "<h3> Create a New Results Parser</h3>\n";
	echo "<p> If you have a file format not currently supported and you want to upload the results, please create a new parser here. This parser will be available to other people afterwards.</p>\n";
	echo "</div>\n";
	echo "<div class=sectie>\n";
	echo "<h3>Step 1: Upload input file</h3>\n";
	echo "<p>Select a file containing the results you want to be parsed. This file will be used to monitor progress.</p>\n";
	echo "<p><form enctype='multipart/form-data' action='index.php?page=custom_methods&amp;type=cp' method=POST>\n";
	echo "<input type=hidden name=step value=2>\n";
	echo "Pick a file: <input type='file' name='inputfile' size='30' /><br />\n";
	echo "<br />";
	echo "<input type='submit' class='button' name='submit' value='Next'>\n";
	echo "</form>\n";
	echo "</div>\n";
}
elseif ($_POST['step'] == 2) {
	echo "<div class=sectie>\n";
	echo "<h3>Step2: Set file format</h3>\n";
	echo "<p>";
	$filetype = $_FILES['inputfile']['type'];
	$ok = 1;
 	if (!($filetype == "text/plain") ) {
 		echo "<span class=nadruk>Problem:</span> Only .txt and .xml files are allowed. (detected $filetype )<br />";
   		$ok = 0;
 	}
 	if ($ok == 1) {
		$filename = basename( $_FILES['inputfile']['name']);

  		if(!move_uploaded_file($_FILES['inputfile']['tmp_name'], "/tmp/$filename") ) {
  			$ok = 0;
			echo "<span class=nadruk>Problem:</span> Upload to /tmp/$filename failed.<br />";
 		}
 	}
	if ($ok == 1) {
		# UPLOADED, start processing
		echo "<form action='index.php?page=custom_methods&amp;type=cp' method='POST'>\n";
		echo "<input type=hidden name='step' value='3'>\n";
		echo "<input type=hidden name='inputfile' value='$filename'>\n";
		$fh = fopen("/tmp/$filename", "r");
		$headline = fgets($fh);
		if (!preg_match("/(\t|;|,|)/",$headline,$matches)) {
			echo "<p>Non tabular file detected, xml files are not supported for now</p>\n";
		}
		else {	
			if (preg_match("/\t/",$headline)){
				$seperator = "\t";
			}
			elseif(preg_match("/;/",$headline)){
				$seperator = ";";
			}
			elseif(preg_match(",",$headline)){
				$seperator = ",";
			}
			echo "<input type=hidden name=seperator value='$seperator'>\n";
			echo "<p>A tabular file format was detected. Please specify the contents of the found columns below. Entries from the first line were used as headings, this behaviour can be changed by deselecting it below. </p>\n";
			echo "<p>If you are missing values in the example data column, this might indicate multiline data. This is typical for exported table files from BeadStudio Bookmark Viewer, where the 'Comments' column can contain multiline info. You can set the number of lines per entry below. </p>\n";
			echo "<p>When individual column values need to be refined to extract the correct information, select them using the checkboxes.You can set a column to be discarded as a whole and still refine it afterwards to extract some of its information.</p>\n";
			echo "<p><table cellspacing=0>\n";
			// check for tableheaders
			if ($_POST['tableheaders'] == 1) {
				$headline = chop($headline);
				echo "<tr><td class=clear>Input file contains column headers </td><td class=clear><input type=checkbox name=tableheaders value=1 CHECKED></td></tr>\n";
				$secondline = chop(fgets($fh));
				$colheadline = "<th >Found Header</th>\n";
			}
			else {
				echo "<tr ><td class=clear>Input file contains column headers</td><td class=clear><input type=checkbox name=tableheaders value=1 ></td></tr>\n";
				$secondline = chop($headline);
				$colheadline = "";
			}
			// check for multiline entries
			if (!isset($_POST['linesperentry'])) {
				$linesperentry = 1;
			}
			else {
				$linesperentry = $_POST['linesperentry'];
			}
			echo "<tr ><td class=clear>How many lines to use per data entry</td><td class=clear><input type='text' name='linesperentry' value='$linesperentry' size='3'/></td></tr>\n";
			echo "</table></p>\n";
			echo "<p><input type=submit class=button name='changedheaders' value=Submit></p>\n";
		

			// concatenate multiline entries
			for ($i = 2; $i<= $lineperentry; $i++) {
				 $secondline .= chop(fgets($fh));
			}

			$cols = explode("$seperator",$headline);
			$values = explode("$seperator",$secondline);
				
			echo "<p><table cellspacing=0>\n";
			echo "<tr>\n";
			echo "<th $firstcell>Column</th>\n";
			echo $colheadline;
			echo "<th >Example Data</th>\n";
			echo "<th>Target</th>\n";
			echo "<th>Refine</th>\n";
			echo "</tr>\n";
			$idx =0;
			foreach ($cols as $value) {
				$idx++;
				$content = '';
				$good = 0;	
				foreach($options as $okey => $ovalue) {
					if (preg_match("/$ovalue/",$value)) {
						$content .= "<option value=$formoptions[$okey] SELECTED>$longoptions[$okey]</option>";
						$good = 1;
					}
					else {
						$content .= "<option value=$formoptions[$okey] >$longoptions[$okey]</option>";
					}
				}
				if ($good == 0) {
					$content .= "<option value=discard SELECTED>Discard</option>";
				}
				else {
					$content .= "<option value=discard>Discard</option>";
				}

				echo "<tr>\n";
				echo "<th $firstcell>$idx</th>\n";
				if ($_POST['tableheaders'] == 1) {
					echo "<td>$value</td>\n";
				}
				echo "<td>".$values[$idx-1]."</td>\n";
				echo "<td><select name=col$idx>$content</select></td>\n";
				echo "<td align=center><input type=checkbox name='refine$idx' value='1'></td>\n";
				echo "</tr>\n";

			}
			echo "</table>\n";
			echo "</p><p>\n";
			echo "There were $idx columns found...<br/>\n";
			echo "<input type=hidden name=nrcols value=$idx>\n";
			echo "<input type=submit class=button name=globalsubmit value=Continue>\n";
			echo "</form>\n";
			echo "</div>\n";
		}
	}
	
}
elseif ($_POST['step'] == 3) {
	$filename = $_POST['inputfile'];
	$seperator = $_POST['seperator'];
	if (isset($_POST['changedheaders'])) {
		echo "<div class=sectie>\n";
		echo "<h3>Step 2: Set file format</h3>\n";

		echo "<form action='index.php?page=custom_methods&amp;type=cp' method='POST'>\n";
		echo "<input type=hidden name='step' value='3'>\n";
		echo "<input type=hidden name='inputfile' value='$filename'>\n";
		echo "<input type=hidden name='seperator' value='$seperator'>\n";
		$fh = fopen("/tmp/$filename", "r");
		$headline = fgets($fh);
		if (!preg_match("/(\t|;|,|)/",$headline)) {
			echo "<p>Non tabular file detected, xml files are not supported for now</p>\n";
		}
		else {
			echo "<p>A tabular file format was detected. Please specify the contents of the found columns below. Entries from the first line were used as headings, this behaviour can be changed by deselecting it below.</p>\n ";
			echo "<p>If you are missing values in the example data column, this might indicate multiline data. This is typical for exported table files from BeadStudio Bookmark Viewer, where the 'Comments' column can contain multiline info. You can set the number of lines per entry below. </p>\n ";
			echo "<p>When individual column values need to be refined to extract the correct information, select them using the checkboxes. You can set a column to be discarded as a whole and still refine it afterwards to extract some of its information.</p>\n";
			echo "<p><table cellspacing=0>\n";

			// check for tableheaders
			if ($_POST['tableheaders'] == 1) {
				$headline = chop($headline);
				echo "<tr><td class=clear>Input file contains column headers </td><td class=clear><input type=checkbox name=tableheaders value=1 CHECKED></td></tr>\n";
				$secondline = chop(fgets($fh));
				$colheadline = "<th >Found Header</th>\n";
			}
			else {
				echo "<tr ><td class=clear>Input file contains column headers</td><td class=clear><input type=checkbox name=tableheaders value=1 ></td></tr>\n";
				$secondline = chop($headline);
				$colheadline = "";
			}
			// check for multiline entries
			if (!isset($_POST['linesperentry'])) {
				$linesperentry = 1;
			}
			else {
				$linesperentry = $_POST['linesperentry'];
			}
			echo "<tr ><td class=clear>How many lines to use per data entry</td><td class=clear><input type='text' name='linesperentry' value='$linesperentry' size='3'/></td></tr>\n";
			echo "</table></p>\n";
			echo "<p><input type=submit class=button name='changedheaders' value=Submit></p>\n";
		

			// concatenate multiline entries
			for ($i = 2; $i <= $linesperentry; $i+=1) {
				 $secondline .= chop(fgets($fh));
			}

			$cols = explode("$seperator",$headline);
			$values = explode("$seperator",$secondline);
				
			echo "<p><table cellspacing=0>\n";
			echo "<tr>\n";
			echo "<th $firstcell>Column</th>\n";
			echo $colheadline;
			echo "<th >Example Data</th>\n";
			echo "<th>Target</th>\n";
			echo "<th>Refine</th>\n";
			echo "</tr>\n";
			$idx =0;
			foreach ($cols as $value) {
				$idx++;
				$content = '';
				$good = 0;	
				foreach($options as $okey => $ovalue) {
					if (preg_match("/$ovalue/i",$value)) {
						$content .= "<option value=$formoptions[$okey] SELECTED>$longoptions[$okey]</option>";
						$good = 1;
					}
					else {
						$content .= "<option value=$formoptions[$okey] >$longoptions[$okey]</option>";
					}
				}
				if ($good == 0) {
					$content .= "<option value=discard SELECTED>Discard</option>";
				}
				else {
					$content .= "<option value=discard>Discard</option>";
				}
				echo "<tr>\n";
				echo "<th $firstcell>$idx</th>\n";
				if ($_POST['tableheaders'] == 1) {
					echo "<td>$value</td>\n";
				}
				echo "<td>".$values[$idx-1]."</td>\n";
				echo "<td><select name=col$idx>$content</select></td>\n";
				echo "<td align=center><input type=checkbox name='refine$idx' value='1'></td>\n";
				echo "</tr>\n";

			}
			echo "</table>\n";
			echo "</p><p>\n";
			echo "<input type=hidden name=nrcols value=$idx>\n";
			echo "<input type=submit class=button name=globalsubmit value=Continue>\n";
			echo "</form>\n";
			echo "</div>\n";
		}
	}
	elseif (isset($_POST['globalsubmit'])) {
		// GET POSTED VARS
		$filename = $_POST['inputfile'];	
		$linesperentry = $_POST['linesperentry'];
		//echo "lpe: $linesperentry<br>\n";
		$ncols = $_POST['nrcols'];
		$tableheaders = $_POST['tableheaders'];
		$seperator = $_POST['seperator'];
		if ($tableheaders != 1) {
			$tableheaders = 0;
		}
		
		echo "<div class=sectie>";
		echo "<h3>Step 3: Refine</h3>\n";
		echo "<p>The parser is almost finished. you should now confirm your settings below and submit. If you have selected columns to be refined, enter the needed information, click submit and check the output in the table at the bottom of the page.</p>\n";
		echo "<form action='index.php?page=custom_methods&amp;type=cp' method=POST>\n";
		echo "<input type=hidden name=inputfile value='$filename'>\n";
		echo "<input type=hidden name=step value=3>\n";
		echo "<input type=hidden name=nrcols value=$ncols>\n";
		echo "<input type=hidden name=tableheaders value='$tableheaders'>\n";
		echo "<input type=hidden name=linesperentry value=$linesperentry>\n";
		echo "<input type=hidden name=seperator value='$seperator'>\n";
		
		// OPEN FH & GET example entry
		$fh = fopen("/tmp/$filename", "r");
		if ($tableheaders == 1) {
			$headline = chop(fgets($fh));
		}
		$exampleline = '';
		for ($i = 1; $i <= $linesperentry; $i++) {
			$exampleline .= chop(fgets($fh));
		}
		$example = explode("$seperator",$exampleline);
		$coldefs = array();
		$refines = array();	
		// check columns for refinement && set column ids
		for ($i = 1; $i <= $ncols; $i++) {
			$coldefs[$i] = $_POST["col$i"];	
			echo "<input type=hidden name='col$i' value='".$coldefs[$i]."'>\n";
			if ($_POST["refine$i"] == 1) {
				echo "<input type=hidden name='refine$i' value=1>\n";
				$regex = $_POST["col$i"."_regex"];
				$keep = $_POST["col$i"."_keep"];
				$refines[$i] = $keep;
				echo "<p><span class=nadruk>Refining column $i:</span></p>";
				echo "<p><table cellspacing=0 style='margin-left:10px;'>\n";
				echo "<tr>\n";
				echo " <th $firstcell>Example entry </th>\n";
				echo " <td style='border-top: 1px solid #a1a6a4;padding-left:3px' colspan=2>".$example[$i-1]."</td>\n";
				echo "</tr>\n";
				echo "<tr>\n";
				echo " <th $firstcell>Regular expression (<a href='http://php.net/manual/en/function.preg-match.php' target='_blank' style='background:#e3e3e6;' >?</a> <a href='http://www.troubleshooters.com/codecorn/littperl/perlreg.htm' target='_blank' style='background:#e3e3e6;'> ?</a>)</th>\n";	
				echo " <td colspan=2><input type=text name=col$i"."_regex size=40 value='$regex'></td>\n";
				echo "</tr>\n";
				echo "<tr>\n";
				echo " <th $firstcell>Keep (space seperated)</th>\n";
				echo " <td colspan=2><input type=text name=col$i"."_keep size=10 value='$keep'></td>\n";
				echo "</tr>\n";
				if ($regex != '' && $keep != '') {
					$result = preg_match("/$regex/i",$example[$i-1],$matches);
					if ($result === false) {
						echo "</table>\n";
						echo "<p>The provided regular expression did not match the sample value!</p>\n";
					}
					else {
						$keeps = explode(" ",$keep);
						foreach ($keeps as $idx) {
							$content = '<option value="none"></option>';
							foreach($options as $okey => $ovalue) {
								if (preg_match("/$ovalue/i",$_POST["col$i"."_keep$idx"])) {
									$content .= "<option value=$formoptions[$okey] SELECTED>$longoptions[$okey]</option>";
								}
								else {
									$content .= "<option value=$formoptions[$okey] >$longoptions[$okey]</option>";
								}
							
							}
							
							if (preg_match("/discard/i",$_POST["col$i"."_keep$idx"])) {
								$content .= "<option value=discard SELECTED>Discard</option>";
							}
							else {
								$content .= "<option value=discard>Discard</option>";
							}
							
							echo "<tr>\n";
							echo " <th $firstcell>group $idx</th>\n";
							echo " <td>$matches[$idx]</td>\n";
							echo " <td><select name='col$i"."_keep$idx'>$content</select></td>\n";
							echo "</tr>\n";
						}
						echo "</table>\n";
					}
				}
				else {
					echo "</table>\n";
				}
			}
			else {
				$refines[$i] = 0;
			}
		}
		echo "<p>\n";
		echo "<input type=submit class=button name=globalsubmit value='Check'>";
		//echo "</form>\n";
		echo "</p>\n";
		echo "</div>\n";


		// PRINT RESULTING TABLE TO CHECK EVERYTHING
		echo "<div class=sectie>\n";
		echo "<h3>Check your settings</h3>\n";
		
		echo "<p>To check your settings, the first 5 entries are parsed into the fields you have specified. Please check everything and submit your parser.</p>\n";
		echo "<p>\n";
		echo "<table cellspacing=0>\n";
		echo "<tr>\n";
		$fc = $firstcell;
		// headings
		$setcols = array();
		$oidx = 1;
		for ($col = 1; $col <= $ncols; $col++){
			if ($coldefs[$col] == 'discard' && $refines[$col] == 0) {
				continue;
			}
			if ($refines[$col] == 0) {
				echo " <th $fc>$coldefs[$col]</th>\n";	
				$setcols[$oidx] = $coldefs[$col];
				#echo "setcols for key $oidx and value " . $coldefs[$col] . " : Result: ". $setcols[$oidx] ."<br/>";
				$oidx++;
				$fc = '';
			}
			else {
				$keep = $refines[$col];
				$keeps = explode(" ",$keep);
				foreach($keeps as $idx) {
					echo " <th $fc>".$_POST["col$col"."_keep$idx"]."</th>\n";
					$setcols[$oidx] = $_POST["col$col"."_keep$idx"];
					$oidx++;
					$fc = '';
				}
			}
		}
		echo "</tr>\n";
		// first line, was already read in
		$fc = $firstcell;
		for ($col = 1; $col <= $ncols; $col++){
			if ($coldefs[$col] == 'discard' && $refines[$col] == 0) {
				continue;
			}
			if ($refines[$col] == 0) {
				echo " <td $fc>".$example[$col-1]."</td>\n";	
				$fc = '';
			}
			else {
				$keep = $refines[$col];
				$keeps = explode(" ",$keep);
				$out = preg_match("/".$_POST["col$col"."_regex"]."/i",$example[$col-1],$matches);
				foreach($keeps as $idx) {
					echo " <td $fc>".$matches[$idx]."</td>\n";
					$fc = '';
				}
			}
		}
		echo "</tr>\n";
		// next 4 rows
		for ($row = 2; $row <= 5; $row++) {
			$line = '';
			for ($lidx = 1; $lidx <= $linesperentry; $lidx++) {
				$line .= chop(fgets($fh));
			}
			$example = explode("$seperator",$line);
			echo "<tr>\n";
			$fc = $firstcell;
			for ($col = 1; $col <= $ncols; $col++){
				if ($coldefs[$col] == 'discard' && $refines[$col] == 0) {
					continue;
				}
				if ($refines[$col] == 0) {
					echo " <td $fc>".$example[$col-1]."</td>\n";	
					$fc = '';
				}
				else {
					$keep = $refines[$col];
					$keeps = explode(" ",$keep);
					$out = preg_match("/".$_POST["col$col"."_regex"]."/i",$example[$col-1],$matches);
					foreach($keeps as $idx) {
						echo " <td $fc>".$matches[$idx]."</td>\n";
						$fc = '';
					}
				}
			}
			echo "</tr>\n";
	 	}

		echo "</table>\n";
		echo "</p>\n";
		echo "</div>\n";
		
		// CHECK MANDATORY FIELDS AND PRINT SUBMIT BUTTON IF OK.
		$chrok = 0;
		$startok = 0;
		$stopok = 0;
		$cnok = 0;	
		$sampleok = 0;
		echo "<div class=sectie>\n";
		echo "<h3>Submit your parser</h3>\n";
		
		foreach ($setcols as $key => $value) {
			if (strtolower($value) == 'chr') {
				$chrok = 1;
			}
			elseif (strtolower($value) == 'start') {
				$startok = 1;
			}
			elseif (strtolower($value) == 'stop') {
				$stopok = 1;
			}
			elseif (strtolower($value) == 'cn') {
				$cnok = 1;
			}
			elseif (strtolower($value) == 'sample') {
				$sampleok = 1;
			}
		}
		if ($chrok == 0) {
			echo "<p>Chromosome is a mandatory column.</p>\n";
			$failed = 1;
		}
		if ($startok == 0) {
			echo "<p>Start position is a mandatory column.</p>\n";
			$failed = 1;
		}
		if ($stopok == 0) {
			echo "<p>End position is a mandatory column.</p>\n";
			$failed = 1;
		}
		if ($cnok == 0) {
			echo "<p>Copy Number is a mandatory column.</p>\n";
			$failed = 1;
		}
		if ($sampleok == 0) {
			echo "<p>Sample ID is a mandatory column.</p>\n";
			$failed = 1;
		}
		if ($failed == 1) {
			echo "<p>There are mandatory column identifiers missing. Submission is impossible untill this is corrected!</p>\n";
		}
		else {
			echo "<p>If you are sure everything is set up correctly, Provide some general information and click the submit button below to store the parser.</p>\n";
			echo "<p>Parser Name: <input type=text name=pname size=40><br/>\n";
			echo "Platform: <input type=text name=platform size=40><br/>\n";
			echo "Algorithm: <input type=text name=algo size=40><br/>\n";
			echo "Comments: <input type=text name=comments size=40><br/>\n";
			echo "</p>\n";
			echo "<p><input type=submit class=button name=final value='Submit Parser'>\n";
		}
		echo "</form>\n";
	}
	elseif (isset($_POST['final'])) {
		$name = $_POST['pname'];
		$platform = $_POST['platform'];
		$algo = $_POST['algo'];
		$comments = $_POST['comments'];
		$ncols = $_POST['nrcols'];
		$headers = $_POST['tableheaders'];
		$lpe = $_POST['linesperentry'];
		$seperator = $_POST['seperator'];
		$parser = "type@@@table|||cols@@@$ncols|||headers@@@$headers|||lpe@@@$lpe|||sep@@@$seperator";
		// get col info
		for ($i = 1; $i <= $ncols; $i++) {
			$coldef = $_POST["col$i"];
			$colref = $_POST["refine$i"];
			if ($colref == 1) {
				$coldef = 'Refine';
				$regex = $_POST["col$i"."_regex"];
				$keep = $_POST["col$i"."_keep"];
				$keeps = explode(" ",$keep);
				$keepdef = '';
				foreach($keeps as $idx) {
					$keepdef .= $_POST["col$i"."_keep$idx"]." ";
				}
				$keepdef = rtrim($keepdef);
				$parser .= "|||col$i"."_def@@@$coldef|||col$i"."_regex@@@$regex|||col$i"."_keep@@@$keep|||col$i"."_keepdef@@@$keepdef";
			}
			else {
				$parser .= "|||col$i"."_def@@@$coldef";
			}
		}
		$parser = addslashes($parser);
		mysql_query("INSERT INTO parsers (Name, Array, Algorithm, Comments, parser,uid) VALUES ('$name', '$platform', '$algo', '$comments', '$parser',$userid)");
		echo "<div class=sectie>\n";
		echo "<h3>Parser saved</h3>\n";
		echo "<p>The parser was successfully stored. It will now be available for uploading custom CNV reports.</p>\n";
		echo "</div>\n";

	}
}






?>
