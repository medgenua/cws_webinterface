<?php
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';

#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

ob_start();


// PROCESS
if (isset($_POST['CreateSample'])) {
	$sname = addslashes($_POST['sname']);
	$splatform = addslashes($_POST['splatform']);
	$schiptype = addslashes($_POST['schiptype']);
	$sgender = $_POST['sgender'];
	// where to add?
	$addto = $_POST['addto'];
	if ($addto == 'new') {
		// create project
		$ptype = $_POST['sampletype'];
		$pname = addslashes($_POST['projectname']);
		$query = mysql_query("INSERT INTO `cus_project` (naam, collection, userID) VALUES ('$pname','$ptype','$userid')");
		$pid = mysql_insert_id();
		$query = mysql_query("INSERT INTO `cus_projectpermission` (projectid, userid, editcnv,editclinic, editsample) VALUES ('$pid','$userid','1','1','1')");
	}
	else {
		$pid = $_POST['targetproject'];
		// check double samplename
		$orig = $sname;
		$query = mysql_query("SELECT idsamp FROM `cus_sample` WHERE idsamp = '$sname' AND idproj = '$pid'");
		while (mysql_num_rows($query) > 0) {
			if (preg_match('/__(\d+)$/',$sname,$matches)) {
				$next = $matches[1] + 1;
				$sname = preg_replace('/__(\d+)$/',"__$next",$sname);
			}
			else {
				$sname .= "__1";
			}
			$query = mysql_query("SELECT idsamp FROM `cus_sample` WHERE idsamp = '$sname' AND idproj = '$pid'");
		}
		$renamed[$orig] = $sname;
		if ($orig != $sname) {
			echo "$orig already existed in this project. Sample was renamed to $sname<br/>";
		}


	}
	// create sample
	mysql_query("INSERT INTO cus_sample (idsamp, idproj, gender, intrack, chiptype, platform) VALUES ('$sname', '$pid', '$sgender','1', '$schiptype', '$splatform')");
	// user annotations
	$query = mysql_query("SELECT id, name, description FROM `user_annotations` WHERE type = 'sample' ORDER BY name");
	while ($row = mysql_fetch_array($query)) {
		if ($_POST['ua_'.$row['id']] != '') {
			$aid = $row['id'];
			$val = addslashes($_POST['ua_'.$row['id']]);
			mysql_query("INSERT INTO `cus_sample_x_user_annotation` (sid, pid, aid, value) VALUES ('$sname','$pid','$aid','$val')");
		}
	}

	// add regions
	$regions = explode("\n",$_POST['sregions']);
	foreach($regions as $key => $region) {
		$region = str_replace(' ','',$region);
		if ($region == '') {
			continue;
		}
		$p = explode(";",$region);
		$q = explode(":",$p[0]);
		$s = explode("-",$q[1]);
		$chr = $chromhash[substr($q[0],3)];
		$start = str_replace(',','',$s[0]);
		$stop = str_replace(',','',$s[1]);
		if (is_numeric($p[1])) {
			$cn = $p[1];
		}
		elseif (preg_match('/HomDel/i',$p[1])) {
			$cn = 0;
		}
		elseif (preg_match('/Del/i',$p[1])) {
			$cn = 1;
		}
		elseif (preg_match('/LOH/i',$p[1])) {
			$cn = 2;
		}
		elseif (preg_match('/Dup/i',$p[1])) {
			$cn = 3;
		}
		else {
			echo "<span class=nadruk>Error:</span> Copy Number '$p[1]' is not a valid option. Region '$p[0]' skipped.<br/>";
			continue;
		}
		$inh = 'Not Defined';
		if (isset($p[2]) && $p[2] != '') {
			if (preg_match('/DN/i',$p[2])) {
				$inh = 'De Novo';
			}
			elseif (preg_match('/Pat/i',$p[2])) {
				$inh = 'Paternal';
			}
			elseif (preg_match('/Mat/i',$p[2])) {
				$inh = 'Maternal';
			}
		}
		## genes
		$query = mysql_query("SELECT COUNT(ID) AS nrgenes FROM `genesum` WHERE chr = '$chr' AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start < '$start' AND stop > '$stop'))");
		$row = mysql_fetch_array($query);
		$nrgenes = $row['nrgenes'];
		$query = mysql_query("INSERT INTO `cus_aberration` (cn, start, stop, chr, sample, idproj, inheritance,nrgenes) VALUES ('$cn','$start','$stop','$chr','$sname','$pid','$inh','$nrgenes')");
	}
	echo "<div class=sectie>";
	echo "<h3>Sample $sname Created</h3>";
	echo "<p>The sample was added to the database. <a href='index.php?page=cusdetails&project=$pid&sample=$sname'>Go to the sample details</a>.</p>";
	echo "</div>";
}

echo "<div class=sectie>";
echo "<h3>Create Sample From Scratch</h3>";
echo "<p><span class=nadruk>General Details</span><br/>";
echo "<div style='padding-left:1em;'>";
echo "<form action='index.php?page=custom_methods&type=create' method=POST>";
echo "<input type=text size=30 name=sname> : Sample Name<br/>";
echo "<input type=text size=30 name=splatform> : Used Platform<br/>";
echo "<input type=text size=30 name=schiptype> : Used Chiptype<br/>";
// user annotations
$query = mysql_query("SELECT id, name, description FROM `user_annotations` WHERE type = 'sample' ORDER BY name");
while ($row = mysql_fetch_array($query)) {
	echo "<input type=text size=30 name=ua_".$row['id']." >";
	echo " : ".$row['name']."<br/>";
}
// some checkboxes
echo "<select name=sgender><option value=''>Unknown</option><option value=Male >Male</option><option value=Female >Female</option></select> : Sample Gender<br/>";
echo "</p></div><p><span class=nadruk>Storage Options</span><br/>";
echo "<div style='padding-left:1em;'>";
// add to new/existing project:
echo "<select name='addto' id='addto' onChange='ToggleFields()'><option value='new' SELECTED>Create new project</option><option value='existing'>Add to an existing project</option></select> : Where should the sample be stored<br/>";
// entry for new project
echo "<div id='newproject' style='padding-left:1em;'>";
echo "<input type=text name=projectname size=30 value='". date('Y-m-d') ."_".date('H\ui\ms\s')."'> : Project Name<br/>\n";
echo "<select name=sampletype><option value=Patient selected>Patients</option><option value=Control>Healthy Controls</option></select> : Type of Sample<br/>";
echo "</div>";	
// entry for existing project
echo "<div id='existingproject' style='display:none;padding-left:1em;'>";
$query = mysql_query("SELECT p.id,p.naam FROM `cus_project` p JOIN `cus_projectpermission` cp ON p.id = cp.projectid WHERE cp.userid = '$userid' AND cp.editsample = 1");	
if (mysql_num_rows($query) == 0) {
	echo "<span class=italic>No Projects Available (new project will be created)</span><input type=hidden name='projectname' value='". date('Y-m-d') ."_".date('H\ui\ms\s')."'><input type=hidden name=targetproject value=0>";
}
else {
	echo "<select name='targetproject'>";
	while ($row = mysql_fetch_array($query)) {
		echo "<option value='".$row['id']."'>".$row['naam']."</option>";
	}
	echo "</select>";
}	 
echo "</div>\n";
echo "</div>";
echo "<p><span class=nadruk>Copy Number Variants</span><br/>";
echo "<div style='padding-left:1em;'>";
echo "<p>Provide the CNVs in the following format, one per line:<br/>";
echo "&nbsp; chr5:1,000,000-5,000,000;del;Pat<br/>";
echo "&nbsp; chr7:6,000,000-8,000,000;dup;Mat<br/>";
echo "&nbsp; chr9:2,000,000-6,000,000;1;DN<br/>";
echo "</p>";
echo "<p>Comma's in the region are optional, Inheritance is optional. Recognised CN-indications are: Del, Dup, HomDel, LOH, 0,1,2,3,4.</p>";
echo "<p><textarea name=sregions rows=10 cols=50></textarea>";
echo "</div>";
echo "<p><input type=submit class=button name='CreateSample' value='Create Sample'></form></p></div></div>";



?>
<script type='text/javascript'>
function ToggleFields() {
	var myselect = document.getElementById('addto');
	if (myselect.options[myselect.selectedIndex].value == 'new') {
		document.getElementById('newproject').style.display='';
		document.getElementById('existingproject').style.display='none';
		document.getElementById('collection').style.display='';
		
		
	}
	else {
		document.getElementById('newproject').style.display='none';
		document.getElementById('existingproject').style.display='';
		document.getElementById('collection').style.display='none';

	}
}
</script>

