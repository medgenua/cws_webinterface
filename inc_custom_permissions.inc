<?php
$switch = 0;
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$userquery = mysql_query("SELECT id, LastName, FirstName FROM users");
$userselect = "    <option value=\"\" selected></option>\n    ";
while ($row = mysql_fetch_array($userquery)) {
	$id = $row['id'];
	$fname = $row['FirstName'];
	$lname = $row['LastName'];
	$userselect = $userselect . "    <option value=". $row['id'] . ">".$row['FirstName'] . " " . $row['LastName'] . "</option>\n";
}	

# Custom, created, set permissions, or delete
echo "<div class=sectie>\n";
if ($_GET['result'] == 1) {
	echo "<h3>Permissions : Update Successful !</h3>\n";
	echo "<h4>... permissions successfully updated</h4>\n";
}
elseif ($_GET['result'] == 2) {
	echo "<h3>Project successfully deleted</h3>\n";
}

else {
	echo "<h3>Permissions</h3>\n";
	echo "<h4>... of uploaded custom results</h4>\n";
}
$query = mysql_query("SELECT id, naam, created, collection FROM cus_project WHERE userID = '$userid' ORDER BY id DESC");
if (mysql_num_rows($query) == 0) {
	echo "<p>You have not yet started any CNV-projects</p>\n";
}
else {
	echo "<p>Below is the list of projects you have started on this server. From here, you can allow other users to see and browse your results.\n";
	echo " Just select the user from the dropdown list \"Give Access\" for the project of interest, and click \"Submit\". Follow the same procedure to cancel access by selecting for the \"Remove Access\" column. </p>\n";
	echo "<p>\n";
	echo "<p> If you want to delete a project, click the 'delete' button on the right side of the row.</p>";
	echo "<form action=cus_change_permissions.php method=POST>\n";
	echo "<input type=hidden name=uid value='$userid'>\n";
	echo "<table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</th>\n";
	//echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Collection</th>\n";
	echo "  <th scope=col class=topcellalt>Give Access</th>\n";
	echo "  <th scope=col class=topcellalt>Remove Access</th>\n";
	echo "  <th scope=col class=topcellalt>Delete</th>\n";
	echo "  <th scope=col class=topcellalt>Join</th>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['naam'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		$deloptions = "    <option value=\"\"></option>\n";
		$subquery = mysql_query("SELECT u.id, u.LastName, u.FirstName FROM users u JOIN cus_projectpermission p ON u.id = p.userid WHERE p.projectid = '$id'");
		while ($subrow = mysql_fetch_array($subquery) ) {
			$deloptions =  $deloptions . "    <option value=".$subrow['id'].">".$subrow['FirstName']." ".$subrow['LastName']."</option>\n";
		}
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		//echo "  <td $tdtype[$switch] >$created</td>\n";
		//echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		echo "  <td $tdtype[$switch] ><select name=add_$id>$userselect</select> </td>\n";
		echo "  <td $tdtype[$switch] ><select name=del_$id>$deloptions</select> </td>\n";
		echo "  <td $tdtype[$swich] ><input type='checkbox' name='deletearray[]' value=$id></td>\n";
		echo "<td $tdtype[$switch] ><input type='checkbox' name='joinarray[]' value=$id></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
	echo "</p>\n<p><input type=submit value='Update Permissions' name=submit class=button>\n\n";
	echo "<p><input type=submit class=button value='Delete Selected Projects' name=delete></p>\n";
	echo "<p><input type=submit class=button value='Join Selected Projects' name=join></p>\n";
	echo "</form>\n";
}
echo "</div>\n"; 

# INTEGRATED, access to see
echo "<div class=sectie>\n";
echo "<h3>Custom Method Results: Overview</h3>\n";
echo "<h4>Of all projects you are entitled to see</h4>\n";
$pquery = mysql_query("SELECT p.id, p.naam, p.created, p.collection, p.userID FROM cus_project p JOIN cus_projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY id DESC");
if (mysql_num_rows($pquery) == 0) {
	echo "<p>You don't have access to any project.</p>\n";
}
else {
	echo "<p>\n";
	echo "These are all the projects you have access to.  Click on 'Details' for further exploration</p>\n";
	echo "<p>\n";
	echo "<table cellspacing=0>\n";
	$switch = 0;
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	//echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Collection</td>\n";
	echo "  <th scope=col class=topcellalt>Samples</td>\n";	
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	//echo "  <th scope=col class=topcellalt>Recreate XML</td>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($pquery)) {
		$id = $row['id'];
		$name = $row['naam'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		$owner = $row['userID'];
		$countquery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS number FROM cus_aberration WHERE idproj = $id");
		$qres = mysql_fetch_array($countquery);
		$number = $qres['number'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		//echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		echo "  <td $tdtype[$switch] >$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=custom_methods&amp;type=tree&amp;collection=$collection&amp;project=$id&amp;sample=\">Details</a></td>\n";
		//echo "  <td $tdtype[$switch]><a href='index.php?page=create_custom_bookmark&amp;p=$id'>XML</a></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
}
echo "</div>\n";

	


?>
