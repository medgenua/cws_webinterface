<?php
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';

#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

ob_start();

//load file and pick parser
if ($_POST['step'] == '' || !isset($_POST['step'])) {
	$parser = $_GET['parser'];
	echo "<div class=sectie>\n";
	echo "<h3>1/ Pick a Parser and file</h3>\n";
	echo "<p>Upload and parse a custom reports file for inclusion in the database. Results will be fully browsable. </p>\n";
	echo "<p>Just pick you parser below and specify an input file. If there is no parser available, feel free to create one. As yet, there is only support for tabular file formats (no xml files).</p>\n";
	echo "<p><form enctype='multipart/form-data' action='index.php?page=custom_methods&amp;type=st' method=POST>\n";
	echo "<input type=hidden name=step value=2>\n";
	echo "<table>\n";
	
	echo " <tr>\n";
	echo "  <td class=clear>Pick a parser</td>\n";
	echo "  <td class=clear colspan=2><select name=parser onchange=\"reloadparserst(this.form)\"><option value=none></option>";
	$result = mysql_query("SELECT id, Name FROM parsers ORDER BY Array");
	while ($row = mysql_fetch_array($result)) {
		if ($row['id'] == $parser) {
			echo "<option value='".$row['id']."' SELECTED>".$row['Name']."</option>";
		}
		else {
			echo "<option value='".$row['id']."' >".$row['Name']."</option>";
		}
	}
	echo "</select></td>\n";
	echo " </tr>\n";
	if (isset($_GET['parser']) && $_GET['parser'] != 'none'){
		echo " <tr>\n";
		echo " <td class=clear>&nbsp;</td>\n";
		echo " <td class=clear>Info</td>\n";
		echo " <td class=clear style='padding-left:3px;border-left:solid 1px ;'>";
		$info = mysql_query("SELECT Array, Algorithm, Comments FROM parsers WHERE id=$parser");
		$row = mysql_fetch_array($info);
		echo "Platform: ". $row['Array'] ."<br/>Algorithm: ". $row['Algorithm'] ."<br/>Comments: ".$row['Comments']."</td>\n";
		echo "</tr>\n";
	}
	echo " <tr>\n";
	echo "  <td class=clear>Input file</td>\n";
	echo "  <td class=clear colspan=2><input type=file name=inputfile ></td>\n";
	echo " </tr>\n";	
	echo "</table>\n";
	echo "</p><p><input type=submit class=button name=submit value=Submit></p>\n";
	echo "</form>\n";
	echo "</div>\n";
}
elseif ($_POST['step'] == 2) {
	$parserid = $_POST['parser'];
	$filetype = $_FILES['inputfile']['type'];
	$ok = 1;
 	if (!($filetype == "text/plain") ) {
 		echo "<span class=nadruk>Problem:</span> Only .txt and .xml files are allowed. (detected $filetype )<br />";
   		$ok = 0;
 	}
 	if ($ok == 1) {
		$filename = basename( $_FILES['inputfile']['name']);

  		if(!move_uploaded_file($_FILES['inputfile']['tmp_name'], "/tmp/$filename") ) {
  			$ok = 0;
			echo "<span class=nadruk>Problem:</span> Upload to /tmp/$filename failed.<br />";
 		}
 	}
	if ($ok == 1) {
		// CHECK IF PARSER IS CORRECT : check two entries. 
		#system("echo $scriptpass | sudo -u $scriptuser -S bash -c \"dos2unix '/tmp/$filename'\"");
		mysql_select_db("$db");
		$query = mysql_query("SELECT parser FROM parsers WHERE id = $parserid");
		$row = mysql_fetch_array($query);
		$parserline = $row['parser'];
		$parts = explode("|||",$parserline);
		$parser = array();
		foreach ($parts as $item) {
			$subparts = explode('@@@',$item);
			$parser[$subparts[0]] = $subparts[1];
		}
		// SET GLOBAL VARS
		$ncol = $parser['cols'];
		$headers = $parser['headers'];
		$lpe = $parser['lpe'];
		$sep = $parser['sep'];
		// START PROCESSING
		$fh = fopen("/tmp/$filename", "r");
		if ($headers == 1){
			$headline = fgets($fh);
		}
		echo "<div class=sectie>\n";
		echo "<h3>2/ Check Parser</h3>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr>\n";
		$fc = $firstcell;
		// headers from parsed file
		for ($i = 1; $i<=$ncol; $i++) {
			$def = $parser["col".$i."_def"];
			if ($def == 'Refine') {
				//$regex = $parser['col'.$i.'_regex'];
				$keep = $parser['col'.$i.'_keep'];
				$keepdef = $parser['col'.$i.'_keepdef'];
				$pieces = explode(' ',$keepdef);
				$keeps = explode(' ',$keep);
				$idx = 0;
				foreach($pieces as $item){
					echo "<th $fc>$item</th>\n";
					$keepidx = $keeps[$idx];
					if (strtolower($item) == 'chr') {
						$chrcol = $i . '_'. $keepidx;
					}
					elseif (strtolower($item) == 'start') {
						$startcol = $i . "_" . $keepidx;
					}
					elseif (strtolower($item) == 'stop') {
						$stopcol = $i . "_" . $keepidx;
					}
					elseif (strtolower($item) == 'cn') {
						$cncol = $i . "_".$keepidx;
					}
					$idx++;	
					$fc = '';
				}
			}
			elseif ($def != 'discard') {
				echo "<th $fc>$def</th>\n";
				$fc = '';
				if (strtolower($def) == 'chr') {
					$chrcol = $i;
				}
				elseif (strtolower($def) == 'start') {
					$startcol = $i;
				}
				elseif (strtolower($def) == 'stop'){
					$stopcol = $i;
				} 
				elseif (strtolower($def) == 'cn') {
					$cncol = $i;
				}
			}
		}
		$lineidx = 0;
		while (!feof($fh) && $lineidx < 3) {
			#$line = fgets($line);
			#echo "<tr><td>cline: $line</td></tr>";	
			$lineidx++;
			$fc = $firstcell;
			$entry = chop(fgets($fh));
			
			for ($i = 2; $i <= $lpe; $i++) {
				#$currline = fgets($fh);
				#echo "<tr><td>line: $currline</td></tr>";
	        		$entry .=  chop(fgets($fh));
			}
			if ($entry == '') {
				continue;
			}
			#echo "<tr><td colspan=3>line $lineidx: <pre>$entry</pre></td></tr>\n";
			$pieces = explode("$sep",$entry);
			//echo " <tr>\n";
			
			// Info from file
			$tableline = '';
			for ($i = 1; $i<=$ncol;$i++) {
				if ($parser['col'.$i.'_def'] == 'discard') {
					continue;
				}
				if ($parser['col'.$i.'_def'] != 'Refine') {
					$cellvalue = $pieces[$i-1];
					if (preg_match("/Size|Start|Stop/i",$parser['col'.$i.'_def'])) {
						$cellvalue = preg_replace('/[\.,]/', '', $cellvalue);
						$cellvalue = number_format($cellvalue,0,'',',');
					}
					$tableline .= "  <td $fc>".$cellvalue."</td>\n";
					$fc = '';
					if ($chrcol == $i) {
						$currchr = $pieces[$i-1];
					}
					elseif ($startcol == $i) {
						$currstart = $pieces[$i-1];
					}
					elseif ($stopcol == $i) {
						$currstop = $pieces[$i-1];
					}
					elseif ($cncol == $i) {
						$currcn = $pieces[$i-1];
					}	
				}
				else {
					$regex = $parser['col'.$i.'_regex'];
					$keep = $parser['col'.$i.'_keep'];
					$keeps = explode(" ",$keep);
					$out = preg_match("/$regex/",$pieces[$i-1],$matches);
					foreach($keeps as $idx){
						$tableline .= "  <td $fc>".$matches[$idx]."</td>\n";
						$fc = '';
						if ($chrcol == $i."_".$idx) {
							$currchr = $matches[$idx];
						}
						elseif ($stopcol == $i."_".$idx) {
							$currstop = $matches[$idx];
						}
						elseif ($startcol == $i . '_'.$idx) {
							$currstart = $matches[$idx];
						}
						elseif ($cncol == $i . "_" . $idx) {
							$currcn = $matches[$idx];
						}

					}
				}
			}
			// print line
			echo "<tr context='customrow' chr='$currchr' start='$currstart' stop='$currstop'>\n";
			echo "$tableline";
			echo " </tr>\n";
			flush();
			ob_flush();
			
    		}
		
    		fclose($handle);
		echo "</table>\n";


	#}		
	
	echo "<p>If everything looks fine click continue below. Otherwise go back and try another parser (or create one yourself). Some parsing errors will be recognised and reported for correction later on.</p>";
	echo "<form action='index.php?page=custom_methods&amp;type=st' method=POST>";
	echo "<input type=hidden name=step value=3>\n";
	echo "<input type=hidden name=parser value='$parserid'>\n";
	echo "<input type=hidden name=filename value='$filename'>\n";
	echo "<input type=submit class=button name=submit value='Continue'>\n";
	
	echo "</form>";
	echo "</div>";
	}
}

elseif ($_POST['step'] == 3) {
	$parserid = $_POST['parser'];
	$filename = $_POST['filename'];
	$query = mysql_query("SELECT parser FROM parsers WHERE id = $parserid");
	$row = mysql_fetch_array($query);
	$parserline = $row['parser'];
	$parts = explode("|||",$parserline);
	$parser = array();
	$clincol = '';
	foreach ($parts as $item) {
		$subparts = explode('@@@',$item);
		$parser[$subparts[0]] = $subparts[1];
	}
	// SET GLOBAL VARS
	$ncol = $parser['cols'];
	$headers = $parser['headers'];
	$lpe = $parser['lpe'];
	$sep = $parser['sep'];
	echo "<form action='index.php?page=custom_methods&amp;type=st' method=POST>";
	echo "<input type=hidden name=step value=4>\n";
	echo "<input type=hidden name=parser value='$parserid'>\n";
	echo "<input type=hidden name=filename value='$filename'>\n";

	echo "<div class=sectie>\n";
	echo "<h3>3a/ General Details</h3>\n";
	echo "<p>Please fill in some general details about the submitted results. These details will be used only if the corresponding info was not found in the input file. Hover for additional information</p>\n";
	echo "<p><table class=clear>\n";
	echo "<tr class=clear title='Results are grouped by projects. Projects can be joined and renamed afterwards' >\n";
	echo "<th class=clear>Add Samples to:</th>";
	echo "<th class=clear><select name='addto' id='addto' onChange='ToggleFields()'><option value='new' SELECTED>A new Project</option><option value='existing'>An Existing Project</option></select></th>";
	echo "</tr>";
	// entry for new project
	echo "<tr id='newproject' >";
	echo "<th class=clear>Project Name</th>\n";
	echo "<td class=clear><input type=text name=projectname size=40 value='". date('Y-m-d') ."_".date('H\ui\ms\s')."'></td>\n";
	echo "</tr>\n";
	// entry for existing project
	echo "<tr id='existingproject' style='display:none;'>";
	echo "<th class=clear>Select a project</th>\n";
	echo "<td class=clear>";
	$query = mysql_query("SELECT p.id,p.naam FROM `cus_project` p JOIN `cus_projectpermission` cp ON p.id = cp.projectid WHERE cp.userid = '$userid' AND cp.editsample = 1");	
	if (mysql_num_rows($query) == 0) {
		echo "<span class=italic>No Projects Available</span><input type=hidden name='projectname' value='". date('Y-m-d') ."_".date('H\ui\ms\s')."'><input type=hidden name=targetproject value=0>";
	}
	else {
		echo "<select name='targetproject'>";
		while ($row = mysql_fetch_array($query)) {
			echo "<option value='".$row['id']."'>".$row['naam']."</option>";
		}
		echo "</select>";
	}	 
	echo "</td>\n";
	echo "</tr>\n";
	
	echo "<tr id=platform class=clear title='If no platform is specified for each CNV, use the one specified here (e.g. Agilent)'>\n";
	echo "<th class=clear>Platform</td>\n";
	echo "<td class=clear><input type=text name=platform size=40></td>\n";
	echo "</tr>\n";
	echo "<tr id=chip class=clear title='If no chiptype is specified for each CNV, use the one specified here (e.g. 44K)'>\n";
	echo "<th class=clear>Chiptype</td>\n";
	echo "<td class=clear><input type=text name=chiptype size=40></td>\n";
	echo "</tr>\n";
	echo "<tr id=collection class=clear title='Are the sumbitted aberrations seen in affected patients or in normal control?'>\n";
	echo "<th class=clear>Sample Type</td>\n";
	echo "<td class=clear><select name=sampletype><option value=Patient selected>Patients</option><option value=Control>Healthy Controls</option></select></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</div>";
	echo "<div class=sectie>\n";
	echo "<h3>3b/ Sample Details</h3>\n";
	echo "<p>Below are the samples found in the input file.  If needed, some details can be added here. </p>";
	echo "<p>When the wanted information is added, click continue below.</p>\n";

	echo "<p><input type=submit class=button name=submit value='Continue'></p>\n";

	echo "<p><table cellspacing=0>";
	echo "<tr>\n";
	echo "<th $firstcell>Sample ID</th>\n";
	echo "<th>Gender</th>\n";
	echo "<th>Clinical Information</th>\n";
	echo "</tr >\n";
	$fh = fopen("/tmp/$filename","r");
	if ($headers == 1){
		$headline = fgets($fh);
	}
	for ($i = 1; $i<=$ncol; $i++) {
		$def = $parser["col".$i."_def"];
		if ($def == 'Refine') {
			//$regex = $parser['col'.$i.'_regex'];
			$keep = $parser['col'.$i.'_keep'];
			$keepdef = $parser['col'.$i.'_keepdef'];
			$pieces = explode(' ',$keepdef);
			$keeps = explode(' ',$keep);
			$idx = 0;
			foreach($pieces as $item){
				$keepidx = $keeps[$idx];
				if (strtolower($item) == 'sample') {
					$samplecol = $i ."_".$keepidx;
				}
				elseif (strtolower($item) == 'sdescr') {
					$clincol = $i . "_" .$keepidx;
				}
				elseif(strtolower($item) == 'sgender') {
					$gendercol = $i . "_".$keepidx;
				}

				$idx++;	
			}
		}
		elseif ($def != 'discard') {
			$fc = '';
			if (strtolower($def) == 'sample') {
				$samplecol = $i;
			}
			elseif (strtolower($def) == 'sdescr') {
				$clincol = $i;
			}
			elseif(strtolower($def) == 'sgender') {
				$gendercol = $i;
			}
		}
	}
	$samples = array();
	$clinics = array();
	$genders = array();
	while (!feof($fh)) {
		$entry = chop(fgets($fh));
		for ($i = 2; $i <= $lpe; $i++) {
	       		$entry .=  chop(fgets($fh));
		}
		if ($entry == '') {
			continue;
		}
		$pieces = explode("$sep",$entry);
		// Info from file
		$tableline = '';
		$sample = '';
		$clinic = '';
		$gender = '';
		for ($i = 1; $i<=$ncol;$i++) {
			if ($i == $samplecol) {
				$samples[$pieces[$i-1]] = 1;
				$sample = $pieces[$i-1];
			}
			elseif ($i == $clincol) {
				$clinic = $pieces[$i-1];
			}
			elseif ($i == $gendercol) {
				$gender = $pieces[$i - 1];
			}
			elseif	(preg_match("/$i"."_/",$samplecol)) {
				$regex = $parser['col'.$i.'_regex'];
				$keep = $parser['col'.$i.'_keep'];
				$keeps = explode(" ",$keep);
				$out = preg_match("/$regex/",$pieces[$i-1],$matches);
				foreach($keeps as $idx){
					$colname = $i."_".$idx;
					if ($colname == $samplecol) {
						$samples[$pieces[$i-1]] = 1;
						$sample = $pieces[$i-1];
					
					}
					elseif ($colname == $clincol) {
						$clinic = $pieces[$i-1];
					} 
					elseif ($colname == $gendercol) {
						$gender = $pieces[$i - 1];
					}
				}
			}
		}
		// line finished
		// attach clinic to sample
		$clinics[$sample] = $clinic;	
		$genders[$sample] = $gender;		
	}	
	foreach($samples as $value => $placeholder) {
		$clinic = $clinics[$value];
		#echo "clinic: $clinic<br/>";
		echo "<tr>";
		echo "<td $firstcell>$value<input type=hidden name='samples[]' value='$value'></td>\n";
		echo "<td><select name='gender_$value'><option value=''></option>";
		if (strtolower($genders[$value]) == 'male') {
			echo "<option value='Male' selected>Male</option><option value='Female'>Female</option>";
		}
		elseif (strtolower($genders[$value]) == 'female') {
			echo "<option value='Male' >Male</option><option value='Female' selected>Female</option>";
		}
		else {
			echo "<option value='Male' >Male</option><option value='Female' >Female</option>";
		}
		echo "</select></td>\n";
		echo "<td><input type=text name='clinic_$value' size=40 value=\"$clinic\"></td>\n";
		echo "</tr>";
	}	

	echo "</table></p>\n";
	echo "<p><input type=submit class=button name=submit value='Continue'></p>\n";
	echo "</form>\n";
	echo "</div>\n";

}
elseif ($_POST['step'] == 4) {
	
	echo "<form action='index.php?page=custom_methods&amp;type=st' method=POST>";
	echo "<input type=hidden name=step value=5>\n";
	$parserid = $_POST['parser'];
	$filename = $_POST['filename'];
	echo "<input type=hidden name=parser value='$parserid'>\n";
	echo "<input type=hidden name=filename value='$filename'>\n";
	

	// FILE UPLOADED, LOAD THE PARSER
	$query = mysql_query("SELECT parser FROM parsers WHERE id = $parserid");
	$row = mysql_fetch_array($query);
	$parserline = $row['parser'];
	$parts = explode("|||",$parserline);
	$parser = array();
	foreach ($parts as $item) {
		$subparts = explode('@@@',$item);
		$parser[$subparts[0]] = $subparts[1];
	}
	// SET GLOBAL VARS
	$ncol = $parser['cols'];
	$headers = $parser['headers'];
	$lpe = $parser['lpe'];
	$sep = $parser['sep'];
	$errorgenders = array();
	$errorclinics = array();
	// ADD TO NEW / EXISTING PROJECT ?
	if ($_POST['addto'] == 'existing' && $_POST['targetproject'] != 0) {
		$pid = $_POST['targetproject'];
		$query = mysql_query("SELECT collection FROM `cus_project` WHERE id = '$pid'");
		$row = mysql_fetch_array($query);
		$collection = $row['collection']; 
		$defplatform = $_POST['platform'];
		$defchip = $_POST['chiptype'];
	}
	else {
		// CREATE project
		$projectname = $_POST['projectname'];
		if ($projectname == '') {
			$projectname = 	date('Y-m-d') ."_".date('H\ui\ms\s');
		}
		$created = date('Y-m-d') ." - ".date('H\ui\ms\s');
		$collection = $_POST['sampletype'];
		$query = mysql_query("INSERT INTO cus_project (naam, created, collection, userID) VALUES ('$projectname','$created','$collection','$userid')");
		$pid = mysql_insert_id();
		$defplatform = $_POST['platform'];
		$defchip = $_POST['chiptype'];
		$query = mysql_query("INSERT INTO cus_projectpermission (projectid, userid, editcnv, editclinic, editsample) VALUES('$pid','$userid','1','1','1')");
	}
	// COLLECT AND STORE SAMPLE INFORMATION
	$samples = $_POST['samples'];
	$genders = array();
	$clinics = array();
	$rename = array();
	foreach($samples as $samplename) {
		$orig = $samplename;
		$currgender = $_POST["gender_$samplename"];
		$currclinic = $_POST["clinic_$samplename"];
		$query = mysql_query("SELECT idsamp FROM `cus_sample` WHERE idsamp = '$samplename' AND idproj = '$pid'");
		while (mysql_num_rows($query) > 0) {
			if (preg_match('/__(\d+)$/',$samplename,$matches)) {
				$next = $matches[1] + 1;
				$samplename = preg_replace('/__(\d+)$/',"__$next",$samplename);
			}
			else {
				$samplename .= "__1";
			}
			$query = mysql_query("SELECT idsamp FROM `cus_sample` WHERE idsamp = '$samplename' AND idproj = '$pid'");
		}
		mysql_query("INSERT INTO cus_sample (idsamp, idproj, gender, clinical, intrack, chiptype, platform) VALUES ('$samplename', '$pid', '$currgender',ENCODE('".addslashes($currclinic)."','$encpass'),'1', '$defchip', '$defplatform')");
		$genders[$samplename] = $_POST["gender_$samplename"];
		$clinics[$samplename] = $_POST["clinic_$samplename"];
		$renamed[$orig] = $samplename;
		if ($orig != $samplename) {
			echo "$orig already existed in this project. Sample was renamed to $samplename<br/>";
		}
	}
		
	// START PROCESSING
	$fh = fopen("/tmp/$filename", "r");
	if ($headers == 1){
		$headline = "(".chop(fgets($fh)).")";
	}
	
	echo "<div class=sectie>\n";
	echo "<h3>4/ Submitting Data</h3>\n";
	echo "<p>The data are being submitted. If entries are found to be missing one or more mandatory fields, they will be shown below. These entries can be completed and resumbitted. Keep in mind however that only the mandatory fields are used in this case. Other columns are discarded !</p>";
	
	echo "<p><table cellspacing=0>\n";
	echo "<tr>\n";
	echo "<th $firstcell>Sample</th>\n";
	echo "<th >Chr</th>\n";
	echo "<th>Start</th>\n";
	echo "<th>Stop</th>\n";
	echo "<th>CopyNumber</th>\n";
	echo "<th NOWRAP>Full Entry $headline</th>\n";
	echo "</tr>\n";
	$fc = $firstcell;
	
	// headers from parsed file
	for ($i = 1; $i<=$ncol; $i++) {
		$def = $parser["col".$i."_def"];
		if ($def == 'Refine') {
			$keep = $parser['col'.$i.'_keep'];
			$keepdef = $parser['col'.$i.'_keepdef'];
			$pieces = explode(' ',$keepdef);
			$keeps = explode(' ',$keep);
			$idx = 0;
			foreach($pieces as $item){
				$keepidx = $keeps[$idx];
				if (strtolower($item) == 'chr') {
					$chrcol = $i . '_'. $keepidx;
				}
				elseif (strtolower($item) == 'start') {
					$startcol = $i . "_" . $keepidx;
				}
				elseif (strtolower($item) == 'stop') {
					$stopcol = $i . "_" . $keepidx;
				}
				elseif (strtolower($item) == 'cn') {
					$cncol = $i . "_".$keepidx;
				}
				elseif (strtolower($item) == 'sample') {
					$samplecol = $i . "_" . $keepidx;
				}
				$idx++;	
				$fc = '';
			}
		}
		elseif ($def != 'discard') {
			$fc = '';
			if (strtolower($def) == 'chr') {
				$chrcol = $i;
			}
			elseif (strtolower($def) == 'start') {
				$startcol = $i;
			}
			elseif (strtolower($def) == 'stop'){
				$stopcol = $i;
			} 
			elseif (strtolower($def) == 'cn') {
				$cncol = $i;
			}
			elseif (strtolower($def) == 'sample') {
				$samplecol = $i;
			}
		}
	}
	$errorcount=0;
	$goodcount = 0;	
	// START READING FILE
	while (!feof($fh)) {
	
		$fc = $firstcell;
		$currstart = '';
		$currstop = '';
		$currcn = '';
		$currchrtxt = '';
		$currchr = '';
		$currsample = '';
		$fieldline = '';
		$valueline = '';
		$platform = '';
		$chiptype = '';
		$entry = chop(fgets($fh));
		
		for ($i = 2; $i <= $lpe; $i++) {
	       		$entry .=  chop(fgets($fh));
		}
		if ($entry == '') {
			continue;
		}
		$pieces = explode("$sep",$entry);
		// Info from file
		for ($i = 1; $i<=$ncol;$i++) {
			if ($parser['col'.$i.'_def'] == 'discard') {
				continue;
			}
			if ($parser['col'.$i.'_def'] != 'Refine') {
				if ($chrcol == $i) {
					$currchrtxt = $pieces[$i-1];
					$currchr = $chromhash[$currchrtxt];
					$fieldline .= 'chr,';
					$valueline .= "'$currchr',";
				}
				elseif ($startcol == $i) {
					$currstart = $pieces[$i-1];
					$currstart = preg_replace('/[\.,]/', '', $currstart);
					$fieldline .= 'start,';
					$valueline .= "'$currstart',";
					
				}
				elseif ($stopcol == $i) {
					$currstop = $pieces[$i-1];
					$currstop =  preg_replace('/[\.,]/', '', $currstop);
					$fieldline .= 'stop,';
					$valueline .= "'$currstop',";
				}
				elseif ($cncol == $i) {
					$currcn = $pieces[$i-1];
					$fieldline .= 'cn,';
					$valueline .= "'$currcn',";
				}	
				elseif ($samplecol == $i) {
					$currsample = $pieces[$i-1];
					$fieldline .= 'sample,';
					$valueline .= "'".$renamed[$currsample]."',";
				}
				else {
					if (preg_match("/Platform/i",$parser['col'.$i.'_def'])) {
						$platform = $pieces[$i-1];
					}
					elseif (preg_match("/Chiptype/i",$parser['col'.$i.'_def'])) {
						$chiptype = $pieces[$i-1];
					}
					$fieldline .= $parser['col'.$i.'_def'] . ",";
					$valueline .= "'".$pieces[$i-1] . "',";
				}
			}
			else {
				$keepdef = $parser['col'.$i.'_keepdef'];
				$keepdefpieces = explode(' ',$keepdef);
				$regex = $parser['col'.$i.'_regex'];
				$keep = $parser['col'.$i.'_keep'];
				$keeps = explode(" ",$keep);
				$out = preg_match("/$regex/",$pieces[$i-1],$matches);
				$counter = -1;
				foreach($keeps as $idx){
					$counter++; 
					$tableline .= "  <td $fc>".$matches[$idx]."</td>\n";
					$fc = '';
					if ($chrcol == $i."_".$idx) {
						$currchrtxt = $matches[$idx];
						$currchr = $chromhash[$currchrtxt];
						$fieldline .= "chr,";
						$valueline .= "'$currchr',";
					}
					elseif ($stopcol == $i."_".$idx) {
						$currstop = $matches[$idx];
						$currstop =  preg_replace('/[\.,]/', '', $currstop);
						$fieldline .= 'stop,';
						$valueline .= "'$currstop',";

					}
					elseif ($startcol == $i . '_'.$idx) {
						$currstart = $matches[$idx];
						$currstart = preg_replace('/[\.,]/', '', $currstart);
						$fieldline .= 'start,';
						$valueline .= "'$currstart',";

					}
					elseif ($cncol == $i . "_" . $idx) {
						$currcn = $matches[$idx];
						$fieldline .= 'cn,';
						$valueline .= "'$currcn',";
					}
					elseif ($samplecol == $i . "_" . $idx) {
						$currsample = $matches[$idx];
						$fieldline .= 'sample,';
						$valueline .= "'".$renamed[$currsample]."',";
					}
					else {
						if (preg_match("/Platform/i",$keepdefpieces[$counter])) {
							$platform = $matches[$idx];
						}
						elseif (preg_match("/Chiptype/i",$keepdefpieces[$counter])) {
							$chiptype = $matches[$idx];
						}
						$fieldline .= $keepdefpieces[$counter] . ",";
						$valueline .= "'".$matches[$idx] . "',";
					}
				}
			}
		}
		if ($currstart == '' || $currstop == '' || $currcn == '' || $currsample == '' || $currchr == '') {
			$errorcount++;
			echo "<tr>\n";
			echo "<td $firstcell><input type=text name='$errorcount"."_sample' size=10 value='".$renamed[$currsample]."'></td>\n";
			echo "<td><input type=text name='$errorcount"."_chr' size=2 value='$currchr'></td>\n";
			echo "<td><input type=text name='$errorcount"."_start' size=10 value='$currstart'></td>\n";
			echo "<td><input type=text name='$errorcount"."_stop' size=10 value='$currstop'></td>\n";
			echo "<td><input type=text name='$errorcount"."_cn' size=4 value='$currcn'></td>\n";
			if ($platform != '') {
				echo "<input type=hidden name='$errorcount"."_platform' value='$platform'>\n";
			}
			if ($chiptype != '') {
				echo "<input type=hidden name='$errorcount"."_chiptype' value='$chiptype'>\n";
			}
			$errorgenders[$renamed[$currsample]] = $genders[$renamed[$currsample]];
			$errorclinics[$renamed[$currsample]] = $clinics[$renamed[$currsample]];
			echo "<td><pre>$entry</pre></td>\n";
			echo "</tr>\n";
			
		}
		else {
			// check for platform
			if ($platform != '') {
				mysql_query("UPDATE cus_sample SET platform = '$platform' WHERE idsamp = '".$renamed[$currsample]."' AND idproj = '$pid'");
			}
			if ($chiptype != '') {
				mysql_query("UPDATE cus_sample SET chiptype = '$chiptype' WHERE idsamp = '".$renamed[$currsample]."' AND idproj = '$pid'");
			}		
						
			if (!preg_match("/Size/i",$fieldline)) {
				$size = $currstop - $currstart +1;
				$fieldline .= "size,";
				$valueline .= "'$size',";
			}
			
			// add sample info
			$fieldline = $fieldline . " idproj,";
			$valueline = $valueline . "'$pid',";
			// get nr of genes
			$query = mysql_query("SELECT Count(DISTINCT symbol) as Aantal FROM genes WHERE chr='$currchr' AND (((start BETWEEN $currstart AND $currstop) OR (end BETWEEN $currstart AND $currstop)) OR (start <= $currstart AND end >= $currstop))");
			$row = mysql_fetch_array($query);
			$nrgenes = $row['Aantal'];
			$fieldline .= "nrgenes";
			$valueline .= "'$nrgenes'";
			// submit
			$query = mysql_query("INSERT INTO cus_aberration ($fieldline) VALUES ($valueline)");
			$goodcount++;
		} 
	
	}
	echo "</table></p>\n";
	if ($errorcount == 0) {
		echo "<p>All entries were valid and successfully stored. You're done.</p>\n";
	}
	else {
		echo "<p>Please provide the missing information above and press continue. Entries that are still invalid will be discarded !</p>\n";
		echo "<input type=hidden name='errorcount' value='$errorcount'>\n";
		echo "<p><input type=submit class=button value=Continue name=submit></p>\n";
		foreach ($errorgenders as $sample => $gender) {
			echo "<input type=hidden name='".$renamed[$sample]."_gender' value='$gender'>\n";
			$clinic = $errorclinics[$renamed[$sample]];
			echo "<input type=hidden name='".$renamed[$sample]."_clinic' value='$clinic'>\n";
		}
	}
	if ($goodcount == 0) {
		echo "<input type=hidden name=pid value=0>\n";
		echo "<input type=hidden name=projectname value='$projectname'>\n";
		echo "<input type=hidden name=sampletype value='$collection'>\n";
		
		$query = mysql_query("DELETE FROM cus_project WHERE id = '$pid'");
		$query = mysql_query("DELETE FROM cus_projectpermission WHERE projectid = '$pid'");
	}
	else {
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=goodcount value='$goodcount'>\n";
	}
	echo "<input type=hidden name=defplatform value='$defplatform'>\n";
	echo "<input type=hidden name=defchip value='$defchip'>\n";
	echo "</form>\n";
	echo "</div>\n";
	
}


elseif ($_POST['step'] == 5) {
	
	echo "<div class=sectie>\n";
	echo "<h3>5/ Storing additional entries</h3>\n";
	// GET POSTED INFO
	$parserid = $_POST['parser'];
	$filename = $_POST['filename'];
	$pid = $_POST['pid'];
	$goodcount = $_POST['goodcount'];
	$errorcount = $_POST['errorcount'];
	$defplatform = $_POST['defplatform'];
	$defchip = $_POST['defchip'];
	echo "going to analyse entries";
	// create project if needed
	if ($pid == 0) {
		$projectname = $_POST['projectname'];
		if ($projectname == '') {
			$projectname = 	date('Y-m-d') ."_".date('H\ui\ms\s');
		}
		$created = date('Y-m-d') ." - ".date('H\ui\ms\s');
		$collection = $_POST['sampletype'];
		// CREATE project
		$query = mysql_query("INSERT INTO cus_project (naam, created, collection, userID) VALUES ('$projectname','$created','$collection','$userid')");
		$pid = mysql_insert_id();
		$query = mysql_query("INSERT INTO cus_projectpermission (projectid, userid,editcnv,editclinic,editsample) VALUES('$pid','$userid','1','1','1')");
	}
	
	// START PROCESSING REMEANING ABERRATIONS
	$discardcount = 0;
	for ($i = 1; $i <= $errorcount; $i++) {
		$sample = $_POST[$i.'_sample'];
		$chrtxt = $_POST[$i.'_chr'];
		$chr = $chromhash[$chrtxt];
		if ($chr == 'X') {
			$chr = 23;
		}
		if ($chr == 'Y') {
			$chr = 24;
		}
		$start = $_POST[$i.'_start'];
		$start =  preg_replace('/[\.,]/', '', $start);
		$stop = $_POST[$i.'_stop'];
		$stop =  preg_replace('/[\.,]/', '', $stop);
		$cn = $_POST[$i.'_cn'];
		
		$platform = $_POST[$i.'_platform'] || $defplatform || '';
		$chiptype = $_POST[$i.'_chiptype'] || $defchip || '';
		$gender = $_POST[$sample."_gender"];
		$clinic = $_POST[$sample."_clinic"];
		if ($start == '' || $stop == '' || $cn == '' || $sample == '' || $chrtxt == '') {
			$discardcount++;
		}
		else {
			$goodcount++;
			$size = $stop - $start + 1;
			// get nr of genes
			$query = mysql_query("SELECT Count(DISTINCT symbol) as Aantal FROM genes WHERE chr='$chr' AND (((start BETWEEN $start AND $stop) OR (end BETWEEN $start AND $stop)) OR (start <= $start AND end >= $stop))");
			$row = mysql_fetch_array($query);
			$nrgenes = $row['Aantal'];
			mysql_query("INSERT INTO cus_aberration (sample, chr, start, stop, cn, platform, chiptype, idproj, sgender, sdescr, size,nrgenes) VALUES ('$sample','$chr','$start','$stop','$cn','$platform','$chiptype','$pid','$gender',ENCODE('".addslashes($clinic)."','$encpass'),'$size','$nrgenes')");
		}
	}
	echo "<p>Submitting is complete. $goodcount entries were stored and $discardcount entries were discared in total.</p>\n";
	echo "</div>\n";
		
}
?>
<div id="txtHint">something must appear here</div>
<!-- context menu loading -->
<!-- <script type="text/javascript" src="javascripts/custom_context.js"></script>-->
<script type='text/javascript'>
function ToggleFields() {
	var myselect = document.getElementById('addto');
	if (myselect.options[myselect.selectedIndex].value == 'new') {
		document.getElementById('newproject').style.display='';
		document.getElementById('existingproject').style.display='none';
		document.getElementById('collection').style.display='';
		
		
	}
	else {
		document.getElementById('newproject').style.display='none';
		document.getElementById('existingproject').style.display='';
		document.getElementById('collection').style.display='none';

	}
}
</script>
	
