<?php 
####################
# LAYOUT VARIABLES #
####################
$collection=$_GET['collection']; 
$projectid = $_GET['project'];
$sampleid = $_GET['sample'];
$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";

#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


##############################
# GET the COLLECTION options #
##############################
$collections = array('Control', 'Patient');
###########################
# GET the PROJECT options #
###########################
if(isset($collection) and strlen($collection) > 0){
	$projectquery=mysql_query("SELECT p.naam, p.id, p.userID FROM cus_project p JOIN cus_projectpermission pp ON p.id = pp.projectid WHERE p.collection='$collection' AND pp.userid = '$userid' order by naam");
}
else{
	$projectquery=mysql_query("SELECT p.naam, p.id, p.userID FROM cus_project p JOIN cus_projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' order by naam");

}

##########################
# GET the SAMPLE options #
##########################
if (isset($collection) and strlen($collection) > 0){
	if (isset($projectid) and is_numeric($projectid)) {
		$samplequery = mysql_query("SELECT a.sample FROM cus_aberration a JOIN cus_project pr JOIN cus_projectpermission pp ON pr.id = pp.projectid AND a.idproj = pr.id where pr.collection = '$collection' AND pr.id = '$projectid' AND pp.userid = '$userid' GROUP BY a.sample"); 
	}
	else {
		$samplequery = mysql_query("SELECT  a.sample FROM cus_aberration a JOIN cus_project pr JOIN cus_projectpermission pp ON pr.id = pp.projectid AND a.idproj = pr.id where pr.collection = '$collection' AND pp.userid = '$userid' GROUP BY a.sample");
	} 
}
else {
	if (isset($projectid) and is_numeric($projectid)) {
		$samplequery = mysql_query("SELECT  a.sample FROM cus_aberration a JOIN cus_project pr JOIN cus_projectpermission pp ON pr.id = pp.projectid AND a.idproj = pr.id WHERE pr.id = '$projectid' AND pp.userid = '$userid' GROUP BY a.sample");  
	}
	else {
		$samplequery = mysql_query("SELECT a.sample FROM cus_aberration a JOIN project pr JOIN projectpermission pp ON pr.id = pp.projectid AND a.idproj = pr.id WHERE pp.userid = '$userid' GROUP BY sa.chip_dnanr GROUP BY a.sample");

	} 

}
#########################
# PRINT SELECTION TABLE #
#########################
echo "<div class=sectie>\n";
echo "<h3>Browse Custom Data Projects</h3>\n";
echo "<h4>... by narrowing down the criteria</h4>";
echo "<p>Pick your collection, project and sample below. After setting anything, the subchoices will be narrowed down.  When you have chosen your sample, click submit to go tho the details page. </p>\n";
echo "<form action=index.php?page=cusdetails method=POST>\n";
echo "<table  class=clear >\n";
echo " <tr>\n";
echo "  <th class=clear>Select a Collection:</td>\n";
echo "  <td class=clear><select name='collection' onchange=\"reloadcustom(this.form)\"><option value=''>Select one</option>";
foreach($collections as $colname) {
	if($colname==@$collection){
		echo "<option selected value='".$colname."'>".$colname."</option>";
	}
	else{
		echo "<option value='".$colname."'>".$colname."</option>";
	}
}
echo "</select></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=clear> &nbsp; &nbsp; => Select a project:</td>\n";
echo "  <td class=clear><select name='project' onchange=\"reloadcustom(this.form)\"><option value=''>Select one</option>";

while($projectrow = mysql_fetch_array($projectquery)) {
	if($projectrow['id']==@$projectid){
		$powner = $projectrow['userID'];
		echo "<option selected value='".$projectrow['id']."'>".$projectrow['naam']."</option>";
	}
	else{
		echo "<option value='".$projectrow['id']."'>".$projectrow['naam']."</option>";
	}
}
echo "</select></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th class=clear> &nbsp; &nbsp; &nbsp; &nbsp; => Select a sample:</td>\n";
echo "  <td class=clear><select name='sample' onchange=\"reloadcustom(this.form)\"><option value=''>Select one</option>";

while($samplerow = mysql_fetch_array($samplequery)) {
	if($samplerow['sample']==@$sampleid){
		echo "<option selected value='".$samplerow['sample']."'>".$samplerow['sample']."</option>";
	}
	else{
		echo "<option value='".$samplerow['sample']."'>".$samplerow['sample']."</option>";
	}
}
echo "</select></td>\n";
echo " </tr>\n";
echo " <tr><td class=clear><input type=submit class=button name=submit value='Show Sample Details'></form></td></tr>";

if ($projectid != "") {
	echo "<tr><td class=clear><form action='index.php?page=overviewbrowse&amp;p=$projectid&amp;cstm=1' method=POST><input type=submit class=button value='Show Project Overview'></form></td><td class=clear><div class=nadruk></div>\n"; 
	if ($powner == $userid) {
		echo "<tr><td class=clear><form action='index.php?page=customshareprojects' method=POST target='_blank'><input type=hidden name=pid value='$projectid'><input type=submit class=button value='Share This Project'></form></td><td class=clear>\n";
	}
}
echo "</tr>";
echo "</table>\n";
echo "</form>\n";
echo "</div>\n";

##########################
# SHOW overview by table #
##########################
if (isset($sampleid) && strlen($sampleid) > 0) {
	echo "<div class=sectie>\n";
	# Print information table on the selected sample
	echo "<h3>$samplename is available in the following projects</h3>\n";
	echo "<h4>... you have access to </h4>\n";
	echo "<p>You can view details of the projects listed below by clicking on the row. For details on the sample, use the submit button above.</p>\n";
	// need: created, projectname, chiptype, platform, collection, projectid
	$projidquery = mysql_query("SELECT pr.created, pr.naam, pr.id, pr.collection FROM cus_project pr JOIN cus_aberration a ON a.idproj = pr.id WHERE a.sample = '$sampleid' GROUP BY a.sample ORDER BY pr.id");
	$created = date('Y-m-d') ." - ".date('H\ui\ms\s');
	#echo "NOW: $created<br/>\n";
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th $firstcell >project</td><th>Created</td><th>Collection</td><th>Platform</td><th>Chiptype</td><th># Aberrations</td></tr>\n";	
	while($result = mysql_fetch_array($projidquery)) {
		$projid = $result['id'];
		$coll = $result['collection'];
		$projname = $result['naam'];
		$created = $result['created'];
		$genderquery = mysql_query("SELECT gender, chiptype, platform FROM cus_sample WHERE idsamp = '$sampleid' AND idproj = '$projid'");
		$grow = mysql_fetch_array($genderquery);
		$gender = $grow['gender'];
		$chiptype = $grow['chiptype'];
		$platform = $grow['platform'];
		if ($gender == '') {
			$gender = 'Not Specified';
		}

		#$platform = $result['platform'];
		if ($platform == '') {
			$platform = 'Not Specified';
		}
		#$chiptype = $result['chiptype'];
		if ($chiptype == '') {
			$chiptype = 'Not Specified';
		}
		$countquery = mysql_query("SELECT COUNT(id) AS number FROM cus_aberration WHERE sample = '$sampleid' AND idproj = $projid");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
				$url = "index.php?page=custom_methods&amp;type=tree&amp;collection=$collection&amp;project=$projid&amp;sample="; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";	
		echo " <tr $trstyle>\n";
		echo "  <td $firstcell >$projname</td>\n";
		echo "  <td>$created</td>\n";
		echo "  <td>$coll</td>\n";
		echo "  <td>$platform</td>\n";
		echo "  <td>$chiptype</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	
	}
	echo "</table>\n";
	echo "</div>\n";	
}		
elseif (isset($projectid) and is_numeric($projectid)){
	echo "<div class=sectie>\n";
	# Print information table on the selected project 
	$details = mysql_query("SELECT naam, created, collection FROM cus_project WHERE id = $projectid");
	$detrow = mysql_fetch_array($details);
	$projname = $detrow['naam'];
	echo "<h3>Samples available in project '$projname'</h3>\n";
	echo "<h4>... and some project details</h4>\n";
	echo "<p>Collection: ". $detrow['collection']. "<br>Created: ".$detrow['created']. "<br></p>\n";
	echo "<p>\n";
	#$samplegender = $detrow['gender'];
	$samplequery = mysql_query("SELECT COUNT(id) as AANTAL, a.sample FROM cus_aberration a WHERE a.idproj = $projectid GROUP BY a.sample");
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th $firstcell >Sample</td><th>Gender</td><th>Platform</td><th>Chiptype</td><th># Aberrations</td></tr>\n";	
	while($result = mysql_fetch_array($samplequery)) {
		$number = $result['AANTAL'];
	 	$sample = $result['sample'];	
		$genderq = mysql_query("SELECT gender , platform, chiptype FROM cus_sample WHERE idsamp = '$sample' AND idproj = '$projectid'");
		$grow = mysql_fetch_array($genderq);
		$gender = $grow['gender'];
		$chip = $grow['chiptype'];
		$platform = $grow['platform'];
		if ($gender == '') {
			$gender = 'Not Specified';
		}
		#$platform = $result['platform'];
		if ($platform == '') {
			$platform = 'Not Specified';
		}
		#$chip = $result['chiptype'];
		if ($chip == '') {
			$chip = 'Not Specified';
		}
		#$countquery = mysql_query("SELECT COUNT(id) AS number FROM aberration WHERE sample = $sid AND idproj = $projectid");
		#$numberres = mysql_fetch_array($countquery);
		#$number = $numberres['number'];
		$url = "index.php?page=custom_methods&amp;type=tree&amp;collection=$collection&amp;project=$projectid&amp;sample=$sample"; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr $trstyle>\n";
		echo "  <td $firstcell >$sample</td>\n";
		echo "  <td>$gender</td>\n";
		echo "  <td>$platform</td>\n";
		echo "  <td>$chip</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	
	}
	echo "</table>\n";
	echo "</div>\n";	
}
elseif (isset($collection) and strlen($collection)>0){
	echo "<div class=sectie>\n";
	# Print information table on the selected collection
 	$details = mysql_query("SELECT p.id, p.naam, p.created FROM cus_project p JOIN cus_projectpermission pp ON pp.projectid = p.id WHERE p.collection='$collection' AND pp.userid = '$userid' ORDER BY p.id");
	echo "<h3>Projects available in collection '$collection'</h3>\n";
	echo "<h4>... that you have access to</h4>\n";
	echo "<p>\n";
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th $firstcell >Project</td><th>Created</td><th># Samples</td></tr>\n";	
	while($result = mysql_fetch_array($details)) {
		$projname = $result['naam'];
		$pid = $result['id'];
		$created = $result['created'];
		$countquery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS number FROM cus_aberration WHERE idproj = '$pid'");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
		$url = "index.php?page=custom_methods&amp;type=tree&amp;collection=$collection&amp;project=$pid&amp;sample="; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr $trstyle>\n";
		echo "  <td $firstcell>$projname</td>\n";
		echo "  <td>$created</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	
	}
	echo "</table>\n";
	echo "</div>\n";	
}

else {
	echo "<div class=sectie>\n";
	# show 10 most recent custom projects
 	$details = mysql_query("SELECT p.id, p.naam, p.created, p.collection FROM cus_project p JOIN cus_projectpermission pp ON pp.projectid = p.id WHERE pp.userid = '$userid' ORDER BY p.id desc LIMIT 10");
	echo "<h3>Ten Most Recent Projects</h3>\n";
	echo "<h4>... you have access to</h4>\n";
	echo "<p>\n";
	echo "<table  cellspacing = 0 width=100%>\n";
	echo "<tr>\n";
	echo "<th $firstcell >Project</td><th>Created</td><th>Collection</td><th># Samples</td></tr>\n";	

	while($result = mysql_fetch_array($details)) {
		$projname = $result['naam'];
		$pid = $result['id'];
		$created = $result['created'];
		$coll = $result['collection'];
		$countquery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS number FROM cus_aberration WHERE idproj = '$pid'");
		$numberres = mysql_fetch_array($countquery);
		$number = $numberres['number'];
		$url = "index.php?page=custom_methods&amp;type=tree&amp;collection=$collection&amp;project=$pid&amp;sample="; 
		$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr $trstyle>\n";
		echo "  <td $firstcell >$projname</td>\n";
		echo "  <td>$created</td>\n";
		echo "  <td>$coll</td>\n";
		echo "  <td>$number</td>\n";
		echo " </tr>\n";
	}
	echo "</table>\n";
	echo "</div>\n"	;	


}

?>
