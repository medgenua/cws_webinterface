<?php

// A whole genome prioritization is used for all models except expression.

if (isset($_POST['training'])) {
	$trainingset = $_POST['training'];
}
else {
	$trainingset = 'Mental Retardation';
}

// DECLARE VARIABLES
$checkbox = array("","checked");
$models = array("Annotation_EnsemblEst", "Annotation_GeneOntology", "Annotation_Interpro", "Annotation_Kegg", "Annotation_Swissprot", "Interaction_Bind", "Interaction_BioGrid", "Interaction_Hprd", "Interaction_InNetDb", "Interaction_Intact", "Interaction_Mint", "Interaction_String", "Precalculated_Ouzounis", "Precalculated_Prospectr", "CisRegModule", "Motif", "Blast", "Text");
$descr[0] = "EST are Expression Sequence Tags which allow the genes to be associated with the locations in the body where they are found to be expressed. The locations are organized in a tree structure.";
$descr[1] = "The GO project has developed three structured controlled vocabularies (ontologies) that describe gene products in terms of their associated biological processes,<br/> cellular components and molecular functions in a species-independent manner.";
$descr[2] = "InterPro is a database of protein families, domains and functional sites in which identifiable features found in known proteins can be applied to unknown protein sequences.";
$descr[3] = "KEGG is a database of biological systems, consisting of genetic building blocks of genes and proteins, molecular wiring diagrams of interaction and reaction networks (KEGG PATHWAY). KEGG provides a reference knowledge base for linking genomes to biological systems and also to environments by the processes of PATHWAY mapping.";
$descr[4] = "UniProtKB/Swiss-Prot is a curated protein sequence database which strives to provide a high level of annotation (such as the description of the function of a protein, its domains structure, post-translational modifications, variants, etc.), a minimal level of redundancy and high level of integration with other databases.";
$descr[5] = "The Biomolecular Interaction Network Database (BIND) is a collection of records documenting molecular interactions. The contents of BIND include high-throughput data submissions and hand-curated information gathered from the scientific literature.";
$descr[6] = "BioGRID is a freely accessible database of protein and genetic interactions. BioGRID release version 2.0 includes more than 116,000 interactions from Saccharomyces cerevisiae, Caenorhabditis elegans, Drosophila melanogaster and Homo sapiens.";
$descr[7] = "The Human Protein Reference Database represents a centralized platform to visually depict and integrate information pertaining to domain architecture, post-translational modifications, interaction networks and disease association for each protein in the human proteome. All the information in HPRD has been manually extracted from the literature by expert biologists who read, interpret and analyze the published data. HPRD has been created using an object oriented database in Zope, an open source web application server, that provides versatility in query functions and allows data to be displayed dynamically.";
$descr[8] = "IntNetDB is a database of human protein-protein interactions (PPIs) and their worm, fruitfly and mouse interologs computationally predicted through Bayesian analysis by integrating large-scale physical protein-protein interactions, protein domain assignments, gene co-expression data, genetic interactions, gene context information and biological function annotations from human and other model organisms.";
$descr[9] = "IntAct provides a freely available, open source database system and analysis tools for protein interaction data. All interactions are derived from literature curation or direct user submissions.";
$descr[10] = "MINT focuses on experimentally verified protein interactions mined from the scientific literature by expert curators. The curated data can be analyzed in the context of the high throughput data.";
$descr[11] = "STRING is a database of known and predicted protein-protein interactions.The interactions include direct (physical) and indirect (functional) associations; they are derived from four sources: Genomic Context, High-throughput Experiments, (Conserved) Coexpression,  Previous Knowledge.";
$descr[12] = "Sequence analysis of the group of proteins known to be associated with hereditary diseases allows the detection of key distinctive features shared within this group.This unique property pattern can be used to predict novel disease genes.";
$descr[13] = "Prospectr (PRiOrization by Sequence &amp; Phylogenetic Extent of CandidaTe Regions) is an alternating decision tree which has been trained to differentiate between genes likely to be involved in disease and genes unlikely to be involved in disease. By using sequence-based features like gene length, protein length and the percent identity of homologs in other species as input a classification can be obtained for a gene of interest.";
$descr[14] = "This submodel finds the best combination of five motifs that could coregulate the training genes.";
$descr[15] = "Putative transcription factors binding sites are given a score corresponding to the probability of this motif to be functionnal.";
$descr[16] = "BLAST (Basic Local Alignment Search Tool) is a sequence comparison algorithm optimized for speed used to search sequence databases for optimal local alignments to a query.";
$descr[17] = "The abstracts of publications are mined in order to find relevant keywords. Each keyword is given a score which represents its over-representation.";
	

//$userquery = mysql_query("SELECT endeavour FROM users where id = '$userid'");
//$userarray = mysql_fetch_array($userquery);
//$defaultmodel = $userarray[ 'endeavour' ];
$defaultmodel = "11111111111100111111";
//$priorquery = mysql_query("SELECT id FROM prioritize WHERE sample = '$sample' AND project = '$project'");	
//$nrrows = mysql_num_rows($priorquery);
// There are no previous runs, or a new one has to be setup

echo "<p><form action=index.php?page=details#list method=POST> \n";
echo "<input type=hidden name=project value=$project>\n";
echo "<input type=hidden name=sample value=$sample>\n";
echo "<input type=hidden name=sort value='rank'>\n";

echo "<table cellspacing=0\n";
echo "<tr>\n";
echo "  <th $firstcell class=topcell>Training set</th>\n";
echo "	<td colspan=2 style=\"border-top: 1px solid #a1a6a4;\"><select name=training style=\"width:100%\">";
if ($trainingset == 'Mental Retardation') {
	echo "<option value=\"Mental Retardation\" selected>Mental Retardation</option><option value=\"Epilepsy\">Epilepsy</option></select></td>\n";
}
elseif ($trainingset == 'Epilepsy') {
	echo "<option value=\"Mental Retardation\">Mental Retardation</option><option value=\"Epilepsy\" selected>Epilepsy</option></select></td>\n";
}
echo "  <td style=\"border-top: 1px solid #a1a6a4;\"> <input type=submit class=button value=\"Prioritize!\" name=prioritize></td>\n";
echo " </tr>\n";
echo " <tr>\n";
$switch = 1;
echo "  <th $firstcell $thtype[1] rowspan=10>Used<br>Prioritization<br>models</th>\n";
for ($i=0;$i<=17;$i+=3) {
	$vink = substr($defaultmodel,$i,1);
	echo "  <td $tdtype[$switch] title=\"$descr[$i]\">$models[$i]</td>\n";
	$j = $i+1;
	if ($j <= 17) {
		$vink = substr($defaultmodel,$j,1);
		echo "  <td $tdtype[$switch] title=\"$descr[$j]\">$models[$j]</td>\n";
	}
	else {
		echo "  <td $tdtype[$switch]></td>\n";
	}
	$j = $j+1;
	if ($j <= 17) {
		$vink = substr($defaultmodel,$j,1);
		echo "  <td $tdtype[$switch] title=\"$descr[$j]\">$models[$j]</td>\n";
	}
	else {
		echo "  <td $tdtype[$switch]></td>\n";
	}
	echo " </tr>\n";
	echo " <tr>\n";
	$switch =  $switch + pow(-1,$switch);
}
echo "</table>\n";
echo "</form>\n";
echo "</p>\n";
echo "<p>\n";
echo "NOTE: prioritization is based on a whole genome prioritization using the java-interface of endeavour. Only genes with a valid ensemble id were included !</p>";
	
	
?>	
