<?php

$priorquery = mysql_query("SELECT id FROM prioritize WHERE sample = '$sample' AND project = '$project' AND model = '$defaultmodel' AND trainingset = '$trainingset'");	
	$pnrrows = mysql_num_rows($priorquery);
	if ($pnrrows == 0) {
		// Rank now
		echo "<div class=sectie id='loading'>\n";
		echo "<h3>Prioritizing, please be patient.</h3>\n";
		echo "<center><img src='images/content/ajax-loader.gif'></center>";	
		echo "</div>\n";
		flush();
		ob_flush();
		// GET abs
		$ranks = array();
		$geneab = array();
		$generanks = array();
		$siggenes = array();
		$abq = mysql_query("SELECT id, chr, start, stop FROM aberration WHERE sample = '$sample' AND idproj = '$project'");
		while ($abr = mysql_fetch_array($abq)){
			$aid = $abr['id'];
			$ranks[$aid] = 0;
			$chr = $abr['chr'];
			$start = $abr['start'];
			$stop = $abr['stop'];
			// get genes for each ab
			$gq = mysql_query("SELECT egID, symbol FROM genes WHERE chr = '$chr' AND (( start BETWEEN $start AND $stop) OR ( end BETWEEN $start AND $stop) OR (start < $start AND end > $stop))");
			while ($gr = mysql_fetch_array($gq)) {
				$egID = $gr['egID'];
				$geneab[$egID] = $aid;				
				$symbol = $gr['symbol'];
				// get rank for each gene
				$rquery = "SELECT rank, pval FROM WGP WHERE model = '$defaultmodel' AND trainingset = '$trainingset' AND (ensid = '$egID' OR symbol = '$symbol')  ORDER BY rank ASC LIMIT 1";
				//echo "<br>$rquery<br>";
				$rq = mysql_query($rquery);
				$rr = mysql_fetch_array($rq);
				$rnr = mysql_num_rows($rq);
				
				if ($rnr > 0) {
					$grank = $rr['rank'];
					$gpval = $rr['pval'];
					$generanks[$egID] = $grank;
					//echo "gene: $egID = $symbol (rank: $generanks[$egID])<br>";
					$roundpval = number_format($gpval,3,'.','');
					$siggenes[$egID] = "$symbol ($roundpval)";
				}
			}		

		}
		asort($generanks);
		$sigfound = 0;
		$rank = 1;
		// order CNVs based on generanks
		foreach($generanks as $key => $value) {
			$ab = $geneab[$key];
			if ($ranks[$ab] == 0) {
				$ranks[$ab] = $rank;
				$rank++;
			}
			$sigstrings[$ab] .= $siggenes[$key] . ",";
			$sigfound++;	
		}
		// set remaining cnvs to same rank
		foreach ($ranks as $key => $value) {
			if ($ranks[$key] == 0) {
				$ranks[$key] = $rank;
			}
		}	
		// insert into db
		if ($sigfound > 0) {
			$sf = 1;
		}
		else {
			$sf = -1;
		}
		
		mysql_query("INSERT INTO prioritize (sample, project, model, trainingset, sigfound) VALUES ('$sample', '$project', '$defaultmodel', '$trainingset', '$sf')");
		$prid = mysql_insert_id();
		
		foreach ($ranks as $key => $value) {
			$ranking = $ranks[$key];
			$sgenes = $sigstrings[$key];
			$abid = $key;
			mysql_query("INSERT INTO priorabs (prid, aid, rank, signGenes) VALUES ('$prid', '$abid', '$ranking', '$sgenes')");
		}
		$querystring = "SELECT a.id, a.cn, a.start, a.stop, a.chr, a.seenby, a.nrsnps, a.class, a.inheritance, a.nrgenes, a.seeninchip, a.seenall, a.seenincontrols, p.rank, p.signGenes, p.prid,a.pubmed FROM aberration a LEFT JOIN priorabs p ON a.id = p.aid WHERE sample='$sample' AND idproj='$project' AND p.prid = $prid ORDER BY $sortoptions[$sort]";
		echo "<script type='text/javascript'>";
		echo "	document.getElementById('loading').style.display = 'none';";
		echo " </script>"; 
  	}
	else {
		$pridrow = mysql_fetch_array($priorquery);
		$prid = $pridrow['id'];
		$querystring = "SELECT a.id, a.cn, a.start, a.stop, a.chr, a.seenby, a.nrsnps, a.class, a.inheritance, a.nrgenes, a.seeninchip, a.seenall, a.seenincontrols, p.rank, p.signGenes, p.prid,a.pubmed,a.validation,a.validationdetails FROM aberration a LEFT JOIN priorabs p ON a.id = p.aid WHERE sample='$sample' AND idproj='$project' AND p.prid = $prid ORDER BY $sortoptions[$sort]";
	}	
?>
