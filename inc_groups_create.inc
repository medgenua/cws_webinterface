<?php
// for syntax coloring
?>
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/groups.js"></script>

<?php

$step = $_GET['step'];
if ($step == '' || $step == '1') {
	echo "<div class=sectie>\n";
	echo "<h3>Create a New Usergroup</h3>\n";
	echo "<p>Usergroups are the most convenient way to share projects 
			with multiple users at once. Usergroups can exist for
			example of lab technicians, or clinicians associated to 
			a specific Institute. When a group is set to be open to 
			all users, this allows for example sharing of large control cohorts. </p>";
	echo "<p>You will be assigned as Group Administrator, meaning that you will
			recieve emails for confirmation of new user assignments to the group.
			For open groups this confirmation can be disabled.</p>";
	echo "<p>Specify the group details below and press submit. Please use clear description !</p>";
	echo "<form action='index.php?page=group&amp;type=create&amp;step=2' method=POST>\n";
	echo "<p><table>\n";
	echo "<tr>\n";
		echo "<th class=clear title='e.g. CMG Antwerp Clinicians'>Group Name</th>\n";
	echo "<td class=clear><input type=text name='groupname' size=50></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<th class=clear valign=top>Group Description</th>\n";
	echo "<td class=clear><TEXTAREA NAME='description' COLS=48 ROWS=2></TEXTAREA></td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<th class=clear title='If selected, all users can join this usergroup'>Open Group</th>\n";
	echo "<td class=clear>	<input type='radio' name='opengroup' value=0 checked onclick=\"document.getElementById('SetAffi').style.display='';document.getElementById('SetConfirm').style.display='none';\"> No
				<input type='radio' name='opengroup' value=1 onclick=\"document.getElementById('SetAffi').style.display='none';document.getElementById('SetConfirm').style.display='';\"> Yes</td>\n";
	echo "</tr>\n";
	echo "<tr id='SetConfirm' style='display:none;' title='Is confirmation needed when a user joins an open group?'>\n";
	echo "<th class=clear>Confirmation needed</th>\n";
	echo "<td class=clear>	<input type='radio' name='confirmation' value=0> No 
				<input type='radio' name='confirmation' value=1 checked> Yes </td>\n";
	echo "</tr>\n";
	echo "<tr id='SetAffi'>\n";
	echo "<th class=clear valign=top>Allowed Institutions</th>\n";
	echo "<td class=clear><select name='Affis[]' size=4 MULTIPLE>";
	// get own affiliation
	$query = mysql_query("SELECT a.name, a.id FROM affiliation a JOIN users u ON u.Affiliation = a.id WHERE u.id = '$userid' LIMIT 1");
	$row = mysql_fetch_array($query);
	$ownaffi = $row['name'];
	$ownid = $row['id'];
	echo "<option value='$ownid'>$ownaffi</option>\n";
	// get others, order alfabetically
	$query = mysql_query("SELECT a.name, a.id FROM affiliation a WHERE NOT a.id = '$ownid' ORDER BY a.name");
	while ($row = mysql_fetch_array($query)) {
		$currname = $row['name'];
		$currid = $row['id'];
		echo "<option value='$currid'>$currname</option>\n";
	}
	echo "</SELECT>\n";
	echo "</td>\n";
	echo "</tr>";
	echo "<tr>\n";
	echo "<th class=clear title='Will users from this group be able to classify, delete or modify CNV calls?'>Change CNVs</th>\n";
	echo "<td class=clear><input type=radio name=editcnv value=0 checked> No <input type=radio name=editcnv value=1> Yes</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<th class=clear title='Will users from this group be able to add or edit clinical information?'>Change Clinical Information</th>\n";
	echo "<td class=clear><input type=radio name=editclinic value=0 > No <input type=radio name=editclinic value=1 checked> Yes</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<th class=clear title='Will users from this group be able to manage projects (share, join, delete)?'>Manage Projects</th>\n";
	echo "<td class=clear><input type=radio name=editsample value=0 checked> No <input type=radio name=editsample value=1 > Yes</td>\n";
	echo "</tr>\n";

	echo "</table>\n";
	echo "</p><p>";
	echo "<input type=submit class=button value='Create'></form>\n";
}
// STEP 2 : SETUP THE NEW GROUP
elseif ($step == '2' ) {
	$groupname = $_POST['groupname'];
	$description = $_POST['description'];
	$opengroup = $_POST['opengroup'];
	$confirmation = $_POST['confirmation'];
	$Affis = $_POST['Affis'];
	$editcnv = $_POST['editcnv'];
	$editclinic = $_POST['editclinic'];
	$editsample = $_POST['editsample'];
	$affihash = array();
	if ($Affis) {
		foreach($Affis as $affid) {
			$affihash["$affid"] = 1;
		}
	}
	#else {
	#	echo "no affids selected<br>";
	#}
	$problem = 0;
	echo "<div class=sectie>\n";
	echo "<h3>Create a New Usergroup</h3>\n";
	if ($groupname == '' || $description == '' ) {
		echo "<p><span class=nadruk>Problem:</span> Group name and description are mandatory. Please complete the details.</p>\n";
		$problem= 1;
	}
	if ($opengroup == 0 && !$Affis) {
		echo "<p><span class=nadruk>Problem:</span> You need to assing at least one institution to a non-public group. Please complete the details.</p>\n";
		$problem = 1;
	}
	if ($problem == 1){
		// insufficient information, re-present the form.
		echo "<form action='index.php?page=group&amp;type=create&amp;step=2' method=POST>\n";
		$checked = array('','checked');
		$checkedzero = array('checked','');
		echo "<p><table>\n";
		echo "<tr>\n";
		echo "<th class=clear title='e.g. CMG Antwerp Clinicians'>Group Name</th>\n";
		echo "<td class=clear><input type=text name='groupname' size=50 value='$groupname'></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=clear valign=top>Group Description</th>\n";
		echo "<td class=clear><TEXTAREA NAME='description' COLS=48 ROWS=2>$description</TEXTAREA></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<tr>\n";
		echo "<th class=clear title='If selected, all users can join this usergroup'>Open Group</th>\n";
		echo "<td class=clear>	<input type='radio' name='opengroup' value=0 $checkedzero[$opengroup] onclick=\"document.getElementById('SetAffi').style.display='';document.getElementById('SetConfirm').style.display='none';\"> No";
		echo " <input type='radio' name='opengroup' value=1 $checked[$opengroup] onclick=\"document.getElementById('SetAffi').style.display='none';document.getElementById('SetConfirm').style.display='';\"> Yes</td>\n";
		echo "</tr>\n";
		echo "<tr id='SetConfirm' style='display:none;' title='Is confirmation needed when a user joins an open group?'>\n";
		echo "<th class=clear>Confirmation needed</th>\n";
		echo "<td class=clear>	<input type='radio' name='confirmation' value=0 $checkedzero[$confirmation]> No ";
		echo "	<input type='radio' name='confirmation' value=1 $checked[$confirmation]> Yes </td>\n";
		echo "</tr>\n";
		echo "<tr id='SetAffi'>\n";
		echo "<th class=clear valign=top>Allowed Institutions</th>\n";
		echo "<td class=clear><select name='Affis[]' size=4 MULTIPLE>";
		// get own affiliation
		$query = mysql_query("SELECT a.name, a.id FROM affiliation a JOIN users u ON u.Affiliation = a.id WHERE u.id = '$userid' LIMIT 1");
		$row = mysql_fetch_array($query);
		$ownaffi = $row['name'];
		$ownid = $row['id'];
		if ($affihash[$ownid] == 1) {
			$selected = 'SELECTED';
		}
		else {
			$selected = '';
		}
		echo "<option value='$ownid' $selected>$ownaffi</option>\n";
		// get others, order alfabetically
		$query = mysql_query("SELECT a.name, a.id FROM affiliation a WHERE NOT a.id = '$ownid' ORDER BY a.name");
		while ($row = mysql_fetch_array($query)) {
			$currname = $row['name'];
			$currid = $row['id'];
			if ($affihash[$currid] == 1) {
				$selected = 'SELECTED';
			}
			else {
				$selected = '';
			}
			echo "<option value='$currid' $selected>$currname</option>\n";
		}
		echo "</SELECT>\n";
		echo "</td>\n";
		echo "</tr>";
		echo "<tr>\n";
		echo "<th class=clear title='Will users from this group be able to classify, delete or modify CNV calls?'>Change CNVs</th>\n";
		echo "<td class=clear><input type=radio name=editcnv value=0 ".$checkedzero[$editcnv]."> No <input type=radio name=editcnv value=1 ".$checked[$editcnv]."> Yes</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=clear title='Will users from this group be able to add or edit clinical information?'>Change Clinical Information</th>\n";
		echo "<td class=clear><input type=radio name=editclinic value=0 ".$checkedzero[$editclinic]."> No <input type=radio name=editclinic value=1 ".$checked[$editclinic]."> Yes</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=clear title='Will users from this group be able to manage projects (share, join, delete)?'>Manage Projects</th>\n";
		echo "<td class=clear><input type=radio name=editsample value=0 ".$checkedzero[$editsample]."> No <input type=radio name=editsample value=1 ".$checked[$editsample] ."> Yes</td>\n";
		echo "</tr>\n";
		
		echo "</table>\n";
		echo "</p><p>";
		echo "<input type=submit class=button value='Resubmit'></form>\n";
	}
	else {
		if ($opengroup == 1) {
			$query = mysql_query("INSERT INTO usergroups (name, description, opengroup, administrator,confirmation,editcnv, editclinic,editsample) VALUES ('$groupname', '$description', '1','$userid','$confirmation','$editcnv','$editclinic','$editsample')");
			$newgroupid = mysql_insert_id();	
		}
		else {
			// get affistring
			$affistring = '';
			foreach($affihash as $key => $value) {
				$affistring .= "$key,";
			}
			$affistring = substr($affistring,0,-1);
			$query = mysql_query("INSERT INTO usergroups (name, description, affiliation, opengroup, administrator,editcnv,editclinic,editsample) VALUES ('$groupname', '$description', '$affistring', '0','$userid','$editcnv','$editclinic','$editsample')");
			$newgroupid = mysql_insert_id();
		}
		if ($newgroupid != ''){
			// group created, Suggest to add users.
			echo "<h4>Created: $groupname</h4> \n";
			echo "<p>The usergroup '$groupname' was successfully created. You can now add users to the group by selecting them below.</p>\n";
			echo "<form action='index.php?page=group&amp;type=create&amp;step=3' method=POST>\n";
			echo "<input type=hidden name=groupid value='$newgroupid'>\n";
			echo "<p><table>\n";
			echo "<tr>\n";	
			echo "<th class=clear title='Select the users to include in the created group' valign=top>Select Users<br/><input type=radio value=1 name=clear onclick=\"selectAll('userlist',true)\"> All<br/><input type=radio value=0 name=clear onclick=\"selectAll('userlist',false)\"> None</th>\n";
			echo "<td class=clear><select name='addusers[]' id='userlist' size=5 MULTIPLE>\n";
			if ($opengroup == 1) {
				$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY a.name, u.LastName");
 			}
			else {
				$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE a.id IN ($affistring) ORDER BY a.name, u.LastName");
			}
			$currinst = '';
			while($row = mysql_fetch_array($query)) {
				$uid = $row['id'];
				$lastname = $row['LastName'];
				$firstname = $row['FirstName'];
				$institute = $row['name'];
				if ($currinst != $institute) {
					if ($currinst != '') {
						echo "</optgroup>";
					}
					echo "<optgroup label='$institute'>\n";
					$currinst = $institute;
				}
				echo "<option value='$uid'>$lastname, $firstname</option>\n";
			}
			echo "</select></td>\n";
			echo "</tr>\n";
			echo "</table>\n";
			echo "</p><p><input type=submit class=button name=submit value='Add Users'></form>\n";
			#echo "The new groupid = $newgroupid<br>";
		}
		else {
			// something went wrong. give message about it.
			echo "<p><span class=nadruk>Problem:</span> We are sorry but something went wrong.Please go back and remove any special characters from the submitted infromation.</p>\n";
		}
	}
	echo "</div>\n";
	
}
elseif($step == 3) {
	$addusers = $_POST['addusers'];
	$groupid = $_POST['groupid'];
	foreach ($addusers as $idx => $uid) {
		mysql_query("INSERT INTO usergroupuser (gid, uid) VALUES('$groupid','$uid') ON DUPLICATE KEY UPDATE uid = '$uid'");
	}
	echo "<div class=sectie>\n";
	echo "<h3>Create a New Usergroup</h3>\n";
	echo "<p>The users have been successfully added to the group.  You can now share projects with this entire group at once. </p>";
	echo "</div>\n";
	

}
?>
