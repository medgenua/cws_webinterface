<?php
// layout vars
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

//get posted vars
$motivation = $_POST['motivation'];
$joinid = $_POST['joinid'];
// get users institution
$query = mysql_query("SELECT Affiliation FROM users WHERE id = '$userid'");
$row  = mysql_fetch_array($query);
$myaffi = $row['Affiliation'];

if (isset($_POST['submit'])) {
	$joinid = $_POST['joinid'];
}
elseif (isset($_GET['jid'])) {
	$joinid = $_GET['jid'];
}
else {
	$joinid = 'none';
}

if (!isset($_POST['submit'])  || $joinid == 'none') {
	echo "<div class=sectie>\n";
	echo "<h3>Join a Usergroup</h3>";
	echo "<p>Please select a group to join from the list below. You can add a motivation if needed in the textfield. If confirmation is needed, an email will be sent to the usergroup administrator upon submission.</p>\n";
}
else {
	// process request
	$query = mysql_query("SELECT affiliation, confirmation, administrator,name,editcnv, editclinic,opengroup FROM usergroups WHERE id = '$joinid'");
	$row = mysql_fetch_array($query);
	$allowedaffis = $row['affiliation'];
	$confirmation = $row['confirmation'];
	$administrator = $row['administrator'];
	$editcnv = $row['editcnv'];
	$editclinic = $row['editclinic'];
	$name = $row['name'];
	$opengroup = $row['opengroup'];
	$affis = explode(',',$allowedaffis);
	$found = 0;
	if ($opengroup == 1) {
		$found = 1;
	}
	else {
		foreach($affis as $value) {
			if ($value == $myaffi) {
				$found = 1;
				break;
			}
		}
	}
	if ($found == 0) {
		echo "<div class=sectie>\n";
		echo "<h3>Problem detected</h3>\n";
		echo "<p>Somehow, you don't seem to be allowed to join this group. please select another one.</p>\n";
		echo "</div>\n";
	}
	elseif ($confirmation == 0) {
		// join the group.
		$query = mysql_query("INSERT INTO usergroupuser (uid, gid) VALUES ('$userid','$joinid') ON DUPLICATE KEY UPDATE uid = '$userid'");
		// update permissions on projects shared with groups. 
		$query = mysql_query("SELECT projectid FROM projectpermissiongroup WHERE groupid = '$joinid'");
		$projects = array();
		if (mysql_num_rows($query) > 0) {
			while ($row = mysql_fetch_array($query)) {
				$pid = $row['projectid'];
				$projects[$pid] = 1;
				$insquery = mysql_query("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic) VALUES ('$pid','$userid', '$editcnv', '$editclinic') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '$editcnv', VALUES(editcnv),'$editcnv'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic')");

			}
		}
			
		echo "<div class=sectie>\n";
		
		echo "<h3>Request processed</h3>\n";
		echo "<h4>$name...</h4>\n";
		echo "<p>You are now a member of the group '$name'. When new projects are shared with this group, you will gain access to them.</p>\n";
		
		if (count($projects) > 0) {
			echo "<p>In addition, you gained access to the following previously shared Illumina projects: </p>\n";
			echo "<table cellspacing=0>\n";
			echo "<tr>\n";
			echo "<th $firstcell >Name</th><th>Collection</th><th>Chiptype</th><th>Details</th>\n";
			echo "</tr>\n";
			foreach($projects as $pid => $value) {
				$query = mysql_query("SELECT naam, collection, chiptype FROM project WHERE id = '$pid'");
				$row = mysql_fetch_array($query);
				$pname = $row['naam'];
				$pcol = $row['collection'];
				$pchip = $row['chiptype'];
				echo "<tr>\n";
				echo "<td $firstcell>$pname</td>\n";
				echo "<td>$pcol</td>\n";
				echo "<td>$pchip</td>\n";
				echo "<td><a href='index.php?page=results&type=tree&collection=$pcol&project=$pid&sample=' target='_blank'>Details</a></td>\n";
				echo "</tr>\n";
			}
			echo "</table>\n";
		
		}
		
		echo "</div>\n";	
		//	
		
	}	
	else {
		// check if request is still open. 
		$ok = 1;
		$query = mysql_query("SELECT linkkey FROM usergrouprequests WHERE uid = '$userid' AND gid = '$joinid'");
		if (mysql_num_rows($query)>0 && $_POST['resend'] != 1) {
			echo "<div class=sectie>\n";
			echo "<h3>Problem Detected</h3>\n";
			echo "<p>You already requested access to this usergroup. If you want to resend the email to the system admininstrator, click continue.</p>\n";
			echo "<form action='index.php?page=group&type=join' method=POST>\n";
			echo "<input type=hidden name=motivation value='$motivation'>\n";
			echo "<input type=hidden name=joinid value='$joinid'>\n";
			echo "<input type=hidden name=resend value=1>\n";
			echo "<input type=submit class=button value='Resend Request' name=submit>\n";
			echo "</form></p></div>\n";
			$ok = 0;
		}
		if ($ok == 1 || ($ok == 0 && $_POST['resend'] == 1)) {
			// send email to administrator with join request
			function random_gen($length)
			{
		  		$random= "";
		  		srand((double)microtime()*1000000);
		  		$char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				$char_list .= "abcdefghijklmnopqrstuvwxyz";
		  		$char_list .= "1234567890";
		  		// Add the special characters to $char_list if needed
				#$char_list .= "_";
		  		for($i = 0; $i < $length; $i++)
		  		{ 
		    			$random .= substr($char_list,(rand()%(strlen($char_list))), 1);
		  		}
		  		return $random;
			} 
		
			// step 1: get request user details
			$query = mysql_query("SELECT u.LastName, u.FirstName, a.name, u.email FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE u.id = '$userid'");
			$row = mysql_fetch_array($query);
			$reqfname = $row['FirstName'];
			$reqlname = $row['LastName'];
			$reqinstitute = $row['name'];
			$reqemail = $row['email'];
			// step 2: get group admin user details. 
			$query = mysql_query("SELECT u.LastName, u.FirstName, u.email FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE u.id = '$administrator'");
			$row = mysql_fetch_array($query);
			$admfname = $row['FirstName'];
			$admlname = $row['LastName'];
			$admemail = $row['email'];
			// step 3: compose message, etc
			$linkkey = random_gen(20);
			$link = "https://$domain/$basepath/index.php?page=usergrouprequest&key=$linkkey";
			  $message = "Message sent from http://$domain\r";  
			  $message .= "Sent by: $reqemail\r";
			  $message .= "Subject: CNV-analysis Page: Usergroup Request\r\r";
			  $message .= "$reqfname $reqlname requested to join the '$name' usergroup at the Illumina BeadArray data analysis and interpretation platform website.\r\r";
			  if ($motivation != '') {
				$message .= "The following motivation was included with the request:\r$motivation\r\r";
				  }
			  $message .= "You can approve or deny this request at the following page (after logging in):\r";
			  $message .= "$link\r";
			  $message .= "\r\rYour CNV-WebStore Administrator\r\n";
			  $headers = "From: no-reply@$domain\r\n";
			  $headers .= "BCC: $admemail, $reqemail\r\n";
			  /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
			// step 4: send the email
			if (mail($email,"CNV-analysis Page: Usergroup Request",$message,$headers)) {
				// message sent, now store request. 
				$query = mysql_query("INSERT INTO usergrouprequests (uid, gid, linkkey,motivation) VALUES ('$userid', '$joinid','$linkkey','$motivation') ON DUPLICATE KEY UPDATE linkkey = '$linkkey', motivation = '$motivation'");
				$info = mysql_info($query);
				echo "$info<br>";
		 		echo "<div class=sectie>\n";
				echo "<h3>Usergroup Request Sent To Admin</h3>\n";
				echo "<h4>$name...</h4>";
				echo "<p>An email has been sent to the usergroup administrator ($admfname $admlname) to approve your request. The email has also been sent your own specified address for reference. You will be notified when the request has been approved.</p>";
				echo" </div>\n";
			}
			 
			else {
				echo "<div class=sectie>\n";
				echo "<h3>Can't send email</h3>\n";
				echo "<p>I'm sorry but we couldn't send the confirmation email to the usergroup administrator.  Please report this problem to us by the contact form.</p>\n";
				echo "</div>\n";
			}
		}	
	}
	// print form to join another group	
	echo "<div class=sectie>\n";
	echo "<h3>Join Another Usergroup</h3>\n";
	echo "<p>Please select a group to join from the list below. You can add a motivation if needed in the textfield. If confirmation is needed, an email will be sent to the usergroup administrator upon submission.</p>\n";
}


// print the form
echo "<p><form action='index.php?page=group&type=join' method=POST>\n";
echo "<table>\n";
echo "<tr>\n";
echo "<th class=clear>Select Group</th>\n";
echo "<td class=clear><select name=joinid onchange=\"reloadgroupinfo(this.form)\">";
if ($joinid == 'none') {
	echo "<option value=none selected></option>\n";
}
else {
	echo "<option value=none></option>\n";
}
// non-public groups
$query = mysql_query("SELECT ug.id, ug.name, ug.description, ug.affiliation FROM usergroups ug WHERE ug.opengroup =0 AND ug.id NOT IN ( SELECT ugu.gid FROM usergroupuser ugu WHERE ugu.uid = '$userid')");
$nrgroups = mysql_num_rows($query);
if ($nrgroups > 0) {
	echo "<OPTGROUP label='Private Groups'>\n";
	while ($row = mysql_fetch_array($query)) {
		$uga = $row['affiliation'];
		if (!preg_match("/(^$myaffi,)|(,$myaffi,)|(,$myaffi$)|(^$myaffi$)/",$uga)) {
			#echo "$myaffi not found in $uga<br>";
			continue;
		}
		$ugid = $row['id'];
		$ugname = $row['name'];
		$ugdescr = $row['description'];
		if ($ugid == $joinid) {
			echo "<option value='$ugid' title='$ugdescr' SELECTED>$ugname</option>\n";

		}
		else {
			echo "<option value='$ugid' title='$ugdescr'>$ugname</option>\n";
		}
	}
}
// public groups
$query = mysql_query("SELECT ug.id, ug.name, ug.description FROM usergroups ug WHERE ug.opengroup = 1 AND ug.id NOT IN ( SELECT ugu.gid FROM usergroupuser ugu WHERE ugu.uid = '$userid')");
$nrgroups = mysql_num_rows($query);
if ($nrgroups > 0) {
	echo "<OPTGROUP label='Public Groups'>\n";
	while ($row = mysql_fetch_array($query)) {
		$ugid = $row['id'];
		$ugname = $row['name'];
		$ugdescr = $row['description'];
		if ($ugid == $joinid) {
			echo "<option value='$ugid' title='$ugdescr' SELECTED>$ugname</option>\n";

		}
		else {
			echo "<option value='$ugid' title='$ugdescr'>$ugname</option>\n";
		}

	}
}
echo "</select></td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo "<th class=clear label='Short Motivation, if you believe this will be needed'>Motivation</td>\n";
echo "<td class=clear><input type=text name=motivation size=40 value='$motivation'></td>\n";
echo "</tr>\n";
echo "</table></p>\n";
if ($joinid != 'none'){
	$yesno = array('Not Allowed', 'Allowed');
	$query = mysql_query("SELECT description, editcnv, editclinic FROM usergroups WHERE id = '$joinid'");
	$row = mysql_fetch_array($query);
	$descr = $row['description'];
	$editcnv = $row['editcnv'];
	$editclinic = $row['editclinic'];
	echo "<p><table cellspacing=0>\n";

	echo " <tr>\n";
	echo " <th $firstcell colspan=2>Group Information</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <th class=specalt $firstcell>Description</th>\n";
	echo " <td>$descr&nbsp;</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <th class=specalt $firstcell>Modify CNV Info</th>\n";
	echo " <td>$yesno[$editcnv]</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <th class=specalt $firstcell NOWRAP>Modify Clinical Info</th>\n";
	echo "<td>$yesno[$editclinic]</td>\n";
	echo "</tr>\n";
	echo "</table>\n";
}

echo "</p><p>\n";
echo "<input type=submit name=submit class=button value='Join'>\n";
echo "</form>\n";
echo "</div>\n";


?>
