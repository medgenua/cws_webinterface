<?php
if (isset($_POST['modify'])) {
	// vars 
	$groupid = $_POST['groupid'];
	$gname = $_POST['groupname'];
	$gdescr = $_POST['description'];
	$opengroup = $_POST['opengroup'];
	$confirmation = $_POST['confirmation'];
	$Affis = $_POST['Affis'];
	$affistring = '';
	foreach ($Affis as $name) {
		$affistring .= "$name,";
	}
	$affistring = substr($affistring,0,-1);
	$administrator = $_POST['administrator'];
	$editcnv = $_POST['editcnv'];
	$editclinic = $_POST['editclinic'];
	$editsample = $_POST['editsample'];
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName FROM usergroups ug JOIN users u ON u.id = ug.administrator WHERE ug.id = '$groupid'");
	$row = mysql_fetch_array($query);
	$oldadmin = $row['id'];
	$oldlname = $row['LastName'];
	$oldfname = $row['FirstName'];
	// update main properties
	if ($opengroup == 1) {
		$query = mysql_query("UPDATE usergroups SET name='$gname', description='$gdescr', opengroup=1, administrator='$administrator', confirmation=$confirmation, editcnv=$editcnv, editclinic=$editclinic, editsample='$editsample' WHERE id = '$groupid'");
	}
	else {
		$query = mysql_query("UPDATE usergroups SET name='$gname', description='$gdescr', affiliation='$affistring', opengroup=0, confirmation=1,administrator='$administrator',editcnv=$editcnv,editclinic=$editclinic, editsample='$editsample' WHERE id = '$groupid'");
	}
	echo "<div class=sectie>\n";
	echo "<h3>Usergroup Updated</h3>\n";
	echo "<h4>$gname</h4>\n";
	echo "<p>The changes were successfully applied. </p>\n";
	// update users
	$addusers = $_POST['addusers'];
	$delusers = $_POST['delusers'];
	foreach($addusers as $uid) {
		// add user to group
		$query = mysql_query("INSERT INTO usergroupuser (gid, uid) VALUES ('$groupid','$uid') ON DUPLICATE KEY UPDATE uid=uid");
		// share illumina projects 
		$projects = mysql_query("SELECT projectid FROM projectpermissiongroup WHERE groupid = '$groupid'");
		while ($project = mysql_fetch_array($projects)) {
			$pid = $project['projectid'];
			$insq = mysql_query("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic,editsample) VALUES ('$pid','$uid', '$editcnv', '$editclinic','$editsample') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '$editcnv', VALUES(editcnv),'$editcnv'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic'),editsample = if(VALUES(editsample) < '$editsample', VALUES(editsample),'$editsample')");
		}
		// share custom projects
		$projects = mysql_query("SELECT projectid FROM cus_projectpermissiongroup WHERE groupid = '$groupid'");
		while ($project = mysql_fetch_array($projects)) {
			$pid = $project['projectid'];
			$insq = mysql_query("INSERT INTO cus_projectpermission (projectid, userid, editcnv, editclinic, editsample) VALUES ('$pid','$uid', '$editcnv', '$editclinic','$editsample') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '$editcnv', VALUES(editcnv),'$editcnv'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic'),editsample = if(VALUES(editsample) < '$editsample', VALUES(editsample),'$editsample')");
		}

	}
	foreach($delusers as $uid) {
		$query = mysql_query("DELETE FROM usergroupuser WHERE gid = '$groupid' AND uid = '$uid'");
	}
	// update un-shared illumina projects
	$delprojects = $_POST['delprojects'];
	foreach($delprojects as $pid) {
		$query = mysql_query("DELETE FROM projectpermissiongroup WHERE groupid='$groupid' AND projectid = '$pid'");
	}
	// update un-shared custom projects
	$delcustomprojects = $_POST['delcustomprojects'];
	foreach($delcustomprojects as $pid) {
		$query = mysql_query("DELETE FROM cus_projectpermissiongroup WHERE groupid='$groupid' AND projectid = '$pid'");
	}

	// update administrator & notify new one if specified	
	if ($administrator != $oldadmin) {
		// send mail to new admin.
		$query = mysql_query("SELECT email, LastName, FirstName FROM users WHERE id = '$administrator'");
		$row = mysql_fetch_array($query);
		$email = $row['email'];
		$lname = $row['LastName'];
		$fname = $row['FirstName'];
		$message = "Message sent from http://$domain\r";  
		$message .= "Subject: CNV-analysis Page: Usergroup supervision\r\r";
		$message .= "$oldfname $oldlname passed the supervision of the project below on to you.  From now on you will recieve emails when new users wish to join this group. As supervisor you can also add or remove additionial users and set permissions. You can do this from the link below (after logging in)\r\r";
		$message .= "Usergroup Name: $gname\r";
		$message .= "Group details: https://$domain/$basepath/index.php?page=group&type=manage&gid=$groupid\r";
		$message .= "\r\rYour CNV-WebStore Administrator\r\n";
		$headers = "From: no-reply@$domain\r\n";
		if (mail($email,"CNV-analysis Page: Usergroup supervision update",$message,$headers)) {
		
		}
		else {
			echo "mail was not sent, something went wrong<br>";
		}


		echo "<p>Since you have passed the supervision of this group on to $fname $lname, you will no longer be able to select this project below. The new adminstrator will be notified of this change by email.</p>\n";
	}
	echo "</div>\n";
}

# ONCE ALL IS PROCESSED, RELOAD THE INFROMATION FOR THE SELECTED GROUP.
// get posted var
$gid = $_POST['groupid'];
if ($gid == '') {
	$gid = $_GET['gid'];
}
if ($gid == '') {
	$gid = $groupid;
}

// group selection form
echo "<div class=sectie>\n";
echo "<h3>Manage usergroups</h3>\n";
echo "<h4>...created by you</h4>\n";
echo "<p>Pick a usergroup from the list below to edit permissions, associated projects, add or remove users and so on.</p>\n";


//GET GROUPS THAT ARE MANAGED BY CURRENT USER
$query = mysql_query("SELECT id, name FROM usergroups WHERE administrator = '$userid'");
$numgroups = mysql_num_rows($query);
if ($numgroups == 0) {
	echo "<p>You don't supervise any usergroups.</p>\n";
	echo "</div>\n";
}
else {
	echo "<form action='index.php?page=group&type=manage' method=POST>\n";
	echo "<p>Available usergroups:\n ";
	echo "<select name=groupid><option value=''></option>\n";
	while ($row = mysql_fetch_array($query)) {
		$cgid = $row['id'];
		$gname = $row['name'];
		if ($gid == $cgid) {
			$selected = "SELECTED";
		}
		else {
			$selected = '';
		}
		echo "<option value='$cgid' $selected>$gname</option>\n";
	}
	echo "</select>\n";
	echo "</p>\n";
	echo "<p><input type=submit class=button name=submit value='Pick'></p>\n";
	echo "</form>\n";
	echo "</div>\n";
}

if ($gid != '') {
	// get group info
	$query = mysql_query("SELECT name, description, affiliation, opengroup, administrator, confirmation, editcnv, editclinic,editsample FROM usergroups WHERE id = '$gid'");
	$row = mysql_fetch_array($query);
	$groupname = $row['name'];
	$description = $row['description'];
	$affistring = $row['affiliation'];
	$Affis = explode(',',$row['affiliation']);
	$opengroup = $row['opengroup'];
	$gadmin = $row['administrator'];
	$confirmation = $row['confirmation'];
	$editcnv = $row['editcnv'];
	$editclinic = $row['editclinic'];
	$editsample = $row['editsample'];
	$affihash = array();
	if ($Affis) {
		foreach($Affis as $affid) {
			$affihash["$affid"] = 1;
		}
	}
	$problem = 0;
	$checked = array('','checked');
	$checkedzero = array('checked','');	
	// print form
	if (isset($_POST['modify'])) {
		echo "<div class=sectie>\n";
		echo "<h3>Updated Properties: $groupname</h3>\n";
		echo "<h4>Double Check your settings</h4>\n";
		//echo "<p>Pick a usergroup from the list below to edit permissions, associated projects, add or remove users and so on.</p>\n";
	}
	else {
		echo "<div class=sectie>\n";
		echo "<h3>$groupname</h3>\n";
		echo "<h4>Set properties</h4>\n";
	}
	if ($gadmin != $userid) {
		echo "<p>You are not the administrator of this usergroup.</p>\n";
		echo "</div>\n";
	}
	else {
		echo "<form action='index.php?page=group&amp;type=manage&amp;groupid=$gid' method=POST>\n";
		echo "<input type=hidden name=modify value=1>\n";
		echo "<input type=hidden name=groupid value='$gid'>\n";
		echo "<p><table>\n";
		echo "<tr>\n";
		echo "<th class=clear title='e.g. CMG Antwerp Clinicians'>Group Name</th>\n";
		echo "<td class=clear><input type=text name='groupname' size=50 value='$groupname'></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=clear valign=top>Group Description</th>\n";
		echo "<td class=clear><TEXTAREA NAME='description' COLS=48 ROWS=2>$description</TEXTAREA></td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<tr>\n";
		echo "<th class=clear title='If selected, all users can join this usergroup'>Open Group</th>\n";
		echo "<td class=clear>	<input type='radio' name='opengroup' value=0 $checkedzero[$opengroup] onclick=\"document.getElementById('SetAffi').style.display='';document.getElementById('SetConfirm').style.display='none';\"> No";
		echo " <input type='radio' name='opengroup' value=1 $checked[$opengroup] onclick=\"document.getElementById('SetAffi').style.display='none';document.getElementById('SetConfirm').style.display='';\"> Yes</td>\n";
		echo "</tr>\n";
		if ($opengroup ==0) {
			echo "<tr id='SetConfirm' style='display:none;' title='Is confirmation needed when a user joins an open group?'>\n";
		}
		else {
			echo "<tr id='SetConfirm' title='Is confirmation needed when a user joins an open group?'>\n";
		}
		echo "<th class=clear>Confirmation needed</th>\n";
		echo "<td class=clear>	<input type='radio' name='confirmation' value=0 $checkedzero[$confirmation]> No ";
		echo "	<input type='radio' name='confirmation' value=1 $checked[$confirmation]> Yes </td>\n";
		echo "</tr>\n";
		if ($opengroup == 0) {
			echo "<tr id='SetAffi'>\n";
		}
		else {
			echo "<tr id='SetAffi' style='display:none;'>\n";
		}
		echo "<th class=clear valign=top>Allowed Institutions</th>\n";
		echo "<td class=clear><select name='Affis[]' size=4 MULTIPLE>";
		// get own affiliation
		$query = mysql_query("SELECT a.name, a.id FROM affiliation a JOIN users u ON u.Affiliation = a.id WHERE u.id = '$userid' LIMIT 1");
		$row = mysql_fetch_array($query);
		$ownaffi = $row['name'];
		$ownid = $row['id'];
		
		if ($affihash[$ownid] == 1) {
			$selected = 'SELECTED';
		}
		else {
			$selected = '';
		}
		echo "<option value='$ownid' $selected>$ownaffi</option>\n";
		// get others, order alfabetically
		$query = mysql_query("SELECT a.name, a.id FROM affiliation a WHERE NOT a.id = '$ownid' ORDER BY a.name");
		while ($row = mysql_fetch_array($query)) {
			$currname = $row['name'];
			$currid = $row['id'];
			if ($affihash[$currid] == 1) {
				$selected = 'SELECTED';
			}
			else {
				$selected = '';
			}
			echo "<option value='$currid' $selected>$currname</option>\n";
		}
		echo "</SELECT>\n";
		echo "</td>\n";
		echo "</tr>";
		echo "<tr>\n";
		echo "<th class=clear title='Will users from this group be able to classify, delete or modify CNV calls?'>Change CNV information</th>\n";
		echo "<td class=clear><input type=radio name=editcnv value=0 $checkedzero[$editcnv]> No <input type=radio name=editcnv value=1 $checked[$editcnv]> Yes</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th class=clear title='Will users from this group be able to add or edit clinical information?'>Change Clinical Information</th>\n";
		echo "<td class=clear><input type=radio name=editclinic value=0 $checkedzero[$editclinic]> No <input type=radio name=editclinic value=1 $checked[$editclinic]> Yes</td>\n";
		echo "</tr>\n";
			echo "<tr>\n";
		echo "<th class=clear title='Will users from this group be able to manage projects (share, join, delete)?'>Manage Projects</th>\n";
		echo "<td class=clear><input type=radio name=editsample value=0 $checkedzero[$editsample]> No <input type=radio name=editsample value=1 $checked[$editsample]> Yes</td>\n";
		echo "</tr>\n";

		echo "<tr>\n";
		echo "<th class=clear title='Make someone else administrator of the group'>Administrator</th>\n";
		if ($opengroup == 0) {
			$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE a.id IN ($affistring) ORDER BY u.Affiliation, u.LastName");
		}
		else {
			$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY u.Affiliation, u.LastName");
		}
		$currinst = '';
		echo "<td class=clear><select name=administrator>\n";
		while ($row = mysql_fetch_array($query)) {
			$newid = $row['id'];
			$fname = $row['FirstName'];
			$lname = $row['LastName'];	
			$newinst = $row['name'];
			if ($newinst != $currinst) {
				if ($currinst != '') {
					echo "</OPTGROUP>\n";
				}
				echo "<OPTGROUP label='$newinst'>\n";
				$currinst = $newinst;
			} 
			if ($newid == $gadmin) {
				$selected = 'selected';
			}
			else {
				$selected = '';
			}
			echo "<option value='$newid' $selected>$lname $fname</option>\n";
		}
		echo "</select></td>\n";
		echo "</tr>\n";
		echo "</p><p>\n";
		// remove users
		$query = mysql_query("SELECT ugu.uid, u.LastName, u.FirstName, a.name FROM usergroupuser ugu JOIN users u JOIN affiliation a ON u.Affiliation = a.id AND ugu.uid = u.id WHERE gid = '$gid' ORDER BY u.Affiliation");
		$members = array();
		echo "<tr>\n";
		echo "<th class=clear title='Remove Members from the group' valign=top>Remove Members</th>\n";
		echo "<td class=clear><select name='delusers[]' id='deluserlist' size=5 MULTIPLE>\n";
		$currinst = '';
		while ($row = mysql_fetch_array($query)) {
			$cuid = $row['uid'];
			$clname = $row['LastName'];
			$cfname = $row['FirstName'];
			$inst = $row['name'];
			if ($currinst != $inst) {
				if ($currinst != '') {
					echo "</optgroup>";
				}
				echo "<optgroup label='$inst'>\n";
				$currinst = $inst;
			}
			echo "<option value='$cuid'>$clname $cfname</option>\n";
			$members[$row['uid']] = 1;
		}
		echo "</select>\n";
		echo "</td>\n";
		echo "</tr>\n";
		// add users
		echo "<tr>\n";	
		echo "<th class=clear title='Add Members to the group' valign=top>Add Members</th>\n";
		echo "<td class=clear><select name='addusers[]' id='userlist' size=5 MULTIPLE>\n";
		if ($opengroup == 1) {
			$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY a.name, u.LastName");
 		}
		else {
			$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE a.id IN ($affistring) ORDER BY a.name, u.LastName");
		}
		$currinst = '';
		while($row = mysql_fetch_array($query)) {
			$uid = $row['id'];
			$lastname = $row['LastName'];
			$firstname = $row['FirstName'];
			$institute = $row['name'];
			if ($currinst != $institute) {
				if ($currinst != '') {
					echo "</optgroup>";
				}
				echo "<optgroup label='$institute'>\n";
				$currinst = $institute;
			}
			if ($members[$uid] == 1) {
				continue;	
			}
			echo "<option value='$uid'>$lastname $firstname</option>\n";
		}
		echo "</select></td>\n";
		echo "</tr>\n";
		
		// illumina projects
		$query = mysql_query("SELECT p.id, p.naam, p.chiptype, u.LastName, u.FirstName FROM project p JOIN users u JOIN projectpermissiongroup ppg ON u.id = ppg.sharedby AND p.id = ppg.projectid WHERE ppg.groupid = '$gid' ORDER BY p.chiptypeid");
		$numprojects = mysql_num_rows($query);
		if ($numprojects > 0) {
			echo "<tr>\n";
			echo "<th valign=top class=clear>Unshare Illumina Projects</th>\n";
			echo "<td class=clear>\n"; // containing cell
			echo " <table cellspacing=0>\n";
			echo " <tr>\n";
			echo " <th $firstcell class=topcellalt><img src='images/content/delete.gif' width=8px height=8px></th><th class=topcellalt>Name</th><th class=topcellalt>Shared By</th><th class=topcellalt>Chiptype</th>\n";
			echo " </tr>\n";
			while ($row = mysql_fetch_array($query)) {
				$pid = $row['id'];
				$sharedby = $row['LastName'] . " " . $row['FirstName'];
				$pname = $row['naam'];
				$chip = $row['chiptype'];
				echo " <tr>\n";
				echo " <td $firstcell><input type=checkbox name='delprojects[]' value='$pid'></td>\n";
				echo " <td>$pname</td>\n";
				echo " <td>$sharedby</td>\n";
				echo " <td>$chip</td>\n";
				echo " </tr>\n";
			}	
 			echo " </table>\n";
			echo " </td>\n";
			echo "</tr>\n";

		}
		// custom projects
		$query = mysql_query("SELECT p.id, p.naam, u.LastName, u.FirstName FROM cus_project p JOIN users u JOIN cus_projectpermissiongroup ppg ON u.id = ppg.sharedby AND p.id = ppg.projectid WHERE ppg.groupid = '$gid'");
		$numprojects = mysql_num_rows($query);
		if ($numprojects > 0) {
			echo "<tr>\n";
			echo "<th valign=top class=clear>Unshare Custom Projects</th>\n";
			echo "<td class=clear>\n"; // containing cell
			echo " <table cellspacing=0>\n";
			echo " <tr>\n";
			echo " <th $firstcell class=topcellalt><img src='images/content/delete.gif' width=8px height=8px></th><th class=topcellalt>Name</th><th class=topcellalt>Shared By</th>\n";
			echo " </tr>\n";
			while ($row = mysql_fetch_array($query)) {
				$pid = $row['id'];
				$sharedby = $row['LastName'] . " " . $row['FirstName'];
				$pname = $row['naam'];
				echo " <tr>\n";
				echo " <td $firstcell><input type=checkbox name='delcustomprojects[]' value='$pid'></td>\n";
				echo " <td>$pname</td>\n";
				echo " <td>$sharedby</td>\n";
				echo " </tr>\n";
			}	
 			echo " </table>\n";
			echo " </td>\n";
			echo "</tr>\n";

		}
	
		echo "</table>\n";
		echo "<input type=submit class=button name=update value='Submit'>\n";
		echo "</form>\n";
		echo "</div>\n";
	}
} 

?>

