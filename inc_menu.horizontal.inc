<?php
// for syntax...
?>

<ul id="topmenu">
	<li><a href="index.php?page=main" >Main</a></li>
	<?php 
	if ($webonly != 1) {
		echo "<li>&nbsp;| <a href='javascript:void(0)' onmouseover=\"mopen('m1')\" onmouseout='mclosetime()' >New Analysis </a>\n";
	}
	else {
		echo "<li>&nbsp;| <a href='javascript:void(0)' onmouseover=\"mopen('m1')\" onmouseout='mclosetime()' >New Data </a>\n";
	}
	?>
	<div class=submenu id="m1" onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
	<?php 
	if ($SiteStatus == 'LiftOver' ){
		echo "<a href='javascript:void()'>The platform is currently <br/>&nbsp;being Lifted to a new<br/>&nbsp;Genome Build. <br/><br/>&nbsp;To prevent data loss, <br/>&nbsp;it is temporarily put in a <br/>&nbsp;read-only mode</a>";
	}
	elseif(array_key_exists('Archived', $_SESSION) && $_SESSION['Archived'] == 1) {
		echo "<a href='javascript:void()'>You are browsing <br/>&nbsp;an archived and read-only<br/>&nbsp;version of the data.</a>";
	}
	else {
	?> 
		<?php 
		if ($webonly != 1) {
		?>
			<h3>Illumina Data</h3><br style="clear:both;"/>
			<a href="index.php?page=run&amp;type=multiple">Majority Vote</a>
			<!--<a href="index.php?page=run&amp;type=quantisnp">QuantiSNP</a>
			<a href="index.php?page=run&amp;type=penncnv">PennCNV</a>
			<a href="index.php?page=run&amp;type=vanillaice">VanillaICE</a>
			-->
			<a href="index.php?page=upload">Upload Data</a>
			<a href="index.php?page=peds">Add Family Info</a>
			<h3>Non-Illumina Data</h3><br style="clear:both;"/>
		<?php
		}
		else {
			echo "<h3>Add CNV-Data</h3><br/>";
		}
		?>
		
		<a href="index.php?page=custom_methods&amp;type=st">Upload File</a>
		<a href='index.php?page=custom_methods&amp;type=create'>Create Sample</a>
		<a href="index.php?page=custom_methods&amp;type=otf">On-The-Fly</a>
		<a href="index.php?page=custom_methods&amp;type=cp">Manage Parsers</a>
	<?php
	}
	?>
	</div>
	 </li>
	<li>&nbsp;| <a href="javascript:void(0)" onmouseover="mopen('m2')" onmouseout="mclosetime()">Browse Results</a>	
	<div class=submenu id="m2"  onmouseover="mcancelclosetime()"   onmouseout="mclosetime()">
		<?php 
		if ($webonly != 1) {
		?>
			<h3>Illumina Data</h3><br style="clear:both;"/>
				<a href="index.php?page=results&amp;type=tree">Project Tree</a>
			<a href="index.php?page=results&type=illumina">Set Permissions</a>
			<a href="index.php?page=results&type=xml">XML Files</a>
			<h3>Non-Illumina Data</h3><br style="clear:both;"/>
		<?php
		}
		else {
			echo "<h3>By Project</h3><br/>";
		}
		?>
		<a href="index.php?page=custom_methods&amp;type=tree">Project Tree</a>
		<a href="index.php?page=custom_methods&amp;type=custom">Set Permissions</a>
		<h3>Search Options</h3><br style="clear:both;"/>
		<a href="index.php?page=results&amp;type=sample">Search Sample</a>
		<a href="index.php?page=results&amp;type=region">Search Region/Gene</a>
		<a href="index.php?page=results&amp;type=pheno">Search Phenotypes</a>
		<a href="index.php?page=results&amp;type=filter">Advanced Filtering</a>
	</div>
	</li>
	<li>&nbsp;| <a href="javascript:void(0)" title="Export data in various formats" onmouseover="mopen('m3')" onmouseout="mclosetime()">Export</a>
	<div class=submenu id='m3' onmouseover="mcancelclosetime()"   onmouseout="mclosetime()">
		<h3>Data Dumps</h3><br style="clear:both;"/>
		<a href="index.php?page=tracks&amp;type=main">Genome Browser Tracks</a>
		<a href="index.php?page=dumpprojects">MySQL/CSV dump</a>
		<a href="index.php?page=stats">Filter Based dump</a>
		<a href="index.php?page=belcocyt">Copy Number Data File (Belcocyt)</a>
		<a href="index.php?page=clinstats">Clinical information</a>
		<h3>Summary Plots</h3><br style="clear:both;"/>
		<a href='index.php?page=datastats'>Graphical Data Statistics</a>
		<?php include('inc_recent2.inc'); ?>
	</div>
	</li>
	<li>&nbsp;| <a href="javascript:void(0)"  onmouseover="mopen('m7')" onmouseout="mclosetime()">Settings</a>
	<div class=submenu id='m7' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
		<h3>Analysis Settings</h3><br style="clear:both;"/>
		<a href="index.php?page=version">Platform Version History</a>
		<a href="index.php?page=chiptypes">Chiptypes</a>
		<a href="index.php?page=settings&amp;type=cnv">Algorithms</a>
		<a href="index.php?page=settings&amp;type=prior">Prioritization</a>
		<h3>Variant Classification Rules (beta)</h3><br style="clear:both;"/>
		<a href="index.php?page=classification&amp;type=create">Create Rules</a>
		<a href="index.php?page=classification&amp;type=manage">Manage Rules</a>
		<a href="index.php?page=classification&amp;type=activate">Activate Rules</a>
		<h3>Visual Filters Settings (beta)</h3><br style="clear:both;"/>
		<a href="index.php?page=filters&amp;type=create">Create Filters</a>
                <a href="index.php?page=filters&amp;type=manage">Manage Filters</a>
		<a href="index.php?page=filters&amp;type=activate">(De)Activate Filters</a>
		<h3>Display &amp; Annotation Settings</h3><br style="clear:both;"/>
		<a href="index.php?page=settings&amp;type=plots&st=res">Results Display</a>
		<a href="index.php?page=settings&amp;type=plots&st=ann">Annotation Data</a>
		<a href="index.php?page=settings&amp;type=plots&st=con">Control Data</a>
		<a href="index.php?page=settings&amp;type=plots&st=publ">Public Resources</a>
	</div>
	</li>
	<li>&nbsp;| <a href="javascript:void(0)" onmouseover="mopen('m6')" onmouseout="mclosetime()">Other Tools</a>	
	<div class=submenu id='m6' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
		<h3>Direct Tool Access</h3><br style="clear:both;"/>
		<a href='index.php?page=genes'>Gene By Region</a>
		<a href='index.php?page=probes'>BeadChip Coverage</a>
		<!--<h3>Q-PCR Tools</h3><br/>
		<a href="index.php?page=manageprimers">PrimerDB</a>
		<a href="index.php?page=primerdesign">Design Genomic Primers</a>
		<a href="http://medgen.ua.ac.be/~gvandeweyer/index.php?page=spanprimers" target='_blank'>Design cDNA Primers</a>
		<a href="index.php?page=sybrgreensetup">SybrGreen exp.</a> 
		<a href="index.php?page=UniSeqSetup">UniSeq Finder</a>
		<h3>Other Tools</h3><br/> -->
		<a href="http://143.169.238.111/pubmedfetcher/index.php?page=main" target='_blank'>PubMed Fetcher</a>
        <!--<a href="http://medgen.ua.ac.be/~gvandeweyer/index.php?page=1000" target='_blank'>1000 Genomes Scanner</a>-->
		<!--<a href="http://143.169.238.111/southernblot/index.php?page=main" target='_blank'>Southern Blot Probe Design</a>-->
	</div>
	</li>

	<li>&nbsp;| <a href="index.php?page=documentation">Documentation</a></li>
	<li>&nbsp;| <a href="index.php?page=contact" title="Contact">Contact</a></li>
	<li>&nbsp;| <a href='javascript:void(0)' onclick="window.print();">Print</a></li>
	<?php
	if (isset($_SESSION['level']) && $_SESSION['level'] >= 3) {
		echo "<li>&nbsp;| <a href=\"javascript:void(0)\" onmouseover=\"mopen('m8')\" onmouseout=\"mclosetime()\" >Admin</a>";
		echo "<div class=submenu id='m8' onmouseover=\"mcancelclosetime()\" onmouseout=\"mclosetime()\">";
		echo "<h3>WebStore Administrator</h3><br style=\"clear:both;\"/>";
		echo "<a href=\"index.php?page=admin_pages\">Overview</a>";
		echo "<a href=\"index.php?page=admin_pages&amp;type=NewFields\">Add Database Fields</a>";
		echo "<a href=\"index.php?page=admin_pages&amp;type=SiteStatus\">Platform Status</a>";
		echo "<a href=\"index.php?page=admin_pages&amp;type=update\">Platform Updates</a>";
		echo "<a href=\"index.php?page=admin_pages&amp;type=users\">User Management</a>";
		echo "<a href=\"index.php?page=admin_pages&amp;type=impersonate\">Impersonate User</a>";
		echo "<a href=\"index.php?page=admin_pages&amp;type=lift\">Genomic Build Update</a>";
		echo "</div>";
		echo "</li>";
	}
	?>

</ul>

<!-- THE USERMENU -->
<ul id=topmenu style='float:right'>
<?php 
if (isset($loggedin) && $loggedin == 1) {
	// are there new messages?
	if ($_SESSION['newmessages'] == 1) {
		$env = "<img src='images/content/mailred.png' style='height:8px'>";
	}
	else {
		$env = "<img src='images/content/mailgrey.png' style='height:8px'>";
	}
	// create context specific link to the documentation.
	$type = '';
	if (array_key_exists('type',$_GET)) $type = $_GET['type'];
	$doc = "|<a href='index.php?page=documentation&amp;s=$page&t=$type' target='_blank' class=img> ? </a>";
	// If administrator, check for platform updates.
	$upd = '';
	//if ($level == 3) {
	//	// provide container
	//	$upd = "<span id='updatesnotifier' style='display:none;'></span>";
	//	// load the script that checks for updates
	//	// only if session variable is not set.
	//	
	//	$upd .= "<script type='text/javascript' src='javascripts/updates.js'></script>";
	//}

?>
<li><a href='javascript:void(0)' title='Set Personal Settings' onmouseover="mopen('m5')" onmouseout="mclosetime()"><?php echo $firstname;?>
	<div class=submenu id='m5' onmouseover="mcancelclosetime()" onmouseout="mclosetime()">
		<a href="index.php?page=group&amp;type=mine">My Usergroups</a>
		<a href="index.php?page=projects">My Projects</a>
		<a href="index.php?page=recent">Recent Activities</a>
		<a href="index.php?page=personal">My Details</a>
		<a href="index.php?page=inbox">Inbox</a>
		<a href='logout.php' title=Logout>Log Out</a>
	</div>
	</li>
	<li><?php echo $upd; ?></li>

	<li>|<a href='index.php?page=inbox' class=img>
<?php 
	echo "$env</a></li><li>&nbsp;$doc&nbsp;&nbsp;</li>\n";
}
else { 
	echo "<li><a href=\"index.php?page=login\">Log In</a></li> &nbsp; \n";
}
?>
</ul>


