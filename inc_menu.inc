<h3>Menu</h3>
<ul id="ul-menu">
	<li><a href="index.php?page=main" >Main</a></li>
	<li><a href="javascript:toggle('run')">New Analysis</a>
	<div id='run' <?php if ($_GET['v'] != 'r') {echo "style='display:none;'";} ?>>
	<ul id="ul-submenu">
		<li><a href="index.php?page=run&amp;type=main&amp;v=r">Main</a></li>
		<li><a href="index.php?page=run&amp;type=multiple&amp;v=r">Multiple</a></li>
		<li><a href="index.php?page=run&amp;type=quantisnp&amp;v=r">QuantiSNP</a></li>
		<li><a href="index.php?page=run&amp;type=penncnv&amp;v=r">PennCNV</a></li>
		<li><a href="index.php?page=run&amp;type=vanillaice&amp;v=r">VanillaICE</a></li>
		<li><a href="index.php?page=upload&amp;v=r">Upload Data</a></li>
		<li><a href="index.php?page=peds&amp;v=r">Add Family Info</a></li>
	</ul>
	</div></li>
	<li><a href="javascript:toggle('browse')">Browse Results</a>	
	<div id=browse <?php if ($_GET['v'] != 'b') {echo "style='display:none;'";} ?>>
	<ul id="ul-submenu">
		<li><a href="index.php?page=results&amp;type=main&amp;v=b">Main</a></li>
		<li><a href="index.php?page=results&amp;type=tree&amp;v=b">Project Tree</a></li>
		<li><a href="index.php?page=results&amp;type=sample&amp;v=b">Search Sample</a></li>
		<li><a href="index.php?page=results&amp;type=region&amp;v=b">Search Region</a></li>
		<li><a href="index.php?page=results&amp;type=pheno&amp;v=b">Search Phenotypes</a></li>
		<li><a href="index.php?page=results&amp;type=filter&amp;v=b">Advanced Filtering</a></li>
		<li><a href="index.php?page=results&type=illumina&amp;v=b">Set Permissions</a></li>
		<li><a href="index.php?page=results&type=xml&amp;v=b">XML Files</a></li>
	</ul>
	</div></li>
	
	<li><a href="index.php?page=tracks&amp;type=main">Browser Tracks</a></li>

	<li><a href="javascript:toggle('custom')">Custom Methods</a>
	<div id=custom <?php  if ($_GET['v'] != 'c') {echo "style='display:none;'";} ?>>
	<ul id="ul-submenu">
		<li><a href='index.php?page=custom_methods&amp;v=c'>Main</a></li>
		<li><a href='index.php?page=custom_methods&amp;type=st&amp;v=c'>Upload File</a></li>
		<li><a href='index.php?page=custom_methods&amp;type=tree&amp;v=c'>Project Tree</a></li>
		<li><a href='index.php?page=custom_methods&amp;type=otf&amp;v=c'>On-The-Fly</a></li>
		<li><a href='index.php?page=custom_methods&amp;type=cp&amp;v=c'>Manage Parsers</a></li>
		<li><a href='index.php?page=custom_methods&amp;type=custom&amp;v=c'>Set Permissions</a></li>
	</ul>
	</div></li>
	<li><a href="javascript:toggle('Profile')">Profile</a>
	<div id=Profile <?php if ($_GET['v'] != 'p') {echo "style='display:none;'";} ?>>
	<ul id="ul-submenu">
		<li><a href="index.php?page=settings&amp;type=plots&amp;v=p">Plot Settings</a></li>
		<li><a href="index.php?page=group&amp;type=mine&amp;v=p">My Usergroups</a></li>
		<li><a href="index.php?page=projects&amp;v=p">My Projects</a></li>
		<li><a href="index.php?page=recent&amp;v=p">Recent Activities</a></li>
		<li><a href="index.php?page=personal&amp;v=p">My Details</a></li>
		<li><a href='index.php?page=inbox&amp;v=p'>Inbox</a></li>

	</ul>
	</div></li>
	<li><a href='index.php?page=stats'>Statistics</a></li>	
	
	<li><a href="javascript:toggle('qpcrstuff')">Q-PCR</a>	
	<div id=qpcrstuff <?php  if ($_GET['v'] != 'q') {echo "style='display:none;'";} ?>>
	<ul id="ul-submenu">
		<li><a href="http://medgen.ua.ac.be/cnv/index.php?page=manageprimers&amp;v=q">PrimerDB</a></li>
		<li><a href="http://medgen.ua.ac.be/cnv/index.php?page=primerdesign&amp;v=q">Design Primers</a></li>
		<li><a href="index.php?page=sybrgreensetup&amp;v=q">SybrGreen exp.</a></li>
		<li><a href="http://medgen.ua.ac.be/cnv/index.php?page=UniSeqSetup&amp;v=q">UniSeq Finder</a></li>
	</ul>
	</div></li>
	<li><a href="javascript:toggle('settings')">Analysis Settings</a>
	<div id=settings <?php if ($_GET['v'] != 's') {echo "style='display:none;'";} ?> >
	<ul id="ul-submenu">
		
		<li><a href="index.php?page=chiptypes&amp;v=s">Chiptypes</a></li>
		<li><a href="index.php?page=settings&amp;type=cnv&amp;v=s">Algorithms</a></li>
		<li><a href="index.php?page=settings&amp;type=prior&amp;v=s">Prioritization</a></li>
	</ul>
	</div></li>

	<li><a href="index.php?page=tutorial">Documentation</a></li>
	<li><a href="index.php?page=contact" title="Contact">Contact</a></li>
	<li><a href="index.php?page=credits" title="Credits">Credits</a></li>
<?php 
if ($loggedin == 1) {
	echo "<li><a href='logout.php' title=Logout>Log Out</a></li>\n";
}
else {
	echo "<li><a href='index.php?page=login' title=Login>Log in</a></li>\n";
}
?>
	<li><div class='menubutton' onclick="window.print();">Print</div></li>
	<?php 
	if ($_SESSION['level'] >= 3) {
		echo "		<li><a href=\"index.php?page=admin_pages\" title=\"Adminstrator pages\">Admin Pages</a></li>\n";
	}
	?>
	<li><a href='http://sourceforge.net/projects/cnv-webstore/' target='_blank'>SourceForge Home</a></li>
</ul>

