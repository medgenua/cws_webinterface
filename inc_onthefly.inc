<?php

$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

ob_start();

//load file and pick parser
if ($_POST['step'] == '' || !isset($_POST['step'])) {
	$parser = $_GET['parser'];
	echo "<div class=sectie>\n";
	echo "<h3>Parse Custom Reports On The Fly</h3>\n";
	echo "<p>Browse custom reports using the tools provided on this website without storing any information.  Use primergeneration, check decipher entries, compare the results to the HapMap samples run on the Illumina BeadStation, and so forth. </p>\n";
	echo "<p>Just pick you parser below and specify an input file. If there is no parser available, feel free to create one. As yet, there is only support for tabular file formats (no xml files).</p>\n";
	echo "<p><form enctype='multipart/form-data' action='index.php?page=custom_methods&amp;type=otf' method=POST>\n";
	echo "<input type=hidden name=step value=2>\n";
	echo "<table>\n";
	
	echo " <tr>\n";
	echo "  <td class=clear>Pick a parser</td>\n";
	echo "  <td class=clear colspan=2><select name=parser onchange=\"reloadparserotf(this.form)\"><option value=none></option>";
	$result = mysql_query("SELECT id, Name FROM parsers ORDER BY Array");
	while ($row = mysql_fetch_array($result)) {
		if ($row['id'] == $parser) {
			echo "<option value='".$row['id']."' SELECTED>".$row['Name']."</option>";
		}
		else {
			echo "<option value='".$row['id']."' >".$row['Name']."</option>";
		}
	}
	echo "</select></td>\n";
	echo " </tr>\n";
	if (isset($_GET['parser']) && $_GET['parser'] != 'none'){
		echo " <tr>\n";
		echo " <td class=clear></td>\n";
		echo " <td class=clear>Info</td\n";
		echo " <td class=clear style='padding-left:3px;border-left:solid 1px ;'>";
		$info = mysql_query("SELECT Array, Algorithm, Comments FROM parsers WHERE id=$parser");
		$row = mysql_fetch_array($info);
		echo "Platform: ". $row['Array'] ."<br/>Algorithm: ". $row['Algorithm'] ."<br/>Comments: ".$row['Comments']."</td>\n";
		echo "</tr>\n";
	}
	echo " <tr>\n";
	echo "  <td class=clear>Input file</td>\n";
	echo "  <td class=clear colspan=2><input type=file name=inputfile ></td>\n";
	echo " </tr>\n";	
	echo "</table>\n";
	echo "</p><p><input type=submit class=button name=submit value=Submit></p>\n";
	echo "</form>\n";
	echo "</div>\n";
}
elseif ($_POST['step'] == 2) {
	$parserid = $_POST['parser'];
	$filetype = $_FILES['inputfile']['type'];
	$ok = 1;
 	if (!($filetype == "text/plain") ) {
 		echo "<span class=nadruk>Problem:</span> Only .txt and .xml files are allowed. (detected $filetype )<br />";
   		$ok = 0;
 	}
 	if ($ok == 1) {
		$filename = basename( $_FILES['inputfile']['name']);

  		if(!move_uploaded_file($_FILES['inputfile']['tmp_name'], "/tmp/$filename") ) {
  			$ok = 0;
			echo "<span class=nadruk>Problem:</span> Upload to /tmp/$filename failed.<br />";
 		}
 	}
	if ($ok == 1) {
		// CHECK IF PARSER IS CORRECT : check two entries. 
		#system("echo $scriptpass | sudo -u $scriptuser -S bash -c \"dos2unix '/tmp/$filename'\"");
		mysql_select_db("$db");
		$query = mysql_query("SELECT parser FROM parsers WHERE id = $parserid");
		$row = mysql_fetch_array($query);
		$parserline = $row['parser'];
		$parts = explode("|||",$parserline);
		$parser = array();
		foreach ($parts as $item) {
			$subparts = explode('@@@',$item);
			$parser[$subparts[0]] = $subparts[1];
		}
		// SET GLOBAL VARS
		$ncol = $parser['cols'];
		$headers = $parser['headers'];
		$lpe = $parser['lpe'];
		$sep = $parser['sep'];
		// START PROCESSING
		$fh = fopen("/tmp/$filename", "r");
		if ($headers == 1){
			$headline = fgets($fh);
		}
		echo "<div class=sectie>\n";
		echo "<h3>Custom Data Report</h3>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr>\n";
		$fc = $firstcell;
		// headers from parsed file
		for ($i = 1; $i<=$ncol; $i++) {
			$def = $parser["col".$i."_def"];
			if ($def == 'Refine') {
				//$regex = $parser['col'.$i.'_regex'];
				$keep = $parser['col'.$i.'_keep'];
				$keepdef = $parser['col'.$i.'_keepdef'];
				$pieces = explode(' ',$keepdef);
				$keeps = explode(' ',$keep);
				$idx = 0;
				foreach($pieces as $item){
					echo "<th $fc>$item</th>\n";
					$keepidx = $keeps[$idx];
					if ($item == 'Chr') {
						$chrcol = $i . '_'. $keepidx;
					}
					elseif ($item == 'Start') {
						$startcol = $i . "_" . $keepidx;
					}
					elseif ($item == 'Stop') {
						$stopcol = $i . "_" . $keepidx;
					}
					elseif ($item == 'CN') {
						$cncol = $i . "_".$keepidx;
					}
					$idx++;	
					$fc = '';
				}
			}
			elseif ($def != 'discard') {
				echo "<th $fc>$def</th>\n";
				$fc = '';
				if ($def == 'Chr') {
					$chrcol = $i;
				}
				elseif ($def == 'Start') {
					$startcol = $i;
				}
				elseif ($def == 'Stop'){
					$stopcol = $i;
				} 
				elseif ($def == 'CN') {
					$cncol = $i;
				}
			}
		}
		$lineidx = 0;
		while (!feof($fh) && $lineidx < 3) {
			#$line = fgets($line);
			#echo "<tr><td>cline: $line</td></tr>";	
			$lineidx++;
			$fc = $firstcell;
			$entry = chop(fgets($fh));
			
			for ($i = 2; $i <= $lpe; $i++) {
				#$currline = fgets($fh);
				#echo "<tr><td>line: $currline</td></tr>";
	        		$entry .=  chop(fgets($fh));
			}
			if ($entry == '') {
				continue;
			}
			#echo "<tr><td colspan=3>line $lineidx: <pre>$entry</pre></td></tr>\n";
			$pieces = explode("$sep",$entry);
			//echo " <tr>\n";
			
			// Info from file
			$tableline = '';
			for ($i = 1; $i<=$ncol;$i++) {
				if ($parser['col'.$i.'_def'] == 'discard') {
					continue;
				}
				if ($parser['col'.$i.'_def'] != 'Refine') {
					$cellvalue = $pieces[$i-1];
					if (preg_match("/Size|Start|Stop/i",$parser['col'.$i.'_def'])) {
						$cellvalue= preg_replace('/[\.,]/', '', $cellvalue);
						$cellvalue = number_format($cellvalue,0,'',',');
					}
					$tableline .= "  <td $fc>".$cellvalue."</td>\n";
					$fc = '';
					if ($chrcol == $i) {
						$currchr = $pieces[$i-1];
					}
					elseif ($startcol == $i) {
						$currstart = $pieces[$i-1];
					}
					elseif ($stopcol == $i) {
						$currstop = $pieces[$i-1];
					}
					elseif ($cncol == $i) {
						$currcn = $pieces[$i-1];
					}	
				}
				else {
					$regex = $parser['col'.$i.'_regex'];
					$keep = $parser['col'.$i.'_keep'];
					$keeps = explode(' ',$keep);
					$out = preg_match("/$regex/",$pieces[$i-1],$matches);
					#foreach ($matches as $entry) {
					#	echo "- $entry<br>";
					#}
					foreach($keeps as $idx){
						$tableline .= "  <td $fc>".$matches[$idx]."</td>\n";
						$fc = '';
						if ($chrcol == $i."_".$idx) {
							$currchr = $matches[$idx];
						}
						elseif ($stopcol == $i."_".$idx) {
							$currstop = $matches[$idx];
						}
						elseif ($startcol == $i . '_'.$idx) {
							$currstart = $matches[$idx];
						}
						elseif ($cncol == $i . "_" . $idx) {
							$currcn = $matches[$idx];
						}

					}
				}
			}
			// print line
			echo "<tr context='customrow' chr='$currchr' start='$currstart' stop='$currstop'>\n";
			echo "$tableline";
			echo " </tr>\n";
			flush();
			ob_flush();
			
    		}
		
    		fclose($handle);
		echo "</table>\n";


	#}		

	echo "<p><form action='index.php?page=custom_methods&amp;type=otf' method=POST>";
	echo "<input type=hidden name=step value=3>\n";
	echo "<input type=hidden name=parser value='$parserid'>\n";
	echo "<input type=hidden name=filename value='$filename'>\n";
	echo "<input type=submit class=button name=submit value='Looks Good!'>\n";
	echo "</form>";
	echo "</div>";
	}
}
elseif ($_POST['step'] == 3) {
	$parserid = $_POST['parser'];
	$filename = $_POST['filename'];
	#$filetype = $_FILES['inputfile']['type'];
	$ok = 1;
 	#if (!($filetype == "text/plain") ) {
 	#	echo "<span class=nadruk>Problem:</span> Only .txt and .xml files are allowed. (detected $filetype )<br />";
   	#	$ok = 0;
 	#}
 	#if ($ok == 1) {
	#	$filename = basename( $_FILES['inputfile']['name']);
	#
  	#	if(!move_uploaded_file($_FILES['inputfile']['tmp_name'], "/tmp/$filename") ) {
  	#		$ok = 0;
	#		echo "<span class=nadruk>Problem:</span> Upload to /tmp/$filename failed.<br />";
 	#	}
 	#}
	if ($ok == 1) {
		
		// FILE UPLOADED, LOAD THE PARSER
		$query = mysql_query("SELECT parser FROM parsers WHERE id = $parserid");
		$row = mysql_fetch_array($query);
		$parserline = $row['parser'];
		$parts = explode("|||",$parserline);
		$parser = array();
		foreach ($parts as $item) {
			$subparts = explode('@@@',$item);
			$parser[$subparts[0]] = $subparts[1];
		}
		// SET GLOBAL VARS
		$ncol = $parser['cols'];
		$headers = $parser['headers'];
		$lpe = $parser['lpe'];
		$sep = $parser['sep'];
		// START PROCESSING
		$fh = fopen("/tmp/$filename", "r");
		if ($headers == 1){
			$headline = fgets($fh);
		}
		echo "<div class=sectie>\n";
		echo "<h3>Custom Data Report</h3>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<tr>\n";
		$fc = $firstcell;
		// headers from parsed file
		for ($i = 1; $i<=$ncol; $i++) {
			$def = $parser["col".$i."_def"];
			if ($def == 'Refine') {
				//$regex = $parser['col'.$i.'_regex'];
				$keep = $parser['col'.$i.'_keep'];
				$keepdef = $parser['col'.$i.'_keepdef'];
				$pieces = explode(' ',$keepdef);
				$keeps = explode(' ',$keep);
				$idx = 0;
				foreach($pieces as $item){
					echo "<th $fc>$item</th>\n";
					$keepidx = $keeps[$idx];
					if ($item == 'Chr') {
						$chrcol = $i . '_'. $keepidx;
					}
					elseif ($item == 'Start') {
						$startcol = $i . "_" . $keepidx;
					}
					elseif ($item == 'Stop') {
						$stopcol = $i . "_" . $keepidx;
					}
					elseif ($item == 'CN') {
						$cncol = $i . "_".$keepidx;
					}
					$idx++;	
					$fc = '';
				}
			}
			elseif ($def != 'discard') {
				echo "<th $fc>$def</th>\n";
				$fc = '';
				if ($def == 'Chr') {
					$chrcol = $i;
				}
				elseif ($def == 'Start') {
					$startcol = $i;
				}
				elseif ($def == 'Stop'){
					$stopcol = $i;
				} 
				elseif ($def == 'CN') {
					$cncol = $i;
				}
			}
		}
		// headers for extra information
		// genes
		echo "<th class=topcellalt>#RS</th>\n";
		echo "<th class=topcellalt>Karyo</th>\n";
		#echo "<th class=topcellalt>Controls</th>\n";
		echo "<th class=topcellalt>Cases</th>\n";
		echo "</tr>\n";	
		
		while (!feof($fh)) {
			$fc = $firstcell;
			$entry = chop(fgets($fh));
			
			for ($i = 2; $i <= $lpe; $i++) {
				#$currline = fgets($fh);
				#echo "<tr><td>line: $currline</td></tr>";
	        		$entry .=  chop(fgets($fh));
			}
			if ($entry == '') {
				continue;
			}
			$pieces = explode("$sep",$entry);
			//echo " <tr>\n";
			
			// Info from file
			$tableline = '';
			for ($i = 1; $i<=$ncol;$i++) {
				if ($parser['col'.$i.'_def'] == 'discard') {
					continue;
				}
				if ($parser['col'.$i.'_def'] != 'Refine') {
					$cellvalue = $pieces[$i-1];
					if (preg_match("/Size|Start|Stop/i",$parser['col'.$i.'_def'])) {
						$cellvalue = preg_replace('/[\.,]/', '', $cellvalue);
						$cellvalue = number_format($cellvalue,0,'',',');
					}
					$tableline .= "  <td $fc>".$cellvalue."</td>\n";
					$fc = '';
					if ($chrcol == $i) {
						$currchr = $pieces[$i-1];
					}
					elseif ($startcol == $i) {
						$currstart = $pieces[$i-1];
					}
					elseif ($stopcol == $i) {
						$currstop = $pieces[$i-1];
					}
					elseif ($cncol == $i) {
						$currcn = $pieces[$i-1];
					}	
				}
				else {
					$regex = $parser['col'.$i.'_regex'];
					$keep = $parser['col'.$i.'_keep'];
					$keeps = explode(" ",$keep);
					$out = preg_match("/$regex/",$pieces[$i-1],$matches);
					foreach($keeps as $idx){
						$tableline .= "  <td $fc>".$matches[$idx]."</td>\n";
						$fc = '';
						if ($chrcol == $i."_".$idx) {
							$currchr = $matches[$idx];
						}
						elseif ($stopcol == $i."_".$idx) {
							$currstop = $matches[$idx];
						}
						elseif ($startcol == $i . '_'.$idx) {
							$currstart = $matches[$idx];
						}
						elseif ($cncol == $i . "_" . $idx) {
							$currcn = $matches[$idx];
						}

					}
				}
			}
			if ($currstart == 0 && $currstop == 0) {
				continue;
			}

			// EXTRA INFO
			// Genes
			if ($currchr == 'X') {
				$currchr = 23;
				$currchrtxt = "X";
			}
			elseif ($currchr == 'Y') {
				$currchr = 24;
				$currchrtxt = "Y";
			}
			$currsize = $currstop - $currstart +1;
			$gq = mysql_query("SELECT COUNT(ID) AS aantal FROM genesum WHERE chr = $currchr AND (((start BETWEEN $currstart AND $currstop) OR (stop BETWEEN $currstart AND $currstop)) OR (start <= $currstart AND stop >= $currstop))");
			$row = mysql_fetch_array($gq);
			$nrgenes = $row['aantal'];
			if ($nrgenes > 0) {
				$tableline .= "<td><a href='index.php?page=genes&amp;chr=$currchr&amp;start=$currstart&amp;stop=$currstop' target='_blank'>$nrgenes</a></td>\n";
			}
			else {
				$tableline .= "<td>0</td>\n";
			}	
			// CYTOBAND
			$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$currchr' AND ('$currstart' BETWEEN start AND stop)");
			$cytorow = mysql_fetch_array($cytoquery);
			$cytostart = $cytorow['name'];
			$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$currchr' AND ('$currstop' BETWEEN start AND stop)");
			$cytorow = mysql_fetch_array($cytoquery);
			$cytostop = $cytorow['name'];
			if ($cytostart == $cytostop) {
				$cytoband = "$currchr$cytostart";
			}
			else {
				$cytoband = "$currchr$cytostart-$cytostop";
			}  
			$tableline .= "<td>$cytoband</td>\n";
			// overlap with hapmap controls
			/*$cq = mysql_query("SELECT start, stop FROM consum WHERE chr = $currchr AND cn = $currcn AND (((start BETWEEN $currstart AND $currstop) OR (stop BETWEEN $currstart AND $currstop)) OR (start <= $currstart AND stop >= $currstop))");
			$totalsize = 0;
			while ($row = mysql_fetch_array($cq)) {
				$rstart = $row['start'];
				$rstop = $row['stop'];
				if ($rstart < $currstart) {
					$rstart = $currstart;
				}
				if ($rstop > $currstop ) {
					$rstop = $currstop;
				}
				$totalsize += $rstop - $rstart + 1; 
			}
			$fraction = ($totalsize / $currsize )* 100;
			$fraction = number_format($fraction, 1,'.',',');
			$tableline .= "<td>$fraction %</td>";
			*/
			// DECIPHER ENTRIES
			mysql_select_db("decipher");	
			$query = mysql_query("SELECT ID FROM syndromes WHERE chr = \"$currchr\" AND ((Start >= \"$currstart\" AND Start <= \"$currstop\") OR (End >= \"$currstart\" AND End <= \"$currstop\") OR (Start <= \"$currstart\" AND End >= \"$currstop\")) ");
			$data = mysql_query($query);
			$nrrows = mysql_num_rows($query);
			if ($nrrows > 0) {
				$decipher = "<a class='clear' href='index.php?page=showdecipher&amp;type=syndromes&amp;chr=$currchrtxt&amp;start=$currstart&amp;end=$currstop' target='new'><img src='images/bullets/syndrome.ico' alt=''/></a>";	 
			}
			else {
				$query = mysql_query("SELECT ID FROM patients WHERE chr = \"$currchr\" AND ((Start >= \"$currstart\" AND Start <= \"$currstop\") OR (End >= \"$currstart\" AND End <= \"$currstop\") OR (Start <= \"$currstart\" AND End >= \"$currstop\")) ");
				$nrrows = mysql_num_rows($query);
				if ($nrrows > 0) {
					$decipher = "<a class='clear' href='index.php?page=showdecipher&amp;type=patients&amp;chr=$currchrtxt&amp;start=$currstart&amp;end=$currstop' target='new'><img src='images/bullets/patient.ico' alt=''/></a>";
				}
				else {
					$decipher = "";
				}
			}
			$tableline .= "<td>$decipher</td>";
			mysql_select_db("$db");
			// print line
			echo "<tr context='customrow' chr='$currchr' start='$currstart' stop='$currstop'>\n";
			echo "$tableline";
			echo " </tr>\n";
			flush();
			ob_flush();
			
    		}
		
    		fclose($handle);
		echo "</table>\n";
		echo "</div>\n";


	}	

}
?>
<div id="txtHint">something must appear here</div>
<!-- context menu loading -->
<script type="text/javascript" src="javascripts/custom_context.js"></script>
		
