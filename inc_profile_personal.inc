<?php 
// SOME VARIABLES
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$checked = array("","checked");

//BUILD PAGE
echo "<div class=sectie>\n";
if (isset($_POST['submit'])) {
	$UserName = $_POST['uname'];
	$FirstName = $_POST['firstname'];
	$LastName = $_POST['lastname'];
	$email = $_POST['email'];
	$Affiliation = $_POST['affiliation'];
	if ($Affiliation == 'other') {
		$Affiliation = $_POST['NewAffiliation'];
		$newaffi = 1;
	}
	else {
		$newaffi = 0;
	}
	$password = $_POST['pass'];
	$query = "SELECT id FROM users WHERE id = '$userid' AND password=MD5('$password')";
	$auth  = mysql_query("$query"); 
	$error = "";
	if ($password == "") {
		$error = $error . "<li>Password needed to update settings</li>\n";
	}
	elseif (mysql_num_rows($auth) == 0) {
		$error = $error . "<li>Specified password incorrect for logged in user </li>\n";
	}
	if ($FirstName == "") {
		$error = $error . "<li>First name can not be empty</li>\n";
	}

	if ($LastName == "") {
		$error = $error . "<li>Last name can not be empty</li>\n";
	}
	if ($username == "") {
		$error = $error . "<li>Username can not be empty</li>\n";
	}
	if ($Affiliation == "") {
		$error = $error . "<li>Affiliation can not be empty</li>\n";
	}
	if (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") {
		$error = $error . "<li>You specified an invalid email address<li>\n";
	}
 	if ($error == "") {
		// inserting new affiliation if needed
		if ($newaffi == 1) {
			mysql_query("INSERT INTO affiliation (name) VALUES ('$Affiliation')");
			$affid = mysql_insert_id();
			$Affiliation = $affid;
		}
		//Updating entry
		$query = "UPDATE users SET username='$UserName', FirstName='$FirstName', LastName = '$LastName', Affiliation = '$Affiliation', email = '$email'";
		//echo "<p>$query</p>\n";
		mysql_query($query);
		echo "<h3>Success !</h3>\n";
		echo "<p>Your user details were successfully updated.</p>\n";
		echo "</div>\n";
	}
	else {
		echo "<h3>Incorrect information provided !</h3>\n";
		echo "<p>There were some problems, so nothing has been updated. Please fix them and try again:\n<ol>\n";
		echo "$error";
		echo "</ol>\n";
		echo "</p></div>\n";
	}
}	
else {
	$result = mysql_query("SELECT u.LastName, u.FirstName, u.Affiliation, u.email, u.username, u.motivation, a.name FROM users u JOIN affiliation a ON u.affiliation = a.id WHERE u.id = '$userid'");
	$row = mysql_fetch_array($result);
	$LastName = $row['LastName'];
	$FirstName = $row['FirstName'];
	$Affiliation = $row['name'];
	$email = $row['email'];
	$UserName = $row['username'];
	//$endeavour = $row['endeavour'];
	echo "<h3>User Details</h3>\n";

	echo "<p>You can change all your personal details here. Change them below and press submit. It is always necessary to fill in all fields and to include your current password.</p>\n";
	echo "</div>\n"; 
	
}
// MAIN FORM
echo "<div class=sectie>\n";
echo "<h3>Personal details</h3>\n";
echo "<h4>*: required fields</h4>\n";
echo "<p>\n";
echo "<form action=index.php?page=profile&amp;type=personal method=POST>\n";
echo "<table cellspacing=0 >\n";
echo "<input type=hidden name=userid value=$userid>\n";
echo " <tr>\n  <th $firstcell $topcell[0]>Option</td>\n";
echo "  <th $topcell[1]>Value</td>\n </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0] $firstcell>Username (*)</th>\n";
echo "  <td $tdtype[0]><input type=text name=uname value=\"$UserName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[1] $firstcell>First Name (*)</th>\n";
echo "  <td $tdtype[1]><input type=text name=firstname value=\"$FirstName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0] $firstcell>Last Name (*)</th>\n";
echo "  <td $tdtype[0]><input type=text name=lastname value=\"$LastName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[1] $firstcell>Email (*)</th>\n";
echo "  <td $tdtype[1]><input type=text name=email value=\"$email\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0] $firstcell>Affiliation (*)</th>\n";
echo "  <td $tdtype[0] ><input type=text name=affiliation value=\"$Affiliation\" size=60></td>\n";
echo " </tr>\n";
echo "</table></p>\n";
echo "</div>\n";
/*
echo "<div class=sectie>\n";
echo "<h3>Endeavour preferences</h3>\n";
echo "<p>Specify which prioritization models need to be included when prioritizing CNV's. These settings are used for background processes, and can be readjusted for on-demand prioritizations.</p>\n";
echo "<p>\n";
echo "<table cellspacing=0>\n";
echo " <tr>\n";
echo "  <th $firstcell $topcell[1]>Include</th>\n";
echo "  <th $topcell[1]>Model Name</th>\n";
echo "  <th $topcell[1]>Short Description</th>\n";
echo " </tr>\n";
$switch = 0;
for ($i = 0;$i<=19;$i+=1) {
	$model = $models[$i];
	$description = $descr[$i];
	$incl = substr($endeavour,$i,1);
	echo " <tr>\n";
	echo "  <td $tdtype[$switch] $firstcell><input type=checkbox name=\"$model\" value=1 $checked[$incl]></td>\n";
	echo "  <td $tdtype[$switch] >$model</td>\n";
	echo "  <td $tdtype[$switch] >$description</td>\n";
	echo " </tr>\n";
}
echo "</table>\n";
echo "</p>\n";
echo "</div>\n";
*/
echo "<div class=sectie>\n";
echo "<h3>Double check and submit</h3>\n";
echo "<p>\n";
echo "<table class=clear>\n";
echo "<tr>\n";
echo " <th class=clear>Password (*)</th>\n";
echo " <td class=clear><input type=password name=pass size=40 maxlength=20 /></td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</p><p>\n";
echo "<input type=submit class=button name=submit value=Submit>\n";
echo "</form>\n";
echo "</div>\n";
?>
