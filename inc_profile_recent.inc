<?php
	#echo "userid = $userid<br>\n";
	echo "<div class=sectie>\n";
	echo "<h3>Recent Activities</h3>\n";
	echo "<p>Below is an overview of the most recent things you have done on this page. \n";
	echo "This can be a handy overview when you need fast access to some analysis results, or you placed a lot of regions into the batch primer design. </p>\n";
	echo "</div>\n";
	# Multi-algo CNV-analysis projects
	echo "<div class=sectie>\n";
	echo "<h3>CNV-analyses (Integrated approach)</h3>\n";
	echo "<h4> ... 5 most recent projects started by you</h4>\n";
	$db = "CNVanalysis" . $_SESSION['dbname'];
	mysql_select_db("$db");
	$query = mysql_query("SELECT id, naam, chiptype, created, collection FROM project WHERE userID = '$userid' ORDER BY id DESC LIMIT 5");
	echo "<p><table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Collection</td>\n";
	echo "  <th scope=col class=topcellalt>Nr Samples</td>\n";
	echo "  <th scope=col class=topcellalt>XML File</td>\n";
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo " </tr>\n"; 
	$switch = 0;
	while ($row = mysql_fetch_array($query) ) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		$countquery = mysql_query("SELECT COUNT(idsamp) AS number FROM projsamp WHERE idproj = $id");
		$qres = mysql_fetch_array($countquery);
		$number = $qres['number'];
		//$url = "index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$id&amp;sample=";
		//$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.location='$url'\"";
		echo " <tr >\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch]>$created</td>\n";
		echo "  <td $tdtype[$switch]>$chiptype</td>\n";
		echo "  <td $tdtype[$switch]>$collection</td>\n";
		echo "  <td $tdtype[$switch]>$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"download.php?file=$name"."_multi.xml\">Combined</a> <a href=\"download.php?file=$name"."_QuantiSNP.xml\">QuantiSNP</a> <a href=\"download.php?file=$name"."_PennCNV.xml\">PennCNV</a> <a href=\"download.php?file=$name"."_VanillaICE.xml\">VanillaICE</a></td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$id&amp;sample=\">Combined</a> <a href=\"index.php?page=detailsxml&amp;project=$name"."_QuantiSNP.xml\">QuantiSNP</a> <a href=\"index.php?page=detailsxml&amp;project=$name"."_PennCNV.xml\">PennCNV</a> <a href=\"index.php?page=detailsxml&amp;project=$name"."_VanillaICE.xml\">VanillaICE</a> </td>\n";

		echo " </tr>\n "; 
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table></p>\n";
	echo "</div>\n ";
	# Single-algo CNV-analysis projects
	echo "<div class=sectie>\n";
	echo "<h3>CNV-analyses (Seperate algorithms)</h3>\n";
	echo "<h4> ... 5 most recent projects started by you</h4>\n";
	echo "<p><table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Algorithm</td>\n";
	echo "  <th scope=col class=topcellalt>Nr Samples</td>\n";
	echo "  <th scope=col class=topcellalt>XML File</td>\n";
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo " </tr>\n"; 
	$switch = 0;
	$query = mysql_query("SELECT nm.ID, nm.algo, nm.project, nm.stalen, nm.bookmarks, nm.date, ct.name FROM nonmulti nm JOIN chiptypes ct ON nm.chiptype = ct.ID WHERE nm.userID = '$userid' ORDER BY ID DESC LIMIT 5");
	while ($row = mysql_fetch_array($query) ) {
		$id = $row['ID'];
		$name = $row['project'];
		$chiptype = $row['name'];
		$created = $row['date'];
		$filename = $row['bookmarks'];
		$created = substr($created, 0, strpos($created, "-"));
		$algo = $row['algo'];
		$number = $row['stalen'];
		
		echo " <tr >\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch]>$created</td>\n";
		echo "  <td $tdtype[$switch]>$chiptype</td>\n";
		echo "  <td $tdtype[$switch]>$algo</td>\n";
		echo "  <td $tdtype[$switch]>$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"download.php?file=$filename\">XML</a></td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=detailsxml&amp;project=$filename\">Details</a></td>\n";
		echo " </tr>\n "; 
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table></p>\n";
	echo "</div>\n ";
	
	# PRIMER DESIGN JOBS
	echo "<div class=sectie>\n";
	echo "<h3>qPCR Primerdesign</h3>\n";
	echo "<h4> ... 5 Most recently submitted regions</h4>\n";
	echo "<p><table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Region</td>\n";
	echo "  <th scope=col class=topcellalt>Submitted</td>\n";
	echo "  <th scope=col class=topcellalt>Finished</td>\n";
	echo "  <th scope=col class=topcellalt>Successful</td>\n";
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo " </tr>\n"; 
	$switch = 0;
	mysql_select_db("primerdesign".$_SESSION['dbname']);
	$query = mysql_query("SELECT region, finished, created FROM Succeeded WHERE submitter = '$userid' ORDER BY created DESC LIMIT 5");
	while ($row = mysql_fetch_array($query) ) {
		$region = $row['region'];
		$finished = $row['finished'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, " "));
		$pieces = explode("_",$region);
		$chrtxt = substr($pieces[0],3);
		$chr = $chromhash[ $chrtxt ];
		$start = $pieces[1];
		$end = $pieces[2];
		$regionformat = "Chr$chr:".number_format($start,0,'',',')."-". number_format($end,0,'',',');
		$pquery = mysql_query("SELECT COUNT(ID) AS total FROM Primers WHERE chr = \"$chr\" AND (Start >= \"$start\" AND End <= \"$end\")");
		$success = 0;
		$row = mysql_fetch_array($pquery);
		$numprim = $row['total'];
		if ($numprim > 0) {	
			$success = 1;
		}
		else {
			$success = 0;
		}	
		echo " <tr >\n";
		echo "  <td $tdtype[$switch] $firstcell>$regionformat</td>\n";
		echo "  <td $tdtype[$switch]>$created</td>\n";
		echo "  <td $tdtype[$switch]>". $icons[ $finished ] ."</td>\n";
		echo "  <td $tdtype[$switch]>". $icons[ $success ] ."</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=primerdesign&amp;type=result&amp;region=$region&amp;operator=$username&amp;uid=$userid\">Details</a></td>\n";
		echo " </tr>\n "; 
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table></p>\n";
	echo "</div>\n ";
	
?>
	
