<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inhhash = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

echo "<div class=sectie>\n";
echo "<h3>Extract Filtered results</h3>\n";
echo "<p>Set your preferences below and press submit to extract a .TXT file with the extracted results. This file can be opened in Excel. The tables will contain sample name, chr, region, copynumber, diagnostic class, inheritance, chiptype and any of the items specified below. </p>\n";

echo "<p><form action=index.php?page=profile&type=stats&step=2' method=POST>\n";

// heading
echo "<table cellspacing=0>\n";
echo "<tr><th colspan=3 $firstcell>DETAILS TO SHOW</th></tr>\n";
echo "<tr>\n";
echo " <th class=topcell $firstcell>USE</th>\n";
echo " <th class=topcell>Feature</th>\n";
echo " <th class=topcell>Details</th>\n";
echo "</tr>\n";

// rows
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=size CHECKED></td>\n";
echo " <td NOWRAP>Size</td>\n";
echo " <td>Genomic Size in basepairs</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=maxregion CHECKED></td>\n";
echo " <td NOWRAP>Maximal Region</td>\n";
echo " <td>Show maximal region next to standard (minimal region)</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=maxsize CHECKED></td>\n";
echo " <td NOWRAP>Maximal Size </td>\n";
echo " <td>Genomic Size, based on the maximal region.</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=genes CHECKED></td>\n";
echo " <td NOWRAP>Nr. Genes</td>\n";
echo " <td>Number of Affected RefSeq Genes</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=genelist CHECKED></td>\n";
echo " <td NOWRAP>Gene List</td>\n";
echo " <td>List of Affected Genes by RefSeq Symbol (based on minimal region)</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=clinic CHECKED></td>\n";
echo " <td NOWRAP>Clinical</td>\n";
echo " <td>A combined list of structured clinical data. </td>\n";
echo "</tr>\n";
echo "<tr><th colspan=3 $firstcell >FILTER SETTINGS</th></tr>\n";
echo "<tr>\n";
echo " <th class=topcell $firstcell>USE</th>\n";
echo " <th class=topcell>VALUE</th>\n";
echo " <th class=topcell>ITEM</th>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=class CHECKED></td>\n";
echo " <td NOWRAP><select name=classvalue>";
echo "   	<option value='ANY' selected>All (including non-specified)</option>";
echo "   	<option value='1'>Only Class 1</option>";
echo "		<option value='1,2'>Class 1-2</option>";
echo "		<option value='1,2,3'>Class 1-3</option>";
echo "		<option value='1,2,3,4'>Class 1-4</option>";
echo "</select></td>\n";
echo " <td>Only retain CNVs from the specified diagnostic classes</td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=inheritance CHECKED></td>\n";
echo " <td NOWRAP><select name=inheritancevalue>";
echo "	<option value='0,1,2,3' selected>All (including non-specified)</option>";
echo "	<option value='3'>De Novo</option>";
echo " 	<option value='1,2'>Inherited</option>";
echo " 	<option value='1,2,3'>Any (but specified)</option>";
echo "</select></td>\n";
echo " <td>Only retain CNVs from the specified inheritance types </td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=minsnp CHECKED></td>\n";
echo " <td NOWRAP><input type=text name=minsnpvalue value=3 size=10></td>\n";
echo " <td>Minimal Number of Probes </td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=minsize CHECKED></td>\n";
echo " <td NOWRAP><input type=text name=minsizevalue value=0 </td>\n";
echo " <td>Retain only CNVs larger than the specified size (in basepairs) </td>\n";
echo "</tr>\n";


echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=copynumber CHECKED></td>\n";
echo " <td NOWRAP><select name=copynumbervalue>";
echo "<option value='0,1,2,3,4' selected>Any</option>";
echo "<option value='0,1'>Loss</option>";
echo "<option value='3,4'>Gain</option>";
echo "<option value='1'>Hemizygous loss</option>";
echo "<option value='0'>Homozygous loss</option>";
echo "</select></td>"; 
echo " <td>Retain only CNVS of the specified copynumbers </td>\n";
echo "</tr>\n";

//echo "<tr>\n";
//echo " <td $firstcell><input type=checkbox name= CHECKED></td>\n";
//echo " <td NOWRAP> </td>\n";
//echo " <td> </td>\n";
//echo "</tr>\n";

echo "</table>\n";

echo "</p><p><input type=submit class=button value=Submit ></p></form>\n";

}
?>




