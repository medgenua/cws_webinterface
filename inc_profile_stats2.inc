<?php
/////////////////////////
// CONNECT TO DATABASE //
/////////////////////////
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
// DEFINE SOME VARS
$inharray = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chararray = array('&nbsp;','_','&ccedil;','&amp;','&sect;','&ecirc;', '$','&micro;', '&lt;', '&gt;', '#','&deg;','&pound;');
$newchararray = array(' ','\_','\c{c}','\&','\textsection','ê','\$','$\mu$','\textless', '\textgreater', '\#', '\textdegree','\pounds' );

// POSTED VARIABLES 
// show stuff
$showsize = $_POST['size'] || 0;
$showmaxregion = $_POST['maxregion'] || 0;
$showmaxsize = $_POST['maxsize'] || 0;
$showgenes = $_POST['genes'] || 0;
$showgenelist = $_POST['genelist'] || 0;
// filter stuff
$useclass = $_POST['class'] || 0;
$classvalue = $_POST['classvalue'];
$useinh = $_POST['inheritance'] || 0;
$inhvalue = $_POST['inheritancevalue'];
$usesnp = $_POST['minsnp'] || 0;
$snpvalue = $_POST['minsnpvalue'];
$usesize = $_POST['minsize'] || 0;
$sizevalue = $_POST['minsizevalue'] || 0;
$usecn = $_POST['copynumber'] || 0;
$clinic = $_POST['clinic'] || 0;
$cnvalue = $_POST['copynumbervalue'];
unlink("/var/tmp/reports/Table_$userid.txt");
$fh = fopen("/var/tmp/reports/Table_$userid.txt", 'a');
$header = "SampleID\tChiptype\tChr\tStart\tStop\tRegion\tCopyNumber\tClass\tInheritance";
if ($showsize == 1) {
	$header .= "\tMin.Size";
}
// GET CNVs following filter and permission settings
$extras = '';
$filter = '';
if ($showmaxregion == 1 || $showmaxsize == 1) {
	$extras .= ', a.largestart, a.largestop ';
	if ($showmaxregion == 1) {
		$header .= "\tMax.Region";
	}
	if ($showmaxsize == 1) {
		$header .= "\tMax.Size";
	} 
}
if ($showgenes == 1 || $showgenelist == 1) {
	$extras .= ", a.nrgenes";
	if ($showgenes == 1) {
		$header .= "\tNr.Genes";
	}
	if ($showgenelist == 1) {
		$header .= "\tGene List";
	}
}
if ($clinic == 1) {
	$header .= "\tClinical Synopsis";
	$extras .= ", s.id";
}
if ($useclass == 1) {
	if ($classvalue != 'ANY') {
		$filter .= "AND a.class IN ($classvalue) ";
	}
}
if ($useinh == 1) {
	$filter .= "AND a.inheritance IN ($inhvalue) ";
}
if ($usesnp == 1) {
	$filter .= "AND a.nrsnps >= $snpvalue ";
}
if ($usecn == 1) {
	$filter .= "AND a.cn IN ($cnvalue) ";
}	
$querystring = "SELECT s.chip_dnanr, a.chr, a.start, a.stop, a.cn, p.chiptype, a.class, a.inheritance $extras FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id WHERE pp.userid = '$userid' AND p.collection <> 'Controls' AND s.intrack = 1 AND s.trackfromproject = p.id $filter ORDER BY s.chip_dnanr, chr ASC, start ASC ";
// STart output 
echo "<div class=sectie>\n";
echo "<h3>Creating Overview file </h3>\n";
echo "<h4>status...</h4>\n";
echo "<ul id=ul-disc>\n";
echo "<li> Fetching results\n";
ob_flush();
flush();
$header .= "\n";
fwrite($fh,$header);
$query = mysql_query($querystring);
// PRINT EXPERIMENTAL DATAILS ON SAMPLE / PROJECT
$totalcnv = mysql_num_rows($query);
echo "<li> Processing $totalcnv results</li>";
ob_flush();
flush();
$phenos = array();
while ($row = mysql_fetch_array($query)) {
	$line = '';
	$sample = $row['chip_dnanr'];
	$chr = $row['chr'];
	$chrtxt = $chromhash[$chr];
	$start = $row['start'];
	$stop = $row['stop'];
	$size = $stop - $start +1 ;
	$cn = $row['cn'];
	$chiptype = $row['chiptype'];
	if ($clinic == 1) {
		$sid = $row['id'];
		// get phenos
		if (array_key_exists($sid,$phenos)) {
			$phenostring = $phenos[$sid];
		}
		else {
			$pheno = array();
			$pquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid'");
			while ($prow = mysql_fetch_array($pquery)) {
				$pheno[] = $prow['lddbcode'];
			}
			mysql_select_db('LNDB');
			$phenostring = '';
			foreach ($pheno as $key => $code) {
				$pquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
				$prow = mysql_fetch_array($pquery);
				$phenostring .= $prow['LABEL'].";";
			}
			if ($phenostring != ''){
				$phenostring = substr($phenostring, 0, -1);
			}
			$phenos[$sid] = $phenostring;
			mysql_select_db($db);
		}
		$phenostring = "\t$phenostring";
	}
	else {
		$phenostring = '';
	}
	$inh = $row['inheritance'];
	$class = $row['class'];
	$line = "$sample\t$chiptype\t$chrtxt\t$start\t$stop\tchr$chrtxt:$start-$stop\t$cn\t$class\t". $inharray[$inharray[$inh]];
	if ($showsize == 1) {
		$line .= "\t$size";
	}
	if ($showmaxregion == 1 || $showmaxsize == 1) {
		$largestart = $row['largestart'];
		$largestop = $row['largestop'];

		if ($largestart == 'ter') {
			$largestartvalue = 0;
		}
		else {
			$largestartvalue = $largestart;
		}
		if ($largestop == 'ter') {
			$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$cendrow = mysql_fetch_array($cendquery);
			$largestopvalue = $cendrow['stop'];
		}
		else {
			$largestopvalue = $largestop;
		}
		$largesize = $largestopvalue - $largestartvalue +1;

		if ($showmaxregion == 1) {
			$line .= "\tchr$chrtxt:$largestart-$largestop";
		}
		if ($showmaxsize == 1) {
			$line .= "\t$largesize";
		} 
	}
	if ($showgenes == 1) {
		$nrgenes = $row['nrgenes'];
		$line .= "\t$nrgenes";
	}
	if ($showgenelist == 1) {
		$nrgenes = $row['nrgenes'];
		$geneline = '';
		if ($nrgenes > 0) {
			$gquery = mysql_query("SELECT symbol FROM genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");	
			$numgenes = mysql_num_rows($gquery);
			while ($grow = mysql_fetch_array($gquery)) {
				$symbol = $grow['symbol'];
				$geneline .= "$symbol,";
			}
			$geneline = substr($geneline,0,-1);
		}
		$line .= "\t$geneline";
		
	}
	$line .= $phenostring;
	$line .= "\n";
	fwrite($fh,$line);
} 
echo "<li>Done, providing file</li>";
echo "</ul>";
echo "</p><p> <a href=\"index.php?page=profile&amp;type=stats\">Go Back</a>\n";
fclose($fh);
#copy("/tmp/reports/Table_$userid.txt", "$sitedir/reports/Table_$userid.txt");
ob_flush();
flush();

echo "<iframe height='0' width='0' src='download.php?path=reports&file=Table_$userid.txt'></iframe>\n";
?>

</div>
