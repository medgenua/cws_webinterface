<?php
$switch = 0;
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
#$userquery = mysql_query("SELECT id, LastName, FirstName FROM users");
#$userselect = "    <option value=\"\" selected></option>\n    ";
#while ($row = mysql_fetch_array($userquery)) {
#	$id = $row['id'];
#	$fname = $row['FirstName'];
#	$lname = $row['LastName'];
#	$userselect = $userselect . "    <option value=". $row['id'] . ">".$row['FirstName'] . " " . $row['LastName'] . "</option>\n";
#}	

# INTEGRATED, created, set permissions, or delete
echo "<div class=sectie>\n";
if ($_GET['result'] == 1) {
	echo "<h3>Permissions : Update Successful !</h3>\n";
	echo "<h4>... permissions successfully updated</h4>\n";
}
elseif ($_GET['result'] == 2) {
	echo "<h3>Project successfully deleted</h3>\n";
}

else {
	echo "<h3>Permissions</h3>\n";
	echo "<h4>... of projects you have started</h4>\n";
}
$query = mysql_query("SELECT id, naam, chiptype, created, collection FROM project WHERE userID = '$userid' ORDER BY id DESC");
if (mysql_num_rows($query) == 0) {
	echo "<p>You have not yet started any CNV-projects</p>\n";
}
else {
	echo "<p>Below is the list of projects you have started on this server. From here, you can allow other users to see and browse your results, join or delete projects.\n";
	//echo " Just select the user from the dropdown list \"Give Access\" for the project of interest, and click \"Submit\". Follow the same procedure to cancel access by selecting for the \"Remove Access\" column. </p>\n";
	echo "<p>\n";
	//echo "<p> If you want to delete a project, click the 'delete' button on the right side of the row.</p>";
	echo "<form action=change_permissions.php method=POST>\n";
	echo "<input type=hidden name=uid value='$userid'>\n";
	echo "<table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</th>\n";
	//echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</th>\n";
	echo "  <th scope=col class=topcellalt>Collection</th>\n";
	echo "  <th scope=col class=topcellalt>Share</th>\n";
	//echo "  <th scope=col class=topcellalt>Remove Access</th>\n";
	echo "  <th scope=col class=topcellalt>Delete</th>\n";
	echo "  <th scope=col class=topcellalt>Join</th>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$chiptype = preg_replace('/(Human)(.*)/',"$2",$chiptype);
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		//$deloptions = "    <option value=\"\"></option>\n";
		//$subquery = mysql_query("SELECT u.id, u.LastName, u.FirstName FROM users u JOIN projectpermission p ON u.id = p.userid WHERE p.projectid = '$id'");
		//while ($subrow = mysql_fetch_array($subquery) ) {
		//	$deloptions =  $deloptions . "    <option value=".$subrow['id'].">".$subrow['FirstName']." ".$subrow['LastName']."</option>\n";
		//}
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		//echo "  <td $tdtype[$switch] >$created</td>\n";
		echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		//echo "  <td $tdtype[$switch] ><select name=add_$id>$userselect</select> </td>\n";
		//echo "  <td $tdtype[$switch] ><select name=del_$id>$deloptions</select> </td>\n";
		echo" <td $tdtype[$switch]><a href='index.php?page=shareprojects&pid=$id' target='_blank'>Share</a></td>\n";
		echo "  <td $tdtype[$swich] ><input type='checkbox' name='deletearray[]' value=$id></td>\n";
		echo "<td $tdtype[$switch] ><input type='checkbox' name='joinarray[]' value=$id></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
	echo "<p><input type=submit class=button value='Delete Selected Projects' name=delete></p>\n";
	echo "<p><input type=submit class=button value='Join Selected Projects' name=join></p>\n";
	echo "</form>\n";
}
echo "</div>\n"; 

# INTEGRATED, access to see
echo "<div class=sectie>\n";
echo "<h3>Integrated Analysis: Overview</h3>\n";
echo "<h4>Of all projects you are entitled to see</h4>\n";
$pquery = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection, p.userID FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY id DESC");
if (mysql_num_rows($pquery) == 0) {
	echo "<p>You don't have access to any project.</p>\n";
}
else {
	echo "<p>\n";
	echo "These are all the projects you have access to.  Click on 'Details' for further exploration</p>\n";
	echo "<p>\n";
	echo "<table cellspacing=0>\n";
	$switch = 0;
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Collection</td>\n";
	echo "  <th scope=col class=topcellalt>Samples</td>\n";	
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo "  <th scope=col class=topcellalt>Recreate XML</td>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($pquery)) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$chiptype = preg_replace('/(Human)(.*)/',"$2",$chiptype);

		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		$owner = $row['userID'];
		$countquery = mysql_query("SELECT COUNT(idsamp) AS number FROM projsamp WHERE idproj = $id");
		$qres = mysql_fetch_array($countquery);
		$number = $qres['number'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		echo "  <td $tdtype[$switch] >$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$id&amp;sample=\">Details</a></td>\n";
		echo "  <td $tdtype[$switch]><a href='index.php?page=create_custom_bookmark&amp;p=$id'>XML</a></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
}
echo "</div>\n";

// SINGLE-ALGO, created

echo "<div class=sectie>\n";
echo "<h3>Single Algorithm: Overview</h3>\n";
echo "<h4>Of all projects you have started</h4>\n";
#$query = mysql_query("SELECT ID, algo, project, stalen, bookmarks, date, chiptype FROM nonmulti WHERE userID = '$userid' ORDER BY id DESC");
$query = mysql_query("SELECT nm.ID, nm.algo, nm.project, nm.stalen, nm.bookmarks, nm.date, ct.name FROM nonmulti nm JOIN chiptypes ct ON nm.chiptype = ct.ID WHERE nm.userID = '$userid' ORDER BY ID DESC ");
if (mysql_num_rows($query) == 0) {
	echo "<p>You didn't start any projects yet.</p>\n";
}
else {
	$switch = 0;
	echo "<p>\n";
	echo "These are all the single-algorithm projects you have created.  Click on 'Details' for further exploration, or on 'XML' to download the bookmark file.</p>\n";
	echo "<p>\n";
	echo "<table cellspacing=0>\n";
	$switch = 0;
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Algorithm</td>\n";
	echo "  <th scope=col class=topcellalt>Samples</td>\n";
	echo "  <th scope=col class=topcellalt>XML File</td>\n";	
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($query)) {
		$id = $row['ID'];
		$name = $row['project'];
		$chiptype = $row['name'];
		$created = $row['date'];
		$created = substr($created, 0, strpos($created, "-"));
		$algo = $row['algo'];
		$filename = $row['bookmarks'];
		$number = $row['stalen'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$algo</td>\n";
		echo "  <td $tdtype[$switch] >$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"download.php?file=$filename\">XML</a></td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=detailsxml&amp;project=$filename\">Details</a></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
}
echo "</div>\n";
	


?>
