<?php
function GetConnection()
{
    global $config;
    $mysqli = new mysqli($config['DBHOST'], $config['DBUSER'], $config['DBPASS'], $config['DB']);
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
        trigger_error("Failed to connect to MySQL: " . $mysqli->connect_error, E_USER_ERROR);
        exit();
    }
    return $mysqli;
}


// escape values for use in SQL
function SqlEscapeValues($data) {
    $mysqli = GetConnection();
    if (is_array($data)) {
        for ($i = 0; $i < count($data); $i = $i + 1) {
            $data[$i] = $mysqli->real_escape_string($data[$i]);
        }
    }
    else {
        $data = $mysqli->real_escape_string($data);
    }
    return($data);
}

// run query without feedback (create / drop /truncate)
function doQuery($query)
{
    $mysqli = GetConnection();
    // run query
    $result = $mysqli->query($query);
    if (!$result) {
        trigger_error($mysqli->error,E_USER_ERROR);
        return false;
    }
    return true;
}

// insert query : return insert id
function insertQuery($query) {
    $mysqli = GetConnection();
    $result = $mysqli->query($query);
    if (!$result) {
        trigger_error($mysqli->error,E_USER_ERROR);
        return false;
    }
    $id = $mysqli->insert_id;
    return ($id);
}

function runQuery($query) {
    $mysqli = GetConnection();
    $sth = $mysqli->query($query);
    if (!$sth) {
        trigger_error($mysqli->error,E_USER_ERROR);
        return array();
    }
    if ($sth->num_rows == 1) {
        $result = $sth->fetch_array();
    } 
    elseif ($sth->num_rows == 0) {
        $result = array();
    } 
    else {
        $result = array();
        while ($row = $sth->fetch_array()) {
            array_push($result, $row);
        }
    }
    return ($result);
}

// create AoA from array in case of a single result
function OneToMulti($a) {
    $result = array();
    if (empty($a)) {
        return ($a);
    }
    array_push($result, $a);
    return ($result);
}