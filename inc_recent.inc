<?php
// FUNCTIONS 
function LoadFiles($dir)
{
 $Files = array();
 $It =  opendir($dir);
 if (! $It)
  die('Cannot list files for ' . $dir);
 while ($Filename = readdir($It))
 {
  if ($Filename == '.' || $Filename == '..')
   continue;
  $LastModified = filemtime($dir . $Filename);
    $Files[] = array($Filename, $LastModified);
 }
 return $Files;
}

function DateCmp($a, $b)
{
  return ($a[1] < $b[1]) ? 1 : 0;
}

function SortByDate(&$Files)
{
  usort($Files, 'DateCmp');
}

// CONTENT
if ($loggedin == 1) {
	echo "<h3>Recent Projects</h3>\n";
	echo "<h4>... Click to download XML</h4>\n";

	//Set number to show
	$num = 10;

	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");


	$Files = LoadFiles('bookmarks/');
	SortByDate($Files);
	$nrfiles = count($Files) - 1;
	echo "<ul id=\"ul-attach\">\n";
	$i = 0;
	$j = 0;
	while ($i <= $num && $j <= $nrfiles) {
		$filename = $Files[$j][0];
		$pname = preg_replace('/(.+)_(multi|PennCNV|QuantiSNP|VanillaICE).xml/',"$1",$filename);
		$query = mysql_query("SELECT p.id FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE p.naam = '$pname' AND pp.userid = $userid");
		if (mysql_num_rows($query) > 0) {
			$firstpart = substr($filename,0,14) . "...";
  			echo "<li><a href=\"bookmarks/" ;
  			echo $Files[$j][0];
  			echo "\" title=\"";
  			echo $Files[$j][0];
  			echo "\">$firstpart</a></li>\n";
			$i = $i+1;
		}
		else {
			// check nonmulti database
			$subquery = mysql_query("SELECT ID FROM nonmulti WHERE project = '$pname' AND userID = '$userid'");
			if (mysql_num_rows($subquery) > 0) {
				$firstpart = substr($Files[$j][0],0,14) . "...";
  				//echo the link html
  				echo "<li><a href=\"bookmarks/" ;
  				echo $Files[$j][0];
  				echo "\" title=\"";
  				echo $Files[$j][0];
  				echo "\">$firstpart</a></li>\n";
				$i = $i+1;
			}
		}
  		$j = $j+1;	
	}
	echo "</ul>\n"; 
}
else {
	echo "<h3>Recent Projects</h3>\n";
	echo "<h4>... Log in for access</h4>\n";
	echo "<p></p>";
}

?>
