<?php
echo "<div class=sectie>\n";
echo "<h3>Parameters for Multi-Algorithm CNV-Analysis</h3>\n";
echo "<p>\n";
echo "This page is temporarily disabled. Settings are displayed at the analysis setup page ==><a href='index.php?page=run&amp;type=multiple'>here <==</a>";
echo "</p>";
echo "</div>";
exit();
// 
// WRITE OUT CHANGES
if ($level > 1 && $_POST['changeit']) {
	echo "<p>The following parameters are used by default in the combined analysis of array data.</p>\n";
	$fh = fopen('files/parameters.txt', "w");
	foreach ($_POST as $key => $value) {
		if ($key != "changeit") {
			fwrite($fh,"$key=$value\n");
		}
	}
	fclose($fh);
}


////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$algos = array('Q' => "QuantiSNP", 'P' => "PennCNV", 'V' => "VanillaICE", 'A' => "All");

// READ IN CURRENT VALUES
$fh = fopen('files/parameters.txt', "r");
while (!feof($fh) ){
	$line = fgets($fh);
	if ($line != ""){
		$parts = explode("=",$line);
		$params[ $parts[0] ] = $parts[1];
	}
}
fclose($fh);

if ($level > 1) {
	echo "<p>You can change these values here.  However, you should be very carefull as it will change the default values for all analyses run from that time on !</p>\n";
	echo "<form METHOD=POST action=\"index.php?page=parameters\">\n";
	echo "<p><table cellspacing=0 >\n";
	echo " <tr>\n"; 
	echo "  <th scope=col class=topcellalt $firstcell>Algorithm</th>\n";
	echo "  <th scope=col class=topcellalt>Parameter</th>\n";
	echo "  <th scope=col class=topcellalt>Current Value</th>\n";
	echo "  <th scope=col class=topcellalt>New Value</th>\n";
	echo " </tr>\n";

	$switch=0;
	foreach ($params as $key => $value) {
		$algo = $algos[ substr($key, 0, 1) ] ;
		$param = substr($key, 1);
		$string = "";
		$string .= "<tr>\n";
		$string .= "  <td $tdtype[$switch] $firstcell>$algo</td>\n";
		$string .= "  <td $tdtype[$switch]>$param</td>\n";
		$string .= "  <td $tdtype[$switch]>$value</td>\n";
		$string .= "  <td $tdtype[$switch]><input type=text name=$key value=$value></td>\n";
		$string .= "</tr>\n";
		echo $string;
		$switch = $switch + pow(-1,$switch);
	}  
	echo "</table></p>\n"; 
	
	echo "<p><input type=submit value=\"Change Default Values\" class=button name=changeit ></form></p>\n";
}
else {
	echo "<p><table cellspacing=0 >\n";
	echo " <tr>\n"; 
	echo "  <th scope=col class=topcellalt $firstcell>Algorithm</th>\n";
	echo "  <th scope=col class=topcellalt>Parameter</th>\n";
	echo "  <th scope=col class=topcellalt>Current Value</th>\n";
	echo " </tr>\n";

	$switch=0;
	foreach ($params as $key => $value) {
		$algo = $algos[ substr($key, 0, 1) ] ;
		$param = substr($key, 1);
		$string = "";
		$string .= "<tr>\n";
		$string .= "  <td $tdtype[$switch] $firstcell>$algo</td>\n";
		$string .= "  <td $tdtype[$switch]>$param</td>\n";
		$string .= "  <td $tdtype[$switch]>$value</td>\n";
		$string .= "</tr>\n";
		echo $string;
		$switch = $switch + pow(-1,$switch);
	}  
	echo "</table></p>\n"; 
}

echo "</div>\n";	
?>
