<?php
// VARS & ARRAYS
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$checks = array("0" => "", "1" => "CHECKED", '' => '');

// FETCH personal SETTINGS (from session)
$RefSeq = $_SESSION['RefSeq'];
$GCcode = $_SESSION['GCcode'];
$GCncode = $_SESSION['GCncode'];
$SegDup = $_SESSION['SegDup'];
$HapSame = $_SESSION['HapSame'];
$HapAll = $_SESSION['HapAll'];
$DGVshow = $_SESSION['DGVshow'];
$DGVinclude = $_SESSION['DGVinclude'];
$Condense = $_SESSION['Condense'];
$ToCondense = $_SESSION['ToCondense'];
$NullCondense = $_SESSION['NullCondense'];
$annotations = $_SESSION['annotations'];
$annotations = explode(",",$annotations);
$DGVincludes = explode(",",$DGVinclude); 
$DGVhash = array();
$db_links = $_SESSION['db_links'];
foreach ($DGVincludes as $key => $value){
	$DGVhash[$value] = 1;
}
$LOHshow = $_SESSION['LOHshow'];
$LOHminsize = $_SESSION['LOHminsize'];
$LOHminsnps = $_SESSION['LOHminsnp'];
$ecaruca = $_SESSION['ecaruca'];
$ecarucaSize = $_SESSION['ecarucaSize'];
$DECsyn = $_SESSION['DECsyn'];
$DECpat = $_SESSION['DECpat'];

$subtype = '';
if (isset($_POST['st'])) {
	$subtype = $_POST['st'];
}
if ($subtype == '' && isset($_GET['st'])) {
	$subtype = $_GET['st'];
}
if ($subtype == '') {
	$subtype = 'res';
}
// INTRO
echo "<div class=sectie>\n";
echo "<h3>Graphical Overview settings</h3>\n";
echo "<p>The graphical browser used to present search or analysis results can be slightly personalised. Using the forms below you can include one or more control, gene or annotation tracks. This page is accessible by clicking on the icon in the top right corner of the graphical overview.</p>\n";
echo "</div>\n";


echo "<div class=sectie>\n";
echo "<form action='index.php?page=setplotprefs' method=POST>";
echo "<input type=hidden name=st value=$subtype>\n";

if ($subtype == 'res') {
	/////////////
	// RESULTS //
	/////////////
	echo "<h3>CNV results</h3>\n";
	echo "<p>Specify how CNV results should be organised in the graphical overviews.Excluding regions by filtering here also influences tabular overiews and created reports.</p><p>\n";
	echo "<table cellspacing=0>\n";
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>LOH-regions</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='showloh' ".$checks[$LOHshow]."></td>";
	echo " <td NOWRAP>Show LOH Regions (CN2 regions on X for Males are still shown)</td>";
	echo " <td colspan=3>Show Regions of decreased heterozygosity rate. These regions are called by VanillaICE</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=text size=8 name=minsizeloh value='$LOHminsize'></td>";
	echo " <td NOWRAP>Minimal LOH Size</td>";
	echo " <td>Specify the minimal LOH region size to be reported (in basepairs)</td>";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell><input type=text size=8 name=minsnpsloh value='$LOHminsnps'></td>";
	echo " <td NOWRAP>Min. nrSNPS in LOH</td>";
	echo " <td>Specify the minial number of probes a LOH region should contain to be reported</td>";
	echo "</tr>";
	echo "<tr>\n";
	echo "<td $firstcell><input type=checkbox name=condense ".$checks[$Condense] ."></td>\n";
	echo "<td NOWRAP>Collapse CNVs</td>\n";
	echo "<td>Collapse CNVs from the diagnostic classes specified below into a single line per copy number state.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	if ($ToCondense == 0) {
		$ToCondense = '';
	}
	echo "<td $firstcell><input type=text name=tocondense size=8 value='$ToCondense'></td>\n";
	echo "<td NOWRAP>Classes to collapse</td>\n";
	echo "<td>The diagnostic classes specified here will be bundled into a condensed track. Seperate by comma.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td $firstcell><input type=checkbox name=nullcondense ". $checks[$NullCondense]."></td>\n";
	echo "<td NOWRAP>Collapse NA class</td>\n";
	echo "<td>Include CNVs without a specified diagnostic class in the bundled condensed track.</td>\n";
	echo "</tr>\n";



	echo "</table></p>";
}
elseif ($subtype == 'ann') {
	////////////////
	// ANNOTATION //
	////////////////
	echo "<h3>Annotations</h3>\n";
	echo "<p>Select the tracks to be included in the graphical overviews, containing various annotation information.</p><p>";
	echo "<table cellspacing=0>\n";

	// Genes
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>Genes</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='RefSeq' ".$checks[$RefSeq]."></td>";
	echo " <td NOWRAP>RefSeq/OMIM</td>";
	echo " <td colspan=3>Shows genes listed in RefSeq database. When OMIM/MORBID information is available, it is shown by color codes: Blue for OMIM entries and red for MORBID entries</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='GCcode' ".$checks[$GCcode]."></td>";
	echo " <td NOWRAP>ENCODE Coding</td>";
	echo " <td colspan=3>Shows Coding transcripts from the ENCODE project. Annotation Level is indicated by color: black for Validated, dark gray for Manual and light gray for automatic annotation.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='GCncode' ".$checks[$GCncode]."></td>";
	echo " <td NOWRAP>ENCODE Non-Coding</td>";
	echo " <td colspan=3>Shows Non-Coding transcripts from the ENCODE project. Annotation Level is indicated by color: black for Validated, dark gray for Manual and light gray for automatic annotation.</td>\n";
	echo "</tr>\n";
	// segdups & repeats
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>Segmental Duplications</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='SegDup' ".$checks[$SegDup]."></td>";
	echo " <td NOWRAP>SegDups</td>";
	echo " <td colspan=3>Show Segmental Duplication Regions. Regions are summarised by single barplots. Hovering will show on which chromosomes a duplication is found, with the corresponding similarity</td>\n";
	echo "</tr>\n";


	// CASES
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>Published Cases</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='DECsyn' ".$checks[$DECsyn]."></td>";
	echo " <td NOWRAP>DECIPHER Syndromes</td>";
	echo " <td colspan=3>Show DECIPHER Syndromes. These are annotated genomic regions for which deletions and/or duplications are associated with specific phenotypes.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='DECpat' ".$checks[$DECpat]."></td>";
	echo " <td NOWRAP>DECIPHER Patients</td>";
	echo " <td colspan=3>Show DECIPHER Patients. Patient reports submitted to DECIPHER are accessible to the public. For full access, request a DECIPHER account and visit the presented links to the DECIPHER pages</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='ecaruca' ".$checks[$ecaruca]."></td>";
	echo " <td NOWRAP>Ecaruca Patients</td>";
	echo " <td colspan=3>Show Ecaruca Patients. Full patient reports submitted to ECARUCA are accessible after logging in to the ECARUCA home page.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=text size=10 name='ecarucaSize' value='$ecarucaSize'></td>";
	echo " <td NOWRAP>Ecaruca Max Region Size</td>";
	echo " <td colspan=3>By default only regions from ECARUCA smaller than 30Mb will be shown.  To limit results to even smaller regions, specify the upper size limit in basepairs here.</td>";
	echo "</tr>";

	// USER ADDED ANNOTATIONS
	if ($level == 3) {
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>User Provided Annotations<span style='float:right;'><a href='index.php?page=add_annotation' style='background-color: #e3e3e6;font-size:8px'>(New)</a></span></th>\n";
	echo "</tr>\n";
	# check private user annotations
	$query = mysql_query("SELECT id, name, description, lastupdate FROM annotationDetails WHERE owner = '$userid' AND public = '0'");
	if (mysql_num_rows($query) > 0) {
		echo "<tr><th $firstcell class=topcellalt colspan=5>Your Private Annotation Sets</td></tr>";
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$name = $row['name'];
			$descr = $row['description'];
			#$usedby = explode(",",$row['usedby']);
			$lastupdate = $row['lastupdate'];
			if (in_array($userid,$annotations)) {
				$checked = 'CHECKED';
			}
			else {
				$checked = '';
			}
			echo "<tr><td $firstcell><input type=checkbox name='userAnn[]' value='$id' $checked></td>";
			echo "<td>$name</td><td colspan=3>$descr ($lastupdate)</td>";
		}

	}

	# check public annotations from current user
	$query = mysql_query("SELECT id, name, description, lastupdate FROM annotationDetails WHERE owner = '$userid' AND public = '1'");
	if (mysql_num_rows($query) > 0) {
		echo "<tr><th $firstcell class=topcellalt colspan=5>Your Public Annotation Sets</td></tr>";
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$name = $row['name'];
			$descr = $row['description'];
			#$usedby = explode(",",$row['usedby']);
			$lastupdate = $row['lastupdate'];
			if (in_array($userid,$annotations)) {
				$checked = 'CHECKED';
			}
			else {
				$checked = '';
			}
			echo "<tr><td $firstcell><input type=checkbox name='userAnn[]' value='$id' $checked></td>";
			echo "<td>$name</td><td colspan=3>$descr ($lastupdate)</td>";
		}

	}

	# check public annotations from other users
	$query = mysql_query("SELECT id, name, description, lastupdate FROM annotationDetails WHERE owner = '$userid' AND public = '1'");
	if (mysql_num_rows($query) > 0) {
		echo "<tr><th $firstcell class=topcellalt colspan=5>Your Public Annotation Sets</td></tr>";
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$name = $row['name'];
			$descr = $row['description'];
			#$usedby = explode(",",$row['usedby']);
			$lastupdate = $row['lastupdate'];
			if (in_array($userid,$annotations)) {
				$checked = 'CHECKED';
			}
			else {
				$checked = '';
			}
			echo "<tr><td $firstcell><input type=checkbox name='userAnn[]' value='$id' $checked></td>";
			echo "<td>$name</td><td colspan=3>$descr ($lastupdate)</td>";
		}

	}

	}

	echo "</table></p>\n";

}
elseif ($subtype == 'con') {
	//////////////
	// CONTROLS //
	//////////////
 
	echo "<h3>Controls</h3>\n";
	echo "<p>Select the control datasets to be included in the plots. For the DGV, a selection can be made on a per publication basis.</p><p>";
	echo "<table cellspacing=0>\n";
	// hapmap
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>HapMap Results</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='HapSame' ".$checks[$HapSame]."></td>";
	echo " <td >HapMap Same Chip</td>";
	echo " <td colspan=3>This track shows CNVs detected in the HapMap Datasets provided by Illumina, using the chiptypes used for the samples you are currently viewing.</td>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name='HapAll' ".$checks[$HapAll]."></td>";
	echo " <td >HapMap All Chips</td>";
	echo " <td colspan=3>This track shows CNVs detected in the HapMap Datasets provided by Illumina, using any of the supported chiptypes. </td>\n";
	echo "</tr>\n";
	// database of genomic variants
	echo "<tr>\n";
	echo " <th colspan=5 $firstcell>Toronto Database Of Genomic Variants</th>\n";
	//echo " <th colspan=2>Show: <input type=checkbox name='DGV_show' ". $checks[$DGVshow] ."></th>\n";
	echo "</tr>\n";
	$query = mysql_query("SELECT id, pubid, paper, method, samples, gain, loss, inversion FROM DGV_studies ORDER BY paper");
	echo "<tr>";
	echo "<th class=topcellalt $firstcell>USE</th>\n";
	echo "<th class=topcellalt>Study</th>\n";
	echo "<th class=topcellalt>Method</th>\n";
	echo "<th class=topcellalt>Samples</th>\n";
	echo "<th class=topcellalt>Entries</th>\n";
	echo "</tr>";

	while ($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$pubid = $row['pubid'];
		$paper = $row['paper'];
		$method = $row['method'];
		$samples = $row['samples'];
		$gain = $row['gain'];
		$loss = $row['loss'];
		$inversion = $row['inversion'];
		$total = $gain + $loss + $inversion;
		echo "<tr>\n";
		echo " <td $firstcell><input type=checkbox name='DGVinc[]' value='$id' ". $checks[$DGVhash[$id] ]."></td>";
		echo " <td NOWRAP><a href='http://www.ncbi.nlm.nih.gov/pubmed/$pubid' target='_blank'>$paper</a></td>";
		echo " <td>$method</td>";
		echo " <td>$samples</td>";
			echo " <td>$total</td>\n";
		echo "</tr>\n";
	}	
	echo "</tr>\n";

	echo "</table>";
}
########################
## PUBLIC ANNOTATIONS ##
########################
if ($subtype == 'publ') {
		$linkchecks = array();
		$db_links = explode(',',$db_links);
		foreach($db_links as $dbid) {
			$linkchecks[$dbid] = 1;	
		}
		echo "<h3>Public Databases</h3>\n";
		echo "<p>The following public databases are available for querying directly from context information on this platform.The links are presented for example on the context menu for genes.</p><p>";
		echo "<table cellspacing=0>\n";
		// By Symbol
		echo "<tr>\n";
		echo " <th colspan=5 $firstcell>Queries by Gene or Transcript Symbol<span style='float:right;'><a href='index.php?page=add_dblink&a=s' style='background-color: #e3e3e6;font-size:8px'>(New)</a></span></th>\n";
		echo "</tr>\n";
		$query = mysql_query("SELECT id, Resource, Description FROM db_links WHERE type = 1");
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$name = $row['Resource'];
			$descr = $row['Description'];
			$sel = $linkchecks[$id] || 0;	
			echo "<tr>\n";
			echo " <td $firstcell><input type=checkbox name='dblinks[]' value='$id' ".$checks[$sel]."></td>";
			echo " <td NOWRAP>$name</td>";
			echo " <td colspan=3>$descr</td>\n";
			echo "</tr>\n";
	
		}
		// By Region
		echo "<tr>\n";
		echo " <th colspan=5 $firstcell>Queries by Genomic Region<span style='float:right;'><a href='index.php?page=add_dblink&a=r' style='background-color: #e3e3e6;font-size:8px'>(New)</a></span></th>\n";
		echo "</tr>\n";
		$query = mysql_query("SELECT id, Resource, Description FROM db_links WHERE type = 2");
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$name = $row['Resource'];
			$descr = $row['Description'];
			$sel = $linkchecks[$id] || 0;	
			echo "<tr>\n";
			echo " <td $firstcell><input type=checkbox name='dblinks[]' value='$id' ".$checks[$sel]."></td>";
			echo " <td NOWRAP>$name</td>";
			echo " <td colspan=3>$descr</td>\n";
			echo "</tr>\n";
	
		}			
		echo "</table></p>\n";
}
echo "<input type=hidden name=uid value='$userid'></p>\n";
echo "<p><input type=submit class=button name='submit' value='Submit'>\n";
echo "</form>";
?>
