<?php 
// SOME VARIABLES
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$models = array("Annotation_EnsemblEst", "Annotation_GeneOntology", "Annotation_Interpro", "Annotation_Kegg", "Annotation_Swissprot", "Interaction_Bind", "Interaction_BioGrid", "Interaction_Hprd", "Interaction_InNetDb", "Interaction_Intact", "Interaction_Mint", "Interaction_String",  "Precalculated_Ouzounis", "Precalculated_Prospectr", "CisRegModule", "Motif", "Blast", "Text");

$descr[0] = "EST are Expression Sequence Tags which allow the genes to be associated with the locations in the body where they are found to be expressed. The locations are organized in a tree structure.";
$descr[1] = "The GO project has developed three structured controlled vocabularies (ontologies) that describe gene products in terms of their associated biological processes,<br/> cellular components and molecular functions in a species-independent manner.";
$descr[2] = "InterPro is a database of protein families, domains and functional sites in which identifiable features found in known proteins can be applied to unknown protein sequences.";
$descr[3] = "KEGG is a database of biological systems, consisting of genetic building blocks of genes and proteins, molecular wiring diagrams of interaction and reaction networks (KEGG PATHWAY). KEGG provides a reference knowledge base for linking genomes to biological systems and also to environments by the processes of PATHWAY mapping.";
$descr[4] = "UniProtKB/Swiss-Prot is a curated protein sequence database which strives to provide a high level of annotation (such as the description of the function of a protein, its domains structure, post-translational modifications, variants, etc.), a minimal level of redundancy and high level of integration with other databases.";
$descr[5] = "The Biomolecular Interaction Network Database (BIND) is a collection of records documenting molecular interactions. The contents of BIND include high-throughput data submissions and hand-curated information gathered from the scientific literature.";
$descr[6] = "BioGRID is a freely accessible database of protein and genetic interactions. BioGRID release version 2.0 includes more than 116,000 interactions from Saccharomyces cerevisiae, Caenorhabditis elegans, Drosophila melanogaster and Homo sapiens.";
$descr[7] = "The Human Protein Reference Database represents a centralized platform to visually depict and integrate information pertaining to domain architecture, post-translational modifications, interaction networks and disease association for each protein in the human proteome. All the information in HPRD has been manually extracted from the literature by expert biologists who read, interpret and analyze the published data. HPRD has been created using an object oriented database in Zope, an open source web application server, that provides versatility in query functions and allows data to be displayed dynamically.";
$descr[8] = "IntNetDB is a database of human protein-protein interactions (PPIs) and their worm, fruitfly and mouse interologs computationally predicted through Bayesian analysis by integrating large-scale physical protein-protein interactions, protein domain assignments, gene co-expression data, genetic interactions, gene context information and biological function annotations from human and other model organisms.";
$descr[9] = "IntAct provides a freely available, open source database system and analysis tools for protein interaction data. All interactions are derived from literature curation or direct user submissions.";
$descr[10] = "MINT focuses on experimentally verified protein interactions mined from the scientific literature by expert curators. The curated data can be analyzed in the context of the high throughput data.";
$descr[11] = "STRING is a database of known and predicted protein-protein interactions.The interactions include direct (physical) and indirect (functional) associations; they are derived from four sources: Genomic Context, High-throughput Experiments, (Conserved) Coexpression,  Previous Knowledge.";
$descr[12] = "Sequence analysis of the group of proteins known to be associated with hereditary diseases allows the detection of key distinctive features shared within this group.This unique property pattern can be used to predict novel disease genes.";
$descr[13] = "Prospectr (PRiOrization by Sequence &amp; Phylogenetic Extent of CandidaTe Regions) is an alternating decision tree which has been trained to differentiate between genes likely to be involved in disease and genes unlikely to be involved in disease. By using sequence-based features like gene length, protein length and the percent identity of homologs in other species as input a classification can be obtained for a gene of interest.";
$descr[14] = "This submodel finds the best combination of five motifs that could coregulate the training genes.";
$descr[15] = "Putative transcription factors binding sites are given a score corresponding to the probability of this motif to be functionnal.";
$descr[16] = "BLAST (Basic Local Alignment Search Tool) is a sequence comparison algorithm optimized for speed used to search sequence databases for optimal local alignments to a query.";
$descr[17] = "The abstracts of publications are mined in order to find relevant keywords. Each keyword is given a score which represents its over-representation.";

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$checked = array("","checked");

//BUILD PAGE
echo "<div class=sectie>\n";
if (isset($_POST['submit'])) {
	$endeavour = "";
	for ($i = 0;$i<=19;$i+=1) {
		$model = $models[$i];
		if (isset($_POST[$model])) {
			$incl = $_POST[$model];
		}
		else {
			$incl = 0;
		}
		$endeavour = $endeavour . "$incl";
		
	}
	$query = "UPDATE users SET endeavour = '$endeavour'";
	mysql_query($query);
	echo "<h3>Success !</h3>\n";
	echo "<p>Models successfully updated</p>\n";
	echo "</div>\n";

}	
else {
	$result = mysql_query("SELECT endeavour FROM users WHERE id = '$userid'");
	$row = mysql_fetch_array($result);
	$endeavour = $row['endeavour'];
	echo "<h3>Prioritzation Preferences</h3>\n";

	 
	
}
// MAIN FORM


echo "<p>Prioritization of CNV's is done using <a href='http://nar.oxfordjournals.org/cgi/content/full/gkn325v1'>Endeavour (Tranchevent et al.)</a> <a href='http://homes.esat.kuleuven.be/~bioiuser/endeavour/index.php'>(Homepage)</a>. Here you get information on prioritization models included when prioritizing CNV's. </p>\n";
echo "<p>\n";
echo "<table cellspacing=0>\n";
echo " <tr>\n";
echo "  <th $topcell[1] $firstcell>Model Name</th>\n";
echo "  <th $topcell[1]>Short Description</th>\n";
echo " </tr>\n";
$switch = 0;
for ($i = 0;$i<=17;$i+=1) {
	$model = $models[$i];
	$description = $descr[$i];
	$incl = substr($endeavour,$i,1);
	echo " <tr>\n";
	echo "  <td $tdtype[$switch] $firstcell>$model</td>\n";
	echo "  <td $tdtype[$switch] >$description</td>\n";
	echo " </tr>\n";
}
echo "</table>\n";
echo "</p>\n";
echo "<p>\n";
echo "</p>\n";
echo "</div>\n";
?>
