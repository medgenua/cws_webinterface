<h3>Server Status</h3>
<?php
// GET user function
define("MAX_IDLE_TIME", 10);
function getOnlineUsers(){
	if ( $directory_handle = opendir( session_save_path() ) ) {
		$count = 0;
		while ( false !== ( $file = readdir( $directory_handle ) ) ) {
			if($file != '.' && $file != '..'){
			// Comment the 'if(...){' and '}' lines if you get a significant amount of traffic
				if(time()- fileatime(session_save_path() . '/' . $file) < MAX_IDLE_TIME * 60) {
					$count++;
				}
			}
		}
		closedir($directory_handle);
		return $count;
	} else {
		return false;
	}
} 


$lines = file('/proc/meminfo');
$info = array('time'=>date("F j, Y, g:i a"));          
foreach ($lines as $line_num => $line) {
	$parts = preg_split("/\s+/",$line);
	$info[$parts[0]]=$parts[1];
}
#$uptime = `uptime`;
#$pieces = preg_split('/\s+/',$uptime);
#$updays = $pieces[3];
#$uphours = $pieces[5];
#$uphours = preg_replace('/,/','',$uphours);
#$uphours = preg_replace('/:/','h ',$uphours);
$users = getOnlineUsers();

$loadavg = `cat /proc/loadavg`;
$pieces = preg_split("/\s+/",$loadavg);
#$load1 = $pieces[0];
#$load2 = $pieces[1];
#$load3 = $pieces[2];
$subpieces = explode("/",$pieces[3]);
$nrrun = $subpieces[0] - 1;

$ramtotal = $info['MemTotal:'];
$ramused = $info['MemTotal:'] - $info['MemFree:'] - $info['Buffers:'] - $info['Cached:'];
$swaptotal = $info['SwapTotal:'];
$swapused = $info['SwapTotal:']-$info['SwapFree:'];

$left = "style=\"background-color: #e3e3e6;padding: 0px;\"";
$right = "style=\"background-color: #e3e3e6;padding: 0px;text-align:right\"";

echo "<table cellspacing=0 width='100%'>\n";
echo " <tr class=clear >\n";
echo "  <th colspan=2 class=clear $left><u>Memory:</u></th>\n";
echo " </tr>\n";
echo " <tr class=clear >\n";
echo "  <td class=clear $left>Total Ram:</td>\n";
echo "  <td class=clear $right>".number_format($ramtotal,0,'',',')."Kb</td>\n";
echo " </tr>\n";
echo " <tr class=clear>\n";
echo "  <td class=clear $left>Used Ram: </td>\n";
echo "  <td class=clear $right>".number_format($ramused,0,'',',')."Kb</td>\n";
echo " </tr>\n";
echo " <tr class=clear>\n";
echo "  <td class=clear $left>Total Swap:</td>\n";
echo "  <td class=clear $right>".number_format($swaptotal,0,'',',')."Kb</td>\n";
echo " </tr>\n";
echo " <tr class=clear>\n";
echo "  <td class=clear $left>Used Swap:</td>\n";
echo "  <td class=clear $right>".number_format($swapused,0,'',',')."Kb</td>\n";
echo " </tr>\n";
echo " <tr class=clear >\n";
echo "  <th colspan=2 class=clear $left><u>Load:</u></th>\n";
echo " </tr>\n";
echo " <tr class=clear >\n";
echo "  <td class=clear $left>Users online:</td>\n";
echo "  <td class=clear $right>$users</td>\n";
echo " </tr>\n";
echo " </tr>\n";
echo " <tr class=clear >\n";
echo "  <td class=clear $left>Active threads:</td>\n";
echo "  <td class=clear $right>$nrrun</td>\n";
echo " </tr>\n";
echo " <tr class=clear >\n";
include('sysmon/inc_sysmon.inc');
echo "</tr>";

#echo " <tr class=clear>\n";
#echo "  <th colspan=2 class=clear $left><u>miscellaneous:</u></th>\n";
#echo " </tr>\n";
#echo " <tr class=clear >\n";
#echo "  <td class=clear $left>Users online:</td>\n";
#echo "  <td class=clear $right>$users</td>\n";
#echo " </tr>\n";
#echo " <tr class=clear >\n";
#echo "  <td class=clear $left>Uptime:</td>\n";
#echo "  <td class=clear $right>$updays"."d $uphours"."m</td>\n";
#echo " </tr>\n";

echo "</table>\n";
?>
