<?php 
include('inc_top_usermenu.inc');
$subjects = array(
	'main' => 'Using the Platform Documentation',
	'run' => 'Setting Up and Starting a CNV analysis',
	'documentation' => 'Using the Platform Documentation',
	'upload' => 'Uploading Large Datasets',
	'peds' => 'Provide Family Information',
	'results' => 'Browsing and Managing Your Data',
	'overview' => 'Graphical Project Overview',
	'shareprojects' => 'Browsing and Managing Your Data' 
);
echo "<b>Platform Documentation: </b>".$subjects[$_GET['s']] ."\n";
?>

