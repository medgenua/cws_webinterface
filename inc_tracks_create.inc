<?php 
ob_start();
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
if ($_GET['step'] == 1) {
	echo "<div class=sectie>\n";
	echo "<h3>Choose a basis for data selection</h3>\n";
	echo "<p>Please pick a basis for further data selection below. You can create tracks based on complete (combined) collections, where all samples in that collection are represented once,regardless of the chiptypes used.  Alternatively it's you can combine all samples run on a (combination of) specicified chiptype(s), or you can create tracks based on individual projects.</p>\n";
	echo "<p><form action=index.php?page=tracks&amp;type=create&amp;step=2 method=POST>\n";
	echo "Select your data based on: <br></p>\n";
	echo "<p><input type=radio name=step1 value=Collection>Collections<br>\n";
	echo "<input type=radio name=step1 value=Chiptype>Chiptypes<br>\n";
	echo "<input type=radio name=step1 value=Project>Projects<br>\n";
	echo "</p><p><input type=submit value=Next class=button>\n";
	echo "</form>\n";
	echo "</p>\n";
	echo "</div>\n";
}

if ($_GET['step'] == 2) {
	$basis = $_POST['step1'];
	echo "<div class=sectie>\n";
	echo "<h3>Choose $basis". "s for inclusion</h3>\n";
	echo  "<form action=index.php?page=tracks&amp;type=create&amp;step=3 method=POST>\n";
	echo "<input type=hidden name=step1 value=$basis>\n";
	if ($basis == "Collection") {
		$info = "<p>Pick the collections you want to combine into a single track from the following options. ";
		$info .= " Also check the \"Finetune\" button if you will need to exlude individual samples in a following step. ";
		$info .= " I recommend to not mix Control and Patient collections into a single track, for clarity reasons</p>\n";
		echo $info;
		echo "<p>Include the following $basis"."s: </p>\n";
		echo "<p>\n";
		$colquery = mysql_query("SELECT p.collection FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = $userid GROUP BY collection");
		while ($row = mysql_fetch_array($colquery) ) {
			$col = $row['collection'];
			echo "<input type=checkbox name=inc_" . $col .">$col<br>";
		}
		echo "</p><p><input type=submit value=Next class=button>\n";
		echo "</form>\n";
		echo "</p>\n";
		echo "</div>\n"; 
	}
	if ($basis == "Project") {
		$info = "<p>Pick the projects you want to combine into a single track from the following options. ";
		$info .= " Also check the \"Finetune\" button if you will need to exlude individual samples in a following step. ";
		$info .= " I recommend to not mix Control and Patient projects into a single track, for clarity reasons</p>\n";
		// illumina
		echo "$info";
		echo "<p>Include the following $basis"."s: </p>\n";
		echo "<p>\n";
		$collection = "";
		$proquery = mysql_query("SELECT p.id, p.naam, p.chiptype, p.collection FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY p.collection, p.naam");
		while ($row = mysql_fetch_array($proquery) ) {
			$col = $row['collection'];
			if ($col != $collection) {
				$collection = $col; 
				echo "</table></p><p>\n";
				echo "<table cellspacing=0>\n";
				echo " <tr>";
				echo "  <th scop=row colspan=4 $firstcell>$collection</td>\n";
				echo " </tr>\n";
				echo " <tr>\n";
				echo "  <th scope=column $firstcell>Include</td>\n";
				echo "  <th scope=column>Name</td>\n";
				echo "  <th scope=column>Chiptype</td>\n";
				echo "  <th scope=column># samples</td>\n";
			}
			$id = $row['id'];
			$name = $row['naam'];
			$chiptype = $row['chiptype'];
			$squery = mysql_query("SELECT COUNT(idsamp) AS aantal FROM projsamp WHERE idproj = '$id'");
			$srow = mysql_fetch_array($squery);
			$nrsample = $srow['aantal'];
			echo " <tr>\n";
			echo "  <td $firstcell><input type=checkbox name=inc_" . $id ."></td>\n";
			echo "  <td>$name</td>\n";
			echo "  <td>$chiptype</td>\n";
			echo "  <td>$nrsample</td>\n";
			echo " </tr>\n";
			
		}
		echo "</table></p>\n";
		//custom
		$collection = '';
		$proquery = mysql_query("SELECT p.id, p.naam, p.collection FROM cus_project p JOIN cus_projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY p.collection, p.naam");
		while ($row = mysql_fetch_array($proquery) ) {
			$col = $row['collection'];
			if ($col != $collection) {
				$collection = $col; 
				echo "</table></p><p>\n";
				echo "<table cellspacing=0>\n";
				echo " <tr>";
				echo "  <th scop=row colspan=4 $firstcell>Non-Illumina Data : $collection</td>\n";
				echo " </tr>\n";
				echo " <tr>\n";
				echo "  <th scope=column $firstcell>Include</td>\n";
				echo "  <th scope=column>Name</td>\n";
				echo "  <th scope=column>Chiptype</td>\n";
				echo "  <th scope=column># samples</td>\n";
			}
			$id = $row['id'];
			$name = $row['naam'];
			$chiptype = $row['chiptype'];
			$squery = mysql_query("SELECT COUNT(idsamp) AS aantal FROM cus_sample WHERE idproj = '$id'");
			$srow = mysql_fetch_array($squery);
			$nrsample = $srow['aantal'];
			echo " <tr>\n";
			echo "  <td $firstcell><input type=checkbox name=cus_inc_" . $id ."></td>\n";
			echo "  <td>$name</td>\n";
			echo "  <td>$chiptype</td>\n";
			echo "  <td>$nrsample</td>\n";
			echo " </tr>\n";
			
		}
		echo "</table></p>\n";

		echo "<p><p><input type=submit value=Next class=button>\n";
		echo "</form>\n";
		echo "</p>\n";
		echo "</div>\n"; 
	}
}


elseif ($_GET['step'] == 3) {
	$finetune = 1;
	$basis = $_POST['step1'];
	$incstring = "";
	$cusincstring = "";
	foreach($_POST as $name => $value) {
		if (substr($name, 0, 4) == "inc_") {
			$inc = substr($name, 4);
			$include[] = $inc;
			$incstring = $incstring . "&amp;" . $inc;
			continue;
		}
		if (substr($name,0,8) == "cus_inc_") {
			$cusinc = substr($name,8);
			$cusinclude[] = $cusinc;
			$cusincstring = $cusincstring . "&amp;" . $cusinc;
			continue;
		}
	}
	echo "<div class=sectie>\n";
	echo "<h3>Set track name and description</h3>\n";
	echo "<p>Please specify a name and a description for your track.  If you're uncertain where this will show, you can view an example <a href=https://$domain/$basepath/images/content/track.png target=new>here</a></p>\n";
	echo "<form action=index.php?page=tracks&amp;type=create&amp;step=4 method=POST>\n";
	echo "<input type=hidden name=step1 value=Collection>\n";
	echo "<input type=hidden name=incs value=\"$incstring\">\n";
	echo "<input type=hidden name=cusincs value=\"$cusincstring\">\n";
	echo "<table type=clear>\n";
	echo "<tr><td class=clear>Track Name:</td><td class=clear><input type=text name=trackname size=40></td></tr>\n";
	echo "<tr><td class=clear>Track Description:</td><td class=clear><input type=text name=trackdescription size=40></td></tr>\n";
	echo "</table><p>\n";
	if ($finetune == 1) {
		echo "<p>Next, select the samples you want to EXCLUDE from the track file. As a guideline, the callrates, chiptypes and number of aberrations are also shown</p>\n";
		if ($basis == "Collection") {
			$nrcol = 6;
			$extracol = " <th scope=column class=spec>From project</td>\n";
			$rowitem = 'collection';

		}
		else {
			$nrcol = 5;
			$extracol = "";
			$rowitem = 'id';

		}
		foreach ($include as $inc) {
			if ($basis == "Collection") {
				$header = $inc;
			}
			else {
				$nquery = mysql_query("SELECT p.naam FROM project p where id = '$inc'");
				$nrow = mysql_fetch_array($nquery);
				$header = $nrow['naam'];
			}
			echo "<p><table cellspacing=0>\n";
			echo "<tr>\n";
			echo "<th colspan=$nrcol $firstcell>$header</td>\n";
			echo "</tr>\n";
			echo "<tr>\n";
			echo " <th scope=column class=spec>Exclude</td>\n";
			echo " <th scope=column class=spec>Sample</td>\n";
			echo $extracol;
			echo " <th scope=column class=spec>Callrate</td>\n";
			echo " <th scope=column class=spec>Chiptype</td>\n";
			echo " <th scope=column class=spec># Aberrations</td>\n";
			echo "</tr>\n";
			if ($basis == "Collection" ) {
				$query = mysql_query("SELECT s.ID, s.chip_dnanr, s.intrack, s.trackfromproject FROM sample s JOIN project p JOIN projectpermission pp JOIN projsamp ps ON s.id = ps.idsamp AND p.id = ps.idproj AND p.id = pp.projectid WHERE p.collection = '$inc' AND pp.userid = '$userid' GROUP BY s.ID  ORDER BY s.chip_dnanr");
			}
			else {
				$query = mysql_query("SELECT s.ID, s.chip_dnanr, s.intrack, s.trackfromproject FROM sample s JOIN project p JOIN projectpermission pp JOIN projsamp ps ON s.id = ps.idsamp AND p.id = ps.idproj AND p.id = pp.projectid WHERE p.id = '$inc' AND pp.userid = '$userid' GROUP BY s.ID ORDER BY s.chip_dnanr");
			}
			while ($row = mysql_fetch_array($query) ) {
				$sname = $row['chip_dnanr'];
				//echo "chipdnanr = $sname<br>";
				$fromproject = "";
				$callrate = "0";
				$chiptype = "";
				$nrab = "";
				$sid = $row['ID'];
				$intrack = $row['intrack'];
				$tfp = $row['trackfromproject'];
				$checked = "";
				$print = "notok";
				if ($intrack == 1) {
					$subquery = mysql_query("SELECT p.chiptype, p.naam, p.collection, ps.callrate, p.id FROM project p JOIN projectpermission pp JOIN projsamp ps ON ps.idproj = p.id AND p.id = pp.projectid WHERE ps.idsamp = '$sid' AND p.id = '$tfp' AND pp.userid = '$userid'");
					if (mysql_num_rows($subquery) > 0) {  // If access to the project that is used for occurence calculations
						$subrow = mysql_fetch_array($subquery);
						if ($subrow[ $rowitem ] == "$inc") {
							$fromproject = $subrow['naam'];
							$fromid = $tfp;
							$callrate = $subrow['callrate'];
							$chiptype = $subrow['chiptype'];
							$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
							$abrow = mysql_fetch_array($abquery);
							$nrab = $abrow['nr'];
							$print = "ok";
							$samples[] = $sid;
						}
						elseif (in_array($subrow[ $rowitem ], $include)) {
							$print = $subrow[ $rowitem ];
					
						}
						elseif (!in_array($sid, $samples)) {  // No access OR accessible project/collection is not selected for this track creation => use this project/collection for track
							$details = mysql_query("SELECT p.naam, p.collection, p.id, ps.callrate, p.chiptype FROM project p JOIN projsamp ps JOIN projectpermission pp ON p.id = ps.idproj AND p.id = pp.projectid WHERE ps.idsamp = '$sid' AND pp.userid = '$userid' AND p.$rowitem = '$inc'");
							$detrow = mysql_fetch_array($details);
							$fromproject = $detrow['naam'];
							$fromid = $detrow['id'];
							$callrate = $detrow['callrate'] ;
							$chiptype = $detrow['chiptype'];
							$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
							$abrow = mysql_fetch_array($abquery);
							$nrab = $abrow['nr'];
							$print = "ok";
							$samples[] = $sid;
						
						}

					}
					
					elseif (!in_array($sid, $samples)) {  // No access OR accessible collection is not selected for this track creation
							$details = mysql_query("SELECT p.naam, p.collection, p.id, ps.callrate, p.chiptype FROM project p JOIN projsamp ps JOIN projectpermission pp ON p.id = ps.idproj AND p.id = pp.projectid WHERE ps.idsamp = '$sid' AND pp.userid = '$userid' AND p.$rowitem = '$inc'");
							$detrow = mysql_fetch_array($details);
							$fromproject = $detrow['naam'];
							$fromid = $detrow['id'];
							$callrate = $detrow['callrate'] ;
							$chiptype = $detrow['chiptype'];
							$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
							$abrow = mysql_fetch_array($abquery);
							$nrab = $abrow['nr'];
							$print = "ok";
							$samples[] = $sid;
						
					}
						
				}
				elseif (!in_array($sid, $samples)) {  // No access OR accessible collection is not selected for this track creation
							$details = mysql_query("SELECT p.naam, p.collection, p.id, ps.callrate, p.chiptype FROM project p JOIN projsamp ps JOIN projectpermission pp ON p.id = ps.idproj AND p.id = pp.projectid WHERE ps.idsamp = '$sid' AND pp.userid = '$userid' AND p.$rowitem = '$inc'");
							$detrow = mysql_fetch_array($details);
							$fromproject = $detrow['naam'];
							$fromid = $detrow['id'];
							$callrate = $detrow['callrate'] ;
							$chiptype = $detrow['chiptype'];
							$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
							$abrow = mysql_fetch_array($abquery);
							$nrab = $abrow['nr'];
							$print = "ok";
							$samples[] = $sid;
							$checked = "CHECKED";
						
				}
				if ($print == "ok") {	
					echo "<tr>\n";
					echo " <td  ><input type=hidden name=inc_$sid"."_"."$fromid value=1><input type=checkbox name=exc_$sid"."_"."$fromid $checked></td>\n";
					echo " <td >$sname </td>\n";
					if ($basis == "Collection") {
						echo " <td >$fromproject</td>\n";
					}
					echo " <td >$callrate</td>\n";
					echo " <td >$chiptype</td>\n";
					echo " <td >$nrab</td>\n";
					echo "</tr>\n";
				}
				else {
					echo "<tr>\n";
					echo " <td  $firstcell><input type=checkbox name=exclude"."_$sid"."_"."$fromid DISABLED></td>\n";
					echo " <td >$sname</td>\n";
					echo " <td colspan = 4>Also included in $rowitem \"$print\" </td>\n";
					echo "</tr>\n";
				}
				ob_flush();
   				flush();

			}		
			echo "</table></p>\n";
		}
		// custom data
		foreach ($cusinclude as $inc) {
			if ($basis == "Collection") {
				$header = $inc;
			}
			else {
				$nquery = mysql_query("SELECT p.naam FROM cus_project p where id = '$inc'");
				$nrow = mysql_fetch_array($nquery);
				$header = $nrow['naam'];
			}
			echo "<p><table cellspacing=0>\n";
			echo "<tr>\n";
			echo "<th $firstcell colspan=$nrcol>$header</td>\n";
			echo "</tr>\n";
			echo "<tr>\n";
			echo " <th scope=column class=spec>Exclude</td>\n";
			echo " <th scope=column class=spec>Sample</td>\n";
			echo $extracol;
			echo " <th scope=column class=spec>Gender</td>\n";
			echo " <th scope=column class=spec>Chiptype</td>\n";
			echo " <th scope=column class=spec># Aberrations</td>\n";
			echo "</tr>\n";
			$query = mysql_query("SELECT s.idsamp, s.chiptype,s.intrack FROM cus_sample s JOIN cus_projectpermission pp ON s.idproj = pp.projectid WHERE s.idproj = '$inc' AND pp.userid = '$userid' ORDER BY s.idsamp");
			#echo "SELECT s.idsamp, s.chiptype,s.intrack FROM cus_sample s JOIN cus_projectpermission pp ON s.idproj = pp.projectid WHERE s.idproj = '$inc' AND pp.userid = '$userid' ORDER BY s.idsamp<br/>";
			while ($row = mysql_fetch_array($query) ) {
				$sname = $row['idsamp'];
				$chiptype = $row['chiptype'];
				if ($chiptype == '') {$chiptype = 'Not Specified';}
				$gender = $row['gender'];
				if ($gender == '') {$gender = 'Not specified';}
				$nrab = "";
				$intrack = $row['intrack'];
				$checked = "";
				$print = "notok";
				if ($intrack == 0) {
					$checked = 'CHECKED';
				}
				$abquery = mysql_query("SELECT COUNT(id) AS nr FROM cus_aberration WHERE idproj = '$inc' AND sample = '$sname'");
				$abrow = mysql_fetch_array($abquery);
				$nrab = $abrow['nr'];
			
				echo "<tr>\n";
				echo " <td $firstcell ><input type=hidden name=cus_inc_$sname"."_"."$inc value=1><input type=checkbox name=cus_exc_$sname"."_"."$inc $checked></td>\n";
				echo " <td >$sname </td>\n";
				echo " <td >$gender</td>\n";
				echo " <td >$chiptype</td>\n";
				echo " <td >$nrab</td>\n";
				echo "</tr>\n";
				ob_flush();
   				flush();
			}
			echo "</table>";
		}		
	
		echo "<input type=hidden name=finetune value=1>\n";	
	}
	else {
		echo "<input type=hidden name=finetune value=0>\n";
	}
	echo "</p></p><input type=submit name=submit value=Finish class=button></p>\n";
	echo "</div>\n";
}

elseif ($_GET['step'] == 4) {
	$name = $_POST['trackname'];
	$description = $_POST['trackdescription'];
	$basis = $_POST['step1'];
	$incstring = $_POST['incs'];
	$cusincstring = $_POST['cusincs'];
	$finetune = $_POST['finetune'];
	$include;
	$exclude;
	if ($basis == "Collection") {
		//$nrcol = 6;
		//$extracol = " <th scope=column class=spec>From project</td>\n";
		$rowitem = 'collection';
	}
	else {
		//$nrcol = 5;
		//$extracol = "";
		$rowitem = 'id';
	}

	/*foreach($_POST as $name => $value) {
		if (substr($name, 0, 4) == "col_") {
			$col = substr($name, 4);
			$include[] = $col;
			$colstring = $colstring . "&amp;" . $col;
		}
	}
	*/
	if ($finetune == 1) {
		foreach($_POST as $varname => $value) {
			if (substr($varname, 0, 4) == "inc_") {
				$val = substr($varname, 4);
				$include[] = $val;
				//$colstring = $colstring . "&amp;" . $col;
			}
			elseif (substr($varname, 0, 4) == "exc_") {
				$val = substr($varname, 4);
				$exclude[] = $val;
			}
			elseif (substr($varname,0,8) == "cus_inc_") {
				$val = substr($varname,8);
				$cusinclude[] = $val;
			}
			elseif (substr($varname,0,8) == "cus_exc_") {
				$val = substr($varname,8);
				$cusexclude[] = $val;
			}
		}
	}
	elseif ($finetune == 0) {
		$incs = explode("&amp;",$incstring);
		
		foreach ($incs as $inc) {
			if ($inc != "") {
				$query = mysql_query("SELECT s.ID, s.chip_dnanr, s.intrack, s.trackfromproject FROM sample s JOIN project p JOIN projectpermission pp JOIN projsamp ps ON s.id = ps.idsamp AND p.id = ps.idproj AND p.id = pp.projectid WHERE p.$rowitem = '$inc' AND pp.userid = '$userid' GROUP BY s.ID ");
				while ($row = mysql_fetch_array($query) ) {
					$sname = $row['chip_dnanr'];
					//echo "chipdnanr = $sname<br>";
					$fromproject = "";
					$callrate = "";
					$chiptype = "";
					$nrab = "";
					$sid = $row['ID'];
					$intrack = $row['intrack'];
					$tfp = $row['trackfromproject'];
					$checked = "";
					if ($intrack == 1) {
						$subquery = mysql_query("SELECT p.chiptype, p.naam, p.collection, ps.callrate FROM project p JOIN projectpermission pp JOIN projsamp ps ON ps.idproj = p.id AND p.id = pp.projectid WHERE p.id = '$tfp' AND pp.userid = '$userid'");
						if (mysql_num_rows($subquery) > 0) {  // If access to the project that is used for occurence calculations
							$subrow = mysql_fetch_array($subquery);
							if ($subrow[ $rowitem ] == "$inc") {
								$fromproject = $subrow['naam'];
								$fromid = $tfp;
								$callrate = $subrow['callrate'];
								$chiptype = $subrow['chiptype'];
								$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
								$abrow = mysql_fetch_array($abquery);
								$nrab = $abrow['nr'];
								$print = "ok";
								$samples[] = $sid;
							}
							elseif (in_array($subrow[ $rowitem ], $include)) {
								$print = $subrow[ $rowitem ];
					
							}
						}
					
						elseif (!in_array($sid, $samples)) {  // No access OR accessible collection is not selected for this track creation
							$details = mysql_query("SELECT p.naam, p.collection, p.id, ps.callrate, p.chiptype FROM project p JOIN projsamp ps JOIN projectpermission pp ON p.id = ps.idproj AND p.id = pp.projectid WHERE ps.idsamp = '$sid' AND pp.userid = '$userid' AND p.$rowitem = '$inc'");
							$detrow = mysql_fetch_row($details);
							$fromproject = $detrow['naam'];
							$fromid = $detrow['id'];
							$callrate = $detrow['callrate'];
							$chiptype = $detrow['chiptype'];
							$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
							$abrow = mysql_fetch_row($abquery);
							$nrab = $abrow['nr'];
							$print = "ok";
							$samples[] = $sid;
						
						}
						
					}
					elseif (!in_array($sid, $samples)) {  // No access OR accessible collection is not selected for this track creation
							$details = mysql_query("SELECT p.naam, p.collection, p.id, ps.callrate, p.chiptype FROM project p JOIN projsamp ps JOIN projectpermission pp ON p.id = ps.idproj AND p.id = pp.projectid WHERE ps.idsamp = '$sid' AND pp.userid = '$userid' AND p.$rowitem = '$inc'");
							$detrow = mysql_fetch_array($details);
							$fromproject = $detrow['naam'];
							$fromid = $detrow['id'];
							$callrate = $detrow['callrate'];
							$chiptype = $detrow['chiptype'];
							$abquery = mysql_query("SELECT COUNT(id) AS nr FROM aberration WHERE idproj = '$fromid' AND sample = '$sid'");
							$abrow = mysql_fetch_array($abquery);
							$nrab = $abrow['nr'];
							$print = "ok";
							$samples[] = $sid;
							$checked = "CHECKED";
						
					}
					if ($print == "ok") {	
						$include[] = $sid . "_" .$fromid;
					}
					//ob_flush();
   					//flush();

				}		
			}
		}
	}
	$namesafe = str_replace(' ', '_', $name);
	$myFile = "/srv/www/htdocs/cnv/tracks/$userid.$namesafe.bed";
	$fh = fopen($myFile, 'w');
	$stringData = "track name=\"$name\" description=\"$description\" itemRgb=On\n";
	fwrite($fh, $stringData);
	//fclose($fh);
	$types = array("255,0,0", "160,32,240", "255,255,255", "0,255,0", "0,0,255");

	foreach ($include as $ids) {
		if (!in_array($ids, $exclude)) {
			$pieces = explode("_",$ids);
			$sid = $pieces[0];
			$pid = $pieces[1];
			mysql_select_db("$db");
			$query = mysql_query("SELECT s.chip_dnanr, a.chr, a.start, a.stop, a.cn, a.seenby FROM aberration a JOIN sample s ON s.id = a.sample WHERE a.idproj = '$pid' AND sample = '$sid'");
			while ($row = mysql_fetch_array($query)) {
				$chr = $row['chr'];
				$chrtxt = $chromhash[ "$chr" ];	
				$start = $row['start'];
				$end = $row['stop'];
				$sample = $row['chip_dnanr'];
				$cn = $row['cn'];
				$seenby = $row['seenby'];
				$numeric = 0;
				$sum = 0;
				if (strpos($seenby, "QuantiSNP")) {
					$score = preg_replace('/(.*QuantiSNP\s\()(.+?)(\).*)/',"$2",$seenby);
					$sum = $sum + $score;
					$numeric++;
				}
				if (strpos($seenby,"PennCNV")) {
					$score = preg_replace('/(.*PennCNV\s\()(.+?)(\).*)/',"$2",$seenby);
					$sum = $sum + $score;
					$numeric++; 
				}
				$score = $sum / $numeric;

				//print "$ids<br>\n";
				$string = "chr$chrtxt\t$start\t$end\t$sample\t$score\t+\t$start\t$end\t$types[$cn]\n";
				//echo "$string<br>";
				fwrite($fh, $string);
			}
		}
	}
	// custom data
	foreach ($cusinclude as $ids) {
		if (!in_array($ids, $cusexclude)) {
			$pieces = explode("_",$ids);
			$pid = array_pop($pieces);
			$sid = join($pieces,"_");
			mysql_select_db("$db");
			$query = mysql_query("SELECT s.idsamp, a.chr, a.start, a.stop, a.cn, a.confidence FROM cus_aberration a JOIN cus_sample s ON s.idsamp = a.sample AND s.idproj = a.idproj WHERE a.idproj = '$pid' AND a.sample = '$sid' ");
			while ($row = mysql_fetch_array($query)) {
				$chr = $row['chr'];
				$chrtxt = $chromhash[ "$chr" ];	
				$start = $row['start'];
				$end = $row['stop'];
				$sample = $row['idsamp'];
				$cn = $row['cn'];
				if (preg_match("/gain/i",$cn)) {
					$cn = 3;
				}
				elseif (preg_match("/los/i",$cn)) {
					$cn = 1;
				}
				elseif (preg_match("/loh/i",$cn)) {
					$cn = 2;
				}
				$numeric = $row['confidence'];
				if (!is_numeric($numeric)) {
					$numeric=0;
				}	
				//print "$ids<br>\n";
				#if ($start > $end) {
				#	$tmp = $start;
				#	$start = $end;
				#	$end = $tmp;
				#}
				$string = "chr$chrtxt\t$start\t$end\t$sample\t$numeric\t+\t$start\t$end\t$types[$cn]\n";
				//echo "$string<br>";
				fwrite($fh, $string);
			}
		}
	}

	fclose($fh);
	system("chmod 777 \"$myFile\"");
	echo "Finished, your track is available as following link: <br>\n";
	echo "https://$domain/$basepath/tracks/$userid.$namesafe.bed<br>\n";
	

}




?>
