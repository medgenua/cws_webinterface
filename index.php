<?php
///////////////////
// START SESSION //
///////////////////
ini_set("session.cookie_lifetime",3600*24); // session life set to 1 day
session_start(); //Allows you to use sessions

////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

/////////////////////////
// GET SERVER LOCATION //
/////////////////////////
// This allowes for local installations, as hostname is filled in dynamically
$domain = $_SERVER['HTTP_HOST']; 


if (isset($_GET['page'])) {
	$page = ($_GET['page']);
}
else {
	$page = 'main';
}
if (empty($page)) {
	$page = "main";
} 
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
#######################
# CONNECT to database #
#######################
$ok = include('.LoadCredentials.php');
if ($ok != 1) {
	include('inc_installation_problem.inc');
	exit();
}
// always log in to the default current build.		
if ($_SESSION['dbname'] == '') {
	// this seems to exlude some sort of race condition, where session is not starting correctly (i think, I had missing SESSION vars)
	mysql_select_db('GenomicBuilds');
	$query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
	$row = mysql_fetch_array($query);
	$_SESSION['dbname'] = $row['name'];
	$_SESSION['dbstring'] = $row['StringName'];
	$_SESSION['dbdescription'] = stripslashes($row['Description']);
}
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

////////////////////////
// GET SERVER VERSION //
////////////////////////
	exec("cd $sitedir && git rev-parse HEAD",$output,$exit); 
	$wrev = '';
	foreach($output as $key => $line) {
		$wrev = substr($line,0,7);
		break;
	}
	$output = array();
	exec("cd $scriptdir && git rev-parse HEAD",$output,$exit); 
	
	$arev = '';
	foreach($output as $key => $line) {
		$arev = substr($line,0,7);
		break;
	}


// CHECK COOKIES
if (!isset($_COOKIE['plotwidth'])) {
	$_COOKIE['plotwidth'] = 1024;
}

// CHECK USER INFORMATION
if (isset($_POST['username']) &&  isset($_POST['password'])) {//If you entered a username and password .
	$username = $_POST['username'];
	$password = $_POST['password'];
	$query = "SELECT level, FirstName, id, projectsrun, samplesrun FROM users WHERE username = '".addslashes($_POST['username'])."' AND password=MD5('".$_POST['password']."')";
	$auth  = mysql_query("$query"); 
	if (mysql_num_rows($auth) == 0) {
		#echo "wrong user/pass entered\n";
		$wrongpass = 1;
	}
	else {
		$row = mysql_fetch_array($auth);
		if ($row['level'] == 0) {
			$disabled = 1;
		}
		else {
		$_SESSION['logged_in'] = "true";
		$_SESSION['username'] = $_POST['username']; //Saves your username.
		$_SESSION['level'] = $row['level'];
		$_SESSION['FirstName'] = $row['FirstName'];
		$_SESSION['userID'] = $row['id'];
		$_SESSION['projectsrun'] = $row['projectsrun'];
		$_SESSION['samplesrun'] = $row['samplesrun'];
		$settingquery = mysql_query("SELECT DGVshow, DGVinclude, HapSame, HapAll, RefSeq, GCcode, GCncode,SegDup,LOHshow, LOHminsize, LOHminsnp, condense, condense_classes, condense_null, db_links, ecaruca,ecarucaSize,DECsyn,DECpat FROM usersettings WHERE id = '".$row['id'] ."'");
		$settingrow = mysql_fetch_array($settingquery);
		$_SESSION['DGVshow'] = $settingrow['DGVshow'];
		$_SESSION['DGVinclude'] = $settingrow['DGVinclude'];
		$_SESSION['HapSame'] = $settingrow['HapSame'];
		$_SESSION['HapAll'] = $settingrow['HapAll'];
		$_SESSION['RefSeq'] = $settingrow['RefSeq'];
		$_SESSION['GCcode'] = $settingrow['GCcode'];
		$_SESSION['GCncode'] = $settingrow['GCncode'];
		$_SESSION['SegDup'] = $settingrow['SegDup'];		
		$_SESSION['LOHshow'] = $settingrow['LOHshow'];
		$_SESSION['LOHminsize'] = $settingrow['LOHminsize'];
		$_SESSION['LOHminsnp'] = $settingrow['LOHminsnp'];
		$_SESSION['Condense'] = $settingrow['condense'];
		$_SESSION['ToCondense'] = $settingrow['condense_classes'];
		$_SESSION['NullCondense'] = $settingrow['condense_null'];
		$_SESSION['db_links'] = $settingrow['db_links'];
		$_SESSION['ecaruca'] = $settingrow['ecaruca'];
		$_SESSION['ecarucaSize'] = $settingrow['ecarucaSize'];
		$_SESSION['DECsyn'] = $settingrow['DECsyn'];
		$_SESSION['DECpat'] = $settingrow['DECpat'];
		$_SESSION['freshlogin'] = 1;
		// remove cookie holding update info on admin login.
		if ($row['level'] == 3 && isset($_COOKIE['updates'])) {
			unset($_COOKIE['updates']);
			setcookie('updates','',1);
		}
		}
	}
}
ob_end_flush();

if (isset($_SESSION['logged_in'])) {
	$loggedin = 1;
	$username = $_SESSION['username'];
	$firstname = $_SESSION['FirstName'];
	$level = $_SESSION['level'];
	$userid = $_SESSION['userID'];
	$projectsrun = $_SESSION['projectsrun'];
	$samplesrun = $_SESSION['samplesrun'];
	$maxqsnpasguest = 10;
	$maxsamplesasguest = 50;
	$inbox = mysql_query("SELECT inbox.id FROM inbox WHERE inbox.to = '$userid' AND inbox.read = 0");
	if (mysql_num_rows($inbox) > 0) {
		$_SESSION['newmessages'] = 1;
	}
	else {
		$_SESSION['newmessages'] = 0;
	}
}
?>
<head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<meta name="verify-v1" content="XGbxV38bulqIY24GD5PeFdvg0KKw5wyVH5JlxYZc16E=" />
 	<?php echo "<base href='https://$domain/$basepath/' />"; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>CNV-WebStore: Online CNV Analysis, Storage and Interpretation (<?php echo $page;?>)</title>
	<meta name="description" content="CNV-WebStore: Analysis, storage and interpretation platform. Tailored towards but not limited to Illumina BeadArray data. Developed at Centre of Medical Genetics, University of Antwerp, Belgium" />
	<meta name="keywords" content="Illumina, BeadArray, CNV, BAF, prioritisation, Uniparental Disomy, UPD, snpTRIO, Mosaic, copy-number, copy number, HMM, hidden markov, qPCR, QuantiSNP, PennCNV, VanillaICE, primerdesign, primerdatabase, primer database, rtpcr, CNV interpretation tool" />
	<!-- Bundled General Stylesheet file -->
	<link rel='stylesheet' type='text/css' href='stylesheets/All_Format2.css'/>
	<!-- all IE below 9 have seperate stylesheet -->
	<!--[if lt IE 9]>
	        <link rel="stylesheet" type="text/css" href="stylesheets/ie8-and-down.css" />
	<![endif]-->
	<!--[if gt IE 9]>
	       <link rel="stylesheet" type="text/css" href="stylesheets/All_But_IE.css" />
	<![endif]-->
	<!--[if !IE]><!-->
	       <link rel="stylesheet" type="text/css" href="stylesheets/All_But_IE.css" />
	<!--<![endif]-->

	<!-- Load GENERAL JavaScripts -->
	<script type="text/javascript" src="javascripts/form_refresh.js"></script>
	<script type="text/javascript" src="javascripts/setBuild.js"></script>
	<script type='text/javascript' src='javascripts/overlib.js'> </script>	
	<?php
	// Load PAGE SPECIFIC scripts & styles //
	switch($page){
		case "details":
			echo '<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>';
		case "diff":
		case "custom_methods":
		case "filter":
		case "cusdetails":
			echo "<script type='text/javascript' src='javascripts/rightcontext.js'></script>\n";
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/rightcontext.css' />\n";
			break;
		case "results":
			if ($_GET['type'] == 'pheno') {
				echo "<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>\n";
				echo "<link rel='stylesheet' type='text/css' href='stylesheets/clinicalpages.css' />";
			}
			if ($_GET['type'] == 'region' ) {
				echo '<link rel="stylesheet" type="text/css" href="stylesheets/imgareaselect-default.css" />';
				echo '<script type="text/javascript" src="javascripts/jquery.min.js"></script>';
				echo ' <script type="text/javascript" src="javascripts/jquery.imgareaselect.pack.js"></script>';
			}
			break;
		case "result":
			echo "<script type='text/javascript' src='javascripts/result.js'></script>\n";
			break;
		case "cusclinical":
		case "clinical":
		case "parents":
			echo "<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>\n";
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/clinicalpages.css' />\n";
			break;
		case "tutorial":
		case "documentation":
			echo "<script type='text/javascript' src='javascripts/lightbox.js'></script>\n";
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/lightbox.css' />\n";
			break;
		case "UniSeqResult":
			echo "<script type='text/javascript' src='javascripts/popup.js'></script>\n";
			break;
		case "upload":
			echo "<script type='text/javascript' src='javascripts/upload.js'></script>\n";
			break;
		case "admin_pages":
			echo "<script type='text/javascript' src='javascripts/adminpages.js'></script>\n";
			break;
		case "overview":
		case "overviewbrowse":
			echo '<link rel="stylesheet" type="text/css" href="stylesheets/imgareaselect-default.css" />';
			echo '<script type="text/javascript" src="javascripts/jquery.min.js"></script>';
			echo ' <script type="text/javascript" src="javascripts/jquery.imgareaselect.pack.js"></script>';
			break;
		case "class":
			echo '<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>';
			break;
        case "LiftPruning":
            echo '<script type="text/javascript" src="javascripts/jquery.min.js"></script>';


	}
	// CHECK SITE STATUS (operative / construction / LiftOver //	
	$query = mysql_query("SELECT status FROM `GenomicBuilds`.`SiteStatus`");
	$row = mysql_fetch_array($query);
	$SiteStatus = $row['status'];
	switch($SiteStatus){
		case "Construction":
		case "LiftOver":
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/construction.css' />\n";
			echo "<script type='text/javascript' src='javascripts/construction.js'></script>\n";
			break;
	}
	// include google analytics
	include('Google_Analytics.txt');
	?>
</head>

<?php
// change background color if in old build //
if (array_key_exists('bgcolor', $_SESSION)) {
	echo "<body ". $_SESSION['bgcolor'] .">\n";
}
else {
	echo "<body >\n";
}

?>
<!-- This div contains popup from overLib package -->
<div id="overDiv" style="position:absolute;visibility:hidden;z-index:5000;"></div>
<div id="overlay" style="display:none;"></div>
<!-- this div contains the tooltips from the wz_tooltip package. tooltipdiv is removed by packages ! -->
<div id='tooltipdiv' style='display:none'><div id='tooltip' style='width:450px'></div></div>
<!-- load some more javascripts... -->
	<script type="text/javascript" src="javascripts/wz_tooltip.js"></script>	
	<script type="text/javascript" src="javascripts/menu2.js"></script>
	<div id="container">  
	<!-- load the div to contain preference settings. -->
	  <div class='transparentdark areatip' id='infodiv' style='display:none' >
	  </div>
 		<?php 
		// display preferences updated => include snippet from browser (no output, just db updates.)
		if (isset($_POST['updatepreferences'])) {
			$subtype = $_POST['set'];
			// process updates
			if ($subtype != 'Cancel') { 
				include('Browser/setplotprefs.php'); // located in Web-Interface/Browser/ (LoadBrowser is included, so path is Web-Interface)!
			}
			// unset relevant variables
			unset($_POST['updatepreferences']);
			unset($_POST['set']);
		}
 		?>
		<div id="inhoud">
			<div id="inhoud-hoofd">
				<?php
				// small joke
				echo "<div id='inhoud-hoofdlinks' style='position:relative;'>";
				if (date('j') > 14 && date('j') <= 26 && date('n') == 12) {
					echo "<img style='position:absolute;top:-48px;left:-67px' src='images/content/xmas.png'>";
				}
				elseif (date('j') > 26 && date('n') == 12) {
					echo "<img style='position:absolute;top:6px;left:350px' src='images/content/newyeardog.png'>";
				}
				elseif (date('j') <= 4 && date('n') == 1) {
					echo "<img style='position:absolute;top:6px;left:350px' src='images/content/newyeardog.png'>";
				}
				echo "<span style='position:absolute;right:5px;top:0px;color:#333333;padding-right:5px;font-size:10px;text-align:right;'>";
				echo "Interface: Rev.$wrev<br/>";
				echo "Analysis: Rev.$arev<br/>";
				if (file_exists('Information.txt')) {
					echo file_get_contents('Information.txt');
				}
				echo "</span>";
				?>
				<h1>CNV-WebStore<span style:'text-size:normal'> v2.0</span></h1>
				<?php
				// check for site-specific name
				if (file_exists('Local_Name.txt')) {
					echo "<span style='float:right;margin-bottom:-100px;font-style:italic;padding-right:5px;font-size:14px'>";
					echo file_get_contents('Local_Name.txt');
					echo "</span>";
				}
				else {
					echo "<span style='float:right;margin-bottom:-100px;font-style:italic;padding-right:5px;font-size:14px'>An Online CNV Analysis, Storage and Interpretation Platform</span>";
			  	}	
				?>
				</div>
 			  	<div id="inhoud-hoofdrechts"></div>
			</div>
			<?php
			// the menu
			echo "<div id='inhoud-menu'>";
			include ("inc_menu.horizontal.inc");
			echo '</div>';
			flush();
			echo "<div id='inhoud-rechts'> ";
			switch ($SiteStatus) {
				case "Install";
					// include first-time run page for finalising setup
					$page = 'freshinstall';
					break;
				case "Construction":
					if ($level < 3 && $loggedin == 1) {  # This shows the blocking overlay for all users with permissions level below 3 (non-admins)
						echo "<div id='displaybox' ><br/><br/><br/><center style='font-size:18px;font-weight:bold;color:white;position:relative;top:25%' ><img src='images/content/under-construction.gif'><br/>The Platform is currently undergoing critical upgrades, making any analysis temporarily impossible. Please check back soon !<br/><br/>Thank you for your patience.</center></div>";
					}
					break;
				case "LiftOver":
					if ($_SESSION['freshlogin'] == 1) {  # This shows the blocking overlay for all users with permissions level below 3 (non-admins)
						echo "<div id='displaybox' ><br/><br/><br/><center style='font-size:18px;font-weight:bold;color:white;position:relative;top:25%' ><img src='images/content/update.jpg'><br/>The Platform is currently being transferred to a new Genome Build. You can close this message and browse the data, but nothing can be changed !<br/><br/>Thank you for your patience.<br/><br/><a href='#' onclick='return clicker();'>CLOSE WINDOW</a></center></div>";
						$_SESSION['freshlogin'] = 0;
					}
					break;

			}
			$inc = 'page_' . $page . '.php'; 
			if (file_exists($inc)) {
				echo "<div id=\"inhoud-rechtstop\"> ";
				if (file_exists("inc_top_$page.inc")) {
					include ('inc_top_' . $page .'.inc');
				}
				
				echo "</div>";
				if (isset($wrongpass)) {
					echo "<div class=sectie><h3>Wrong Username/Password Combination !!</h3></div>";
					include('page_login.php');
				}
				elseif (isset($disabled) && $disabled == 1) {
					echo "<div class=sectie><h3>This account is not active. Please contact your administrator !!</h3></div>";
					include('page_login.php');
				}
				else {
					include ($inc); 
				}
			}
			else {
				echo "Page not found!\n";
			}
			?>
			</div>
			<?php
			switch($page) {	
				case 'details':
				case 'results':
				case 'cusdetails':
				case 'overview':
				case 'project_differences':
					echo "<br style='clear:both' />";
					echo "<div><div id=empty style='float:right;width:1px;height:30px;'></div></div>";
					break;
			}
			
			?>
		</div>

	</div>
	<?php
        if (file_exists("javascripts/page.$page.js")) {
            echo "<script type='text/javascript' src='javascripts/page.$page.js?$arev'></script>";
        }
     ?>
	</body>
</html>
