<?php
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

/////////////////////////
// GET SERVER LOCATION //
/////////////////////////
// This allowes for local installations, as hostname is filled in dynamically
$domain = $_SERVER['HTTP_HOST']; 

///////////////////
// START SESSION //
///////////////////
session_start(); //Allows you to use sessions
$page = ($_GET['page']);
if (empty($page)) {
	$page = "main";
} 
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
#######################
# CONNECT to database #
#######################
include('.LoadCredentials.php');
if ($_SESSION['dbname'] == '') {
	// this seems to exlude some sort of race condition, where session is not starting correctly (i think, I had missing SESSION vars)
	mysql_select_db('GenomicBuilds');
	$query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
	$row = mysql_fetch_array($query);
	$_SESSION['dbname'] = $row['name'];
	$_SESSION['dbstring'] = $row['StringName'];
	$_SESSION['dbdescription'] = stripslashes($row['Description']);
}
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


// CHECK COOKIES
if (!isset($_COOKIE['plotwidth'])) {
	$_COOKIE['plotwidth'] = 1024;
}

// CHECK USER INFORMATION
if (isset($_POST['username']) &&  isset($_POST['password'])) {//If you entered a username and password .
	$username = $_POST['username'];
	$password = $_POST['password'];
	$query = "SELECT level, FirstName, id, projectsrun, samplesrun FROM users WHERE username = '".addslashes($_POST['username'])."' AND password=MD5('".$_POST['password']."')";
	$auth  = mysql_query("$query"); 
	if (mysql_num_rows($auth) == 0) {
		echo "wrong user/pass entered\n";
	}
	else {
		$_SESSION['logged_in'] = "true";
		$_SESSION['username'] = $_POST['username']; //Saves your username.
		$row = mysql_fetch_array($auth);
		$_SESSION['level'] = $row['level'];
		$_SESSION['FirstName'] = $row['FirstName'];
		$_SESSION['userID'] = $row['id'];
		$_SESSION['projectsrun'] = $row['projectsrun'];
		$_SESSION['samplesrun'] = $row['samplesrun'];
		$settingquery = mysql_query("SELECT DGVshow, DGVinclude, HapSame, HapAll, RefSeq, GCcode, GCncode,SegDup,LOHshow, LOHminsize, LOHminsnp, condense, condense_classes, condense_null, db_links, ecaruca,ecarucaSize,DECsyn,DECpat FROM usersettings WHERE id = '".$row['id'] ."'");
		$settingrow = mysql_fetch_array($settingquery);
		$_SESSION['DGVshow'] = $settingrow['DGVshow'];
		$_SESSION['DGVinclude'] = $settingrow['DGVinclude'];
		$_SESSION['HapSame'] = $settingrow['HapSame'];
		$_SESSION['HapAll'] = $settingrow['HapAll'];
		$_SESSION['RefSeq'] = $settingrow['RefSeq'];
		$_SESSION['GCcode'] = $settingrow['GCcode'];
		$_SESSION['GCncode'] = $settingrow['GCncode'];
		$_SESSION['SegDup'] = $settingrow['SegDup'];		
		$_SESSION['LOHshow'] = $settingrow['LOHshow'];
		$_SESSION['LOHminsize'] = $settingrow['LOHminsize'];
		$_SESSION['LOHminsnp'] = $settingrow['LOHminsnp'];
		$_SESSION['Condense'] = $settingrow['condense'];
		$_SESSION['ToCondense'] = $settingrow['condense_classes'];
		$_SESSION['NullCondense'] = $settingrow['condense_null'];
		$_SESSION['db_links'] = $settingrow['db_links'];
		$_SESSION['ecaruca'] = $settingrow['ecaruca'];
		$_SESSION['ecarucaSize'] = $settingrow['ecarucaSize'];
		$_SESSION['DECsyn'] = $settingrow['DECsyn'];
		$_SESSION['DECpat'] = $settingrow['DECpat'];
		
	}
}
ob_end_flush();
if (isset($_SESSION['logged_in'])) {
	$loggedin = 1;
	$username = $_SESSION['username'];
	$firstname = $_SESSION['FirstName'];
	$level = $_SESSION['level'];
	$userid = $_SESSION['userID'];
	$projectsrun = $_SESSION['projectsrun'];
	$samplesrun = $_SESSION['samplesrun'];
	$maxqsnpasguest = 10;
	$maxsamplesasguest = 50;
	$inbox = mysql_query("SELECT inbox.id FROM inbox WHERE inbox.to = '$userid' AND inbox.read = 0");
	if (mysql_num_rows($inbox) > 0) {
		$_SESSION['newmessages'] = 1;
	}
	else {
		$_SESSION['newmessages'] = 0;
	}

}
?>
<head>
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<meta name="verify-v1" content="XGbxV38bulqIY24GD5PeFdvg0KKw5wyVH5JlxYZc16E=" />
 	<?php echo "<base href='https://$domain/$basepath/' />"; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>CNV-WebStore: Online CNV Analysis, Storage and Interpretation (<?php echo $page;?>)</title>
	<meta name="description" content="CNV-WebStore: Analysis, storage and interpretation platform. Tailored towards but not limited to Illumina BeadArray data. Developed at Centre of Medical Genetics, University of Antwerp, Belgium" />
	<meta name="keywords" content="Illumina, BeadArray, CNV, BAF, prioritisation, Uniparental Disomy, UPD, snpTRIO, Mosaic, copy-number, copy number, HMM, hidden markov, qPCR, QuantiSNP, PennCNV, VanillaICE, primerdesign, primerdatabase, primer database, rtpcr, CNV interpretation tool" />
	<!-- Bundled General Stylesheet file -->
	<link rel='stylesheet' type='text/css' href='stylesheets/All_Format2.css'/>
	<!-- Load GENERAL JavaScripts -->
	<script type="text/javascript" src="javascripts/form_refresh.js"></script>	
	<?php
	# Load PAGE SPECIFIC scripts & styles #
	switch($page){
		case "details":
		case "diff":
		case "custom_methods":
		case "filter":
		case "cusdetails":
			echo "<script type='text/javascript' src='javascripts/rightcontext.js'></script>\n";
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/rightcontext.css' />\n";
			break;
		case "results":
			if ($_GET['type'] == 'pheno') {
				echo "<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>\n";
				echo "<link rel='stylesheet' type='text/css' href='stylesheets/clinicalpages.css' />";
			}
			break;
		case "result":
			echo "<script type='text/javascript' src='javascripts/result.js'></script>\n";
			break;
		case "cusclinical":
		case "clinical":
			echo "<script type='text/javascript' src='javascripts/bsn.AutoSuggest_2.1.3.js'></script>\n";
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/clinicalpages.css' />";
			break;
		case "tutorial":
		case "documentation":
			echo "<script type='text/javascript' src='javascripts/lightbox.js'></script>\n";
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/lightbox.css' />\n";
			break;
		case "UniSeqResult":
			echo "<script type='text/javascript' src='javascripts/popup.js'></script>\n";
			break;
		case "upload":
			echo "<script type='text/javascript' src='javascripts/upload.js'></script>\n";
			break;
		case "admin_pages":
			echo "<script type='text/javascript' src='javascripts/adminpages.js'></script>\n";
			break;

	}
	# CHECK SITE STATUS (operative / construction / LiftOver	
	$query = mysql_query("SELECT status FROM `GenomicBuilds`.`SiteStatus`");
	$row = mysql_fetch_array($query);
	$SiteStatus = $row['status'];
	switch($SiteStatus){
		case "Construction":
			echo "<link rel='stylesheet' type='text/css' href='stylesheets/construction.css'/>\n";
			echo "<script type='text/javascript' src='javascripts/construction.js'/>\n";
			break;
	}
	echo "</head>";
	#flush();
?>
<body>
	<script type="text/javascript" src="javascripts/wz_tooltip.js"></script>	
	<script type="text/javascript" src="javascripts/menu2.js"></script>
	<div id="container">    
		<div id="inhoud">
			<div id="inhoud-hoofd">
			  <div id="inhoud-hoofdlinks">
			     <h1>CNV-WebStore</h1>
					<span style='float:right;margin-bottom:-100px;font-style:italic;padding-right:5px;font-size:14px'>An Online CNV Analysis, Storage and Interpretation Platform</span>
			  </div>
 			  <div id="inhoud-hoofdrechts">
			  </div>
			</div>
			<?php
			echo "<div id='inhoud-menu'>";
			include ("inc_menu.horizontal.inc");
			flush();
			

			echo '</div>';
			echo "<div id='inhoud-rechts'> ";
			switch ($SiteStatus) {
				case "Construction":
					if ($level < 3 && $loggedin == 1) {  # This shows the blocking overlay for all users with permissions level below 3 (non-admins)
						echo "<div id='displaybox' ><br/><br/><br/><center style='font-size:18px;font-weight:bold;color:white;position:relative;top:25%' ><img src='images/content/under-construction.gif'><br/>The Platform is currently undergoing critical upgrades, making any analysis temporarily impossible. Please check back soon !<br/><br/>Thank you for your patience.</center></div>";
					}
					break;
			}
			$inc = 'page_' . $page . '.php'; 
			if (file_exists($inc)) {
				echo "<div id=\"inhoud-rechtstop\"> ";
				include ('inc_top_' . $page .'.inc');
				echo "</div>";
				include ($inc); 
			}
			else {
				echo "Page not found!\n";
			}
			?>
			</div>
			<?php
			switch($page) {	
				case 'details':
				case 'results':
				case 'cusdetails':
				case 'overview':
					echo "<br style='clear:both' />";
					echo "<div><div id=empty style='float:right;width:1px;height:300px;'></div></div>";
					break;
			}
			
			?>
		</div>

	</div>
	<!-- Google Analytics javascript -->
	<script type="text/javascript">
  		var _gaq = _gaq || [];
  		_gaq.push(['_setAccount', 'UA-11622434-1']);
  		_gaq.push(['_trackPageview']);
  		(function() {
    			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  		})();
	</script>

	</body>
</html>
