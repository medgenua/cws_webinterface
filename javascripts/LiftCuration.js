function autoSelect(radiofield, regionfield) {
    var radioObj = document.forms['myForm'].elements[radiofield];
    var regionObj = document.forms['myForm'].elements[regionfield];
    var region = regionObj.value;
    if (region == '') {
        //alert('empty field');
        radioObj[0].checked = false;
        radioObj[1].checked = false;
    }
    else {
        //alert('filled field');	
        radioObj[1].checked = true;
    }
}

var xmlhttp;
var response;
var mousey;
var oldscroll;

function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}
function getMouse(e) {
    if (!e) { e = window.event; }
    var IE = document.all ? true : false;  // TRUE is internet explorer?
    var cursor = { x: 0, y: 0 };
    if (!IE) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    }
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX +
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY +
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    return cursor.y;
}

function adjustpage() {
    if (oldscroll < getDocHeight()) {
        var missing = getDocHeight() - oldscroll + 100;
        var divh = parseInt(document.getElementById('empty').style.height);
        divh = divh + missing;
        document.getElementById('empty').style.height = divh + 'px';
    }
    else {
        if (mousey > oldscroll - 300) {
            var missing = oldscroll - mousey + 100;
            var divh = parseInt(document.getElementById('empty').style.height);
            divh = divh + missing;
            document.getElementById('empty').style.height = divh + 'px';

        }
    }

}

function ToolTip(aid, uid, table, build, e) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();
    response = '<div id=tooltip style=\'width:450px;border-color:red;\'><div style=\'text-align:center\'><img src=\'images/content/ajax-loader.gif\' height=20px></br>Loading...</div></div>';
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "tooltipLiftCuration.php";
    url = url + "?q=" + aid;
    url = url + "&u=" + uid;
    url = url + "&t=" + table;
    url = url + "&b=" + build;
    url = url + "&rv=" + Math.random();

    xmlhttp.onreadystatechange = stateChanged;
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

    return response;
}

function stateChanged() {
    if (xmlhttp.readyState == 4) {

        //document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
        document.getElementById("tooltip").innerHTML = xmlhttp.responseText;

        adjustpage();
    }
}

function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}


