function reloadspan(spanname) {
    var filename = "%maintenancedir%/LiftOver/Files/" + spanname + ".out";
    var xmlhttp = false;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "LiftOver/getLiftOutput.php";
    url = url + "?filename=" + filename;
    url = url + "&rv=" + Math.random();
    xmlhttp.open("GET", url, true);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            document.getElementById(spanname).innerHTML = xmlhttp.responseText;
            document.getElementById(spanname).scrollTop = document.getElementById(spanname).scrollHeight;
        }
    }
    xmlhttp.send(null);
}

function reloadupdatespan(rand) {
    var spanname = 'db' + rand;
    var filename = "/tmp/" + rand + ".hg.out";
    var xmlhttp = false;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "LiftOver/getLiftOutput.php";  // borrow this from the LiftOver scripts
    url = url + "?filename=" + filename;
    url = url + "&rv=" + Math.random();
    xmlhttp.open("GET", url, true);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            document.getElementById(spanname).innerHTML = xmlhttp.responseText;
            document.getElementById(spanname).scrollTop = document.getElementById(spanname).scrollHeight;
        }
    }
    xmlhttp.send(null);
}

function checkstatus(items) {
    var intInterval = 0;
    var filename = "../status/LiftOver.status";
    var xmlhttp = false;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "LiftOver/getLiftOutput.php";
    url = url + "?filename=" + filename;
    url = url + "&rv=" + Math.random();
    xmlhttp.open("GET", url, true);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            var statusvalue = xmlhttp.responseText;
            console.log(statusvalue);
            if (statusvalue == 0) {
                for (item = 1; item <= items; item = item + 1) {
                    var interval = eval("interval" + item);
                    clearInterval(interval);
                }
                document.getElementById('continuebutton').style.display = '';
                alert('done');

            }
        }
    }
    xmlhttp.send(null);
}

function checkupdatestatus(items, rand) {
    var intInterval = 0;
    var filename = "/tmp/" + rand + ".db.status";
    var xmlhttp = false;
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "LiftOver/getLiftOutput.php";
    url = url + "?filename=" + filename;
    url = url + "&rv=" + Math.random();
    xmlhttp.open("GET", url, true);
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            var statusvalue = xmlhttp.responseText;
            if (statusvalue == 0) {
                for (item = 1; item <= items; item = item + 1) {
                    var interval = eval("interval" + item);
                    clearInterval(interval);
                }
                document.getElementById('toupdates').style.display = '';
                alert('done');

            }
        }
    }
    xmlhttp.send(null);
}


function check(setthis) {
    var node_list = document.getElementsByTagName('input');
    for (var i = 0; i < node_list.length; i++) {
        var node = node_list[i];
        if (node.getAttribute('value') == setthis) {
            // do something here with a <input type="text" .../>
            // we alert its value here
            node.checked = true;
        }
    }
}

