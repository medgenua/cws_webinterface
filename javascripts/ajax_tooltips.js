var xmlhttp;
var response;
var mousey;
var oldscroll;


function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}

function getMouse(e) {
    if (!e) { e = window.event; }
    var IE = document.all ? true : false;  // TRUE is internet explorer?
    var cursor = { x: 0, y: 0 };
    if (!IE) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    }
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX +
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY +
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    return cursor.y;
}

function adjustpage() {
    var tiph = document.getElementById('tooltip').offsetHeight;
    if (oldscroll < getDocHeight()) {
        var missing = getDocHeight() - oldscroll + 100;
        var divh = parseInt(document.getElementById('empty').style.height);
        divh = divh + missing;
        document.getElementById('empty').style.height = divh + 'px';
    }
    else {
        if (mousey > oldscroll - 300) {
            var missing = oldscroll - mousey + 100;
            var divh = parseInt(document.getElementById('empty').style.height);
            divh = divh + missing;
            document.getElementById('empty').style.height = divh + 'px';

        }
    }

}

function ClearTip() {
    document.getElementById('tooltip').innerHTML = '<div style=\'text-align:center;width:450px\'><img src=\'images/content/ajax-loader.gif\' height=20px ><br/>Loading...</div>';
}


function DefaultTT(aid, uid, packing, e) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();
    response = '';
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "Browser/ToolTips/Default.php";
    url = url + "?q=" + aid;
    url = url + "&u=" + uid;
    url = url + "&packing=" + packing;
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = stateChanged;
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
    return response;
}

function CustomTT(query, trackname, uid, packing, e) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();
    response = ''
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "Browser/CustomCodes/ToolTips/" + trackname + ".php";
    url = url + "?q=" + query;
    url = url + "&u=" + uid;
    url = url + "&packing=" + packing;
    url = url + "&rv=" + Math.random();
    xmlhttp.onreadystatechange = stateChanged;
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
    return response;
}


function ToolTipUPD(aid, uid, show, fi, sid, pid, e) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();
    response = '';
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "tooltipUPD.php";
    url = url + "?q=" + aid;
    url = url + "&u=" + uid;
    url = url + "&s=" + show;
    url = url + "&fi=" + fi;
    url = url + "&sid=" + sid;
    url = url + "&pid=" + pid;
    url = url + "&rv=" + Math.random();

    xmlhttp.onreadystatechange = stateChanged;
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);

    return response;
}



function CondenseTT(aid, uid, e) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();


    response = '';
    xmlhttp = GetXmlHttpObject();
    if (xmlhttp == null) {
        alert("Browser does not support HTTP Request");
        return;
    }
    var url = "CondenseTT.php";
    url = url + "?q=" + aid;
    url = url + "&u=" + uid;
    url = url + "&rv=" + Math.random();

    xmlhttp.onreadystatechange = stateChanged;
    xmlhttp.open("GET", url, true);
    xmlhttp.send(null);
    return response;
}


function setsubtip(type, aid, userid, e) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();


    if (type == 'tools') {
        document.getElementById('UPDtable').style.display = 'none';
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linkprobes').style.textDecoration = 'none';
        document.getElementById('linktool').style.textDecoration = 'underline';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'none';
        if (document.contains(document.getElementById('linkclassifier'))) {
            document.getElementById('linkclassifier').style.textDecoration = 'none';
        }

        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "context.php";
        url = url + "?aid=" + aid;
        url = url + "&uid=" + userid;
        url = url + "&type=a";
        url = url + "&fc=1";
        url = url + "&rv=" + Math.random();
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<img src=ajaxload.gif /> Loading...";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }
    if (type == 'clinical') {
        document.getElementById('UPDtable').style.display = 'none';
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linkprobes').style.textDecoration = 'none';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'underline';
        document.getElementById('linkkaryo').style.textDecoration = 'none';
        if (document.contains(document.getElementById('linkclassifier'))) {
            document.getElementById('linkclassifier').style.textDecoration = 'none';
        }
        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "clinical.php";
        url = url + "?sid=" + aid;
        url = url + "&uid=" + userid;
        url = url + "&fc=1";
        url = url + "&rv=" + Math.random();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
                adjustpage();
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }
    if (type == 'karyo') {
        document.getElementById('UPDtable').style.display = 'none';
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linkprobes').style.textDecoration = 'none';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'underline';
        if (document.contains(document.getElementById('linkclassifier'))) {
            document.getElementById('linkclassifier').style.textDecoration = 'none';
        }

        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "smallkaryo.php";
        url = url + "?sid=" + aid;
        url = url + "&uid=" + userid;
        url = url + "&fc=1";
        url = url + "&rv=" + Math.random();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
                adjustpage();
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }

    if (type == 'upd') {
        document.getElementById('UPDtable').style.display = 'block';
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'none';
        document.getElementById('linkprobes').style.textDecoration = 'underline';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'none';
        if (document.contains(document.getElementById('linkclassifier'))) {
            document.getElementById('linkclassifier').style.textDecoration = 'none';
        }
    }


    if (type == 'plots') {
        document.getElementById('UPDtable').style.display = 'none';
        document.getElementById('plotdiv').style.display = 'block';
        document.getElementById('subtip').style.display = 'none';
        document.getElementById('linkprobes').style.textDecoration = 'none';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'underline';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'none';
        if (document.contains(document.getElementById('linkclassifier'))) {
            document.getElementById('linkclassifier').style.textDecoration = 'none';
        }

    }
    if (type == 'classifier') {
        document.getElementById('UPDtable').style.display = 'none';
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linkprobes').style.textDecoration = 'none';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'underline';
        document.getElementById('linkclassifier').style.textDecoration = 'underline';
        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "Browser/CustomCodes/ToolTips/ClassifierDetails.php";
        url = url + "?st=1&aid=" + aid;
        url = url + "&uid=" + userid;
        url = url + "&rv=" + Math.random();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
                adjustpage();
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }


}

function setcustomsubtip(type, aid, userid, e, pid) {
    mousey = getMouse(e);
    oldscroll = getDocHeight();


    if (type == 'cus_tools') {
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linktool').style.textDecoration = 'underline';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'none';
        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "cus_context.php";
        url = url + "?aid=" + aid;
        url = url + "&uid=" + userid;
        url = url + "&type=a";
        url = url + "&fc=1";
        url = url + "&rv=" + Math.random();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<img src=ajaxload.gif /> Loading...";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }
    if (type == 'cus_clinical') {
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'underline';
        document.getElementById('linkkaryo').style.textDecoration = 'none';

        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "clinical.php";
        url = url + "?sid=" + aid;
        url = url + "&uid=" + userid;
        url = url + "&fc=1";
        url = url + "&pid=" + pid;
        url = url + "&cstm=1";
        url = url + "&rv=" + Math.random();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
                adjustpage();
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }
    if (type == 'cus_karyo') {
        document.getElementById('plotdiv').style.display = 'none';
        document.getElementById('subtip').style.display = 'block';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'none';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'underline';

        response = '';
        xmlhttp = GetXmlHttpObject();
        if (xmlhttp == null) {
            alert("Browser does not support HTTP Request");
            return;
        }
        var url = "smallkaryo.php";
        url = url + "?sid=" + aid;
        url = url + "&uid=" + pid;
        url = url + "&fc=1";
        url = url + "&cstm=1";
        url = url + "&rv=" + Math.random();

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 1) {
                document.getElementById('subtip').innerHTML = "<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
            }
            if (xmlhttp.readyState == 4) {
                document.getElementById('subtip').innerHTML = xmlhttp.responseText;
                adjustpage();
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send(null);
        // return response;
    }

    if (type == 'cus_plots') {
        document.getElementById('plotdiv').style.display = 'block';
        document.getElementById('subtip').style.display = 'none';
        document.getElementById('linktool').style.textDecoration = 'none';
        document.getElementById('linkplot').style.textDecoration = 'underline';
        document.getElementById('linkclin').style.textDecoration = 'none';
        document.getElementById('linkkaryo').style.textDecoration = 'none';

    }

}

function stateChanged() {
    if (xmlhttp.readyState == 1) {
        //document.getElementById('tooltip').innerHTML="<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/>Loading...</div>";
    }
    if (xmlhttp.readyState == 4) {
        document.getElementById('tooltip').innerHTML = xmlhttp.responseText;
        adjustpage();

    }
}


function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}


