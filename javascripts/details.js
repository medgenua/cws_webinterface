function LoadMerr(sid,chrom) {
	var errresponse;
	errresponse = '<div style=\'padding-left:2em;\'><img src=\'images/content/ajax-loader.gif\' height=20px></br>Loading...</div>';
	document.getElementById('mendelian').innerHTML = errresponse;	
	var url="ajaxqueries/mendelian.php?rv="+Math.random();
	url=url+"&s="+sid+"&c="+chrom;
	var xmlhttp=GetXmlHttpObject();	
	if (xmlhttp==null) {
  		alert ("Browser does not support HTTP Request");
		return;
  	}
	xmlhttp.onreadystatechange=function () {
		if (xmlhttp.readyState==4)
		{
			document.getElementById('mendelian').innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}

