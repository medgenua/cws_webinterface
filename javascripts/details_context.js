// DEFINE CONTEXT MENU FOR ABERRATION ROWS
abrowmenu = { attributes: "aid,uid" ,

              items: [
			{type:RightContext.TYPE_TEXT_EXT,
				url:"context.php?type=a&aid=[aid]&uid=[uid]" }
		]
     };

cusabrowmenu = { attributes: "aid,uid" ,

              items: [
			{type:RightContext.TYPE_TEXT_EXT,
				url:"context.php?type=c&aid=[aid]&uid=[uid]" }
		]
     };

// ADD TO CONTEXT
RightContext.addMenu("abrow", abrowmenu);
RightContext.addMenu("cusabrow",cusabrowmenu);
// initialize RightContext
RightContext.initialize();


