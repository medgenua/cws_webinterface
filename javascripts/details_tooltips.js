var xmlhttp;
var response;
var mousey;
var oldscroll;

function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}
function getMouse(e) {
    if (!e) { e = window.event;}
    var IE = document.all?true:false;  // TRUE is internet explorer?
    var cursor = {x:0, y:0};
    if (!IE) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    } 
    else {
        var de = document.documentElement;
        var b = document.body;
        cursor.x = e.clientX + 
            (de.scrollLeft || b.scrollLeft) - (de.clientLeft || 0);
        cursor.y = e.clientY + 
            (de.scrollTop || b.scrollTop) - (de.clientTop || 0);
    }
    return cursor.y;
}

function adjustpage() {
	if (oldscroll < getDocHeight()) {
		var missing = getDocHeight() - oldscroll + 100;
		var divh = parseInt(document.getElementById('empty').style.height);
		divh = divh + missing;
		document.getElementById('empty').style.height = divh+'px';
	}
	else {
		if (mousey > oldscroll - 300) {
			var missing = oldscroll - mousey + 100;
			var divh = parseInt(document.getElementById('empty').style.height);
			divh = divh + missing;
			document.getElementById('empty').style.height = divh+'px';

		}
	}

}

function MakeTitle(aid,uid,e)
{
response = '<div style=\'width:450px\' id=tooltip>Loading...</div>';
xmlhttp=GetXmlHttpObject();
if (xmlhttp==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var url="tooltip.php";
url=url+"?q="+aid;
url=url+"&u="+uid;
url=url+"&rv="+Math.random();

xmlhttp.onreadystatechange=stateChanged;
xmlhttp.open("GET",url,true);
xmlhttp.send(null);
return response;
}


function ToolTip(aid,uid,show,fi,e)
{
mousey = getMouse(e);
oldscroll = getDocHeight();
response = '<div style=\'width:450px\' id=tooltip><div style=\'text-align:center\'><img src=\'images/content/ajax-loader.gif\' height=20px></br>Loading...</div></div>';
xmlhttp=GetXmlHttpObject();
if (xmlhttp==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var url="tooltipnew.php";
url=url+"?q="+aid;
url=url+"&u="+uid;
url=url+"&s="+show;
url=url+"&fi="+fi;
url=url+"&rv="+Math.random();

xmlhttp.onreadystatechange=stateChanged;
xmlhttp.open("GET",url,true);
xmlhttp.send(null);

return response;
}

function ToolTipUPD(aid,uid,show,fi,sid,pid,e)
{
mousey = getMouse(e);
oldscroll = getDocHeight();
response = '<div style=\'width:450px\' id=tooltip><div style=\'text-align:center\'><img src=\'images/content/ajax-loader.gif\' height=20px></br>Loading...</div></div>';
xmlhttp=GetXmlHttpObject();
if (xmlhttp==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var url="tooltipUPD.php";
url=url+"?q="+aid;
url=url+"&u="+uid;
url=url+"&s="+show;
url=url+"&fi="+fi;
url=url+"&sid="+sid;
url=url+"&pid="+pid;
url=url+"&rv="+Math.random();

xmlhttp.onreadystatechange=stateChanged;
xmlhttp.open("GET",url,true);
xmlhttp.send(null);

return response;
}


function CustomToolTip(aid,uid,show,fi,e)
{
mousey = getMouse(e);
oldscroll = document.body.scrollHeight;

response = '<div style=\'width:450px\' id=tooltip>Loading...</div>';
xmlhttp=GetXmlHttpObject();
if (xmlhttp==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var url="custom_ab_TT.php";
url=url+"?q="+aid;
url=url+"&u="+uid;
url=url+"&s="+show;
url=url+"&fi="+fi;
url=url+"&rv="+Math.random();

xmlhttp.onreadystatechange=stateChanged;
xmlhttp.open("GET",url,true);
xmlhttp.send(null);
return response;
}

function Parent(aid,parid,ppid,uid,e)
{
mousey = getMouse(e);
oldscroll = document.body.scrollHeight;

response = '<div style=\'width:450px\' id=tooltip><div style=\'text-align:center\'><img src=\'images/content/ajax-loader.gif\' height=20px><br/>Loading...</div></div>';
xmlhttp=GetXmlHttpObject();
if (xmlhttp==null)
  {
  alert ("Browser does not support HTTP Request");
  return;
  }
var url="ParentTT.php";
url=url+"?q="+aid;
url=url+"&psid="+parid;
url=url+"&ppid="+ppid;
url=url+"&u="+uid;
url=url+"&rv="+Math.random();

xmlhttp.onreadystatechange=stateChanged;
xmlhttp.open("GET",url,true);
xmlhttp.send(null);
return response;
}

function setsubtip(type,aid,userid,e,pid) 
{
   mousey = getMouse(e);
   oldscroll = document.body.scrollHeight;
   if (type == 'tools') {
	document.getElementById('plotdiv').style.display='none';
	document.getElementById('UPDtable').style.display='none';
	document.getElementById('subtip').style.display='block';
	document.getElementById('linktool').style.textDecoration='underline';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='none';
	document.getElementById('linkprobes').style.textDecoration='none';
	response = '';
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
  	alert ("Browser does not support HTTP Request");
  	return;
  	}
	var url="context.php";
	url=url+"?aid="+aid;
	url=url+"&uid="+userid;
	url=url+"&type=a";
	url=url+"&fc=1";
	url=url+"&rv="+Math.random();

	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState == 1)
		{
			document.getElementById('subtip').innerHTML="<img src=ajaxload.gif /> Loading...";
		}
		if (xmlhttp.readyState==4)
		{
			document.getElementById('subtip').innerHTML=xmlhttp.responseText;
			adjustpage();
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	// return response;
   }
   else if (type == 'clinical') {
	document.getElementById('plotdiv').style.display='none';
	document.getElementById('UPDtable').style.display='none';
	document.getElementById('subtip').style.display='block';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='underline';
	document.getElementById('linkkaryo').style.textDecoration='none';
	document.getElementById('linkprobes').style.textDecoration='none';

	response = '';
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
  	alert ("Browser does not support HTTP Request");
  	return;
  	}
	var url="clinical.php";
	url=url+"?sid="+aid;
	url=url+"&uid="+userid;
	url=url+"&fc=1";
	url=url+"&cstm=0";
	url=url+"&rv="+Math.random();

	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState == 1)
		{
			document.getElementById('subtip').innerHTML="<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
		}
		if (xmlhttp.readyState==4)
		{
			document.getElementById('subtip').innerHTML=xmlhttp.responseText;
			adjustpage();
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	// return response;
   }
   else if (type == 'karyo') {
	document.getElementById('plotdiv').style.display='none';
	//document.getElementById('UPDtable').style.display='none';
	document.getElementById('subtip').style.display='block';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='underline';
	//document.getElementById('linkprobes').style.textDecoration='none';

	response = '';
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
  	alert ("Browser does not support HTTP Request");
  	return;
  	}
	var url="smallkaryo.php";
	url=url+"?sid="+aid;
	url=url+"&uid="+userid;
	url=url+"&fc=1";
	url=url+"&rv="+Math.random();

	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState == 1)
		{
			document.getElementById('subtip').innerHTML="<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
		}
		if (xmlhttp.readyState==4)
		{
			document.getElementById('subtip').innerHTML=xmlhttp.responseText;
			adjustpage();
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	// return response;
   }

   else if (type == 'plots') {
	document.getElementById('plotdiv').style.display='block';
	document.getElementById('UPDtable').style.display='none';
	document.getElementById('subtip').style.display='none';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='underline';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='none';
	document.getElementById('linkprobes').style.textDecoration='none';

   }
   else if (type == 'upd') {
	document.getElementById('plotdiv').style.display='none';
	document.getElementById('UPDtable').style.display='block';
	document.getElementById('subtip').style.display='none';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='none';
	document.getElementById('linkprobes').style.textDecoration='underline';
   }
   else if (type == 'cus_tools') {
	document.getElementById('plotdiv').style.display='none';
	document.getElementById('subtip').style.display='block';
	document.getElementById('linktool').style.textDecoration='underline';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='none';
	response = '';
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
  	alert ("Browser does not support HTTP Request");
  	return;
  	}
	var url="context.php";
	url=url+"?aid="+aid;
	url=url+"&uid="+userid;
	url=url+"&type=c";
	url=url+"&fc=1";
	url=url+"&rv="+Math.random();

	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState == 1)
		{
			document.getElementById('subtip').innerHTML="<img src=ajaxload.gif /> Loading...";
		}
		if (xmlhttp.readyState==4)
		{
			document.getElementById('subtip').innerHTML=xmlhttp.responseText;
			adjustpage();
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	// return response;
   }

  else if (type == 'cus_clinical') {
	document.getElementById('plotdiv').style.display='none';
	document.getElementById('subtip').style.display='block';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='underline';
	document.getElementById('linkkaryo').style.textDecoration='none';

	response = '';
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
  	alert ("Browser does not support HTTP Request");
  	return;
  	}
	var url="clinical.php";
	url=url+"?sid="+aid;
	url=url+"&uid="+userid;
	url=url+"&fc=1";
	url=url+"&cstm=1";
	url=url+"&pid="+pid;
	url=url+"&rv="+Math.random();

	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState == 1)
		{
			document.getElementById('subtip').innerHTML="<img src=ajaxload.gif /> Loading...";
		}
		if (xmlhttp.readyState==4)
		{
			document.getElementById('subtip').innerHTML=xmlhttp.responseText;
			adjustpage();
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	// return response;
   }
   else if (type == 'cus_karyo') {
	document.getElementById('plotdiv').style.display='none';
	document.getElementById('subtip').style.display='block';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='none';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='underline';

	response = '';
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
  	alert ("Browser does not support HTTP Request");
  	return;
  	}
	var url="smallkaryo.php";
	url=url+"?sid="+aid;
	url=url+"&uid="+userid;
	url=url+"&fc=1";
	url=url+"&cstm=1";
	url=url+"&rv="+Math.random();

	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState == 1)
		{
			document.getElementById('subtip').innerHTML="<div style='text-align:center'><img src='images/content/ajax-loader.gif' height=20px /><br/> Loading...</div>";
		}
		if (xmlhttp.readyState==4)
		{
			document.getElementById('subtip').innerHTML=xmlhttp.responseText;
			adjustpage();
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	// return response;
   }

   else if (type == 'cus_plots') {
	document.getElementById('plotdiv').style.display='block';
	document.getElementById('subtip').style.display='none';
	document.getElementById('linktool').style.textDecoration='none';
	document.getElementById('linkplot').style.textDecoration='underline';
	document.getElementById('linkclin').style.textDecoration='none';
	document.getElementById('linkkaryo').style.textDecoration='none';

   }

   	

}


function stateChanged()
{
if (xmlhttp.readyState==4)
{

//document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
document.getElementById("tooltip").innerHTML=xmlhttp.responseText;
adjustpage();
}
}

function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}

