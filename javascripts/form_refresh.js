function reload(form) {
	var collection=form.collection.options[form.collection.options.selectedIndex].value;
	var project=form.project.options[form.project.options.selectedIndex].value;
	var sample=form.sample.options[form.sample.options.selectedIndex].value;
	self.location='index.php?page=results&type=tree&collection=' + collection + '&project=' + project +'&sample=' + sample;
}
function reloadProjects(form,type) {
	var collection = form.collection.options[form.collection.options.selectedIndex].value;
	self.location='index.php?page=projects&type='+type+'&collection='+collection;
}
function reloadcustom(form) {
	var collection=form.collection.options[form.collection.options.selectedIndex].value;
	var project=form.project.options[form.project.options.selectedIndex].value;
	var sample=form.sample.options[form.sample.options.selectedIndex].value;
	self.location='index.php?page=custom_methods&type=tree&collection=' + collection + '&project=' + project +'&sample=' + sample;
}

function reloadparserotf(form) {
	var parserid = form.parser.options[form.parser.options.selectedIndex].value;
	self.location='index.php?page=custom_methods&type=otf&parser=' + parserid ;
}


function reloadparserst(form) {
	var parserid = form.parser.options[form.parser.options.selectedIndex].value;
	self.location='index.php?page=custom_methods&type=st&parser=' + parserid ;
}

function reloadgroupinfo(form) {
	var groupid = form.joinid.options[form.joinid.options.selectedIndex].value;
	self.location='index.php?page=group&type=join&jid=' + groupid ;
}

