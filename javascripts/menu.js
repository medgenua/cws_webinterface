function maximize() {
	var ele = document.getElementById("inhoud-links");
	if(ele.style.display == "block" || ele.style.display == '') {
    		ele.style.display = "none";
		document.getElementById('inhoud-rechts').style.width = "1025px";
		document.getElementById('inhoud-rechtstop').style.width="1025px";
		Set_Cookie('showmenu', 0, 30 ,'/');
		Set_Cookie('plotwidth',1024,30,'/');
		if (document.getElementById('plotview')) {
			location.href=location.href;
		}
  	}
	else {
		ele.style.display = "block";
		document.getElementById('inhoud-rechts').style.width = "805px";
		document.getElementById('inhoud-rechtstop').style.width = "805px";
		Set_Cookie('showmenu', 1, 30 ,'/');
		Set_Cookie('plotwidth',820,30,'/');
		if (document.getElementById('plotview')) {
			location.href=location.href;
		}
	}
} 

function setmenu(toggle) {
	var menu = document.getElementById("inhoud-links");
	if(toggle == 0) {
    		menu.style.display = "none";
		document.getElementById('inhoud-rechts').style.width = "1025px";
		document.getElementById('inhoud-rechtstop').style.width="1025px";
  	}
	else {
		menu.style.display = "block";
		document.getElementById('inhoud-rechts').style.width = "805px";
		document.getElementById('inhoud-rechtstop').style.width = "805px";
	}
}

function Set_Cookie( name, value, expires, path, domain, secure )
{
var today = new Date();
today.setTime( today.getTime() );
if ( expires )
{
	expires = expires * 1000 * 60 * 60 * 24;
}
var expires_date = new Date( today.getTime() + (expires) );
document.cookie = name + "=" +escape( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
( ( path ) ? ";path=" + path : "" ) +
( ( domain ) ? ";domain=" + domain : "" ) +
( ( secure ) ? ";secure" : "" );
}

function ChangedState()
{
	if (xmlhttp.readyState==4)
	{
		document.getElementById('tooltip').innerHTML=xmlhttp.responseText;
		document.getElementById('txtHint').innerHTML=xmlhttp.responseText;
	}
}

function GXmlHttpObject()
{
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  return new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
	return null;
}



