function menuparts(ele) {
	if(document.getElementById(ele).style.display == "block" || ele.style.display == '') {
    		document.getElementById(ele).style.display = "none";
		 
  	}
	else {
		document.getElementById(ele).style.display = "block";
		
	}
} 


function toggle(id) {
	var state = document.getElementById(id).style.display;
	if (state == 'block') {
		document.getElementById(id).style.display = 'none';
	} else {
		document.getElementById(id).style.display = 'block';
	}
}

function reloadstats(spanname) {
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="inc_stats.php?";
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			document.getElementById(spanname).innerHTML = xmlhttp.responseText;		
		}
	}
	xmlhttp.send(null);
}

