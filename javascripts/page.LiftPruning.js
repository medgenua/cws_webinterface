function PreCalculate(uids, sourcedb) {
    // chiptypes
    ct = $("#chiptype").val()
    if (ct === null || ct.length == 0) {
        cts = ''
    }
    else {
        cts = ct.join(',')
    }
    // classes 
    classes = $("#class").val()
    if (classes === null || classes.length == 0) {
        classes = ''
    }
    else {
        classes = classes.join(',')
    }
    // snps
    minsnps = $("#minsnps").val()
    maxsnps = $("#maxsnps").val()
    // prefix
    prefix = $("#prefix").val()

    // send to ajax.
    data = { cts: cts, classes: classes, minsnps: minsnps, maxsnps: maxsnps, prefix: prefix, uids: uids, sourcedb: sourcedb }
    console.log(data)
    $.post("ajaxqueries/CalculateLiftNumbers.php", data, function (result) {
        $("#EvaluationResult").html("Passing Variants : " + result);
        $("#EvaluationResult").show();
        $("#PruningResult").hide();
        //console.log(result)
    })


}

function Prune(uids, sourcedb) {
    // chiptypes
    ct = $("#chiptype").val()
    if (ct === null || ct.length == 0) {
        cts = ''
    }
    else {
        cts = ct.join(',')
    }
    // classes 
    classes = $("#class").val()
    if (classes === null || classes.length == 0) {
        classes = ''
    }
    else {
        classes = classes.join(',')
    }
    // snps
    minsnps = $("#minsnps").val()
    maxsnps = $("#maxsnps").val()
    // prefix
    prefix = $("#prefix").val()

    // send to ajax.
    data = { cts: cts, classes: classes, minsnps: minsnps, maxsnps: maxsnps, prefix: prefix, uids: uids, sourcedb: sourcedb }
    console.log(data)
    $.post("ajaxqueries/PruneLiftFailures.php", data, function (result) {
        $("#PruningResult").html(result);
        $("#PruningResult").show();
        $("#EvaluationResult").hide();
        //console.log(result)
    })


}