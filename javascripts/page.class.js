function refresh() {
    $(document).ready(function() {
        $('#overlay').hide();
        location.reload();
    })
}

function updatecnvs() {
    $(document).ready(function() {
        var newcontent = "<div class='section' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p id='overlay_text'><img src='images/layout/loading.gif' />&nbsp;<br/>Just a second...</p>";
        newcontent += "<p><input id='overlay_close' type=button value='Cancel' onClick=\"refresh()\"/></p></div>";
        
        $('#overlay').html(newcontent);
        $('#overlay').show();

        // update class/inheritance
        var sid = $('#sample').val();
        var pid = $('#project').val();
        // for internal API call, need a known uid
        var uid = $('#userid').val();

        var data = {};
        $('.cnvclass').each(function(index, element) {
            data[$(this).attr('id')] = {};
            data[$(this).attr('id')]['class'] = $(element).val();
        })
        $('.inheritance').each(function(index, element) {
            data[$(this).attr('id')]['inheritance'] = $(element).val();

        })
        data = JSON.stringify(data);

        // nest AJAX calls so reload works
        // set class/inheritance
        $.ajax({
            url: 'ajaxqueries/Update_Class_Inheritance.php',
            type: 'POST',
            data: {
                data : data,
                sid : sid,
                pid : pid
            },
            success: function(data) {
                var toconfirm = data['toconfirm'];
                if (toconfirm != '') {
                    $('#overlay').hide();
                    popitup("changemultipleclasses.php?tc=" + toconfirm + "&s=" + sid + "&p=" + pid + "&");
                    return;
                }

                // merge
                interval = $('#FromTo').is(':checked');
                var cnvs_to_merge = [];
                $('.merge').each(function(index, element) {
                    if($(element).is(':checked')) {
                        cnvs_to_merge.push($(element).val())
                    }
                });
    
                if (interval || cnvs_to_merge.length > 0){
                    $.ajax({
                        url: 'api/api.php',
                        type: 'POST',
                        datatype: 'json',
                        data: {
                            request: 'mergeCNVs',
                            uid: uid,
                            sid : sid,
                            pid : pid,
                            interval: interval,
                            cnvs : cnvs_to_merge
            
                        },
                        success: function() {
                            location.reload();
            
                        },
                        error: function(xhr) {
                            if (xhr.responseJSON && xhr.responseJSON.msg) {
                                $('#overlay_text').text('ERROR:\n' + xhr.responseJSON.msg);
                                $('#overlay_close').val('OK');
                            } else {
                                $('#overlay_text').text('Unknown Error');
                                $('#overlay_close').val('OK');
                            }
                        }
                    })
                }
    
                else {
                    location.reload();
                }
            }
        })
        
    });
}


