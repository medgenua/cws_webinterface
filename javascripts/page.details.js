$(document).ready(function () {
    sid = $("#sample").val();
    pid = $("#project").val();
    scriptuser = $("#scriptuser").val();
    credentialsfile = $("#credentialsfile").val();
    error_message = $("#upd_errors").val();

    myBtn = $("#run_upd_btn");
    myBtn.click(function () {
        // check if input is OK
        if (error_message != "") {
            alert(error_message);
            return;
        }
        myBtn.addClass("button_disabled").removeClass("button").val("UPD analysis started...");
        $.ajax({
            url: 'python/launch_upd_analysis.py',
            type: 'POST',
            data: { 
                sid: sid,
                pid: pid,
                scriptuser: scriptuser,
                credentialsfile: credentialsfile
            },
            success: function(response) {
                // console.log(response);
            },
            error: function(xhr, status, error) {
                // console.error("Error:", error);
            }
        });
    });
});

