function reloadresults(spanname,outfile) {
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="getAnalysisOutput.php";
	url=url+"?filename="+outfile;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			document.getElementById(spanname).innerHTML = xmlhttp.responseText;		
			document.getElementById(spanname).scrollTop = document.getElementById(spanname).scrollHeight;
		}
	}
	xmlhttp.send(null);
}


function checkstatus(filename) {
	var intInterval = 0;
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="getAnalysisOutput.php";
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			var statusvalue = xmlhttp.responseText;
			if (statusvalue == 0) {
				window.clearInterval(intervalstatus);
				window.clearInterval(intervalresults);
				document.getElementById('formdiv').style.display = 'block'; 
		
			}
		}
	}
	xmlhttp.send(null);
}

