function GetXmlHttpObject() {
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        return new XMLHttpRequest();
    }
    if (window.ActiveXObject) {
        // code for IE6, IE5
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    return null;
}

function CheckDelete(uid) {
    // get delete array
    var chk_arr =  document.getElementsByName("deletearray[]");
    var chklength = chk_arr.length;
    if (chklength === 0) {
        return;
    }
    var newcontent = "<div class='sectie' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='images/layout/loading.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Cancel' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    //var newcontent = "<div class='displaybox' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='images/layout/loading.gif' />&nbsp;<br/>Just a second...</p>";
    //newcontent += "<p><input type=button value='Cancel' onClick=\"document.getElementById('overDiv').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML=newcontent;
    document.getElementById('overlay').style.display='';

    // get checked items
    var to_delete = '';
    for(k=0;k< chklength;k++){
        if (chk_arr[k].checked === true) {
            to_delete += chk_arr[k].value+',';
        }
    }

    // send to ajax
    if (to_delete.length > 0) {
        to_delete = to_delete.substring(0,to_delete.length-1);

        //console.log(to_delete);

        // submit to ajax.
        var xmlhttp=GetXmlHttpObject();
        if (xmlhttp === null) {
            alert ("browser does not support http request");
            return;
        }

        // onchangestate function
        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState === 4) {
                // need to reload the sample details here.
                document.getElementById('overlay').innerHTML=xmlhttp.responseText;
            }
        }
        // settings
        var url="ajaxqueries/ProjectHandler.php";
        url=url+"?uid="+uid;
        url=url+"&a=d";
        url=url+"&s="+to_delete;
        console.log(url);
        // send request with get
        xmlhttp.open("GET",url,true);
        xmlhttp.send(null);

    }

}

// DELETE SAMPLES function from page_variants.
function ConfirmedDeleteProjects(userid) {
    var to_delete = document.getElementById('todelete').value;
    var newcontent = "<div class='sectie' style='border:0.2em dashed #aaa;margin-left:20%;margin-right:20%;padding:1em;color:#fff'><span style='float:left;color:#FFF;clear:both;font-size:1.4em;font-weight:bold;margin-bottom:2em'>Processing Request</span><p><img src='images/layout/loading.gif' />&nbsp;<br/>Just a second...</p>";
    newcontent += "<p><input type=button value='Close (this might screw up your data)' onClick=\"document.getElementById('overlay').style.display='none'\" /></p></div>";

    document.getElementById('overlay').innerHTML=newcontent;
    document.getElementById('overlay').style.display='';


    // submit to ajax.
    var xmlhttp=GetXmlHttpObject();
    if (xmlhttp === null) {
        alert ("Browser does not support HTTP Request");
        return;
    }
    // onchangestate function
    xmlhttp.onreadystatechange=function() {
        if (xmlhttp.readyState === 4) {
            // need to reload the sample details here.
            document.getElementById('overlay').innerHTML=xmlhttp.responseText;
        }
    }
    // settings
    var url="ajaxqueries/ProjectHandler.php";
    url=url+"?uid="+userid;
    url=url+'&a=cd';
    url=url+"&s="+to_delete;
    console.log(url);
    // SEND REQUEST WITH GET
    xmlhttp.open("GET",url,true);
    xmlhttp.send(null);

}

