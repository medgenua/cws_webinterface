function divHide(spanid) {
	
	document.getElementById(spanid).style.display = 'none';


}
function reloadqueue(spanname) {
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="ajaxqueries/qload.php";
	url=url+"?rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			document.getElementById(spanname).innerHTML = xmlhttp.responseText;		
		}
	}
	xmlhttp.send(null);
}



function reloadresults(spanname,outfile) {
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="getAnalysisOutput.php";
	url=url+"?filename="+outfile;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			document.getElementById(spanname).innerHTML = xmlhttp.responseText;		
			document.getElementById(spanname).scrollTop = document.getElementById(spanname).scrollHeight;
		}
	}
	xmlhttp.send(null);
}


function checkstatus(filename,projectname,pid,type,suff,det,pcol) {
	var intInterval = 0;
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
  		return;
	}
	var url="getAnalysisOutput.php";
	url=url+"?filename="+filename;
	url=url+"&rv="+Math.random();
	xmlhttp.open("GET",url,true);
	xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			var statusvalue = xmlhttp.responseText;
			if (statusvalue == 0) {
				window.clearInterval(intervalstatus);
				window.clearInterval(intervalresults);
				var newcontent = '';
				newcontent = newcontent + '<p>Project ' + projectname +' finished !</p>';
				if (type == 'multiple') {
					newcontent = newcontent + '<p>Bookmark files (right click to save) / Tab delimited Results (right click to save) /  Detailed results (click to show): <ul id=ul-attach><li>Combined <a href=\"bookmarks/'+projectname+'_multi.xml\"> Bookmarks</a> / <a href=\"CNVlists/'+projectname+'_multi.txt\">TXT</a> / <a href=\"index.php?page=results&amp;type=tree&amp;collection='+pcol+'&amp;project='+pid+'&amp;sample=\">Details</a></li><li>QuantiSNP<a href=\"bookmarks/'+projectname+'_QuantiSNPv2.xml\"> Bookmarks</a> / <a href=\"CNVlists/'+projectname+'_QuantiSNPv2.txt\">TXT</a> /  <a href=\"index.php?page=detailsxml&amp;project='+projectname+'_QuantiSNPv2.xml\">Details</a> </li><li>PennCNVas<a href=\"bookmarks/'+projectname+'_PennCNVas.xml\"> Bookmarks</a> / <a href=\"CNVlists/'+projectname+'_PennCNVas.txt\">TXT</a> / <a href=\"index.php?page=detailsxml&amp;project='+projectname+'_PennCNVas.xml\">Details</a></li><li>PennCNVx<a href=\"bookmarks/'+projectname+'_PennCNVx.xml\"> Bookmarks</a> / <a href=\"CNVlists/'+projectname+'_PennCNVx.txt\">TXT</a> / <a href=\"index.php?page=detailsxml&amp;project='+projectname+'_PennCNVx.xml\">Details</a></li><li>VanillaICE<a href=\"bookmarks/'+projectname+'_VanillaICE.xml\"> Bookmarks</a> / <a href=\"CNVlists/'+projectname+'_VanillaICE.txt\">TXT</a> / <a href=\"index.php?page=detailsxml&amp;project='+projectname+'_VanillaICE.xml\">Details</a></li></ul></p><p><form action=\"index.php?page=overview&amp;p='+pid+'\" method=POST target=\"_blank\"><input type=submit class=button value=\"Graphical Project Overview\"></form></p><p><form action=\"index.php?page=create_custom_bookmark&amp;p='+pid+'\"method=POST target=\"_blank\"><input type=submit class=button value=\"Create Custom Bookmarks\"></form></p>';
				}
				else {
					newcontent = newcontent + '<p><a href=\"bookmarks/'+projectname+'_'+suff+'.xml\">Bookmark file</a><br><a href=\"index.php?page=details'+det+'&amp;project='+projectname+'_'+suff+'\">Details</a></p>';
				}
				document.getElementById('toptext').innerHTML = newcontent;
		
			}
		}
	}
	xmlhttp.send(null);
}

