var xmlhttp;
var response;


function setBuild(gb)
{
	response = '';
	xmlhttp=BuildGetXmlHttpObject();
	if (xmlhttp==null)
	  {
	  alert ("Browser does not support HTTP Request");
	  return;
	}
	var url="setBuild.php";
	url=url+"?gb="+gb;
	url=url+"&rv="+Math.random();
	xmlhttp.onreadystatechange=BuildChanged;
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
	return response;
}


function BuildChanged()
{
	if (xmlhttp.readyState==4)
	{

		// reload page
		//window.location.href=window.location.href;
		//window.opener.location.reload();
		window.location.reload();
		
	}
}

function BuildGetXmlHttpObject()
{
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  return new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		return new ActiveXObject("Microsoft.XMLHTTP");
  	}
	return null;
}

