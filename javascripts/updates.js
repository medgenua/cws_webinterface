var updates=getCookie("updates");
if (updates==null)
{
	var xmlhttp = false;
	if (window.XMLHttpRequest)
  	{
  		// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp = new XMLHttpRequest();
	}
	if (window.ActiveXObject)
  	{
  		// code for IE6, IE5
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
	if (xmlhttp==null)
 	{
  		alert ("Browser does not support HTTP Request");
	}
	else {
  	  var url="check_updates.php";
	  url=url+"?rv="+Math.random();
	  xmlhttp.open("GET",url,true);
	  xmlhttp.onreadystatechange=function() {
		if (xmlhttp.readyState==4)
		{
			var response = xmlhttp.responseText;
    			setCookie("updates",response,1);
			if (response == '1') {
				document.getElementById('updatesnotifier').style.display = '';
				document.getElementById('updatesnotifier').innerHTML = '|<a title="Updates Available" href="index.php?page=admin_pages&type=update" class=img> <img src="images/content/update_red.png" style="height:12px">&nbsp; ';
			}
			else {
				document.getElementById('updatesnotifier').style.display = 'none';
				document.getElementById('updatesnotifier').innerHTML = '';
			}
			return(1);
		}
	  
	  }
	  xmlhttp.send(null);
	}
}
//
else {
	if (updates=="1") 
	{
		document.getElementById('updatesnotifier').style.display = '';
		document.getElementById('updatesnotifier').innerHTML = '|<a title="Updates Available" href="index.php?page=admin_pages&type=update" class=img> <img src="images/content/update_red.png" style="height:12px">&nbsp; ';
	}
	else {
		document.getElementById('updatesnotifier').style.display = 'none';
		document.getElementById('updatesnotifier').innerHTML = '';
	}

}



function getCookie(c_name)
{
var i,x,y,ARRcookies=document.cookie.split(";");
for (i=0;i<ARRcookies.length;i++)
{
  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
  x=x.replace(/^\s+|\s+$/g,"");
  if (x==c_name)
    {
    return unescape(y);
    }
  }
}

function setCookie(c_name,value,exdays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate() + exdays);
var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
document.cookie=c_name + "=" + c_value;
}


