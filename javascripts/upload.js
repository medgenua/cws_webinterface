	var exts = "txt|gz|zip|rar";
	function StartUpload(f1)
	{
		if(f1.file_1.value=="")
		{
			alert('Select at least one file to upload');return false;
		}
		if(f1.targetname.value=="")
		{
			alert('You have to select a target name');return false;
		}
		if(!checkExt(f1.file_1.value))return false;
		var UID = Math.round(10000*Math.random())+'0'+Math.round(10000*Math.random());
		f1.action = f1.action.split('?')[0]+'?upload_id='+UID;
 	   	if(f1.popup.checked)
 	   	{
 	     		win1 = window.open(f1.action.split('upload.cgi')[0]+'upload_status.cgi?upload_id='+UID,'win1','width=320,height=240,resizable=1');
	      		win1.window.focus();
	    	}
	    	return true;
	}

	function checkExt(value)
	{
 	   	if(value=="")return true;
	 	var re = new RegExp("^.+\.("+exts+")$","i");
 	   	if(!re.test(value))
 	   	{
   	     		alert("This file extension is not allowed: \n" + value + "\n\nOnly these extensions are allowed: "+exts.replace(/\|/g,',')+" \n\n");
	        	return false;
	    	}
	    	return true;
	}

