<?php
session_start();
//Tell the browser what kind of file is coming in
header("Content-Type: image/png");
$filterquery = $_SESSION['filterquery'];
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$font = 'arial';
$maxsize = 249000000 ;// rounded from chromosome 1 (in build hg18)
$maxheight = 500; // max height of a chromosome, corresponds to 1
$scalef = 500/$maxsize;
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);

$chrsize = 500; #*($chrstop/$maxsize);
#$imgheight += 2*$chrsize + 20;  ## prepare for two rows of chromosomes

////////////////////////////////////////////
// STEP 1 CHECK SOME RESULTING DIMENSIONS //
////////////////////////////////////////////
$xoff = 0;
$result = mysql_query($filterquery);
$maxoffsets = array();
$chromoffsets = array();
$chromy = array();
$y = 10;
$addtox = -5;
$maxy = array();
while ($row = mysql_fetch_array($result)) {
	$chr = $row['chr'];
	$chromy[$chr] = $y;
	$start = $row['start'];
	$stop = $row['stop'];
	$cn = $row['cn'];
	if (!defined($maxy[$chr])) {
		// get size of chromosome
		$squery = mysql_query("SELECT stop FROM cytoBand WHERE chr IN ('$chr') ORDER BY stop DESC");
		$srow = mysql_fetch_array($squery);
		$maxy[$chr] = intval(round(($srow['stop'])*$scalef));
	}
	// (new chromosome )
	if ($currchr != $chr && $currchr != '') {
		// get size of chromosome
		#$squery = mysql_query("SELECT stop FROM cytoBand WHERE chr IN ('$chr') ORDER BY stop DESC");
		#$srow = mysql_fetch_array($squery);
		#$maxy[$chr] = $srow['stop'];
		if ($currcn < 2) {
			#echo "new chrom, currcn < 2";
			$maxoffsets[$currchr] = $addtox;
			$xoff = $xoff + $addtox + 30;
			$chromoffsets[$currchr] = $xoff;
			$offsets = array();
			$addtox = 0;
			#echo "chr $currchr xoff $xoff, yoff: $chromy[$currchr] and maxoff $maxoffsets[$currchr]<br>";
		}
		else {
			#echo "new chrom, currcn > 2<br>";
			$xoff += 30;
			#$chromoffsets[$chr] = $xoff;
		}
		// if too wide, next line
		if ($xoff > 780) {
			$diff = $xoff - $chromoffsets[$currchr];
			#$y = $y + $maxy + 20;
			// clear size of two last chromosomes chromosome
			$currchrsize = $maxy[$currchr];
			$chrsize = $maxy[$chr];
			unset($maxy[$chr]);
			unset($maxy[$currchr]);
			// set Y-offset
			$y = $y + max($maxy) + 20;
			$maxy = array();
			$maxy[$chr] = $chrsize;
			$maxy[$currchr] = $currchrsize;
			$chromy[$currchr] = $y;
			$chromy[$chr] = $y;
			$chromoffsets[$currchr] = $maxoffsets[$currchr] + 10 ;
			$chromoffsets[$chr] = $chromoffsets[$currchr] + 10 + $diff;
			$xoff = $chromoffsets[$currchr] + $diff;
		}
	
	}
	elseif ($currchr == '' && $cn >= 2) {
		// get size of chromosome
		#$squery = mysql_query("SELECT stop FROM cytoBand WHERE chr IN ('$chr') ORDER BY stop DESC");
		#$srow = mysql_fetch_array($squery);
		#$maxy[$chr] = $srow['stop'];
		$xoff += 10;
		$chromoffsets[$chr] = $xoff;
	}
	// same chromosome, other side of karyo OR new chromosome but only cn > 2  =>both cases: plot karyo of current chrom
	if (($currchr == $chr && $currcn < 2 && $cn >= 2 ) || ($currchr != $chr && $cn >= 2)) {
		// plot the current chromosome
		if ($currchr == $chr) {
			#echo "here with addtox for $currchr = $addtox<br>";
			$maxoffsets[$currchr] = $addtox;
			$chromoffsets[$currchr] = $xoff + $addtox + 30;
		}
		else {
			#echo "here with chromoffset  for $chr = ".($addtox +$xoff + 5)."<br>";
			$chromoffsets[$chr] = $xoff + $addtox + 30;
		}
		$xoff = $xoff + $addtox + 5 ;
		$xoff += 7;
		$offsets = array();
		$addtox = 0;
	}
	// set current cn & chr values
	$currchr = $row['chr'];
	$currcn = $row['cn'];
	// plot the aberration
	$scaledstart = intval(round(($start)*$scalef));
        $scaledstop = intval(round(($stop)*$scalef));
	if ($scaledstop - $scaledstart < 2) {
		$scaledstop = $scaledstart + 2;
	}
	if ($scaledstop > $maxy) {
		$maxy = $scaledstop;
	}
	$plotted = 0;
	foreach ($offsets as $offset => $positions) {
		$ok = 1;
		#echo "chr $chr on offset $offset: ". count($offsets[$offset]) . " entries<br/>";
		foreach($offsets[$offset] as $begin => $end) {
			if ($scaledstop >= $begin - 2 && $scaledstop <= $end) {  $ok = 0; break;}
			elseif ($scaledstart <= $end -2 && $scaledstart >= $begin ) {$ok = 0; break;}
			elseif ($scaledstart <= $begin && $scaledstop >= $end) {$ok = 0; break;}
		}
		
		if ($ok == 1) {
			// plot into current offset value
			$xpos = $xoff + $offset;
			#imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);	
			$offsets[$offset][$scaledstart] = $scaledstop;
			$plotted = 1;
			break;
		}
		
	}
	
	if ($plotted == 0) {
		$addtox += 5;
		$xpos = $xoff + $addtox;
		#imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);
		$offsets[$addtox][$scaledstart] = $scaledstop;
		if ($cn < 2) {
			krsort($offsets);
		}
		else {
			ksort($offsets);
		}
		#echo "addtox : $addtox";

	}
		
}
if ($currcn < 2) {
	// if last cnv was cn < 2, last chromosome has to be placed.
	$maxoffsets[$currchr] = $addtox;
	$chromoffsets[$currchr] = $xoff + $addtox + 30;
}


# DEFINE IMAGE PROPERTIES
#$height = $imgheight ;
$height = $y + max($maxy) + 30 ;
//Specify constant values
#$width = ($plotwidth -1) * 0.80 +5; //Image width in pixels
$width = 800;  
#$scalef = 300/$maxsize;

//Create the image resource
$image = ImageCreate($width,$height);

//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);
$blue  = imageColorallocate($image,0,0,255);
$darklightblue = ImageColorAllocate($image,122,189,255);
$lightblue = ImageColorAllocate($image,235,235,255);
$green = ImageColorAllocate($image,0,190,0);
$lightgreen = ImageColorAllocate($image,156,255,56);
$purple = ImageColorAllocate($image,136,34,135);
$lightpurple = ImageColorAllocate ($image, 208, 177, 236);
$orange = ImageColorAllocate($image,255, 179,0);
$lightorange = ImageColorAllocate($image,255,210,127);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);

#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

# CREATE KARYO BANDS
#$y = 10;  //start here
$xoff = 0;  // start here
#$idx = 0;
#$rowidx = 1;

// START QUERY:
$currchr = '';
$currcn = '';
$offsets = array();
$addtox = -5;
#$result = mysql_query($filterquery);
$row = 0;
if(!mysql_data_seek($result,$row))continue;
while ($row = mysql_fetch_array($result)) {
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$cn = $row['cn'];
	$y = $chromy[$chr];
	#$xoff = $chromoffsets[$chr];
	// (new chromosome )
	if ($currchr != $chr ) {
		// plot the chromosome
		$xoff = $chromoffsets[$chr];
			
		// get last p
		$sresult = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
		$srow = mysql_fetch_array($sresult);
		$lastp = $srow['stop'];

		// first draw chromosome
		$squery = mysql_query("SELECT start, stop, name, gieStain FROM cytoBand WHERE chr = '$chr' ORDER BY start");
		$arm = 'p';
		while ($srow = mysql_fetch_array($squery)) { 
			$cstart = $srow['start'];
			$cstop = $srow['stop'];
			$name = $srow['name'];
			$gieStain = $srow['gieStain'];
			$scaledstart = intval(round(($cstart)*$scalef));
			$scaledstop = intval(round(($cstop)*$scalef));
			if ($cstop == $lastp) {
				if ($gieStain != 'gneg') {
					imagefilledpolygon($image, array($xoff,$y+$scaledstart,$xoff+5,$y+$scaledstop,$xoff+10,$y+$scaledstart),3, $colors[$gieStain]);
				}
				imagepolygon($image, array($xoff,$y+$scaledstart,$xoff+5,$y+$scaledstop,$xoff+10,$y+$scaledstart),3,$black);
				$nexttriang = 1;
			}
			elseif ($nexttriang == 1) {
				if ($gieStain != 'gneg') {
					imagefilledpolygon($image, array($xoff+5,$y+$scaledstart,$xoff,$y+$scaledstop,$xoff+10,$y+$scaledstop),3,$colors[$gieStain]);
				}
				imagepolygon($image, array($xoff+5,$y+$scaledstart,$xoff,$y+$scaledstop,$xoff+10,$y+$scaledstop),3,$black);
				$nexttriang = 0;
			}
			else{
				if ($gieStain != 'gneg') {
					imagefilledrectangle($image, $xoff,$y+$scaledstart, $xoff+10,$y + $scaledstop, $colors[$gieStain]);
				}
				imagerectangle($image, $xoff,$y+$scaledstart, $xoff+10, $y+$scaledstop, $black);
			}
		}
		$fontwidth = imagefontwidth(1);
		$str = $chromhash[$chr];
		$txtwidth = strlen($str)*$fontwidth;
		$txtx = $xoff+ 5 - ($txtwidth/2);
		imagestring($image,1,$txtx,$y-8,$str,$black);
			
		#$xoff += 12;
		$offsets = array();
		$addtox = -5;
	}
	// same chromosome, other side of karyo OR new chromosome but only cn > 2  =>both cases: reset offsets 
	if ($currchr == $chr && $currcn < 2 && $cn >= 2 ) {
		#$xoff += 7;
		$offsets = array();
		$addtox = -5;
	}
	// set current cn & chr values²
	$currchr = $row['chr'];
	$currcn = $row['cn'];
	// plot the aberration
	$scaledstart = intval(round(($start)*$scalef));
        $scaledstop = intval(round(($stop)*$scalef));
	if ($scaledstop - $scaledstart < 2) {
		$scaledstop = $scaledstart + 2;
	}
	$plotted = 0;
	foreach ($offsets as $offset => $positions) {
		$ok = 1;
		foreach($offsets[$offset] as $begin => $end) {
			if ($scaledstop >= $begin - 2 && $scaledstop <= $end) { $ok = 0; break;}
			elseif ($scaledstart <= $end -2 && $scaledstart >= $begin ) {$ok = 0; break;}
			elseif ($scaledstart <= $begin && $scaledstop >= $end) {$ok = 0; break;}
		}
		
		if ($ok == 1) {
			// plot into current offset value
			if ($cn < 2) {
				$xpos = $chromoffsets[$chr] - 5 - $offset;
			}
			else {
				$xpos = $chromoffsets[$currchr] + 12 + $offset;
			}
			imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);	
			$offsets[$offset][$scaledstart] = $scaledstop;
			$plotted = 1;
			break;
		}
		
	}
	
	if ($plotted == 0) {
		$addtox += 5;
		if ($cn < 2) {
			$xpos = $chromoffsets[$chr] - 5 - $addtox;
		}
		else {
			$xpos = $chromoffsets[$currchr] + 12 + $addtox;
		}

		#$xpos = $xoff + $addtox;
		imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+3,$y+$scaledstop,$cns[$cn]);
		$offsets[$addtox][$scaledstart] = $scaledstop;
		#if ($cn < 2) {
		ksort($offsets);
		#}
		#else {
		#	ksort($offsets);
		#}

	}
		
}
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 


?>

