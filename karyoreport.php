<?php
header("Content-Type: image/png");
session_start();
//Tell the browser what kind of file is coming in
header("Content-Type: image/png");
#$_SESSION['abstoplot']['1'][] = '1000000|3000000|1';
#$_SESSION['abstoplot']['3'][] = '21512354|22111111|3';
#$_SESSION['abstoplot']['23'][] = '125000000|125020000|0';


#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial';
$maxsize = 249000000 ;// rounded from chromosome 1 (in build hg18)
$maxheight = 300; // max height of a chromosome, corresponds to 1
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
# GET POSTED VARS
#$samplename = $_GET['sample'];
#$samplename = "testsample";
# get chromosomes to plot
$toplot = array();
$imgheight = 0;
$rowidx = 0;
foreach ($abstoplot as $chr => $content) {
	$toplot[$chr] = 1;
}

# find largest chromosome
$subquery = mysql_query("SELECT stop FROM cytoBand ORDER BY stop DESC LIMIT 1");
$subrow = mysql_fetch_array($subquery);
$chrstop = $subrow['stop'];
$chrsize = 300*($chrstop/$maxsize);
$imgheight += $chrsize + 20;

# DEFINE IMAGE PROPERTIES
$height = $imgheight ;

//Specify constant values
#$width = ($plotwidth -1) * 0.80 +5; //Image width in pixels
$width = 700;  // 10 per chr, 6 aan elke kant voor cnvs, 5 spacing tussen elk + marge
$scalef = 300/$maxsize;
//Create the image resource
$image = ImageCreate($width,$height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);
$blue  = imageColorallocate($image,0,0,255);
$darklightblue = ImageColorAllocate($image,122,189,255);
$lightblue = ImageColorAllocate($image,235,235,255);
$green = ImageColorAllocate($image,0,190,0);
$lightgreen = ImageColorAllocate($image,156,255,56);
$purple = ImageColorAllocate($image,136,34,135);
$lightpurple = ImageColorAllocate ($image, 208, 177, 236);
$orange = ImageColorAllocate($image,255, 179,0);
$lightorange = ImageColorAllocate($image,255,210,127);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);

#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

# CREATE SCALE
# CREATE KARYO BANDS
$y = 10;  //start here
$xoff = -18;  // start here
$idx = 0;
$rowidx = 1;

for ($i=1;$i<=24;$i++) {
	$xoff += 27;
	// get last p
	$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$i' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
	$row = mysql_fetch_array($result);
	$lastp = $row['stop'];

	// first draw chromosome
	$query = mysql_query("SELECT start, stop, name, gieStain FROM cytoBand WHERE chr = '$i' ORDER BY start");
	$arm = 'p';
	$nexttriang = 0;
	while ($row = mysql_fetch_array($query)) { 
		$cstart = $row['start'];
		$cstop = $row['stop'];
		$name = $row['name'];
		$gieStain = $row['gieStain'];
		$scaledstart = intval(round(($cstart)*$scalef));
		$scaledstop = intval(round(($cstop)*$scalef));
		if ($cstop == $lastp) {
			if ($gieStain != 'gneg') {
				imagefilledpolygon($image, array($xoff,$y+$scaledstart,$xoff+5,$y+$scaledstop,$xoff+10,$y+$scaledstart),3, $colors[$gieStain]);
			}
			imagepolygon($image, array($xoff,$y+$scaledstart,$xoff+5,$y+$scaledstop,$xoff+10,$y+$scaledstart),3,$black);
			$nexttriang = 1;
		}
		elseif ($nexttriang == 1) {
				if ($gieStain != 'gneg') {
				imagefilledpolygon($image, array($xoff+5,$y+$scaledstart,$xoff,$y+$scaledstop,$xoff+10,$y+$scaledstop),3,$colors[$gieStain]);
			}
			imagepolygon($image, array($xoff+5,$y+$scaledstart,$xoff,$y+$scaledstop,$xoff+10,$y+$scaledstop),3,$black);
			$nexttriang = 0;
		}
		else{
			if ($gieStain != 'gneg') {
				imagefilledrectangle($image, $xoff,$y+$scaledstart, $xoff+10,$y + $scaledstop, $colors[$gieStain]);
			}
			imagerectangle($image, $xoff,$y+$scaledstart, $xoff+10, $y+$scaledstop, $black);
		}
		

	}
	$fontwidth = imagefontwidth(1);
	$str = $chromhash[$i];
	$txtwidth = strlen($str)*$fontwidth;
	$txtx = $xoff+ 5 - ($txtwidth/2);
	imagestring($image,1,$txtx,$y-8,$str,$black);
	// PLOT ABERRATIONS
	if (!array_key_exists($i,$toplot)) {
		continue;
	}
		
	foreach($abstoplot[$i] as $key => $data) {
	#while ($row = mysql_fetch_array($query)) {
		$pieces = explode('|',$data);
		$ccn = $pieces[2];
		$cstart = $pieces[0];
                $cstop = $pieces[1];
                $scaledstart = intval(round(($cstart)*$scalef));
                $scaledstop = intval(round(($cstop)*$scalef));
		if ($scaledstop - $scaledstart < 2) {
			$scaledstop = $scaledstart + 2;
		}
		if ($ccn <= 2) {
			$xpos = $xoff - 6;	
		}
		else {
			$xpos = $xoff + 12;
		}
		## custom  copynumber ?
		
		imagefilledrectangle($image,$xpos,$y+$scaledstart,$xpos+4,$y+$scaledstop,$cns[$ccn]);	
	}
	

	
}
//Output the newly created image in jpeg format
imagepng($image,"$sitedir/Reports/$samplename.png");

//Free up resources
ImageDestroy($image); 
?>

