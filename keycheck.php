<?php
$key = $_GET['key'];
if (!file_exists("data/$key")) {
	echo "INVALID KEY";
	exit;
}
$fh = fopen("data/$key", 'r');
$line = fgets($fh);
fclose($fh);
$line = rtrim($line);
$file = "data/$line.tar.gz";
if (file_exists($file)) {
	echo "$line";
}
else {
	echo "ARCHIVE NOT FOUND";
}
?>
