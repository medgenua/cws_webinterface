<?php
## get session vars
$currentdb = "CNVanalysis".$_SESSION['dbname'];
$cdbname = $_SESSION['dbstring'];
$output = $_GET['output'];
## get source build (compare to current)
if ($userid == 6 || $userid == 29 ) {
	$userpart = "pp.userid IN (6,29)";
}
else {
	$userpart = "pp.userid = '$userid'";

}
## get source build (compare to current)
if (isset($_POST['sdb'])) {
	$sourcebuild = $_POST['sdb']; 
	$sourcedb = "CNVanalysis".$sourcebuild;
}
$query = mysql_query("SELECT name, StringName FROM `GenomicBuilds`.PreviousBuilds");
if (mysql_num_rows($query) == 0) {
	echo "<div class=sectie>";
	echo "<h3>Comparing Gene Content Between Genome Builds</h3>";
	echo "<p>There is no previous Genomic Build available for comparison.</p>";
	exit();
}
echo "<div class=sectie>";
echo "<h3>Select Source Build For Comparison</h3>";
echo "<form action='index.php?page=LiftCompareGenes&output=$output' method=POST>";
echo "<p>Select a Genome Build to compare to the current build version ($cdbname): <select name=sdb><option value=''>Choose a Build</option>";
while ($row = mysql_fetch_array($query)) {
	$odbname = $row['StringName'];
	$odb = $row['name'];
	if ($odb == $sourcebuild) {
		$selected = 'SELECTED';
	}
	else {
		$selected = '';
	}
	echo "<option value='$odb' $selected>$odbname</option>";
}
echo "</select><input type=submit class=button name=submit value='Go'></form></p>";
echo "</div>";
if ($sourcedb == '') {
	exit();
}

## Get regions, ordered by sample, chr, position.
$query = mysql_query("SELECT a.id, a.sample, s.chip_dnanr, a.chr, a.start, a.stop,a.cn,a.class,a.inheritance,a.idproj FROM aberration a JOIN sample s JOIN projectpermission pp JOIN project p ON a.sample = s.id AND a.idproj = pp.projectid AND a.idproj = p.id WHERE $userpart AND s.intrack = 1 AND s.trackfromproject = a.idproj AND p.collection <> 'Controls' GROUP BY a.id ORDER BY a.chr, a.start");
$nrabs = mysql_num_rows($query);
## set limit on printing for now
$limit = $_GET['view'];
#$limit = $nrabs;
if ($limit == '') {
	$limit = 25;
}
$skip = $_GET['skip'];
if ($skip == '') {
	$skip = 0;
}
$csample = '';
$nrprinted = 0;

// variables
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_end_flush();
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
if ($output == 'file') {
	$limit = $nrabs;
	$fh = fopen("/tmp/$userid.GeneComparison.txt","w");
	$line = "Sample Name\t$odbname Region\t$cdbname Region\tCopy Number\tDiagnostic Class\tInheritance\tUnique to $odbname\tUnique to $cdbname\tOMIM/MORBID entries\n";
	fwrite($fh,$line);
}
// get public resources ids
$squery = mysql_query("SELECT db_links FROM usersettings WHERE id = '$userid'");
$srow = mysql_fetch_array($squery);
$db_links = $srow['db_links'];
$extracoltop = "";

## process
echo "<div class=sectie>";
echo "<h3>Comparing Gene Content Between Genome Builds</h3>";
if ($output == 'file') {
	echo "<p>$nrabs Regions are now being checked for gene content differences between Genome Builds. When finished, the resulting file will be presented for download. To browse the results on-site in block of 25 items,<a href='index.php?page=LiftCompareGenes&view=25&skip=0'> click here</a></p>";
}
else {
	echo "<p>$nrabs Regions are now being checked for gene content differences between Genome Builds. Only the first $limit discrepancies will be printed. To get a tab-seperated file containing all the regions,<a href='index.php?page=LiftCompareGenes&output=file'> click here</a></p>";
	$next = $skip + $limit;
	$prev = $skip - $limit;
	if ($prev < 0) {
		$prev = 0;
	}
	echo "<p>";
	if ($skip > 0) {
		echo "<a href='index.php?page=LiftCompareGenes&view=$limit&skip=$prev'>Previous</a> | ";
	}
	echo "Result $skip-$next | <a href='index.php?page=LiftCompareGenes&view=$limit&skip=$next'>Next</a></p>";
}
flush();
ob_flush();

while ($row = mysql_fetch_array($query)) {
	if ($nrprinted >= ($limit+$skip) ) {
		break;
	}
	$sname = $row['chip_dnanr'];
	if (substr($sname,0,2) == 'NA') {
		# skip hapmap
		continue;
	}
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$newregion = "Chr".$chromhash[$chr] . ":".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
	$aid = $row['id'];
	$class = $row['class'];
	if ($class == '') {
		$class = 'Not Defined';
	}
	$cn = $row['cn'];
	$inher = $inh[$inh[$row['inheritance']]];
	if ($inher == '') {
		$inher = 'Not Defined';
	}
	$sid = $row['sample'];
	$pid = $row['idproj'];
	$oldgenes = array();
	$newgenes = array();
	$oldsyns = array();
	$newsyns = array();
	# get old values 
	$squery = mysql_query("SELECT chr, start, stop FROM `$sourcedb`.aberration WHERE id = '$aid'");
	if (mysql_num_rows($squery) == 0) {
		#echo  " not present in old build SELECT chr, start, stop FROM `$sourcedb`.aberration WHERE id = '$aid'<br/>";
		continue;
	}
	$oldrow = mysql_fetch_array($squery);
	$oldchr = $oldrow['chr'];
	$oldstart = $oldrow['start'];
	$oldstop = $oldrow['stop'];
	$oldregion = "Chr".$chromhash[$oldchr] . ":".number_format($oldstart,0,'',',').'-'.number_format($oldstop,0,'',',');
	## get genes on new assembly
	$newgenes = array();
	$results = mysql_query("SELECT symbol, omimID, morbidTXT, geneTXT, geneID,synonyms FROM `$currentdb`.genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");
	while ($srow = mysql_fetch_array($results)) {
		$symbol = $srow['symbol'];
		$newgenes[$symbol]['geneTXT'] = $srow['geneTXT'];
		$newgenes[$symbol]['omim'] = $srow['omimID'];
		$newgenes[$symbol]['morbid'] = $srow['morbidTXT'];
		$newgenes[$symbol]['geneid'] = $srow['geneID'];
		$synonyms = $srow['synonyms'];
		$syns = explode('|',$synonyms);
		foreach ($syns as $idx => $syn) {
			$newsyns[$syn] = $symbol;
		}
	}
	## get genes on old assembly
	$oldgenes = array();
	$results = mysql_query("SELECT symbol, omimID, morbidTXT, geneTXT, geneID,synonyms FROM `$sourcedb`.genesum WHERE chr=$oldchr AND ( (start BETWEEN $oldstart AND $oldstop) OR (stop BETWEEN $oldstart AND $oldstop) OR (start <= $oldstart AND stop >= $oldstop) ) ORDER BY symbol");
	while ($srow = mysql_fetch_array($results)) {
		$symbol = $srow['symbol'];
		$oldgenes[$symbol]['geneTXT'] = $srow['geneTXT'];
		$oldgenes[$symbol]['omim'] = $srow['omimID'];
		$oldgenes[$symbol]['morbid'] = $srow['morbidTXT'];
		$oldgenes[$symbol]['geneid'] = $srow['geneID'];
		$synonyms = $srow['synonyms'];
		$syns = explode('|',$synonyms);
		foreach ($syns as $idx => $syn) {
			$oldsyns[$syn] = $symbol;
		}

	}
	## compare
	$oldunique = '';
	$oldtxt = '';
	$newunique = '';
	$newtxt = '';
	$morbids = '';
	foreach ($oldgenes as $symbol => $array) {
		if (isset($newgenes[$symbol])) {
			# ok, seen in new
			continue;
		}
		if (isset($newsyns[$symbol])) {
			# name changed: old reference name is synonym now.
			continue;
		}
		# last option. original reference name is lost (not even a synonym now), try synonyms.
		$found = 0;
		foreach($oldsyns as $syn => $gene) {
			if ($gene != $symbol) {
				continue;
			}
			if (isset($newgenes[$syn]) || isset($newsyns[$syn])) {
				$found = 1;
				break;
			}
		}
		if ($found == 1) {
			continue;
		}
		# not seen, add to oldunique list
		$geneTXT = $array['geneTXT'];
		$geneid = $array['geneid'];
		$omimID = $array['omim'];

		$oldtxt .= "$symbol,";
		$morbidTXT = $array['morbid'];
		if ($morbidTXT != '') {
			$morbidTXT = preg_replace('/^(\|)(.*)/','$2',$morbidTXT);
			$morbidTXT = str_replace('|','; ',$morbidTXT);
			$morbidTXT = preg_replace('/(.*),\s\d+\s.*$/','$1',$morbidTXT);
			$morbids .= "$symbol:$omimID:$morbidTXT; ";
		}
		elseif ($omimID != '') {
			$morbids .= "$symbol:$omimID; ";
		}
		$oldunique .= "<li ><span title=\"$geneTXT $morbidTXT\"><u>$symbol</u> </span>:  ";
		if ($omimID != 0) {
			$oldunique .= " <a href=\"http://www.omim.org/entry/$omimID\" target='_blank' class=italic>OMIM</a>, ";
		}
		$ssquery = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=1 ORDER BY Resource");
		$nrlinks = mysql_num_rows($ssquery);
		if ($nrlinks > 0) {
			while ($ssrow = mysql_fetch_array($ssquery)) {
				$dbname = $ssrow['Resource'];
				$link = $ssrow['link'];
				$title = $ssrow['Description'];
				$link = str_replace('%s',$symbol,$link);
				$link = str_replace('%o',$omimID,$link);	
				$link = str_replace('%g',$geneid,$link);
				$link = str_replace('%u',$ucscdb,$link);
				$oldunique .= "<a title='$title' href='$link' target='_blank'>$dbname</a>, ";
			}
		}
		$oldunique = substr($oldunique,0,-2) . "</li>\n";
	}
	foreach ($newgenes as $symbol => $array) {
		if (isset($oldgenes[$symbol])) {
			# ok, seen in new
			continue;
		}
		if (isset($oldsyns[$symbol])) {
			# name changed: new reference name was old synonym.
			continue;
		}
		# last option. New reference name was not known in old build (not even a synonym now), try synonyms.
		$found = 0;
		foreach($newsyns as $syn => $gene) {
			if ($gene != $symbol) {
				continue;
			}
			if (isset($oldgenes[$syn]) || isset($oldsyns[$syn])) {
				$found = 1;
				break;
			}
		}
		if ($found == 1) {
			continue;
		}
		# not seen, add to newunique list
		$geneTXT = $array['geneTXT'];
		$geneid = $array['geneid'];
		$omimID = $array['omim'];

		$newtxt .= "$symbol,";
		$morbidTXT = $array['morbid'];
		if ($morbidTXT != '') {
			$morbidTXT = preg_replace('/^(\|)(.*)/','$2',$morbidTXT);
			$morbidTXT = str_replace('|','; ',$morbidTXT);
			$morbidTXT = preg_replace('/(.*),\s\d+\s.*$/','$1',$morbidTXT);
			$morbids .= "$symbol:$omimID:$morbidTXT; ";
		}
		elseif ($omimID != '') {
			$morbids .= "$symbol:$omimID; ";
		}
		$newunique .= "<li ><span title=\"$geneTXT $morbidTXT\"><u>$symbol</u> </span>:  ";
		if ($omimID != 0) {
			$newunique .= " <a href=\"http://www.omim.org/entry/$omimID\" target='_blank' class=italic>OMIM</a>, ";
		}
		$ssquery = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=1 ORDER BY Resource");
		$nrlinks = mysql_num_rows($ssquery);
		if ($nrlinks > 0) {
			while ($ssrow = mysql_fetch_array($ssquery)) {
				$dbname = $ssrow['Resource'];
				$link = $ssrow['link'];
				$title = $ssrow['Description'];
				$link = str_replace('%s',$symbol,$link);
				$link = str_replace('%o',$omimID,$link);	
				$link = str_replace('%g',$geneid,$link);
				$link = str_replace('%u',$ucscdb,$link);
				$newunique .= "<a title='$title' href='$link' target='_blank'>$dbname</a>, ";
			}
		}
		$newunique = substr($newunique,0,-2) . "</li>\n";
	}
	if ($oldunique != '' || $newunique != '') {
		$nrprinted++;
		if ($output == 'file') {
			$line = "$sname\t$oldregion\t$newregion\t$cn\t$class\t$inher\t$oldtxt\t$newtxt\t$morbids\n";
			fwrite($fh,$line);
		}
		else {
			if ($nrprinted <= $skip) {
				continue;
			}
			echo "<p><table cellspacing=0 style='width:90%'>";
			echo "<tr><th colspan=2 $firstcell>$sname | $odbname : $oldregion | $cdbname : $newregion</th></tr>";
			echo "<tr><th class=specalt colspan=2>CopyNumber : $cn | Diagnostic Class : $class | Inheritance : $inher <span style='float:right'><a class=ttsmall href='index.php?page=details&sample=$sid&project=$pid' target='_blank'>Go To Sample Details</a></span></th></tr>";
			echo "<tr><th class=specalt>Unique to $odbname</th><th class=specalt>Unique to $cdbname</th></tr>";
			echo "<tr><td $firstcell width='50%'><ul class=genes>$oldunique</ul></td>";
			echo "<td width='50%'><ul class=genes >$newunique</ul></td>";
			echo "</tr>";
			echo "</table></p>";
			flush();
			ob_flush();
		}
	}

}
if ($output == 'file') {
	fclose($fh);
	echo "in total: $nrprinted discrepancies were detected<br/>";
	echo "<p>";
	echo "<iframe height='5' width='5' style='display:none' src='download.php?path=/tmp/&file=$userid.GeneComparison.txt'></iframe>\n";
}
?>
