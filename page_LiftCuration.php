<?php
ob_end_flush();
// check if logged in
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
        include('login.php');
        echo "</div>";
        exit();
}

## javascript
?>
<script type="text/javascript" src="javascripts/LiftCuration.js"></script>
<?php
################
## CHROM HASH ##
################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["25"] = "M";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["M"] = "25";


/////////////////////////////
// process submitted items //
/////////////////////////////
if (isset($_POST['CurateSubmit'])) {
	$shortname = $_SESSION['dbname'];
	$tabledone = $_POST['tablename'];
	$sourcebuild = $_POST['sourcebuild'];
	if ($userid == 6 || $userid == 29) {
		$uidpart = "uid IN (6,29)";
	}
	else {
		$uidpart = "uid = '$userid'";
	}
	#if ($userid == 1) {
		
	$query = mysql_query("SELECT itemID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tabledone' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart");
	// check all failed items from selected table
	while ($row = mysql_fetch_array($query)) {
		$aid = $row['itemID'];
		$radioname = $tabledone . '_' . $row['itemID'];
		$fieldname = $radioname . "_remapped";
		// checkbox set for item
		if (isset($_POST[$radioname])) {
			$value = $_POST[$radioname];
			if ($value == 'D') {
                // info
                $item_query = mysql_query("SELECT sample, idproj FROM `deletedcnvs` WHERE id = '$aid'");
                $item_row = mysql_fetch_array($item_query);
                $item_sid = $item_row['sample'];
                $item_pid = $item_row['idproj'];
                // log
                //trigger_error("INSERT INTO `log` (`sid`, `pid`, `aid`, `uid`, `entry`, `arguments`) VALUES ('$item_sid', '$item_pid', '$aid', '$userid', 'Deleted during manual liftOver curation', '')");
                mysql_query("INSERT INTO `log` (`sid`, `pid`, `aid`, `uid`, `entry`, `arguments`) VALUES ('$item_sid', '$item_pid', '$aid', '$userid', 'Deleted during manual liftOver curation', '')");
				// delete item
				mysql_query("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tabledone' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart AND itemID = '$aid'");
			}
			elseif (isset($_POST[$fieldname])) {
				// remap set; check region, update and restore if correct region
				$newregion = $_POST[$fieldname];
				trim($newregion);
				$newregion = str_replace(",","",$newregion);
				if (preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $newregion)) {	
					$pieces = preg_split("/[:\-_+]/",$newregion);
					$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
					$chr = $chromhash[ $chrtxt ] ;
					$start = $pieces[1];
					$end = $pieces[2];
					$end = str_replace(",","",$end);
					$start = str_replace(",","",$start);
					$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
					#echo "Found Region : chr$chr:$start,$end<br/>";
				}
				else {
					echo "invalid region format detected, skipping ($newregion)<br/>";
					continue;
				}
				// restore item to tables for new build. 
				$insquery = "INSERT INTO `CNVanalysis$shortname`.$tabledone SELECT * FROM `CNVanalysis$sourcebuild`.$tabledone WHERE id = '$aid'";
				mysql_query($insquery);
				$updquery = "UPDATE `CNVanalysis$shortname`.$tabledone SET chr = '$chr', start = '$start', stop = '$end', LiftedFrom = '$sourcebuild' WHERE id = '$aid'";
				mysql_query($updquery);
				if ($tabledone == 'aberration') {
					$squery = mysql_query("SELECT idproj, sample FROM `CNVanalysis$shortname`.aberration WHERE id = '$aid'");
					$srow = mysql_fetch_array($squery);
					$sid = $srow['sample'];
					$pid = $srow['idproj'];
                    // delete from deleted table
					$delq = "DELETE FROM `CNVanalysis$shortname`.`deletedcnvs` WHERE id = '$aid'";
					mysql_query($delq);
                    // add to log
					$logq = "INSERT INTO `CNVanalysis$shortname`.`log` (sid, pid, aid, uid, entry, arguments) VALUES ('$sid', '$pid', '$aid','$userid', 'Restored After Manual liftOver curation', '')";
					mysql_query($logq);
                    // add remark
                    mysql_query("UPDATE TABLE `CNVanalysis$shortname`.$tabledone  SET Remarks = CONCAT(Remarks,'Manually Lifted|')");
				}
				elseif ($tabledone == 'cus_aberration') {
					$squery = mysql_query("SELECT idproj, sample FROM `CNVanalysis$shortname`.cus_aberration WHERE id = '$aid'");
					$srow = mysql_fetch_array($squery);
					$sid = $srow['sample'];
					$pid = $srow['idproj'];
					$delq = "DELETE FROM `CNVanalysis$shortname`.`cus_deletedcnvs` WHERE id = '$aid'";
					mysql_query($delq);
					$logq = "INSERT INTO `CNVanalysis$shortname`.`cus_log` (sid, pid, aid, uid, entry, arguments) VALUES ('$sid', '$pid', '$aid','$userid', 'Restored After Lifting', '')";
					mysql_query($logq);
				}
				// remove from failed items
				mysql_query("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tabledone' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart AND itemID = '$aid'");
			
				// item restored.	

			}
		}
	}
	

}


echo "<h3>Prune Failed Lifts</h3>";
echo "<p>Before manually curating CNVs, subsets of failed lifs can be automatically discarded in bulk: It's recommended to follow these options in order</p>";
echo "<p><ol>";
echo "  <li><a href='index.php?page=LiftPruning&type=collection'>By Collection</a></li>";
echo "  <li><a href='index.php?page=LiftPruning&type=expdet'>By Experimental Details</a></li>";
echo "</ol></p>";


// select chromosome to handle
if (isset($_GET['chr'])) {
    $set_chr = $_GET['chr'];
}
elseif (isset($_POST['chr'])) {
    $set_chr = $_POST['chr'];
}
else {
    $set_chr = '';
}


//////////////////////////////////////////////////////////////////////////
// process remaining per source build, then table, then per chromosome. //
//////////////////////////////////////////////////////////////////////////

// get source build
$shortname = $_SESSION['dbname'];
$ucscto = substr($shortname,1);
if ($userid == 6 || $userid == 29) {
		$uidpart = "uid IN (6,29)";
	}
	else {
		$uidpart = "uid = '$userid'";
	}

$query = mysql_query("SELECT DISTINCT(frombuild) as sourcedb FROM `GenomicBuilds`.`FailedLifts` WHERE tobuild = '$shortname' AND $uidpart LIMIT 1");
if (mysql_num_rows($query) == 0) {
	echo "<div class=sectie><h3>Curation Finished</h3>\n";
        echo "<p>No more regions are left to curate. <a href='index.php'>Click here to go back to the main page</a>.</p>";
	echo "</div>";
        exit();
}
$row = mysql_fetch_array($query);
$sourcebuild = $row['sourcedb'];
$sourcedb = "CNVanalysis$sourcebuild";
$ucscfrom = substr($sourcebuild,1);
echo "<div class=sectie>";
echo "<h3>Curation Lifts from UCSC$sourcebuild to UCSC$shortname</h3>";

// get table
$query = mysql_query("SELECT DISTINCT(tablename) as tablename FROM `GenomicBuilds`.`FailedLifts` WHERE tobuild = '$shortname' AND frombuild = '$sourcebuild' AND $uidpart ORDER BY tablename  LIMIT 1");
$row = mysql_fetch_array($query);
$tablename = $row['tablename'];


//////////////////////////////////////////////////////////////
// IF PARENTS_REGIONS, FIRST CHECK ACTIONS ON CHILD REGIONS //
//////////////////////////////////////////////////////////////
if ($tablename == 'parents_regions') {
	$rescued = 0;
	$query = mysql_query("SELECT itemID, ID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tablename' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart");
	$rows = mysql_num_rows($query);
	echo "Automatically updating parental regions based on supplied offspring data: $rows items to do: ";
	flush();
	while ($row = mysql_fetch_array($query)) {
		// get corresponding offpring aid
		$aid = $row['itemID'];	
		$failedid = $row['ID'];
		$squery = mysql_query("SELECT po.aid, pr.avgLogR, pr.sid FROM `CNVanalysis$sourcebuild`.parent_offspring_cnv_relations po JOIN `CNVanalysis$sourcebuild`.parents_regions pr ON po.parent_aid = pr.id AND po.parent_sid = pr.sid WHERE pr.id = '$aid'");
		$srow = mysql_fetch_array($squery);
		$oaid = $srow['aid'];
		$osid = $srow['sid'];
		$avgLogR = $srow['avgLogR'];
		// was it lifted ?
		$squery = mysql_query("SELECT chr, start, stop FROM `CNVanalysis$shortname`.aberration WHERE id = '$oaid'");
		if (mysql_num_rows($squery) == 0) {
			#echo "FAILED parent aid : $aid => FAILED Offspring aid : $oaid<br/>";
		}
		else {
			$srow = mysql_fetch_array($squery);
			$newchr = $srow['chr'];
			$newstart = $srow['start'];
			$newstop = $srow['stop'];
			$rescued++;
			mysql_query("INSERT INTO `CNVanalysis$shortname`.parents_regions (id, chr, start, stop, sid, avgLogR, LiftedFrom) VALUES ('$aid', '$newchr', '$newstart', '$newstop', '$osid','$avgLogR', '$sourcebuild')");
			mysql_query("DELETE FROM `GenomicBuilds`.FailedLifts WHERE ID = '$failedid'");
			echo "FAILED parent aid : $aid => LIFTED Offspring aid : $oaid : chr$newchr:$newstart-$newstop<br/>";
		}
	}
	echo "Rescued $rescued items.</p>";
	flush();

}

##################################
## PROCEED WITH MANUAL CURATION ##
##################################
echo "<p><span class=bold>Table : $tablename : </span>";
// get itemIDs for current table
$query = mysql_query("SELECT itemID FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tablename' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart");
$total_vars = mysql_num_rows($query);
echo " (total items left: $total_vars) ";
$instring = '';
while ($row = mysql_fetch_array($query)) {
	$instring .= $row['itemID'].",";
}
$instring = substr($instring,0,-1);
// compose query depending on table
if ($tablename == 'aberration') {
    $a_classes = array();
    $q = mysql_query("SELECT abbreviated_name, id FROM `diagnostic_classes`");
    while ($r = mysql_fetch_array($q)) {
        $a_classes[$r['id']] = $r['abbreviated_name'];
    }
	//$qs =  "SELECT a.id AS aid, a.chr AS chr, a.start AS start, a.stop AS stop, s.chip_dnanr AS sample, d.abbreviated_name AS Class FROM `$sourcedb`.`diagnostic_classes` d JOIN `$sourcedb`.`$tablename` a JOIN `$sourcedb`.`sample` s ON a.class = d.id AND a.sample = s.id WHERE a.id IN ($instring) ORDER BY a.chr, a.start, a.stop, sort_order LIMIT 500";
    $qs =  "SELECT a.id AS aid, a.chr AS chr, a.start AS start, a.stop AS stop, s.chip_dnanr AS sample, a.class AS Class FROM `$sourcedb`.`$tablename` a JOIN `$sourcedb`.`sample` s ON a.sample = s.id WHERE a.id IN ($instring) ORDER BY a.chr, a.start, a.stop LIMIT 500";

}
elseif ($tablename == 'parents_regions' || $tablename == 'parents_upd') {
	$qs =  "SELECT a.id AS aid, a.chr AS chr, a.start AS start, a.stop AS stop, s.chip_dnanr AS sample, '-' AS Class FROM `$sourcedb`.`$tablename` a JOIN `$sourcedb`.`sample` s ON a.sid = s.id WHERE a.id IN ($instring) ORDER BY a.chr, a.start, a.stop LIMIT 500";
}
elseif ($tablename == 'cus_aberration') {
	$qs = "SELECT a.id AS aid, a.chr AS chr, a.start AS start, a.stop AS stop, a.sample AS sample, '-' AS Class FROM `$sourcedb`.`$tablename` a WHERE a.id IN ($instring) ORDER BY a.chr, a.start, a.stop LIMIT 500";
}
else {
	echo " table $tablename not supported. Please contact system admin.<br/>";
	exit();
}
$query = mysql_query($qs);
if (mysql_num_rows($query) == 0) {
	echo "Deleting orphan regions (no associated sample) ...<br/>";
	flush();
	//$query = mysql_query("DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tablename' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart AND itemID IN ($instring)");
	#echo "query : DELETE FROM `GenomicBuilds`.`FailedLifts` WHERE tablename = '$tablename' AND frombuild ='$sourcebuild' AND tobuild = '$shortname' AND $uidpart AND itemID IN ($instring)<br/>";
	echo "<meta http-equiv='refresh' content='0;URL=index.php?page=LiftCuration'>\n"; 
	exit();
}

// print entries (for one chromosome, max 500 entries)
 
$cchr = '';
$cstart = 0;
$cstop = 0;
while ($row = mysql_fetch_array($query)) {
	$aid = $row['aid'];
	$chr = $row['chr'];
	$chrtxt = $chromhash[$chr];
	$start = $row['start'];
	$stop = $row['stop'];
	$region = "Chr$chrtxt:".number_format($start,0,'',',') . '-' . number_format($stop, 0,'',',');
	$sample = $row['sample'];
	// end if next chromosome
	if ($chr != $cchr) {
		if ($cchr == '') {
			$cchr = $chr;
			echo " Chromosome $chrtxt</p>";
			echo "<form action='index.php?page=LiftCuration' method=POST name='myForm' id='myForm'>\n";
			echo "<input type=hidden name=tablename value='$tablename'>";
			echo "<input type=hidden name=sourcebuild value='$sourcebuild'>";
			echo "<p><table cellspacing=0>";
			echo "<tr><th $firstcell>Sample</th><th>Original Region</th><th>Class</th><th>Action : <span onClick=ToggleDelete()>(Delete all)</span> </th><th>Tools</th><th>New Region</th></tr>";
		}
		else {
			break;
		}
	}
	if ($start == $cstart && $stop == $cstop) {
		$idem = "<span class=italic style='color:red'> Same as previous</span>";
	}
	else {
		$idem = '';
	}
	$cstart = $start;
	$cstop = $stop;
    $class = $row['Class'];
    if ($class != '') {
        $class = $a_classes[$class];
    }
    else {
        $class = '-';
    }
	// print output
	$radioname = $tablename.'_'.$aid;
	$fieldname = $tablename.'_'.$aid.'_remapped';
	echo "<tr>";
	echo "<td $firstcell>$sample</td>";
	echo "<td NOWRAP onmouseover=\"Tip(ToolTip('$aid','$userid','$tablename','$sourcebuild',event))\" onmouseout=\"UnTip()\">$region$idem</td><td>$class</td>";
	echo "<td NOWRAP><input type=radio name='$radioname' value='D'> Delete / <input type=radio name='$radioname' value='E'>Map Manually</td>";
	echo "<td NOWRAP><a href='http://genome.ucsc.edu/cgi-bin/hgConvert?hgsID=&db=$ucscfrom&position=chr$chrtxt:$start-$stop&hglft_toOrg=Human&hglft_toDb=$ucscto&hglft_doConvert=Submit' target='_blank'>UCSC Convert</a></td>";
	echo "<td><input type=text name='$fieldname' size=30 onKeyUp=\"autoSelect('$radioname','$fieldname')\" onMouseUp=\"autoSelect('$radioname','$fieldname')\"></td>";
	echo "</tr>";
}
echo "</table></p>";
echo "<input type=submit class=button name=CurateSubmit value=Submit>";
echo "</form>";


echo "</div>";

?>
