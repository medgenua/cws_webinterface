<?php

echo "<div class=sectie>";

$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// get source DB
$shortname = $_SESSION['dbname'];
$query = mysql_query("SELECT frombuild as sourcedb FROM `GenomicBuilds`.`FailedLifts` WHERE tobuild = '$shortname' LIMIT 1");
if (mysql_num_rows($query) == 0) {
    echo "<div class=sectie><h3>Curation Finished</h3>\n";
        echo "<p>No more regions are left to curate. <a href='index.php'>Click here to go back to the main page</a>.</p>";
    echo "</div>";
        exit();
}
$row = mysql_fetch_array($query);
$sourcebuild = $row['sourcedb'];   
$sourcedb = "CNVanalysis$sourcebuild";
//UZA people get access to all the data.
// ugroup == 10
$query = mysql_query("SELECT gid FROM `usergroupuser` WHERE uid = '$userid' AND gid = 10");
if (mysql_num_rows($query) > 0) {
    error_log("detected UZA user");
    $uids = array();
    $query = mysql_query("SELECT uid FROM `usergroupuser` WHERE gid = 10");
    while ($row = mysql_fetch_array($query)) {
        array_push($uids,$row['uid']);
    }
    
    $uids = implode(",",$uids);
}
else {
    $uids = $uid;
}
    
////////////////////////////////
// OPTION 1 : By Collection : //
////////////////////////////////

if (isset($_GET['type']) && $_GET['type'] == 'collection') {
    // submitted ? 
    if (isset($_POST['DiscardCollections'])) {
        $toDiscard = $_POST['toDiscard'];
        foreach ($toDiscard as $cname) {
            echo "Discarding $cname<br/>";
            $query = "SELECT f.ID FROM GenomicBuilds.`FailedLifts` f JOIN `$sourcedb`.`aberration` a JOIN `$sourcedb`.`project` p ";
            $query .= " ON a.id = f.itemID AND a.idproj = p.id ";
            $query .= " WHERE f.uid in ($uids) AND f.tablename = 'aberration' AND p.collection = '$cname' LIMIT 5000";
            $q = mysql_query($query);
            while (mysql_num_rows($q) > 0) {
                $aids = array();
                while ($row = mysql_fetch_array($q)) {
                    array_push($aids,$row['ID']);

                }
                $aids = implode(",",$aids);
                $dq = "DELETE FROM `GenomicBuilds`.FailedLifts WHERE ID in ($aids)";
                mysql_query($dq);
                $q = mysql_query($query);
            }
        }


    }



    // list.
    $query = "SELECT COUNT(f.itemID) as nrCNV, p.collection FROM `GenomicBuilds`.FailedLifts f JOIN `$sourcedb`.`aberration` a JOIN `$sourcedb`.`project` p ";
    $query .= " ON a.id = f.itemID AND a.idproj = p.id ";
    $query .= " WHERE f.uid in ($uids) AND f.tablename = 'aberration' GROUP BY p.collection";

    echo "<h3>Discard Lifts By Collection</h3>";
    echo "<p>Select the Collections for which failed lifts can be discarded without review:</p>";
    echo "<form method=POST action='index.php?page=LiftPruning&type=collection'/>";
    echo "<p><table cellspacing=0>";
    echo "<tr><th $firstcell>Discard</th><th>Nr. Failed CNVs</th><th>Collection Name</th>";
    $query = mysql_query($query);
    while ($row = mysql_fetch_array($query)) {
        echo "<tr><td $firstcell><input type=checkbox name='toDiscard[]' value='".$row['collection']."' /></td><td>".$row['nrCNV']."</td><td>".$row['collection']."</td></tr>";
    }
    echo "</table>";
    echo "</p><p><input type=submit value='Discard' name='DiscardCollections'/></p>";
    echo "</form>";



}


/////////////////////////////////////
// OPTION 2 : By Experimental Info //
/////////////////////////////////////
if (isset($_GET['type']) && $_GET['type'] == 'expdet') {
    // this is jquery based to get numbers.
    // get variables
    echo "<h3>Discard by combined criteria</h3>";
    echo "<p>Select a set of criteria for varians that can be pruned automatically. The number of matching variants can be previewed by clicking 'evaluate'.</p>";

    // the chiptypes
    $query = "SELECT DISTINCT(p.chiptypeid) AS chiptype_id, ct.name AS chiptype_name ";
    $query .= " FROM `GenomicBuilds`.FailedLifts f JOIN `$sourcedb`.`aberration` a JOIN `$sourcedb`.`project` p JOIN `$sourcedb`.chiptypes ct ";
    $query .= " ON a.id = f.itemID AND a.idproj = p.id AND ct.ID = p.chiptypeid";
    $query .= " WHERE f.uid in ($uids) AND f.tablename = 'aberration' and f.tobuild = '$shortname'";
    error_log($query);
    $query = mysql_query($query);
    $chiptypes = array();

    echo "<p><span class=nadruk>Chiptype:</span><br/><select style='margin-left:1em' id=chiptype MULTIPLE>";
    while ($row = mysql_fetch_array($query)) {
        $chiptypes[$row['chiptype_id']] = $row['chiptype_name'];
        $cid = $row['chiptype_id'];
        $cname = $row['chiptype_name'];
        echo "<option value='$cid'>$cname</option>";
    }
    echo "</select></p>";
    
    // the Classes
    $query = "SELECT DISTINCT(d.id) AS class_id, d.name AS class_name FROM `GenomicBuilds`.FailedLifts f JOIN `$sourcedb`.`aberration` a JOIN `$sourcedb`.`diagnostic_classes` d ";
    $query .= " ON a.id = f.itemID ";
    $query .= " WHERE f.uid in ($uids) AND f.tablename = 'aberration' AND d.id = a.class ";
    //echo "$query<br/>";
    $query = mysql_query($query);
    $classes = array();
    echo "<p><span class=nadruk>Diagnostic Class:</span><br/><select style='margin-left:1em' id=class MULTIPLE>";
    while ($row = mysql_fetch_array($query)) {
        $classes[$row['class_id']] = $row['class_name'];
        $cid = $row['class_id'];
        $cname = $row['class_name'];
        echo "<option value='$cid'>$cname</option>";

    }
    echo "</select></p>";
    // nrSnps:
    echo "<p><span class=nadruk>Minimal number of probes:</span><br/><input style='margin-left:1em' id=minsnps type=text size=20 value=0></p>";
    echo "<p><span class=nadruk>Maximal number of probes:</span><br/><input style='margin-left:1em' id=maxsnps type=text size=20 value=0></p>";

    // prefix:
    echo "<p><span class=nadruk>Sample name prefix:</span><br/><input style='margin-left:1em' id=prefix type=text size=20 ></p>";

    // preview
    echo "<p><input type=submit value='Evaluate' onClick=\"PreCalculate('$uids','$sourcedb')\"> &nbsp; <span id=EvaluationResult style='border:1px solid red;color:red;display:none'>--</span>";

    echo "<p>Click the button below to delete failed lifts matching your criteria:<br/><input type=submit value='Execute' onClick=\"Prune('$uids','$sourcedb')\"> &nbsp; <span id=PruningResult style='border:1px solid red;color:red;display:none'>--</span>";

}



echo "</div>";



?>

