<?php
ob_start();
$statusfile = "status/status.site";
$fstatus = fopen($statusfile,'r');
$status = fread($fstatus,1);
$blaststatus = "/Blast/files/status.txt";
$acceptedfiles = "/Blast/files/accepted.txt";
$blastout = "/Blast/blast.out";
$killed = "/Blast/files/killed.txt";
$seqname = $_GET["seqname"];
$strict = $_GET["strict"];
$match = $_GET["match"];
$cover = $_GET["cover"];

if ($status == "1") { 
  echo "<meta http-equiv='refresh' content='15;URL=index.php?page=UniSeqResult&seqname=$seqname&strict=$strict&match=$match&cover=$cover'>\n"; 
}
?>
<?php
echo "<div class=\"sectie\">\n";
echo "<h3>UniSeq Results: $seqname</h3>\n";

if ($status == "1" ) {
  echo "<p>UniSeq Analysis Running...</p>\n";
  echo "<p>Page will refresh in 15 seconds</p>\n";
  echo "<p>To end analysis, click <a href=\"endblast.php?seqname=$seqname&amp;strict=$strict&amp;match=$match&amp;cover=$cover\">here</a></p>\n";
  echo "</div><div class=\"sectie\"><h3>Status:</h3>\n";
  echo "<PRE>\n";
  readfile("$blaststatus");
  echo "</PRE>\n</div>";
  echo "<div class=\"sectie\"><h3>Query fragments accepted so far... :</h3>\n";
  if ($strict == 0) {
	echo "<p> &nbsp; => Remark: Only off-target hits with at least $match percent match and $cover basepairs long were taken into account !</p>\n";
  }
  echo "<p>\n";
}
else {
//  system("(killall ps.sh -SIGKILL) >/dev/null");
  echo "<p>UniSeq Analysis Finished</p>\n</div>\n";
  echo "<div class=\"sectie\"><h3>Following fragments of the query were unique: </h3>\n";
  if ($strict == 0) {
	echo "<p> &nbsp; => Remark: Only off-target hits with at least $match percent match and $cover basepairs long were taken into account !</p>\n";
  }
  echo "<p>\n";  
}

$nrfiles = 0;
$accepted = fopen($acceptedfiles, 'r');
while (!feof($accepted)) {
	$line = fgets($accepted);
	$line = rtrim($line);
	$pieces = explode("\t",$line);
	if ($pieces[0] != "") {
		$length = $pieces[1] - $pieces[0] +1;
		echo "<a href=page_getpart.php?start=".$pieces[0]."&amp;end=$pieces[1] onClick=\"return popup(this, '$pieces[0]-$pieces[1]')\">Start: $pieces[0] - End: $pieces[1] : $length"."bp</a><br>\n";
	$nrfiles++;
	}
}
fclose($accepted);
if ($nrfiles == 0) { echo "No unique fragments found\n";}
echo "</div>\n";

echo "<div class=\"sectie\"><h3>Killed Processes</h3>\n";
echo "<p>As a measure of memory overload protection, analysis of fragments that cause Blast to use over 1.5Gb of memory are killed.  <br>Nothing valuable will be lost since only a massive amount of hits can cause this.  Nevertheless, the fragments (if any) are listed below:</p>\n";
echo "<PRE>";
readfile("$killed");
echo "</PRE>\n";
echo "</div>";
echo "<div class=\"sectie\"><h3>Some more information</h3>\n";
echo "<PRE>\n";
readfile("$blastout");
echo "</PRE>\n";
echo "</div>\n";


?>


