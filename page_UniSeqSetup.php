<?php 
$user = $_SESSION['username'];
if ($loggedin != 1) {
	echo "<div class=sectie>\n";
	echo "<h3>UniSeq Finder</h3>\n";
	echo "<h4>Find unique fragments in DNA-sequences</h4>\n";
	include('login.php');
	echo "</div>\n";
}
elseif ($level < 2) {
	include('page_noguest.php');
}
else {
	$statusfile = "status/status.site";
	$fstatus = fopen($statusfile, 'r');
	$status = fread($fstatus,1);
	$detailsfile = "status/details.txt";
	$fdetails = fopen($detailsfile, 'r');
	$line = fgets($fdetails);
	$line = rtrim($line);
	$pieces = explode("\t",$line);
	$operator = $pieces[1];
	$line = fgets($fdetails);
	$line = rtrim($line);
	$pieces = explode("\t",$line);
	$seqname = $pieces[1];
	$line = fgets($fdetails);
	$line = rtrim($line);
	$pieces = explode("\t",$line);
	$date = $pieces[1];

?>


<div id="inhoud-rechtstop">
<div style="float: right">
<?php 
if ($loggedin == 1) {
	echo "<b>$firstname</b> | <a href=\"index.php?page=details\">Profile</a> | <a href=\"logout.php\">Log Out</a>\n";
}
else { 
	echo "<b>Not logged in</b> | <a href=\"index.php?page=login\">Log In</a>\n";
}
echo "</div>";
if ($status == 1) {
	echo "<a href=\"index.php?page=UniSeqSetup\">Start Analysis</a> | <a href=\"index.php?page=UniSeqResult&amp;seqname=$seqname\">Running Analysis</a> ";
}
else {

	echo "<a href=\"index.php?page=UniSeqSetup\">Start Analysis</a> | <a href=\"index.php?page=UniSeqResult&amp;seqname=$seqname\">Last Results</a> ";
}
?> 
</div>
<div class=sectie>
<h3>UniSeq Finder</h3>
<h4>Find unique fragments in DNA-sequences</h4>
<p> This module provides an automated blasting procedure to identify unique fragments in a DNA sequence. The provided query is split up into 1KB fragments (for speed) and compared to the human reference genomic assembly using megablast.  Unique fragments, with a length exceeding the specified treshold, are blasted again using blastn to exlude less exact hits.  The program returns the sequence fragments that are unique (1 chromosome, 1 hit) on both megablast and blastn analysis.</p>

<p>Starting from this sequence, it is suggested to use Primer3 to create primers, and then perform a SNP blast on them (SNPs are not checked here). </p>
</div>
<?php 

ob_start();


if ($status == 1) {
	echo "<div class=sectie>\n";
	echo "<h3>Server Busy</h3>\n";
	echo "<p>Due to the intense nature of this application, only one analysis at a time is allowed on this server. Below are the details on the analysis that's currently running.</p>";
	echo "<table cellspacing=0 id=mytable>\n";
	echo " <tr>\n";
	echo "  <th class=clear scope=row>Operator: </th>\n";
	echo "  <td class=clear>$operator</td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Query name: </th>\n";
	echo "  <td class=clear>$seqname</td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Started at: </th>\n";
	echo "  <td class=clear>$date</td>\n";
	echo " </tr>\n";
	echo "</table>\n";
	echo "<p> If, and only if, you really belief there has been an error somewhere and there is cuurently no analysis running, or it is OK to kill the currently running analysis, you can do so by using the button below. Otherwise, you can use the other button to go to the results page.\n";
	echo "<p><form action=\"endblast.php?seqname=$seqname\" method=GET><input type=hidden name=from value=setup><input type=submit value=\"Kill Running Analysis\" class=button></form>\n";
	echo "&nbsp; &nbsp; <form><input type=button value=\"Go To Results\" ONCLICK=\"window.location.href='index.php?page=UniSeqResult&seqname=$seqname'\" class=button></form></p>\n"; 
	echo "</div>\n";
 
}
else {
	echo "<div class=sectie>\n";
	echo "<h3>Last finished analysis</h3>\n";
	echo "<h4>This will be overwritten !</h4>\n";
	echo "<p>These are the details on the last finished analysis.  The results will be overwritten when a new analysis is started, so please ask the operator if that is all right\n";
	echo "<table cellspacing=0 id=mytable>\n";
	echo " <tr>\n";
	echo "  <th class=clear scope=row>Operator: </th>\n";
	echo "  <td class=clear>$operator</td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Query name: </th>\n";
	echo "  <td class=clear>$seqname</td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Started at: </th>\n";
	echo "  <td class=clear>$date</td>\n";
	echo " </tr>\n";
	echo "</table>\n";
	echo "<form><input type=button value=\"Go To Results\" ONCLICK=\"window.location.href='index.php?page=UniSeqResult&seqname=$seqname'\" class=button></form></p>\n";
	echo "</div>\n";
	

        echo "<div class=sectie>\n";
	echo "<h3>Start new analysis</h3>\n";
	echo "<h4>Previous results will be overwritten !!!</h4>\n";
	echo "<p>Please complete the form below to start the analysis.  There is no size limit on the sequence (afaik), but the pasted sequence MAY NOT contain fasta headers or other non-code lines.  Line breaks and numbers are no problem and N's will be filtered out. </p>\n";
	echo "<form action=blaststep2.php method=post>\n";
	echo "<table cellspacing=0 id=mytable>\n";
	echo " <tr>\n";
	echo "  <th class=clear scope=row>Operator: </th>\n";
	echo "  <td class=clear><input name=operator type=text size=40 value=$username></td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Query name: </th>\n";
	echo "  <td class=clear><input name=seqname type=text size=40 value=\"e.g. fasta header\"></td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Started at: </th>\n";
	echo "  <td class=clear><input name=date type=text size=40 value=\"". date('Y-m-d')."_".date('H\ui\ms\s')."\"></td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Minimal unique stretch length</th>\n";
	echo "  <td class=clear><input name=stretch type=text size=40 value=40></td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>Use Following settings:</th>\n";
	echo "  <td class=clear><input type=radio name=\"strict\" value=\"0\" checked>Yes<input type=radio name=\"strict\" value=\"1\">No</td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>&nbsp; &nbsp; Minimal identity (percent) for a valid hit</th>\n";
	echo "  <td class=clear><input name=match size=40 type=text value=100></td>\n";
	echo " </tr>\n <tr>\n";
	echo "  <th class=clear scope=row>&nbsp; &nbsp; Minimal length (bp) for a valid hit</th>\n";
	echo "  <td class=clear><input name=cover size=40 type=text value=20></td>\n"; 
	echo " </tr>\n";
	echo "</table>\n";
	echo "<p>Past your sequence here (no none-code lines!)</p>\n";
	echo "<p><textarea name=sequence rows=30 cols=100></textarea></p>\n";
	echo "<p><input type=submit value=Submit class=button><p>\n";
	echo "</div>\n";

}
}
?>
