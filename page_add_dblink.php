<?php
if ($loggedin != 1) {
	include('login.php');
	exit();
}

$ucscdb = str_replace('-','',$_SESSION['dbname']);
// get type of new entry
$addtype = $_GET['a'];
// if new entry is submitted
if (isset($_POST['addresource'])) {
	// process new entry
	$name = $_POST['resource'];
	$descr = $_POST['descr'];
	$link = $_POST['link'];
	$addtype = $_POST['addtype'];
	$ok = 1;
	$string = '';
	if ($addtype == 's' && !(preg_match('/%s/',$link)||preg_match('/%o/',$link)||preg_match('/%g/',$link))) {
		$string .=  "<p><span class=nadruk>Problem:</span> No '%s' was found in the URL as placeholder for the gene symbol to use in in the actual queries. Go back and correct this.</p>\n";
		$ok = 0;
	}
	if ($addtype == 'r') {
		if (!preg_match('/%r/',$link) && !(preg_match('/%c/',$link) && preg_match('/%s/',$link) && preg_match('/%e/',$link))) {
			$string .=  "<p><span class=nadruk>Problem:</span>Necessary placeholders for the region to use in in the actual queries were not found. Go back and correct this.</p>\n";
			$ok = 0;
		}
	}
	if ($descr == '') {
		$string .=  "<p><span class=nadruk>Problem:</span> Please go back and provide a description of this resource.</p>\n";
		$ok = 0;
	}
	if ($name == '') {
		$string .=  "<p><span class=nadruk>Problem:</span> Please go back and provide a Name for this resource.</p>\n";
		$ok = 0;
	}
	if ($ok == 0) {
		echo "<div class=sectie>\n";
		echo "<h3>Insufficient information provided</h3>\n";
		echo "$string";
	}
	else {
		$types['s'] = 1;
		$types['r'] = 2;
		$link = addslashes($link);
		$descr = addslashes($descr);
		$numtype = $types[$addtype];
		$name = addslashes($name);
		$query = mysql_query("INSERT INTO db_links (Resource, link, Description, type) VALUES ('$name', '$link', '$descr', '$numtype')");
		echo "<meta http-equiv='refresh' content='0;URL=index.php?page=settings&amp;type=plots&amp;st=publ&amp;v=p'>\n";
	}
	#echo "<meta http-equiv='refresh' content='0;URL=index.php?page=settings&amp;type=plots&amp;st=publ&amp;v=p'>\n";
		

}
else {
	// print the form to insert new site
	echo "<form action='index.php?page=add_dblink' method=POST>\n";
	echo "<input type=hidden name=addtype value='$addtype'>\n";
	// add a symbol based query
	if ($addtype == 's') {
		echo "<div class=sectie>\n";
		echo "<h3>Add Symbol Based Query</h3>\n";
		echo "<p>Enter the information needed for the query to be performed. The resource should be accessible by a standard URL, where the keyword is replaced by '%s' (Refseq Symbol), '%o' (omim ID), or '%g' (NCBI geneID, uid). UCSC Build (eg $ucscdb) should be replaced by '%u'. (LOWERCASE, without the quotes).</p>\n";
		echo "<p> The link to query google scholar for example is : <pre>http://scholar.google.com/scholar?hl=en&amp;lr=&amp;btnG=Search&amp;q=%s</pre></p>\n"; 
		echo "<table>\n";
		echo " <tr>\n";
		echo "  <th class=clear title='Add a clear and unambiguous name to be used as link'>Resource Name</th>\n";
		echo "  <td class=clear><input type=text name=resource size=40></td>\n";
		echo " </tr>\n";
		echo " <tr>\n";
		echo "  <th class=clear title='The search URL, replace the symbol by %s'>Query URL</th>\n";
		echo "  <td class=clear><input type=text name=link size=40></td>\n";
		echo " </tr>\n";
		echo " <tr>\n";
		echo "  <th class=clear title='Give a description of what this database provides'>Description</th>\n";
		echo "  <td class=clear><input type=text name=descr size=40 maxlength=250></td>\n";
		echo " </tr>\n";
		echo "</table></p>\n";
		echo "<p><input type=submit class=button name=addresource value=Submit></p>\n";
		echo "</form>\n";
		echo "</div>\n";
	}
	elseif($addtype == 'r') {
		// add region based query
		echo "<div class=sectie>\n";
		echo "<h3>Add Region Based Query</h3>\n";
		echo "<p>Enter the information needed for the query to be performed. The resource should be accessible by a standard URL, where the region is replaced by the following characters: </p>\n";
		echo "<pre>\n";
		echo " - Chromosome: %c\n";
		echo " - Start Position: %s\n";
		echo " - End Position : %e\n";
		echo "     OR\n";
		echo " - Entire Region : %r\n";
		echo " - UCSC Build (eg $ucscdb) : '%u'.";
		echo "</pre>\n";
		echo "<p> The link to query UCSC is for example : <pre>http://genome.ucsc.edu/cgi-bin/hgTracks?clade=mammal&org=Human&db=%u&position=chr%c:%s-%e\n OR\nhttp://genome.ucsc.edu/cgi-bin/hgTracks?clade=mammal&org=Human&db=%u&position=%r</pre></p>\n"; 
		echo "<table>\n";
		echo " <tr>\n";
		echo "  <th class=clear title='Add a clear and unambiguous name to be used as link'>Resource Name</th>\n";
		echo "  <td class=clear><input type=text name=resource size=40></td>\n";
		echo " </tr>\n";
		echo " <tr>\n";
		echo "  <th class=clear title='The search URL, replace the symbol by %s'>Query URL</th>\n";
		echo "  <td class=clear><input type=text name=link size=40></td>\n";
		echo " </tr>\n";
		echo " <tr>\n";
		echo "  <th class=clear title='Give a description of what this database provides'>Description</th>\n";
		echo "  <td class=clear><input type=text name=descr size=40 maxlength=250></td>\n";
		echo " </tr>\n";
		echo "</table></p>\n";
		echo "<p><input type=submit class=button name=addresource value=Submit></p>\n";
		echo "</form>\n";
		echo "</div>\n";


	}
}


?>
