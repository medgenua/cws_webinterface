<?php 
echo "<div class=sectie>";
echo "<h3>Adding Default Projects</h3>";
if ($loggedin != 1) {
	include('login.php');
	echo "</div>\n";
	exit;
}

include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$query = mysql_query("SELECT id, naam, chiptype FROM project WHERE isdefault = 1");

if (mysql_num_rows($query) == 0) {
	echo "<p>No default projects were specified. You will now be redirected to the projects page.</p></div>";
	echo "<meta http-equiv='refresh' content='3;URL=index.php?page=projects'>\n"; 
	exit;
}

if (!isset($_POST['adddefproj'])) {
	echo "Invalid redirection. You will be redirected</div>";
	echo "<meta http-equiv='refresh' content='3;URL=index.php?page=projects'>\n";
	exit;
}

//check projects and for $userid
echo "<p>The following projects will be added to your account (if you do not already have access).</p>";
echo "<p><div class=nadruk>Default Projects:</div><p>";
while ($row = mysql_fetch_array($query)) {
	$pid = $row['id'];
	$name = $row['naam'];
	$chip = $row['chiptype'];
	$subq = mysql_query("SELECT editcnv, editclinic, editsample FROM projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
	if (mysql_num_rows($subq) == 0) {
		# new access : readonly by default
		mysql_query("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic, editsample) VALUES ('$pid', '$userid', '0','0','0')");
		echo "<span class=bold style='padding-left:15px'>$name</span>: NEW: Read-Only <br/>";
	}
	else {
		# access exists, don't update
		$srow = mysql_fetch_array($subq);
		$editcnv = $srow['editcnv'];
		$editclinic = $srow['editclinic'];
		$editsample = $srow['editsample'];	
		if ($editcnv == 1 && $editclinic == 1) {
			$perm = 'Full';
		}
		elseif ($editcnv == 1) {
			$perm = 'CNV';
		}
		elseif ($editclinic == 1) {
			$perm = 'Clinic';
		}
		else {
			$perm = 'Read-only';
		}
		if ($editsample == 1 ) {
			$perm .= " / Project Control";
		}
		echo "<span class=bold style='padding-left:15px'>$name</span>: $perm<br/>";
		
	}
}
echo "</p><p><a href='index.php?page=projects'>Go Back to Projects</a></p></div>";

?>
