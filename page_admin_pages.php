<?php 
if ($level < 3) {
	echo "<div class=sectie><h3>Access Denied</h3><p>Only System administrator has access to these pages</p></div>";
	exit();
}

$type = $_GET['type'];
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

if ($type == 'users' ) {
	// userlevels
	$levels[] = "<option value=0 selected>Disabled</option>\n<option value=1>Guest access</option>\n<option value=2>Full access</option>\n<option value=3>Administrator</option>\n";
	$levels[] = "<option value=0 >Disabled</option>\n<option value=1 selected>Guest access</option>\n<option value=2>Full access</option>\n<option value=3>Administrator</option>\n";
	$levels[] = "<option value=0 >Disabled</option>\n<option value=1 >Guest access</option>\n<option value=2 selected>Full access</option>\n<option value=3>Administrator</option>\n";
	$levels[] = "<option value=0 >Disabled</option>\n<option value=1 >Guest access</option>\n<option value=2 >Full access</option>\n<option value=3 selected>Administrator</option>\n";

	echo "<div class=sectie>\n";
	echo "<h3>User Management</h3>\n";
	echo "<p>Overview of users, and option to set user levels. These levels define what can be used on the site.</p>\n";
	echo "<form action='set_userlevel.php' method=POST>\n";
	echo "<input type=hidden name=userid value=$userid>";
	echo "<p>\n<table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th $firstcell class=topcellalt>Name</td>\n";
	echo "  <th class=topcellalt>username</td>\n";
	echo "  <th class=topcellalt>Affiliation</td>\n";
	echo "  <th class=topcellalt>E-mail</td>\n";
	echo "  <th class=topcellalt>level</td>\n";
	echo " <tr>\n";
	$switch = 0;
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name, u.Affiliation, u.email, u.username, u.level, u.motivation FROM users u JOIN affiliation a ON u.Affiliation = a.id");
	while ($row = mysql_fetch_array($query)) {
		$uid = $row['id'];
		$uLastName = $row['LastName'];
		$uFirstName = $row['FirstName'];
		$uAffiliation = $row['Affiliation'];
		$uAffiName = $row['name'];
		$uemail = $row['email'];
		$uname = $row['username'];
		$ulevel = $row['level'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell><input type=hidden name='uids[]' value='$uid'>$uFirstName $uLastName</td>\n";
		echo "  <td $tdtype[$switch] >$uname</td>\n";
		echo "  <td $tdtype[$switch] >$uAffiName</td>\n";
		echo "  <td $tdtype[$switch] >$uemail</td>\n";
		echo "  <td $tdtype[$switch] ><select name=perm_$uid>$levels[$ulevel]</select></td>\n";
		echo " </tr>\n";
	}
	echo "</table>\n";
	echo "</p>";
	echo "<input type=submit class=button value=Update></form>\n";
	echo "</div>\n";
	
}
elseif ($type == 'impersonate') {
	echo "<div class=sectie>";
	echo "<h3>Impersonate User</h3>";
	echo "<p>Impersonating a different user will allow administrators to access all data of that user, and to perform tasks in their name. Select a user from the list below and click 'Impersonate'. You will be redirected to their start page. To access your own results again, log out and log in under your own account. </p>";
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE u.id != '$userid' ORDER BY a.name ASC, u.LastName ASC");
	$rows = mysql_num_rows($query);
	$curraffi = '';
	echo "<p><form action='Impersonate_user.php' method='POST'>";
	echo "<select name='ImpersonateID'>";
	while ($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['LastName'] . ' ' . $row['FirstName'];
		$affi = $row['name'];
		if ($affi != $curraffi) {
			## print affiliation group
			if ($curraffi != '') {
				echo "</optgroup>";
			}
			echo "<optgroup label='$affi'>";
			$curraffi = $affi;
		}
		echo "<option value='$id'>$name</option>";
	}
	if ($curraffi != '') {
		echo "</optgroup>";
	}
	echo "</select>";
	echo "<input type='submit' class=button name='Impersonate' value='Impersonate'></form> </p>";
	echo "</div>";
}

elseif ($type == 'update') {
	include('page_update.php');
}
elseif($type=='lift') {
	$step = $_GET['step'];
	if (!isset($_GET['step'])) { 
		include('LiftOver/inc_Intro.inc');	
	}
 	else {
		mysql_select_db("GenomicBuilds");
		$query = mysql_query("SELECT file FROM LiftOverSteps WHERE idx = '$step'");
		$row = mysql_fetch_array($query);
		include($row['file']);
	}
}
elseif($type=='SiteStatus') {
	if (isset($_POST['changestatus'])) {
		$newstatus = $_POST['SS'];
		$SiteStatus = $newstatus;
		$query = mysql_query("UPDATE `GenomicBuilds`.`SiteStatus` SET status = '$newstatus' WHERE 1=1");
	}
	echo "<div class=sectie>\n";
	echo "<h3>Platform Status : $SiteStatus</h3>\n";
	echo "<p>Access to the platform can be blocked for all non-admin users, so that upgrades can be performed safely. To do so, set the status to 'Under Construction'. When you are planning a LiftOver operation to a new genomic build, use 'LiftOver'. This will block all future analyses, while allowing regular browsing (i.e. The platform becomes read-only). </p>\n";
	echo "<form action='index.php?page=admin_pages&amp;type=SiteStatus' method=POST>\n";
	echo "Set platform status: <select name='SS'>";
	if ($SiteStatus =='Operative') {
		echo "<option value=Operative selected>Fully Operational</option><option value=Construction>Under Construction</option><option value=LiftOver>Performing LiftOver</option></select>";
	}
	elseif ($SiteStatus =='Construction') {
		echo "<option value=Operative >Fully Operational</option><option value=Construction selected>Under Construction</option><option value=LiftOver>Performing LiftOver</option></select>";
	}
	elseif ($SiteStatus =='LiftOver') {
		echo "<option value=Operative >Fully Operational</option><option value=Construction>Under Construction</option><option value=LiftOver selected>Performing LiftOver</option></select>";
	}
	else {
		echo "<option value=Operative >Fully Operational</option><option value=Construction>Under Construction</option><option value=LiftOver >Performing LiftOver</option></select>";
	}
	echo "<input type=submit class=button value='Set Mode' name=changestatus></p>";
	echo "</form>";
	echo "</div>\n";


}
elseif ($type == 'NewFields') {
	include('inc_newfields.inc');
}
else {
	echo "<div class=sectie>\n";
	echo "<h3>Administration Pages</h3>\n";
	echo "<p>These pages are only accessible for the platform administrators. They are assigned by a userlevel of 3 (see 'users'). The different tasks that can be performed from here are accessible from the top menu.</p><p> One advice: <span class=bold> Be Careful !</span></p>\n";
	echo "</div>\n";
}
