<?php

if ($loggedin != 1) {
	include('login.php');
}
else {
	/////////////////////////
	// CONNECT TO DATABASE //
	/////////////////////////
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");

	$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';

	$defineprojects = $_POST['setsamples'] || 0;

	///////////////////////////////////////////
	// SELECT WHICH PROJECTS TO EXTRACT FROM //
	///////////////////////////////////////////
	if (isset($_POST['setsamples'])) {
		unlink("$sitedir/Reports/Table_$userid.txt");
		$fh = fopen("$sitedir/Reports/Table_$userid.txt", 'a');
		$header = "SampleID\tChr\tStart\tEnd\tValue";
		
		// Start output
		echo "<div class=sectie>\n";
		echo "<h3>Creating Overview file </h3>\n";
		echo "<h4>status...</h4>\n";
		echo "<ul id=ul-disc>\n";
		echo "<li> Fetching results\n";
		ob_flush();
		flush();
		$header .= "\n";
		fwrite($fh,$header);

		## Reformat projects to use
		$instring = '';
		if (isset($_POST['usesamples'])) {
			foreach($_POST['usesamples'] as $key => $sid) {
				$instring .= "$sid,";
			}
		}

		if ($instring != '') {
			$instring = " AND s.id IN (".substr($instring,0,-1).") ";
		}

		// compose query, get CNVs
		$querystring = "SELECT s.chip_dnanr, s.gender, a.chr, a.start, a.stop, a.cn FROM aberration a LEFT JOIN sample s ON s.id = a.sample LEFT JOIN projsamp ps ON ps.idsamp = s.id LEFT JOIN projectpermission pp ON pp.projectid = ps.idproj WHERE pp.userid = '$userid' $instring ORDER BY s.chip_dnanr ASC, a.chr ASC, a.start ASC";
		//echo "<p>$querystring</p>";
		$query = mysql_query($querystring);

		echo "</ul>";
		echo "<li>Printing results files</li>";

		while ($row = mysql_fetch_array($query)) {
			$sid = $row['id'];
			$sample = $row['chip_dnanr'];
			$gender = $row['gender'];
			$cn = $row['cn'];
			$chr = $row['chr'];
			if ($chr == '23') {
				$chr = 'X';
			}
			if ($chr == '24') {
				$chr = 'Y';
			}
			$start = $row['start'];
			$stop = $row['stop'];

			$line = "$sample\t$chr\t$start\t$stop\t$cn\n";
			fwrite($fh, $line);
		}

		echo "<li>Done, providing file</li>";
		echo "</ul>";
		echo "</p><p> <a href=\"index.php?page=clinstats\">Go Back</a>\n";
		fclose($fh);
		ob_flush();
		flush();
		echo "<iframe height='0' width='0' src='download.php?path=Reports&file=Table_$userid.txt'></iframe>\n";
	}

	else {
		//$query = "SELECT s.id, s.chip_dnanr, p.naam, p.chiptype, p.created, p.collection FROM sample s LEFT JOIN projsamp ps ON ps.idsamp = s.id LEFT JOIN project p ON p.id = ps.idproj LEFT JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '6' AND s.chip_dnanr LIKE 'PRE%' ORDER BY s.id DESC";
		$query = "SELECT s.id, s.chip_dnanr, p.naam, p.chiptype, p.created, p.collection FROM sample s LEFT JOIN projsamp ps ON ps.idsamp = s.id LEFT JOIN project p ON p.id = ps.idproj LEFT JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '6' AND s.chip_dnanr LIKE 'PRE%' AND s.id > 17200 ORDER BY s.id DESC";
		//echo "<p>$query</p>";
		$query = mysql_query($query);
		if (mysql_num_rows($query) == 0) {
			echo "<div class=sectie>";
			echo "<h3>Problem Detected</h3>";
			echo "<p>You don't have access to any project to export data from. </p>\n";
			echo "</div>";
		}

		else {
			echo "<div class=sectie>";
			echo "<h3>Extract CNV Data</h3>";
			echo "<h4>Select Samples</h4>";
			echo "<p>Select the samples you wish to extract the CNV data from below.</p>";
			echo "<p><form action='index.php?page=belcocyt' method=POST>";
			// hidden fields for filter settings
			echo "<input type=hidden name='setsamples' value=1>";

			// table headers
			echo "<table cellspacing=0>\n";
			echo "<tr><th colspan=6 $firstcell>Samples to Include</th></tr>\n";
			echo "<tr>\n";
			echo " <th class=topcell $firstcell>USE</th>\n";
			echo " <th class=topcell>Sample ID</th>\n";
			echo " <th class=topcell>Project</th>\n";
			echo " <th class=topcell>Chiptype</th>\n";
			echo " <th class=topcell>Created</th>\n";
			echo " <th class=topcell>Collection</th>\n";

			echo "</tr>\n";
 			// entries
			while ($row = mysql_fetch_array($query)) {
				$id = $row['id'];
				$sample = $row['chip_dnanr'];
				$name = $row['naam'];
				$col = $row['collection'];
				$chip = $row['chiptype'];
				$created = $row['created'];
				echo "<tr><td $firstcell><input type=checkbox name=usesamples[] value=$id></td>";
				echo "<td>$sample</td><td>$name</td><td>$chip</td><td>$created</td><td>$col</td></tr>";
			}
			echo "</table>";
			echo "</p><p><input type=submit class=button value=Proceed></p>";
			echo "</form>";
		}
	}
	echo "</div>";
}

?>
