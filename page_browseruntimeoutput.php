<?php

## posted pid value
$pid = $_GET['p'];

## get permissions ##
$query = mysql_query("SELECT userid FROM projectpermission WHERE userid = '$userid' AND projectid = '$pid'");
if (mysql_num_rows($query) == 0) {
	if ($level < 3) {
		echo "<div class=sectie>";
		echo "<h3>Permission Denied</h3>";
		echo "<p>You don't have permission to view details for this project.</p>";
		echo "</div>";
		exit();
	}
	else {
		echo "<div class=sectie ><h3 style='color:red'>Browsing Without proper permissions.</h3><p class=bold>ASK FOR PROPER PRIVILEGES !!!</p></div>";
	}

}

## set user friendly file names
$alias['BafSegmentation'] = 'Mosaicism Analysis';
$alias['CreatePDF'] = 'Whole Genome Plot Creation';
$alias['FormatResults'] = 'Majority Vote Filtering';
$alias['GWAnalysis'] = 'Genomic Wave Analysis';
$alias['Segment.PennCNVas'] = 'PennCNV Analysis (1-22)';
$alias['Segment.PennCNVx'] = 'PennCNV Analysis (X)';
$alias['Segment.QuantiSNPv2'] = 'QuantiSNPv2 Analysis';
$alias['Segment.VanillaICE'] = 'VanillaICE Analysis';
$alias['PdfsMain'] = 'Whole Genome Plot Creation';
$alias['CreateInputFiles'] = 'Create Input Files';
$alias['FamilyAnalysis'] = 'Inheritance Analysis';
$alias['CheckUPD'] = 'Uniparent Disomy Analysis';
$alias['Segment.PlinkHomoz'] = 'Homozygosity Detection';
$alias['triPOD'] = 'TriPOD Analysis';
## get samples ##
$samples = array();
$query = mysql_query("SELECT s.chip_dnanr,s.id FROM sample s JOIN projsamp ps ON ps.idsamp = s.id WHERE ps.idproj = '$pid'");
while ($row = mysql_fetch_array($query)) {
	$samples[$row['id']] = $row['chip_dnanr'];
	$revsamples[$row['chip_dnanr']] = $row['id'];
	$toprint = array();
}

## directory ##
$dir = $config['CJO'] . '/' . $scriptuser . '/'. $pid . '/';
$It =  opendir($dir);
$gFiles = array();
$Files = array();
$toprint = array();
if (! $It) {
	die('Cannot list files for ' . $dir);
}

while ($Filename = readdir($It)) {
	if ($Filename == '.' || $Filename == '..') {
   		continue;
	}
	## strip pid
	$Filename = substr_replace($Filename,'',0,(strpos($Filename,'.')+1));
	## general files
	if (preg_match('/\.e\.txt\.\d+/',$Filename) ) {
		continue;
	}
	if (substr($Filename,0,6) == 'Create') {
		$display = preg_replace('/\.o\.txt\.\d.*/','',$Filename);
		$gFiles[$Filename] = $display;
		continue;
	}

	$sample = substr($Filename,0,strpos($Filename,'.'));
	$display = substr_replace($Filename,'',0,(strpos($Filename,'.')+1));
	$display = preg_replace('/\.o\.txt\.\d.*/','',$display);
	#$samples[$sample] = 1;
	$toprint[$samples[$sample]][$Filename] = $display;
}

## set permissions (in case project is not finished yet...)
$command = "COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $dir && chmod a+rx *\"";
$dump = system($command);


// get project name
$query = mysql_query("SELECT naam FROM project WHERE ID = '$pid'");
$row = mysql_fetch_array($query);
$ptitle = $row['naam'];

## OUTPUT
echo "<div class=sectie>";
echo "<h3 style='margin-bottom:25px;'>Runtime Output Viewer: $ptitle</h3>";
echo "<div style='float:left;width:19%' >";
ksort($samples);
## general
echo "<span class=nadruk>General:</span>";
echo "<div style='border-left:1px solid #CCC;padding-left:5px;margin-left:10px;'>";
foreach($gFiles as $f=>$n) {
	echo "<a href='index.php?page=browseruntimeoutput&p=$pid&f=$f&i=$n'>$n</a><br/>";
}
echo "</div></p>";
## per sample
foreach($toprint as $sample => $files) {
	echo "<span class=nadruk>$sample:</span>";
	echo "<div style='border-left:1px solid #CCC;padding-left:5px;margin-left:10px;'>";
	$sid = $revsamples[$sample];
	ksort($items);
	foreach ($files as $f => $d) {
		echo "<a href='index.php?page=browseruntimeoutput&p=$pid&f=$f&i=$d'>".$alias[$d]."</a><br/>";
	}
	echo "</div></p>";
}
echo "</div>";

## right panel
echo "<div style='float:right;width:75%;border-left:1px solid #CCC;padding-left:15px;'>";
if (!isset($_GET['f'])) {
	echo "<h3 style='margin-top:0px'>Runtime Output : No Item Selected</h3>";
	echo "<p>Select an item from the list on the left to display.</p>";
}
else {
	$ofile = "$dir/$pid.".$_GET['f'];
	$efile = str_replace(".o.txt.",".e.txt.",$ofile);
	#if (substr($item,0, == 'CreateInputFiles') {
	#	$ofile = "$dir$pid.CreateInputFiles.o.txt";
	#	$efile = "$dir$pid.CreateInputFiles.e.txt";
	#	$subitem = $item;
		
	#}
	#elseif ($item == 'PdfsMain') {
	#	$ofile = "$dir$pid.CreatePfsMain.out.txt";
	#	$efile = "$dir$pid.PdfsMain.e.txt";
	#	$subitem = $item;
	#}
	#else {
	#	$sid = substr($item,0,strpos($item,'.'));
	#	$sample = $samples[$sid];
	#	$subitem = substr($item,(strpos($item,'.')+1)) ;
	#	$ofile = "$dir$pid.$sid.$subitem.o.txt";
	#	$efile = "$dir$pid.$sid.$subitem.e.txt";
	#}
	$subitem = $_GET['i'];
	echo "<h3>".$alias[$subitem]." : STDOUT</h3>";
	if (!file_exists($ofile)) {
		echo "<p>No Contents in STDOUT file</p>";
	}
	else {
		$file = file($ofile);
		if (count($file) == 0) {
			echo "<p>No Contents in STDOUT file</p>";
		}
		else {
			echo "<pre >";
			foreach ($file as $linenr => $line) {
				echo $line;
			}	
			echo "</pre>";
		}
	}
	echo "<h3>".$alias[$subitem]." : STDERR</h3>";
	if (!file_exists($efile)) {
		echo "<p>No Contents in STDERR file</p>";
	}
	else {
		$file = file($efile);
		if (count($file) == 0) {
			echo "<p>No Contents in STDERR file</p>";
		}
		else {
			echo "<pre>";
			foreach ($file as $linenr => $line) {
				echo $line;
			}	
			echo "</pre>";
		}
	}	
}
echo "</div>";
echo "<div style='clear: both;'>";
echo "</div>";
?>
