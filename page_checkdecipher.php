<?php
//syntax
?>
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>
<script type='text/javascript' src='javascripts/ajax_tooltips.js'></script>
<script type='text/javascript' src='javascripts/setclass.js'></script>

<?php
if ($loggedin != 1) {
	include('login.php');
	exit();
}
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_end_flush();
$inh = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

######################
## POSTED VARIABLES ##
######################
$aid = $_GET['aid'];


// tmp part
if ($aid == '') {
	$aid = $_POST['aid'];
}
echo "<div class=sectie><h3>Double Check Class 4 against DECIPHER Syndromes</h3>";

$query = mysql_query("select a.id, a.chr, a.start, a.stop, a.size, s.chip_dnanr, a.cn,a.inheritance,a.class from aberration a JOIN sample s ON a.sample = s.id WHERE NOT chip_dnanr LIKE 'NA%' AND ( class = 4 ) ORDER BY chr ASC, start ASC");
$next =	 0;
// PROCESS
echo "<p> Table with aberrations classified as  4, overlapping with Decipher syndromes (syndrome list might be out of date!)</p>";
echo "<p>";
echo "<table cellspacing=0>";
echo "<tr><th $firstcell>Sample</th><th>Sample Region</th><th>CN type</th><th>Inh.</th><th>Syndrome</th><th>Syndrome CN</th><th>Syndrome Region</th></tr>";
while ($row = mysql_fetch_array($query)) {
	$aid = $row['id'];
	$start = $row['start'];
	$stop = $row['stop'];
	$chr = $chromhash[$row['chr']];
	$class = $row['class'];
	$inhe = $inh[$row['inheritance']];
	$sample = $row['chip_dnanr'];
	$cn = $row['cn'];	
	#mysql_select_db("decipher");
	$synquery = mysql_query("SELECT Start, End, Variance, Name,ID FROM `decipher`.`syndromes` WHERE Chr = '$chr' AND ((Start BETWEEN $start AND $stop) OR (End BETWEEN $start AND $stop) OR ($start BETWEEN Start AND End))  ORDER BY Start");
	if (mysql_num_rows($synquery) == 0) {
		#mysql_select_db($db);
		continue;
	}
	$syndromes = '';
	$nrs = 0;
	while ($srow = mysql_fetch_array($synquery)) {
		$nrs++;
		if ($nrs > 1) {
			$syndromes .= "<tr>";
		}
		$did = $srow['ID'];
		$syndromes .= "<td onmouseover=\"Tip(DecipherTT('$did&amp;t=s',event))\" onmouseout=\"UnTip()\">".$srow['Name']."</td>";
		$syndromes .= "<td>".$srow['Variance']."</td>";
		$syndromes .= "<td NOWRAP>Chr$chr:".number_format($srow['Start'],0,'',',')."-".number_format($srow['End'],0,'',',')."<br/>(".number_format(($srow['End']-$srow['Start']),0,'',',')."bp)</td></tr>";
	}
	#mysql_select_db($db);

	echo "<tr>";
	echo "<td rowspan=$nrs $firstcell>$sample</td>";
	echo "<td rowspan=$nrs NOWRAP>Chr$chr:".number_format($start,0,'',',')."-".number_format($stop,0,'',',')."<br/>(".number_format(($stop-$start),0,'',',')."bp)</td>";
	echo "<td rowspan=$nrs onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\">$cn</td>";
	echo "<td rowspan=$nrs>$inhe</td>";
	echo "$syndromes";
	ob_flush();
	flush();
}
echo "</table>";
?>
<!-- context menu loading -->
<script type="text/javascript" src="javascripts/details_context.js"></script>

