
<div class=sectie>
<h3>Supported Chip Types</h3>
<p>The analysis proposed on this website is rather generic.  However, each Illumina chiptype has some specific probe names and non-polymorphic probes, and those information is needed for each type. When converting full call reports as described in the documentation, you will need probe position files and lists of these non polymorphic probes. Both are included in the files you can download below.</p>

<?php
echo "<p>The currently supported chiptypes are: ";
echo "<ul id=ul-simple>";
$query = "SELECT name FROM chiptypes";
$result = mysql_query($query);
$dbname = $_SESSION['dbname'];
while($row = mysql_fetch_array($result)) {
	$name = $row[0];
	$filename = "files/$name$dbname.zip";
	if (file_exists($filename)) {
		echo "<li>- $name : <a href='$filename'>Position Files</a></li>\n";
	}
	else {
		echo "<li>- $name : Position Files not available</li>";
	}
}

echo "</ul></p>";

echo "</div>\n";
if ($loggedin == 1) {
	if ($level > 1) {
?>
<div class=sectie>
<h3>New chiptype</h3>
<p>What you need to include or update a chiptype is a tab seperated textfile with the name, chromosome and position of each probe. This can easily be exported from BeadStudio. Secondly, you need a list of all zero'ed probes.  Zero'ed probes are probes for which there is no genotype calling (i.e. there are no clusters defined). This is important for good VanillaICE results. To create such a file, hide all columns but the probe names. Next, create a filter for "GenTrain Score = 0". What rests are the names of the zero'ed probes. <p>

<p>Activating new chiptypes from the webinterface is disabled to allow centralised platform updates and synchronisation.</p>
<?php
}
}
?>
