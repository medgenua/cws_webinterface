starting...<br>
<?php

if ($loggedin == 1) {

	for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}
	$chromhash["X"] = "23";
	$chromhash["Y"] = "24";
	$chromhash["XY"] = "23";
	$chromhash["MT"] = "25";	
 	$db = "CNVanalysis" . $_SESSION["dbname"];
 	mysql_select_db("$db");
	$ucscdb = str_replace('-','',$_SESSION['dbname']);
	$chipname = $_POST['name'];
	$query = mysql_query("Select ID from chiptypes where name = \"$chipname\"");
 	$nrrows = mysql_num_rows($query);
	$ok = 1;
	if ($nrrows != 0 ) {
		echo "This chiptype already exists !\n";
		$ok = 0;
	}
	if ($ok == 1) {
		if(move_uploaded_file($_FILES['position']['tmp_name'], "/tmp/chippos.txt") ) {
			echo "Position table successfully uploaded<br>\n";
		}
		else {
			echo "position file upload error\n";
			$ok = 0;
		}
	}
	
	if ($ok == 1) {
		$fhpos = fopen("/tmp/chippos.txt","r");
		$line = fgets($fhpos);
		$line = rtrim($line);
		$header = explode("\t",$line);
		if (in_array("Name",$header) && in_array("Chr",$header) && in_array("Position",$header) ) {
   			$namepos = array_search("Name", $header);
   			$chrpos = array_search("Chr",$header);
   			$pospos = array_search("Position",$header);
			echo "All needed position data found <br>\n";
		}
		else {
			fclose($fhpos);
			echo "position table contains the wrong columns\n";
			$ok = 0;
		}
	}
		
	if ($ok == 1) {
		if(move_uploaded_file($_FILES['zeroed']['tmp_name'], "/tmp/zeroed.txt") ) {
			echo "Zeroed probes table successfully uploaded<br>\n";
		}
		else {
			echo "zeroed file upload error\n";
			$ok = 0;
		}
	}

	if ($ok == 1) {
		$fhzero = fopen("/tmp/zeroed.txt","r");
		$line = fgets($fhzero);
		$line = rtrim($line);
		$header = explode("\t",$line);
		if (in_array("Name",$header) ) {
   			$nameposzero = array_search("Name", $header);
			echo "All needed naming information found<br>\n";
		}
		else {
			fclose($fhzero);
			echo "Zeroed probes table contains the wrong columns\n";
			$ok = 0;
		}
	}
	
	if ($ok == 1) {
		
		mysql_query("INSERT INTO chiptypes (name) VALUES ('$chipname')");
		
		$query = mysql_query("Select ID from chiptypes where name = \"$chipname\"");
		$row = mysql_fetch_array($query, MYSQL_ASSOC);
		$chiptypeid = $row['ID'];
		
		while(!feof($fhpos)) {
    			$line = fgets($fhpos);
			//echo "$line<br>";
    			$line = rtrim($line);
    			$pieces = explode("\t",$line);
			if ($pieces[0] != "") {
				$chr = $pieces[$chrpos];
				$nrChr = $chromhash[$chr];
				mysql_query("INSERT INTO probelocations (name, chromosome, position, chiptype) VALUES ('$pieces[$namepos]', '$nrChr','$pieces[$pospos]','$chiptypeid')");
			}
		}
		fclose($fhpos);
		//mysql_query("ALTER TABLE 'probelocations' ADD INDEX('name')");
		echo "inserting probes done<br>\nsetting zeroed probes\n";
		
		while(!feof($fhzero)) {
			$line = fgets($fhzero);
			#echo "$line<br>";
    			$line = rtrim($line);
    			$pieces = explode("\t",$line);
			if ($pieces[0] != "") {
	
				#$query = mysql_query("Select ID from probelocations WHERE name='$pieces[$nameposzero]' AND chiptype='$chiptypeid'");
				#$row = mysql_fetch_array($query, MYSQL_ASSOC);
				#$probeid = $row['ID'];
				mysql_query("UPDATE probelocations SET zeroed='1' WHERE name='$pieces[$nameposzero]' AND chiptype='$chiptypeid' LIMIT 1");
			}
		}
		fclose("$fhzero");
		# create links for PennCNV files
		system("cd $scriptdir/PennCNV/lib && ln -s hhall.$ucscdb.pfb $chipname.$ucscdb.pfb"); 
		echo "done!<br>\n";
		
	}
		
		 	

}
else {
	echo "illegal request, you're not logged in!";
}

?>
