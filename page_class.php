<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

function calculateAverage($array) {
	$array = array_filter($array);
	if(count($array)) {
		$average = array_sum($array)/count($array);
		return $average;
	}
	else {
		return null;
	}
	
}


// chromhash
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
// table layout
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// connect to database
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");

// inheritance array
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

// diagnostic classes from db
$dc_query = mysql_query("SELECT id, name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
}

// get variables
$sid = $_GET['sid'];
$pid = $_GET['pid'];

$query = mysql_query("SELECT ct.ID FROM chiptypes ct JOIN project p ON p.chiptype = ct.name WHERE p.id = '$pid'");
$row = mysql_fetch_array($query);
$chiptypeid = $row['ID'];

$query = mysql_query("SELECT chip_dnanr FROM sample where id = '$sid'");
$row = mysql_fetch_array($query);
$sample = $row['chip_dnanr'];

$query = mysql_query("SELECT naam FROM project WHERE id = '$pid'");
$row = mysql_fetch_array($query);
$project = $row['naam'];

echo '<script type="text/javascript" src="javascripts/setclass.js"></script>'."\n";

// INSERT New CNV
if (isset($_POST['InsertNew']) && $_POST['chr'] != '' && $_POST['start'] != '' && $_POST['stop'] != '' && $_POST['CN'] != '' && $_POST['arguments'] != '') {
	$inchrtxt = $_POST['chr'];
	$inchr = $chromhash[$inchrtxt];
	$instart = $_POST['start'];
	$instop = $_POST['stop'];
	$incn = $_POST['CN'];
	$arguments = addslashes($_POST['arguments']);
	$insize = $instop - $instart +1;
	$geneq = mysql_query("SELECT COUNT(ID) as aantal FROM genesum WHERE chr = '$inchr' AND ((start BETWEEN $instart AND $instop ) OR (stop BETWEEN $instart AND $instop) OR (start <= $instart AND stop >= $instop))");
	$generow = mysql_fetch_array($geneq);
	$innrgenes = $generow['aantal']; 
	$snpq = mysql_query("SELECT COUNT(ID) as aantal FROM probelocations WHERE chromosome = '$inchr' AND chiptype = '$chiptypeid' AND position BETWEEN $instart AND $instop");
	$snprow = mysql_fetch_array($snpq);
	$insnps = $snprow['aantal'];
	# FIND next 5' probe
	$smaller = "SELECT position FROM probelocations WHERE chromosome = '$inchr' AND chiptype = $chiptypeid AND position < $instart ORDER BY position DESC LIMIT 1";
	$smallq = mysql_query($smaller);
	$smallrows = mysql_num_rows($smallq);
	
	if ($smallrows > 0) {
		$smallrow = mysql_fetch_array($smallq);
		$largestart = $smallrow['position'];
	}
	else {
		$largestart = 'ter';
	}
	# FIND next 3' probe
	$larger = "SELECT position FROM probelocations WHERE chromosome = '$inchr' AND chiptype = $chiptypeid AND position > $instop ORDER BY position ASC LIMIT 1";
	$largeq = mysql_query($larger);
	$largerows = mysql_num_rows($largeq);
	if ($largerows > 0) {
		$largerow = mysql_fetch_array($largeq);	
		$largestop = $largerow['position'];
	}
	else {
		$largestop = 'ter';
	}

	mysql_query("INSERT INTO aberration (chr, start, stop, cn, sample, idproj, nrsnps, nrgenes, size, largestart, largestop, inheritance,Remarks) VALUES ('$inchr', '$instart', '$instop', '$incn', '$sid', '$pid', '$insnps', '$innrgenes', '$insize', '$largestart', '$largestop', '','$arguments|')");
	$insaid = mysql_insert_id();
	$logentry = "Manually added";
	mysql_query("INSERT INTO log (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$insaid','$userid', '$logentry','$arguments')");
}


// output
echo "<div class=sectie>\n";
echo "<h3>Define CNV properties for sample '$sample'</h3>\n";
echo "<h4>Observed in project '$project'</h4>\n";
echo "<p>";
echo "<a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\">Go Back to the sample details</a></p>\n";
echo "<p>\n";

echo "<input type=hidden id='sample' value=$sid>";
echo "<input type=hidden id='project' value=$pid>";
echo "<input type=hidden id='userid' value=$userid>";

echo "<table cellspacing=0>\n";
$switch=0;
echo " <tr>\n"; 
echo "  <th scope=col class=topcellalt $firstcell>Location</th>\n";
echo "  <th scope=col class=topcellalt>Size</th>\n";
echo "  <th scope=col class=topcellalt>CN</th>\n";
echo "  <th scope=col class=topcellalt>Class</th>\n";
echo "  <th scope=col class=topcellalt>Inheritance</th>\n";
echo "  <th scope=col class=topcellalt>Merge</th>\n";
echo "  <th scope=col class=topcellalt>Modify</th>\n"; 
echo " </tr>\n";

$query = mysql_query("SELECT id, chr, start, stop, cn, class, inheritance, nrsnps, datatype FROM aberration WHERE sample = '$sid' AND idproj = '$pid' ORDER BY chr, start"); 
while ($row = mysql_fetch_array($query)) {
	$inheritance = $row['inheritance'];
	$class = $row['class'];
	$aid = $row['id'];
	$cn = $row['cn'];
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$datatype = $row['datatype'];
	if ($cn == 9) {
		# get upd type
		$updq = mysql_query("select type from parents_upd WHERE sid = '$sid' AND chr = '$chr' AND start = '$start' AND stop = '$stop'");
		$updr = mysql_fetch_array($updq);
		$updtype = $updr['type'];
	}

	$csize = $stop - $start + 1;
	$nrsnps = $row['nrsnps'];
	if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] || $nrsnps < $_SESSION['LOHminsnp'] )) {
		echo "<input type=hidden name='inheritance[$aid]' value=$inheritance>";
		continue;
	}

	echo " <tr>\n";
	echo "  <td $tdtype[$switch] $firstcell>chr". $chromhash[ $chr ].":" . number_format($start,0,'',','). "-" . number_format($stop,0,'',',') . "</td>\n";
	echo "  <td $tdtype[$switch] >".number_format($csize,0,'',',')."</td>\n";
	if ($datatype == 'swgs') {
		$tooltiptype = 'sWGS_CNV';
	}
	else {
		$tooltiptype = 'IlluminaSearchSamples';
	}
	//echo "  <td $tdtype[$switch] onmouseover=\"Tip(ToolTip('$aid','$userid','i',event),EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\">";
	echo  "  <td $tdtype[$switch] onClick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$start&amp;stop=$stop&amp;id=$aid&amp;loh=0','$tooltiptype','$userid','full',event)\" >";
	if ($cn == 9) {
		echo "$updtype";
	}
	else {
		echo $cn;
	}
	echo "</td>\n";
	echo "  <td $tdtype[$switch] ><select class='cnvclass' name='class_$aid' id='$aid'>";
	// echo "<option value=null> </option>\n"; 
	foreach ($dc as $class_level => $name) {
		if ($class_level == $class) {
			$selected = 'selected';
		}
		else {
			$selected = '';
		}
		echo "  <option $selected value=$class_level>".$name."</option>\n";
	}
	
	echo "</select></td>\n";
	echo "  <td $tdtype[$switch] ><select class='inheritance' name='inheritance[$aid]' id='$aid' >";
	for ($i = 0;$i<=3;$i++){
		if ($i == $inheritance) {
			$selected = 'selected';
		}
		else {
			$selected = '';
		}
		echo "<option $selected value=$i>".$inh[$inh[$i]]."</option>\n";
	}
	echo " </select></td>\n";
	echo "  <td $tdtype[$switch] ><input class=merge type=checkbox name=merge[] value=$aid id=merge[]></td>\n";
	echo "  <td $tdtype[$switch] ><a class=img href=\"javascript:void(null)\" onclick=\"return popitup('deletecnv.php?aid=$aid&u=$userid&ftt=0')\"><img src='images/content/delete.gif' width=12px height=12px></a> ";
	#if ($level == 3) {
		echo "&nbsp; <a class=img href=\"javascript:void(null)\" onclick=\"return popitup('editcnv.php?aid=$aid&u=$userid&ftt=0')\"><img src='images/content/edit.gif' width=12px height=12px></a>";
	#}
	echo "</td>\n";
	echo " </tr>\n";
	$switch = $switch + pow(-1,$switch);
}
echo "</table>\n</p>";
echo "<p><input type=checkbox name=FromTo value=1 id=FromTo> Merge all segments between first selected and last selected CNV entry.</p>";
echo "<p><input type=submit class=button value='Submit' onClick='updatecnvs()'</p>\n";


echo "</div>\n";

echo "<div class=sectie>\n";
echo "<h3>Insert Extra Regions</h3>\n";
echo "<p>In case you have evidence for a false negative result, you can insert it here. Inserted regions will be listed above for further classification. All fields are mandatory!</p>\n";

echo "<p><form action=\"index.php?page=class&amp;sid=$sid&amp;pid=$pid\" method=POST>\n";
echo "<table cellspacing=0>\n";
echo " <tr>\n"; 
echo "  <th scope=col class=topcellalt $firstcell>Chr</th>\n";
echo "  <th scope=col class=topcellalt>Start</th>\n";
echo "  <th scope=col class=topcellalt>Stop</th>\n";
echo "  <th scope=col class=topcellalt>Copy Number</th>\n";
echo "  <th scope=col class=topcellalt>Arguments</th>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "<td $firstcell><input type=text name=chr size=3 /></td>\n";
echo "<td ><input type=text name=start size=10 /></td>\n";
echo "<td ><input type=text name=stop size=10 /></td>\n";
echo "<td ><input type=text name=CN size=3/></td>\n";
echo "<td ><input type=text name=arguments size=35 maxlength=255></td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</p>\n";
echo "<p><input type=submit class=button value=Insert name=InsertNew>\n";
echo "</form>\n";
echo "</div>\n";

mysql_select_db($db);
echo "<div class=sectie>\n";
echo "<p>\n";
echo "<span class=nadruk>History:</span>\n";
echo "</p>\n";
echo "<p><ul id=ul-log>\n";
$history = mysql_query("SELECT l.aid, l.time, u.FirstName, u.LastName, l.entry, l.arguments FROM log l JOIN users u ON u.id = l.uid WHERE l.sid = '$sid' AND l.pid = '$pid' ORDER BY l.time DESC LIMIT 15");
$rows = mysql_num_rows($history);
while ($row = mysql_fetch_array($history)) {
	$aid = $row['aid'];
	$sq = mysql_query("SELECT chr, start, stop, cn FROM aberration WHERE id = '$aid'");
	$srows = mysql_num_rows($sq);
	if ($srows > 0) {
		$srow = mysql_fetch_array($sq);
		$chr = $srow['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $srow['start'];
		$starttxt = number_format($start,0,'',',');
		$stop = $srow['stop'];
		$stoptxt = number_format($stop,0,'',',');
		$cn = $srow['cn'];
	}
	else {
		$ssq = mysql_query("SELECT chr, start, stop, cn FROM deletedcnvs WHERE id = '$aid'");
		$srow = mysql_fetch_array($ssq);
		$chr = $srow['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $srow['start'];
		$starttxt = number_format($start,0,'',',');
		$stop = $srow['stop'];
		$stoptxt = number_format($stop,0,'',',');
		$cn = $srow['cn'];
	}
	$time = $row['time'];
	$FName = $row['FirstName'];
	$LName = $row['LastName'];
	$logentry = $row['entry'];
	$arguments = $row['arguments'];
	echo "<li>$time : Chr$chrtxt:$starttxt-$stoptxt (cn:$cn) : $logentry by $FName $LName";
	if ($arguments != '') {
		echo " : $arguments</li>\n";
	}
	else {
		echo "</li>\n";
	}
}
echo "</ul>\n";	
echo "</div>\n";
	

}
?>
<div id="txtHint">something must appear here</div>
<! -- LOAD page specific javascript functions --> 

<script type="text/javascript" src="javascripts/ajax_tooltips.js"></script>

