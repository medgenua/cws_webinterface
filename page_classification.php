<?php
ob_start();
if (!isset($loggedin) || $loggedin != 1){
	include('page_login.php');
	exit();
}

for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}

$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["x"] = "23";
$chromhash["y"] = "24";

$chromhash["23"] = "X";
$chromhash["24"] = "Y";

// get action
$type = $_GET['type'];

$dcs = array();
$dc_query = mysql_query("SELECT id, name, abbreviated_name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dcs[$row['id']] = array(
		'abbr' => $row['abbreviated_name'],
		'full' => $row['name'],
	);
}

// 1. present list of classifiers to manage
if ($type == 'manage') {
	echo "<div class=sectie><h3>Edit Classifiers</h3>";
	// get classifiers
	$query = mysql_query("SELECT c.id, c.Name, c.Comment,c.WholeGenome,cu.sharing,cu.editing FROM `Classifier_x_Userpermissions` cu JOIN `Classifier` c  ON cu.fid = c.id WHERE cu.uid = '$userid' ORDER BY c.Name");
	// ,COUNT(cr.id) AS nrreg JOIN `Classifier_x_Regions` cr AND c.id = cr.cid 

	if (mysql_num_rows($query) == 0) {
		echo "<p>You do not have any classifiers.</p>";
	}
	else {
		echo "<p>Select a classifier below using the radio buttons and click 'Edit'. Editing includes adding or removing regions, and changing the classification rules.</p>";
		echo "<p><form action='index.php?page=classification&type=edit' method='POST'><table cellspacing=0><tr><th class=topcellalt $firstcell>Edit</th><th class=topcellalt>Name</th><th class=topcellalt>Comment</th><th class=topcellalt>Number of Regions</th><th class=topcellalt>Set Access Permissions</th></tr>";
		while ($row = mysql_fetch_array($query)) {
			echo "<tr>";
			if ($row['editing'] == 1) {
				echo "<td $firstcell><input type=radio name=editcid value=".$row['id']."></td>";
			}
			else {
				echo "<td $firstcell><input type=radio name=editcid value=".$row['id']." DISABLED></td>";
			}
			echo "<td>".$row['Name']."</td>";
			echo "<td>".$row['Comment']."&nbsp;</td>";
			if ($row['WholeGenome'] == 1) {
				echo "<td>Whole Genome</td>";
			}
			else {
				$nrq = mysql_query("SELECT id FROM `Classifier_x_Regions` WHERE cid = '".$row['id']."'");
				$nrr = mysql_num_rows($nrq);
				echo "<td>$nrr</td>";
			}
			if ($row['sharing'] == 1) {
				echo "<td><a href='index.php?page=classification&type=share&cid=".$row['id']."'>Share</a></td>";
			}
			else {
				echo "<td>&nbsp;</td>";
			}
			echo "</tr>";	
		}
		echo "</table></p>";
		echo "<p><input type=submit class=button name='EditClassifier' value='Edit'></form></p></div>";
		exit();
	}
}

// process submitted values
elseif ($type == 'create') {
   	if (isset($_POST['createstep2'])) {
		echo "<div class=sectie><h3>Create new classification rule : Step 2</h3>";

		$rname = addslashes($_POST['rulename']);
		$rcomment = addslashes($_POST['rulecomment']);
		
		$public = 0;
		if (isset($_POST['rulepublic'])) {
			$public = 1;
		}
		$gender = $_POST['gender'];
		$wg = $_POST['wg'];
		$cn = $_POST['cn'];
		// create rule
		$query = mysql_query("INSERT INTO `Classifier` (Name, Comment, WholeGenome, Gender, CN, Public, uid) VALUES ('$rname','$rcomment','$wg','$gender','$cn','$public','$userid')");
		$rid = mysql_insert_id();
		echo "<p><form action='index.php?page=classification&type=create' method=POST>";
		echo "<input type=hidden name=rid value='$rid' />";
		// add permissions entry
		$query = mysql_query("INSERT INTO `Classifier_x_Userpermissions` (fid, uid, editing,sharing) VALUES ('$rid','$userid','1','1')");

		// Further details: 
		// Class to assign:
		echo "<ol class=indent>";
		echo "<li  style='padding-bottom:1em';><span class=nadruk>Assign class</span>";
		echo "<ul class=indent>";
		echo "<li><select name='dc'>";
		foreach ($dcs as $class => $names) {
			echo "<option value=$class>" . $names['full']. "</option>";
		}
		echo "</select>";	
		echo " : Assign this class to matching variants</li>";
		echo "</ul></li>";
		// needed criteria
		echo" <li style='padding-bottom:1em';><span class=nadruk>Criteria</span> : Select criteria that need to be fulfilled<ul class=indent>";
		  echo "<li><input type=checkbox name=sameCN> : <select name=sameCNdet><option value='exact'>Exact</option><option value='type'>Type (del/dup)</option></select> Previously seen with same CopyNumber</li>";
		  echo "<li><input type=checkbox name=sameDC> : Previously seen with same diagnostic class.</li>";
		  echo "<li><input type=text name=sameNR size=3 value=1> : Minimal number of matching variants needed</li>";
		echo" </ul></li>";
		// allowed/needed overlaps
		echo "<li style='padding-bottom:1em';><span class=nadruk>Overlap</span> : Specify allowed/needed overlap <table>";
		  echo "<tr><td class=clear><img src='images/content/Overlap_bigger.png' style='border:1px solid #aeaeae' /></td><td class=clear><input type=text name=sizebigger size=4 value='1' /> : Maximal Size Ratio for larger new variants (-1 to allow any size)</td></tr>";
		  echo "<tr><td class=clear><img src='images/content/Overlap_smaller.png'  style='border:1px solid #aeaeae' /></td><td class=clear><input type=text name=sizesmaller size=4 value='1' /> : Minimal Size Ratio for smaller new variants (1 equals to identical variants)</td></tr>";
		  echo "<tr><td class=clear><img src='images/content/Overlap_overlap.png'  style='border:1px solid #aeaeae' /></td><td class=clear><input type=text name=sizeoverlapinner size=4 value='1' /> : Minimal Fraction of original variant that should be covered<br/><input type=text name=sizeoverlapouter size=4 value='20' /> : Maximal nr of bases extending in the new variant</td></tr>";
		echo "</table></li>";
		if ($wg == 0){
			echo "<li  style='padding-bottom:1em';><span class=nadruk>Only Classify in these Regions</span>: format: chr1:2000000-5000000<ul class=indent>";
			echo "<li><textarea rows=5 cols=25 name='Regions' ></textarea></li></ul></li>";
		}
		echo "</ol>";
		echo "<p><input type=submit class=button name='createstep3' value='Next'></form></p>";
	}
	elseif (isset($_POST['createstep3'])) {
		$rid = $_POST['rid'];
		$dc = $_POST['dc'];
		if (!is_numeric($dc)) {
			echo "DC Security breach. Not proceeding.<br/>";
			exit;
		}
		$sameCN = 0;
		if (isset($_POST['sameCN'])) {
			$sameCN = 1;
		}
		$sameCNdet = $_POST['sameCNdet'];
		$sameDC = 0;
		if (isset($_POST['sameDC'])) {
			$sameDC = 1;
		}
		$sameNR = $_POST['sameNR'];
		if (!is_numeric($sameNR)) {
			echo "SNR Security breach. Not proceeding.<br/>";
			exit;
		}
		$sizebigger = $_POST['sizebigger'];
		if ($sizebigger != '0' && !is_numeric($sizebigger)) {
			echo "SB Security breach. Not proceeding.<br/>";
			exit;
		}
		$sizesmaller = $_POST['sizesmaller'];
		if ($sizesmaller != '0' && !is_numeric($sizesmaller)) {
			echo "SS Security breach. Not proceeding.<br/>";
			exit;
		}
		$sizeoverlapinner = $_POST['sizeoverlapinner'];
		if ($sizeoverlapinner != '0' && !is_numeric($sizeoverlapinner)) {
			echo "SOI Security breach. Not proceeding.<br/>";
			exit;
		}
		$sizeoverlapouter = $_POST['sizeoverlapouter'];
		if ($sizeoverlapouter != '0' && !is_numeric($sizeoverlapouter)) {
			echo "SOO Security breach. Not proceeding.<br/>";
			exit;
		}
		// set global details.
		mysql_query("UPDATE `Classifier` SET dc = '$dc', scn = '$sameCN', scnd = '$sameCNdet', sdc = '$sameDC', snr = '$sameNR', sbigger = '$sizebigger', ssmaller = '$sizesmaller', soinner = '$sizeoverlapinner', soouter = '$sizeoverlapouter' WHERE id = '$rid'");
		$failed = '';
		// where there regions specified ?
		if (isset($_POST['Regions'])) {
			$regions = explode("\n",$_POST['Regions']);
			foreach($regions as $key => $region) {
				// UCSC style
				$region = trim($region);
				if (preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $region)) {				
					$pieces = preg_split("/[:\-_+]/",$region);
					$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
					if ($chrtxt == '23' || $chrtxt == '24'){
						$chrtxt = $chromhash[$chrtxt];
					}
					$chr = $chromhash[ $chrtxt ] ;
					$start = $pieces[1];
					$start = str_replace(",","",$start);
					if ($start < 0) {
						$start = 0;
					}
					$end = $pieces[2];
					$end = str_replace(",","",$end);
					// check end
					$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
					$nrow = mysql_fetch_array($nq);
					$chrstop = $nrow['stop'];
					if ($end > $chrstop) {
						$end = $chrstop;
					}
				
					$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
					mysql_query("INSERT INTO `Classifier_x_Regions` (cid,chr,start,stop) VALUES ($rid,$chr,$start,$end)");
				}
				else {
					$failed .= "$region<br/>";
				}
			}
		}
		if ($failed != '') {
			echo "<div class=sectie>";
			echo "<h3>Failed Regions</h3>";
			echo "<p>The following regions were not recognized as valid genomic coordinates:<br/>";
			echo "$failed";
			echo "</p><p>These regions can be corrected and added on the 'Edit Classifier page'</p></div>";
		}
		else {
			echo "<div class=sectie>";
			echo "<h3>Classifier created</h3>";
			echo "<p>Classifier was successfully created. Activate and edit using the menu options above.</p>";
			echo "</div>";			
		}
		

	}
   	else {
		echo "<div class=sectie><h3>Create new classification rule</h3>";
		echo "<p>Variants can be automatically assigned to diagnostic classes based on flexible rules. You should first provide some general information here, followed by more specific details.</p>";
		echo "<p><form action='index.php?page=classification&type=create' method=POST>";
		echo "<ol class=indent>";
		// general
		echo "<li ><span class=nadruk>General Information</span>";	
		echo "<ul class=indent>";
		echo "<li><input type=text size=30 name=rulename> : Rule Name</li>";
		echo "<li><input type=text size=30 name=rulecomment> : Additional Comments</li>";
		echo "<li><input type='checkbox' name=rulepublic value=1> : Make this filter public (usable by everybody)</li>";
		echo "</ul>";
		echo "</li>";
		// actual rules
		echo "<li ><span  class=nadruk>Classification details</span>";
		echo "<ul class=indent style='padding-top:1em;padding-bottom:1em;'>";
		echo "<li><select name=gender><option value='Male,Female'>Male and Female</option><option value='Male'>Male only</option><option value='Female'>Female Only</option></select> : Apply to specific gender</li>";
		echo "<li><select name=wg><option value='1'>Whole Genome</option><option value='0'>Specify Regions</option></select> : Apply to specific regions</li>";
		echo "<li><select name=cn><option value='0,1,2,3,4'>All</option><option value='0'>0x (hom.del)</option><option value='1'>1x (het.del)</option><option value='2'>2x (LOH)</option><option value='3'>3x (1x dup)</option><option value='4'>4x (ampl)</option><option value='0,1'>Del (0/1x)</option><option value='3,4'>Dup (3/4x)</option></select> : Apply to specific Copy-Number</li>";
		echo "</ul>";
		echo "</ol>";
		echo "<p><input type=submit class=button name='createstep2' value='Next'></form></p>";
		echo "</div>";
   	}
}
// 2. edit selected and submitted classifier
elseif ($type == 'edit') {
	// filter selected?
	if (!isset($_POST['editcid']) || !is_numeric($_POST['editcid'])) {
		print "<div class=sectie><h3>Problem detected</h3>";
		print "<p>No classifier was selected. Please go back and select a filter.</p>";
		print "</div>";
		exit();
	}
	$cid = $_POST['editcid'];
	// details were updated?
	if (isset($_POST['UpdateGeneral'])) {
		$newname = addslashes($_POST['cname']);
		$newcomment = addslashes($_POST['ccomment']);
		if (isset($_POST['cpublic'])) {
			$newpublic = 1;
		}
		else {
			$newpublic = 0;
		}
		$wg = $_POST['wg'];;
		$dc = $_POST['dc'];
		$sameCN = (isset($_POST['scn']) ? 1 : 0);
		$sameCNdet = $_POST['scnd'];
		$sameDC = $_POST['sdc'];
		$sameNR = $_POST['snr'];
		$sizebigger = $_POST['sbigger'];
		$sizesmaller = $_POST['ssmaller'];
		$sizeoverlapinner = $_POST['soinner'];
		$sizeoverlapouter = $_POST['soouter'];
		$gender = $_POST['gender'];
		$cn = $_POST['cn'];
		//$query = mysql_query("UPDATE `Classifier` SET Name = '$newname', Comment = '$newcomment', Public = $newpublic WHERE fid = $fid");
		mysql_query("UPDATE `Classifier` SET CN = '$cn', dc = '$dc', scn = '$sameCN', scnd = '$sameCNdet', sdc = '$sameDC', snr = '$sameNR', sbigger = '$sizebigger', ssmaller = '$sizesmaller', soinner = '$sizeoverlapinner', soouter = '$sizeoverlapouter', Name = '$newname', Comment = '$newcomment', Public = '$newpublic',WholeGenome = '$wg', Gender = '$gender' WHERE id = '$cid'");
	}
	// get details
	$query = mysql_query("SELECT dc,scn,scnd,sdc,snr,sbigger,ssmaller,soinner,soouter,Name,Comment,Public,WholeGenome,Gender,CN FROM `Classifier` WHERE id = '$cid' AND uid = '$userid'");
	$row = mysql_fetch_array($query);
	echo "<div class=sectie>";
	echo "<h3>Editing: '" . stripslashes($row['Name']). "'</h3>";
	if (isset($_POST['st'])) {
		$st = $_POST['st'];
	}
	else {
		$st = 'general';
	}
	$options = array('general' => 'Edit general details','add' => 'Add Extra Regions','edit' => 'Edit or Remove Regions');
	echo "<p><form action='index.php?page=classification&type=edit' method='POST'><input type=hidden name=editcid value=$cid>";
	echo "<span class=nadruk >Select action:</span></p>";
	echo "<p class=indent><select name='st'>";
	foreach ( $options as $action => $comment) {
		if ($action == $st) {
			echo "<option value='$action' selected>$comment</option>";
		}
		else {
			echo "<option value='$action'>$comment</option>";
		}
	}
	echo "</select> &nbsp; <input type=submit class=button name='EditClassifier' value='Select'></form></p>";

	// based on action, print output.
	if ($st == 'general') {
		echo "<p><span class=nadruk >Edit general details:</span></p>";
		echo "<p>";
		echo "<form action='index.php?page=classification&type=edit' method='POST'>";
		echo "<input type=hidden name=editcid value=$cid><input type=hidden name=st value='$st'>";
		echo "<ul class=indent>";
		echo "<li>Classifier name: <input type=text size=30 name=cname value='".stripslashes($row['Name'])."' /></li>";
		echo "<li>Classifier description: <input type=text name=ccomment size=30 value='".stripslashes($row['Comment'])."' /></li>";
		// class to assign
		echo "<li>Assign Class: <select name='dc'>";
		foreach ($dcs as $class => $names) {
			if ($row['dc'] == $class) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			echo "<option value=$class $sel>" . $names['full'] . "</option>";
		}
		echo "</select></li>";	
		$cv = array(0=>"",1=>"CHECKED");
		$sv = array(0=>"",1=>"SELECTED");
		// public ?
		echo "<li>Classifier is public: <input type=checkbox name=cpublic value=1 ".$cv[$row['Public']]." /></li>";
		// whole genome ?
		if ($row['WholeGenome'] == 1) {
			echo "<li>Apply WholeGenome/Targeted: <select name=wg><option value='1' SELECTED>Whole Genome</option><option value='0'  >Targeted</option></select></li>";
		}
		else {
			echo "<li>Apply WholeGenome/Targeted: <select name=wg><option value='1' >Whole Genome</option><option value='0' SELECTED >Targeted</option></select></li>";
		}			
		// copynumber ?
		echo "<li>Apply to Specific CopyNumber: <select name=cn>";
		echo ($row['CN'] == '0,1,2,3,4' ? "<option value='0,1,2,3,4' SELECTED>All</option>" : "<option value='0,1,2,3,4' >All</option>");
		echo ($row['CN'] == '0' ? "<option value='0' SELECTED>0x (hom.del)</option>" : "<option value='0'>0x (hom.del)</option>");
		echo ($row['CN'] == '1' ? "<option value='1' SELECTED>1x (het.del)</option>" : "<option value='1'>1x (het.del)</option>");
		echo ($row['CN'] == '2' ? "<option value='2' SELECTED>2x (LOH)</option>" : " <option value='2' >2x (LOH)</option>"); 
		echo ($row['CN'] == '3' ? "<option value='3' SELECTED>3x (1x dup)</option>":"<option value='3'>3x (1x dup)</option>");
		echo ($row['CN'] == '4' ? "<option value='4' SELECTED>4x (ampl)</option>":"<option value='4'>4x (ampl)</option>");
		echo ($row['CN'] == '0,1' ? "<option value='0,1' SELECTED>Del (0/1x)</option>" : "<option value='0,1' >Del (0/1x)</option>");
		echo ($row['CN'] == '3,4' ? "<option value='3,4' SELECTED>Dup (3/4x)</option>" :  "<option value='3,4'>Dup (3/4x)</option>");
		echo "</select></li>";
		// same Copy Number needed? 
		echo "<li> Previously seen with same CopyNumber : <input type=checkbox name='scn' ".$cv[$row['scn']]." /> : <select name=scnd>";
		echo ($row['scnd'] == 'exact' ? "<option value='exact' SELECTED >Exact</option><option value='type'>Type (del/dup)</option>" : "<option value='exact'>Exact</option><option value='type' SELECTED>Type (del/dup)</option>");
		echo " </select> </li>";
		// gender? 
		echo "<li>Apply to Specific Genders: <select name=gender>";	
		echo ($row['Gender'] == 'Male,Female' ? "<option value='Male,Female' SELECTED>Male and Female</option>" : "<option value='Male,Female'>Male and Female</option>");
		echo ($row['Gender'] == 'Male' ? "<option value='Male' SELECTED>Male only</option>" : "<option value='Male' >Male only</option>" );
		echo ($row['Gender'] == 'Female' ? "<option value='Female' SELECTED>Female Only</option>" : "<option value='Female' >Female Only</option>");
		echo "</select></li>";
		// sameDC
		echo "<li>Previously seen with same diagnostic class: <input type=checkbox name=sdc value=1 ".$cv[$row['sdc']]."> </li>";	
		// nr Regions needed
		echo "<li>Minimal number of matching variants needed : <input type=text name=snr size=3 value='".$row['snr']."' ></li>";
		// overlaps
		echo "<li>Allowed/Required Overlaps: <table>";
		  echo "<tr><td class=clear><img src='images/content/Overlap_bigger.png' style='border:1px solid #aeaeae' /></td><td class=clear><input type=text name=sbigger size=4 value='".$row['sbigger']."' /> : Maximal Size Ratio for larger new variants (-1 to allow any size)</td></tr>";
		  echo "<tr><td class=clear><img src='images/content/Overlap_smaller.png'  style='border:1px solid #aeaeae' /></td><td class=clear><input type=text name=ssmaller size=4 value='".$row['ssmaller']."' /> : Minimal Size Ratio for smaller new variants (1 equals to identical variants)</td></tr>";
		  echo "<tr><td class=clear><img src='images/content/Overlap_overlap.png'  style='border:1px solid #aeaeae' /></td><td class=clear><input type=text name=soinner size=4 value='".$row['soinner']."' /> : Minimal Fraction of original variant that should be covered<br/><input type=text name=soouter size=4 value='".$row['soouter']."' /> : Maximal nr of bases extending in the new variant</td></tr>";
		echo "</table></li>";
		echo "</ul></p>";
		echo "<p class=indent><input type=submit class=button name='UpdateGeneral' value='Save'></form></p>";
	}
	elseif ($st == 'add') {
		## process added regions
		if (isset($_POST['submitadd'])) {
			$newregions = array();
			$regions = explode("\n",$_POST['addpaste']);
			$failed = '';
			foreach ($regions as $key => $region) {	
				// UCSC style
				$region = trim($region);
				if (preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $region)) {				
					$pieces = preg_split("/[:\-_+]/",$region);
					$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
					if ($chrtxt == '23' || $chrtxt == '24'){
						$chrtxt = $chromhash[$chrtxt];
					}
					$chr = $chromhash[ $chrtxt ] ;
					$start = $pieces[1];
					$start = str_replace(",","",$start);
					if ($start < 0) {
						$start = 0;
					}
					$end = $pieces[2];
					$end = str_replace(",","",$end);
					// check end
					$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
					$nrow = mysql_fetch_array($nq);
					$chrstop = $nrow['stop'];
					if ($end > $chrstop) {
						$end = $chrstop;
					}
				
					//$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
					mysql_query("INSERT INTO `Classifier_x_Regions` (cid,chr,start,stop) VALUES ($cid,$chr,$start,$end)");
				}
				else {
					$failed .= "$region<br/>";
				}
			}
		
			if ($failed != '') {
				echo "<p><span class=nadruk>Failed Regions</span></p>";
				echo "<p>The following regions were not recognized as valid genomic coordinates:<br/>";
				echo "$failed";
				echo "</p><p>These regions can be corrected and added on the 'Edit Classifier page'</p></div>";
			}
		}
		echo "<p><span class=nadruk>Add Regions:</span></p>";
		echo "<p class=indent>The format of provided regions should follow UCSC notation (chr3:154000000-157000000)</p>";
		echo "<form action='index.php?page=classification&type=edit' method='POST'>";
		echo "<input type=hidden name=editcid value=$cid><input type=hidden name=st value='$st'>";
		echo "<span style='width:15em;float:left' >Paste Interval Regions:</span><span style='float:toleft;'><textarea name='addpaste' cols=40 rows=6></textarea></span><br/>";
		echo "<span class=indent><input type=submit name=submitadd value='Submit' class=button></span></form>";
		echo "</p>";
	}
	elseif ($st == 'edit') {
		// process changes to values.
		if (isset($_POST['UpdateRegions'])) {
			## get regionIDs from db
			$query = mysql_query("SELECT id FROM `Classifier_x_Regions` WHERE cid = '$cid'");
			while ($row = mysql_fetch_array($query)) {
				$rid = $row['id'];
				$chr = $chromhash[$_POST["chr"]["$rid"]];
				$start = $_POST["start"]["$rid"];
				$stop = $_POST["stop"]["$rid"];
				$start = str_replace(",","",$start);
				$stop = str_replace(",","",$stop);
				if (!is_numeric($chr) || $chr == 0 || !is_numeric($start) || !is_numeric($stop)) {
					echo "<span class=nadruk>Error:$rid : </span> Region: $chr:$start:$stop is invalid. not updated.<br/>";
					continue;
				}
				if ($start < 0) {
					$start = 0;
				}
				## stop not checked for > chr.length (todo)
				mysql_query("UPDATE `Classifier_x_Regions` SET chr = '$chr', start = '$start', stop = '$stop' WHERE id = '$rid'");
			}
		}
		elseif(isset($_POST['DeleteRegions'])) {
			$todelete = $_POST['rdel'];
			if (count($todelete) == 0) {
				echo "No regions selected to delete<br/>";
			}
			else {
				$string = implode(",",$todelete);
				mysql_query("DELETE FROM `Classifier_x_Regions` WHERE id IN ($string)");
				echo "DELETE FROM `Classifier_x_Regions` WHERE id IN ($string)<br/>";
			}	

		}
		echo "<p><span class=nadruk>Edit Regions</span></p>";
		echo "<p>All the regions in the current Classifier are listed below. Two actions can be taken from here. First, you can edit any or all fields, and press update at the end of the page. This will save the updated values to the database. Second, you can select regions using the checkboxes at the front of each row, and press delete to remove them from the filter. <br/><span class=nadruk>Note:</span> If you delete regions, changes to other regions will NOT be saved!</p>";
		echo "<form action='index.php?page=classification&type=edit' method='POST'>";	 
		echo "<input type=hidden name=editcid value=$cid><input type=hidden name=st value='$st'>";
		echo "<p class=indent>";
		echo "<table cellspacing=0 style='width:95%'>";
		echo "<tr><th class=light>Delete</th><th class=light>Chrom</th><th class=light>Start</th><th class=light>Stop</th></tr>";
		$query = mysql_query("SELECT id, chr, start, stop FROM `Classifier_x_Regions` WHERE cid = '$cid' ORDER BY chr ASC, start ASC, stop ASC");
		while ($row = mysql_fetch_array($query)) {
			$rid = $row['id'];
			$out = "<tr>";
			$out .= "<td class=light><input type=checkbox name='rdel[]' value='$rid' /></td>";
			$out .= "<td class=light><input type=text name='chr[$rid]' value='".$chromhash[$row['chr']]."' size=4 /></td>";
			$out .= "<td class=light><input type=text name='start[$rid]' value='".$row['start']."' size=10 /></td>";
			$out .= "<td class=light><input type=text name='stop[$rid]' value='".$row['stop']."' size=10 /></td>";
			$out .= "</tr>";
			echo $out;
		}
		echo "<tr><td class=last colspan=4>&nbsp;</td></tr>";
		echo "</table></p>";
		echo "<p><input type=submit class=button value='Update Regions' name='UpdateRegions'> &nbsp; <input type=submit class=button value='Delete Selected Regions' name='DeleteRegions'></form></p>";

	}

	echo "</div>"; // sectie

}
elseif($type == 'activate') {
	// process.
	if (isset($_POST['ActivateClassifiers'])) {
		//loop them cids
		foreach ($_POST['cids'] as $key => $cid) {
			$to = isset($_POST["to_$cid"]) ? $_POST["to_$cid"] : 0;
			$ip = isset($_POST["ip_$cid"]) ? $_POST["ip_$cid"] : 0;
			$ic = isset($_POST["ic_$cid"]) ? $_POST["ic_$cid"] : 0;
			mysql_query("INSERT INTO `Users_x_Classifiers` (uid, cid, Type,MinSNP,TrackOnly,IncludeParents,IncludeControls) VALUES ('$userid','$cid','".$_POST["type_$cid"]."','".$_POST["ms_$cid"]."',$to,$ip,$ic ) ON DUPLICATE KEY UPDATE Type = '".$_POST["type_$cid"]."',TrackOnly = $to , IncludeParents = $ip, IncludeControls = $ic,MinSNP = '".$_POST["ms_$cid"]."'");
		}
	}

	// print filters 
	echo "<div class=sectie>";
	echo "<h3>(De)Activate Classifiers</h3>";
	echo "<p>Select what classifiers should be applied, and how they should be applied. Take the following into account:</p>";
	echo "<p><span class=nadruk>Classifier types:</span><br/>";
	echo "- Assign : Assign classes permanently during CNV analysis. Assignment is static and logged<br/>";
	echo "- Suggest : Suggest classes on existing data during browsing. Assigment is dynamic.<br/>";
	echo "</p>";
	echo "<p><span class=nadruk>Min.SNP:</span><br/>";
	echo "&nbsp;&nbsp;Apply this rule only to CNVs containing at least this number of SNPs. <br/>";
	echo "</p>";
	echo "<p><span class=nadruk>Reference Samples:</span><br/>";
	echo "- Track Only : Only use those samples that are included in search results and region browser. This excludes duplicates and low quality samples<br/>";
	echo "- Include Parents : Include parental data in the classification. By default, these data are not used.<br/>";
	echo "- Include Controls : Include control data in the classification (Mainly HapMap data). By default, these data are not used.<br/>";
	echo "</p>";

	echo "<form action='index.php?page=classification&type=activate' method=POST>";
	echo "<p><table cellspacing=0>";
	// own and shared filters
	echo "<tr><th $firstcell colspan=7>Your Classifiers &amp; Shared Classifiers</th></tr>";
	echo "<tr><th $firstcell class=topcellalt >Classifier Name</th><th class=topcellalt>Nr.Regions</th><th class=topcellalt>Classifier Type</th><th class=topcellalt>Target Class</th><th class=topcellalt>Min.SNP</th><th class=topcellalt>Classifier Details</th><th class=topcellalt>Reference Samples</th></tr>";
	
	$query = mysql_query("SELECT c.id, c.Name, c.Comment, c.WholeGenome, c.Gender,c.CN,c.public,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter FROM `Classifier_x_Userpermissions` cu JOIN `Classifier` c  ON cu.fid = c.id  WHERE cu.uid = '$userid' ORDER BY c.Name");
	// COUNT(cr.id) AS nrreg FROM JOIN `Classifier_x_Regions` cr AND c.id = cr.cid
	$types = array("0" => "Disabled", "1" => "Assign", "2" => "Suggest", "3" => "Assign &amp; Suggest");
	
	if (mysql_num_rows($query) == 0) {
		echo "<tr><td $firstcell colspan=7><span class=italic>No Classifiers available</td></tr>";
	}
	$cntypes = array(0=>"Hom.Del",1=>"Het.Del",2=>"LOH",3=>"1xDup",4=>"2xDup");
	$cv = array(0=>"",1=>"CHECKED");
	// keep track of ids in first table part.
	$notin = '';
	while ($row = mysql_fetch_array($query)) {
		$cid = $row['id'];
		$out = "<tr><td $firstcell title='".$row['Comment']."'><input type=hidden name='cids[]' value=$cid />".$row['Name']."</td>";
		$wg = $row['WholeGenome'];
		if ($wg == 1) {
			$out .= "<td>Whole Genome</td>";
		}
		else {
			$nrq = mysql_query("SELECT id FROM `Classifier_x_Regions` WHERE cid = '$cid'");
			$nrr = mysql_num_rows($nrq);
			$out .= "<td>$nrr</td>";
		}
		// get type
		$subquery = mysql_query("SELECT MinSNP,Type,TrackOnly,IncludeParents,IncludeControls FROM `Users_x_Classifiers` WHERE uid = '$userid' AND cid = '$cid'");
		if (mysql_num_rows($subquery) == 0) {
			$type = '0';
			$to = 0;
			$ip = 0;
			$ic = 0;
			$msnp = 3;
		}	
		else {
			$srow = mysql_fetch_array($subquery);
			$type = $srow['Type'];
			$to = $srow['TrackOnly'];
			$ip = $srow['IncludeParents'];
			$ic = $srow['IncludeControls'];
			$msnp = $srow['MinSNP'];
		}
		// type
		$out .= "<td><select name='type_$cid'>";
		for ($i = 0;$i<=3;$i++) {
			if ($type == $i) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			$out .= "<option value='$i' $sel>".$types[$i]."</option>";
		}
		$out .= "</select></td>";
		$out .= "<td>".$dcs[$row['dc']]['full']."</td>";
		$out .= "<td><input type=text name='ms_$cid' value='$msnp' size=7 /></td>";
		// print details overview.
		// assign to which cn
		$tcns = explode(",",$row['CN']);
		$details = "Apply to: ";
		foreach($tcns as $idx => $cn){
			$details .= $cntypes[$cn] ."," ;
			//$details .= $cn;
		}
		// in which genders
		$details .= " in ".$row['Gender']."<br/>";
		// requires cn, nr, ...
		$snr = $row['snr'];
		$scn = $row['scn'];
		$scnd = $row['scnd'];
		$sdc = $row['sdc'];
		if ($scn == 1) {
			$details .= "CopyNumber match required: Yes ($scnd)<br/>";
		}
		else {
			$details .= "CopyNumber match required: No<br/>";
		}
		if ($sdc == 1) {
			$details .= "Diagnostic Class match required: Yes (".$dcs[$row['dc']]['full'].")<br/>";
		}
		else {
			$details .= "Diagnostic Class match required: No<br/>";
		}
		$details .= "Nr. of matching CNVs required : $snr<br/>";
		// overlap sizes.
		$details .= "Allowed/Required Size Constraints: Smaller:".$row['ssmaller']." ; Bigger:".$row['sbigger']." ; Overlap_Inner_Part:".$row['soinner']." ; Overlap_Extending_Part:".$row['soouter']; 
		//
		$out .= "<td>$details</td>";	
		$out .= "<td NOWRAP><input type=checkbox name='to_$cid' value=1 $cv[$to] /> Track Only<br/><input type=checkbox name='ip_$cid' value=1 $cv[$ip] /> Include Parents<br/><input type=checkbox name='ic_$cid' value=1 $cv[$ic] /> Include Controls</td>";
		$out .= "</tr>";
		$notin .= "$fid,";
		echo $out;		
	}
	// public classifiers, not listed above.
	if ($notin != "") {
		$notin = "AND c.id NOT IN (".substr($notin,0,-1).")";
	}
	echo "<tr><th $firstcell colspan=7>Public Classifiers</th></tr>";
	echo "<tr><th $firstcell class=topcellalt >Classifier Name</th><th class=topcellalt>Nr.Regions</th><th class=topcellalt>Classifier Type</th><th class=topcellalt>Target Class</th><th class=topcellalt>Min.SNP</th><th class=topcellalt>Classifier Details</th><th class=topcellalt>Reference Samples</th></tr>";
	
	$query = mysql_query("SELECT c.id, c.Name, c.Comment, c.WholeGenome, c.Gender,c.CN,c.public,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter FROM  `Classifier` c WHERE c.public = 1 $notin ORDER BY c.Name");
	
	if (mysql_num_rows($query) == 0) {
		echo "<tr><td $firstcell colspan=7><span class=italic>No Classifiers available</td></tr>";
	}

	while ($row = mysql_fetch_array($query)) {
		$cid = $row['id'];
		$out = "<tr><td $firstcell title='".$row['Comment']."'><input type=hidden name='cids[]' value=$cid />".$row['Name']."</td>";
		$wg = $row['WholeGenome'];
		if ($wg == 1) {
			$out .= "<td>Whole Genome</td>";
		}
		else {
			$nrq = mysql_query("SELECT id FROM `Classifier_x_Regions` WHERE cid = '$cid'");
			$nrr = mysql_num_rows($nrq);
			$out .= "<td>$nrr</td>";
		}
		// get type
		$subquery = mysql_query("SELECT MinSNP,Type,TrackOnly,IncludeParents,IncludeControls FROM `Users_x_Classifiers` WHERE uid = '$userid' AND cid = '$cid'");
		if (mysql_num_rows($subquery) == 0) {
			$type = '0';
			$to = 0;
			$ip = 0;
			$ic = 0;
			$msnp = 3;
		}	
		else {
			$srow = mysql_fetch_array($subquery);
			$type = $srow['Type'];
			$to = $srow['TrackOnly'];
			$ip = $srow['IncludeParents'];
			$ic = $srow['IncludeControls'];
			$msnp = $srow['MinSNP'];
		}

		$out .= "<td><select name='type_$cid'>";
		for ($i = 0;$i<=3;$i++) {
			if ($type == $i) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			$out .= "<option value='$i' $sel>".$types[$i]."</option>";
		}
		$out .= "</select></td>";
		$out .= "<td>".$dcs[$row['dc']]['full']."</td>";
		$out .= "<td><input type=text name='ms_$cid' value='$msnp' size=7 /></td>";
		// print details overview.
		// assign to which cn
		$tcns = explode(",",$row['CN']);
		$details = "Apply to: ";
		foreach($tcns as $idx => $cn){
			$details .= $cntypes[$cn] ."," ;
			//$details .= $cn;
		}
		// in which genders
		$details .= " in ".$row['Gender']."<br/>";
		// requires cn, nr, ...
		$snr = $row['snr'];
		$scn = $row['scn'];
		$scnd = $row['scnd'];
		$sdc = $row['sdc'];
		if ($scn == 1) {
			$details .= "CopyNumber match required: Yes ($scnd)<br/>";
		}
		else {
			$details .= "CopyNumber match required: No<br/>";
		}
		if ($sdc == 1) {
			$details .= "Diagnostic Class match required: Yes (".$dcs[$row['dc']]['full'].")<br/>";
		}
		else {
			$details .= "Diagnostic Class match required: No<br/>";
		}
		$details .= "Nr. of matching CNVs required : $snr<br/>";
		// overlap sizes.
		$details .= "Allowed/Required Size Constraints: Smaller:".$row['ssmaller']." ; Bigger:".$row['sbigger']." ; Overlap_Inner_Part:".$row['soinner']." ; Overlap_Extending_Part:".$row['soouter']; 
		//
		$out .= "<td>$details</td>";	
		$out .= "<td NOWRAP><input type=checkbox name='to_$cid' value=1 $cv[$to] /> Track Only<br/><input type=checkbox name='ip_$cid' value=1 $cv[$ip] /> Include Parents<br/><input type=checkbox name='ic_$cid' value=1 $cv[$ic] /> Include Controls</td>";

		$out .= "</tr>";
		echo $out;		
	}
	echo "</table>";
	echo "</p><p><input type='submit' class=button name='ActivateClassifiers' value='Update Settings'></form></p>";

	echo "</div>";

}
//////////////////////	
// SHARING OPTIONS  //
//////////////////////
elseif ($type == 'share') {
	// check numeric fid.
	$cid = $_GET['cid'];
	if (!is_numeric($cid)) {
		echo "<div class=sectie><h3>Invalid classifier ID</h3><p>Don't mess with the system!</p></div>";
		exit;
	}
	// check permissions
	$query = mysql_query("SELECT fid FROM `Classifier_x_Userpermissions` WHERE fid = '$cid' AND uid = '$userid' AND sharing = '1'");
	if (mysql_num_rows($query) == 0) {	
		echo "<div class=sectie><h3>Access Denied</h3><p>You're not allowed to share this classifier.</p></div>";
		exit;
	}
	// get filter details
	$query = mysql_query("SELECT Name, Comment FROM `Classifier` WHERE id = '$cid'");
	$row = mysql_fetch_array($query);
	$cname = stripslashes($row['Name']);
	$ccomment = stripslashes($row['Comment']);
	// print header
	echo "<div class=sectie><h3>Setting permissions for '$cname'</h3>";
	echo "<h4>Filter Info: $ccomment</h4>";

	// FIRST shared with new users, present options or finalise options.
	if (isset($_POST['share'])) {
		$addusers = $_POST['users'];	
		if (count($addusers) > 0) {
			echo "<p>Specify the permissions for the users you want to share this classifier with, and press 'Finish'.</p>\n";
			echo "<form action='index.php?page=classification&type=share&cid=$cid' method=POST>\n";
			echo "<table cellspacing=0>\n";
			echo "<th $firstcell class=topcellalt>User</th>\n";
			echo "<th class=topcellalt>Edit Regions &amp; Details</th>\n";
			echo "<th class=topcellalt>Project Control</th>\n";
			echo "</tr>\n";
			foreach($addusers as $uid) {
				// get userinfo 
				$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$uid'");
				$row = mysql_fetch_array($query);
				$fname = $row['FirstName'];
				$lname = $row['LastName'];
				// print table row
				echo "<tr>\n";
				echo "<td $firstcell><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
				echo "<td><input type=radio name='edit_$uid' value=1 checked> Yes <input type=radio name='edit_$uid' value=0> No</td>\n";
				echo "<td><input type=radio name='share_$uid' value=1 checked> Yes <input type=radio name='share_$uid' value=0> No</td>\n";
				echo "</tr>\n";
			}
			echo "</table>\n";
			echo "</p>\n";
			echo "<input type=submit class=button name=finalise value='Finish'>\n";
			echo "</form>\n";
		}
		echo "<p><a href='index.php?page=classification&type=share&cid=$cid'>Go Back</a></p>\n";
		echo "</div>\n";
		exit();
	}
	elseif (isset($_POST['finalise'])) {
		$addusers = $_POST['users'];
		$yesno = array('Not Allowed','Allowed');
		// print table
		echo "<p>The following users gained access to the classifier.</p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<th $firstcell class=topcellalt>User</th>\n";
		echo "<th class=topcellalt>Edit Regions &amp; Details</th>\n";
		echo "<th class=topcellalt>Project Control</th>\n";
		echo "</tr>\n";
		foreach($addusers as $uid) {
			$editing = $_POST["edit_$uid"];
			$sharing = $_POST["share_$uid"];
			// user info
			$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$uid'");
			$row = mysql_fetch_array($query);
			$fname = $row['FirstName'];
			$lname = $row['LastName'];
			// print table row
			echo "<tr>\n";
			echo "<td $firstcell>$lname $fname</td>\n";
			echo "<td>".$yesno[$editing]."</td>\n";
			echo "<td>".$yesno[$sharing]."</td>\n";
			echo "</tr>\n";
			// INSERT or UPDATE permissions
			$insquery = mysql_query("INSERT INTO `Classifier_x_Userpermissions` (fid, uid, editing, sharing) VALUES ('$cid','$uid', '$editing', '$sharing') ON DUPLICATE KEY UPDATE editing = if(VALUES(editing) < '$editing', VALUES(editing),'$editing'), sharing = if(VALUES(sharing) < '$sharing', VALUES(sharing),'$sharing')");
		// notify the user (to inbox)
		$subject = "You were granted access to a new filter ($cname)";
		$insquery = mysql_query("INSERT INTO inbox (inbox.from, inbox.to, inbox.subject, inbox.body, inbox.type, inbox.values, inbox.date) VALUES ('$userid','$uid','$subject','','shared_classifier','$cid',NOW())");
	}
	echo "</table></p>";
	echo "<p><a href='index.php?page=classification&type=share&cid=$cid'>Go Back</a></p>\n";	
	echo "</div>\n";
	exit();

	}	
	
	// SECOND main page
	echo "<p>Select the users you want to share this classification with from the list below and press 'Share'. Exact permissions will be presented in the next step.</p>\n";
	echo "<p>\n";
	echo "<form action='index.php?page=classification&type=share&cid=$cid' method=POST>\n";
	// get users with full access
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name, fu.editing, fu.sharing FROM `users` u JOIN  `Classifier_x_Userpermissions` fu JOIN `affiliation` a ON u.Affiliation = a.id AND u.id = fu.uid WHERE fu.fid = '$cid' ORDER BY a.id, u.LastName");
	$uwa = array();
	while ($row = mysql_fetch_array($query)) {
		$lname = $row['LastName'];
		$currid = $row['id'];
		$fname = $row['FirstName'];
		$affi = $row['name'];
		$editing = $row['editing'];
		$sharing = $row['sharing'];
		$uwa[$currid] = "$lname $fname@@@$affi@@@$editing@@@$sharing";
	}
	// list users with no or restricted access
	echo "<span class=nadruk>Select users to grant access</span><br>\n";
	echo "<select name='users[]' size=15 MULTIPLE style='margin-left:1em;'>\n";
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY a.name, u.LastName");
	$currinst = '';
	while($row = mysql_fetch_array($query)) {
		$uid = $row['id'];
		// skip users with full access
		if (array_key_exists($uid, $uwa)) {
			$pieces = explode('@@@',$uwa[$uid]);
			if ($pieces[2] == 1 && $pieces[3] == 1 ) {
				continue;
			}
		}
		$lastname = $row['LastName'];
		$firstname = $row['FirstName'];
		$institute = $row['name'];
		if ($currinst != $institute) {
			if ($currinst != '') {
				echo "</optgroup>";
			}
			echo "<optgroup label='$institute'>\n";
			$currinst = $institute;
		}
		echo "<option value='$uid'>$lastname $firstname</option>\n";
	}
	echo "</select></p>\n";
	echo "<p><input type=submit class=button name=share value='Share'></p>\n";
	echo "</form>\n";
	echo "</div>";
	// show who has access now.
	echo "<div class=sectie>\n";
	echo "<h3>Currently Shared</h3>\n";
	echo "<h4>Classifier: $cname</h4>\n";
	echo "<p>The classifier is currently accessible by the following users. 'Full' means read-write access to classifier regions and details. 'Read-only' means no information can be changed. 'Classifier Control' means this user can edit access permissions to share the classifier with additional users.</p>\n";
	echo "You can remove their access by clicking on the garbage bin.</p>\n";
	echo "<span class=nadruk>Users with access:</span><br>\n";
	$currinst = '';
	echo "<ul id=ulsimple style='margin-left:1em;'>\n";
	foreach ($uwa as $uid => $value) {
		$pieces = explode('@@@',$value);
		$uname = $pieces[0];
		$institute = $pieces[1];
		$editing = $pieces[2];
		$sharing = $pieces[3];
		// check institution
		if ($currinst != $institute) {
			if ($currinst != '') {
				echo "</ol>";
			}
			echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
			$currinst = $institute;
		}
		// check permissions
		if ($editing == 1 ) {
			$perm = 'Full';
		}
		else {
			$perm = 'Read-only';
		}
		if ($sharing == 1 ) {
			$perm .= " / Project Control";
		}
		echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?cid=$cid&u=$uid&type=cuser&from=classification\"><img src='images/content/delete.gif' width=8px height=8px></a></li>\n";
	}
	echo "</ol></ul>\n";
	//echo "</select>\n";
	echo "</p></div>";
}
?>
