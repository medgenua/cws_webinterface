<?php

include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
$uid = $_SESSION['userID'];


## get all existing projects
$query = mysql_query("SELECT id,userID FROM project"); 
$array = array();
$instring = '';
while ($row = mysql_fetch_array($query)) {
	if ($row['id'] == '') {
		continue;
	}
	$array[] = $row['id'];	
	$instring .= $row['id'] .',';
	mysql_query("UPDATE projectpermission SET editsample = 1 WHERE projectid = '".$row['id']."' AND userid = '".$row['userID']."'");
}
$instring = substr($instring,0,-1);


$mainquery = mysql_query("SELECT idproj, id as test FROM aberration WHERE idproj NOT IN ($instring)");
$pids = array();
$nrids = 0;
while ($mainrow = mysql_fetch_array($mainquery)) {
	$pids[$mainrow['idproj']] = 1;
	$nrids++;
}

echo "nr of projects found: ". count($pids) . " and $nrids cnvs<br/>";
foreach ($pids as $pid => $dummy) {
	echo "Deleting pid $pid <br/>";
	# datapoints
	$abs = mysql_query("SELECT id FROM aberration WHERE idproj = '$pid'");
	echo " project $pid : datapoints : " . mysql_num_rows($abs) . "<br/>";
	while ($row = mysql_fetch_array($abs)) {
		$aid = $row['id'];
		$del = mysql_query("DELETE FROM datapoints WHERE id = '$aid'");

	}
	# plots (and delete the files themselve)
	$files = mysql_query("SELECT filename FROM plots WHERE idproj = '$pid'");
	echo " project $pid : plots : " . mysql_num_rows($files) . "<br/>";
	// you really need filenames from table, because joining and renaming projects can change pid for a file. 
	while (list($filename) = mysql_fetch_array($files)) {
		unlink("Whole_Genome_Plots/$filename");
	}
	$del = mysql_query("DELETE FROM plots WHERE idproj = '$pid'");
	$files = mysql_query("SELECT filename FROM BAFSEG WHERE idproj = '$pid'");
	echo " project $pid : bafseg : " . mysql_num_rows($files) . "<br/>";
	while (list($filename) = mysql_fetch_array($files)) {
		unlink("Whole_Genome_Plots/$filename");
	}
	$del = mysql_query("DELETE FROM BAFSEG WHERE idproj = '$pid'");
	# aberrations
	$del = mysql_query("DELETE FROM aberration WHERE idproj = '$pid'");
	#Prioritizations
	$prs = mysql_query("SELECT id FROM prioritize WHERE project = '$pid'");
	echo " project $pid : priorabs : " . mysql_num_rows($prs) . "<br/>";
	while ($row = mysql_fetch_array($prs)) {
		$prid = $row['id'];
		$del = mysql_query("DELETE FROM priorabs WHERE prid = '$prid'");
	}
	$del = mysql_query("DELETE FROM prioritize WHERE project = '$pid'");
	#project permissions
	$del = mysql_query("DELETE FROM projectpermission WHERE projectid = '$pid'");
	# group permissions 
	$del = mysql_query("DELETE FROM projectpermissiongroup WHERE projectid = '$pid'");
	# from UPD tables
	$sel = mysql_query("SELECT id FROM parents_upd WHERE spid = '$pid'");
	$ids = "";
	while ($row = mysql_fetch_array($sel)) {
		$ids .= $row['id'] . ",";
	}
	$ids = substr($ids,0,-1);
	$del = mysql_query("DELETE FROM parents_upd WHERE spid = '$pid'");
	$del = mysql_query("DELETE FROM parents_upd_datapoints WHERE id IN ($ids)");
	
	# project samples & samples
	$samples = mysql_query("SELECT idsamp FROM projsamp WHERE idproj = '$pid'");
	while ($row = mysql_fetch_array($samples)) {
		$sid = $row['idsamp'];
		$details = mysql_query("SELECT intrack, trackfromproject, chip_dnanr FROM sample WHERE id = '$sid'");
		$detrow = mysql_fetch_array($details);
		$intrack = $detrow['intrack'];
		$tfp = $detrow['trackfromproject'];
		$sample = $detrow['chip_dnanr'];
		if ($intrack == 1 && $tfp == $pid) { 
			$alternatives = mysql_query("SELECT ps.idproj, p.naam, p.created FROM project p JOIN projsamp ps ON ps.idproj = p.id WHERE ps.idsamp = '$sid'  AND  NOT ps.idproj = '$pid' ORDER BY ps.idproj DESC");
			if (mysql_num_rows($alternatives) > 0) {
				$formstring .= "Select an alternative for $sample :<select name=$sid>";
				$sidstring .= "$sid"."_";
				$needsupdate = 1;
				echo "$sample needs alternative : <br>";
				# delete from projsamp, and update table sample to most recent project by default,  and create form to update table if other project is wanted
				$selected = "selected";
				while ($prow  = mysql_fetch_array($alternatives)) {
					$altid =$prow['idproj'];
					$altname = $prow['naam'];
					$altcreated = $prow['created'];
					#echo " - $altid : $altname<br>";
					if ($selected != "") {
						$update = mysql_query("UPDATE sample SET trackfrompoject = '$altid' WHERE id = '$sid'");
						$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
						$formstring .="<option value='$sid-$altid' $selected>$altname ($altcreated)</option>";
						$selected = '';
					}
					else { 
						$formstring .= "<option value='$sid-$altid'>$altname ($altcreated)</option>";
					}

				}
				$formstring .= "</select><br>\n";
			}
			else {
				echo "$sample will be deleted (no other projects<br>";
				$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
				$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
				# delete from projsamp &amp; sample
			}
	
		}
		elseif ($intrack == 1) {
			# delete from projsamp ?
			$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
			continue;
			# in track from different project (assume it exists)
			
		}
		else{ 
			# not in track, check if it is included in a different project 
			$checkproj = mysql_query("SELECT idproj FROM projsamp WHERE idsamp = '$sid' AND NOT idproj = '$pid'");
			if (mysql_num_rows($checkproj) > 0) {
				echo "$sample not in track but in other projects, not deleting completely<br>";
				#only delete from projsamp
				$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
			}
			else {
				echo "$sample not in track nor other projects, deleting completely<br>";
				#delete from projsamp &amp; sample
				$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
				$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
				
			}	
		}
	}
		
	## and delete project if allowed
}

?>
