<?php

if ($loggedin != 1) {
	include('login.php');
}
else {
	$sid = $_POST['sid'];
	if ($sid == '') {
		$sid = $_GET['s'];
	}
	$pid = $_POST['pid'];
	if ($pid == '') {
		$pid = $_GET['p'];
	}
	if (isset($_POST['insert']) && $_POST['insert'] == 1) {
		$insert = 1;
	}
	else {
		$insert = 0;
	}
	$type = '';
	if (isset($_POST['type'])) {
		$type = $_POST['type'];
	}
	if ($type == '' && isset($_GET['type'])) {
		$type = $_GET['type'];
	}
	if ($sid == '' || $pid == '' ) {
		echo "<div class=sectie>\n";
		echo "<h3>Insert or Edit Clinical Data</h3>\n";
		echo "<p>There was insufficient information found to start adding clinical data (missing sample or project info)</p>\n";
		echo "</div>\n";
	}	
	elseif ($type == '' || $type == 'freetext') {
		include('inc_clinical_freetext.inc');
	}
	elseif ($type == 'HPO') {
		include('inc_clinical_LDDB.inc');
	}



}
?>
