<?php

if ($loggedin != 1) {
	include('login.php');
}
else {
	/////////////////////////
	// CONNECT TO DATABASE //
	/////////////////////////
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");

	$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';

	$defineprojects = $_POST['setprojects'] || 0;

	///////////////////////////////////////////
	// SELECT WHICH PROJECTS TO EXTRACT FROM //
	///////////////////////////////////////////
	if (isset($_POST['setprojects'])) {
		unlink("$sitedir/Reports/Table_$userid.txt");
		$fh = fopen("$sitedir/Reports/Table_$userid.txt", 'a');
		$header = "SampleID\tGender\tStructured_clinical_info\tFree_text";
		$querystring = "SELECT s.chip_dnanr, a.chr, a.start, a.stop, a.cn, p.chiptype, a.class, a.inheritance $extras FROM sample s JOIN aberration a JOIN projectpermission pp JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id WHERE pp.userid = '$userid' $instring$filter ORDER BY chr ASC, start ASC ";

		// Start output
		echo "<div class=sectie>\n";
		echo "<h3>Creating Overview file </h3>\n";
		echo "<h4>status...</h4>\n";
		echo "<ul id=ul-disc>\n";
		echo "<li> Fetching results\n";
		ob_flush();
		flush();
		$header .= "\n";
		fwrite($fh,$header);

		## Reformat projects to use
		$instring = '';
		if (isset($_POST['useprojects'])) {
			foreach($_POST['useprojects'] as $key => $pid) {
				$instring .= "$pid,";
			}
		}

		if ($instring != '') {
			$instring = " AND pp.projectid IN (".substr($instring,0,-1).") ";
			//echo "instring: $instring<br/>";
		}

		// compose query, get samples and free text
		$querystring = "SELECT DISTINCT(s.chip_dnanr), s.id, s.gender, CONVERT(DECODE(s.clinical,'$encpass') USING utf8) AS data FROM projsamp ps JOIN sample s ON ps.idsamp=s.id JOIN projectpermission pp ON pp.projectid = ps.idproj WHERE pp.userid = '$userid' $instring ORDER BY `s`.`chip_dnanr` ASC";
		$query = mysql_query($querystring);

		echo "</ul>";
		echo "<li>Printing results files</li>";

		while ($row = mysql_fetch_array($query)) {
			$sid = $row['id'];
			$sample = $row['chip_dnanr'];
			$gender = $row['gender'];
			$free = $row['data'];
			$free = strip_tags($free);
			$free = trim(preg_replace('/\s\s+/', ' ', $free));
			$free = html_entity_decode($free);

			// compose query, get phenotype codes
			$pheno = array();
			$pquery = mysql_query("SELECT ph.lddbcode FROM sample_phenotypes ph WHERE ph.sid='$sid' ORDER BY ph.sid ASC");
			while ($prow = mysql_fetch_array($pquery)) {
				$pheno[] = $prow['lddbcode'];
			}
			mysql_select_db('LNDB');
			$coded = '';
			foreach ($pheno as $key => $code) {
					$pquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
					$prow = mysql_fetch_array($pquery);
					$coded .= $prow['LABEL'].";";
			}
			mysql_select_db("$db");
			if ($coded != '') {
				$coded = substr($coded, 0, -1);
			}

			$line = "$sample\t$gender\t$coded\t$free\n";
			fwrite($fh, $line);
		}

		echo "<li>Done, providing file</li>";
		echo "</ul>";
		echo "</p><p> <a href=\"index.php?page=clinstats\">Go Back</a>\n";
		fclose($fh);
		ob_flush();
		flush();
		echo "<iframe height='0' width='0' src='download.php?path=Reports&file=Table_$userid.txt'></iframe>\n";
	}

	else {
		$query = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY id ASC");
		if (mysql_num_rows($query) == 0) {
			echo "<div class=sectie>";
			echo "<h3>Problem Detected</h3>";
			echo "<p>You don't have access to any project to export data from. </p>\n";
			echo "</div>";
		}

		else {
			echo "<div class=sectie>";
			echo "<h3>Extract Clinical information</h3>";
			echo "<h4>Select Source Projects</h4>";
			echo "<p>Select the projects you wish to extract the samples' clinical info from below.</p>";
			echo "<p><form action='index.php?page=clinstats' method=POST>";
			// hidden fields for filter settings
			echo "<input type=hidden name='setprojects' value=1>";

			// table headers
			echo "<table cellspacing=0>\n";
			echo "<tr><th colspan=5 $firstcell>Projects to Include</th></tr>\n";
			echo "<tr>\n";
			echo " <th class=topcell $firstcell>USE</th>\n";
			echo " <th class=topcell>Name</th>\n";
			echo " <th class=topcell>Chiptype</th>\n";
			echo " <th class=topcell>Created</th>\n";
			echo " <th class=topcell>Collection</th>\n";

			echo "</tr>\n";
			// entries
			while ($row = mysql_fetch_array($query)) {
				$id = $row['id'];
				$name = $row['naam'];
				$col = $row['collection'];
				$chip = $row['chiptype'];
				$created = $row['created'];
				echo "<tr><td $firstcell><input type=checkbox name=useprojects[] value=$id></td>";
				echo "<td>$name</td><td>$chip</td><td>$created</td><td>$col</td></tr>";
			}
			echo "</table>";
			echo "</p><p><input type=submit class=button value=Proceed></p>";
			echo "</form>";
		}
	}
	echo "</div>";
}

?>
