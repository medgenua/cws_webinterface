<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#######################
# CONNECT TO DATABASE #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

$pid = $_GET['p'];

$pq = mysql_query("SELECT naam FROM project WHERE id = '$pid'");
$row = mysql_fetch_array($pq);
$pname = $row['naam'];


if (isset($_POST['create'] )){
	
	$minprobes = $_POST['probes'];
	$minsize = $_POST['size'];
	$minqsnp = $_POST['qsnp'];
	$minpcnv = $_POST['pcnv'];
	$mustvice = $_POST['mustvice'];
	$mustpcnv = $_POST['mustpcnv'];
	$mustqsnp = $_POST['mustqsnp'];
	$bname = $_POST['bname'];
	$excluded = $_POST['exclude'];
	
	$types = array("CNV Bin: Min 0 to Max 0.5" , "CNV Bin: Min 0.5 to Max 1.5" , "CNV Bin: Min 1.5 to Max 2.5" , "CNV Bin: Min 2.5 to Max 3.5", "CNV Bin: Min 3.5 to Max 4.5");

	$instring = "(";
	while (list ($key,$val) = @each ($excluded)) {
		$instring .= "$val, ";
	}	 
	if ($instring != '(') {
		$instring = substr($instring,0,-2);
	}
	$instring .= ")";
	#echo "minqsnp: $minqsnp - minpcnv: $minpcnv - minvice: $minvice<br>";
	$today = date("Y/n/j - G:i:s"); 
	$filename = $bname;
	$filename = str_replace(' ','_',$filename);
	$filename = $filename . '.xml';
	$fh =fopen("$sitedir/bookmarks/$filename",'w');
	$string = "<Project_Bookmarks><Version>2.0.0</Version><Name>$pname Custom</Name><Author></Author><Comment>minsnp=$minprobes; minsize=$minsize; min.LBF=$minqsnp; min.CONF=$minpcnv</Comment><CreateDate>$today</CreateDate><Algorithm>VanillaICE</Algorithm><AlgorithmVersion></AlgorithmVersion><Bookmark_Templates><bookmark_template><type>CNV Bin: Min 0 To Max 0.5</type><fill_color>Red</fill_color><fill_style>Solid</fill_style><fill_opacity>20</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 0.5 To Max 1.5</type><fill_color>Purple</fill_color><fill_style>Solid</fill_style><fill_opacity>20</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 1.5 To Max 2.5</type><fill_color>White</fill_color><fill_style>Solid</fill_style><fill_opacity>20</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 2.5 To Max 3.5</type><fill_color>Green</fill_color><fill_style>Solid</fill_style><fill_opacity>20</fill_opacity></bookmark_template><bookmark_template><type>CNV Bin: Min 3.5 To Max 4.5</type><fill_color>Blue</fill_color><fill_style>Solid</fill_style><fill_opacity>20</fill_opacity></bookmark_template><bookmark_template><type>RECURRENT CNV</type><fill_color>Black</fill_color><fill_style>Solid</fill_style><fill_opacity>20</fill_opacity></bookmark_template></Bookmark_Templates>\n<Bookmarks>";
	fwrite($fh, "$string");
	#echo "hello world<br>";
	if ($instring == '()') {
		echo "isntring empty<br>";
		$sq = mysql_query("SELECT s.chip_dnanr, ps.idx, s.id FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE ps.idproj = '$pid'");
	}
	else {	
		echo "instring not empty<br>";
		$sq = mysql_query("SELECT s.chip_dnanr, ps.idx, s.id FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE ps.idproj = '$pid' AND NOT s.id IN $instring");
	}
	while ($row = mysql_fetch_array($sq)) {
		$sid = $row['id'];
		$sname = $row['chip_dnanr'];
		$sidx = $row['idx'];
		#echo "$sname [$sidx] (sid= $sid)<br>";
		$aq = mysql_query("SELECT id, cn, start, stop, chr, seenby, nrsnps FROM aberration WHERE sample = '$sid' AND idproj = '$pid'");
		while ($arow = mysql_fetch_array($aq)){
			$aid = $arow['id'];
			$cn = $arow['cn'];
			$start = $arow['start'];
			$stop = $arow['stop'];
			$chr = $arow['chr'];
			$chrtxt = $chromhash[$chr];
			$seenby = $arow['seenby'];
			$snps = $arow['nrsnps'];
			if ($snps < $minprobes || ($stop - $start +1) < $minsize) {
				echo "$snps less than $minprobes or ". ($stop - $start +1) ." less than $minsize<br>";
				continue;
			}
			$qsnp = preg_replace("/.*QuantiSNP \((\d+\.\d+)\).*/","$1",$seenby);
			if (!preg_match('/QuantiSNP/',$seenby)) {
				if ($mustqsnp == 1) {
					continue;
				}
				$qsnp = 0;
			}
			$pcnv = preg_replace("/.*PennCNV \((\d+\.\d+)\).*/","$1",$seenby);
			if (!preg_match('/PennCNV/',$seenby)) {
				if ($mustpcnv == 1) {
					continue;
				}	
				$pcnv = 0;
			}
			if (!preg_match('/VanillaICE/',$seenby)) {
				if ($mustvice == 1) {
					continue;
				}
				$vice = 0;
			}
			else {
				$vice = 1;
			}
				#echo "LBF: $qsnp - CONF: $pcnv - VICE: $vice<br>";
			if ( ($qsnp < $minqsnp && $mustqsnp == 1) || ($pcnv < $minpcnv && $mustpcnv == 1)) {
				echo "$seenby<br>";
				echo "$qsnp less than $minqsnp or $pcnv less than $minpcnv<br>";
				continue;
			}
			#echo "LBF: $qsnp - CONF: $pcnv - VICE: $vice<br>";
			$entry =  "<bookmark>\n";
			$entry .= "<sample_id>$sname [$sidx]</sample_id>\n";
			$entry .= "<bookmark_type>$types[$cn]</bookmark_type>\n";
			$entry .= "<entry_date></entry_date>\n";
			$entry .= "<chr_num>$chrtxt</chr_num>\n";
			$entry .= "<base_start_pos>$start</base_start_pos>\n";
			$entry .= "<base_end_pos>$stop</base_end_pos>\n";
			$entry .= "<author></author>\n";
			$entry .= "<value>$cn</value>\n";
			$entry .= "<comment>";
			$entry .= "<![CDATA[NrSNP's: $snps\n";
			$entry .= "Seen by (conf):\n";
			if ($qsnp > 0) {
				$entry .= "QuantiSNP ($qsnp)\n";
			}
			if ($pcnv > 0) {
				$entry .= "PennCNV ($pcnv)\n";
			}
			if ($vice > 0) {
				$entry .= "VanillaICE (NaN)\n";
			}
			$entry .= "]]>\n";
			$entry .= "</comment>\n";
			$entry .= "</bookmark>\n";
			fwrite($fh, $entry);
			ob_flush();
			flush();
			
		}
	}
	$endstring = "</Bookmarks>\n</Project_Bookmarks>";
	fwrite($fh,$endstring);
	fclose($fh);
	#echo "<br><br>File finished, redirecting in 10 seconds<br>";
	echo "<meta http-equiv='refresh' content='0;URL=index.php?page=create_custom_bookmark&amp;r=1&amp;p=$pid&amp;fn=$filename'>\n";
}


if ($_GET['r'] == 1) {
	$filename = $_GET['fn'];
	echo "<div class=sectie>\n";
	echo "<h3>Bookmarks Created</h3>\n";
	echo "<h4>...Click to download XML</h4>\n";
	echo "<p> You can download your file by clicking <a href='download.php?file=$filename'>here</a></p>\n";
	echo "</div>\n";
}


echo "<div class=sectie>\n";
echo "<h3>Create Bookmarks: '$pname'</h3>\n";
echo "<p>\n";
echo "<form action='index.php?page=create_custom_bookmark&amp;p=$pid' method=POST>\n";
echo "<table cellspacing=0>\n";
echo " <tr>\n";
echo "  <th $firstcell colspan=4>Bookmark Properties</th>\n";  
echo " </tr>\n";
echo " <tr >\n";
echo "  <th $thtype[0]>Minimal Nr Probes</td><td $tdtype[0]><input type=text size=25 name=probes value='3' title='Must be at least 3 probes'></td>\n";
echo "  <th $thtype[0]>Minimal Size (bp)</td><td $tdtype[0]><input type=text size=25 name=size value='0'></td>\n";
echo "  </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0]>Minimal QuantiSNP score</td><td $tdtype[0]><input type=text size=25 name=qsnp value='8' title='Must be at least 8'></td>\n";
echo "  <th $thtype[0]>Minimal PennCNV score</td><td $tdtype[0]><input type=text size=25 name=pcnv value='10' title='Must be at least 10'></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0]>Must Be Seen by QuantiSNP</td><td $tdtype[0]><input type=radio name=mustqsnp value=1> Yes / <input type=radio name=mustqsnp value=0 checked> No</td>\n";
echo "  <th $thtype[0]>Must Be Seen by PennCNV</td><td $tdtype[0]><input type=radio name=mustpcnv value=1> Yes / <input type=radio name=mustpcnv value=0 checked> No</td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0]>Must Be Seen by VanillaICE</td><td $tdtype[0]><input type=radio name=mustvice value=1> Yes / <input type=radio name=mustvice value=0 checked> No</td>\n";
echo "  <th $thtype[0]>Bookmark file name</td><td $tdtype[0]><input type=text size=25 value='$pname" . "_custom' name=bname></td>\n";
echo " </tr>\n";

echo " <tr>\n";
echo "  <th $firstcell colspan=4>Exclude Samples</th>\n";  
echo " </tr>\n";
$col = 1;
$sq = mysql_query("SELECT s.chip_dnanr, s.id FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE ps.idproj = '$pid'");
while ($row = mysql_fetch_array($sq)) {
	$sname = $row['chip_dnanr'];
	$sid = $row['id'];
	if ($col == 5) {
		echo " </tr>\n";
		echo " <tr>\n";
		$col =1;
	}
	if ($col == 1) {
		echo "  <td $firstcell>";
	}
	else {
		echo "  <td >\n";
	}
	echo "$sname <input type=checkbox name=\"exclude[]\" value=$sid></td>\n";
	$col=$col+1;
}
while ($col < 5) {
	echo "  <td></td>\n";
	$col = $col +1;
}
echo " </tr>\n";
echo "</table>\n";
echo "</p><p> <input type=submit class=button name=create value='Create Bookmarks'></p>";
echo "</form>\n";

echo "</div>\n";










}
