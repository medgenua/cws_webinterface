<div class="sectie">
	<h3>Credits</h3>
	<h4>made possible by:</h4>
	<p>The contents and functionalities of this website are created by Geert Vandeweyer, associated to the Centre for Medical Genetics, University of Antwerp, Belgium. </p>
	<p>Underlying programs are used as proposed by the authors, after optimization of parameters</p>
	
	<p>The layout is loosely based on the framework of <a href="http://www.23ste.be">this</a> site, created by <a href="http://www.emmerinc.be/">this</a> man. Many thanks for that !</p>
	<p>The system runs on SUSE Linux server edition, and multiple programs are run in a chrooted ubuntu environment.  All programming is done in perl and php.</p>
</div>
