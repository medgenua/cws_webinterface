<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

ob_end_flush();
if (!isset($_GET['step'])) {
	echo "<div class=sectie>";
	echo "<h3>Add Probe Level Data</h3>";
	echo "<p>Load raw datapoints into the database, to generate plots for the called CNV regions. The input file needs to have the following column structure:</p>";
	echo "<ol>";
	echo "<li> Probe Chromosome</li>";
	echo "<li> Probe Position</li>";
	echo "<li> Probe Information (at least one is needed), using 'SampleID.InfoType' column names:";
	echo "  <ol>";
	echo "  <li> LogR  : Probe Intensity, on log2 scale</li>";
	echo "  <li> GType : Genotype information, with AA/AB/BB/NC syntax</li>";
	echo "  <li> BAF   : B-Allele-Frequency information, scale of 0 to 1</li>";
	echo "  </ol>";
	echo "</li></ol>";
	echo "</p>";
	## print form
	$sid = $_POST['sid'];
	$pid = $_POST['pid'];
	echo "<p><form enctype='multipart/form-data' action='index.php?page=cus_adddata&step=2' method=POST>";
	echo "<table cellspacing=0>";
	echo "<tr><th colspan=3 $firstcell>Specify Data File</th></tr>";
	echo "<tr>";
  	echo "<th scope=row class=spec rowspan=2>Data File:</td>";
  	echo "<th scope=row class=spec >Specify local file:</td>";
  	echo "<td ><input type='file' name='localfile' size='40' /></td>";
	echo "</tr>";
	echo "<tr>";
  	echo "<th scope=row class=spec>Select uploaded file:</td>";
  	echo "<td ><select name='uploadedfile'><option value=''></option>";
	$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username"; // "$sitedir/uploads/";
	$Files = array();
	$It =  opendir($dir);
	if (! $It)
		 die('Cannot list files for ' . $dir);
	while ($Filename = readdir($It)) {
 		if ($Filename == '.' || $Filename == '..' || substr($Filename, 0, (strlen($userid)+1)) !== "$userid." )
  			continue;
 		//$LastModified = filemtime($dir . $Filename);
 		$Files[] = $Filename;
	}
	sort($Files);
	$num = count($Files) - 1 ;
	For($i=0;$i<=$num;$i += 1) {
		$filename = substr($Files[$i], (strlen($userid)+1));
		echo "<option value=\"". $Files[$i]."\">$filename</option>\n";
	}
 	echo "</select></td>";
	echo "</tr>";
	echo "</table></p><input type=hidden name=sid value='$sid'><input type=hidden name=pid value='$pid'><p><input type=submit class=button name=submit value='Next'></form></p>";
	echo "</div>"; 
}
elseif ($_GET['step'] == 2) {
	$target = $config['DATADIR']; //"$scriptdir/datafiles/";
	$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username"; // "$sitedir/uploads/";
 	$uploadedfile = 0;
	$ok = 1;
	$sid = $_POST['sid'];
	$pid = $_POST['pid'];
	echo "Sample: $sid<br/>Project: $pid<br/>";
	//$genderfile = basename( $_FILES['genders']['name']);
	if (basename( $_FILES['localfile']['name']) != "") {
		if ($_POST['uploadedfile'] != "") {
			echo "You can only specify one source for the data file!\n";
			$ok = 0;
		}
		echo $_FILES['localfile']['name'] . "<= used file<br/>";
		$datafile = basename( $_FILES['localfile']['name']);
 		$datafile = str_replace(' ','_',$datafile);
 		$data = $target . $datafile ;
		$data_type = $_FILES['localfile']['type'];
		echo "data: $data<br/>";
	}
 	elseif ($_POST['uploadedfile'] != "") {
		$genderfile = $_POST['uploadedfile'];
		$sdatafile = substr($datafile, (strlen($userid)+1));
		$data = $target . $sdatafile;
		$uploadedfile = 1;
		echo "Using data file from server: $datafile<br>\n";
		$data_type = "text/plain";
	 }
	 else {
		echo "You must specify a data file !<br>\n";
		$ok = 0;
 	}	
	if ($ok == 1) {
		if ($uploadedfile == 1) {
			if(copy($dir.$datafile, $data)) {
			}
			else {
				echo "Copying $datafile to $data failed<br>\n";
				$ok = 0;
			}
		}
		else {
			if(move_uploaded_file($_FILES['localfile']['tmp_name'], $data) ) {
	   		}
		 	else {
	  			$ok = 0;
	 		}
		}
	}	
	if ($ok == 1) {
		//check datafile
	  	$fh = fopen("$data","r");
  		$line = fgets($fh);
  		$line = rtrim($line);
  		$pieces = explode("\t",$line);
  		$dataok = 1;
		$nrcols = count($pieces);
		$logrcol;
		$bafcol;
		$gtcol;
		$chrcol;
		$poscol;
		$allcols = array();
  		for ($i=0;$i<$nrcols;$i++) {
			$colname = $pieces[$i];
			if ($colname == 'Chr') {
				$chrcol = $i + 1;
			}
			elseif ($colname == 'Position') {
				$poscol = $i + 1;
			}
			elseif ($colname == $sid.'.LogR') {
				$logrcol = $i + 1;
			}
			elseif ($colname == $sid.'.GType') {
				$gtcol = $i + 1;
			}
			elseif ($colname == $sid.'.BAF') {
				$bafcol = $i + 1;
			}
			$allcols[($i + 1)] = $colname;
		}
		if ($chrcol == '' || $poscol == '' || $logrcol == '' || $bafcol == '' || $gtcol == '') {
			# needed columns not found. print all with selection box.
			echo "<div class=sectie>";
			echo "<h3>Not all possible column headers were not found.</h3>";
			echo "<p>Please specify the columns that contain LogR/GType/B-Allele information, and probe position. If at least one of the three information types is specified here, the data will be accepted.</p>";
			echo "<form action='index.php?page=cus_adddata&step=3' method=POST>";
			echo "<p><table cellspacing=0>";
			echo "<tr><th $firstcell>Column Idx</th><th>Header</th><th>Usage</th></tr>";
			$options = array("Chr" => "Chromosome", "Position" => "Position", "LogR" => "Log(Intensity)", "BAF" => "B-Allele Frequency",  "GType" => "GenoType");
			foreach ($allcols as $idx => $name) {
				$dropdown = "<select name='col_$idx'>";
				$selected = 0;
				
				foreach ($options as $short => $long) {
					$sel = '';
					switch($short) {
						case 'Chr':
							if (preg_match('/chr/i',$name)) {
								$selected = 1;
								$sel = 'SELECTED';
							}
							break;
						case 'Position':
							if (preg_match('/position|location/i',$name)) {
								$selected = 1;
								$sel = 'SELECTED';
							}
							break;
						case 'LogR':
							if (preg_match("/$sid.{0,2}(logr|log.{1}r|intensity)/i",$name)) {
								$selected = 1;
								$sel = 'SELECTED';
							}
							break;
						case 'BAF':
							if (preg_match("/$sid.{0,2}(baf|B.Allele)/i",$name)) {
								$selected = 1;
								$sel = 'SELECTED';
							}
							break;
						case 'GType':
							if (preg_match("/$sid.{0,2}(gt|genotype)/i",$name)) {
								$selected = 1;
								$sel = 'SELECTED';
							}
							break;
					}
					
					$dropdown .= "<option value='$short' $sel>$long</option>";
				}
				$sel = '';
				if ($selected == 0) {
					$sel = 'SELECTED';
				}
				$dropdown .= "<option value='discard' $sel>Discard</option></select>";
				echo "<tr><td $firstcell>$idx</td><td>$name</td><td>$dropdown</td></tr>";
				
			}
			echo "</table>";
			# recheck if all is set
			echo "</p><p>";
			echo "<input type=hidden name=sid value='$sid'><input type=hidden name=pid value='$pid'>";
			echo "<input type=hidden name=filename value='$data'>";
			echo "<input type=hidden name=nrcols value='$nrcols'>";
			echo "<input type=submit class=button name=submit value='ReCheck'></p></form>";

			echo "</div>";
		}
		else {
			# file ok, to scanning
			echo "<div class=sectie>";
			echo "<h3>Checking Input file</h3>";
			echo "<p>All seems to be ok. Click 'next' to continue.</p>";
			echo "<p><form action='index.php?page=cus_adddata&step=4' method=POST>";
			# set sample & project
			echo "<input type=hidden name=sid value='$sid'><input type=hidden name=pid value='$pid'>";
			# set columns
			echo "<input type=hidden name=chrcol value='$chrcol'>";
			echo "<input type=hidden name=poscol value='$poscol'>";
			echo "<input type=hidden name=logrcol value='$logrcol'>";
			echo "<input type=hidden name=bafcol value='$bafcol'>";
			echo "<input type=hidden name=gtcol value='$gtcol'>";
			echo "<input type=hidden name=filename value='$data'>";
			echo "<input type=hidden name=nrcols value='$nrcolumns'>";
			echo "<input type=submit class=button name=submit value='Next'></form></p></div>";
			
		}
  	}   
}
elseif ($_GET['step'] == 3) {
	# get data
	$sid = $_POST['sid'];
	$pid = $_POST['pid'];
	$data = $_POST['filename'];
	echo "Sample: $sid<br/>Project: $pid<br/>Data: $data<br/>";
	$nrcolumns = $_POST['nrcols'];
	$chrcol;
	$poscol;
	$logrcol;
	$bafcol;
	$gtcol;
	for ($i = 1; $i<=$nrcolumns; $i++) {
		switch($_POST["col_$i"]) {
			case 'Discard':
				break;
			case 'GType':
				$gtcol = $i;
				break;
			case 'BAF':
				$bafcol = $i;
				break;
			case 'LogR':
				$logrcol = $i;
				break;
			case 'Chr': 
				$chrcol = $i;
				break;
			case 'Position':
				$poscol = $i;
				break;
		}
	}
	if ($chrcol == '' || $poscol == '' || ($logrcol == '' && $bafcol == '' && $gtcol == '')) {
		# still not ok.
		echo "<div class=sectie>";
		echo "<h3>Missing Columns.</h3>";
		echo "<p>Please specify at least on of the columns that contain LogR/GType/B-Allele information, and both probe position and chromosome columns. </p>";
		echo "<form action='index.php?page=cus_adddata&step=3' method=POST>";
		echo "<p><table cellspacing=0>";
		echo "<tr><th $firstcell>Column Idx</th><th>Header</th><th>Usage</th></tr>";
		$options = array("Chr" => "Chromosome", "Position" => "Position", "LogR" => "Log(Intensity)", "BAF" => "B-Allele Frequency",  "GType" => "GenoType");
		# rescan headerline.
		$fh = fopen("$data","r");
  		$line = fgets($fh);
  		$line = rtrim($line);
  		$pieces = explode("\t",$line);
		$allcols = array();
		for ($i=0;$i<$nrcolumns;$i++) {
			$colname = $pieces[$i];
			$allcols[($i + 1)] = $colname;
		}

		foreach ($allcols as $idx => $name) {
			$dropdown = "<select name='col_$idx'>";
			$selected = 0;
			
			foreach ($options as $short => $long) {
				$sel = '';
				switch($short) {
					case 'Chr':
						if (preg_match('/chr/i',$name)) {
							$selected = 1;
							$sel = 'SELECTED';
						}
						break;
					case 'Position':
						if (preg_match('/position|location/i',$name)) {
							$selected = 1;
							$sel = 'SELECTED';
						}
						break;
					case 'LogR':
						if (preg_match("/$sid.{0,2}(logr|log.{1}r|intensity)/i",$name)) {
							$selected = 1;
							$sel = 'SELECTED';
						}
						break;
					case 'BAF':
						if (preg_match("/$sid.{0,2}(baf|B.Allele)/i",$name)) {
							$selected = 1;
							$sel = 'SELECTED';
						}
						break;
					case 'GType':
						if (preg_match("/$sid.{0,2}(gt|genotype)/i",$name)) {
							$selected = 1;
							$sel = 'SELECTED';
						}
						break;
				}
				
				$dropdown .= "<option value='$short' $sel>$long</option>";
			}
			$sel = '';
			if ($selected == 0) {
				$sel = 'SELECTED';
			}
			$dropdown .= "<option value='discard' $sel>Discard</option></select>";
			echo "<tr><td $firstcell>$idx</td><td>$name</td><td>$dropdown</td></tr>";
			
		}
		echo "</table>";
		# recheck if all is set
		echo "</p><p>";
		echo "<input type=hidden name=sid value='$sid'><input type=hidden name=pid value='$pid'>";
		echo "<input type=hidden name=filename value='$data'>";
		echo "<input type=hidden name=nrcols value='$nrcols'>";
		echo "<input type=submit class=button name=submit value='ReCheck'></p></form>";

		echo "</div>";

	}
	else {
		# columns found. go to step 4
		# file ok, to scanning
			echo "<div class=sectie>";
			echo "<h3>Checking Input file</h3>";
			echo "<p>All seems to be ok now. Click 'next' to continue.</p>";
			echo "<p><form action='index.php?page=cus_adddata&step=4' method=POST>";
			# set sample & project
			echo "<input type=hidden name=sid value='$sid'><input type=hidden name=pid value='$pid'>";
			# set columns
			echo "<input type=hidden name=chrcol value='$chrcol'>";
			echo "<input type=hidden name=poscol value='$poscol'>";
			echo "<input type=hidden name=logrcol value='$logrcol'>";
			echo "<input type=hidden name=bafcol value='$bafcol'>";
			echo "<input type=hidden name=gtcol value='$gtcol'>";
			echo "<input type=hidden name=filename value='$data'>";
			echo "<input type=hidden name=nrcols value='$nrcolumns'>";
			echo "<input type=submit class=button name=submit value='Next'></form></p></div>";

	}

}
elseif ($_GET['step'] == 4) {
	# data
	$sid = $_POST['sid'];
	$pid = $_POST['pid'];
	$chrcol = $_POST['chrcol'];
	$poscol = $_POST['poscol'];
	$logrcol = $_POST['logrcol'];
	$bafcol = $_POST['bafcol'];
	$gtcol = $_POST['gtcol'];
	$data = $_POST['filename'];
	## set datapoints field structure
	$structure = "1";
	$storecols = array();
	if ($logrcol != '') {
		$structure .= "1";
		$storecols[] = $logrcol - 1;
	}
	else {
		$structure .= "0";
	}
	if ($gtcol != '') {
		$structure .= "1";
		$storecols[] = $gtcol - 1;
	}
	else {
		$structure .= "0";
	}
	if ($bafcol != '') {
		$structure .= "1";
		$storecols[] = $bafcol - 1;
	}
	else {
		$structure .= "0";
	}
	$poscol--;
	$chrcol--;
	echo "<div class=sectie>";
	echo "<h3>Loading Data to Database</h3>";
	echo "<p>";
	echo "<ul>";
	## load CNVS from db
	echo "<li> - Fetching CNV regions from database</li>";
	flush();
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db($db);
	$query = mysql_query("SELECT chr, start, stop, id FROM cus_aberration WHERE sample = '$sid' AND idproj = '$pid'");
	$cnvs = array();
	while ($row = mysql_fetch_array($query)) {
		$chr = $row['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $row['start'];
		$stop = $row['stop'];
		$aid = $row['id'];
		#if ($aid == '') {
		#	echo "empty aid : $chr:$start-$stop<br/>";
		#}
		$idx = count($cnvs[$chrtxt]) + 1;
		$cnvs[$chrtxt][$idx]['aid'] = $aid;
		$idx = count($cnvs[$chrtxt]); 
		$cnvs[$chrtxt][$idx]['start'] = $start;
		$cnvs[$chrtxt][$idx]['stop'] = $stop;
	}
	## SCAN FILE
	echo "<li> - Scanning File</li>";
	flush();
	$fh = fopen("$data","r");
	$tostore = array();
	while(!feof($fh)) {
		$line = fgets($fh) ;
  		$line = rtrim($line);
  		$pieces = explode("\t",$line);
		$pos = $pieces[$poscol];
		$chrtxt = $pieces[$chrcol];
		if ($chrtxt == 'XY') {
			$chrtxt = 'X';
		} 
		foreach ($cnvs[$chrtxt] as $idx => $array) {
			if ($pos >= ($cnvs[$chrtxt][$idx]['start'] - 250000) && $pos <= ($cnvs[$chrtxt][$idx]['stop'] + 250000)) {
				# keep this probe
				$aid = $cnvs[$chrtxt][$idx]['aid'];
				#if ($aid == '') {
				#	echo "empty aid : $chr:$pos (".$cnvs[$chr][$idx]['start'].") <br/>";
				#}
			
				$tostore[$aid] = $tostore[$aid] . $pos . "_";
				foreach ($storecols as $key => $colidx ) { 
					$tostore[$aid] .= $pieces[$colidx] . "_";
				}
			#break;
			}
		}
		
	}
	echo "<li> - Storing Results</li>";
	flush();
	foreach ($tostore as $aid => $content) {
		#echo "store $aid<br/>";
		if ($content == '') {
			continue;
		}
		else {
			$content = substr($content,0,-1); # remove trailing underscore
		}
		#$query = "REPLACE INTO cus_datapoints (id, content, structure) VALUES ('$aid', '$content', '$structure')";
		#echo "$query<br/>";
		mysql_query("REPLACE INTO cus_datapoints (id, content, structure) VALUES ('$aid', '$content', '$structure')");
	}
	echo "<li>- FINISHED</li>";
	echo "</ul>";
	echo "</p><p><a href='index.php?page=cusdetails&sample=$sid&project=$pid'>Go Back To Sample</a></p>";
	echo "</div>";	
}	
