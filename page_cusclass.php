<! -- LOAD page specific javascript functions --> 
<script type="text/javascript" src="javascripts/class.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>
<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

// chromhash
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
// table layout
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// inheritance array
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');


// connect to database
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");


// get variables
$sid = $_GET['sid'];
$pid = $_GET['pid'];

$query = mysql_query("SELECT ct.ID FROM chiptypes ct JOIN project p ON p.chiptype = ct.name WHERE p.id = '$pid'");
$row = mysql_fetch_array($query);
$chiptypeid = $row['ID'];

$query = mysql_query("SELECT chip_dnanr FROM sample where id = '$sid'");
$row = mysql_fetch_array($query);
$sample = $row['chip_dnanr'];

$query = mysql_query("SELECT naam FROM project WHERE id = '$pid'");
$row = mysql_fetch_array($query);
$project = $row['naam'];

// process input
if (isset($_POST['SetClass']) || isset($_POST['InsertNew'])) {
	// INSERT New CNV
	if ($_POST['chr'] != '' && $_POST['start'] != '' && $_POST['stop'] != '' && $_POST['CN'] != '' && $_POST['arguments'] != '') {
		$inchrtxt = $_POST['chr'];
		$inchr = $chromhash[$inchrtxt];
		$instart = $_POST['start'];
		$instop = $_POST['stop'];
		$incn = $_POST['CN'];
		$arguments = $_POST['arguments'];
		$insize = $instop - $instart +1;
		$geneq = mysql_query("SELECT COUNT(ID) as aantal FROM genesum WHERE chr = '$inchr' AND ((start BETWEEN $instart AND $instop ) OR (stop BETWEEN $instart AND $instop) OR (start <= $instart AND stop >= $instop))");
		$generow = mysql_fetch_array($geneq);
		$innrgenes = $generow['aantal']; 
		$snpq = mysql_query("SELECT COUNT(ID) as aantal FROM probelocations WHERE chromosome = '$inchr' AND chiptype = '$chiptypeid' AND position BETWEEN $instart AND $instop");
		$snprow = mysql_fetch_array($snpq);
		$insnps = $snprow['aantal'];
		# FIND next 5' probe
		$smaller = "SELECT position FROM probelocations WHERE chromosome = '$inchr' AND chiptype = $chiptypeid AND position < $instart ORDER BY position DESC LIMIT 1";
		$smallq = mysql_query($smaller);
		$smallrows = mysql_num_rows($smallq);
		
		if ($smallrows > 0) {
			$smallrow = mysql_fetch_array($smallq);
			$largestart = $smallrow['position'];
		}
		else {
			$largestart = 'ter';
		}
		# FIND next 3' probe
		$larger = "SELECT position FROM probelocations WHERE chromosome = '$inchr' AND chiptype = $chiptypeid AND position > $instop ORDER BY position ASC LIMIT 1";
		$largeq = mysql_query($larger);
		$largerows = mysql_num_rows($largeq);
		if ($largerows > 0) {
			$largerow = mysql_fetch_array($largeq);	
			$largestop = $largerow['position'];
		}
		else {
			$largestop = 'ter';
		}

		mysql_query("INSERT INTO aberration (chr, start, stop, cn, sample, idproj, nrsnps, nrgenes, size, largestart, largestop, inheritance) VALUES ('$inchr', '$instart', '$instop', '$incn', '$sid', '$pid', '$insnps', '$innrgenes', '$insize', '$largestart', '$largestop', '')");
		$insaid = mysql_insert_id();
		$logentry = "Manually added";
		mysql_query("INSERT INTO log (sid, pid, aid, uid, entry,arguments) VALUES ('$sid', '$pid', '$insaid','$userid', '$logentry','$arguments')");
	}
	// SET class & inheritance
	$toconfirm = '';
	$query = mysql_query("SELECT id, class, chr, start, stop, inheritance FROM aberration WHERE sample = '$sid' AND idproj = '$pid' ");
	while ($row = mysql_fetch_array($query)){
		$aid = $row['id'];
		$origclass = $row['class'];
		$setchr = $row['chr'];
		$setchrtxt = $chromhash[$setchr];
		$setstart = $row['start'];
		$setstop = $row['stop'];		
		$previnh = $row['inheritance'];
		// new posted class 
		$class = $_POST["class_$aid"];
		if ($class != $origclass && $class != 'null' ) {
			if ($origclass == '') {
				$logentry = "Diagnostic Class Set to $class";
				mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
				$update = mysql_query("UPDATE aberration SET class=$class WHERE id = '$aid'");
			}
			else {
				// need confirmation 
				$toconfirm .= "$aid-$origclass-$class,";
				#$logentry = "Diagnostic Class Changed From $origclass to $class";
				#mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
			}
		}
		if ($origclass > 0 && $class == 'null') {
			# resetting annotation, needs confirmation
			$toconfirm .= "$aid-$origclass-$class,";
			#$logentry = "Diagnostic Class Changed From $origclass to Undefined";
			#mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
		}
		$inheritance = $_POST['inheritance'][$aid];
		if ($inheritance != $previnh) {
				echo "Chr$setchrtxt:$setstart-$setstop: inheritance $inheritance vs previnh $previnh:<br/>";
				$logentry = "Inheritance Changed From ". $inh[$inh[$previnh]]." to ". $inh[$inh[$inheritance]];
				mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
				$update = mysql_query("UPDATE aberration SET inheritance=$inheritance WHERE id = '$aid'");
		}
		#$update = mysql_query("UPDATE aberration SET class=$class, inheritance=$inheritance WHERE id = '$aid'");
	}
	if ($toconfirm != '') {
		$toconfirm = substr($toconfirm,0,-1);
		echo "<script type='text/javascript'>popitup('changemultipleclasses.php?tc=$toconfirm&u=$userid&s=$sid&p=$pid&');</script>";
	}
	// merge
	$merge=$_POST['merge'];
	$firstaid = $merge[0];
	$query = mysql_query("Select chr, start, stop, cn FROM aberration WHERE id='$firstaid'");
	$row = mysql_fetch_array($query);
	$chr = $row['chr'];
	$firststart = $row['start'];
	$laststop = $row['stop'];
	$cn = $row['cn'];
	$continue = 1;
	if ($merge[0] == '') {
		$continue = 0;
	}
	$qsum = 0;
	$psum = 0;
	$qnumber = 0;
	$pnumber = 0;
	$vice = 0;
	$fullcontent = '';
	foreach ($merge as $key => $aid) {
		$query = mysql_query("Select a.chr, a.start, a.stop, a.cn, a.seenby, d.content FROM aberration a LEFT JOIN datapoints d ON a.id = d.id WHERE a.id='$aid'");
		$row = mysql_fetch_array($query);
		$cchr = $row['chr'];
		$cstart = $row['start'];
		$cstop = $row['stop'];
		$ccn = $row['cn'];
		$seenby = $row['seenby'];
		$content = $row['content'];
		if ($ccn != $cn || $cchr != $chr) {
			//echo "current cn : $ccn ; current chr: $cchr<br>";
			$output = "<div class=sectie>\n<h3>You cannot merge CNV's from different chromosomes or copy number states !</h3>\n<p>Please try again with a correct selection</p></div>\n";
			$continue = 0;
			break;
		}
		if ($cstart < $firststart) {
			$firststart = $cstart;
		}
		if ($cstop > $laststop) {
			$laststop = $cstop;
		}
		if (preg_match('/VanillaICE/',$seenby)) {
			$vice = 1;
		}
		if (preg_match('/QuantiSNP\s\((\S+)\)\s-/',$seenby,$match)) {
			$score = preg_replace("/QuantiSNP\s\((\S+)\)\s-/","$1",$match[0]);	
			$qsum = $qsum + $score;
			$qnumber++;
		}
		if (preg_match('/PennCNV\s\((\S+)\)\s-/',$seenby,$match)) {
			$score = preg_replace("/PennCNV\s\((\S+)\)\s-/","$1",$match[0]);
			$psum = $psum + $score;
			$pnumber++;
		}
		$fullcontent .= $content;
		
	} 
	if ($continue == 1) {
		//find number of snps
		$snpquery = mysql_query("SELECT Count(name) as nrsnps FROM probelocations WHERE chiptype='$chiptypeid' AND chromosome='$chr' AND  position>='$firststart' AND position<='$laststop'");
		$snprow = mysql_fetch_array($snpquery);
		$snps = $snprow['nrsnps'];
		//find number of genes
		$genquery = mysql_query("SELECT Count(DISTINCT symbol) as nrgenes FROM genes WHERE chr='$chr' AND (((start BETWEEN $firststart AND $laststop) OR (end BETWEEN $firststart AND $laststop)) OR (start <= $firststart AND end >= $laststop))");
		$genrow = mysql_fetch_array($genquery);
		$nrgenes = $genrow['nrgenes'];
		// GET SIZE
		$size = $laststop - $firststart +1;
		$seenbyavg = "";
		if ($qnumber > 0) {
			$qavg = round(($qsum / $qnumber),2);
			$seenbyavg = $seenbyavg . "- QuantiSNP ($qavg) ";
		}
		if ($pnumber > 0) {
			$pavg = round(($psum / $pnumber),2);
			$seenbyavg = $seenbyavg . "- PennCNV ($pavg) ";
		}
		if ($vice == 1) {
			$seenbyavg = $seenbyavg . "- VanillaICE (NaN)";
		}
		# FIND next 5' probe
		$smaller = "SELECT position FROM probelocations WHERE chromosome = '$chr' AND chiptype = $chiptypeid AND position < $firststart ORDER BY position DESC LIMIT 1";
		$smallq = mysql_query($smaller);
		$smallrows = mysql_num_rows($smallq);
		
		if ($smallrows > 0) {
			$smallrow = mysql_fetch_array($smallq);
			$largestart = $smallrow['position'];
		}
		else {
			$largestart = 'ter';
		}
		# FIND next 3' probe
		$larger = "SELECT position FROM probelocations WHERE chromosome = '$chr' AND chiptype = $chiptypeid AND position > $laststop ORDER BY position ASC LIMIT 1";
		$largeq = mysql_query($larger);
		$largerows = mysql_num_rows($largeq);
		if ($largerows > 0) {
			$largerow = mysql_fetch_array($largeq);	
			$largestop = $largerow['position'];
		}
		else {
			$largestop = 'ter';
		}

		//echo "chr$chr:$firststart-$laststop : $cn : $seenbyavg (chiptype: $chiptypeid), snps: $snps ; genes: $nrgenes<br>";
		$query = mysql_query("INSERT INTO aberration (chr, start, stop, cn, sample, idproj, nrsnps, nrgenes, seenby, largestart, largestop, size ) VALUES ('$chr', '$firststart', '$laststop', '$cn', '$sid', '$pid', '$snps', '$nrgenes', '$seenbyavg', '$largestart', '$largestop', '$size')");
		$chrtxt = $chromhash[$chr];
		$insid = mysql_insert_id();
		$query = mysql_query("INSERT INTO datapoints (id, content) VALUES ('$insid', '$fullcontent')");
		$logentry = "Added as merging result";
		mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$insid','$userid', '$logentry')");
		foreach ($merge as $key => $aid) {
			if ($aid != '') {
				$subq = mysql_query("SELECT id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby FROM aberration WHERE id = $aid");
				$subr = mysql_fetch_array($subq);
				$delid = $subr['id'];
				$delcn = $subr['cn'];
				$delstart = $subr['start'];
				$delstop = $subr['stop'];
				$delchr = $subr['chr'];
				$delchrtxt = $chromhash[$delchr];
				$delsid = $subr['sample'];
				$delpid = $subr['idproj'];
				$delinh = $subr['inheritance'];
				$delclass = $subr['class'];
				$delsb = $subr['seenby'];
				mysql_query("INSERT INTO deletedcnvs (id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby) VALUES ('$delid','$delcn','$delstart','$delstop','$delchr','$delsid','$delpid','$delinh','$delclass','$delsb')");
				$logentry = "Removed after merging";
				mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$delsid', '$delpid', '$aid','$userid', '$logentry')");
				$query = mysql_query("DELETE FROM aberration WHERE id = '$aid'");
			}
		}
				
		//update occurence
		//delete seperate regions
	}
	else {
		echo $output;
	}
	// Delete
	#$delete = $_POST['delete'];
	#foreach ($delete as $key => $aid) {
		#echo "<div class=sectie><h3>Deleting of CNV's not supported yet!</h3></div>";
		
	#	break;
	#}
} 

// output
echo "<div class=sectie>\n";
echo "<h3>Define CNV properties for sample '$sample'</h3>\n";
echo "<h4>Observed in project '$project'</h4>\n";
echo "<p>";
echo "<a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\">Go Back to the sample details</a></p>\n";
echo "<p>\n";

// input form
echo "<form action=\"index.php?page=class&amp;sid=$sid&amp;pid=$pid\" method=POST>\n";
echo "<table cellspacing=0>\n";
$switch=0;
echo " <tr>\n"; 
echo "  <th scope=col class=topcellalt $firstcell>Location</th>\n";
echo "  <th scope=col class=topcellalt>Size</th>\n";
echo "  <th scope=col class=topcellalt>CN</th>\n";
echo "  <th scope=col class=topcellalt>Class</th>\n";
echo "  <th scope=col class=topcellalt>Inheritance</th>\n";
echo "  <th scope=col class=topcellalt>Merge</th>\n";
echo "  <th scope=col class=topcellalt>Delete</th>\n"; 
echo " </tr>\n";

$query = mysql_query("SELECT id, chr, start, stop, cn, class, inheritance, nrsnps FROM aberration WHERE sample = '$sid' AND idproj = '$pid' ORDER BY chr, start"); 
while ($row = mysql_fetch_array($query)) {
	$inheritance = $row['inheritance'];
	$class = $row['class'];
	$aid = $row['id'];
	$cn = $row['cn'];
	$start = $row['start'];
	$stop = $row['stop'];
	$csize = $stop - $start + 1;
	$nrsnps = $row['nrsnps'];
	if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] || $nrsnps < $_SESSION['LOHminsnp'] )) {
		echo "<input type=hidden name='inheritance[$aid]' value=$inheritance>";
		continue;
	}

	echo " <tr>\n";
	echo "  <td $tdtype[$switch] $firstcell>chr". $chromhash[ $row['chr'] ].":" . number_format($row['start'],0,'',','). "-" . number_format($row['stop'],0,'',',') . "</td>\n";
	echo "  <td $tdtype[$switch] >".number_format(($row['stop']-$row['start']+1),0,'',',')."</td>\n";
	echo "  <td $tdtype[$switch] onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event),EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\">".$row['cn']."</td>\n";
	echo "  <td $tdtype[$switch] ><select name='class_$aid'>";
	echo "<option value=null> </option>\n"; 
	for ($i = 1;$i<=4;$i++) {
		if ($i == $class) {
			$selected = 'selected';
		}
		else {
			$selected = '';
		}
		echo "  <option $selected value=$i>$i</option>\n";
	}
	
	echo "</select></td>\n";
	echo "  <td $tdtype[$switch] ><select name='inheritance[$aid]' >";
	for ($i = 0;$i<=3;$i++){
		if ($i == $inheritance) {
			$selected = 'selected';
		}
		else {
			$selected = '';
		}
		echo "<option $selected value=$i>".$inh[$inh[$i]]."</option>\n";
	}
	echo " </select></td>\n";
	echo "  <td $tdtype[$switch] ><input type=checkbox name=merge[] value=$aid></td>\n";
	echo "  <td $tdtype[$switch] ><a class=img href=\"javascript:void(null)\" onclick=\"return popitup('deletecnv.php?aid=$aid&u=$userid&ftt=0')\"><img src='images/content/delete.gif' width=12px height=12px></a></td>\n";
	echo " </tr>\n";
	$switch = $switch + pow(-1,$switch);
}
echo "</table>\n</p>";
echo "<p><input type=submit class=button value='Submit' name=SetClass></p>\n";
//echo "</form>\n";

echo "</div>\n";

echo "<div class=sectie>\n";
echo "<h3>Insert Extra Regions</h3>\n";
echo "<p>In case you have evidence for a false negative result, you can insert it here. Inserted regions will be listed above for further classification. All fields are mandatory!</p>\n";
//echo "<p><form action=\"index.php?page=class&amp;sid=$sid&amp;pid=$pid\" method=POST>\n";
echo "<table cellspacing=0>\n";
echo " <tr>\n"; 
echo "  <th scope=col class=topcellalt $firstcell>Chr</th>\n";
echo "  <th scope=col class=topcellalt>Start</th>\n";
echo "  <th scope=col class=topcellalt>Stop</th>\n";
echo "  <th scope=col class=topcellalt>Copy Number</th>\n";
echo "  <th scope=col class=topcellalt>Arguments</th>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "<td $firstcell><input type=text name=chr size=3 /></td>\n";
echo "<td ><input type=text name=start size=10 /></td>\n";
echo "<td ><input type=text name=stop size=10 /></td>\n";
echo "<td ><input type=text name=CN size=3/></td>\n";
echo "<td ><input type=text name=arguments size=35 maxlength=255></td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</p>\n";
echo "<p><input type=submit class=button value=Insert name=InsertNew>\n";
echo "</form>\n";
echo "</div>\n";

mysql_select_db($db);
echo "<div class=sectie>\n";
echo "<p>\n";
echo "<span class=nadruk>History:</span>\n";
echo "</p>\n";
echo "<p><ul id=ul-log>\n";
$history = mysql_query("SELECT l.aid, l.time, u.FirstName, u.LastName, l.entry, l.arguments FROM log l JOIN users u ON u.id = l.uid WHERE l.sid = '$sid' AND l.pid = '$pid' ORDER BY l.time DESC LIMIT 15");
$rows = mysql_num_rows($history);
while ($row = mysql_fetch_array($history)) {
	$aid = $row['aid'];
	$sq = mysql_query("SELECT chr, start, stop, cn FROM aberration WHERE id = '$aid'");
	$srows = mysql_num_rows($sq);
	if ($srows > 0) {
		$srow = mysql_fetch_array($sq);
		$chr = $srow['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $srow['start'];
		$starttxt = number_format($start,0,'',',');
		$stop = $srow['stop'];
		$stoptxt = number_format($stop,0,'',',');
		$cn = $srow['cn'];
	}
	else {
		$ssq = mysql_query("SELECT chr, start, stop, cn FROM deletedcnvs WHERE id = '$aid'");
		$srow = mysql_fetch_array($ssq);
		$chr = $srow['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $srow['start'];
		$starttxt = number_format($start,0,'',',');
		$stop = $srow['stop'];
		$stoptxt = number_format($stop,0,'',',');
		$cn = $srow['cn'];
	}
	$time = $row['time'];
	$FName = $row['FirstName'];
	$LName = $row['LastName'];
	$logentry = $row['entry'];
	$arguments = $row['arguments'];
	echo "<li>$time : Chr$chrtxt:$starttxt-$stoptxt (cn:$cn) : $logentry by $FName $LName";
	if ($arguments != '') {
		echo " : $arguments</li>\n";
	}
	else {
		echo "</li>\n";
	}
}
echo "</ul>\n";	
echo "</div>\n";
	

}
?>
<div id="txtHint">something must appear here</div>

