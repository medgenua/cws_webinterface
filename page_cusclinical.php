<?php

if ($loggedin != 1) {
	include('login.php');
}
else {
	$sid = $_POST['sid'];
	if ($sid == '') {
		$sid = $_GET['s'];
	}
	$pid = $_POST['pid'];
	if ($pid == '') {
		$pid = $_GET['p'];
	}
	if ($_POST['insert'] == 1) {
		$insert = 1;
	}
	else {
		$insert = 0;
	}
	$type = $_POST['type'];
	if ($type == '') {
		$type = $_GET['type'];
	}
	if ($sid == '' || $pid == '' ) {
		echo "<div class=sectie>\n";
		echo "<h3>Insert or Edit Clinical Data</h3>\n";
		echo "<p>There was insufficient information found to start adding clinical data (missing sample or project info)</p>\n";
		echo "</div>\n";
	}	
	elseif ($type == '' || $type == 'freetext') {
		include('inc_cusclinical_freetext.inc');
	}
	elseif ($type == 'HPO') {
		include('inc_cusclinical_LDDB.inc');
	}



}
?>
