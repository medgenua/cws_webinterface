<div id="txtHint"></div>
<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
// $dc = array(0=>'',1=>'Path',2=>'Path',3=>'UV',4=>'Ben',5=>'FP',6=>'Path_Rec',''=>'','Path'=>'Pathogenic','UV'=>'Unknown Significance','Ben'=>'Benign','FP'=>'False Positive','Path_Rec'=>'Recessive Pathogenic');
$dc = array();
$dc_query = mysql_query("SELECT id, name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
}

$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
$primers = '';
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
###################
# SORTING OPTIONS #
###################
if (isset($_POST['sort'] )) {
	$sort = $_POST['sort'];
}
else { 
	$sort = 'position';
}
$sortoptions = array('size' => 'nrgenes desc, (stop - start) DESC', 'position' => 'chr ASC, start ASC', 'rank' => 'rank ASC, chr ASC, start ASC');

########################
# GET POSTED VARIABLES #
########################
if (isset($_POST['sample'] ) ) {
	$sample = $_POST['sample'];
}
elseif (isset($_GET['sample'] ) ) {
	$sample = $_GET['sample'];
}
if (isset($_POST['project'] ) ) {
	$project = $_POST['project'];
}
elseif (isset($_GET['project'] ) ) {
	$project = $_GET['project'];
}

#################################
# GET Sample/project properties #
#################################
$samplename = $sample;

// LOAD FILTERS //
//////////////////
$exp = array("IlluminaProjectSamples","CustomProjectSamples","IlluminaSearchSamples","CustomSearchSamples","Healthy");
$query = mysql_query("SELECT f.Name, uf.fid,  uf.Type, uf.ApplyTo,fr.chr,fr.start, fr.stop,fr.rid,fr.Comment FROM `Users_x_Filters` uf JOIN `Filters_x_Regions` fr JOIN `Filters` f ON f.fid = fr.fid AND fr.fid = uf.fid WHERE uf.uid = '$userid' AND uf.Type != 0 ORDER BY Type ASC");
$filter = array("1" => array(), "2" => array(), "3" => array());
while ($row = mysql_fetch_array($query)) {
	$fchr = $row['chr'];
	// create chromosome entry in filter hash
	if (!isset($filter[$row['Type']][$fchr])) {
		$filter[$row['Type']][$fchr] = array();
	}

	// skip filters that are not applied to illumina-project samples (browsing details implies this.)
	if ($row['ApplyTo'] != 'Experimental') {
		$at = explode(";",$row['ApplyTo']);
		if (!in_array("IlluminaProjectSamples")) {
			continue;
		}
	}
	// first time start pos on this chrom is seen
	if (!isset($filter[$row['Type']][$fchr][$row['start']])) {
		$filter[$row['Type']][$fchr][$row['start']] = array();		
		$filter[$row['Type']][$fchr][$row['start']]['stop'] = $row['stop'];
		$filter[$row['Type']][$fchr][$row['start']]['comment'] = $row['Comment'];
		$filter[$row['Type']][$fchr][$row['start']]['Name'] = $row['Name'];	
	}
	// else... extend if newend > end ; concat names and comments.
	else {
		if ($row['stop'] > $filter[$row['Type']][$fchr][$row['start']]['stop']) {
			$filter[$row['Type']][$fchr][$row['start']]['stop'] = $row['stop'];
		}
		if ($row['Comment'] != "") {
			$filter[$row['Type']][$fchr][$row['start']]['comment'] .= "\n".$row['Comment'];
		}
		$filter[$row['Type']][$fchr][$row['start']]['Name'] .= "; ".$row['Name'];
	}
}	

###########################################
# OUTPUT PROJECT OPTIONS IF NONE SELECTED #
###########################################
if ($project == "") {
	echo "<div class=sectie>\n";
	echo "<h3>No Project Selected:</h3>\n";
	echo "<p>You didn't select a project to show the sample details from. Please pick one, if the default one is not correct:</p>\n";
	echo "<p><form action='index.php?page=cusdetails' method=POST>\n";
	echo "<input type=hidden name=sample value='$sample' />\n";
	echo "Available Projects: \n";
	echo "<SELECT name=project>\n";
	$projects = mysql_query("SELECT DISTINCT(p.id), p.naam FROM cus_project AS p JOIN cus_sample s ON p.id = s.idproj WHERE s.idsamp='$sample'");
	while($projrow = mysql_fetch_array($projects)) {
		#if($projrow['idproj']==$tfp){
			echo "<option value='".$projrow['id']."'>".$projrow['naam']."</option>";
			$project = $projrow['idproj'];
		#}
		#else{
		#	echo "<option value='".$projrow['idproj']."'>".$projrow['naam']."</option>";
		#}
	
	}
	#if ($project == "") {
	#	$projects = mysql_query("SELECT idproj FROM projsamp WHERE idsamp=$sample LIMIT 1");
	#	$projrow = mysql_fetch_array($projects);
	#	$project = $projrow['idproj'];
	#}
	echo "</select>&nbsp;\n";
	echo "<input type=submit class=button value=Go /></form></p>\n";
	echo "</div>\n";
}

#########################
# GET nr of ABERRATIONS #
#########################
$nrquery = mysql_query("SELECT a.id FROM cus_aberration a WHERE a.sample='$sample' AND a.idproj='$project' ");
$number=mysql_num_rows($nrquery); 

##################################
# GET PERMISSIONS ON THIS SAMPLE #
##################################
$permquery = mysql_query("SELECT editcnv, editclinic,editsample FROM cus_projectpermission WHERE projectid = '$project' AND userid = '$userid'");
$permrow = mysql_fetch_array($permquery);
$editcnv = $permrow['editcnv'];
$editclinic = $permrow['editclinic'];
$editsample = $permrow['editsample'];

######################################
# OUTPUT SAMPLE / PROJECT PROPERTIES #
######################################
$samplequery = mysql_query("SELECT a.idsamp, pr.created, a.gender, a.chiptype, pr.naam , a.platform, pr.collection,a.intrack, pr.userID FROM cus_sample a JOIN cus_project pr ON a.idproj = pr.id WHERE pr.id='$project' AND a.idsamp='$sample' ");
$samplerow = mysql_fetch_array($samplequery);
$samplename = $samplerow['idsamp'];
$gender = $samplerow['gender'] ;
if ($gender == '') {
	$gender = 'Not Specified';
}
$chiptype = $samplerow['chiptype'];
if ($chiptype == '') {
	$chiptype = 'Not Specified';
}
$platform = $samplerow['platform'];
if ($platform == '') {
	$platform = 'Not Specified';
}
$created = $samplerow['created'];
$prnaam = $samplerow['naam'];
$collection = $samplerow['collection'];
$intrack = $samplerow['intrack'];
$pown = $samplerow['userID'];
echo "<div class=sectie>\n";
echo "<p><form action='index.php?page=cusdetails' method=POST>Switch Sample:&nbsp; \n";
echo "<input type=hidden name=project value='$project'>\n";
$select = " <select name=sample>";
$prevlink = '';
$nextlink = '';
$prevsid = '';
$prevselected = 0;
$first = 1;
// get other samples from the project
$query = mysql_query("SELECT (a.idsamp) AS samplename FROM cus_sample a WHERE a.idproj = '$project' ORDER BY a.idsamp");
while($row = mysql_fetch_array($query)) {
	$csid = $row['samplename'];
	$cchip_dnanr = $row['samplename'];
	if ($cchip_dnanr == $samplename) {
		$selected = "SELECTED";
		if ($prevsid != '') {
			$prevlink = "<a href='index.php?page=cusdetails&project=$project&sample=$prevsid'>Previous</a>";
		}
		$prevselected = 1;
	}
	else {
		$selected = "";
		if ($prevselected == 1 ) {
			$nextlink = "<a href='index.php?page=cusdetails&project=$project&sample=$csid'>Next</a>";
			$prevselected = 0;
		}

	}
	$select .= "<option value='$csid' $selected>$cchip_dnanr</option>\n";
	$prevsid = $csid;
	$first = 0;
}
$select .= "</select> <input type=submit class=button name=picksubmit value=Show></form></p>\n";
//echo "</select> <input type=submit class=button name=picksubmit value=Show></form></p>\n"; 
echo "$prevlink $nextlink or select: $select";
//////////////////////////
// PRINT SAMPLE DETAILS //
//////////////////////////
echo "<h3>Overview of sample $samplename </h3>\n";
echo "<p><table cellspacing=0>\n";
echo " <tr>\n";
echo "  <th $firstcell colspan=4>Sample Details</th>";
echo " </tr>\n";
echo " <tr >\n";
echo "  <th $thtype[0]>Project </th><td $tdtype[0]>$prnaam</td>\n";
echo "  <th $thtype[0]>Created </th><td $tdtype[0]>$created</td>\n";
echo "  </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0]>Platform </th><td $tdtype[0]>$platform</td>\n";
echo "  <th $thtype[0]>Gender </th><td $tdtype[0]>$gender</td>\n";
echo "  </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0]>Chiptype </th><td $tdtype[0] >$chiptype</td>\n";
echo "  <th $thtype[0]>Total aberrations </th><td $tdtype[0]>$number</td>\n";
echo "  </tr>\n";
////////////////////////////////////
// PRINT CUSTOM ANNOTATION FIELDS //
////////////////////////////////////
$query = mysql_query("SELECT cu.value, cu.aid FROM `cus_sample_x_user_annotation` cu WHERE cu.sid = '$sample'");
$cua = array();
while ($row = mysql_fetch_array($query)) {
	$cua[$row['aid']] = $row['value'];
}
$query = mysql_query("SELECT id, name, description FROM `user_annotations` WHERE type = 'sample' ORDER BY name");
$col = 0;
while ($row = mysql_fetch_array($query)) {
	if ($col == 0) {
		echo "<tr>\n";
	
		echo "<th $thtype[0] title='".$row['description']."'>".$row['name']."</th><td>";
		if (isset($cua[$row['id']])) {
			echo $cua[$row['id']];
		}
		else {
			echo "--";
		}
		echo "</td>";
		$col = 1;
	}
	else {
		echo "<th $thtype[0] title='".$row['description']."'>".$row['name']."</th><td>";
		if (isset($cua[$row['id']])) {
			echo $cua[$row['id']];
		}
		else {
			echo "--";
		}
		echo "</td></tr>";
		$col = 0;
	}
}
if ($col == 1) {
	echo "<th $thtype[0] >&nbsp;</th><td>&nbsp;</td></tr>";
}
///////////////////
// print actions //
///////////////////
echo" <tr>\n";
echo "<th colspan=4 $firstcell>Actions</th>\n";
echo "</tr>\n"; 
echo "<tr>\n";
echo "<td $firstcell colspan=2><form id=report action='index.php?page=cusreport' method=POST>\n<input type=hidden name=sid value='$sample'>\n<input type=hidden name=pid value='$project'>\n<input type=submit class=button value='Create Patient Report'></form></td>\n";
$col = 2;
$cols = array('',$firstcell,'');
if ($editclinic == 1) {
	if ($col == 1) {
		echo "<tr>\n";
	}
	echo "<td $cols[$col] colspan=2><form action='index.php?page=cusclinical' method='POST'><input type=hidden name=insert value=1><input type=hidden name=pid value='$project'><input type=hidden name=sid value='$sample'><input type=submit class=button value='Add Clinical Information' /></form></td>\n";
	if ($col == 2) {
		echo "</tr>\n";
		$col = 1;
	}
	else {
		$col++;
	}
}
// remove sample from search results
if ($intrack == 1 ) {
	$trackbutton = "<form id=tracksetting action='cus_settrack.php' method=POST><input type=hidden name=sid value='$sample'><input type=hidden name=goal value=remove><input type=hidden name='newproject' value='$project'><input type=submit class=button name=changetrack value='Remove Sample From Search Results'></form>";
}
elseif ($intrack == 0) {
	$trackbutton = "<form id=tracksetting action='cus_settrack.php' method=POST><input type=hidden name=sid value='$sample'><input type=hidden name=goal value=add><input type=hidden name='newproject' value='$project'><input type=submit class=button name=changetrack value='Add Sample To Search Results'></form>";
}
if ($editsample == 1) {
	if ($col == 1) {
		echo "<tr>\n";
	}
	echo "<td $cols[$col] colspan=2>$trackbutton</td>\n";
	if ($col == 2) {
		echo "</tr>\n";
		$col = 1;
	}
	else {
		$col++;
	}
}
// edit sample details
if ($editsample == 1) {
	if ($col == 1) {
		echo "<tr>\n";
	}
	$trackbutton = "<form id=editsampledetails action='index.php?page=edit_sample&cus=1' method=POST><input type=hidden name=sid value='$sample'><input type=hidden name='pid' value='$project'><input type=submit class=button name=editdetails value='Edit Sample Details'></form>";
	echo "<td $cols[$col] colspan=2>$trackbutton</td>\n";
	if ($col == 2) {
		echo "</tr>\n";
		$col = 1;
	}
	else {
		$col++;
	}
}

// get regions 
$tempq = mysql_query("SELECT chr, start, stop, LiftedFrom FROM cus_aberration WHERE sample='$sample' AND idproj='$project' ORDER BY chr, start");
$regionstring = '';
while ($trow = mysql_fetch_array($tempq)) {
	$regionstring .= "chr".$chromhash[$trow['chr']] .":".$trow['start'].'-'.$trow['stop'].'|';
	 if ($trow['LiftedFrom'] != '') {
                $liftnote = '<p><span style="border:solid 1px red;padding:3px;"><span class=nadruk style="color:red">NOTE: </span>This sample is lifted from UCSC'.$trow['LiftedFrom']."</span></p>";
        }

}
if ($regionstring != '') {
	$regionstring = substr($regionstring,0,-1);
}

$urlquery = mysql_query("SELECT link FROM db_links WHERE Resource = 'PubMed Fetcher'");
$urow = mysql_fetch_array($urlquery);
$pmlink = $urow['link'];
$pmlink = str_replace('%r',$regionstring,$pmlink);

// get phenotypes
$tparray = array();
$tclinquery = mysql_query("SELECT lddbcode FROM cus_sample_phenotypes WHERE sid = '$sample'");
while ($trow = mysql_fetch_array($tclinquery)) {
	$code = $trow['lddbcode'];
	$tparray[] = $code;
}
mysql_select_db('LNDB');
$clinics = array();
foreach ($tparray as $key => $code) {
	$tquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
	$trow = mysql_fetch_array($tquery);
	array_push($clinics, $trow['LABEL']);
}
if (! empty($clinics)) {
    $clinhtml = http_build_query(array('term' => $clinics));
    $pmlink .= "&$clinhtml";
}

$trackbutton = "<form action='$pmlink' target='_blank' method=POST><input class=button type=submit value='Search PubMed'></form>";

mysql_select_db($db);
if ($col == 1) {
	echo "<tr>\n";
}
echo "<td $cols[$col] colspan=2>$trackbutton</td>\n";
if ($col == 2) {
	echo "</tr>\n";
	$col = 1;
}
else {
	$col++;
}
if ($pown == $userid) {
	if ($col == 1) {
		echo "<tr>\n";
	}
	echo "<td $cols[$col] colspan=2><form action='index.php?page=shareprojects' method=POST target='_blank'><input type=hidden name=pid value='$project'><input type=submit class=button value='Share This Project'></form></td>\n";
	if ($col == 2) {
		echo "</tr>\n";
		$col = 1;
	}
	else {
		$col++;
	}
}
// check if data is available
$hdquery = mysql_query("SELECT hasdata FROM cus_sample WHERE idsamp = '$sample' AND idproj = '$project'");
$hdrow = mysql_fetch_array($hdquery);
$hasdata = $hdrow['hasdata'];
if ($hasdata == 0) {
	$button = "<form action='index.php?page=cus_adddata' method=POST ><input type=hidden name=pid value='$project'><input type=hidden name=sid value='$sample'><input type=submit class=button value='Add Probe-Level Data'></form>";
}
else {
	$button = "<form action='index.php?page=cus_updatedata' method=POST ><input type=hidden name=pid value='$project'><input type=hidden name=sid value='$sample'><input type=submit class=button value='Update Probe-Level Data'></form>";
}
if ($col == 1) {
	echo "<tr>\n";
}
echo "<td $cols[$col] colspan=2>$button</td>\n";
if ($col == 2) {
	echo "</tr>\n";
	$col = 1;
}
else {
	$col++;
}


if ($col == 2) {
	echo "<td colspan=2>&nbsp;</td>\n";
	echo "</tr>\n";
}




echo "</table>\n";
echo "</p>\n";
if (defined($liftnote)) {
	echo $liftnote;
}
/////////////////////////////
// check for clinical data //
/////////////////////////////
//freetext
$clinquery = mysql_query("SELECT DECODE(clinical, '$encpass') as clindata FROM cus_sample WHERE idsamp = '$sample' AND idproj = '$project' ");
$row = mysql_fetch_array($clinquery);
$clindata = $row['clindata'];
if ($clindata != '') {
	$clindata = str_replace(array("\r\n", "\r", ),"\n",$clindata);
	$nrlines = substr_count($clindata,"\n");
	echo "\n";
	echo "<p><div class=clinical><div class=clinheader >&nbsp;Clinical Synopsis:</div>$clindata</div></p>\n";

}

//ontology based
mysql_select_db($db);
$parray = array();
$clinquery = mysql_query("SELECT lddbcode FROM cus_sample_phenotypes WHERE sid = '$sample' AND pid = '$project' ORDER BY lddbcode");
if (mysql_num_rows($clinquery) > 0) {
	echo "<p><div class=clinheader >&nbsp;Structured Clinical Synopsis:</div></p>\n";
	while ($row = mysql_fetch_array($clinquery)) {
		$code = $row['lddbcode'];
		$pieces = explode('.',$code);
		$parray[$pieces[0]][$pieces[1]][] = $pieces[2];
	}
	mysql_select_db('LNDB');
	echo "<p><table cellspacing=0>";
	echo "<tr>\n";
	echo "<th $firstcell class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
	echo "</tr>\n";
	$fts = '&ft=1';
	foreach($parray as $first => $sarray) {
		$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
		$row = mysql_fetch_array($query);
		$firstlabel = $row['LABEL'];
		foreach ($sarray as $second => $tarray) {
			if ($second == '00') {
				echo "<tr><td $firstcell>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>\n";
			}
			else {
				$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
				$row = mysql_fetch_array($query);
				$secondlabel = $row['LABEL'];
				foreach ($tarray as $third) {
					if ($third == '00') {
						echo "<tr><td $firstcell>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>\n";	
					}
					else {
						$query = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
						$row = mysql_fetch_array($query);
						$thirdlabel = $row['LABEL'];
					
						echo "<tr><td $firstcell>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>\n";
					}	
				}
			}
		}
	}
	echo "</table>\n";
	mysql_select_db($db);
}
echo "</div>\n";


###################
# SORTING OPTIONS #
###################
echo "<div class=sectie>\n";
echo "<h3>Sorting options</h3>\n";
echo "<p>The called CNV's can be ordered in three ways. The first and standard method is by chromosomal position ('Chromosomal Position'). Secondly, CNV's can be sorted by decreasing number of affected genes and size ('Genes &amp; Size'). The final method is to rank them based on gene prioritization. If you select this method ('Prioritization'), you will be prompted for further options. If you need the list of all affected genes, you can get it <a href=\"listgenes.php?sample=$sample&amp;idproj=$project&amp;type=symbol\" target=new>here</a>.\n";

$switch=0;
echo "<p><form action='index.php?page=cusdetails' method=POST>\n";
echo "  <input type=hidden name=sample value='$sample'>\n";
echo "  <input type=hidden name=project value='$project'>\n";
echo "  Order by: <select name=sort>\n";
if ($sort == 'position' ) { 
	echo "  <option value=position checked>Chromosomal Position</option>\n";
	echo "  <option value=size>Genes &amp; Size</option>\n";
	//echo "  <option value=rank>Prioritization</option>\n";
	$titlepart = "Ordered By Chromosomal Position";
}
elseif ($sort == 'size') {
	echo "  <option value=position >Chromosomal Position</option>\n";
	echo "  <option value=size selected>Genes &amp; Size</option>\n";
	//echo "  <option value=rank>Prioritization</option>\n";
	$titlepart = "Ordered By Number of Affected Genes &amp; CNV Size";
}
elseif ($sort == 'rank') {
	echo "  <option value=position >Chromosomal Position</option>\n";
	echo "  <option value=size>Genes &amp; Size</option>\n";
	//echo "  <option value=rank selected>Prioritization</option>\n";
	$titlepart = "Ordered By Gene Prioritization";
	
}
echo "</select>\n";
echo "  <input type=submit class=button value=change>\n";
echo "</form>\n</p>\n";

if ($sort == 'rank') {
	include('inc_cus_details_WGP_pt1.inc');
}
echo "</div>\n";

####################
# GET ABERRATIONGS #
####################

if ($sort == 'rank') {
	include('inc_cus_details_WGP_pt2.inc');	
}
else {
	$querystring = "SELECT a.id, a.cn, a.start, a.stop, a.chr, a.confidence, a.nrprobes, a.class, a.inheritance, a.nrgenes, a.pubmed FROM cus_aberration a  WHERE sample='$sample' AND idproj='$project' ORDER BY $sortoptions[$sort]";
	$userquery = mysql_query("SELECT endeavour FROM users where id = '$userid'");
	$userarray = mysql_fetch_array($userquery);
	$defaultmodel = $userarray[ 'endeavour' ];
	$priorquery = mysql_query("SELECT id FROM cus_prioritize WHERE sample = '$sample' AND project = '$project' AND sigfound = 1 LIMIT 1");	
	$nrrows = mysql_num_rows($priorquery);
	if ($nrrows > 0) {
		$checkprior = 1;
		$row = mysql_fetch_array($priorquery);
		$prid = $row['id'];
	}
	else {
		$checkprior = 0;
	}
		

}

$pmids = '';
$abquery = mysql_query($querystring);
################
# RESULTS TABLE #
################
echo "<div class=sectie>\n";
echo "<h3>Observed Aberrations $titlepart</h3>\n";
echo "<h4>Hover column headers for more information</h4>\n ";
#echo "<p><a href=\"index.php?page=cusclass&amp;sid=$sample&amp;pid=$project\">Click here to change 'Diagnostic Class' or 'Inheritance' settings for the CNV's below.</a></p>\n"; 
echo "<p><table cellspacing=0 >\n";
echo " <tr>\n"; 
echo "  <th scope=col class='topcellalt' $firstcell>!</th>\n";
echo "  <th scope=col class='topcellalt' title='Chromosomal position using build $ucscdb'>Location</th>\n";
echo "  <th scope=col class='topcellalt' title='Predicted Copy Number'>CN</th>\n";
echo "  <th scope=col class='topcellalt'>Size</th>\n";
echo "  <th scope=col class='topcellalt' title='Number of probes covering this CNV'>#Probes</th>\n";
echo "  <th scope=col class='topcellalt' title='Diagnostic Class: 1=Published region ; 2=Considered pathogenic ; 3=Unclear significance ; 4=Considered polymorphism'>DC</th>\n";
echo "  <th scope=col class='topcellalt' title='Inheritance: DN=De Novo ; P=Paternal ; M=Maternal ; empty=Not defined'>Inher.</th>\n";
echo "  <th scope=col class='topcellalt' title='Calling Method Confidence Levels'>Confidence</th>\n";
echo "  <th scope=col class=topcellalt title=\"RefSeq Genes: Hover red numbers to see significantly prioritized genes. They will be marked on the gene details page as well\">#RS</th>\n";
echo "  <th scope=col class=topcellalt>Cases</th>\n";
echo "  <th scope=col class=topcellalt>Extra Info</th>\n";
echo " </tr>\n";

$pmids = '';
$filtered = 0;
$bgcolors = array("2" => "#D1D1D1","3" => "#FFCCCC");
while($row = mysql_fetch_array($abquery)) {
	mysql_select_db("$db");
	$aid = $row['id'];
	$chr = $row['chr'];
	$chrtxt = $chromhash[ $chr ];
	$start = $row['start'];
	$end = $row['stop'];
	// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
	foreach ($filter["1"][$chr] as $fstart => $farray) {
		if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $end) {
			$filtered++;
			continue 2;
		}
	}	
	$csize = $end-$start+1;
	$cn = $row['cn'];
	$class = $row['class'];
	$cntxt = $cn;
	$pubmed = $row['pubmed'];
	$confidence = $row['confidence'];
	$nrprobes = $row['nrprobes'];
	if (is_numeric($cn)) {
		# keep original
	}
	elseif (preg_match("/gain/i",$cntxt)) {
		$cn = 3;
	}
	elseif (preg_match("/los/i",$cntxt)) {
		$cn = 1;
	}
	elseif (preg_match("/loh/i",$cntxt)) {
		$cn = 2;
	}
	if ($cn == 2 && ($_SESSION['LOHshow'] == 0 || $csize < $_SESSION['LOHminsize'] )) {
		continue;
	}
	$inheritance = $row['inheritance'];
	$nrgenes = $row['nrgenes'];
	$score = 0;
	// Check if there are prioritization results
	if ($checkprior == 1 && $nrgenes > 0) {
		//echo "$aid found<br>";
		$sgquerystring = "SELECT signGenes FROM cus_priorabs WHERE prid = '$prid' AND aid = '$aid' ";
		$sgquery = mysql_query("SELECT signGenes FROM cus_priorabs WHERE prid = '$prid' AND aid = '$aid' ");
		$sgrow = mysql_fetch_array($sgquery);
		$signGenes = $sgrow['signGenes'];
	}
	elseif ($sort == 'rank') {
		$signGenes = $row['signGenes'];
	}
	else {
		$signGenes = '';
	}
	if ($signGenes != "" ) {
		$signGenes = substr($signGenes,0,-1);
		$fontstyle = "style='color:red'";
		$title = "title=\"$signGenes\"";
		$linkpart = "&amp;prid=$prid&amp;aid=$aid";
	}
	else {
		$fontstyle = "";
		$title = "";
		$linkpart="";
	}

	// adapt scores
	if ($nrgenes > 0 && $nrgenes <= 5) {
		$score = $score + 1;
	}
	if ($nrgenes > 5) {
		$score = $score + 2;
	}
	// Determine Cyto Band
	$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
	$cytorow = mysql_fetch_array($cytoquery);
	$cytostart = $cytorow['name'];
	$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$end' BETWEEN start AND stop)");
	$cytorow = mysql_fetch_array($cytoquery);
	$cytostop = $cytorow['name'];
	if ($cytostart == $cytostop) {
		$cytoband = "($chr$cytostart)";
	}
	else {
		$cytoband = "($chr$cytostart-$cytostop)";
	}  

	# check decipher entries
	#mysql_select_db("decipher");	
	$query = mysql_query("SELECT ID FROM decipher_syndromes WHERE chr = '$chr' AND ((start >= '$start' AND start <= '$end') OR (stop >= '$start' AND stop <= '$end') OR (start <= '$start' AND stop >= '$end')) ");
	//$data = mysql_query($query);
	$nrrows = mysql_num_rows($query);
	if ($nrrows > 0) {
		$decipher = "<a class='clear' href='index.php?page=showdecipher&amp;type=syndromes&amp;chr=$chrtxt&amp;start=$start&amp;end=$end&amp;case=$sampleid' target='new'><img src='images/bullets/syndrome.ico' alt=''/></a>";	 
		$score = $score + 2;
	}
	else {
		$query = mysql_query("SELECT id FROM decipher_patsum WHERE chr = '$chr' AND ((start >= '$start' AND start <= '$end') OR (stop >= '$start' AND stop <= '$end') OR (start <= '$start' AND stop >= '$end')) ");
		$nrrows = mysql_num_rows($query);
		if ($nrrows > 0) {
			$decipher = "<a class='clear' href='index.php?page=showdecipher&amp;type=patients&amp;chr=$chrtxt&amp;start=$start&amp;end=$end&amp;case=$sampleid' target='new'><img src='images/bullets/patient.ico' alt=''/></a>";
			$score = $score +1;
		}
		else {
			$decipher = "";
		}
	}
	$score = floor($score);
	$extracol = '';
	## CHECK PUBMED ENTRIES ##
	if ($pubmed != '') {
		//$pmimg = "<a class='clear' href='index.php?page=literature&amp;type=read&aid=$aid&cstm=1' ><img src='images/content/pubmed.png' height='24'></a>";
		$pmids .= "$pubmed,";
		require_once 'xmlLib2.php';
		$link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=$pubmed&retmode=xml&rettype=abstract";
		system("wget -O '/tmp/output.fetching' '$link'");
		flush();
		$xml_data = implode("\n",file('/tmp/output.fetching'));
		unlink("/tmp/output.fetching");
		$xmlObj = new XmlToArray($xml_data);
		$abArray = $xmlObj->createArray();
		foreach ($abArray['PubmedArticleSet']['PubmedArticle'] as $key => $value) {
			$date = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['PubDate'][0];
			$year = $date['Year'];
			$authors = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['AuthorList'][0]['Author'];
			$astring = '';
			if (count($authors) > 2) {
				$astring = "<li><a class=clear href='index.php?page=literature&amp;type=read&aid=$aid&cstm=1' >".$authors[0]['LastName'] . "<span class=italic> et al.</span>, $year</a></li>";
			}
			elseif (count($authors) == 2) {
				$astring = "<li><a class=clear href='index.php?page=literature&amp;type=read&aid=$aid&cstm=1' >".$authors[0]['LastName'] . " and ". $authors[1]['Lastname'].", $year</a></li>";
			}
			else {
				$astring = "<li><a class=clear href='index.php?page=literature&amp;type=read&aid=$aid&cstm=1' >".$authors[0]['LastName'] . ", $year</a></li>";
			}
			$extracol .= "$astring";
		}

	}
	#else {
	#	$pmimg = '';
	#}
	if ($extracol != '') {
		$extracol = "<td $tdtype[$switch]><ul id=ul-table style='padding-left:1px;' >$extracol</ul></td>";
	}
	else {
		$extracol = "<td $tdtype[$switch]>&nbsp;</td>";
	}

	# Fill Table
	ob_flush();
	flush();
      	$size = $end - $start;
      	$location = "chr". $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($end,0,'',',') ;
	// filter check
	$filterbg = '';
	$filtericon = '&nbsp;';
	$fname = '';
	$ftitle = '';
	for ($ftype = 2; $ftype <= 3 ; $ftype++) {
		foreach ($filter[$ftype][$chr] as $fstart => $farray) { 
			if (($fstart <= $start && $filter[$ftype][$chr][$fstart]['stop'] >= $end) || ($fstart >= $start && $fstart <= $end) || ($filter[$ftype][$chr][$fstart]['stop'] >= $start && $filter[$ftype][$chr][$fstart]['stop'] <= $end)) {
				// set bg color.
				$filterbg = "style='background-color:".$bgcolors[$ftype].";'";
				// set tooltip entry.
				$filtericon = "<img src='images/content/filter.png' height='24px' ";
				$fname .= $filter[$ftype][$chr][$fstart]['Name']."; ";
				$ftitle .= $filter[$ftype][$chr][$fstart]['comment']."\n";
			}
		}

	}
	if ($fname != '') {
		$fname = substr($fname,0,-2);
	}	
	if ($filtericon != '&nbsp;') {
		$filtericon .= "title='Filter:".$fname."\n\n".$ftitle."' />";
	}
      	echo " <tr context='cusabrow' aid='$aid' uid='$userid' $filterbg>\n";
	echo "<td $firstcell $tdtype[$switch] >$filtericon</td>";
	#if ($score > 0) {
	#      echo "  <td $tdtype[$switch] $firstcell><img src='images/bullets/$color[$score].ico' alt=''/></td>\n";
	#}
	#else {
	#	echo "<td $tdtype[$switch] $firstcell>&nbsp;</td>\n";
	#}
      	echo "  <td $tdtype[$switch] >$location<br/>$cytoband</td>\n";
	echo  "  <td $tdtype[$switch] onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;c=$chr&amp;start=$start&amp;stop=$end&amp;id=$aid','CustomSearchSamples','$userid','full',event)\" >$cn</td>\n";
      	//echo "  <td $tdtype[$switch] onmouseover=\"Tip(CustomToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\">$cn</td>\n";
      	echo "  <td $tdtype[$switch]>" . number_format($size,0,'',',') . "</td>\n";
      	echo "  <td $tdtype[$switch] >$nrprobes</td>\n";
      	/*if ($class == 0) {
		$class = '';
      	}
      	elseif ($class == 5) {
		$class = 'FP';
      	}
	*/
	if (array_key_exists($class,$dc)){	
	      	echo "  <td $tdtype[$switch] >$dc[$class] &nbsp;</td>\n";
	}
	else {
	      	echo "  <td $tdtype[$switch] >$class &nbsp;</td>\n";
	}		
	if ($inheritance == 'Not Defined') {
		$inheritance = '0';
	}
	if (array_key_exists($inheritance,$inh)) {

	      	echo "  <td $tdtype[$switch] >$inh[$inheritance]&nbsp;";
	}
	else {
		echo "<td $tdtype[$switch] >$inheritance&nbsp;";
	}
	echo "</td>\n";
      	echo "  <td $tdtype[$switch] NOWRAP>$confidence</td>\n";
      	if ($nrgenes > 0) {
	      echo "  <td $tdtype[$switch] $title><a href='index.php?page=genes&amp;chr=$chrtxt&amp;start=$start&amp;stop=$end&amp;cstm=1$linkpart' target='new' $fontstyle >$nrgenes</a></td>\n";
      	}
      	else {
	     echo "   <td $tdtype[$switch]>0</td>\n";
      	}
      	echo "  <td $tdtype[$switch]>";
      	echo "  $decipher&nbsp;</td>$extracol\n";
      	echo "</tr>\n";
      	$switch = $switch + pow(-1,$switch);
      	flush();
   
}
echo "</table>\n";
echo "</div>\n";

########################
## PRINT BIBLIOGRAPHY ##
########################
if ($pmids != '') {
	$pmitems = array();
	$firstauthors = array();
	$pmids = substr($pmids,0,-1);
	#$first = 1;
	require_once 'xmlLib2.php';
	$link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=$pmids&retmode=xml&rettype=abstract";
	system("wget -O '/tmp/output.fetching' '$link'");
	flush();
	$xml_data = implode("\n",file('/tmp/output.fetching'));
	unlink("/tmp/output.fetching");
	$xmlObj = new XmlToArray($xml_data);
	$abArray = $xmlObj->createArray();
	foreach ($abArray['PubmedArticleSet']['PubmedArticle'] as $key => $value) {
		$title = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['ArticleTitle'];
		$pmid = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['PMID'];
		$date = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['PubDate'][0];
		$volume = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['Volume'];
		$issue = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['Issue'];
		$pages = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Pagination'][0]['MedlinePgn'];
		$datestring = $date['Year'] .' ' . $date['Month'] . ";$volume($issue):$pages";
		$journal = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['ISOAbbreviation'];
		$authors = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['AuthorList'][0]['Author'];
		$pmitems[$pmid]['title'] = $title;
		$pmitems[$pmid]['datestring'] = $datestring;
		$pmitems[$pmid]['journal'] = $journal;
		$astring = '';
		$firstauthor = 1;
		foreach ($authors as $akey => $aarray) {
			#$astring .= "";
			if ($firstauthor == 1) {
				$astring .= $aarray['LastName'] ;
				$firstauthors[$aarray['LastName']] = $pmid;
				$firstauthor = 0;
			}
			else {
				$astring .= "<span class=italic> et al.</span>";
				break;
			}
		} 
		#$astring = substr($astring,0,-2);
		#echo "$astring </td></tr>";
		$pmitems[$pmid]['authors'] = $astring;
	}
	ksort($firstauthors);
	echo "<div class=sectie>";
	echo "<h3>References</h3>\n";
	echo "<p><ul id=ul-log>";
	foreach($firstauthors as $aut => $pmid) {
		echo "<li>".$pmitems[$pmid]['authors']. ", ".$pmitems[$pmid]['title']." ". $pmitems[$pmid]['journal']. " ". $pmitems[$pmid]['datestring'].". <a href='http://www.ncbi.nlm.nih.gov/pubmed/$pmid' target='_blank'>[PubMed]</a></li>\n";
	}
	echo "</ul></p>";
	echo "</div>";
}

// PRINT HISTORY
mysql_select_db($db);
echo "<div class=sectie>\n";
echo "<p>\n";
echo "<span class=nadruk>History:</span>\n";
echo "</p>\n";
echo "<p><ul id=ul-log>\n";
$history = mysql_query("SELECT l.aid, l.time, u.FirstName, u.LastName, l.entry, l.arguments FROM cus_log l JOIN users u ON u.id = l.uid WHERE l.sid = '$sample' AND l.pid = '$project' ORDER BY l.time DESC LIMIT 10");
while ($row = mysql_fetch_array($history))  {
	$aid = $row['aid'];
	$sq = mysql_query("SELECT chr, start, stop, cn FROM cus_aberration WHERE id = '$aid'");
	$srows = mysql_num_rows($sq);
	if ($srows > 0) {
		$srow = mysql_fetch_array($sq);
		$chr = $srow['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $srow['start'];
		$starttxt = number_format($start,0,'',',');
		$stop = $srow['stop'];
		$stoptxt = number_format($stop,0,'',',');
		$cn = $srow['cn'];
	}
	else {
		$ssq = mysql_query("SELECT chr, start, stop, cn FROM cus_deletedcnvs WHERE id = '$aid'");
		$srow = mysql_fetch_array($ssq);
		$chr = $srow['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $srow['start'];
		$starttxt = number_format($start,0,'',',');
		$stop = $srow['stop'];
		$stoptxt = number_format($stop,0,'',',');
		$cn = $srow['cn'];
	}
	$time = $row['time'];
	$FName = $row['FirstName'];
	$LName = $row['LastName'];
	$logentry = $row['entry'];
	if (preg_match("/Diagnostic Class/",$logentry)) {
		$logentry = str_replace("5", "False Positive", $logentry);
	}
	$arguments = $row['arguments'];
	echo "<li><span class=italic>$time : </span>Chr$chrtxt:$starttxt-$stoptxt (cn:$cn) : $logentry by $FName $LName";
	if ($arguments != '') {
		echo " : $arguments</li>\n";
	}
	else {
		echo "</li>\n";
	}
}
echo "</div>\n";
}
?>


<!-- context menu loading -->
<!-- <script type="text/javascript" src="javascripts/details_context.js"></script> -->
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/ajax_tooltips.js"></script>
<!-- <script type="text/javascript" src="javascripts/details_tooltips.js"></script> -->
<script type='text/javascript' src='javascripts/details.js'></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>

