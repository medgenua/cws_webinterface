<?php
/////////////////////////
// CONNECT TO DATABASE //
/////////////////////////
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$query = mysql_query("SELECT StringName FROM `GenomicBuilds`.`CurrentBuild` LIMIT 1");
$row = mysql_fetch_array($query);
$buildname = $row['StringName'];

// DEFINE SOME VARS
$inharray = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chararray = array('&nbsp;','_','&ccedil;','&amp;','&sect;','&ecirc;', '$','&micro;', '&lt;', '&gt;', '#','&deg;','&pound;');
$newchararray = array(' ','\_','\c{c}','\&','\textsection','ê','\$','$\mu$','\textless', '\textgreater', '\#', '\textdegree','\pounds' );

// POSTED VARIABLES 
$sample = $_POST['sid'];
$sid = $sample;
$pid = $_POST['pid'];
$minprobe = $_POST['minprobe'];
if (isset($_POST['minqs'])) {
	$minqs = $_POST['minqs'];
}
if (isset($_POST['minps'])) {
	$minps = $_POST['minps'];
}
$maxclass= $_POST['maxclass'];
$minsize = $_POST['minsize'];

// GET SAMPLE & PROJECT DETAILS 
$query = mysql_query("SELECT a.idsamp, a.gender, p.naam, a.platform, a.chiptype, p.collection, p.created, u.FirstName, u.LastName FROM cus_sample a JOIN cus_project p JOIN users u ON a.idproj = p.id AND u.id = p.userID WHERE a.idsamp = '$sample' AND p.id = $pid" );
$row = mysql_fetch_array($query);
$samplename = $row['idsamp'];
#echo "This is under Construction !\n";
$gender = $row['gender'];
if ($gender == '') {
	$gender = 'Not Specified';
}
#$callrate = $row['callrate'];
#$lrrsd = $row['LRRSD'];
#$bafsd = $row['BAFSD'];
#$wave = $row['WAVE'];
#$iniwave = $row['INIWAVE'];
$pname = $row['naam'];
$platform = $row['platform'];
if ($platform == '') {
	$platform = 'Not Specified';
}
$chiptype = $row['chiptype'];
if ($chiptype == '') {
	$chiptype = 'Not Specified';
}
$collection = $row['collection'];
$fname = str_replace('_','\_',$row['FirstName']);
$lname = str_replace('_','\_',$row['LastName']);
$created = $row['created'];

# generate karyotype plot
#if ($_POST['inckaryo'] == 1) {
#	$karyocustom = 1;
#	include('karyoreport.php');
#}
## replace spaces
$samplename = str_replace(" ","_",$samplename);
// PREPARE TEX FILE
echo "<div class=sectie>\n";
echo "<h3> Preparing the report file</h3>\n";
echo "<h4>status...</h4>\n";
echo "<ul id=ul-disc>\n";
echo "<li> Preparing LaTeX document\n";
flush();
ob_flush();
unlink("$sitedir/Reports/$samplename.tex");
copy("$sitedir/Reports/preamble.tex", "$sitedir/Reports/$samplename.tex");
$fh = fopen("$sitedir/Reports/$samplename.tex", 'a');
$footerstring = '\fancyfoot[L]{CNV report: '.str_replace('_','\_',$samplename).'}'."\n";
fwrite($fh,$footerstring);

// PRINT EXPERIMENTAL DATAILS ON SAMPLE / PROJECT
$titlestring = '\section*{\huge{'.str_replace('_','\_',$samplename).'}}'."\n" . '\hrule' . "\n" . '\vspace{0.5cm}'. "\n";
fwrite($fh, $titlestring);
$tablestring = '\subsection*{\textsl{\underline{Experimental Details}}}'."\n";
$tablestring .= '\begin{tabular}{lllll}'."\n";
$tablestring .= '\textbf{Sample Gender} & '. $gender .' & & \textbf{Project} & '. str_replace('_','\_',$pname) . '\\\\' ."\n";
$tablestring .= '\textbf{Platform} & '. $platform . ' & & \textbf{Collection} & '. str_replace('_','\_',$collection) . '\\\\' ."\n";
$tablestring .= '\textbf{Chiptype} & '. $chiptype .' & & \textbf{Analysis Date} & '. $created . ' \\\\'."\n";
$tablestring .= '\textbf{Genomic Build} & '.$buildname.' & & \textbf{Analysed By} & '. $fname . ' ' . $lname . '\\\\' ."\n";
$tablestring .= '\end{tabular}'."\n";
fwrite($fh, $tablestring);


// PROCESS EXTRA COMMENTS
if ($_POST['inccom'] == 1 && !preg_match('/Experimental Comments/',$_POST['comments']) && $_POST['comments'] != '') {
	$subtitlestring = '\subsection*{\textsl{\underline{Additional comments}}}'."\n";
	fwrite($fh,$subtitlestring);
	$comments = $_POST['comments'];
	# replace text chars	
	$comments = str_replace($chararray, $newchararray, $comments);
	# replace aigu accents
	while (preg_match('/&.acute;/',$comments)) {
		$comments = preg_replace('/(.*)&(.{0,1})(acute;)(.+)/',"$1".'\\\'{'."$2".'}'."$4",$comments);
	} 
	# replace grave accents
	while (preg_match('/&.grave;/',$comments)) {
		$comments = preg_replace('/(.*)&(.{0,1})(grave;)(.+)/',"$1".'\\\`{'."$2".'}'."$4",$comments);
	}
	# replace umlauts
	while (preg_match('/&.uml;/',$comments)) {
		$comments = preg_replace('/(.*)&(.{0,1})(uml;)(.+)/',"$1".'\"{'."$2".'}'."$4",$comments);
	}
	# replace tilde
	while (preg_match('/&.tilde;/',$comments)){
		$comments = preg_replace('/(.*)&(.{0,1})(tilde;)(.+)/',"$1".'\~{'."$2".'}'."$4",$comments);
	}
	# replace bold tags
	while (preg_match('/<strong>.*<\/strong>/',$comments)) {
		$comments = preg_replace('/(.*)(<strong>)(.*)?(<\/strong>)(.*)/',"$1".'\textbf{'. "$3". '}'. "$5", $comments);
	}
	#replace underline tags
	while (preg_match('/<span style="text-decoration: underline;">.*<\/span>/',$comments)) {
		$comments = preg_replace('/(.*)(<span style="text-decoration: underline;">)(.*)?(<\/span>)(.*)/',"$1".'\underline{'."$3".'}'."$5",$comments);
	}
	# replace italic tags
	while (preg_match('/<em>.*<\/em>/',$comments)) {
		$comments = preg_replace('/(.*)(<em>)(.*)?(<\/em>)(.*)/',"$1".'\textsl{'."$3".'}'."$5",$comments);
	}
	# replace other chars	
	$comments = str_replace(array("\r\n", "\r", '<br>', '<br />'), "\n", $comments);
	$comments = str_replace('<ul>', '\begin{packed_item} ',$comments);
	$comments = str_replace('</ul>', '\end{packed_item} ',$comments);
	$comments = str_replace('<ol>','\begin{packed_enum} ',$comments);
	$comments = str_replace('</ol>','\end{packed_enum} ',$comments);
	$comments = str_replace('<li>','\item ',$comments);
	$comments = str_replace('<p>',"\n".'\noindent'."\n",$comments);
	$comments = str_replace(array('</li>', '<div>','</div>','</p>'),' ',$comments);
	fwrite($fh,$comments);
}

// PROCESS CLINICAL DETAILS
if ($_POST['incclin'] == 1 && !preg_match('/Clinical Information/', $_POST['clinical']) && $_POST['clinical'] != '') {
	$subtitlestring = '\subsection*{\textsl{\underline{Clinical Summary}}}'."\n";
	fwrite($fh,$subtitlestring);
	$clinical = $_POST['clinical'];
	
	$clinical = str_replace($chararray, $newchararray, $clinical);
	# replace aigu accents
	while (preg_match('/&.acute;/',$clinical)) {
		$clinical = preg_replace('/(.*)&(.{0,1})(acute;)(.+)/',"$1".'\\\'{'."$2".'}'."$4",$clinical);
	} 
	# replace grave accents
	while (preg_match('/&.grave;/',$clinical)) {
		$clinical = preg_replace('/(.*)&(.{0,1})(grave;)(.+)/',"$1".'\\\`{'."$2".'}'."$4",$clinical);
	}
	# replace umlauts
	while (preg_match('/&.uml;/',$clinical)) {
		$clinical = preg_replace('/(.*)&(.{0,1})(uml;)(.+)/',"$1".'\"{'."$2".'}'."$4",$clinical);
	}
	# replace tilde
	while (preg_match('/&.tilde;/',$clinical)){
		$clinical = preg_replace('/(.*)&(.{0,1})(tilde;)(.+)/',"$1".'\~{'."$2".'}'."$4",$clinical);
	}
	# replace bold tags
	while (preg_match('/<strong>.*<\/strong>/',$clinical)) {
		$clinical = preg_replace('/(.*)(<strong>)(.*)?(<\/strong>)(.*)/',"$1".'\textbf{'. "$3". '}'. "$5", $clinical);
	}
	#replace underline tags
	while (preg_match('/<span style="text-decoration: underline;">.*<\/span>/',$clinical)) {
		$clinical = preg_replace('/(.*)(<span style="text-decoration: underline;">)(.*)?(<\/span>)(.*)/',"$1".'\underline{'."$3".'}'."$5",$clinical);
	}
	# replace italic tags
	while (preg_match('/<em>.*<\/em>/',$clinical)) {
		$clinical = preg_replace('/(.*)(<em>)(.*)?(<\/em>)(.*)/',"$1".'\textsl{'."$3".'}'."$5",$clinical);
	}
	# replace other chars	
	$clinical = str_replace(array("\r\n", "\r", '<br>', '<br />'), "\n", $clinical);
	$clinical = str_replace('<ul>', '\begin{packed_item} ',$clinical);
	$clinical = str_replace('</ul>', '\end{packed_item} ',$clinical);
	$clinical = str_replace('<ol>','\begin{packed_enum} ',$clinical);
	$clinical = str_replace('</ol>','\end{packed_enum} ',$clinical);
	$clinical = str_replace('<li>','\item ',$clinical);
	$clinical = str_replace('<p>',"\n".'\noindent'."\n",$clinical);
	$clinical = str_replace(array('</li>', '<div>','</div>','</p>'),' ',$clinical);
	fwrite($fh,$clinical);
}
////////////////////////////////////
// PROCESS ONTOLOGY CLINICAL INFO //
////////////////////////////////////

if ($_POST['incclinontology'] == 1 ){
	$subarray = array();
	$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
	$lddb = "LNDB";
	mysql_select_db($cnvdb);
	$subquery = mysql_query("SELECT lddbcode FROM cus_sample_phenotypes WHERE sid = '$sid' AND pid = '$pid' ORDER BY lddbcode");
	while ($subrow = mysql_fetch_array($subquery)) {
		$code = $subrow['lddbcode'];
		$pieces = explode('.',$code);
		$subarray[$pieces[0]][$pieces[1]][] = $pieces[2];
	}
	mysql_select_db($lddb);
	$subtablestring = '';
	$match = 0;
	foreach($subarray as $first => $sarray) {
		$ssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
		$ssubrow = mysql_fetch_array($ssubquery);
		$firstlabel = $ssubrow['LABEL'];
		foreach ($sarray as $second => $tarray) {
			if ($second == '00') {
				$subtablestring .= "$firstlabel & & ".'\\\\' ."\n";
			}
			else {
				$sssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
				$sssubrow = mysql_fetch_array($sssubquery);
				$secondlabel = $sssubrow['LABEL'];
				foreach ($tarray as $third) {
					if ($third == '00') {
						$subtablestring .= "$firstlabel & $secondlabel & ".'\\\\' ."\n";	
					}
					else {
						$ssssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
						$ssssubrow = mysql_fetch_array($ssssubquery);
						$thirdlabel = $ssssubrow['LABEL'];
						$subtablestring.= "$firstlabel & $secondlabel & $thirdlabel ".'\\\\'."\n";
					}	
				}
			}
		}	
	}
	if ($subtablestring != '') {
		$subtitlestring = '\subsection*{\textsl{\underline{Clinical Information (LNDB)}}}'."\n";
		fwrite($fh,$subtitlestring);
		#$filter .= '\begin{tabular}{p{0.4\textwidth}p{0.3\textwidth}p{0.27\textwidth} }' ."\n";
		$tableheadstring = '{\footnotesize '."\n".'\begin{tabular}{@{\extracolsep{\fill}}lll}'."\n" . '\hline' . "\n";
		$tableheadstring .= '\textbf{Primary} & \textbf{Secondary} & \textbf{Tertiary} '.'\\\\'."\n";
		$tableheadstring .= '\hline'."\n";
		fwrite($fh,$tableheadstring);
		fwrite($fh,$subtablestring);
		$endtablestring = '\hline' . "\n".'\end{tabular}'."\n".' } '."\n";
		fwrite($fh,$endtablestring);
	}
	mysql_select_db($cnvdb);
}

// PROCESS CNV INFO FOR CNV TABLE
$cnvtablestring = '';
$subtitlestring = '\subsection*{\textsl{\underline{Copy Number Variations}}}'."\n";
$cnvtablestring .= $subtitlestring;
//fwrite($fh, $subtitlestring);
$headerline = '';
$columns = 'l';  # at least one for copynumber

if (isset($_POST['genomic'])) {
	$columns .= 'l';
	$headerline .= '\textbf{Minimal Region} & ';
}
if (isset($_POST['maxgenomic'])) {
	$columns .= 'l';
	$headerline .= '\textbf{Maximal Region} & ';
}
if (isset($_POST['cytoband'])) {
	$columns .= 'l';
	$headerline .= '\textbf{CytoBand} & ';
}
$headerline .= ' \textbf{CN} & ';

if (isset($_POST['size'])) {
	$columns .= 'l';
	$headerline .= ' \textbf{Min. Size} & ';
}
if (isset($_POST['maxsize'])) {
	$columns .= 'l';
	$headerline .= ' \textbf{Max. Size} & ';
}
if (isset($_POST['nrprobes'])) {
	$columns .= 'l';
	$headerline .= ' \textbf{NrProbes} & ';
}
if (isset($_POST['inheritance']) ){
	$columns .= 'l';
	$headerline .= ' \textbf{Inh.} & '; 
}
if (isset($_POST['DC'])) {
	$columns .= 'l';
	$headerline .= ' \textbf{D.C.} & ';
}
if (isset($_POST['scores'])) {
	$columns .= 'l';
	$headerline .= ' \textbf{Confidence} & ';
}
$nrcolumns = strlen($columns);
$headerline = substr($headerline,0,-2);
$headerline .= '\\\\ '. "\n";
// Starting long table for CNV entries
// First the headers and footers
$tableheadstring = '\begin{longtable}[l]{@{\extracolsep{\fill}}'.$columns.'}'."\n" . '\hline' . "\n";
$cnvtablestring .= $tableheadstring;
$cnvtablestring .= $headerline;
$endhead = '\hline' . "\n" .'\endhead'."\n";
$cnvtablestring .= $endhead;
$longfooter = '\hline \multicolumn{'. "$nrcolumns" .'}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
$longfooter .= '\endfoot' . "\n";
$longfooter .= "\n";
$longfooter .= '\endlastfoot' . "\n";
$cnvtablestring .= $longfooter;


// FILL TABLE
$morbidstring = '';
$aids = $_POST['include'];
foreach( $aids as $key => $value) {
	$checkaids[$value] = 1;
	#echo "$key => $value<br>\n"; 
}
$morbidsarray = array();

$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.size, a.cn, a.nrgenes, a.nrprobes, a.confidence, a.inheritance, a.class, a.largestart, a.largestop FROM cus_aberration a WHERE a.sample = '$sample' AND a.idproj = $pid ORDER BY a.chr, a.start");
$manualhide = 0;
$classhide = 0;
$confhide = 0;
$pcnvhide = 0;
$snphide = 0;
$sizehide = 0;
$morbidsmall = 0;
$morbidlarge = 0;
$genesmall = 0;
$genelarge = 0;
$genesnotshown = 0;
$notlistedcnv = 0;
$missedmorbid = 0;
$totalcnv = mysql_num_rows($query);
while ($row = mysql_fetch_array($query)) {
	$line = '';
	$aid = $row['id'];
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$size = $row['size'];
	$cn = $row['cn'];
	$cnstring = $cn;
	if (preg_match("/gain/i",$cn)) {
		$cn = 3;
	}
	elseif (preg_match("/los/i",$cn)) {
		$cn = 1;
	}
	elseif (preg_match("/loh/i",$cn)) {
		$cn = 2;
	}

	$nrgenes = $row['nrgenes'];
	$nrprobes = $row['nrprobes'];
	$confidence = $row['confidence'];
	$inh = $row['inheritance'];
	$class = $row['class'];
	$largestart = $row['largestart'];
	if ($largestart == 'ter') {
		$largestartvalue = 0;
		$largestartstring = 'ter';
	}
	elseif ($largestart != '') {
		$largestartvalue = $largestart;
		$largestartstring = number_format($largestart,0,'',',');
	}
	else {
		$largestartvalue= '';
		$largestartstring = '';
	}	
	$largestop = $row['largestop'];
	if ($largestop == 'ter') {
		$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
		$cendrow = mysql_fetch_array($cendquery);
		$largestopvalue = $cendrow['stop'];
		$largestopstring = 'ter';
	}
	elseif ($largestop != '') {
		$largestopvalue = $largestop;
		$largestopstring = number_format($largestop,0,'',',');
	}
	else {
		$largestopstring = '';
		$largestopstring = '';
	}

	$largesize = $largestopvalue - $largestartvalue +1;
	# filter on class
	if ($maxclass != '' && ($maxclass < $class || $class == '')) {
		$classhide++;
		continue;
	}
	# filter on coverage
	if ($nrprobes != '' && $nrprobes < $minprobe) {
		$snphide++;
		continue;
	}
	# filter on size
	if ($size < $minsize) {
		$sizehide++;
		continue;
	}
	# manual filtering
	if ($checkaids[$aid] != 1) {
		$manualhide++;
		continue;
	}

	if (isset($_POST['genomic'])) {
		$region = "Chr" . $chromhash[$chr] . ":". number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');	
		$line .= $region .' & ';
	}
	if (isset($_POST['maxgenomic'])) {
		$maxregion = "Chr" . $chromhash[$chr] . ":". $largestartstring . '-' . $largestopstring;
		$line .= $maxregion .' & ';
	}
	if (isset($_POST['cytoband'])) {
		// GET KARYO
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostart = $cytorow['name'];
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$stop' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostop = $cytorow['name'];
		if ($cytostart == $cytostop) {
			$cytoband = "$chr$cytostart";
		}
		else {
			$cytoband = "$chr$cytostart-$cytostop";
		} 
		$line .= $cytoband . ' & ';
	}
	$line .= $cnstring .' & ';
	if (isset($_POST['size'])) {
		$line .= number_format($size,0,'',',') . ' & ';
	}
	if (isset($_POST['maxsize'])) {
		$line .= number_format($largesize,0,'',',') . ' & ';
	}
	if (isset($_POST['nrprobes'])) {
		$line .= $nrprobes . ' & ';
	}
	if (isset($_POST['inheritance'])) {
		$line .= $inharray[$inh] . ' & ';
	}
	if (isset($_POST['DC'])) {
		$line .= $class . ' & ';
	}
	if (isset($_POST['scores'])) {
	    #if ($seenby == '') {
		$line .= $confidence . ' &';
	    #}
	}
	// add line to the table
	$line = substr($line, 0, -2);
	$line .= '\\\\'. "\n";
	$cnvtablestring .= $line;
	// ISCN NOTATION
	// remarks: 
	//   - iscn uses pter and qter in banding info (4.3.2.1 in version 2009)
	//   - sex chromosome abberations come first
	//   - only class 1-2 are included in the ISCN notation.
	//   - only CNVs listed in the report are considered (filter-dependend!)
	if ($class == 1 || $class == 2) {
		$iscnpart = $iscnband . "(";
		if ($largestartstring =='ter') {
			$iscnpart .= 'ter'. "-" ;
		}
		else {
			$iscnpart .= $largestartstring;
			if ($chr > 22 && $gender == 'Male') {
				$iscnpart .= "x1,";
			}
			else {
				$iscnpart .= "x2,";
			}
			$iscnpart .= number_format($start,0,'',',') . "-";
		}
		if ($largestopstring == 'ter') {
			$iscnpart .= "terx$cn)";
		}
		else {
			$iscnpart .= number_format($stop,0,'',',')."x$cn";
			$iscnpart .= ",$largestopstring";
			if ($chr > 22 && $gender == 'Male') {
				$iscnpart .= "x1)";
			}
			else {
				$iscnpart .= "x2)";
			}
		}
		if ($inh > 0) {
			$iscnpart .= " ".$iscninh[$inh];
		}
		$iscnpart .= ",";
		if ($chr > 22) {
			$iscnxy .= $iscnpart;
		}
		else {
			$iscn .= $iscnpart;
		}
	}
	// SET SESSION VAR FOR KARYO PLOT
	$abstoplot[$chr][] = "$start|$stop|$cn"; 

	// collect genes
	
	if ($_POST['genes_'.$aid] == 'A') {
		$geneline = '';
		$printgenes = $_POST['genes_'.$aid];
		$nrcols = strlen($columns);
		$col = 0;
		$genesfound = 0;
		if ($nrgenes > 0) {
			$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
			$gquery = mysql_query("SELECT symbol, morbidTXT FROM genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");	
			$numgenes = mysql_num_rows($gquery);
			$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
			while ($grow = mysql_fetch_array($gquery)) {
				$genesfound++;
				$smallgene++;
				$col++;
				$symbol = $grow['symbol'];
				$symbol = str_replace('_','\_',$symbol);
				if ($col == 1) {
					$geneline .= '\hspace{1.5cm}';
				}
				if ($grow['morbidTXT'] != '' && isset($_POST['morbid'])) {
					$morbidsmall++;
					$geneline .= '{\footnotesize \textbf{\textcolor{darkgrey}{' . $symbol .'}}}' ;
					$morbidtxt = $grow['morbidTXT'];
					// strip final pipe
					$morbidtxt = preg_replace('/^(.*)(\|\s*)$/','$1',$morbidtxt);
					
					// split morbid in pipe
					$morbidtxt = str_replace('|',' \newline \textbullet ',$morbidtxt);
					// strip brackets
					$morbidtxt = str_replace(array('{','}','[',']'),'',$morbidtxt);
					// this strip of leading newline is not needed  
					//$morbidtxt = preg_replace('/^(\s\\newline)(.*)/','$2',$morbidtxt);
					if (!in_array($symbol,$morbidsarray)) {
						$morbidsarray[$symbol] = $symbol . ' & \textbullet ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
						//$csvmorbidline .= "$symbol;$csvmorbidtxt\n";
					}

					$morbidline .= $symbol . ' & ' . $morbidtxt . '\\\\' ."\n";
				}
				else {
					$geneline .= '{\tt{\footnotesize \textcolor{grey}{ '.$symbol.'}}}' ;
				}
				if ($col == 9) {
					$geneline .= ' \\\\ ' ."\n"; 
					$col = 0;
				}
				else {
					$geneline .= ' & ';
				}
			}
		}
		
		if ($largestart != '' && $largestop != '' && isset($_POST['genelarge'])) {
			$largegenes = mysql_query("SELECT symbol, morbidTXT FROM genesum WHERE chr=$chr AND ( (stop BETWEEN $largestart AND $start) OR (start BETWEEN $stop AND $largestop)) ORDER BY symbol");
			$numlgenes = mysql_num_rows($largegenes);
			if ($numlgenes > 0 && $genesfound == 0) {
				$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
				$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
			}	
			while ($lrow = mysql_fetch_array($largegenes)) {
				$genesfound++;
				$largegene++;
				$col++;
				$symbol = $lrow['symbol'];
				$symbol = str_replace('_','\_',$symbol);
				if ($col == 1) {
					$geneline .= '\hspace{1.5cm}';
				}
				if ($lrow['morbidTXT'] != '' && isset($_POST['morbid'])) {
					$morbidlarge++;
					$geneline .= '{\footnotesize \textbf{\textsl{\textcolor{darkgrey}{' . $symbol .'}}}}' ;
					$morbidtxt = $lrow['morbidTXT'];
					// strip final pipe
					$morbidtxt = preg_replace('/^(.*)(\|\s*)$/','$1',$morbidtxt);
					// split on pipe
					$morbidtxt = str_replace('|',' \newline \textbullet ',$morbidtxt);
					// strip brackets
					$morbidtxt = str_replace(array('{','}','[',']'),'',$morbidtxt);
					//$morbidtxt = preg_replace('/^(\ \\newline)(.*)/','$2',$morbidtxt);
					//$morbidline .= '\textsl{'.$symbol . '} & ' . $morbidtxt . '\\\\' ."\n";
					if (!in_array($symbol,$morbidsarray)) {
						$morbidsarray[$symbol] = '\textsl{'. $symbol . '} ' . ' & \textbullet ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
						//$csvmorbidline .= "$symbol;$csvmorbidtxt\n";
					}

				}
				else {
					$geneline .= '{\tt{\footnotesize \textsl{\textcolor{grey}{'.$symbol.'}}}}' ;
				}
				if ($col == 9) {
					$geneline .= ' \\\\ ' ."\n"; 
					$col = 0;
				}
				else {
					$geneline .= ' & ';
	
				}
			}
		}
		
		if ($genesfound > 0) {
			$col++;
			while ($col < 9 && $col != 1) {
				$geneline .= ' &  ';
				$col++;
			}
			if ($col == 9) {
				$geneline .= '\\\\'. "\n";
			}
			$geneline .= '\end{tabular}'."\n".  '\end{minipage}'  . "\n";
			$geneline .= ' } \\\\ '."\n ";	
			$cnvtablestring .= $geneline;
		}
		
	}
	
	else {
		$genesnotshown += $nrgenes;
		$notlistedcnv++;
		$mq = mysql_query("SELECT symbol FROM genesum WHERE morbidID IS NOT NULL AND chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ");
		$missedmorbid += mysql_num_rows($mq);
		
		
	}
	
	
} 
$endhead = '\hline' . "\n" ; 
$endhead .= '\end{longtable}'."\n";
$cnvtablestring .= $endhead;

// PRINT ISCN NOTATION
if ($iscn != '' || $iscnxy != '') {
	$iscn = "arr $iscnxy$iscn";
	$iscn = substr($iscn, 0,-1);
	$subtitlestring = '\subsection*{\textsl{\underline{ISCN Notation}}}'."\n";
	$string = 'Note: ISCN notation is composed of class 1 and class 2 aberrations.'."\n\n";
	fwrite($fh,$subtitlestring);
	//fwrite($fh,$string);
	fwrite($fh,'{\small ');
	fwrite($fh,$iscn);
	fwrite($fh,' } '."\n");
}

// PRINT FILTER OVERVIEW TABLE
$subtitlestring = '\subsection*{\textsl{\underline{CNV Statistics}}}'."\n";
fwrite($fh,$subtitlestring);
$filter = '\begin{tabular}{p{0.4\textwidth}p{0.3\textwidth}p{0.27\textwidth} }' ."\n";
$filter .= '\hline' . "\n" .'Filter Item & Treshold & Filtered CNVs \\\\ ' ."\n";
$filter .= '\hline' . "\n";
if ($maxclass == '') {
	$filter .= 'Shown diagnostic classes & All (including non specified) & 0 \\\\ '. "\n";
}
elseif ($maxclass ==4) {
	$filter .= 'Shown diagnostic classes & Class 1 to 4  & ' . $classhide . ' \\\\ '. "\n";
}
elseif ($maxclass == 3) {
	$filter .= 'Shown diagnostic classes & Class 1 to 3  & ' . $classhide . ' \\\\ '. "\n";
}
elseif ($maxclass == 2) {
	$filter .= 'Shown diagnostic classes & Class 1 to 2  &' . $classhide . ' \\\\ '. "\n";
}
elseif ($maxclass == 1) {
	$filter .= 'Shown diagnostic classes & Class 1 only &' . $classhide . ' \\\\ '. "\n";
}

$filter .= 'Minimal Coverage & ' . $minprobe ." & $snphide "  . '\\\\' . "\n";
$filter .= 'Minimal CNV size {\footnotesize (in bp)} & ' . $minsize ." & $sizehide " . '\\\\'."\n";
$filter .= 'Minimal Confidence Score & ' . $minqs ."& $confhide "  . '\\\\' . "\n";
#$filter .= 'Minimal PennCNV Score & ' . $minps ."& $pcnvhide "  . '\\\\' . "\n";
if ($manualhide > 0) {
	$filter .= 'Manually Hided CNVs & &' . $manualhide . '\\\\' . "\n";
}
$filter .= '\hline' ."\n";
$showncnv = $totalcnv - $classhide - $snphide -$sizehide - $confhide - $manualhide;
$filter .= '\multicolumn{2}{r}{{\textsl{Shown CNVs:}}} & \textsl{'.$showncnv.' out of ' . $totalcnv .'} \\\\' ."\n";
$filter .= '\hline'."\n".'\end{tabular}'."\n";
fwrite($fh,$filter);

// PRINT AFFECTED GENE STATS

$subtitlestring = '\subsection*{\textsl{\underline{Gene Statistics}}}'."\n";
fwrite($fh,$subtitlestring);
$genetable = ' \begin{tabular}{p{0.4\textwidth}p{0.3\textwidth}p{0.27\textwidth}}' ."\n";
$genetable .= '\hline' . "\n" .'Description & Total Amount of Genes & MORBID entries {\footnotesize  \textsl{(bold)}}\\\\ ' ."\n";
$genetable .= '\hline' . "\n";
$genetable .= 'In Minimal Region & ' . $smallgene . ' & ' . $morbidsmall .' \\\\' ."\n";
$genetable .= 'In Maximal Region {\footnotesize \textsl{(slanted)}} & ' . $largegene.' & '. $morbidlarge . '\\\\'."\n";
if ($genesnotshown > 0) {
	$genetable .= 'Non Listed Genes {\footnotesize \textsl{(from ' .$notlistedcnv . ' listed regions)}} & ' . $genesnotshown . ' & '.$missedmorbid .'\\\\ ' . "\n";
}

$genetable .= '\hline'."\n";
$totalgenes = $smallgene + $largegene + $genesnotshown;
$totalmorbid = $morbidsmall + $morbidlarge + $morbidmissed;
$genetable .= '\textsl{Total} & \textsl{'. $totalgenes . '} & \textsl{' . $totalmorbid . '} \\\\ ' . "\n";
$genetable .= '\hline ' . "\n";
$genetable .= '\end{tabular}'."\n";
fwrite($fh,$genetable);

// PRINT KARYOGRAM
echo "<li>Create Karyogram</li>";
flush();

if ($_POST['inckaryo'] == 1) {
	$karyocustom = 1;
	include('karyoreport.php');
	$karyostring = '';
	$karyostring .= '\subsection*{\textsl{\underline{Karyogram}}}'."\n";
	$karyostring .= '\includegraphics[width=0.90\textwidth]{'.$samplename.'.png}'."\n";
	fwrite($fh,$karyostring);
}


// PRINT MORBID GENES IF FOUND
if (count($morbidsarray) > 0) {
//if ($morbidline != '') {
	// sort array
	ksort($morbidsarray);
	$subtitlestring = '\subsection*{\textsl{\underline{MORBID Gene Summaries}}}'."\n";
	//fwrite($fh,$subtitlestring);
	//$csvout .= "\nMORBID Gene information\n";
	//$csvout .= $csvmorbidline;
	$morbidtable .= $subtitlestring;
	$morbidtable .= '\begin{longtable}[l]{p{0.3\textwidth} p{0.7\textwidth} }' ."\n";
	$morbidtable .= '\hline' . "\n" .'Gene Symbol & MORBID Summary \\\\ ' ."\n";
	$morbidtable .= '\hline' . "\n". '\endhead' . "\n";
	$morbidtable .= '\hline \multicolumn{2}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
	$morbidtable .= '\endfoot' . "\n";
	$morbidtable .= "\n";
	$morbidtable .= '\endlastfoot' . "\n";
	foreach ($morbidsarray as $symbol => $entry) {
		//$morbidtable .= $morbidline;
		$morbidtable .= $entry;
	}
	$morbidtable .= '\hline'."\n".'\end{longtable}'."\n";
	//fwrite($fh,$morbidtable);
	//$pdfout .= $morbidtable;
	fwrite($fh,$morbidtable);
}
/*
if ($morbidline != '') {
	$subtitlestring = '\subsection*{\textsl{\underline{MORBID Gene Summaries}}}'."\n";
	fwrite($fh,$subtitlestring);
	$morbidtable = '\begin{longtable}[l]{p{0.3\textwidth} p{0.7\textwidth} }' ."\n";
	$morbidtable .= '\hline' . "\n" .'Gene Symbol & MORBID Summary \\\\ ' ."\n";
	$morbidtable .= '\hline' . "\n". '\endhead' . "\n";
	$morbidtable .= '\hline \multicolumn{2}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
	$morbidtable .= '\endfoot' . "\n";
	$morbidtable .= "\n";
	$morbidtable .= '\endlastfoot' . "\n";
	$morbidtable .= $morbidline;
	$morbidtable .= '\hline'."\n".'\end{longtable}'."\n";
	fwrite($fh,$morbidtable);
}
*/
// PRINT LOG IF SELECTED
if ($_POST['inclog'] == 1) {
	$logstring = '';
	$history = mysql_query("SELECT l.aid, l.time, u.FirstName, u.LastName, l.entry, l.arguments FROM cus_log l JOIN users u ON u.id = l.uid WHERE l.sid = '$sid' AND l.pid = '$pid' ORDER BY l.time DESC");	
	while ($row = mysql_fetch_array($history))  {
		$aid = $row['aid'];
		$sq = mysql_query("SELECT chr, start, stop, cn FROM cus_aberration WHERE id = '$aid'");
		$srows = mysql_num_rows($sq);
		if ($srows > 0) {
			$srow = mysql_fetch_array($sq);
			$chr = $srow['chr'];
			$chrtxt = $chromhash[$chr];
			$start = $srow['start'];
			$starttxt = number_format($start,0,'',',');
			$stop = $srow['stop'];
			$stoptxt = number_format($stop,0,'',',');
			$cn = $srow['cn'];
		}
		else {
			$ssq = mysql_query("SELECT chr, start, stop, cn FROM cus_deletedcnvs WHERE id = '$aid'");
			$srow = mysql_fetch_array($ssq);
			$chr = $srow['chr'];
			$chrtxt = $chromhash[$chr];
			$start = $srow['start'];
			$starttxt = number_format($start,0,'',',');
			$stop = $srow['stop'];
			$stoptxt = number_format($stop,0,'',',');
			$cn = $srow['cn'];
		}
		$time = $row['time'];
		$FName = $row['FirstName'];
		$LName = $row['LastName'];
		$logentry = $row['entry'];
		$arguments = $row['arguments'];
		$logstring .= '- \textsl{'.$time.' : }Chr'.$chrtxt.':'.$starttxt.'-'.$stoptxt.' (cn:'.$cn.') : '.$logentry.' by '.$FName.' '.$LName;
		if ($arguments != '') {
			$logstring.= ' : '.$arguments .'\\\\'."\n";
		}
		else {
			$logstring .= '\\\\' ."\n";
		}
	}
	if ($logstring != '') {
		$subtitlestring = '\subsection*{\textsl{\underline{Annotation Log}}}'."\n";
		fwrite($fh,$subtitlestring);
		fwrite($fh,'{\scriptsize '.$logstring. ' }'."\n");
	}
}

// PRINT CNV TABLE AFTER MORBID !
$cnvtablestring = '\begin{landscape}'."\n" . $cnvtablestring . '\label{endofdoc}'."\n".'\end{landscape}'."\n";

fwrite($fh,$cnvtablestring);

fwrite($fh, '\end{document}');
fclose($fh);
echo "<li> Compiling LaTeX document</li>\n";
system("(chmod 777 '$sitedir/Reports/$samplename.tex' && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $sitedir/Reports && perl compiletex.pl '$samplename' && chmod 777 '$sitedir/Reports/$samplename.pdf' \" ) > /dev/null 2>&1 ");
echo "<li> Opening PDF document</li>\n";
echo "<li> Done. <a href=\"index.php?page=cusdetails&amp;project=$pid&amp;sample=$samplename\">Go Back to the sample details</a></li>\n";
echo "<iframe height='0' width='0' src='download.php?path=Reports&file=$samplename.pdf'></iframe>\n";
?>

</div>
