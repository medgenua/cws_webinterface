<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inhhash = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// GET VARS
$sample = $_POST['sid'];
$sid = $sample;
$pid = $_POST['pid'];

//GET Format options
$includescores = 1;
$includegenes = 1;
$query = mysql_query("SELECT DECODE(clinical, '$encpass') AS clinical FROM cus_sample WHERE idsamp = '$sample' AND idproj = '$pid'");
$row = mysql_fetch_array($query);
$experimental = "Experimental Comments";
$clinicaldata = $row['clinical'];
if ($clinicaldata == '') {
	$clinicaldata = "Clinical Information";
}	
ob_flush();
flush();

?>
<script type="text/javascript" src="javascripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	theme_advanced_buttons1 : "bold,italic,underline,|,bullist,numlist,|,undo,redo",
	theme_advanced_buttons2 : "",
	theme_advanced_buttons3 : "",
	theme_advanced_buttons4 : "",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "none",
	theme_advanced_resizing : true,
});
</script>
<script type="text/javascript">
function GeneSet(setting) 
{ 
	for(i=0; i<document.pickdetails.elements.length; i++){
		var myName = document.pickdetails.elements[i].name;
		if (myName.match(/genes_/) ) {
			var myValue = document.pickdetails.elements[i].value;
			if (myValue == setting) {
				document.pickdetails.elements[i].checked = true;
			}
		}
   	}

}

function ShowSet()
{
	if (document.pickdetails.showtoggle.checked == 1) {
		var newvalue = 1;
	}
	else {
		var newvalue = 0;
	}
	for (i=0; i<document.pickdetails.elements.length; i++) {
        	if (document.pickdetails.elements[i].name=='include[]')
            			document.pickdetails.elements[i].checked = newvalue;
    	}
}
</script>

<div class=sectie>
<h3>Configure Report for sample <?php echo "$sample"; ?></h3>
<p>Please specify the information you want to include in the report. Comments and clinical information will not be shown when left blank or default. You can specify three options for listing affected genes. 'N(one)' lists no genes, 'A(ll)' lists all genes and 'P(rioritized)' lists only genes that were significantly prioritized. These settings can be changed on a per aberration level.</p>
</div>

<div class=sectie>
<form name="pickdetails" action='index.php?page=cusgetreport' method='POST' >
<input type=hidden name=sid value='<?php echo $sample;?> '>
<input type=hidden name=pid value=<?php echo $pid; ?>>

<p><span class=nadruk>1/ Include addtional comments:</span> <input type=radio name=inccom value=1 checked> Yes / <input type=radio name=inccom value=0>No</p>

<div style='margin-left:10px'>
  <TEXTAREA name="comments" rows=10 style='width:95%' onfocus="if (this.value == 'Experimental Comments') {this.value = '';}"><?php echo "$experimental"; ?></TEXTAREA>
</div>
<p><span class=nadruk>2a/ Include clinical information:</span> <input type=radio name=incclin value=1 checked> Yes / <input type=radio name=incclin value=0>No</p>
<div style='margin-left:10px'>
  <TEXTAREA name="clinical" rows="10" style='width:95%' onfocus="if (this.value == 'Clinical Information') {this.value = '';}"><?php echo "$clinicaldata";?></TEXTAREA>
</div>
<p><span class=nadruk>2b/ Include ontology based clinical information:</span> <input type=radio name=incclinontology value=1 checked> Yes / <input type=radio name=incclinontology value=0>No</p>
<div style='margin-left:10px'>
<?php 
$subarray = array();
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
$lddb = "LNDB";
mysql_select_db($cnvdb);
$subquery = mysql_query("SELECT lddbcode FROM cus_sample_phenotypes WHERE sid = '$sid' AND pid = '$pid' ORDER BY lddbcode");
while ($subrow = mysql_fetch_array($subquery)) {
	$code = $subrow['lddbcode'];
	$pieces = explode('.',$code);
	$subarray[$pieces[0]][$pieces[1]][] = $pieces[2];
}
mysql_select_db($lddb);
$subtablestring = '';
$match = 0;
foreach($subarray as $first => $sarray) {
	$ssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
	$ssubrow = mysql_fetch_array($ssubquery);
	$firstlabel = $ssubrow['LABEL'];
	foreach ($sarray as $second => $tarray) {
		if ($second == '00') {
			$subtablestring .= "<tr><td $firstcell>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
		}
		else {
			$sssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
			$sssubrow = mysql_fetch_array($sssubquery);
			$secondlabel = $sssubrow['LABEL'];
			foreach ($tarray as $third) {
				if ($third == '00') {
					$subtablestring .= "<tr><td $firstcell>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>";	
				}
				else {
					$ssssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
					$ssssubrow = mysql_fetch_array($ssssubquery);
					$thirdlabel = $ssssubrow['LABEL'];
					$subtablestring.= "<tr><td $firstcell>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>";
				}	
			}
		}
	}
}
echo "<table cellspacing=0 >";
echo "<tr><th $firstcell class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
echo "</tr>";
echo $subtablestring;
echo "</table>";
mysql_select_db($cnvdb);
?>

</div>
<p><span class=nadruk>3/ Show Karyogram:</span> <input type=radio name=inckaryo value=1 checked> Yes / <input type=radio name=inckaryo value=0>No<br/>
<span style='padding-left:10px'>The karyogram shows all chromosomes with markings for all aberrations included in the report. </span><br/><span style='padding-left:10px'>The same colorcode is used as seen elsewhere on the platform.</span></p>

<p><span class=nadruk>4/ Show Log:</span> <input type=radio name=inclog value=1 > Yes / <input type=radio name=inclog value=0 checked>No<br/>
<span style='padding-left:10px'>All changes to CNV information is stored. If selected, these changes will be added to the end of the report.</span>


<p><span class=nadruk>5/ Select Details to Show in CNV Table: </span></p>
<p>
<ul id=ul-simple>
 <li><input type=checkbox name=genomic checked>Minimal Genomic Region
 <li><input type=checkbox name=maxgenomic >Maximal Genomic Region 
 <li><input type=checkbox name=cytoband checked>Cytoband
 <li><input type=checkbox name=size checked>Minimal Genomic Size
 <li><input type=checkbox name=maxsize >Maximal Genomic Size
 <li><input type=checkbox name=nrprobes >Number of probes
 <li><input type=checkbox name=scores >Confidence scores
 <!-- <li><input type=checkbox name=genes checked>Affected Genes -->
 <li><input type=checkbox name=inheritance checked>Inheritance (if defined)
 <li><input type=checkbox name=DC checked>Diagnostic Class (if defined)
</ul>
</p>
<p><span class=nadruk>6/ Gene List Settings</span></p>
<ul id=ul-simple>
 <li><input type=checkbox name=morbid checked>Include MORBID summary for listed genes (will be bold faced in list)
 <li><input type=checkbox name=genelarge >Include genes only present in maximal region (will be slanted in list)
</ul>
</p>

<p><span class=nadruk>4/ Filter Results</span></p>
<p>
<table cellspacing=0>
<tr >
  <td class=clear><select name=maxclass>
	<option value='' selected>All (including non-specified)</option>
	<option value=1>Only Class 1</option>
	<option value=2>Class 1-2</option>
	<option value=3>Class 1-3</option>
	<option value=4>Class 1-4</option>
 </select></td><td class=clear> : On Diagnostic Class </td>
</tr>
<input type=hidden name=minprobe value=0>
<tr >
 <td class=clear><input type=text name=minsize value=0></td><td class=clear> : On Minimal Size (in bp)</td>
</tr>
<tr >
 <td class=clear><input type=text name=minconf value=0></td><td class=clear> : On Minimal Confidence </td>
</tr>

</table>
</p>

<p><span class=nadruk>8/ Set CNV specific Details:</span></p>
<div style='margin-left:10px'>
<p>Besides filtering based on the settings above, single CNVs can be excluded by deselecting them below.</p>
<p>
<table cellspacing=0>
<tr>
 <th <?php echo $firstcell; ?>><input type=checkbox name=showtoggle checked onclick="ShowSet()">Include</th>
 <th>Region</th>
 <th>CN</th>
 <th>Inh./D.C.</th>
 <th>Genes:<input type=radio name=genes value=N onClick="GeneSet('N')">N<input type=radio name=genes value=A checked onclick="GeneSet('A')">A</th>
</tr>

<?php 
ob_flush();
flush();
$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.cn, a.nrgenes, a.inheritance, a.class FROM cus_aberration a WHERE a.sample = '$sample' AND a.idproj = '$pid' ORDER BY a.chr, a.start");
while ($row = mysql_fetch_array($query)) {
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$cn = $row['cn'];
	$aid = $row['id'];
	$inh = $row['inheritance'];
	$inh = $inhhash[$inh];
	$class = $row['class'];
	echo "<tr>\n";
	echo " <td $firstcell><input type=checkbox name=include[] value=$aid checked></td>\n";
	echo " <td >chr$chr:$start-$stop</td>\n";
	echo " <td>$cn</td>\n";
	if ($class == '' || $class == 0) {
		$class = '-';
	}
	elseif ($class == 5) {
		$class = 'FP';
	}
	echo " <td>$inh / $class</td>\n";
	echo " <td><input type=radio name=genes_$aid value=N>N<input type=radio name=genes_$aid value=A checked>A</td>\n";
	echo "</tr>";
}
?>


</table>
</p>
<p><input type=submit class=button value='Create Report'></p>


</div>


<?php 

}
?>




