<?php 
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}

if ($_GET['type'] == 'main' || $_GET['type'] == '' ) {
	echo "<div class=sectie>\n";
	echo "<h3>Manage Data From Custom Sources</h3>\n";
	echo "<p>If you have none Illumina based data you'd like to browse using this website, or add to the database as a reference, you can do it here. \n";
	echo "It is done using a flexible import tool of custom CNV reports.  When there is no support for your platform yet, please create a parser first. </p>\n";
	echo "<p>After uploading your data, please choose wether you want the sample to be included in your search results.</p>\n";
	echo "</div>\n";
}
elseif ($_GET['type'] == 'cp') {
	include('inc_create_parser.inc');
}
elseif ($_GET['type'] == 'otf') {
	include('inc_onthefly.inc');
}
elseif ($_GET['type'] == 'st') {
	include('inc_custom_store.inc');
}
elseif ($_GET['type'] == 'tree') {
	include('inc_custom_tree.inc');
}
elseif ($_GET['type'] == 'custom') {
	include('page_projects.php');
}
elseif ($_GET['type'] == 'create') {
	include('inc_custom_create_sample.inc');
}
