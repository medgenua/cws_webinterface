<?php
// layout vars
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// check login
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Share your projects</h3>\n";
	include('login.php');
	echo "</div>\n";
}
else {

// get pid 
$pid = $_POST['pid'];
if ($pid == '') {
	$pid = $_GET['pid'];
}

if ($pid != '' && !isset($_POST['share']) && !isset($_POST['finalise'])) {
	// get project details
	$query = mysql_query("SELECT naam, userID FROM cus_project WHERE id = '$pid'");
	$row = mysql_fetch_array($query);
	$pname = $row['naam'];
	$pown = $row['userID'];
	if ($pown != $userid) {
		echo "<div class=sectie>\n";
		echo "<h3>Share Project</h3>\n";
		echo "<h4>project: $pname</h4>\n";
		echo "<p>Projects can only be shared by the user who created them. </p>\n";
	}	
	else {

		echo "<div class=sectie>\n";
		echo "<h3>Share Project</h3>\n";
		echo "<h4>project: $pname</h4>\n";
		echo "<p>Please select the usergroups and/or users you want to share this project with from the lists below and press 'Share'.</p>\n";
		echo "<p>\n";
		echo "<form action='index.php?page=customshareprojects' method=POST>\n";
		echo" <input type=hidden name=pid value='$pid'></p>\n";
		
		// get groups with access
		$query = mysql_query("SELECT ug.id, ug.name, ug.editcnv, ug.editclinic FROM usergroups ug JOIN cus_projectpermissiongroup ppg ON ug.id = ppg.groupid WHERE ppg.projectid = '$pid'");
		echo "<ol>\n";
		$gwa = array(); // groups with access
		while ($row = mysql_fetch_array($query)) {
			$ugname = $row['name'];
			$ugid = $row['id'];
			$ugcnv = $row['editcnv'];
			$ugclin = $row['editclinic'];
			$gwa[$ugid] = "$ugname@@@$ugcnv@@@$ugclin";
		}
		// list groups without access
		echo "<p><table>\n";
		echo "<tr>\n";
		echo "<td class=clear valign=top>\n";
		echo "<span class=nadruk>Share With Usergroups</span><br>\n";
		echo "<select name='groups[]' size=10 MULTIPLE>\n";
		$query = mysql_query("SELECT ug.name, ug.id FROM usergroups ug JOIN usergroupuser ugu ON ug.id = ugu.gid WHERE ugu.uid = '$userid'");
		while ($row = mysql_fetch_array($query)) {
			$ugname = $row['name'];
			$ugid = $row['id'];
			if (!array_key_exists($ugid,$gwa)) {
				echo "<option value='$ugid'>$ugname</option>\n";
			}
			}
		echo "</select>\n";
		echo "</td>\n";
		// get seperate users with full access
		$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name, pp.editcnv, pp.editclinic FROM users u JOIN cus_projectpermission pp JOIN affiliation a ON u.Affiliation = a.id AND u.id = pp.userid WHERE pp.projectid = '$pid' ORDER BY a.id, u.LastName");
		$uwa = array();
		while ($row = mysql_fetch_array($query)) {
			$lname = $row['LastName'];
			$currid = $row['id'];
			$fname = $row['FirstName'];
			$affi = $row['name'];
			$editcnv = $row['editcnv'];
			$editclinic = $row['editclinic'];
			$uwa[$currid] = "$lname $fname@@@$affi@@@$editcnv@@@$editclinic";
		}
		// list users with no or restricted access
		echo "<td class=clear valign=top>\n";
		echo "<span class=nadruk>Share With Single Users</span><br>\n";
		echo "<select name='users[]' size=10 MULTIPLE>\n";
		$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY a.name, u.LastName");
		$currinst = '';
		while($row = mysql_fetch_array($query)) {
			$uid = $row['id'];
			// skip users with full access
			if (array_key_exists($uid, $uwa)) {
				$pieces = explode('@@@',$uwa[$uid]);
				if ($pieces[2] == 1 && $pieces[3] == 1) {
					continue;
				}
				}
			$lastname = $row['LastName'];
			$firstname = $row['FirstName'];
			$institute = $row['name'];
			if ($currinst != $institute) {
				if ($currinst != '') {
					echo "</optgroup>";
				}
				echo "<optgroup label='$institute'>\n";
				$currinst = $institute;
			}
			echo "<option value='$uid'>$lastname $firstname</option>\n";
		}
	
		echo "</select>\n";
		echo "</td>\n";
		echo "</tr>\n";
		echo "</table>\n";
		echo "<input type=submit class=button name=share value='Share'></p>\n";
		echo "</form>\n";
		echo "</div>\n";
		// show who has access now.
		echo "<div class=sectie>\n";
		echo "<h3>Currently Shared</h3>\n";
		echo "<h4>project: $pname</h4>\n";
		echo "<p>The project is currently accessible by the following users and usergroups. 'Full' means read-write access to CNV and clinical information. 'Read-only' means no information can be changed. 'CNV' and 'Clinic' mean that only CNV an Clinical information can be changed respectively.</p>\n";
		echo "You can remove their access by clicking on the garbage bin. Removing access from usergroup only prevents new users from gaining access to projects when joining the usergroup. Actual access to projects has to be removed on a per-user basis.</p>\n";
		echo "<p><table cellspacing=0 >\n";
		echo "<tr>\n";
		echo "<td class=clear valign=top>\n";
		echo "<span class=nadruk>Usergroups with access:</span><br>\n";
		echo "<ol>\n";
		foreach ($gwa as $gid => $value) {
			$values = explode('@@@',$value);
			$gname = $values[0];
			$editcnv = $values[1];
			$editclinic = $values[2];
			if ($editcnv == 1 && $editclinic == 1) {
				$perm = 'Full';
			}
			elseif ($editcnv == 1) {
				$perm = 'CNV';
			}
			elseif ($editclinic == 1) {
				$perm = 'Clinic';
			}
			else {
				$perm = 'Read-only';
			}

			echo "<li>$gname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?pid=$pid&u=$gid&type=group&from=customshareprojects&cstm=1\"><img src='images/content/delete.gif' width=8px height=8px></a></li>\n";
		}
		echo "</ol>\n";
		echo "</td>\n";
		// seperate users
		echo "<td class=clear valign=top $firstcell>\n";
		echo "<span class=nadruk>Users with access:</span><br>\n";
		$currinst = '';
		echo "<ul id=ulsimple>\n";
		foreach ($uwa as $uid => $value) {
			$pieces = explode('@@@',$value);
			$uname = $pieces[0];
			$institute = $pieces[1];
			$editcnv = $pieces[2];
			$editclinic = $pieces[3];
			// check institution
			if ($currinst != $institute) {
				if ($currinst != '') {
					echo "</ol>";
				}
				echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
				$currinst = $institute;
			}
			// check permissions
			if ($editcnv == 1 && $editclinic == 1) {
				$perm = 'Full';
			}
			elseif ($editcnv == 1) {
				$perm = 'CNV';
			}
			elseif ($editclinic == 1) {
				$perm = 'Clinic';
			}
			else {
				$perm = 'Read-only';
			}
			echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?pid=$pid&u=$uid&type=user&from=customshareprojects&cstm=1\"><img src='images/content/delete.gif' width=8px height=8px></a></li>\n";
		}
		echo "</ol></ul>\n";
		//echo "</select>\n";
		echo "</td>\n";
		echo "</tr>\n";
		echo "</table>\n";
	}

}
elseif ($pid != '' && isset($_POST['share'])) {
	// get project details
	$query = mysql_query("SELECT naam FROM cus_project WHERE id = '$pid'");
	$row = mysql_fetch_array($query);
	$pname = $row['naam'];

	// get posted vars
	$addgroups = $_POST['groups'];
	$addusers = $_POST['users'];
	echo "<div class=sectie>\n";
	echo" <h3>Project Shared</h3>\n";
	echo "<h4>project: $pname</h4>\n";
	// process groups
	if (count($addgroups) > 0) {
	
		echo "<p>Project shared with following usergroups:</p>\n";
		echo "<table cellspacing=0>\n";
		echo "<th $firstcell class=topcellalt>Group</th>\n";
		echo "<th class=topcellalt>Edit CNVs</th>\n";
		echo "<th class=topcellalt>Edit Clinic</th>\n";
		echo "</tr>\n";
		$yesno = array('Not Allowed','Allowed');
		foreach ($addgroups as $gid) {
			// get permission and other info on usergroup
			$query = mysql_query("SELECT name, editcnv, editclinic FROM usergroups WHERE id = '$gid'");
			$row = mysql_fetch_array($query); 
			$gname = $row['name'];
			$editcnv = $row['editcnv'];
			$editclinic = $row['editclinic'];
			// insert into permissions for group
			$query = mysql_query("INSERT INTO cus_projectpermissiongroup (projectid, groupid,sharedby) VALUES ('$pid', '$gid','$userid') ON DUPLICATE KEY UPDATE groupid = '$gid'");
			// update permissions for users in group
			$query = mysql_query("SELECT uid FROM usergroupuser WHERE gid = '$gid'");
			while ($row = mysql_fetch_array($query)) {
				$uid = $row['uid'];
				$insquery = mysql_query("INSERT INTO cus_projectpermission (projectid, userid, editcnv, editclinic) VALUES ('$pid','$uid', '$editcnv', '$editclinic') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '$editcnv', VALUES(editcnv),'$editcnv'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic')");
			}
			echo "<tr>\n";
			echo "<td $firstcell>$gname</td>\n";
			echo "<td>$yesno[$editcnv]</td>\n";
			echo "<td>$yesno[$editclinic]</td>\n";
			echo "</tr>\n";
			
		}
		echo "</table>\n";
		echo "</p>\n";
	}
	// process single users
	if (count($addusers) > 0) {
		
		echo "<p>Please specify the permissions for the users you want to share this project with, and press 'Finish'.</p>\n";
		echo "<form action='index.php?page=customshareprojects' method=POST>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=finalise value=1>\n";
		echo "<table cellspacing=0>\n";
		echo "<th $firstcell class=topcellalt>User</th>\n";
		echo "<th class=topcellalt>Edit CNVs</th>\n";
		echo "<th class=topcellalt>Edit Clinic</th>\n";
		echo "</tr>\n";
		foreach($addusers as $uid) {
			// get userinfo 
			$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$uid'");
			$row = mysql_fetch_array($query);
			$fname = $row['FirstName'];
			$lname = $row['LastName'];
			// print table row
			echo "<tr>\n";
			echo "<td $firstcell><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
			echo "<td><input type=radio name='cnv_$uid' value=1 checked> Yes <input type=radio name='cnv_$uid' value=0> No</td>\n";
			echo "<td><input type=radio name='clinic_$uid' value=1 checked> Yes <input type=radio name='clinic_$uid' value=0> No</td>\n";
			echo "</tr>\n";
		}
		echo "</table>\n";
		echo "</p>\n";
		echo "<input type=submit class=button name=next value='Finish'>\n";
		echo "</form>\n";
	}
	echo "<p><a href='index.php?page=customshareprojects&pid=$pid'>Go Back</a></p>\n";
	echo "</div>\n";
}
elseif ($pid != '' && isset($_POST['finalise'])) {
	// get project details
	$query = mysql_query("SELECT naam FROM cus_project WHERE id = '$pid'");
	$row = mysql_fetch_array($query);
	$pname = $row['naam'];
	echo "<div class=sectie>\n";
	echo" <h3>Project Shared</h3>\n";
	echo "<h4>project: $pname</h4>\n";

	// get vars: 
	$addusers = $_POST['users'];
	$yesno = array('Not Allowed','Allowed');
	// print table
	echo "<p>The following users gained access to the project.</p>\n";
	echo "<p><table cellspacing=0>\n";
	echo "<th $firstcell class=topcellalt>User</th>\n";
	echo "<th class=topcellalt>Edit CNVs</th>\n";
	echo "<th class=topcellalt>Edit Clinic</th>\n";
	echo "</tr>\n";

	foreach($addusers as $uid) {
		$editcnv = $_POST["cnv_$uid"];
		$editclinic = $_POST["clinic_$uid"];
		// user info
		$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$uid'");
		$row = mysql_fetch_array($query);
		$fname = $row['FirstName'];
		$lname = $row['LastName'];
		// print table row
		echo "<tr>\n";
		echo "<td $firstcell>$lname $fname</td>\n";
		echo "<td>$yesno[$editcnv]</td>\n";
		echo "<td>$yesno[$editclinic]</td>\n";
		echo "</tr>\n";
		$insquery = mysql_query("INSERT INTO cus_projectpermission (projectid, userid, editcnv, editclinic) VALUES ('$pid','$uid', '$editcnv', '$editclinic') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '$editcnv', VALUES(editcnv),'$editcnv'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic')");

	}
	echo "<p><a href='index.php?page=customshareprojects&pid=$pid'>Go Back</a></p>\n";	
	echo "</div>\n";
}


// end of check login
}
?>
