<?php

/////////////////////////// //////
// DATA STATISTICS              // 
//  1. GENERAL SAMPLE OVERVIEWS //
//  2. GENERAL CNV OVERVIEWS    //
//  2. QUERY BASED GRAPHS       //
//////////////////////////// /////


// this is the portal. all files are in subdir datastats.
if (!isset($_GET['type']) ||$_GET['type'] == '' || $_GET['type'] == '1') {
	include('datastats/inc_samplestats.inc');
}
elseif ($_GET['type'] == 2) {
	include('datastats/inc_cnvstats.inc');
}
else {
	include('datastats/inc_queried.inc');
}
?>
