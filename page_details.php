<div id="txtHint"></div>

<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}

#################
# CONNECT TO DB #
#################
mysql_select_db('GenomicBuilds');
$query = mysql_query("SELECT name, StringName, Description, size FROM CurrentBuild LIMIT 1");
$row = mysql_fetch_array($query);
$genomesize = $row['size'];

$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);

###################
# SORTING OPTIONS #
###################
if (isset($_POST['sort'] )) {
	$sort = $_POST['sort'];
}
else { 
	$sort = 'position';
}
$sortoptions = array('size' => 'nrgenes desc, (stop - start) DESC', 'position' => 'chr ASC, start ASC', 'rank' => 'rank ASC, chr ASC, start ASC');

########################
# GET POSTED VARIABLES #
########################
if (isset($_SESSION['setfromeditcnv'])) {
	$_POST['sample'] = $_SESSION['sample'];
	$_POST['project'] = $_SESSION['project'];
	unset($_SESSION['sample']);
	unset($_SESSION['project']);
	unset($_SESSION['setfromeditcnv']);
}

if (isset($_POST['sample'] ) ) {
	$sample = $_POST['sample'];
}
elseif (isset($_GET['sample'] ) ) {
	$sample = $_GET['sample'];
}
	
if (isset($_POST['project'] ) ) {
	$project = $_POST['project'];
}
elseif (isset($_GET['project'] ) ) {
	$project = $_GET['project'];
}

// datatype query
$typequery = mysql_query("SELECT datatype from project WHERE id = '$project'");
$typerow = mysql_fetch_array($typequery);

if (isset($typerow['datatype']) && ($typerow['datatype'] == 'swgs')) {
	include('inc_details_swgs.inc');
}
else {
	include('inc_details_array.inc');
}

############################
## END OF LOGGED IN CHECK ##
############################
echo "</div>\n";
?>


<!-- context menu loading -->
<!-- <script type="text/javascript" src="javascripts/details_context.js"></script> -->
<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/ajax_tooltips.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>
<script type='text/javascript' src='javascripts/details.js'></script> <!-- mendelian error stuff -->
<!-- functions that use values from php... -->
<script type='text/javascript'>
function LoadPreferences(subtype) {
	// put temporary response in place
	document.getElementById('infodiv').innerHTML = "<div style='text-align:center;height:100%;padding-top:15%'><img src='images/layout/loading.gif' height=50px /><br/><h3>Loading Preferences...</h3></div>";
	// set height
	var D = document;
    	var bodyh =  Math.max(
	        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
       	 	Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
       	 	Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    		);
	bodyh = bodyh*1 - 25*1; 
	//if (divh < bodyh) {
		document.getElementById('infodiv').style.height = bodyh+'px';
	//}
	// show the div
	document.getElementById('infodiv').style.display = '';
	// fetch the preferences pages with ajax
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
	  alert ("Browser does not support HTTP Request");
	  return;
	  }
	var url="Browser/setplotprefs.php";
	url=url+"?subtype="+subtype;
	url=url+"&rv="+Math.random();
	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState==4)
		{
			
			// start form to submit selection
			// add get variables to action
			<?php 
			$newcontent = "index.php?";
			foreach ($_GET as $gkey => $gval) {
				$newcontent .= "$gkey=$gval&";
			}
			$newcontent = substr($newcontent,0,-1);
			$newcontent = "<form action=\"$newcontent\"  method=POST>";
			// add hidden variable triggering db update
			$newcontent .= "<input type=hidden name=\"updatepreferences\" value=1 />";
			// add posted variables as hidden fields
			foreach ($_POST as $pkey => $pval) {
				$newcontent .= "<input type=hidden name=\"$pkey\" value=\"$pval\" />";
			}
			//print the newcontent
			echo "document.getElementById('infodiv').innerHTML='$newcontent'+xmlhttp.responseText+'</form>';";
			?>	
			// readjust height if needed
    			bodyh =  Math.max(
			        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
       	 			Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
		       	 	Math.max(D.body.clientHeight, D.documentElement.clientHeight)
		    		);
			bodyh = bodyh*1; 
			document.getElementById('infodiv').style.height = bodyh+'px';
			window.scrollTo(0,0);
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}
</script>
