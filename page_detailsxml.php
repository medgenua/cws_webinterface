<?php
////////////////////////////////
// Variables for table layout //
////////////////////////////////
  $tdtype= array("","class=alt");
  $thtype= array("class=spec","class=specalt");
  $topstyle = array("class=topcell","class=topcellalt");
  $firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
#database
$db = "CNVanalysis" . $_SESSION['dbname'];
$ucscdb = str_replace('-','',$_SESSION['dbname']);
if ($loggedin == 1) {
	for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}
	$chromhash["X"] = "23";
	$chromhash["Y"] = "24";


	ob_start();
	#echo "<html>\n";
	$projectname = $_GET['project'];
	#echo "<body>\n";
	//echo "$nrrows<br>";
	if (substr($projectname,-4) == '.xml') {
		$bookmarkfile = "bookmarks/$projectname";
	}
	else {
		$bookmarkfile = "bookmarks/$projectname.xml";
	}
	$fh = fopen($bookmarkfile, "r");
	if ($fh == false) {
		echo "Problem. Could not open bookmark file.<br/>";
		exit;
	}
	$line = fgets($fh);
	$Naam = preg_replace('/(.*<Name>)(.*)(<\/Name>.*)/', "$2", $line);
	echo "<div class=\"sectie\">\n";
	echo "<h3>Details:</h3>\n<ul id=\"ul-mine\">\n";
	echo "<p>\n";
	echo "</p>\n";
	echo "<li>Project Name: $Naam</li>\n";

	if (preg_match('/Algorithm>QuantiSNP/', $line)) {
		$algo = "qsnp";
 		$em = preg_replace('/(.*em: )(\d*)( -.*)/', "$2", $line);
  		$lsetting = preg_replace('/(.*Ls: )(\d*)( -.*)/', "$2", $line);
  		$mcopy = preg_replace('/(.*mc: )(\d*)( -.*)/', "$2", $line);
  		$dogcc = preg_replace('/(.*dogcc: )(\w*)( -.*)/', "$2", $line);
  		$pRS = preg_replace('/(.*pRS: )(\w+)( -.*)/', "$2", $line);
  		$minsnp = preg_replace('/(.*minSNP: )(\d*)( -.*)/', "$2", $line);
  		$minlbf = preg_replace('/(.*minLBF: )(\d*)(<.*)/', "$2", $line);
  		echo "<li>Minimal number of SNP's: $minsnp</li>\n";
  		echo "<li>Minimal Log Bayes Factor: $minlbf</li>\n";
  		echo "<li>Number of Expectation maximisation steps: $em</li>\n";
  		echo "<li>Maximal copy number: $mcopy </li>\n";
  		echo "<li>Length Setting: $lsetting</li>\n";
  		echo "<li>GC-correction applied: $dogcc</li>\n";
  		echo "<li>Probe names used: $pRS</li>\n";
	}
	elseif (preg_match('/Algorithm>PennCNV/',$line)) { 
		$algo = "pcnv";
		$minscore = preg_replace('/(.*Minconf: )(\d*)( -.*)/', "$2", $line);
		$minsnp = preg_replace('/(.*minsnp: )(\d*)(<.*)/',"$2",$line);
  		echo "<li> Minimal Confidence: $minscore</li>\n";
		echo "<li> Minimal number of SNP's: $minsnp</li>\n";
  	}
	else {
		echo "<li>No other params available</li>\n";
	}
	$prevsampleid = '';
	echo "</ul>\n";
	echo "<table cellspacing=0>\n";
	$nrab = 0;
	while (!feof($fh)){
    		$line = fgets($fh);
    		if (preg_match('/<bookmark>/', "$line")) {
			$line = fgets($fh);
			$sampleid = preg_replace('/(.*<sample_id>)(\w+)( \[.*)/',"$2",$line) ;
   			$line = fgets($fh);
			$type = preg_replace('/(.*<bookmark_type>)(.+)(<\/bookmark_type>.*)/',"$2",$line);
   			$line = fgets($fh);
   			$line = fgets($fh);
			$chr = trim(preg_replace('/(.*<chr_num>)(\w+)(<\/chr_num>.*)/',"$2",$line));
   			$line = fgets($fh);
   			$start = trim(preg_replace('/(.*pos>\s*)(\d+)(<.*)/',"$2",$line));
   			$line = fgets($fh);
   			$end = trim(preg_replace('/(.*pos>\s*)(\d+)(<.*)/',"$2",$line));
   			$line = fgets($fh);
   			$line = fgets($fh);
   			$value = preg_replace('/(.*value>)(\d*)(<.*)/',"$2",$line);
   			$line = fgets($fh);
			$recurrent = "No";
			if (preg_match('/RECURRENT/',$type)) {
    				$recurrent = "Yes";
				$line = fgets($fh);
    				$line = fgets($fh);
   			}
			$nrsnp = preg_replace('/(.*SNP.*: )(\d*)(.*)/',"$2",$line);
   			if ($algo == 'qsnp' ) {
				$lbf = preg_replace('/(.*LBF: )(.*)(,.*)/',"$2",$line);
			}
			elseif ($algo == 'pcnv') {
				$lbf = preg_replace('/(.*Conf: )(.*)(,.*)/',"$2",$line);
			}
			else {
				$lbf = "NaN";
			}
			mysql_select_db($db);
			$chrnum = $chromhash{ $chr };
			$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chrnum' AND ('$start' BETWEEN start AND stop)");
			$cytorow = mysql_fetch_array($cytoquery);
			$cytostart = $cytorow['name'];
			$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chrnum' AND ('$end' BETWEEN start AND stop)");
			$cytorow = mysql_fetch_array($cytoquery);
			$cytostop = $cytorow['name'];
			if ($cytostart == $cytostop) {
				$cytoband = "($chr$cytostart)";
			}
			else {
				$cytoband = "($chr$cytostart-$cytostop)";
			}  

			# check decipher entries
			mysql_select_db("decipher");	
			$query = mysql_query("SELECT ID FROM syndromes WHERE chr = \"". $chromhash[ $chr ]. "\" AND ((Start >= \"$start\" AND Start <= \"$end\") OR (End >= \"$start\" AND End <= \"$end\") OR (Start <= \"$start\" AND End >= \"$end\")) ");
			$data = mysql_query($query);
			$nrrows = mysql_num_rows($query);
			if ($nrrows > 0) {
				$decipher = "<a class=clear href=index.php?page=showdecipher&amp;type=syndromes&amp;chr=$chr&amp;start=$start&amp;end=$end&amp;case=$sampleid target=new><image src=images/bullets/syndrome.ico></a>";	 
			}
			else {
				$query = mysql_query("SELECT ID FROM patients WHERE chr = \"". $chromhash[ $chr ] . "\" AND ((Start >= \"$start\" AND Start <= \"$end\") OR (End >= \"$start\" AND End <= \"$end\") OR (Start <= \"$start\" AND End >= \"$end\")) ");
				#$data = mysql_query("$query");
				$nrrows = mysql_num_rows($query);
				if ($nrrows > 0) {
					$decipher = "<a class=clear href=index.php?page=showdecipher&amp;type=patients&amp;chr=$chr&amp;start=$start&amp;end=$end&amp;case=$sampleid target=new><image src=images/bullets/patient.ico></a>";
				}
				else {
					$decipher = "";
				}
			}

			# check primer entries
			mysql_select_db("Primers");
			$query = mysql_query("SELECT Chr, Start, End, Forward, Reverse, Class, Creator FROM primers WHERE chr = \"". $chromhash [$chr ] ."\" AND (Start >= \"$start\" AND End <= \"$end\")");
			$nrrows = mysql_num_rows($query);
			if ($nrrows > 0) {
				$primers = "<a class=clear href=index.php?page=manageprimers&amp;type=search&amp;location=chr$chr:$start-$end target=new><image src=images/bullets/qpcr.ico></a>";
			}
			else {
				$primers = "";
			}

			# Fill Table
     			if ($prevsampleid != $sampleid) {
				$switch=0;
 				echo "</table>\n";
				if ($prevsampleid != "") {
					echo "<p> => $nrab Aberrations found for $prevsampleid</p>\n";
				}
				echo "</div>\n<div class=\"sectie\">\n<h3>$sampleid</h3>\n";
				$nrab = 0;
			        $prevsampleid = $sampleid;
				echo "<p><table cellspacing=0 >\n";
				echo " <tr>\n"; 
				echo "  <th scope=col class=topcellalt $firstcell>Location</th>\n";
				echo "  <th scope=col class=topcellalt>CN</th>\n";
				#echo "  <th scope=col class=topcellalt>Start</th>\n";
				#echo "	<th scope=col class=topcellalt>End</th>\n";
				echo "  <th scope=col class=topcellalt>Size</th>\n";
				echo "  <th scope=col class=topcellalt>nrSNP's</th>\n";
				echo "  <th scope=col class=topcellalt>Confidence Values</th>\n";
				echo "  <th scope=col class=topcellalt>Recurrent</th>\n";
				echo "  <th scope=col class=topcellalt>Links</th>\n";
				echo " </tr>\n";
	
				ob_flush;
				flush();
			}
      			$nrab = $nrab +1;
      			$size = $end - $start;
     			$location = "chr". $chr . ":" . number_format($start,0,'',',') . "-" . number_format($end,0,'',',') ;
      			echo " <tr>\n";
     			echo "  <td $tdtype[$switch] $firstcell>$location<br>$cytoband</td>\n";
      			echo "  <td $tdtype[$switch]>$value</td>\n";
      			#echo "  <td $tdtype[$switch]>" . number_format($start,0,'',',') . "</td>\n";
      			#echo "  <td $tdtype[$switch]>" . number_format($end,0,'',','). "</td>\n";
      			echo "  <td $tdtype[$switch]>" . number_format($size,0,'',',') . "</td>\n";
      			echo "  <td $tdtype[$switch]>$nrsnp</td>\n";
      			echo "  <td $tdtype[$switch]>$lbf</td>\n";
      			echo "  <td $tdtype[$switch]>$recurrent</td>\n";
			// get links to ensembl, ucsc and dgv
			$lquery = mysql_query("SELECT Resource, link FROM db_links WHERE Resource IN ('UCSC','Ensembl')");
			while ($row = mysql_fetch_array($lquery)) {
				$link = $row['link'];
				if ($row['Resource'] == 'Ensembl') {
					$linkparts = explode('@@@',$link) ;
					if ($size < 1000000) {
						$link = $linkparts[0];
					}
					else {
						$link = $linkparts[1];
					}
				}
				$link = str_replace('%c',$chrtxt,$link);
				$link = str_replace('%s',$start,$link);
				$link = str_replace('%e',$stop,$link);
				$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
				$link = str_replace('%u',$ucscdb,$link);
				$links[$row['Resource']] = $link;
			}
      			echo "  <td $tdtype[$switch]><a href='".$links['UCSC']."' target='_blank><img src=images/bullets/ucsc.ico></a>\n";
      			echo "  <a href='".$links['Ensembl']."' target='_blank'><img src=images/bullets/ensemble.ico></a>\n";
  			echo "  $decipher$primers</td>\n";
      			echo "</tr>\n";
      			$switch = $switch + pow(-1,$switch);
      			ob_flush();
      			flush();
   		}
	}
	echo "</table>\n";
	if ($prevsampleid != "") {
		echo "<p> => $nrab Aberrations found for $prevsampleid</p>\n";
	}
	else {
		echo "<p> No samples found in this project file</p>\n";
	}

	//echo "$projectname<br>";
	//$page = $_GET['page'];
	//echo "$page<br>";
	echo "</div>\n";
	fclose($fh);
}
?>


