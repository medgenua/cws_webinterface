<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>


<div class=sectie>
<h3>Find difference between two projects</h3>
<?php 
////////////////////////////////
// Variables for table layout //
////////////////////////////////
if ($loggedin != 1) {
	include('login.php');
}
else {
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
if (!isset($_POST['submit'])) {
	echo "<p><form action=index.php?page=diff method=POST>\n";
	echo "Select projects to compare: </p><p>Project 1: <select name=project1>\n";
	
	$query = mysql_query("SELECT id, naam, chiptype, created, collection FROM project WHERE userID = '$userid' ORDER BY id DESC");
	while($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		echo "  <option value=$id>$name, $chiptype, $created</option>\n";
	}
	echo "</select></p><p>Project 2: <select name=project2>\n";
		$query = mysql_query("SELECT id, naam, chiptype, created, collection FROM project WHERE userID = '$userid' ORDER BY id DESC");
	while($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		echo "  <option value=$id>$name, $chiptype, $created</option>\n";
	}
	echo "</select></p>\n";
	echo "<p><input type=submit name=submit class=button value=Submit>\n";

	echo "</div>\n";
}
else {
	$id1 = $_POST['project1'];
	$id2 = $_POST['project2'];
	$query = mysql_query("SELECT naam, chiptype, created, collection FROM project WHERE id = '$id1' ");
	$row = mysql_fetch_array($query);
	$name1 = $row['naam'];
	$chiptype1 = $row['chiptype'];
	$created1 = $row['created'];
	$collection1 = $row['collection'];
	$query = mysql_query("SELECT naam, chiptype, created, collection FROM project WHERE id = '$id2' ");
	$row = mysql_fetch_array($query);
	$name2 = $row['naam'];
	$chiptype2 = $row['chiptype'];
	$created2 = $row['created'];
	$collection2 = $row['collection'];
	echo "<p>Below you will find the comparison of project \"$name1\" (refered as '1') with project \"$name2\" (refered as '2').  Comparison goes in two directions, to find CNV's found in one, but not in the other project for the same sample (based on chip_dnanr). </p></div>\n";
	// Step 1 : what is in id1 and not in id2
	echo "<div class=sectie>\n";
	echo "<h3>CNV's found in 1 but not in 2</h3>\n";
	echo "<p>\n";
	$samplequery = mysql_query("SELECT s.id, s.chip_dnanr, s.gender FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE ps.idproj = '$id1'");
	$nrsamples = mysql_num_rows($samplequery);
	while ($samples = mysql_fetch_array($samplequery)) {
		$sname = $samples['chip_dnanr'];
		$sid = $samples['id'];
		$gender = $samples['gender'];
		$subquery = mysql_query("SELECT idsamp FROM projsamp WHERE idsamp = '$sid' AND idproj = '$id2'");
		$result = mysql_fetch_array($subquery);
		if ($result['idsamp'] == "") {  
			echo "<b>$sname ($gender) :</b></p><p> Not available in project 2</p>\n";
		}
		else{
			echo "<p><b>$sname ($gender) :</b></p>\n";
			$found = 0;
			$switch = 0;
			$tablestring = "";
			$abquery = mysql_query("SELECT id, cn, start, stop, chr, seenby, nrsnps, nrgenes, seeninchip, seenall, seenincontrols FROM aberration WHERE sample='$sid' AND idproj='$id1' ORDER BY chr, start");
			while ($cnvs = mysql_fetch_array($abquery)) {
				$aid = $cnvs['id'];
				$CurrCN = $cnvs['cn'];
				if ($CurrCN == 2)  {
					# skip LOH for now...
					continue;
				}
				$CurrStart = $cnvs['start'];
				$CurrStop = $cnvs['stop'];
				$CurrChr = $cnvs['chr'];
				$CurrChrtxt = $chromhash[ $CurrChr ];
				$absubquery = mysql_query("SELECT id FROM aberration WHERE sample='$sid' AND idproj='$id2' AND chr = '$CurrChr' AND (start BETWEEN ($CurrStart - 30000) AND ($CurrStart + 30000)) AND (stop BETWEEN ($CurrStop - 30000) AND ($CurrStop + 30000))");
				$nrrows = mysql_num_rows($absubquery);
				# no results => not seen in second project
				if ($nrrows == 0) {
					$found = $found + 1;
					$seenby = $cnvs['seenby'];
					$seenby = preg_replace("/^\s-\s/","",$seenby);
					$seenby = str_replace(" - ", "<BR>",$seenby);
					$nrsnps = $cnvs['nrsnps'];
					$nrgenes = $cnvs['nrgenes'];
					$inchip = $cnvs['seeninchip']*100;
					$seenall = $cnvs['seenall']*100;
					$seencon = $cnvs['seenincontrols']*100;
					$score = 0;
					if ($inchip < 5 && $inchip > 0 && $seencon == 0) {
						$score = $score + 2;
					}
					elseif ($inchip < 5 && $inchip > 0 ) {
						$score = $score + 1.5;
					}

					if ($seenall < 5 && $inchip > 0) {
						$score = $score + 0.5;
					}
					if ($nrgenes > 0 && $nrgenes <= 5) {
						$score = $score + 1;
					}
					if ($nrgenes > 5) {
						$score = $score + 2;
					}
					mysql_select_db("decipher");	
					$query = mysql_query("SELECT ID FROM syndromes WHERE chr = \"$CurrChr\" AND ((Start >= \"$CurrStart\" AND Start <= \"$CurrStop\") OR (End >= \"$CurrStart\" AND End <= \"$CurrStop\") OR (Start <= \"$CurrStart\" AND End >= \"$CurrStop\")) ");
					$data = mysql_query($query);
					$nrrows = mysql_num_rows($query);
					if ($nrrows > 0) {
						$decipher = "<a class=clear href=index.php?page=showdecipher&amp;type=syndromes&amp;chr=$CurrChrtxt&amp;start=$CurrStart&amp;end=$CurrStop&amp;case=$sname target=new><image src=images/bullets/syndrome.ico></a>";	 
						$score = $score + 2;
					}
					else {
						$query = mysql_query("SELECT ID FROM patients WHERE chr = \"$CurrChr\" AND ((Start >= \"$CurrStart\" AND Start <= \"$CurrStop\") OR (End >= \"$CurrStart\" AND End <= \"$CurrStop\") OR (Start <= \"$CurrStart\" AND End >= \"$CurrStop\")) ");
						#$data = mysql_query("$query");
						$nrrows = mysql_num_rows($query);
						if ($nrrows > 0) {
							$decipher = "<a class=clear href=index.php?page=showdecipher&amp;type=patients&amp;chr=$CurrChrtxt&amp;start=$CurrStart&amp;end=$CurrStop&amp;case=$sname target=new><image src=images/bullets/patient.ico></a>";
							$score = $score +1;
						}
						else {
							$decipher = "";
						}
					}
					$score = floor($score);
					mysql_select_db("$db");
					$size = $CurrStop - $CurrStart;
				      	$location = "chr". $CurrChrtxt . ":" . number_format($CurrStart,0,'',',') . "-" . number_format($CurrStop,0,'',',') ;
					$tablestring .= " <tr>\n";
					$tablestring .= "  <td $tdtype[$switch] $firstcell><img src=images/bullets/$color[$score].ico></td>\n";
      					$tablestring .= "  <td $tdtype[$switch]>$location</td>\n";
      					$tablestring .= "  <td $tdtype[$switch] onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\">$CurrCN</td>\n";
      		 			$tablestring .= "  <td $tdtype[$switch]>" . number_format($size,0,'',',') . "</td>\n";
      					$tablestring .= "  <td $tdtype[$switch] >$nrsnps</td>\n";
      					$tablestring .= "  <td $tdtype[$switch] NOWRAP>$seenby</td>\n";
      					$tablestring .= "  <td $tdtype[$switch]>$inchip / $seenall / $seencon</td>\n";
					if ($nrgenes > 0) {
	      					$tablestring .= "  <td $tdtype[$switch]><a href=index.php?page=genes&amp;chr=$CurrChrtxt&amp;start=$CurrStart&amp;stop=$CurrStop target=new class=$tdtype[$switch]>$nrgenes</a></td>\n";
      					}
					else {
	     					$tablestring .= "   <td $tdtype[$switch]>$nrgenes</td>\n";
      					}
					if ($size < 1000000) 
       						$ensemble = "http://www.ensembl.org/Homo_sapiens/Location/View?r=" . $CurrChrtxt .":" . $CurrStart . "-" . $CurrStop;
      					else 
       						$ensemble = "http://www.ensembl.org/Homo_sapiens/Location/Overview?r=" . $CurrChrtxt .":" . $CurrStart . "-" . $CurrStop ;
     					$ucsc = "http://genome.ucsc.edu/cgi-bin/hgTracks?position=chr". $CurrChrtxt . ":" . $CurrStart . "-" . $CurrStop;
      					$tablestring .= "  <td $tdtype[$switch]><a href=\"$ucsc\" target=\"new\"><img src=images/bullets/ucsc.ico></a>\n";
      					$tablestring .= "  <a href=\"$ensemble\" target=\"new\"><img src=images/bullets/ensemble.ico></a>\n";
      					$tablestring .= "  $decipher$primers</td>\n";
      					$tablestring .= "</tr>\n";
					//echo "$tablestring";
      					$switch = $switch + pow(-1,$switch);
      					//ob_flush();
      					//flush();
				}
			}
			if ($found > 0) {
				echo "<p><table cellspacing=0>\n";
				echo "<tr>\n";
				echo "  <th scope=col class=topcellalt $firstcell>!</th>\n";
				echo "  <th scope=col class=topcellalt >Location</th>\n";
				echo "  <th scope=col class=topcellalt>CN</th>\n";
				#echo "  <th scope=col class=topcellalt>Start</th>\n";
				#echo "	<th scope=col class=topcellalt>End</th>\n";
				echo "  <th scope=col class=topcellalt>Size</th>\n";
				echo "  <th scope=col class=topcellalt>#SNP</th>\n";
				echo "  <th scope=col class=topcellalt>Scores</th>\n";
				echo "  <th scope=col class=topcellalt title=\"Aberration is seen in % of samples from : same collection, same chip / same collection, all chips / controls, same chip\" >Seen (%)</th>\n";
				echo "  <th scope=col class=topcellalt>#RefSeq</th>\n";
				echo "  <th scope=col class=topcellalt>Links</th>\n";
				echo " </tr>\n";
				echo "$tablestring";
				echo "</table></p>\n";
			}
			else {
				echo "<p>No extra fragments found in project 1</p>\n";
			}
		}
	}
	echo "</div>\n";
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
	// Step two, the opposite	
	echo "<div class=sectie>\n";
	echo "<h3>CNV's found in 2 but not in 1</h3>\n";
	echo "<p>\n";
	$samplequery = mysql_query("SELECT s.id, s.chip_dnanr, s.gender FROM sample s JOIN projsamp ps ON s.id = ps.idsamp WHERE ps.idproj = '$id2'");
	$nrsamples = mysql_num_rows($samplequery);
	while ($samples = mysql_fetch_array($samplequery)) {
		$sname = $samples['chip_dnanr'];
		$sid = $samples['id'];
		$gender = $samples['gender'];
		$subquery = mysql_query("SELECT idsamp FROM projsamp WHERE idsamp = '$sid' AND idproj = '$id1'");
		$result = mysql_fetch_array($subquery);
		if ($result['idsamp'] == "") {  
			echo "<b>$sname ($gender) :</b></p><p> Not available in project 1</p>\n";
		}
		else{
			echo "<p><b>$sname ($gender) :</b></p>\n";
			$found = 0;
			$switch = 0;
			$tablestring = "";
			$abquery = mysql_query("SELECT id, cn, start, stop, chr, seenby, nrsnps, nrgenes, seeninchip, seenall, seenincontrols FROM aberration WHERE sample='$sid' AND idproj='$id2' ORDER BY chr, start");
			while ($cnvs = mysql_fetch_array($abquery)) {
				$aid = $cnvs['id'];
				$CurrCN = $cnvs['cn'];
				if ($CurrCN == 2) {
					# skip LOH
					continue;
				}
				$CurrStart = $cnvs['start'];
				$CurrStop = $cnvs['stop'];
				$CurrChr = $cnvs['chr'];
				$CurrChrtxt = $chromhash[ $CurrChr ];
				$absubquery = mysql_query("SELECT id FROM aberration WHERE sample='$sid' AND idproj='$id1' AND chr = '$CurrChr' AND (start BETWEEN ($CurrStart - 30000) AND ($CurrStart + 30000)) AND (stop BETWEEN ($CurrStop - 30000) AND ($CurrStop + 30000))");
				$nrrows = mysql_num_rows($absubquery);
				if ($nrrows == 0) {
					$found = $found + 1;
					$seenby = $cnvs['seenby'];
					$seenby = preg_replace("/^\s-\s/","",$seenby);
					$seenby = str_replace(" - ", "<BR>",$seenby);
					$nrsnps = $cnvs['nrsnps'];
					$nrgenes = $cnvs['nrgenes'];
					$inchip = $cnvs['seeninchip']*100;
					$seenall = $cnvs['seenall']*100;
					$seencon = $cnvs['seenincontrols']*100;
					$score = 0;
					if ($inchip < 5 && $inchip > 0 && $seencon == 0) {
						$score = $score + 2;
					}
					elseif ($inchip < 5 && $inchip > 0 ) {
						$score = $score + 1.5;
					}

					if ($seenall < 5 && $inchip > 0) {
						$score = $score + 0.5;
					}
					if ($nrgenes > 0 && $nrgenes <= 5) {
						$score = $score + 1;
					}
					if ($nrgenes > 5) {
						$score = $score + 2;
					}
					mysql_select_db("decipher");	
					$query = mysql_query("SELECT ID FROM syndromes WHERE chr = \"$CurrChr\" AND ((Start >= \"$CurrStart\" AND Start <= \"$CurrStop\") OR (End >= \"$CurrStart\" AND End <= \"$CurrStop\") OR (Start <= \"$CurrStart\" AND End >= \"$CurrStop\")) ");
					$data = mysql_query($query);
					$nrrows = mysql_num_rows($query);
					if ($nrrows > 0) {
						$decipher = "<a class=clear href=index.php?page=showdecipher&amp;type=syndromes&amp;chr=$CurrChrtxt&amp;start=$CurrStart&amp;end=$CurrStop&amp;case=$sname target=new><image src=images/bullets/syndrome.ico></a>";	 
						$score = $score + 2;
					}
					else {
						$query = mysql_query("SELECT ID FROM patients WHERE chr = \"$CurrChr\" AND ((Start >= \"$CurrStart\" AND Start <= \"$CurrStop\") OR (End >= \"$CurrStart\" AND End <= \"$CurrStop\") OR (Start <= \"$CurrStart\" AND End >= \"$CurrStop\")) ");
						#$data = mysql_query("$query");
						$nrrows = mysql_num_rows($query);
						if ($nrrows > 0) {
							$decipher = "<a class=clear href=index.php?page=showdecipher&amp;type=patients&amp;chr=$CurrChrtxt&amp;start=$CurrStart&amp;end=$CurrStop&amp;case=$sname target=new><image src=images/bullets/patient.ico></a>";
							$score = $score +1;
						}
						else {
							$decipher = "";
						}
					}
					$score = floor($score);
					mysql_select_db("$db");
					$size = $CurrStop - $CurrStart;
				      	$location = "chr". $CurrChrtxt . ":" . number_format($CurrStart,0,'',',') . "-" . number_format($CurrStop,0,'',',') ;
					$tablestring .= " <tr>\n";
					$tablestring .= "  <td $tdtype[$switch] $firstcell><img src=images/bullets/$color[$score].ico></td>\n";
      					$tablestring .= "  <td $tdtype[$switch]>$location</td>\n";
      					$tablestring .= "  <td $tdtype[$switch] onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\">$CurrCN</td>\n";
      		 			$tablestring .= "  <td $tdtype[$switch]>" . number_format($size,0,'',',') . "</td>\n";
      					$tablestring .= "  <td $tdtype[$switch] >$nrsnps</td>\n";
      					$tablestring .= "  <td $tdtype[$switch] NOWRAP>$seenby</td>\n";
      					$tablestring .= "  <td $tdtype[$switch]>$inchip / $seenall / $seencon</td>\n";
					if ($nrgenes > 0) {
	      					$tablestring .= "  <td $tdtype[$switch]><a href=index.php?page=genes&amp;chr=$CurrChrtxt&amp;start=$CurrStart&amp;stop=$CurrStop target=new class=$tdtype[$switch]>$nrgenes</a></td>\n";
      					}
					else {
	     					$tablestring .= "   <td $tdtype[$switch]>$nrgenes</td>\n";
      					}
					if ($size < 1000000) 
       						$ensemble = "http://www.ensembl.org/Homo_sapiens/Location/View?r=" . $CurrChrtxt .":" . $CurrStart . "-" . $CurrStop;
      					else 
       						$ensemble = "http://www.ensembl.org/Homo_sapiens/Location/Overview?r=" . $CurrChrtxt .":" . $CurrStart . "-" . $CurrStop ;
     					$ucsc = "http://genome.ucsc.edu/cgi-bin/hgTracks?position=chr". $CurrChrtxt . ":" . $CurrStart . "-" . $CurrStop;
      					$tablestring .= "  <td $tdtype[$switch]><a href=\"$ucsc\" target=\"new\"><img src=images/bullets/ucsc.ico></a>\n";
      					$tablestring .= "  <a href=\"$ensemble\" target=\"new\"><img src=images/bullets/ensemble.ico></a>\n";
      					$tablestring .= "  $decipher$primers</td>\n";
      					$tablestring .= "</tr>\n";
					//echo "$tablestring";
      					$switch = $switch + pow(-1,$switch);
      					//ob_flush();
      					//flush();
				}
			}
			if ($found > 0) {
				echo "<p><table cellspacing=0>\n";
				echo "<tr>\n";
				echo "  <th scope=col class=topcellalt $firstcell>!</th>\n";
				echo "  <th scope=col class=topcellalt >Location</th>\n";
				echo "  <th scope=col class=topcellalt>CN</th>\n";
				#echo "  <th scope=col class=topcellalt>Start</th>\n";
				#echo "	<th scope=col class=topcellalt>End</th>\n";
				echo "  <th scope=col class=topcellalt>Size</th>\n";
				echo "  <th scope=col class=topcellalt>#SNP</th>\n";
				echo "  <th scope=col class=topcellalt>Scores</th>\n";
				echo "  <th scope=col class=topcellalt title=\"Aberration is seen in % of samples from : same collection, same chip / same collection, all chips / controls, same chip\" >Seen (%)</th>\n";
				echo "  <th scope=col class=topcellalt>#RefSeq</th>\n";
				echo "  <th scope=col class=topcellalt>Links</th>\n";
				echo " </tr>\n";
				echo "$tablestring";
				echo "</table></p>\n";
			}
			else {
				echo "<p>No extra fragments found in project 1</p>\n";
			}
		}
	}
	echo "</div>\n";	
}
}
?>


<div id="txtHint">something must appear here</div>
<!-- context menu loading -->
<script type="text/javascript" src="javascripts/details_context.js"></script>

