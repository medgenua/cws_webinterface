<?php

if (isset($_GET['s'])) {
	$subject = $_GET['s'];
}
else {
	$subject = '';
}
if ($subject == '') {
?>
	<div class=sectie>
	<h3>Documentation</h3>
	<h4>Please inform us if there is anything unclear in the documentation.</h4>
	<p>Select the topics you need information on from the table below. Direct access to page specific documentation is available from the question mark in the top right corner of the page.</p>
	<p>
	<ul id=ul-simple style='width: 250px; padding:15px; border: solid 2px #e6e6e6; '>
		 <li> - <a href='index.php?page=documentation&amp;s=handson'>Hands-On demo with sample data</a></li>
		 <li> - <span class=nadruk>Handling Illumina Data</span><ul id=ul-simple style='width: 250px; padding-left:15px; '>
			<li> - <a href='index.php?page=documentation&amp;s=upload'>Upload large datafiles</a></li>
		 	<li> - <a href='index.php?page=documentation&amp;s=run'>Running an analysis</a></li>
			<li> - <a href='index.php?page=documentation&amp;s=peds'>Adding family information</a></li>
		 	<li> - <a href='index.php?page=documentation&amp;s=results&amp;t=main'>Browsing results</a></li>
	 		<li> - <a href='index.php?page=documentation&amp;s=results&amp;t=illumina'>Managing projects</a></li>
			<li> - <a href='index.php?page=documentation&amp;s=results&amp;t=xml'>Recreate XML files</a></li>
			</ul></li>
	 	 <li> - <span class=nadruk>Handling Non-Illumina Data</span><ul id=ul-simple style='width: 250px; padding-left:15px; '>
			<li> - <a href='index.php?page=documentation&amp;s=custom_methods&amp;t=cp'>Creating a parser</a></li>
			<li> - <a href='index.php?page=documentation&amp;s=custom_methods&amp;t=st'>Adding new data</a></li>
	 		<li> - <a href='index.php?page=documentation&amp;s=custom_methods&amp;t=tree'>Browsing results</a></li>

			</ul></li>
	</ul>
	</p>
	</div>

<?php
}
else { 
	if (file_exists("documentation/inc_$subject.inc")) {
		include("documentation/inc_$subject.inc");
	}
	else {
		echo "<div class=sectie>";
		echo "<h3>Platform documentation</h3>";
		echo "<p>This page is not yet documented. Please feel free to contact me if you need information</p>";
	}
}
?>

