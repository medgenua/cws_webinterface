<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

// clean file
if (isset($_GET['clean'])) {
	$key = $_GET['key'];
	$fh = fopen("data/$key",'r');
	$line = fgets($fh);
	$line = rtrim($line);
	fclose($fh);
	$file = "$line.tar.gz";
	system("rm data/$file && rm data/$key");
	echo "<div class=sectie>";
	echo "<h3>File Deleted</h3>";
	echo "<p> The dump has been successfully made, downloaded and removed on the server. You can sepecify another dump below.</p>";
	echo "</div>";
}
if (!isset($_POST['submit'])) {
	## input form
	echo "<div class=sectie>";
	echo "<h3>Dump Data to Downloadable File</h3>";
	echo "<p>If needed, it is possible to dump projects into a several output formats for analysis using third party software, or to import them into a local installation of CNV-WebStore. Select the projects to be exported and the format below, and press 'Create Download'</p>";
	echo "<p>If mysql format is used, data can be imported in CNV-WebStore as Illumina Data, but ownership and sharing options will be mostly lost. You will gain full control over the projects once you import them again. For all other formats, a parser will have to be created.  </p>";
	echo "</div>";
	echo "<div class=sectie>";
	echo "<h3>Exporting settings</h3>";
	echo "<p><form action='index.php?page=dumpprojects' method=POST>";
	echo "Output format: <select name=format>";
	echo "<option value=mysql>MySQL : All Data, for import in CNV-WebStore</option> ";
	echo "<option value=txt>.CSV : Comma Seperated File. Contains one CNV per line, with sample info redundantly present.</option>";
	echo "<option value=xls>.zip : Multiple CSV Files. Each file contains part of the info (CNV/RAW data/sample Info/...)</option>";
	echo "</select>";  
	echo "</p>";
	echo "<p>";
	echo "<table cellspacing=0>";
	echo "<tr><th colspan=4 $firstcell>ILLUMINA PROJECTS TO EXPORT</th> <tr>";
	echo "<tr><th class=topcellalt $firstcell>Include</th><th class=topcellalt>Project Name</th><th class=topcellalt>Chiptype</th><th class=topcellalt># Samples</th></tr>";
	$query = mysql_query("SELECT p.id, p.naam, p.chiptype , count(ps.idsamp) AS nrsamples FROM project p JOIN projectpermission pp JOIN projsamp ps ON p.id = pp.projectid AND ps.idproj = p.id WHERE pp.userid = $userid GROUP BY ps.idproj ORDER BY p.naam");
	while($row = mysql_fetch_array($query)) {
		echo "<tr>";
		$pid = $row['id'];
		$name = $row['naam'];
		$chip = $row['chiptype'];
		$nrs = $row['nrsamples'];
		echo "<td $firstcell><input type=checkbox name='include[]' value='$pid' CHECKED></td>";
		echo "<td>$name</td><td>$chip</td><td>$nrs</td>";
		echo "</tr>";
	}
	echo "</table>";
	echo "</p>";
	echo "<p><input type=submit class=button name='submit' value='Create Download'></p>";
	echo "</form></div>";
}
else {
	// process submission
	if ($_POST['format'] == 'mysql') {
		echo "<div class=sectie>";
		echo "<h3>Dump in progress</h3>";
		echo "<p>";
		echo "<ol>";
		$todump = $_POST['include'];
		$instring = '';
		foreach($todump as $idx => $pid) {
			$instring = $instring . "$pid,";	
		}
		$instring = substr($instring,0,-1);
		system("mkdir /tmp/$userid.dump && chmod -R 777 /tmp/$userid.dump");
		// projects
		echo "<li>Dumping projects</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM project WHERE id IN ($instring) INTO OUTFILE '/tmp/$userid.dump/project.txt'");
		// samples
		echo "<li>Dumping samples</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT s.id, s.chip_dnanr, s.gender, s.intrack, s.trackfromproject FROM sample s JOIN projsamp ps ON ps.idsamp = s.id WHERE ps.idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/sample.txt'");
		// store clinical info into xml
		$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
		$query = mysql_query("SELECT s.id, DECODE(s.clinical, '$encpass') as clindata FROM sample s JOIN projsamp ps ON ps.idsamp = s.id WHERE ps.idproj IN ($instring)");
		$sidstring = '';
		$fh = fopen("/tmp/$userid.dump/clinic.xml", 'w');
		while ($row = mysql_fetch_array($query)) {
			$sidstring = $sidstring . $row['id'] . ',';
			$sid = $row['id'];
			$clindata = $row['clindata'];
			if ($clindata != '') {
				$clindata = str_replace(array("\r\n", "\r", ),"\n",$clindata);
				$xmlline = "<sample>\n<sid>$sid</sid>\n<clinic>$clindata</clinic>\n</sample>\n";
				fwrite($fh,$xmlline);
			}	
		}
		fclose($fh);
		$sidstring = substr($sidstring,0,-1);
		// project sample
		echo "<li>Dumping project-sample links</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM projsamp WHERE idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/projsamp.txt'");
		// aberrations
		echo "<li>Dumping aberrations</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM aberration WHERE idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/aberration.txt'");
		// data points
		echo "<li>Dumping datapoints</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT d.* FROM datapoints d JOIN aberration a ON d.id = a.id WHERE a.idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/datapoints.txt'");
		// LDDB phenotypes
		echo "<li>Dumping Clinical data</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT sp.* FROM sample_phenotypes sp JOIN projsamp ps ON ps.idsamp = sp.sid WHERE ps.idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/sample_phenotypes.txt'");
		// WGP plots
		echo "<li>Dumping Whole Genome Plots</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM plots WHERE idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/plots.txt'");
		system("mkdir /tmp/$userid.dump/WGP/ && chmod -R 777 /tmp/$userid.dump/WGP/");
		$query = mysql_query("SELECT filename FROM plots WHERE idproj IN ($instring)");
		while ($row = mysql_fetch_array($query)) {
			$file = $row['filename'];
			system("cp $plotdir/$file /tmp/$userid.dump/WGP/");
		}
		// prioritisation
		echo "<li>Dumping Prioritised aberrations</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM prioritize WHERE project IN ($instring) INTO OUTFILE '/tmp/$userid.dump/prioritize.txt'");
		$query = mysql_query("SELECT id FROM prioritize WHERE project IN ($instring)");
		$idstring = '';
		while ($row = mysql_fetch_array($query)) {
			$idstring = $idstring . $row['id'] . ",";
		}
		if ($idstring != '') {
			$idstring = substr($idstring,0,-1);
			$query = mysql_query("SELECT * FROM priorabs WHERE prid IN ($idstring) INTO OUTFILE '/tmp/$userid.dump/priorabs.txt'");
		}
		// parental information ( 
		echo "<li>Dumping Parental information</li>";
		$query = mysql_query("SELECT * FROM parents_relations WHERE id IN ($sidstring) OR father IN ($sidstring) OR mother IN ($sidstring) INTO OUTFILE '/tmp/$userid.dump/parents_relations.txt'");
		$query = mysql_query("SELECT * FROM parent_offspring_cnv_relations WHERE parent_sid IN ($sidstring) INTO OUTFILE '/tmp/$userid.dump/parent_offspring_cnv_relations.txt'");
		$query = mysql_query("SELECT parent_aid FROM parent_offspring_cnv_relations WHERE parent_seen = 0 AND parent_sid IN ($sidstring)");
		$paidstring = '';
		if (mysql_num_rows($query) > 0) {
			while ($row = mysql_fetch_array($query)) {
				$paidstring = $paidstring . $row['parent_aid'] . ',';
			}
			$paidstring = substr($paidstring, 0, -1);
			$query = mysql_query("SELECT * FROM parents_regions WHERE id IN ($paidstring) INTO OUTFILE '/tmp/$userid.dump/parents_regions.txt'");
			$query = mysql_query("SELECT * FROM parents_datapoints WHERE id IN ($paidstring) INTO OUTFILE '/tmp/$userid.dump/parents_datapoints.txt'");
		}
		$paidstring = '';
		// upd results
		echo "<li>Dumping UPD analysis results</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM parents_upd WHERE spid IN ($instring) OR fpid IN ($instring) OR mpid IN ($instring) INTO OUTFILE '/tmp/$userid.dump/parents_upd.txt'");
		$query = mysql_query("SELECT id FROM parents_upd WHERE spid IN ($instring) OR fpid IN ($instring) OR mpid IN ($instring)");
		if (mysql_num_rows($query) > 0) {
			$paidstring = '';
			while ($row = mysql_fetch_array($query)) {
				$paidstring = $paidstring . $row['id'] . ',';
			}
			$paidstring = substr($paidstring, 0, -1);
			$query = mysql_query("SELECT * FROM parents_upd_datapoints WHERE id in ($paidstring) INTO OUTFILE '/tmp/$userid.dump/parents_upd_datapoints.txt'");
			$paidstring = '';
		}
		// the log file
		echo "<li>Dumping Log entries</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM log WHERE pid IN ($instring) INTO OUTFILE '/tmp/$userid.dump/log.txt'");
		// deleted cnvs (datapoints already stored)
		echo "<li>Dumping Deleted CNV info</li>";
		flush();
		ob_flush();
		$query = mysql_query("SELECT * FROM deletedcnvs WHERE idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/deletedcnvs.txt'");
		// baf segmentation
		echo "<li>Dumping BAF-Segmentation Results</li>";
		ob_flush();
		flush();
		$query = mysql_query("SELECT * FROM BAFSEG WHERE idproj IN ($instring) INTO OUTFILE '/tmp/$userid.dump/bafseg.txt' FIELDS TERMINATED BY '@@@'");
		$query = mysql_query("SELECT filename FROM BAFSEG WHERE idproj IN ($instring)");
		system("mkdir /tmp/$userid.dump/BAFSEG/ && chmod -R 777 /tmp/$userid.dump/BAFSEG");
		if (mysql_num_rows($query) > 0) {
			while ($row = mysql_fetch_array($query)) {
				$file = $row['filename'];
				system("cp $plotdir/$file /tmp/$userid.dump/BAFSEG/");
			}
		}
		// done, create tarbal
		echo "<li>Create tarbal</li>";
		ob_flush();
		flush();
		// generate random values
		$rand = rand(0,1000);
		$key ='';
		$array = array();
		foreach (range('a','z') as $val) {
		        $array[] = $val;
		}
		foreach (range('A','Z') as $val) {
		        $array[] = $val;
		}
		foreach (range(0,9) as $val) {
		        $array[] = $val;
		}
		shuffle($array);
		$randarray = array_rand(array_flip($array),24);
		foreach($randarray as $ky => $value) {
		        $key .= $value;
		}
		system("echo $rand > /site/data/$key");
		system("cd /tmp/$userid.dump/ && tar czf /site/data/$rand.tar.gz *");
		echo "</ul>";
		echo "</p><p> => Done </p>";
		echo "<p>";
		$size = filesize("/site/data/$rand.tar.gz");
		// format size
		function format_size($size) {
   		   $sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
		      if ($size == 0) { return('n/a'); } else {
		      return (round($size/pow(1024, ($i = floor(log($size, 1024)))), $i > 1 ? 2 : 0) . $sizes[$i]); }
		}
		$size = format_size($size);
		echo "<span class=nadruk>Download:</span> The Dump is complete. FYI, the filesize is $size:";
		#$size = filesize("/site/data/$rand.tar.gz");
		echo "<ol><li>Download the archive and upload it later to the target server. Keep in mind that upload of >2Gb files is NOT supported !</li><li>Provide the download key to the target server. This will download the file directly to the target server.</ol>";
		echo "</p><p>";
		echo "<span class=italic>Option 1: </span><a href='data/$rand.tar.gz'>Right-Click and Save-As here</a> To download it.<br/>";
		echo "<span class=italic>Option 2: </span>Copy this download link to the target server : https://$domain/$basepath/keycheck.php?key=$key<br/>";
		echo "</p><p>";
		echo "<span class=nadruk>Note:</span> Please click <a href='index.php?page=dumpprojects&key=$key'>here</a> after the file is downloaded to delete the file on our server. </p>";
		#system("rm -Rf /tmp/$userid.dump/");
		echo "</div>";
	}
}

?>
