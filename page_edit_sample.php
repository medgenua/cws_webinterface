<?php

if (!isset($loggedin) || $loggedin != 1) {
	include('page_login.php');
	exit;
}

// variables
if (isset($_GET['cus'])) {
	$cstm = $_GET['cus'];
	$table = 'cus_';
}
else {
	$cstm = 0;
	$table = '';
}
$sid = $_POST['sid'];
$pid = $_POST['pid'];

// permissions? 
$query = mysql_query("SELECT editsample FROM $table"."projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
if (mysql_num_rows($query) == 0) {
	echo "<div class=sectie>";
	echo "<h3>Access Denied</h3>";
	echo "</div>";
	exit();
}
$row = mysql_fetch_array($query);
if ($row['editsample'] != 1) {
		echo "<div class=sectie>";
	echo "<h3>Access Denied</h3>";
	echo "</div>";
	exit();
}

// process
if (isset($_POST['updatesample'])) {
  if ($cstm != 1) {
	//todo
  }
  else {
	$sname = addslashes($_POST['sname']);
	$sgender = $_POST['sgender'];
	$sid = $_POST['sid'];
	$pid = $_POST['pid'];
	$splatform = addslashes($_POST['splatform']);
	$schiptype = addslashes($_POST['schiptype']);
	$ok = 1;
	if ($sname != $sid) {
		// check if no overlap.
		$query = mysql_query("SELECT idsamp FROM `cus_sample` WHERE idsamp = '$sname' AND idproj = '$pid'");
		if (mysql_num_rows($query) > 0) {
			echo "<div class=sectie>";
			echo "<h3>Error: Samplename already exists in this project</h3>";
			echo "<p>All sample names within a project must be unique. Please provide another name. </p>";
			echo "</div>";
			$ok = 0;
		}
	}
	if ($ok == 1) {
		$query = mysql_query("UPDATE `cus_sample` SET idsamp = '$sname', gender = '$sgender', platform = '$splatform', chiptype = '$schiptype' WHERE idsamp = '$sid' AND idproj = '$pid'");
		$query = mysql_query("UPDATE `cus_sample_x_user_annotation` SET sid = '$sname' WHERE sid = '$sid' AND pid = '$pid'");
		$query = mysql_query("UPDATE `cus_aberration` SET sample = '$sname' WHERE sample = '$sid' AND idproj = '$pid'");
		$query = mysql_query("UPDATE `cus_deletedcnvs` SET sample = '$sname' WHERE sample = '$sid' AND idproj = '$pid'");
		$query = mysql_query("UPDATE `cus_log` SET sid = '$sname' WHERE sid = '$sid' AND idproj = '$pid'");
		$query = mysql_query("UPDATE `cus_prioritize` SET sample = '$sname' WHERE sample = '$sid' AND project = '$pid'");
		$query = mysql_query("UPDATE `cus_phenotype` SET sid = '$sname' WHERE sid = '$sid' AND pid = '$pid'");
			
		$_POST['sid'] = $sname;
		// user annotations
		$query = mysql_query("SELECT id FROM `user_annotations` WHERE type = 'sample' ORDER BY name");
		while ($row = mysql_fetch_array($query)) {
			if ($_POST['ua_'.$row['id']] == '') {	
				mysql_query("DELETE FROM `cus_sample_x_user_annotation` WHERE sid = '$sname' AND pid = '$pid' AND aid = '".$row['id']."'");
			}
			else {
				mysql_query("INSERT INTO `cus_sample_x_user_annotation` (sid,pid,aid,value) VALUES ('$sname','$pid','".$row['id']."','".$_POST['ua_'.$row['id']]."') ON DUPLICATE KEY UPDATE value = '".$_POST['ua_'.$row['id']]."'");	
			}
		}

	}
  }			
}
$sid = $_POST['sid'];
// Print the values.
echo "<div class=sectie>";
echo "<h3>Edit Sample Details</h3>";
echo "<p>Change the needed values in the fields below, and press update to save the changes. Close this page to cancel.</p>";
echo "<form action='index.php?page=edit_sample&cus=$cstm' method=POST>";
echo "<input type=hidden name=sid value='$sid'>";
echo "<input type=hidden name=pid value='$pid'>";
// Main details
if ($cstm != 1) {
	$query = mysql_query("SELECT chip_dnanr, gender FROM `sample` WHERE id = '$sid'");
	$row = mysql_fetch_array($query);
	echo "Sample Name: <input type=text name=sname value='".$row['chip_dnanr']."' value='".$row['chip_dnanr']."'> (this might break genome-studio interaction!)<br/>";
	echo "Sample Gender: <select name=sgender>";
	if ($row['gender'] == 'Male') {
		echo "<option value=Male SELECTED>Male</option><option value=Female>Female</option>";
	}
	elseif ($row['gender'] == 'Female') {
		 echo "<option value=Male >Male</option><option value=Female SELECTED>Female</option>";
	}
	else {
		 echo "<option value=Male >Male</option><option value=Female >Female</option>";
	}
	echo "</select><br/>";
	
}
// custom samples
else {
	// default items
	$query = mysql_query("SELECT idsamp, gender, chiptype, platform FROM `cus_sample` WHERE idsamp = '$sid' and idproj = '$pid'");
	$row = mysql_fetch_array($query);
	echo "Sample Name: <input type=text name=sname value='".$row['idsamp']."'> (this might break genome-studio interaction!)<br/>";
	echo "Sample Gender: <select name=sgender>";
	if ($row['gender'] == 'Male') {
		echo "<option value=''>Unknown</option><option value=Male SELECTED>Male</option><option value=Female>Female</option>";
	}
	elseif ($row['gender'] == 'Female') {
		 echo "<option value=''>Unknown</option><option value=Male >Male</option><option value=Female SELECTED>Female</option>";
	}
	else {
		 echo "<option value=''>Unknown</option><option value=Male >Male</option><option value=Female >Female</option>";
	}
	echo "</select><br/>";
	echo "Platform: <input type=text name=splatform value='".$row['platform']."'><br/>";
	echo "Chiptype: <input type=text name=schiptype value='".$row['chiptype']."'><br/>";
	// user annotations
	$query = mysql_query("SELECT cu.value, cu.aid FROM `cus_sample_x_user_annotation` cu WHERE cu.sid = '$sid'");
	$cua = array();
	while ($row = mysql_fetch_array($query)) {
		$cua[$row['aid']] = $row['value'];
	}
	$query = mysql_query("SELECT id, name, description FROM `user_annotations` WHERE type = 'sample' ORDER BY name");
	$col = 0;
	while ($row = mysql_fetch_array($query)) {
		echo $row['name'].": ";
		if (isset($cua[$row['id']])) {
			$val = $cua[$row['id']];
		}
		else {
			$val = '';
		}
		echo "<input type=text size=30 name=ua_".$row['id']." value='$val'><br/>";
	}


}	
echo "<input type=submit class=button name=updatesample value='Save Changes' />";
echo "</form>";
echo "</p><p><a href='index.php?page=cusdetails&project=$pid&sample=$sid'>Back to CNV details</a></p>";
echo "</div>";
?>
