<?php

if (!isset($_SESSION['logged_in']) || $level < 3) {
	echo "<div class=sectie><h3>Access Denied</h3></div>";
	exit();
}

if (!isset($_POST['copynumber'])) {
	echo "<div class=sectie><h3>Estimate experimental logR values</h3></div>";
	echo "<p>";
	echo "<form action='index.php?page=estimate_levels' method=POST>";
	echo "Select type the copy number to estimate logR values for: <select name=copynumber><option value='0'>0</options><option value='1'>1</option><option value='3'>3</options><option value=4>4</option></select>";
	echo "</p><p>Select the chiptypes to include in the estimate.:<br/>";
	$query = mysql_query("SELECT ID, name FROM chiptypes ORDER BY name");
	while ($row = mysql_fetch_array($query)) {
		$id = $row['ID'];
		$name = $row['name'];
		echo "<input type=checkbox name='chips[]' value=$id> $name <br/>";
	} 
	echo "</p>";
	echo "<p>To decrease the selection load, only CNVs of at least 75 probes will be included.</p><p>";
	echo "<input type=hidden name=idx value=0>";
	echo "<input type=hidden name=lastaid value=0>";
	echo "<input type=hidden name='aids' value=''>";
	echo "<input type=submit class=button value='go'></form></div>";
	exit();
}

// process if 'add' is set
if (isset($_POST['add'])) {
	$aids = $_POST['aids'] . $_POST['lastaid'] . '|';
}
else {
	$aids = $_POST['aids'];
} 

// show a cnv

echo "<div class=sectie>"; 
$item = $_POST['idx'];
if (isset($_POST['chips'])) {
	$chipids = implode(',',$_POST['chips']);
}
else {
	$chipids = $_POST['chipids'];
}
if ($chipids == '') {
	echo "no chips specified. exiting!";
	exit();
}
$cn = $_POST['copynumber'];
$lastaid = $_POST['lastaid'];
$item++;
if (!isset($_POST['total'])) {
	if ($cn == 4) {
		$nr = 50;
	}
	else {
		$nr = 75;
	}
	$query = mysql_query("SELECT COUNT(a.id) as nr FROM aberration a JOIN projsamp ps JOIN project p ON ps.idsamp = a.sample AND ps.idproj = a.idproj AND p.id = a.idproj WHERE ps.LRRSD < 0.2 AND a.cn = '$cn' AND a.nrsnps >= $nr AND p.chiptypeid IN ($chipids) AND p.collection <> 'Controls'");
	$row = mysql_fetch_array($query);
	$total = $row['nr'];
}
else {
	$total = $_POST['total'];
}
#$_POST['calculate'] = 1;
if (isset($_POST['calculate'])) {
	echo "<h3>Calculation of logr statistics for currently selected CNVs</h3>";
	echo "<p>";
	flush();
	// get aids array
	$aidss = substr($aids,0,-1);
	$aidss = str_replace('|',',',$aidss);
	// set variables
	$logRs = array();
	// loop cnvs
	$sumquery = mysql_query("SELECT a.start, a.stop, d.content, d.structure FROM aberration a JOIN datapoints d ON a.id = d.id WHERE a.id IN ($aidss) ");
	while ($row = mysql_fetch_array($sumquery)) {
		$start = $row['start'];
		$stop = $row['stop'];
		$content = $row['content'];
		$structure = $row['structure'];
		echo "struc: $structure<br/>";
		$content = explode('_',$content);
		$incr = array_sum(str_split($structure));
		if (substr($structure,1,1) == 0) {
			echo "missing logR values<br/>";
			continue;
		}
		for ($i = 0; $i<count($content);$i = $i+$incr) {
			//$logRnum++;
			if ($content[$i] >= $start && $content[$i] <= $stop) {
				array_push($logRs,$content[$i + 1]);
			}
			//$logRsum += $content[$i + 1]; 
		} 
	}
	// trim logRs (5th to 95th percentile)
	asort($logRs);
	echo "total logR values: ". count($logRs)."<br/>";
	echo "total avg: ". (array_sum($logRs)/count($logRs))."<br/>";
	$nr = count($logRs);
	$startnr = ceil($nr*0.05);
	$stopnr = floor($nr*0.95);
	$keepnr = $stopnr - $startnr + 1;
	$logRs = array_slice($logRs,$startnr,$keepnr);
	echo "trimmed (5th-95th perc.) logR values: ". count($logRs)."<br/>";
	echo "trimmed avg: ". (array_sum($logRs)/count($logRs))."<br/>";
	$sample_count = count($logRs);
	//for ($current_sample = 0; $sample_count > $current_sample; ++$current_sample) $sample_square[$current_sample] = pow($logRs[$current_sample], 2);

	//$standard_deviation = sqrt(array_sum($sample_square) / $sample_count - pow((array_sum($logRs) / $sample_count), 2));	
	function sd_square($x, $mean) { return pow($x - $mean,2); }
	// Function to calculate standard deviation (uses sd_square)    
	function sd($array) {
		// square root of sum of squares devided by N-1
		return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
	}
	echo "trimmed stdev : ".sd($logRs) . "<br/>";//$standard_deviation<br/>";
	echo "</p>";
	echo "</div><div class=sectie>";
}


if ($item > $total) {
	echo "last item reached. press process to get the average logR";
	// output form
	echo "<p><form action='index.php?page=estimate_levels' method=POST>";
	echo "<input type=hidden name=copynumber value='$cn'>";
	echo "<input type=hidden name=total value='$total'>";
	echo "<input type=hidden name=idx value='$item'>";
	echo "<input type=hidden name=lastaid = value='$aid'>";
	echo "<input type=hidden name=aids value='$aids'>";
	echo "<input type=hidden name=chipids value='$chipids'>";
	echo "<input type=submit class=button name=calculate value='Calculate average logR'> </form></p>";
	exit();
}

// get details
if ($cn == 4) {
	$nr = 50;
}
else {
	$nr = 75;
}
$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.nrsnps, s.chip_dnanr, s.gender, ps.callrate, ps.LRRSD, ps.INIWAVE , p.chiptype FROM aberration a JOIN sample s JOIN projsamp ps JOIN project p ON s.id = a.sample AND ps.idsamp = a.sample AND ps.idproj = a.idproj AND p.id = a.idproj WHERE a.cn = '$cn' AND a.id > '$lastaid' AND a.nrsnps >= $nr AND p.collection != 'Controls' AND ps.LRRSD < 0.2 AND p.chiptypeid IN ($chipids) ORDER BY a.id LIMIT 1");
$row = mysql_fetch_array($query);
$aid = $row['id'];
$chr = $row['chr'];
if ($chr == 23) {
	$chr = 'X';
}
$start = $row['start'];
$stop = $row['stop'];
$size = $stop-$start+1;
$probes = $row['nrsnps'];
$sname = $row['chip_dnanr'];
$gender = $row['gender'];
$cr = $row['callrate'];
$lrr = $row['LRRSD'];
$ini = $row['INIWAVE']; 
$chip = $row['chiptype'];
echo "<h3>Estimation of mean logR for CNVs of cn $cn : Candidate $item/$total</h3>";
// output details
echo "<div style='width:30%;float:left'>";
echo "<p>Sample : $sname<br/>";
echo "Gender : $gender<br/>";
echo "Chiptype: $chip<br/>";
if ($lrr > 0.2 || $iniwave != '') {
	echo "<span style='color:red'>Quality: </span>LRR.SD: $lrr<br/>";
	echo "<span style='color:red'>Quality: </span>Normalised for Wave<br/>";
}
else {
	echo "Quality: LRR.SD: $lrr<br/>";
}

echo "CNV region: Chr$chr:".number_format($start,0,'',',') . '-'. number_format($stop,0,'',',').'<br/>';
echo "CNV size: ".number_format($size,0,'',',') . "bp<br/>";
echo "CNV probes: $probes<br/>";
echo "</p>";
// output form
echo "<p><form action='index.php?page=estimate_levels' method=POST>";
echo "<input type=hidden name=copynumber value='$cn'>";
echo "<input type=hidden name=total value='$total'>";
echo "<input type=hidden name=idx value='$item'>";
echo "<input type=hidden name=lastaid = value='$aid'>";
echo "<input type=hidden name=aids value='$aids'>";
echo "<input type=hidden name=chipids value='$chipids'>";
echo "<input type=submit class=button name=add value='add CNV to estimate'> &nbsp; &nbsp; <input type=submit class=button name=discard value='Discard CNV'></p>";
echo "<p><input type=submit class=button name=calculate value='Calculate average logR on current selection'> </form></p>";

echo "</div>";

// plot? 
echo "<div style='float:right;width:40%'>";
echo "<img src='plot.php?aid=$aid&u=$userid&cstm=0' border=0 >";
echo "</div>";


?>
