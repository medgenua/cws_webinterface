<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>
<script language="javascript" type="text/javascript">

<!--
function popitup(url) {
	newwindow=window.open(url,'name','height=200,width=150,location=no,menubar=no,status=no,screenX=400,screenY=200');
	if (window.focus) {newwindow.focus()}
	return false;
}
// -->
</script>
<?php
ob_start();
if ($loggedin != 1){ 
	include('page_login.php');
}
else {

$ucscdb = str_replace('-','',$_SESSION['dbname']);

/// START PAGE
echo "<div class=sectie>\n";
echo "<h3>Filter and reorder your results</h3>\n";
echo "<p>";
echo "<h4>Hover column headers for more information</h4>\n ";
echo "<p><table cellspacing=0 >\n";
echo " <tr>\n"; 
echo "  <th scope=col class='topcellalt' title='Chromosomal position using build $ucscdb' $firstcell>Location</th>\n";
echo "  <th scope=col class='topcellalt' title='Predicted Copy Number'>CN</th>\n";
echo "  <th scope=col class='topcellalt'>Size</th>\n";
echo "  <th scope=col class='topcellalt' title='Sample: click to go to details page'>Sample</th>\n";
echo "  <th scope=col class='topcellalt' title='Diagnostic Class: 1=Published region ; 2=Considered pathogenic ; 3=Unclear significance ; 4=Considered polymorphism'>DC</th>\n";
echo "  <th scope=col class='topcellalt' title='Inheritance: DN=De Novo ; P=Paternal ; M=Maternal ; empty=Not defined'>Inher.</th>\n";
echo "  <th scope=col class='topcellalt' title=\"Aberration is seen in % of samples from : same collection, same chip / same collection, all chips / controls, same chip\" >Seen (%)</th>\n";
echo "  <th scope=col class=topcellalt title=\"RefSeq Genes: BLUE = OMIM; RED = MORBID\">#RS</th>\n";
echo "  <th scope=col class='topcellalt' title='Nr of affected exons'># Exons</th>\n";
echo "  <th scope=col class='topcellalt' title='Set CNV to specific Diagnostic class'>Set DC</th>\n";
echo " </tr>\n";	

// NEEDED ARRAYS AND VARS
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inh = array(0=>'', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");




$querystring = "SELECT aid, pid, sid, chip_dnanr, cn, start, stop, chr, class, inh, seeninchip, seenall, seencontrols, hasgenes, hasomim, hasmorbid, hasexons FROM scantable WHERE 1 ORDER BY hasmorbid DESC, chr ASC, start ASC ";
$abquery = mysql_query($querystring);

while($row = mysql_fetch_array($abquery)) {
	// GET CNV details
	$aid = $row['aid'];
	$pid = $row['pid'];
	$sid = $row['sid'];
	$sname = $row['chip_dnanr'];
	$cn = $row['cn'];
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$class= $row['class'];
	$inheritance = $row['inh'];
	$seeninchip = $row['seeninchip'];
	$seenall = $row['seenall'];
	$seenincontrols = $row['seencontrols'];
	$hasgenes = $row['hasgenes'];
	$hasomim = $row['hasomim'];
	$hasmorbid = $row['hasmorbid'];
	$hasexons = $row['hasexons'];
	$chrtxt = $chromhash[ $chr ];
	// Determine Cyto Band
	$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
	$cytorow = mysql_fetch_array($cytoquery);
	$cytostart = $cytorow['name'];
	$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$end' BETWEEN start AND stop)");
	$cytorow = mysql_fetch_array($cytoquery);
	$cytostop = $cytorow['name'];
	if ($cytostart == $cytostop) {
		$cytoband = "($chrtxt$cytostart)";
	}
	else {
		$cytoband = "($chrtxt$cytostart-$cytostop)";
	}  
	$size = $stop - $start +1 ;
      	$location = "chr". $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',') ;
	// DETERMINE GENE LINK COLOR
	if ($hasmorbid == 1) {
		$fontstyle = "style='color:red'";
	}
	elseif ($hasomim == 1) {
		$fontstyle = "style='color:blue'";
	}
	else {
		$fontstyle = "style='color:black'";
	}
      	echo " <tr context='abrow' aid='$aid' uid='$userid'>\n";
	echo "  <td $tdtype[$switch] $firstcell >$location<br/>$cytoband</td>\n";
      	echo "  <td $tdtype[$switch] onmouseover=\"Tip(MakeTitle('$aid'), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\">$cn</td>\n";
      	echo "  <td $tdtype[$switch]>" . number_format($size,0,'',',') . "</td>\n";
      	echo "  <td $tdtype[$switch] ><a href='index.php?page=details&project=$pid&sample=$sid' target=new>$sname</a></td>\n";
      	echo "  <td $tdtype[$switch] >$class&nbsp;</td>\n";
      	echo "  <td $tdtype[$switch] >$inh[$inheritance]&nbsp;</td>\n";
      	//echo "  <td $tdtype[$switch] NOWRAP>$seenby</td>\n";
      	echo "  <td $tdtype[$switch]>$seeninchip / $seenall / $seenincontrols</td>\n";
	if ($hasgenes > 0) {
	      echo "  <td $tdtype[$switch] ><a href='index.php?page=genes&amp;chr=$chrtxt&amp;start=$start&amp;stop=$stop' target='new' $fontstyle>$hasgenes</a></td>\n";
      	}
      	else {
	     echo "   <td $tdtype[$switch]>$hasgenes</td>\n";
      	}
	if ($hasexons > 0) {
	      echo "  <td $tdtype[$switch] >$hasexons</td>\n";
      	}
      	else {
	     echo "   <td $tdtype[$switch]>&nbsp;</td>\n";
      	}
	echo "<td $tdtype[$switch] >";
	echo "<a href=# onclick=\"return popitup('setclass.php?aid=$aid&class=1&u=$userid')\">1</a>&nbsp;";
	echo "<a href=# onclick=\"return popitup('setclass.php?aid=$aid&class=2&u=$userid')\">2</a>&nbsp;";
	echo "<a href=# onclick=\"return popitup('setclass.php?aid=$aid&class=3&u=$userid')\">3</a>&nbsp;";
	echo "<a href=# onclick=\"return popitup('setclass.php?aid=$aid&class=4&u=$userid')\">4</a></td>\n";
	echo "</tr>\n";
}



/// END CHECK LOGIN BLOCK
}
?>
</div>


<div id="txtHint">something must appear here</div>
<!-- context menu loading -->
<script type="text/javascript" src="javascripts/details_context.js"></script>

