<?php
ob_start();
if (! isset($loggedin) || $loggedin != 1){
	include('page_login.php');
	exit();
}

for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}

$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["x"] = "23";
$chromhash["y"] = "24";

$chromhash["23"] = "X";
$chromhash["24"] = "Y";

// get action
$type = $_GET['type'];

// first if new filter created (overrules type)
if (isset($_POST['createfilter'])) {
	$filtername = addslashes($_POST['filtername']);
	$filtercomment = addslashes($_POST['filtercomment']);
	$public = 0;
	if (isset($_POST['filterpublic'])) {
		$public = 1;
	}
	// create filter
	$query = mysql_query("INSERT INTO `Filters` (Name, Comment, Public, uid) VALUES ('$filtername','$filtercomment','$public','$userid')");
	$fid = mysql_insert_id();
	// add permissions entry
	$query = mysql_query("INSERT INTO `Filters_x_Userpermissions` (fid, uid, editing,sharing) VALUES ('$fid','$userid','1','1')");
	// overrule type
	$_POST['editfilterid'] = $fid;
	$_POST['st'] = 'add';
	$type = 'edit';
}

// 1. present list of filters to manage
if ($type == 'manage') {
	echo "<div class=sectie><h3>Edit Filters</h3>";
	// get filters
	$query = mysql_query("SELECT f.fid, f.Name, f.Comment, f.Public, fu.sharing FROM `Filters_x_Userpermissions` fu JOIN `Filters` f  ON fu.fid = f.fid WHERE fu.uid = $userid AND fu.editing = 1 ORDER BY Name");
	// , COUNT(fr.rid) AS nrR RIGHT JOIN `Filters_x_Regions` fr  AND f.fid = fr.fid
	if (mysql_num_rows($query) == 0) {
		echo "<p>You do not have any filters.</p>";
	}
	else {
		echo "<p>Select a filter below using the radio buttons and click 'Edit'. Editing includes adding or removing regions, adding comments to regions, etc.</p>";
		echo "<p><form action='index.php?page=filters&type=edit' method='POST'><table cellspacing=0><tr><th class=topcellalt $firstcell>Edit</th><th class=topcellalt>Name</th><th class=topcellalt>Comment</th><th class=topcellalt>Number of Regions</th><th class=topcellalt>Set Access Permissions</th></tr>";
		while ($row = mysql_fetch_array($query)) {
			echo "<tr>";
			echo "<td $firstcell><input type=radio name=editfilterid value=".$row['fid']."></td>";
			echo "<td>".$row['Name']."</td>";
			echo "<td>".$row['Comment']."&nbsp;</td>";
			$nrq = mysql_query("SELECT rid FROM `Filters_x_Regions` WHERE fid = '".$row['fid']."'");
			$nrr = mysql_num_rows($nrq);
			echo "<td>$nrr</td>";
			if ($row['sharing'] == 1) {
				echo "<td><a href='index.php?page=filters&type=share&fid=".$row['fid']."'>Share</a></td>";
			}
			else {
				echo "<td>&nbsp;</td>";
			}
			echo "</tr>";	
		}
		echo "</table></p>";
		echo "<p><input type=submit class=button name='EditFilter' value='Edit'></form></p></div>";
		exit();
	}
}
// 2. edit the selected and submitted filter
elseif ($type == 'edit') {
	// filter selected?
	if (!isset($_POST['editfilterid']) || !is_numeric($_POST['editfilterid'])) {
		print "<div class=sectie><h3>Problem detected</h3>";
		print "<p>No filter was selected. Please go back and select a filter.</p>";
		print "</div>";
		exit();
	}
	$fid = $_POST['editfilterid'];
	// details were updated?
	if (isset($_POST['UpdateGeneral'])) {
		$newname = addslashes($_POST['filtername']);
		$newcomment = addslashes($_POST['filtercomment']);
		if (isset($_POST['filterpublic'])) {
			$newpublic = 1;
		}
		else {
			$newpublic = 0;
		}
		$query = mysql_query("UPDATE `Filters` SET Name = '$newname', Comment = '$newcomment', Public = $newpublic WHERE fid = $fid");
	}


	// get details. 
	$query = mysql_query("SELECT f.Name, f.Comment, f.Public FROM `Filters` f WHERE f.uid = $userid AND f.fid = $fid");
	$row = mysql_fetch_array($query);
	echo "<div class=sectie>";
	echo "<h3>Editing: '" . stripslashes($row['Name']). "'</h3>";
	// selection : general/add/edit. 
	if (isset($_POST['st'])) {
		$st = $_POST['st'];
	}
	else {
		$st = 'general';
	}
	$options = array('general' => 'Edit general details','add' => 'Add Extra Regions','edit' => 'Edit or Remove Regions');
	echo "<p><form action='index.php?page=filters&type=edit' method='POST'><input type=hidden name=editfilterid value=$fid>";
	echo "<span class=nadruk >Select action:</span></p>";
	echo "<p class=indent><select name='st'>";
	foreach ( $options as $action => $comment) {
		if ($action == $st) {
			echo "<option value='$action' selected>$comment</option>";
		}
		else {
			echo "<option value='$action'>$comment</option>";
		}
	}
	echo "</select> &nbsp; <input type=submit class=button name='EditFilter' value='Select'></form></p>";
	
	// based on action, print output.
	if ($st == 'general') {
		echo "<p><span class=nadruk >Edit general details:</span></p>";
		echo "<p>";
		echo "<form action='index.php?page=filters&type=edit' method='POST'>";
		echo "<input type=hidden name=editfilterid value=$fid><input type=hidden name=st value='$st'>";
		echo "<span class=indent style='width:1.2em;float:left'>1.</span><span style='width:10em;float:left' >Filter name:</span><span style='float:toleft;'><input type=text size=30 name=filtername value='".stripslashes($row['Name'])."'></span><br/>";
		echo "<span class=indent style='width:1.2em;float:left'>2.</span><span style='width:10em;float:left' >Filter description:</span><span style='float:toleft;'><input type=text name=filtercomment size=30 value='".stripslashes($row['Comment'])."'></span><br/>";
		if ($row['Public'] ==  1) {
			$checked = 'CHECKED';
		}
		else {
			$checked = '';
		}
		echo "<span class=indent style='width:1.2em;float:left'>3.</span><span style='width:10em;float:left' >Filter is public: </span><span style='float:toleft;'><input type=checkbox name=filterpublic value=1 $checked></span></p>";
		echo "<p class=indent><input type=submit class=button name='UpdateGeneral' value='Save'></form></p>";
	}
	elseif ($st == 'add') {
		## process added regions
		if (isset($_POST['submitadd'])) {
			## merge ?
			if (isset($_POST['addmerge'])) {
				$merge = 1;
			}
			else {
				$merge = 0;
			}
			$newstyle = $_POST['newstyle'];
			## get regions from pasted values
			$new = 0;
			$merged = 0;
			$newregions = array();
			if ($newstyle == 'paste') {
				$regions = explode("\n",$_POST['addpaste']);
				foreach ($regions as $key => $string) {
					$p = explode(";",$string);
					$region = array_shift($p);
					$cn = 3;
					if (strtolower($p[0]) == 'gain' || strtolower($p[0]) == 'dup') {
						$cn = 2;
						$shift = array_shift($p);
					}
					elseif (strtolower($p[0]) == 'loss' || strtolower($p[0]) == 'del') {
						$cn = 1;
						$shift = array_shift($p);
					}
					$comment = implode(';',$p);
					$region=trim($region);
					if (!preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $region)) {
						echo "INVALID FORMAT: $region<br />";
						continue;
					}
					$pieces = preg_split("/[:\-_+]/",$region);
					$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
					if ($chrtxt == '23' || $chrtxt == '24'){
						$chrtxt = $chromhash[$chrtxt];
					}
					$chr = $chromhash[ $chrtxt ] ;
					$start = $pieces[1];
					$start = str_replace(",","",$start);
					if ($start < 0) {
						$start = 0;
					}
					$end = $pieces[2];
					$end = str_replace(",","",$end);
					// check end
					$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
					$nrow = mysql_fetch_array($nq);
					$chrstop = $nrow['stop'];
					if ($end > $chrstop) {
						$end = $chrstop;
					}
					$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
					$newregions[$chr][$start.'-'.$end]['comment'] = $comment;
					$newregions[$chr][$start.'-'.$end]['cn'] = $cn;
				}
				
			}
			## get regions from file
			elseif($newstyle == 'file') {
				if (basename( $_FILES['addfile']['name']) != "") {
					$fh = fopen($_FILES['addfile']['tmp_name'],'r');
					while(!feof($fh)) {
						$line = fgets($fh);
						$line = rtrim($line);
						if ($line == '') {
							continue;
						}
						$pieces = explode("\t",$line);
						$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
						if ($chrtxt == '23' || $chrtxt == '24'){
							$chrtxt = $chromhash[$chrtxt];
						}
						$chr = $chromhash[ $chrtxt ] ;
						$start = $pieces[1];
						$start = str_replace(",","",$start);
						if (!is_numeric($start)) {
							continue;
						}
						if ($start < 0) {
							$start = 0;
						}
						$end = $pieces[2];
						$end = str_replace(",","",$end);
						if (!is_numeric($end)){
							continue;
						}
						// check end
						$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
						$nrow = mysql_fetch_array($nq);
						$chrstop = $nrow['stop'];
						if ($end > $chrstop) {
							$end = $chrstop;
						}
						$cn = 3;
						if (count($pieces) == 5) {
							$comment = $pieces[4];
							if (strtolower($pieces[3]) == 'gain' || strtolower($pieces[3]) == 'dup') {
								$cn = 2;
								$shift = array_shift($p);
							}
							elseif (strtolower($p[0]) == 'loss' || strtolower($p[0]) == 'del') {
								$cn = 1;
								$shift = array_shift($p);
							}
						}
						else {
							$comment = $pieces[3];
						}
						if (array_key_exists($start.'-'.$end,$newregions[$chr])) {
							## identical region found. merge now !
							$merged++;
						 	if (preg_match("/MERGED REGION/",$newregions[$chr][$start.'-'.$end])) {
								$newregions[$chr][$start.'-'.$end]['comment'] .= "\n$comment";
							}
							else {
								$newregions[$chr][$start.'-'.$end]['comment'] = "MERGED REGION\n#############\n".$newregions[$chr][$start.'-'.$end]."\n$comment";
							}
							if ($newregions[$chr][$start.'-'.$end]['cn'] != $cn) {
								// if they do not match, it's both.
								$newregions[$chr][$start.'-'.$end]['cn'] = 3;
							}
						}
						else {
							$newregions[$chr][$start.'-'.$end]['comment'] = $comment;
							$newregions[$chr][$start.'-'.$end]['cn'] = $cn;
						}
					}
					fclose($fh);		

				}
			}
			foreach($newregions as $chr => $array) {
				## get regions for this filter from database if merge is needed.
				if ($merge == 1) {
					## check database for each region => merges overlapping regions in input file at initial submission.
					foreach($array as $ss => $sarray) {
						$q = explode('-',$ss);
						$comment = $sarray['comment'];
						$cn = $sarray['cn'];
						$query = mysql_query("SELECT rid, start, stop, cn, Comment FROM `Filters_x_Regions` WHERE fid = '$fid' AND chr = '$chr'");
						while ($row = mysql_fetch_array($query)) {
							## check overlapping regions in db, if so merge and delete from newregions.
							if ($row['stop'] < $q[0]) {
								# dbregion stop < newregion start
								continue;
							}
							if ($row['start'] > $q[1]) {
								# dbregion start > newregion stop
								continue;
							}
							## here == overlapping
							$dbstart = $row['start'];
							$dbstop = $row['stop'];
							$nstart = min($q[0],$dbstart);
							$nstop = max($q[1],$dbstop);
							$dbcn = $row['cn'];
							$dbcomm = stripslashes($row['Comment']);
							## merged before?
							if (preg_match("/MERGED REGION/", $dbcomm)) {
								$ncomm = addslashes($dbcomm."\n".$comment);
							}
							else {
								$ncomm = addslashes("MERGED REGION\n#############\n$dbcomm\n$comment");
							}
							$rid = $row['rid'];
							if ($dbcn != $cn) {
								$ncn = 3;
							}
							else {
								$ncn = $cn;
							}
							## update in db
							mysql_query("UPDATE `Filters_x_Regions` SET start = '$nstart',stop = '$nstop', cn = '$ncn', Comment = '$ncomm' WHERE rid = '$rid'");
							$merged++;
							## remove from $array
							unset($array[$ss]);
							## goto next item in $array !
							continue 2; 	
						}
						## store item as new if no overlap was found
						$start = $q[0];
						$end = $q[1];
						$comment = addslashes($comment);
						mysql_query("INSERT INTO `Filters_x_Regions` (fid, chr, start, stop, cn, Comment) VALUES ('$fid','$chr','$start','$end','$cn','$comment')");
						$new++;
					}

				}
				else {
					## no merging, just insert !
					foreach ($array as $ss => $sarray) {
						$q = split('-',$ss);
						$start = $q[0];
						$end = $q[1];
						$comment = $sarray['comment'];
						$cn = $sarray['cn'];
						$comment = addslashes($comment);
						mysql_query("INSERT INTO `Filters_x_Regions` (fid, chr, start, stop, cn, Comment) VALUES ('$fid','$chr','$start','$end','$cn','$comment')");
						$new++;
					}
				}
			}
			echo "<p><span class=nadruk>Regions Added.</span><br/><span class=indent>New Regions: $new</span><br/><span class=indent>Updated Regions: $merged</span></p>";	
		}
		echo "<p><span class=nadruk>Add Regions:</span></p>";
		echo "<p class=indent>For uploaded files, provided regions should be tab seperated intervals. Needed column order is : chr;start;stop;CN;comment. The CN column should be any of 'gain', 'dup', 'loss', 'del', 'any' or 'both'. The comment column is optional and can contain any string shorter than 250 characters, shown as a title in graphical overviews. For pasted regions, the style follows UCSC notation, followed by a semicolon and the comment string (chr3:154000000-157000000;CN;Comment)</p>";
		echo "<form enctype='multipart/form-data' action='index.php?page=filters&type=edit' method='POST'>";
		echo "<input type=hidden name=editfilterid value=$fid><input type=hidden name=st value='$st'>";
		echo "<p class=indent>";
		## checkbox to merge overlapping intervals
		echo "<span class=indent style='width:2em;float:left'>1.</span><span style='width:15em;float:left' >Merge Overlapping Intervals:</span><span style='float:toleft;'><input type=checkbox name=addmerge value=1 checked></span><br/>";
		## field to provide regions by file
		echo "<span class=indent style='width:1.7em;float:left'>2a.</span><span style='width:15em;float:left' ><input type=radio name=newstyle value='file'>Upload Interval File:</span><span style='float:toleft;'><input type=file name='addfile' size='40' /></span><br/>";	
		## field to provide regions by copy-paste
		echo "<span class=indent style='width:1.7em;float:left'>2b.</span><span style='width:15em;float:left' ><input type=radio name=newstyle value='paste' checked>Paste Interval Regions:</span><span style='float:toleft;'><textarea name='addpaste' cols=40 rows=6></textarea></span><br/>";	
		## submit button 
		echo "<span class=indent><input type=submit name=submitadd value='Submit' class=button></span></form>";
		echo "</p>";
	}
	## EDIT REGIONS
	elseif ($st == 'edit'){
		## process changes to values
		if (isset($_POST['UpdateRegions'])) {
			## get regionIDs from db
			$query = mysql_query("SELECT rid FROM `Filters_x_Regions` WHERE fid = '$fid'");
			while ($row = mysql_fetch_array($query)) {
				$rid = $row['rid'];
				$chr = $chromhash[$_POST["chr"]["$rid"]];
				$start = $_POST["start"]["$rid"];
				$stop = $_POST["stop"]["$rid"];
				$cn = $_POST["cn"]["$rid"];
				$comment = addslashes($_POST["comment"]["$rid"]);
				$start = str_replace(",","",$start);
				$stop = str_replace(",","",$stop);
				if (!is_numeric($chr) || $chr == 0 || !is_numeric($start) || !is_numeric($stop)) {
					echo "<span class=nadruk>Error:</span> Region: $chr:$start:$stop is invalid. not updated.<br/>";
					continue;
				}
				if ($start < 0) {
					$start = 0;
				}
				## stop not checked for > chr.length (todo)
				mysql_query("UPDATE `Filters_x_Regions` SET chr = '$chr', start = '$start', stop = '$stop', cn = '$cn', Comment = '$comment' WHERE rid = '$rid'");
			}
		}
		elseif(isset($_POST['DeleteRegions'])) {
			$todelete = $_POST['rdel'];
			if (count($todelete) == 0) {
				echo "No regions selected to delete<br/>";
			}
			else {
				$string = implode(",",$todelete);
				mysql_query("DELETE FROM `Filters_x_Regions` WHERE rid IN ($string)");
			}	

		}
		echo "<p><span class=nadruk>Edit Regions</span></p>";
		echo "<p>All the regions in the current filter are listed below. Two actions can be taken from here. First, you can edit any or all fields, and press update at the end of the page. This will save the updated values to the database. Second, you can select regions using the checkboxes at the front of each row, and press delete to remove them from the filter. <br/><span class=nadruk>Note:</span> If you delete regions, changes to other regions will NOT be saved!</p>";
		echo "<form action='index.php?page=filters&type=edit' method='POST'>";	 
		echo "<input type=hidden name=editfilterid value=$fid><input type=hidden name=st value='$st'>";
		echo "<p class=indent>";
		echo "<table cellspacing=0 style='width:95%'>";
		echo "<tr><th class=light>Delete</th><th class=light>Chrom</th><th class=light>Start</th><th class=light>Stop</th><th class=light>CN</th><th class=light>Comment</th></tr>";
		$query = mysql_query("SELECT rid, chr, start, stop, cn, Comment FROM `Filters_x_Regions` WHERE fid = '$fid' ORDER BY chr ASC, start ASC, stop ASC");
		$cns = array("1" => "Loss", "2" => "Gain", "3" => "Both");
		while ($row = mysql_fetch_array($query)) {
			$rid = $row['rid'];
			$out = "<tr>";
			$out .= "<td class=light><input type=checkbox name='rdel[]' value='$rid' /></td>";
			$out .= "<td class=light><input type=text name='chr[$rid]' value='".$chromhash[$row['chr']]."' size=4 /></td>";
			$out .= "<td class=light><input type=text name='start[$rid]' value='".$row['start']."' size=10 /></td>";
			$out .= "<td class=light><input type=text name='stop[$rid]' value='".$row['stop']."' size=10 /></td>";
			$out .= "<td class=light><select name='cn[$rid]'>";
			for ($i = 1; $i <= 3 ; $i++) {
				$sel = '';
				if ($i == $row['cn']) {
					$sel = 'selected';
				}
				$out .= "<option value='$i' $sel>".$cns[$i]."</option>";
			}
			$out .= "</select></td>";
			$out .= "<td class=light><textarea name='comment[$rid]' cols=60 rows=3>".stripslashes($row['Comment'])."</textarea></td>";
			$out .= "</tr>";
			echo $out;
		}
		echo "<tr><td class=last colspan=6>&nbsp;</td></tr>";
		echo "</table></p>";
		echo "<p><input type=submit class=button value='Update Regions' name='UpdateRegions'> &nbsp; <input type=submit class=button value='Delete Selected Regions' name='DeleteRegions'></form></p>";

	}

	echo "</div>"; // sectie

}
// CREATE NEW FILTER
elseif($type == 'create') {
	echo "<div class=sectie><h3>Create new filter</h3>";
	echo "<p>Filters can be applied to CNV data to highlight important variants, or to remove benign variants from your data. To create a filter, first specify some general options here. In the next steps, you will be asked to provide the regions.</p>";
	echo "<p><form action='index.php?page=filters' method=POST>";
	echo "<ol>";
	echo "<li><input type=text size=30 name=filtername> : Filter Name</li>";
	echo "<li><input type=text size=30 name=filtercomment> : Additional Comments</li>";
	echo "<li><input type='checkbox' name=filterpublic value=1> : Make this filter public (usable by everybody)</li>";
	echo "</ol></p>";
	echo "<p><input type=submit class=button name='createfilter' value='Create Filter'></form></p>";
	echo "</div>";

}
// ACTIVATE FILTER
elseif($type == 'activate') {
	// process.
	if (isset($_POST['ActivateFilters'])) {
		//loop them fids
		foreach ($_POST['fids'] as $key => $fid) {
			mysql_query("INSERT INTO `Users_x_Filters` (uid, fid, Type,ApplyTo) VALUES ('$userid','$fid','".$_POST["type_$fid"]."', '".$_POST["at_$fid"]."' ) ON DUPLICATE KEY UPDATE Type = '".$_POST["type_$fid"]."',ApplyTo='".$_POST["at_$fid"]."'");
		}
	}

	// print filters 
	echo "<div class=sectie>";
	echo "<h3>(De)Activate Filters</h3>";
	echo "<p>Select what filters should be applied, and on which data. Take the following into account:</p>";
	echo "<p><span class=nadruk>Filter types:</span><br/>";
	echo "- Hard Negative : Blank out the region completely. Data are invisible<br/>";
	echo "- Soft Negative : Grey overlay in the region: Data remain visible<br/>";
	echo "- Positive : Red overlay in the region for emphasis. Put on top of 'Soft Negative'";
	echo "</p>";
	echo "<p><span class=nadruk>Apply To:</span><br/>";
	echo "- All : All the experimental tracks (Illumina and Non-Illumina)<br/>";
	echo "- Selection : Select specific collections of experiments";
	echo "</p>";
	echo "<form action='index.php?page=filters&type=activate' method=POST>";
	echo "<p><table cellspacing=0>";
	// own and shared filters
	echo "<tr><th $firstcell colspan=5>Your Filters &amp; Shared Filters</th></tr>";
	echo "<tr><th $firstcell class=topcellalt >Filter Name</th><th class=topcellalt>Nr.Regions</th><th class=topcellalt>Filter Type</th><th class=topcellalt>Apply To</th><th class=topcellalt>Track List (if Selection)</th></tr>";
	$query = mysql_query("SELECT f.fid, f.Name, f.Comment FROM `Filters_x_Userpermissions` fu JOIN `Filters` f  ON fu.fid = f.fid WHERE fu.uid = '$userid'  ORDER BY f.Name");
	$types = array("0" => "Disabled", "1" => "Hard Negative", "2" => "Soft Negative", "3" => "Positive");
	if (mysql_num_rows($query) == 0) {
		echo "<tr><td $firstcell colspan=5><span class=italic>No filters available</td></tr>";
	}
	// keep track of ids in first table part.
	$notin = '';
	while ($row = mysql_fetch_array($query)) {
		$fid = $row['fid'];
		$out = "<tr><td $firstcell title='".$row['Comment']."'><input type=hidden name='fids[]' value=$fid />".$row['Name']."</td>";
		$nrq = mysql_query("SELECT rid FROM `Filters_x_Regions` WHERE fid = '$fid'");
		$nrr = mysql_num_rows($nrq);
		$out .= "<td>$nrr</td>";
		// get type
		$atstring = '&nbsp;';
		$subquery = mysql_query("SELECT Type, ApplyTo FROM `Users_x_Filters` WHERE uid = '$userid' AND fid = '$fid'");
		if (mysql_num_rows($subquery) == 0) {
			$type = '0';
			$applyTo = 'Experimental';
		}	
		else {
			$row = mysql_fetch_array($subquery);
			$type = $row['Type'];
			$atstring = $row['ApplyTo'];
			if ($atstring != 'Experimental') {
				$applyTo = 'Selection';
			}
			else {
				$applyTo = 'Experimental';
			}
		}
		// type
		$out .= "<td><select name='type_$fid'>";
		for ($i = 0;$i<=3;$i++) {
			if ($type == $i) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			$out .= "<option value='$i' $sel>".$types[$i]."</option>";
		}
		$out .= "</select></td>";
		// Apply To
		// NOW: set to experimental
		$applyTo = 'Experimental';
		if ($applyTo == 'Experimental') {
			$out .= "<td><select name='at_$fid'><option SELECTED value='Experimental'>Experimental</option><option value='Selection' disabled>Selection (todo)</option></select></td><td id='tracklist_$fid'>&nbsp;</td>";
		}
		else {
			$out .= "<td><select name='at_$fid'><option value='Experimental'>Experimental</option><option SELECTED value='Selection'>Selection</option></select></td><td id='tracklist_$fid'>$atstring (option to edit, todo)</td>";
		}
		$out .= "</tr>";
		$notin .= "$fid,";
		echo $out;		
	}
	// public filters, not listed above.
	if ($notin != "") {
		$notin = "AND f.fid NOT IN (".substr($notin,0,-1).")";
	}
	echo "<tr><th $firstcell colspan=5>Public Filters</th></tr>";
	echo "<tr><th $firstcell class=topcellalt>Filter Name</th><th class=topcellalt>Nr.Regions</th><th class=topcellalt>Filter Type</th><th class=topcellalt>Apply To</th><th class=topcellalt>Track List (if Selection)</th></tr>";
	$query = mysql_query("SELECT f.fid, f.Name, f.Comment FROM `Filters` f  WHERE f.public = 1 $notin  ORDER BY f.Name");
	if (mysql_num_rows($query) == 0) {
		echo "<tr><td $firstcell colspan=5><span class=italic>No filters available</td></tr>";
	}

	while ($row = mysql_fetch_array($query)) {
		$fid = $row['fid'];
		$out = "<tr><td $firstcell title='".$row['Comment']."'><input type=hidden name='fids[]' value=$fid />".$row['Name']."</td>";
		$nrq = mysql_query("SELECT rid FROM `Filters_x_Regions` WHERE fid = '$fid'");
		$nrr = mysql_num_rows($nrq);
		$out .= "<td>$nrr</td>";
		
		// get type
		$atstring = '&nbsp;';
		$subquery = mysql_query("SELECT Type, ApplyTo FROM `Users_x_Filters` WHERE uid = '$userid' AND fid = '$fid'");
		if (mysql_num_rows($subquery) == 0) {
			$type = '0';
			$applyTo = 'Experimental';
		}	
		else {
			$row = mysql_fetch_array($subquery);
			$type = $row['Type'];
			$atstring = $row['ApplyTo'];
			if ($atstring != 'Experimental') {
				$applyTo = 'Selection';
			}
			else {
				$applyTo = 'Experimental';
			}
		}
		// type
		$out .= "<td><select name='type_$fid'>";
		for ($i = 0;$i<=3;$i++) {
			if ($type == $i) {
				$sel = 'SELECTED';
			}
			else {
				$sel = '';
			}
			$out .= "<option value='$i' $sel>".$types[$i]."</option>";
		}
		$out .= "</select></td>";
		// Apply To
		// NOW: set to experimental
		$applyTo = 'Experimental';
		if ($applyTo == 'Experimental') {
			$out .= "<td><select name='at_$fid'><option SELECTED value='Experimental'>Experimental</option><option value='Selection' disabled>Selection</option></select></td><td id='tracklist_$fid'>&nbsp;</td>";
		}
		else {
			$out .= "<td><select name='at_$fid'><option value='Experimental'>Experimental</option><option SELECTED value='Selection'>Selection</option></select></td><td id='tracklist_$fid'>$atstring (edit)</td>";
		}
		$out .= "</tr>";
		echo $out;		
	}
	echo "</table>";
	echo "</p><p><input type='submit' class=button name='ActivateFilters' value='Update Settings'></form></p>";

	echo "</div>";

}
//////////////////////	
// SHARING OPTIONS  //
//////////////////////
elseif ($type == 'share') {
	// check numeric fid.
	$fid = $_GET['fid'];
	if (!is_numeric($fid)) {
		echo "<div class=sectie><h3>Invalid filter ID</h3><p>Don't mess with the system!</p></div>";
		exit;
	}
	// check permissions
	$query = mysql_query("SELECT fid FROM `Filters_x_Userpermissions` WHERE fid = '$fid' AND uid = '$userid' AND sharing = '1'");
	if (mysql_num_rows($query) == 0) {	
		echo "<div class=sectie><h3>Access Denied</h3><p>You're not allowed to share this filter.</p></div>";
		exit;
	}
	// get filter details
	$query = mysql_query("SELECT Name, Comment FROM `Filters` WHERE fid = '$fid'");
	$row = mysql_fetch_array($query);
	$fname = stripslashes($row['Name']);
	$fcomment = stripslashes($row['Comment']);
	// print header
	echo "<div class=sectie><h3>Setting permissions for '$fname'</h3>";
	echo "<h4>Filter Info: $fcomment</h4>";

	// FIRST shared with new users, present options or finalise options.
	if (isset($_POST['share'])) {
		$addusers = $_POST['users'];	
		if (count($addusers) > 0) {
			echo "<p>Specify the permissions for the users you want to share this filter with, and press 'Finish'.</p>\n";
			echo "<form action='index.php?page=filters&type=share&fid=$fid' method=POST>\n";
			echo "<table cellspacing=0>\n";
			echo "<th $firstcell class=topcellalt>User</th>\n";
			echo "<th class=topcellalt>Edit Regions &amp; Details</th>\n";
			echo "<th class=topcellalt>Project Control</th>\n";
			echo "</tr>\n";
			foreach($addusers as $uid) {
				// get userinfo 
				$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$uid'");
				$row = mysql_fetch_array($query);
				$fname = $row['FirstName'];
				$lname = $row['LastName'];
				// print table row
				echo "<tr>\n";
				echo "<td $firstcell><input type=hidden name='users[]' value='$uid'>$lname $fname</td>\n";
				echo "<td><input type=radio name='edit_$uid' value=1 checked> Yes <input type=radio name='edit_$uid' value=0> No</td>\n";
				echo "<td><input type=radio name='share_$uid' value=1 checked> Yes <input type=radio name='share_$uid' value=0> No</td>\n";
				echo "</tr>\n";
			}
			echo "</table>\n";
			echo "</p>\n";
			echo "<input type=submit class=button name=finalise value='Finish'>\n";
			echo "</form>\n";
		}
		echo "<p><a href='index.php?page=filters&type=share&fid=$fid'>Go Back</a></p>\n";
		echo "</div>\n";
		exit();
	}
	elseif (isset($_POST['finalise'])) {
		$addusers = $_POST['users'];
		$yesno = array('Not Allowed','Allowed');
		// print table
		echo "<p>The following users gained access to the filter.</p>\n";
		echo "<p><table cellspacing=0>\n";
		echo "<th $firstcell class=topcellalt>User</th>\n";
		echo "<th class=topcellalt>Edit Regions &amp; Details</th>\n";
		echo "<th class=topcellalt>Project Control</th>\n";
		echo "</tr>\n";
		foreach($addusers as $uid) {
			$editing = $_POST["edit_$uid"];
			$sharing = $_POST["share_$uid"];
			// user info
			$query = mysql_query("SELECT LastName, FirstName FROM users WHERE id = '$uid'");
			$row = mysql_fetch_array($query);
			$fname = $row['FirstName'];
			$lname = $row['LastName'];
			// print table row
			echo "<tr>\n";
			echo "<td $firstcell>$lname $fname</td>\n";
			echo "<td>$yesno[$editing]</td>\n";
			echo "<td>$yesno[$sharing]</td>\n";
			echo "</tr>\n";
			// INSERT or UPDATE permissions
			$insquery = mysql_query("INSERT INTO `Filters_x_Userpermissions` (fid, uid, editing, sharing) VALUES ('$fid','$uid', '$editing', '$sharing') ON DUPLICATE KEY UPDATE editing = if(VALUES(editing) < '$editing', VALUES(editing),'$editing'), sharing = if(VALUES(sharing) < '$sharing', VALUES(sharing),'$sharing')");
		// notify the user (to inbox)
		$subject = "You were granted access to a new filter ($fname)";
		$insquery = mysql_query("INSERT INTO inbox (inbox.from, inbox.to, inbox.subject, inbox.body, inbox.type, inbox.values, inbox.date) VALUES ('$userid','$uid','$subject','','shared_filter','$fid',NOW())");
	}
	echo "</table></p>";
	echo "<p><a href='index.php?page=filters&type=share&fid=$fid'>Go Back</a></p>\n";	
	echo "</div>\n";
	exit();

	}	
	
	// SECOND main page
	echo "<p>Select the users you want to share this filter with from the list below and press 'Share'. Exact permissions will be presented in the next step.</p>\n";
	echo "<p>\n";
	echo "<form action='index.php?page=filters&type=share&fid=$fid' method=POST>\n";
	// get users with full access
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name, fu.editing, fu.sharing FROM `users` u JOIN  `Filters_x_Userpermissions` fu JOIN `affiliation` a ON u.Affiliation = a.id AND u.id = fu.uid WHERE fu.fid = '$fid' ORDER BY a.id, u.LastName");
	$uwa = array();
	while ($row = mysql_fetch_array($query)) {
		$lname = $row['LastName'];
		$currid = $row['id'];
		$fname = $row['FirstName'];
		$affi = $row['name'];
		$editing = $row['editing'];
		$sharing = $row['sharing'];
		$uwa[$currid] = "$lname $fname@@@$affi@@@$editing@@@$sharing";
	}
	// list users with no or restricted access
	echo "<span class=nadruk>Select users to grant access</span><br>\n";
	echo "<select name='users[]' size=15 MULTIPLE style='margin-left:1em;'>\n";
	$query = mysql_query("SELECT u.id, u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id ORDER BY a.name, u.LastName");
	$currinst = '';
	while($row = mysql_fetch_array($query)) {
		$uid = $row['id'];
		// skip users with full access
		if (array_key_exists($uid, $uwa)) {
			$pieces = explode('@@@',$uwa[$uid]);
			if ($pieces[2] == 1 && $pieces[3] == 1 ) {
				continue;
			}
		}
		$lastname = $row['LastName'];
		$firstname = $row['FirstName'];
		$institute = $row['name'];
		if ($currinst != $institute) {
			if ($currinst != '') {
				echo "</optgroup>";
			}
			echo "<optgroup label='$institute'>\n";
			$currinst = $institute;
		}
		echo "<option value='$uid'>$lastname $firstname</option>\n";
	}
	echo "</select></p>\n";
	echo "<p><input type=submit class=button name=share value='Share'></p>\n";
	echo "</form>\n";
	echo "</div>";
	// show who has access now.
	echo "<div class=sectie>\n";
	echo "<h3>Currently Shared</h3>\n";
	echo "<h4>Filter: $fname</h4>\n";
	echo "<p>The filter is currently accessible by the following users. 'Full' means read-write access to filter regions and details. 'Read-only' means no information can be changed. 'Filter Control' means this user can edit access permissions to share the filter with additional users.</p>\n";
	echo "You can remove their access by clicking on the garbage bin.</p>\n";
	echo "<span class=nadruk>Users with access:</span><br>\n";
	$currinst = '';
	echo "<ul id=ulsimple style='margin-left:1em;'>\n";
	foreach ($uwa as $uid => $value) {
		$pieces = explode('@@@',$value);
		$uname = $pieces[0];
		$institute = $pieces[1];
		$editing = $pieces[2];
		$sharing = $pieces[3];
		// check institution
		if ($currinst != $institute) {
			if ($currinst != '') {
				echo "</ol>";
			}
			echo "<li><span class=italic>&nbsp;&nbsp;$institute</span><ol>\n";
			$currinst = $institute;
		}
		// check permissions
		if ($editing == 1 ) {
			$perm = 'Full';
		}
		else {
			$perm = 'Read-only';
		}
		if ($sharing == 1 ) {
			$perm .= " / Project Control";
		}
		echo "<li>$uname <span class=italic>($perm)</span><a class=img href=\"remove_access.php?fid=$fid&u=$uid&type=fuser&from=filters\"><img src='images/content/delete.gif' width=8px height=8px></a></li>\n";
	}
	echo "</ol></ul>\n";
	//echo "</select>\n";
	echo "</p></div>";
}

?>


