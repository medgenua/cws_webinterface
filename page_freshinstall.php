<?php

#include('/opt/ServerMaintenance/PHPcredentials.inc');

// STORE POSTED VARS IF ANY
$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$email = $_POST['email'];
$affiliation = $_POST['affiliation'];
if ($affiliation == 'other') {
	$affiliation = $_POST['NewAffiliation'];
	$newaffi =1;
}
else {
	$newaffi = 0;
}
$requsername = $_POST['requsername'];
$password = $_POST['password'];
$confirm = $_POST['confirm'];

// FIRST IF SUBMITTED, CHECK AND CREATE USER
if ($_POST['register'] ){
	if ($lastname == "" || $firstname == "") 
	{
 		 echo "<div class=sectie><h3>No name specified</h3><p>Please enter your name.</p></div>";
 	}
	elseif (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") 
 	{
  		echo "<div class=sectie><h3>Invalid email address</h3><p>Please correct your emailadres to a valid one.</p></div>";
 	}
	elseif ($affiliation == "Affiliation" || $affiliation == "") 
	{
		  echo "<div class=sectie><h3>No affiliation specified</h3><p>Please enter the university or company you work for.</p></div>";
 	}
	elseif ($requsername == "Requested User Name" || $requsername == "") 
	{
		  echo "<div class=sectie><h3>No username specified</h3><p>Please choose a username.</p></div>";
 	}
	elseif ($password == "") 
	{
		  echo "<div class=sectie><h3>No password specified</h3><p>Please choose your password.</p></div>";
	}
	elseif ($password != $confirm) {
		  echo "<div class=sectie><h3>Passwords don't match</h3><p>Please enter your password again and make sure the two fields match.</p></div>";
	}
	else  {
		mysql_select_db('GenomicBuilds');
		$query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
		$row = mysql_fetch_array($query);
		$_SESSION['dbname'] = $row['name'];
		$_SESSION['dbstring'] = $row['StringName'];
		$_SESSION['dbdescription'] = stripslashes($row['Description']);
		$db = "CNVanalysis" . $_SESSION["dbname"];
		mysql_select_db("$db");
		$result = mysql_query("SELECT id FROM users WHERE username = '". addslashes($requsername)."'");
		if (mysql_num_rows($result) > 0)
		{
			echo "<div class=sectie><h3>Username exists</h3><p>The user $requsername already exists. Please choose a different name.</p></div>";
		}
		else {
			$lastname = addslashes($lastname);
			$firstname = addslashes($firstname);
			$affiliation = addslashes($affiliation);
			$email = addslashes($email);
			$requsername = addslashes($requsername);
			$text = addslashes($text);
			if ($newaffi == 1) {
				mysql_query("INSERT INTO affiliation (name) VALUES('$affiliation')");
				$affid = mysql_insert_id();
				$affiliation = $affid;
			}
			$query = "INSERT INTO users (LastName, FirstName, Affiliation, email, username, password, motivation,level) VALUES ('$lastname', '$firstname', '$affiliation', '$email', '$requsername', MD5('$password'), '$text', '3')";
			mysql_query($query);
			$uid = mysql_insert_id();
			mysql_query("INSERT INTO usersettings (id) VALUES ('$uid')");
			$subject = "CNV-WebStore: Admin Account Created"; 
 			//compose message, etc
			$message = "Message sent from http://$domain\r";  
			$message .= "Sent by: $email\r";
			$message .= "Subject: $subject\r";
			$message .= "Requested username: $requsername\r";
			$message .= "Motivation: \r\r";
			$message .= "$text\r";
			$message .= "\r\rThe CNV-WebStore administrator account ($requsername) has been created. Use this user for maintenance tasks. All Admin-related email will be sent to $email. \r\r";
			$headers = "From: $email\r\n";
			$headers .= "To: $email\r\n";
			
			/* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
			
			if (mail($email,"$subject",$message,$headers)) {
				/// user is created
				// set up email 
				system("find -type f | xargs sed -i s/'ADMIN@EMAIL.com'/'$email'/g;'");
	 			echo "<div class=sectie>";
				echo "<h3>CNV-WebStore Final Configuration</h3>";
				echo "<p></p><p><div class=nadruk>1: setting up administrative account</div></p>";
				echo "<p> &nbsp; &nbsp; => Succeeded.</p>";
				// update website status, all email & passwords are correct. 
				echo "<div class=nadruk>2: Activating CNV-WebStore</div>";
				mysql_query("UPDATE `GenomicBuilds`.`SiteStatus` SET status = 'Operative' WHERE 1");
				echo "<p> &nbsp; &nbsp; => Done.</p>";
				// this should not be, but if needed, updates the system-admin email address 
				if ($adminemail == 'ADMIN@EMAIL.COM') {
				 	echo "<p><div class=nadruk>4: Setting up hostname</div></p>";
					ob_flush();
					flush();
					// replace email in credentials file
					system("sed -i s/'ADMIN@EMAIL.COM'/'$email'/g /opt/ServerMaintenance/.credentials");
				}
				ob_flush();
				flush();
				echo "<p>You will now be redirected to the main page.</p>";
			        echo "<meta http-equiv='refresh' content='5;URL=index.php'>\n";	
				exit;
				
				
			} 
			else {
				  echo "<h4>Can't send email </h4>";
			}
			
		}
	}	

}
// if not: print form
if ($lastname == "")
	$lastname = "CNV-WebStore";
if ($firstname == "")
	$firstname = "Administrator";
if ($email == "")
	$email = "Email";
if ($requsername == "")
	$requsername = "Requested User Name";

echo "<div class=sectie>";
echo "<h3>CNV-WebStore Final Configuration</h3>";
echo "<p><form action= 'index.php' method=POST></p>";
echo "<div class=nadruk> 1: Setting up administrative account</div>";
echo "<p> Provide a username and password for the CNV-WebStore adminstrative user.  It is recommended to create a user for this that will not be used for daily usage. The associated email address will be used to send username request, and update information (if wanted). This Email is NOT related to the email address that will recieve database backup notifications.</p>";
?>

<table id=mytable cellspacing=0 >
<tr>
<th class=clear>Last Name:</td>
<td class=clear><input type="text" name="lastname" value="<?php echo $lastname ?>" size="40" maxlength="40" onfocus="if (this.value == 'CNV-WebStore') {this.value = '';}" /></td>
</tr>
<tr>
<th class=clear>First Name:</td>
<td class=clear><input type="text" name="firstname" value="<?php echo $firstname ?>" size="40" maxlength="40" onfocus="if (this.value == 'Administrator') {this.value = '';}" /></td>
</tr>
<tr>
<th class=clear>Administrator email:</td>
<td class=clear><input type="text" name="email" value="<?php echo $email ?>" size="40" maxlength="50" onfocus="if (this.value == 'Email') {this.value = '';}" /></td>
</tr>
<tr class=clear>
<th class=clear> Affiliation:</td>
<td class=clear><select name=affiliation>
<?php
	if ($affiliation == 'other' || $affiliation == '' ) {
		echo "<option value='other'>Other, Specify below</option>\n";
	}
	$query = mysql_query("SELECT id, name FROM affiliation");
	while($row = mysql_fetch_array($query)) {
		$affid = $row['id'];
		$affname = $row['name'];
		if ($affid == $affiliation) {
			echo "<option value='$affid' SELECTED>$affname</option>\n";
		}
		else {
			echo "<option value='$affid'>$affname</option>\n";
		}
	}
	echo "</select>\n";
	if ($affiliation == '' || $affilation == 'other') {
		$affiliation = 'New Affiliation';
	}
?>
</tr>
<tr class=clear>
<th class=clear>&nbsp;</td>
<td class=clear> OR <input type="text" name="NewAffiliation" value="<?php echo $affiliation ?>" size="37" maxlength="255" onfocus="if (this.value == 'New Affiliation') {this.value = '';}" /></td>
</tr>
<tr class=clear>
<th class=clear>Requested Username:</td>
<td class=clear><input type="text" name="requsername" value="<?php echo $requsername ?>" size="40" maxlength="20" onfocus="if (this.value == 'Requested User Name') {this.value = '';}" /></td>
</tr>
<tr class=clear>
<th class=clear>Choose a Password:</td>
<td class=clear><input type="password" name="password" size="40" maxlength="20" /></td>
</tr>
<tr class=clear>
<th class=clear>Confirm Password:</td>
<td class=clear><input type="password" name="confirm" size="40" maxlength="20" /></td>
</tr>
<tr> 
<td class=clear><input type="submit" name=register value=" Send " class=button ></td>
</tr>
</table>
</form>
</div>


