<?php
// general variables.
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash[23]  = "X";
$chromhash[24] = "Y";
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$ucscdb = str_replace('-','',$_SESSION['dbname']);
$fieldvalue = 'Chromosomal Region';

// POSTED ITEMS FROM INPUT FIELD ? => add to the $_GET.
if (isset($_GET['region']) && $_GET['region'] != '') {
	$region = $_GET['region'];
	$validfound =0;	
	// UCSC style
	$region = trim($region);
	if (preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $region)) {				
		$pieces = preg_split("/[:\-_+]/",$region);
		$length = count($pieces);
		# start was negative, '-' used as delimiter
		if ($length > 3) {
			$pieces[1] = -1 * abs($pieces[2]);
			$pieces[2] = $pieces[3];
		}
		$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
		if ($chrtxt == '23' || $chrtxt == '24'){
			$chrtxt = $chromhash[$chrtxt];
		}
		$chr = $chromhash[ $chrtxt ] ;
		$start = $pieces[1];
		$start = str_replace(",","",$start);
		if ($start < 0) {
			$start = 1;
		}
		$end = $pieces[2];
		$end = str_replace(",","",$end);
		// check end
		$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
		$nrow = mysql_fetch_array($nq);
		$chrstop = $nrow['stop'];
		if ($end > $chrstop) {
			$end = $chrstop;
		}
		
		$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
		$validfound = 1;
	}
	// CYTOBAND style
	elseif (preg_match("/(\d{1,2}|X|x|Y|y)(p|q)(\d+|ter)/",$region)) {
		$fiveprimevalid = 0;
		$threeprimevalid = 0;
		$invalidprinted = 0;
		$regiontxt = $region;
		$chrtxt = preg_replace("/(\w{1,2})(p|q)(\S+)/","$1",$region);
		$chr = $chromhash[$chrtxt];
		if (preg_match("/\-/",$region)) {  // cytoband range provided
			$startband = preg_replace("/(\w{1,2})(p|q)(\S+)\-(\S+)/","$2$3",$region);
			$stopband = preg_replace("/(\w{0,2})(p|q)(\S+)\-(\S+)/","$4",$region);
			$stopband = preg_replace("/(\w{0,2})(p|q)(\S+)/","$2$3",$stopband);
		}
		else {
			$startband = preg_replace("/(\w{1,2})(p|q)(\S+)/","$2$3",$region);
			$stopband = $startband;
		}
		if ($startband == 'pter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY start ASC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$startband = $nrow['name'];
		}
		if ($stopband == 'pter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY start ASC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$stopband = $nrow['name'];
		}
		if ($startband == 'qter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$startband = $nrow['name'];
		}
		if ($stopband == 'qter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$stopband = $nrow['name'];
		}

		$qstart = mysql_query("SELECT start, stop FROM cytoBand WHERE chr = '$chr' AND name = '$startband'");
		$qstop = mysql_query("SELECT start, stop FROM cytoBand WHERE chr= '$chr' AND name = '$stopband'");
		if (mysql_num_rows($qstart) == 0 ) {
			$invalidprinted = 1;
			echo "<div class=sectie>\n<h3>Invalid Band provided</h3>\n";
			echo "<p><span class=nadruk>5' Band</span></p>\n";
			echo "<p>According to ".$_SESSION['dbstring'].", there is no '$startband'-band for chromosome $chrtxt.\n";
			$qsquery = mysql_query("SELECT start, name FROM cytoBand WHERE chr = '$chr' AND name LIKE '$startband%' ORDER BY start ASC LIMIT 1");
			if (mysql_num_rows($qsquery) != 0) {
				$cstartarray = mysql_fetch_array($qsquery);
				$fivestart = $cstartarray['start'];
				$name1 = $cstartarray['name'];
				//if ($startband == $stopband) {
				$qssquery = mysql_query("SELECT stop, name FROM cytoBand WHERE chr= '$chr' AND name LIKE'$startband%' ORDER BY stop DESC LIMIT 1");	
				$cstartarray = mysql_fetch_array($qssquery);
				$fivestop = $cstartarray['stop'];
				$name2 = $cstartarray['name'];
				if ($name1 == $name2) {
					echo " $chrtxt$startband was converted to $chrtxt$name1.";
				}
				else {
					echo " $chrtxt$startband was converted to $chrtxt$name1-$chrtxt$name2.";
				}
				$fiveprimevalid = 1;
			}
			else {
				echo "Please check your query and try again.\n";	
			}

		}
		else {
			$cstartarray = mysql_fetch_array($qstart);
			$fivestart = $cstartarray['start'];
			$fivestop = $cstartarray['stop'];
			$fiveprimevalid = 1;
		}
		if (mysql_num_rows($qstop) == 0 ) {
			if ($invalidprinted == 0) {
				echo "<div class=sectie>\n<h3>Invalid Band provided</h3>\n";
			}
			echo "<p><span class=nadruk>3' Band</span></p>";
			echo "<p>According to ".$_SESSION['dbstring'].", there is no '$stopband'-band for chromosome $chrtxt. \n";
			$qsquery = mysql_query("SELECT start, name FROM cytoBand WHERE chr = '$chr' AND name LIKE '$stopband%' ORDER BY start ASC LIMIT 1");
			if (mysql_num_rows($qsquery) != 0) {
				$cstoparray = mysql_fetch_array($qsquery);
				$threestart = $cstoparray['start'];
				//echo "<h3>threestart $threestart</h3>";
				$name1 = $cstoparray['name'];
				$qssquery = mysql_query("SELECT stop, name FROM cytoBand WHERE chr= '$chr' AND name LIKE'$stopband%' ORDER BY stop DESC LIMIT 1");	
				$cstoparray = mysql_fetch_array($qssquery);
				$threestop = $cstoparray['stop'];
				$name2 = $cstoparray['name'];
				if ($name1 == $name2) {
					echo " $chrtxt$stopband was converted to $chrtxt$name1.";
				}
				else {
					echo " $chrtxt$stopband was converted to $chrtxt$name1-$chrtxt$name2.";
				}
				$threeprimevalid = 1;
			}
			else {
				echo "Please check your query and try again.\n";	
			}

		}
		else {
			$cstoparray = mysql_fetch_array($qstop);
			$threestop = $cstoparray['stop'];
			$threestart = $cstoparray['start'];
			$threeprimevalid = 1;
		}
		if ($fivestart >= $threestop) {
			$start = $threestart;
			$end = $fivestop;
		}
		else {
			$start = $fivestart;
			$end = $threestop;
		}
		if ($invalidprinted == 1) {
			echo "</div>";
		}
		if ($fiveprimevalid == 1 && $threeprimevalid == 1) {
			$validfound = 1;
		}
		$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";

	}
	if ($validfound == 1) {
		// put into $_GET;
		$_GET['chr'] = $chrtxt;
		$_GET['start'] = $start;
		$_GET['stop'] = $end;
		
	}
	else {
		$fieldvalue = $region;
	}
}


$chrtxt= (isset($_GET['chr']) ? $_GET['chr'] : '');

if ($chrtxt == 'X' || $chrtxt == 'Y') {
	$chr = $chromhash[$chrtxt];
}
else {
	$chr = $chrtxt;
	if ($chr != '') {
		$chrtxt = $chromhash[$chr];
	}
}
$start=(isset($_GET['start']) ? $_GET['start'] : '');
$stop=(isset($_GET['stop']) ? $_GET['stop'] : '');
if ($chr != '' && $start != '' && $stop != '') {
	$fieldvalue = "chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
}
// connect to DB.
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
// get public resources ids
$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$userid'");
$row = mysql_fetch_array($query);
$db_links = $row['db_links'];
$sigs = array();
// prioritization.
if (isset($_GET['prid']) && isset($_GET['aid'])){
	if (isset($_GET['cstm'])) {
		$table = 'cus_priorabs';
	}
	else {
		$table = 'priorabs';
	}
	$prid = $_GET['prid'];
	$aid = $_GET['aid'];
	$query = mysql_query("SELECT signGenes FROM $table WHERE prid = '$prid' AND aid = '$aid'");
	$sGs = mysql_fetch_array($query);
	$signGenes = $sGs['signGenes'];
	$signGenes = substr($signGenes,0,-1);
	$stringparts = explode(',',$signGenes);
	
	foreach ($stringparts as $part) {
		preg_match('/^(.+) \((.+)\)$/',$part,$matches);
		$sigs[ $matches[1] ] = "<td>" . $matches[2] . "</td>\n";
	}
	
	$extracoltop = " <th scope=col class=topcellalt>P-Value</td>\n";
}
else {
	$extracoltop = "";
}

// selection box.
?>
<div class=sectie>
<h3>Browse genes by chromosomal region</h3>
<p>Enter the chromosomal region in the field below en click 'search'. The region should be specified in UCSC-style or by chromosome band, e.g. chr7:7500000-9200000,  4q21.3 or 16p11.2-p12.2<p>

<p><form action='index.php' method=GET>
<input type=hidden name='page' value='genes' />
Search region: &nbsp;
<input type=text name=region size=30 value="<?php echo $fieldvalue ?>" onfocus="if (this.value == 'Chromosomal Region') {this.value = '';}"> &nbsp;  
<input type=submit class=button value=search>

</form>
</div>
<?php
// only print genes if selection is made.
if ($chr == '' || $start == '' || $stop == '') {
	exit;
} 
echo "<div class=sectie>\n";
echo "<h3>Genes located at Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',')."</h3>\n";
echo "<p>The following genes are disrupted by, or completely inside the queried chromosomal region.  For each gene, links to some handy sites are provided. To change the displayed resources, or add new ones, go to 'Profile'->'Plot Settings'->'Public Databases', or click <a href='index.php?page=settings&type=plots&st=publ&v=p'>here</a>.\n";

echo "</div><div class=sectie><p><br><table cellspacing=0>\n";

echo "<tr>\n";
echo " <th scope=col class=topcellalt $firstcell>Symbol</td>\n";
echo $extracoltop;
echo " <th scope=col class=topcellalt>Description</td>\n";
echo " <th scope=col class=topcellalt>Omim</td>\n";
echo " <th scope=col class=topcellalt>Morbid Description(s)</td>\n";
echo " <th scope=col class=topcellalt>Public Resources</td>\n";
echo "</tr>\n";
$results = mysql_query("SELECT symbol, omimID, morbidID, morbidTXT, geneTXT, geneID FROM genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");
//AND ( (start BETWEEN $start AND $stop) OR (end BETWEEN $start AND $stop) OR (start <= $start AND end >= $stop))
while ($row = mysql_fetch_array($results)) {
	$symbol = $row['symbol'];
	$omimID = $row['omimID'];
	$morbidID = $row['morbidID'];
	$morbidTXT = $row['morbidTXT'];
	$geneid = $row['geneID'];
	if ($morbidTXT != '') {
		$morbidTXT = preg_replace('/^(\|)(.*)/','$2',$morbidTXT);
		#$morbidTXT = str_replace('|','; ',$morbidTXT);
		#$morbidTXT = preg_replace('/(.*),\s\d+\s.*$/','$1',$morbidTXT);
	}
	$geneTXT = $row['geneTXT'];
	echo "<tr>\n";
	echo " <td $firstcell>$symbol</td>\n";
    if (array_key_exists($symbol,$sigs)) {
        echo $sigs[$symbol];
    }
	elseif ($extracoltop != "") {
		echo "<td>&nbsp;</td>\n";
	}
	echo "<td>$geneTXT&nbsp;</td>\n";
	#echo " <td><a href=\"http://www.ensembl.org/Homo_sapiens/Search/Details?species=Homo_sapiens;idx=;q=$symbol\" target=new>Synonyms</a></td>\n";	
	// OMIM column
	if ($omimID != 0) {
		echo " <td><a href=\"http://www.omim.org/entry/$omimID\" target='_blank' class=italic>$omimID</a></td>\n";
	}
	else {
		echo " <td>/</td>\n";
	}
	$morbidresults = mysql_query("SELECT `morbidID`, `disorder`, `inheritance mode` AS inheritance FROM `morbidmap` WHERE `omimID` = '$omimID' ORDER BY `morbidID`");
	if (mysql_num_rows($morbidresults) > 0) {
		echo " <td><ul>\n";
		while ($morbidrow = mysql_fetch_array($morbidresults)) {
			$morbidID = $morbidrow['morbidID'];
			$disorder = $morbidrow['disorder'];
			$inheritance = $morbidrow['inheritance'];
			if ($inheritance) {
				echo "<li><a href=\"http://www.omim.org/entry/$morbidID\" target='_blank' class=italic>[$morbidID] $disorder</a><span style=\"color:blue\"> [$inheritance]</span></li>\n";
			}
			else {
				echo "<li><a href=\"http://www.omim.org/entry/$morbidID\" target='_blank' class=italic>[$morbidID] $disorder</a></li>\n";
			}
		}
		echo " </ul></td>\n";
	}
	else {
		echo " <td>/</td>\n";
	}

	// public resources:
	$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=1 ORDER BY Resource");
	$nrlinks = mysql_num_rows($query);
	if ($nrlinks == 0) {
		echo "<td> - No resources specified </td>\n";
	}
	else {
		$content = "<td>";
		while ($row = mysql_fetch_array($query)) {
			$dbname = $row['Resource'];
			$link = $row['link'];
			$title = $row['Description'];
			$link = str_replace('%s',$symbol,$link);
			$link = str_replace('%o',$omimID,$link);	
			$link = str_replace('%g',$geneid,$link);
			$link = str_replace('%u',$ucscdb,$link);
			$content .= "<a title='$title' href='$link' target='_blank'>$dbname</a>, ";
		}
		$content = substr($content,0,-2) . "</td>\n";
		echo "$content";
	}

	#echo " <td><a href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&amp;cmd=search&amp;term=$symbol\" target=new>PubMed</a></td>\n";
	#echo " <td><a href=\"http://scholar.google.com/scholar?hl=en&amp;lr=&amp;btnG=Search&amp;q=$symbol\" target=new>Scholar</a></td>\n";
	#echo " <td><a href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=unigene&amp;cmd=search&amp;term=$symbol%20AND%20Homo%20Sapiens\" target=new>UniGene</a></td>\n";
	#echo " <td><a href=\"http://www.pubgene.org/tools/Ontology/BioAssoc.cgi?mode=simple&amp;terms=$symbol&amp;organism=hs&amp;termtype=gap\" target=new>PubGene</a></td>\n";
	#echo " <td><a href=\"index.php?page=results&amp;type=region&amp;region=$symbol\" target=new>Search</a></td>\n";
	echo "</tr>\n";
}

echo "</table></p>\n";
echo "</div>\n";
?>
	
