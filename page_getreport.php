<?php
/////////////////////////
// CONNECT TO DATABASE //
/////////////////////////
mysql_select_db('GenomicBuilds');
$query = mysql_query("SELECT name, StringName, Description, size FROM CurrentBuild LIMIT 1");
$row = mysql_fetch_array($query);
$genomesize = $row['size'];

$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
#$query = mysql_query("SELECT StringName FROM `GenomicBuilds`.`CurrentBuild` LIMIT 1");
#$row = mysql_fetch_array($query);
$buildname = $_SESSION['dbstring'];
// DEFINE SOME VARS
$inharray = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', '-' => 'Not Defined');
$dc = array();
$name_dc = array();
$dc_query = mysql_query("SELECT id, name, sort_order AS class_sorted, abbreviated_name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = array(
		'abbr' => $row['abbreviated_name'],
		'full' => $row['name'],
		'sorted' => $row['class_sorted']
	);
	$name_dc[$row['abbreviated_name']] = $row['class_sorted'];
}

$iscninh = array(0=>'', 1=>'pat', 2=> 'mat', 3=>'dn');
$iscnmoscn = array('del' => 1, 'dup' => 3, 'homdel' => 0, 'homdup' => 4);
$iscnmoscn_XY = array('del' => 0, 'dup' => 2);

for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chararray = array('&nbsp;','_','&ccedil;','&amp;','&sect;','&ecirc;', '$','&micro;', '&lt;', '&gt;', '#','&deg;','&pound;');
$newchararray = array(' ','\_','\c{c}','\&','\textsection','ê','\$','$\mu$','\textless', '\textgreater', '\#', '\textdegree','\pounds' );

// POSTED VARIABLES 
$sid = $_POST['sid'];
$pid = $_POST['pid'];
$minsnp = $_POST['minsnp'];
$minqs = $_POST['minqs'];
$minpas = $_POST['minpas'];
$minpxs = $_POST['minpxs'];
// sorted class ~ level of pathogenicity
//  use the sorted diagnostic class ids (the primary ids do not follow the level of pathogenicity)
$maxclass = $_POST['maxclass'];
$minsize = $_POST['minsize'];
$format = $_POST['format'];
$incnv = $_POST['incnv'];  		// 0-1
// LOH RELATED SETTINGS
$incloh = $_POST['incloh'];  		// 0-1
$lohmorbid = $_POST['lohmorbid'];	// 0-1
$lohminsize = str_replace(',','',$_POST['lohminsize']);
$lohmaxclass = $_POST['lohmaxclass'];

// CSV  delimiter
$del= "\t";

// the output holders
$pdfout = '';
$csvout = '';

//////////////////
// LOAD FILTERS //
//////////////////
$exp = array("IlluminaProjectSamples","CustomProjectSamples","IlluminaSearchSamples","CustomSearchSamples","Healthy");
$query = mysql_query("SELECT f.Name, uf.fid,  uf.Type, uf.ApplyTo,fr.chr,fr.start, fr.stop,fr.rid,fr.Comment FROM `Users_x_Filters` uf JOIN `Filters_x_Regions` fr JOIN `Filters` f ON f.fid = fr.fid AND fr.fid = uf.fid WHERE uf.uid = '$userid' AND uf.Type != 0 ORDER BY Type ASC");
$filter = array("1" => array(), "2" => array(), "3" => array());
while ($row = mysql_fetch_array($query)) {
	$fchr = $row['chr'];
	// create chromosome entry in filter hash
	if (!isset($filter[$row['Type']][$fchr])) {
		$filter[$row['Type']][$fchr] = array();
	}

	// skip filters that are not applied to illumina-project samples (browsing details implies this.)
	if ($row['ApplyTo'] != 'Experimental') {
		$at = explode(";",$row['ApplyTo']);
		if (!in_array("IlluminaProjectSamples")) {
			continue;
		}
	}
	// first time start pos on this chrom is seen
	if (!isset($filter[$row['Type']][$fchr][$row['start']])) {
		$filter[$row['Type']][$fchr][$row['start']] = array();		
		$filter[$row['Type']][$fchr][$row['start']]['stop'] = $row['stop'];
		$filter[$row['Type']][$fchr][$row['start']]['comment'] = $row['Comment'];
		$filter[$row['Type']][$fchr][$row['start']]['Name'] = $row['Name'];	
	}
	// else... extend if newend > end ; concat names and comments.
	else {
		if ($row['stop'] > $filter[$row['Type']][$fchr][$row['start']]['stop']) {
			$filter[$row['Type']][$fchr][$row['start']]['stop'] = $row['stop'];
		}
		if ($row['Comment'] != "") {
			$filter[$row['Type']][$fchr][$row['start']]['comment'] .= "\n".$row['Comment'];
		}
		$filter[$row['Type']][$fchr][$row['start']]['Name'] .= "; ".$row['Name'];
	}
}	


// GET SAMPLE & PROJECT DETAILS 
$query = mysql_query("SELECT s.chip_dnanr, s.gender, ps.callrate, ps.LRRSD, ps.BAFSD, ps.WAVE, ps.INIWAVE, ps.SentrixID, ps.SentrixPos, ps.swgs_resolution, ps.swgs_nreads, ps.swgs_macon, ps.swgs_gendertest, p.naam, p.chiptype, p.collection, p.created, u.FirstName, u.LastName FROM sample s JOIN projsamp ps JOIN project p JOIN users u ON s.id = ps.idsamp AND p.id = ps.idproj AND u.id = p.userID WHERE s.id = $sid AND p.id = $pid" );
$row = mysql_fetch_array($query);
$samplename = $row['chip_dnanr'];
$gender = $row['gender'];
$callrate = $row['callrate'];
$lrrsd = $row['LRRSD'];
$bafsd = $row['BAFSD'];
$wave = $row['WAVE'];
$iniwave = $row['INIWAVE'];
$pname = $row['naam'];
$chiptype = $row['chiptype'];
$collection = $row['collection'];
$fname = str_replace('_','\_',$row['FirstName']);
$lname = str_replace('_','\_',$row['LastName']);
$created = $row['created'];
$sentrixid = $row['SentrixID'];
if ($sentrixid == '') {$sentrixid = 'NA';}
$sentrixpos = $row['SentrixPos'];
if ($sentrixpos == '') {$sentrixpos = 'NA';}
// swgs data ? 
$swgs_resolution = $row['swgs_resolution'];
$swgs_macon = $row['swgs_macon'];
if ($swgs_macon) {
	$swgs_macon = str_replace($swgs_macon, '%', '\%');
}
$swgs_nreads = $row['swgs_nreads'];
$swgs_gendertest = $row['swgs_gendertest'];


// get datatype from projsamp
$q = mysql_query("SELECT datatype FROM `projsamp` WHERE idsamp = '$sid' AND idproj = '$pid'");
$r = mysql_fetch_array($q);
$datatype = $r['datatype'];

$lf = '';
// check one CNV to see if they are lifted & determine datatype
$query = mysql_query("SELECT a.id, LiftedFrom, datatype FROM aberration a WHERE a.sample = $sid AND a.idproj = $pid LIMIT 1");
if (mysql_num_rows($query) != 0) {
    $row = mysql_fetch_array($query);
    $lf = $row['LiftedFrom'];
}

if ($lf != '') {
	$query = mysql_query("SELECT StringName FROM `GenomicBuilds`.`PreviousBuilds` WHERE name = '$lf'");
	$row = mysql_fetch_array($query);
	$lfpdf = ' {\color{red}\scriptsize\textsl{(Lifted From '. $row['StringName'] . ')}}';
	$lfcsv = ' (Lifted From '. $row['StringName'] . ')';
}
else {
    $lfpdf = '';
    $lfcsv = '';
}
// progress printing
echo "<div class=sectie>\n";
echo "<h3> Preparing the report file</h3>\n";
echo "<h4>status...</h4>\n";
echo "<ul id=ul-disc>\n";

// some general info
//fwrite($fh,$footerstring);
$footerstring = '\fancyfoot[L]{CNV report: '.str_replace('_','\_',$samplename).'}'."\n";
$pdfout .= '\begin{landscape}'."\n";

$pdfout .= $footerstring;


// PRINT EXPERIMENTAL DATAILS ON SAMPLE / PROJECT
$titlestring = '\section*{\huge{'.str_replace('_','\_',$samplename).'}}'."\n" . '\hrule' . "\n" . '\vspace{0.5cm}'. "\n";
$csvout .= "Sample Details\n";
$csvout .= "Sample Name$del$samplename\n";
//fwrite($fh, $titlestring);
$pdfout .= $titlestring;
$tablestring = '\subsection*{\textsl{\underline{Experimental Details}}}'."\n";
$tablestring .= '{\small ';
$tablestring .= '\begin{tabular}{lllll}'."\n";
$tablestring .= '\textbf{Sample Gender} & '. $gender .' & & \textbf{Project} & '. str_replace('_','\_',$pname) . '\\\\' ."\n";
if ($datatype != 'swgs') {
    $tablestring .= '\textbf{Chip Position} & '. $sentrixpos .' & & \textbf{Chip ID} & '. $sentrixid . '\\\\' ."\n";
    $tablestring .= '\textbf{Call Rate} & '. $callrate . ' & & \textbf{Collection} & '. str_replace('_','\_',$collection) . '\\\\' ."\n";
    $tablestring .= '\textbf{LogR Std.Dev} & '. $lrrsd .' & & \textbf{Chiptype} & '. str_replace('_','\_',$chiptype). ' \\\\'."\n";
    $tablestring .= '\textbf{BAF Std.Dev} & '. $bafsd . ' & & \textbf{Analysis Date} & '. $created  . '\\\\' ."\n";
    if ($iniwave != 0) {
        $tablestring .= '\textbf{Initial Waviness} & '. $iniwave . ' & & \textbf{Genome Build} & '.$buildname.$lfpdf.' \\\\ \textbf{Post Norm. Wave} & '. $wave. ' & &  & \\\\'."\n";
        // $csvout .= "Initial Waviness;$iniwave\n";
    }
    else {
        $tablestring .= '\textbf{Waviness} & '. $wave .' & & \textbf{Genome Build} & '.$buildname.$lfpdf .' \\\\'."\n";
    }
}
else {
    $tablestring .= '\textbf{Library Kit} & '. $chiptype . ' & & \textbf{Resolution} & ' . $swgs_resolution . '\\\\' . "\n";
    $tablestring .= '\textbf{Nr.Reads} & '. $swgs_nreads. ' & & \textbf{MACON} & ' . $swgs_macon . ' \\\\' . "\n";
    $tablestring .= '\textbf{Pred.Gender} & '.$swgs_gendertest. ' & & \textbf{Genome Build} & '.$buildname.$lfpdf .'\\\\'."\n";
}
$csvout .= "Sample Gender$del$gender\n";
$csvout .= "Project$del$pname\n";
$csvout .= "Collection$del$collection\n";
$csvout .= "Analysis Date$del$created\n";
// $csvout .= "\nExperimental Details:\n";
// $csvout .= "Chiptype;$chiptype\n";
// $csvout .= "Chip Position;$sentrixpos\n";
// $csvout .= "Chip ID;$sentrixid\n";
// $csvout .= "Call Rate;$callrate\n";
// $csvout .= "LogR Std.Dev;$lrrsd\n";
// $csvout .= "BAF Std.Dev;$bafsd\n";

// $csvout .= "Waviness;$wave\n";
// $csvout .= "Genome Build;$buildname$lfcsv\n";
$tablestring .= '\end{tabular}'."\n";
$tablestring .= ' }' ."\n"; // end of 'small'
//fwrite($fh, $tablestring);
$pdfout .= $tablestring;

// PROCESS EXTRA COMMENTS
if ($_POST['inccom'] == 1 && !preg_match('/Experimental Comments/',$_POST['comments']) && $_POST['comments'] != '') {
	$subtitlestring = '\subsection*{\textsl{\underline{Additional comments}}}'."\n";
	//fwrite($fh,$subtitlestring);
	$pdfout .= $subtitlestring;
	//fwrite($fh, '{\small ');
	$pdfout .= '{\small ';
	$comments = $_POST['comments'];
	# replace text chars	
	$comments = str_replace($chararray, $newchararray, $comments);
	# replace aigu accents
	$csvout .= "\nExperimental Comments\n$comments\n";
	while (preg_match('/&.acute;/',$comments)) {
		$comments = preg_replace('/(.*)&(.{0,1})(acute;)(.+)/',"$1".'\\\'{'."$2".'}'."$4",$comments);
	} 
	# replace grave accents
	while (preg_match('/&.grave;/',$comments)) {
		$comments = preg_replace('/(.*)&(.{0,1})(grave;)(.+)/',"$1".'\\\`{'."$2".'}'."$4",$comments);
	}
	# replace umlauts
	while (preg_match('/&.uml;/',$comments)) {
		$comments = preg_replace('/(.*)&(.{0,1})(uml;)(.+)/',"$1".'\"{'."$2".'}'."$4",$comments);
	}
	while (preg_match('/ë/',$clinic)) {
		$comments = preg_replace('/(.*)(ë)(.+)/',"$1".'\"{'."e".'}'."$4",$comments);
	}
	# replace tilde
	while (preg_match('/&.tilde;/',$comments)){
		$comments = preg_replace('/(.*)&(.{0,1})(tilde;)(.+)/',"$1".'\~{'."$2".'}'."$4",$comments);
	}
	# replace bold tags
	while (preg_match('/<strong>.*<\/strong>/',$comments)) {
		$comments = preg_replace('/(.*)(<strong>)(.*)?(<\/strong>)(.*)/',"$1".'\textbf{'. "$3". '}'. "$5", $comments);
	}
	#replace underline tags
	while (preg_match('/<span style="text-decoration: underline;">.*<\/span>/',$comments)) {
		$comments = preg_replace('/(.*)(<span style="text-decoration: underline;">)(.*)?(<\/span>)(.*)/',"$1".'\underline{'."$3".'}'."$5",$comments);
	}
	# replace italic tags
	while (preg_match('/<em>.*<\/em>/',$comments)) {
		$comments = preg_replace('/(.*)(<em>)(.*)?(<\/em>)(.*)/',"$1".'\textsl{'."$3".'}'."$5",$comments);
	}
	# replace other chars	
	$comments = str_replace(array("\r\n", "\r", '<br>', '<br />'), "\n", $comments);
	$comments = str_replace('<ul>', '\begin{packed_item} ',$comments);
	$comments = str_replace('</ul>', '\end{packed_item} ',$comments);
	$comments = str_replace('<ol>','\begin{packed_enum} ',$comments);
	$comments = str_replace('</ol>','\end{packed_enum} ',$comments);
	$comments = str_replace('<li>','\item ',$comments);
	$comments = str_replace('<p>',"\n".'\noindent'."\n",$comments);
	$comments = str_replace(array('</li>', '<div>','</div>','</p>'),' ',$comments);
	//fwrite($fh,$comments);
	$pdfout .= $comments;
	//fwrite($fh,' }'."\n"); // end of small 
	$pdfout .= ' }'."\n";
}

// PROCESS FREE TEXT CLINICAL DETAILS
if ($_POST['incclin'] == 1 && !preg_match('/Clinical Information/', $_POST['clinical']) && $_POST['clinical'] != '') {
	$subtitlestring = '\subsection*{\textsl{\underline{Clinical Summary}}}'."\n";
	//fwrite($fh,$subtitlestring);
	//fwrite($fh,'{\small ');
	$pdfout .= $subtitlestring;	
	$pdfout .= '{\small ';
	$clinical = $_POST['clinical'];
	$csvout .= "\nFree Text Clinical Information:\n$clinical\n";	
	$clinical = str_replace($chararray, $newchararray, $clinical);
	# replace aigu accents
	while (preg_match('/&.acute;/',$clinical)) {
		$clinical = preg_replace('/(.*)&(.{0,1})(acute;)(.+)/',"$1".'\\\'{'."$2".'}'."$4",$clinical);
	} 
	# replace grave accents
	while (preg_match('/&.grave;/',$clinical)) {
		$clinical = preg_replace('/(.*)&(.{0,1})(grave;)(.+)/',"$1".'\\\`{'."$2".'}'."$4",$clinical);
	}
	# replace umlauts
	while (preg_match('/&.uml;/',$clinical)) {
		$clinical = preg_replace('/(.*)&(.{0,1})(uml;)(.+)/',"$1".'\"{'."$2".'}'."$4",$clinical);
	}
	# replace tilde
	while (preg_match('/&.tilde;/',$clinical)){
		$clinical = preg_replace('/(.*)&(.{0,1})(tilde;)(.+)/',"$1".'\~{'."$2".'}'."$4",$clinical);
	}
	# replace bold tags
	while (preg_match('/<strong>.*<\/strong>/',$clinical)) {
		$clinical = preg_replace('/(.*)(<strong>)(.*)?(<\/strong>)(.*)/',"$1".'\textbf{'. "$3". '}'. "$5", $clinical);
	}
	#replace underline tags
	while (preg_match('/<span style="text-decoration: underline;">.*<\/span>/',$clinical)) {
		$clinical = preg_replace('/(.*)(<span style="text-decoration: underline;">)(.*)?(<\/span>)(.*)/',"$1".'\underline{'."$3".'}'."$5",$clinical);
	}
	# replace italic tags
	while (preg_match('/<em>.*<\/em>/',$clinical)) {
		$clinical = preg_replace('/(.*)(<em>)(.*)?(<\/em>)(.*)/',"$1".'\textsl{'."$3".'}'."$5",$clinical);
	}
	# replace other chars	
	$clinical = str_replace(array("\r\n", "\r", '<br>', '<br />'), "\n", $clinical);
	$clinical = str_replace('<ul>', '\begin{packed_item} ',$clinical);
	$clinical = str_replace('</ul>', '\end{packed_item} ',$clinical);
	$clinical = str_replace('<ol>','\begin{packed_enum} ',$clinical);
	$clinical = str_replace('</ol>','\end{packed_enum} ',$clinical);
	$clinical = str_replace('<li>','\item ',$clinical);
	$clinical = str_replace('<p>',"\n".'\noindent'."\n",$clinical);
	$clinical = str_replace(array('</li>', '<div>','</div>','</p>'),' ',$clinical);
	//fwrite($fh,$clinical);
	//fwrite($fh,' } '."\n"); // end of small
	$pdfout .= $clinical;
	$pdfout .= ' } '."\n";
}
////////////////////////////////////
// PROCESS ONTOLOGY CLINICAL INFO //
////////////////////////////////////

if ($_POST['incclinontology'] == 1 ){
	$subarray = array();
	$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
	$lddb = "LNDB";
	mysql_select_db($cnvdb);
	$subquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
	while ($subrow = mysql_fetch_array($subquery)) {
		$code = $subrow['lddbcode'];
		$pieces = explode('.',$code);
		$subarray[$pieces[0]][$pieces[1]][] = $pieces[2];
	}
	mysql_select_db($lddb);
	$subtablestring = '';
	$csvsubtablestring = "\nStructured Clinical Data (LNDB):\n";
	$match = 0;
	foreach($subarray as $first => $sarray) {
		$ssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
		$ssubrow = mysql_fetch_array($ssubquery);
		$firstlabel = $ssubrow['LABEL'];
		foreach ($sarray as $second => $tarray) {
			if ($second == '00') {
				$subtablestring .= "$firstlabel & & ".'\\\\' ."\n";
				$csvsubtablestring .= "$firstlabel$del$del\n"; 
			}
			else {
				$sssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
				$sssubrow = mysql_fetch_array($sssubquery);
				$secondlabel = $sssubrow['LABEL'];
				foreach ($tarray as $third) {
					if ($third == '00') {
						$subtablestring .= "$firstlabel & $secondlabel & ".'\\\\' ."\n";
						$csvsubtablestring .= "$firstlabel$del$secondlabel$del\n";
					}
					else {
						$ssssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
						$ssssubrow = mysql_fetch_array($ssssubquery);
						$thirdlabel = $ssssubrow['LABEL'];
						$subtablestring.= "$firstlabel & $secondlabel & $thirdlabel ".'\\\\'."\n";
						$csvsubtablestring .= "$firstlabel$del$secondlabel$del$thirdlabel\n";
					}	
				}
			}
		}	
	}
	if ($subtablestring != '') {
		$subtitlestring = '\subsection*{\textsl{\underline{Clinical Information (LNDB)}}}'."\n";
		//fwrite($fh,$subtitlestring);
		$pdfout .= $subtitlestring;
		#$filter .= '\begin{tabular}{p{0.4\textwidth}p{0.3\textwidth}p{0.27\textwidth} }' ."\n";
		$tableheadstring = '{\footnotesize '."\n".'\begin{tabular}{@{\extracolsep{\fill}}lll}'."\n" . '\hline' . "\n";
		$tableheadstring .= '\textbf{Primary} & \textbf{Secondary} & \textbf{Tertiary} '.'\\\\'."\n";
		$tableheadstring .= '\hline'."\n";
		//fwrite($fh,$tableheadstring);
		$pdfout .= $tableheadstring;
		//fwrite($fh,$subtablestring);
		$pdfout .= $subtablestring;
		$endtablestring = '\hline' . "\n".'\end{tabular}'."\n".' } '."\n";
		//fwrite($fh,$endtablestring);
		$pdfout .= $endtablestring;
		$csvout .= $csvsubtablestring;
	}
	mysql_select_db($cnvdb);
}
////////////////////////////////////
// FIRST : CNV INFO FOR CNV TABLE //
//////////////////////////////////// 

if ($incnv == 1) {
	$cnvtablestring = '';
	$subtitlestring = '\subsection*{\textsl{\underline{Copy Number Variations}}}'."\n";
	$cnvtablestring .= $subtitlestring;
	$headerline = '';
	$columns = 'l';  # at least one for copynumber
	$csvheaderline = '';
	if (isset($_POST['cytoband'])) {
		$csvheaderline .= 'CytoBand'.$del;	
	}
	$csvheaderline .= 'CNV_type'.$del;

	if (isset($_POST['genomic'])) {
		$columns .= 'l';
		$headerline .= '\textbf{Minimal Region} & ';
		$csvheaderline .= 'Minimal Region'.$del;
	}
	if (isset($_POST['maxgenomic'])) {
		$columns .= 'l';
		$headerline .= '\textbf{Maximal Region} & ';
		$csvheaderline .= 'Maximal Region'.$del;
	}

	if (isset($_POST['cytoband'])) {
		$columns .= 'l';
		$headerline .= '\textbf{CytoBand} & ';
	}

	$headerline .= ' \textbf{CN} & ';


	if (isset($_POST['size'])) {
		$columns .= 'l';
		$headerline .= ' \textbf{Min. Size} & ';
		$csvheaderline .= 'Minimal/Maximal Size'.$del;
	}
	if (isset($_POST['maxsize'])) {
		$columns .= 'l';
		$headerline .= ' \textbf{Max. Size} & ';
	}
	if (isset($_POST['nrprobes'])) {
		$columns .= 'l';
        if ($datatype != 'swgs') {
		    $headerline .= '\textbf{NrSnps} & ';
		    // $csvheaderline .= 'Number of Probes;';
        }
        else {
            $headerline .= ' \begin{minipage}{2cm} \textbf{NrBins} \vspace{-0.1cm} \newline  {\scriptsize \textbf{Used (Total)}} \end{minipage} & ';
            // $csvheaderline .= "Number of bins Informative(Total);";
        }
	}
	if (isset($_POST['inheritance']) ){
		$columns .= 'l';
		$headerline .= ' \textbf{Inh.} & '; 
		$csvheaderline .= 'Inheritance'.$del;
	}
	if (isset($_POST['DC'])) {
		$columns .= 'l';
		$headerline .= ' \textbf{D.C.} & ';
		$csvheaderline .= 'Diagnostic Class'.$del;
	}
	if (isset($_POST['scores'])) {
		$columns .= 'lll';
        if ($datatype != 'swgs') {
            $columns .= 'lll';
		    $headerline .= ' \textbf{Q} & \textbf{P} & \textbf{V} & ';
		    $csvheaderline .= 'QuantiSNP Score;PennCNV Score;VanillaICE Score;';
        }

	}
	$csvheaderline .= "Genes in Minimal Region".$del;
	if (isset($_POST['genelarge'])) {
		$csvheaderline .= "Genes in Maximal Region";
	}
	$nrcolumns = strlen($columns);
	$headerline = substr($headerline,0,-2);
	$headerline .= '\\\\ '. "\n";
	$csvheaderline = substr($csvheaderline,0,-1)."\n";
	$csvout .= "\nCNV Details:\n";
	$csvout .= $csvheaderline;
	// Starting long table for CNV entries
	// First the headers and footers
	$tableheadstring = '\begin{longtable}[l]{@{\extracolsep{\fill}}'.$columns.'}'."\n" . '\hline' . "\n";
	$cnvtablestring .= $tableheadstring;
	$cnvtablestring .= $headerline;
	$endhead = '\hline' . "\n" .'\endhead'."\n";
	$cnvtablestring .= $endhead;
	$longfooter = '\hline \multicolumn{'. "$nrcolumns" .'}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
	$longfooter .= '\endfoot' . "\n";
	$longfooter .= "\n";
	$longfooter .= '\endlastfoot' . "\n";
	$cnvtablestring .= $longfooter;


	// FILL TABLE
	$morbidstring = '';
	$csvmorbidstring = '';
	$morbidsarray = array();
	$morbidscsvarray = array();

	$aids = $_POST['include'];
	foreach( $aids as $key => $value) {
		$checkaids[$value] = 1;
		#echo "$key => $value<br>\n"; 
	}

	$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.size, a.cn, a.nrgenes, a.nrsnps, a.total_nrsnps, a.seenby, a.inheritance, a.class, a.largestart, a.largestop,a.pubmed,a.avgLogR, a.segmentZscore, a.avgZscore, a.likelihood_ratio, a.datatype FROM aberration a WHERE a.sample = $sid AND a.idproj = $pid ORDER BY a.chr, a.start");
	$manualhide = 0;
	$classhide = 0;
	$qsnphide = 0;
	$pcnvhide = 0;
	$snphide = 0;
	$sizehide = 0;
	$morbidsmall = 0;
	$morbidlarge = 0;
	$genesmall = 0;
	$smallgene = 0;
	$largegene = 0;
	$genelarge = 0;
	$genesnotshown = 0;
	$notlistedcnv = 0;
	$missedmorbid = 0;
	$totalcnv = mysql_num_rows($query);
	// print_r($totalcnv);
	$iscn = '';
	$iscnxy = '';
	$pmids = '';
	$firstline = 1;
	while ($row = mysql_fetch_array($query)) {
		$line = '';
		$csvline = '';
		$aid = $row['id'];
		$chr = $row['chr'];
		$start = $row['start'];
		$stop = $row['stop'];
		// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
		foreach ($filter["1"][$chr] as $fstart => $farray) {
			if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $stop) {
				$totalcnv--;
				continue 2;
			}
		}	
		$size = $row['size'];
		if ($size <= 0) {
			$size = $stop - $start + 1;
		}
		$cn = $row['cn'];
		$nrgenes = $row['nrgenes'];
		$nrprobes = $row['nrsnps'];
		$nrtotalprobes = $row['total_nrsnps'];
		$datatype = $row['datatype'];
		$seenby = $row['seenby'];
		$inh = $row['inheritance'];
		// the ids of the diagnostic classes do not align with the level of pathogenicity
		// using the sorted ids for this
		$class_db = $row['class'];
		$class =  $dc[$class_db]['sorted'];
		$pmid = $row['pubmed'];
		$largestart = $row['largestart'];
		if ($largestart == 'ter') {
			$largestartvalue = 0;
			$largestartstring = 'ter';
		}
		else {
			$largestartvalue = $largestart;
			$largestartstring = number_format($largestart,0,'',',');
		}
		$largestop = $row['largestop'];
		if ($largestop == 'ter') {
			$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$cendrow = mysql_fetch_array($cendquery);
			$largestopvalue = $cendrow['stop'];
			$largestopstring = 'ter';
		}
		else {
			$largestopvalue = $largestop;
			$largestopstring = number_format($largestop,0,'',',');
		}
		$largesize = $largestopvalue - $largestartvalue;
		# filter on class
		if ($maxclass != '' && ($maxclass < $class || $class == '')) {
			$classhide++;
			continue;
		}
		# filter on coverage
		if ($datatype == 'swgs') {
			if ($nrtotalprobes < $minsnp) {
				$snphide++;
				continue;
			} 
		}
		else {
			if ($nrprobes < $minsnp){
				$snphide++;
				continue;
			}
		} 
		# filter on size
		if ($size < $minsize) {
			$sizehide++;
			continue;
		}
		# manual filtering
		if ($checkaids[$aid] != 1) {
			$manualhide++;
			continue;
		}
		if ($pmid != '') {
			$pmids .= "$pmid,";
		}

		// GET KARYO
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostart = $cytorow['name'];
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$stop' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostop = $cytorow['name'];
		if ($cytostart == $cytostop) {
			$cytoband = $chromhash[$chr].$cytostart;
			$iscnband = $cytoband;
		}
		else {
			$cytoband = $chromhash[$chr]."$cytostart$cytostop";
			$iscnband = $chromhash[$chr]."$cytostart$cytostop";
		} 

		if (isset($_POST['cytoband'])) {
			$csvline .= "$cytoband".$del;
		}

		if ($chr  == 23 && $gender == 'Male') {
			if ($cn < 1) {
				$cnvtype = 'Deletie';
			}
			else {
				$cnvtype = 'Duplicatie';
			}
		}
		else {
			if ($cn < 2) {
				$cnvtype = 'Deletie';
			}
			else {
				$cnvtype = 'Duplicatie';
			}

		}
		$csvline .= "$cnvtype$del";


		if (isset($_POST['genomic'])) {
			$region_fullcontext = "Chr" . $chromhash[$chr] . ":". number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');	
			$region = $start . "-" . $stop;	
			$line .= $region_fullcontext .' & ';
			$csvline .= "$region$del";
		}
		if (isset($_POST['maxgenomic'])) {
			$maxregion_fullcontext = "Chr" . $chromhash[$chr] . ":". $largestartstring . '-' . $largestopstring;
			$maxregion = $largestart . '-' . $largestop;
			$line .= $maxregion_fullcontext .' & ';
			$csvline .= "$maxregion$del";
		}
		
		if (isset($_POST['cytoband'])) {
			$line .= $cytoband . ' & ';
		}

		$line .= $cn .' & ';
		
		if (isset($_POST['size'])) {
			$line .= number_format($size,0,'',',') . ' & ';
			if ($largesize >= 1000000) {
				$csvline .= number_format($size/1000000, 2, ',', '') . '-' . number_format($largesize/1000000, 2, ',', '') . ' Mb' . $del;
			}
			elseif ($largesize >= 1000 ) {
				$csvline .= number_format($size/1000, 2, ',', '') . '-' . number_format($largesize/1000, 2, ',', '') . ' kb' . $del;
			}
			else {
				$csvline .= $size . '-' . $largesize . ' bp' . $del;
			}
		}
		if (isset($_POST['maxsize'])) {
			$line .= number_format($largesize,0,'',',') . ' & ';
			// $csvline .= "$largesize;";
		}
		if (isset($_POST['nrprobes'])) {
            // for sWGS : total & informative
            if ($datatype != 'swgs') {
			    $line .= $nrprobes . ' & ';
			    // $csvline .= "$nrprobes;";
            }
            else {
                $line .= "$nrprobes ($nrtotalprobes)".' & ';
			    // $csvline .= "$nrprobes ($nrtotalprobes)";
            }
		}
		if (isset($_POST['inheritance'])) {
			$line .= $inharray[$inh] . ' & ';
			$csvline .= $inharray[$inharray[$inh]] . $del; 
		}
		$img = '';
		if (isset($_POST['DC'])) {
			$class_report = $dc[$class_db]['abbr'].'.';
			if ($class_report != '') {
				$logq = mysql_query("SELECT uid, entry,arguments FROM `log` WHERE aid = '$aid' ORDER BY time DESC");
				while ($logrow = mysql_fetch_array($logq)) {
					if (preg_match('/Diagnostic Class/', $logrow['entry']) && preg_match('/Applied Rule/',$logrow['arguments'])) {
						$class_report .= '$\hspace{-0.15cm}\includegraphics[height=1.2em]{../images/bullets/classifierhigh.png}$';
						$img = 1;
						break;
					}
				}
			}
		
			$csvclass = $class_report;
			$line .= str_replace('_','\_',$class_report) . ' & ';
			$csvline .= "$csvclass$del";
		}
		if (isset($_POST['scores'])) {
            if ($datatype != 'swgs') {
			    if ($seenby == '') {
			        $line .= '\multicolumn{3}{l}{Manually Inserted} &';
			        $csvline .= "Manually Inserted;;;";
			    }
			    else {
			        if (preg_match('/(.*- QuantiSNPv2 \()(\d*\.\d*)(\).*)/',$seenby)) {
			        	$qs = preg_replace('/(.*- QuantiSNPv2 \()(\d*\.\d*)(\).*)/', "$2", $seenby);
			        }
			        else {
			        	$qs = preg_replace('/(.*- QuantiSNP \()(\d*\.\d*)(\).*)/', "$2", $seenby);
			        }
			        $qs = round($qs, 0);
			        if ($qs == 0) {
			        	$qs = '';
			        }
			        if ($qs > 0 && $qs < $minqs) {
			        	$qsnphide++;
			        	continue;
			        }
			        if ( preg_match('/(.*- PennCNVas \()(\d*)(\.\d*)(\).*)/',$seenby)) {
			        	$ps = preg_replace('/(.*- PennCNVas \()(\d*)(\.\d*)(\).*)/',"$2",$seenby);
			        	$minps = $minpas;
			        }
			        elseif ( preg_match('/(.*- PennCNVx \()(\d*)(\.\d*)(\).*)/',$seenby)) {
			        	$ps = preg_replace('/(.*- PennCNVx \()(\d*)(\.\d*)(\).*)/',"$2",$seenby);
			        	$minps = $minpxs;
			        }
			        else {
			        	$ps = preg_replace('/(.*- PennCNV \()(\d*)(\.\d*)(\).*)/',"$2",$seenby);
			        	$minps = $minpas;
			        }
			        $ps = round($ps, 0);
			        if ($ps == 0) {
			        	$ps = '';
			        }
			        if ($ps > 0 && $ps < $minps) {
			        	$pcnvhide++;
			        	continue;
			        }
			        if (preg_match('/VanillaICE/',$seenby)) {
			        	$vs = 'X';
			        }
			        else {
			        	$vs = '';
			        }
			        $line .= $qs . ' & ' . $ps . ' & ' . $vs . ' & ';
			        $csvline .= "$qs;$ps;$vs;";
			    }
            }
		}
		// add line to the table
		$line = substr($line, 0, -2);
		$line .= '\\\\'. "\n";
		if ($img != '' && $firstline != 1) {
			$cnvtablestring = rtrim($cnvtablestring);
			$cnvtablestring =  substr($cnvtablestring,0,-2).'\vspace{-0.25em} \\\\'."\n";
		}
		$firstline = 0;
		$cnvtablestring .= $line;

		// ISCN NOTATION
		// remarks: 
		//	 - june 2023: short notation with mosaic cnvs
		//   - iscn uses pter and qter in banding info (4.3.2.1 in version 2009)
		//   - sex chromosome abberations come first
		//   - only pathogenic and VUS are included in the ISCN notation.
		//   - only CNVs listed in the report are considered (filter-dependent!)
		if ($class <= $name_dc['VUS']) {
			$iscnpart = $iscnband . "(";
			$iscnpart .= number_format($start,0,'','') . "\_";
			$iscnpart .= number_format($stop,0,'','').")x$cn";
			if ($inh > 0) {
				$iscnpart .= " ".$iscninh[$inh];
			}
			$iscnpart .= ",";
			if ($chr > 22) {
				$iscnxy .= $iscnpart;
			}
			else {
				$iscn .= $iscnpart;
			}
		}

		// SET SESSION VAR FOR KARYO PLOT
		$abstoplot[$chr][] = "$start|$stop|$cn"; 
		// collect genes
		if ($_POST['cnvgenes_'.$aid] == 'A') {
			$geneline = '';
			$csvgeneline = '';
			$printgenes = $_POST['cnvgenes_'.$aid];
			$nrcols = strlen($columns);
			$col = 0;
			$genesfound = 0;
			if ($nrgenes > 0) {
				$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
				$gquery = mysql_query("SELECT symbol, omimID, morbidTXT FROM genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");
				$numgenes = mysql_num_rows($gquery);
				$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
				while ($grow = mysql_fetch_array($gquery)) {
					$genesfound++;
					$smallgene++;
					$col++;
					$symbol = $grow['symbol'];
					$omimid = $grow['omimID'];
					if ($col == 1) {
						$geneline .= '\hspace{1.5cm}';
					}
					$csvline .= "$symbol,";
					$symbol = str_replace("_",'\_',$symbol);

					if ($grow['morbidTXT'] == '' || !isset($_POST['morbid']) ) {
						$geneline .= '{\tt{\footnotesize \textcolor{grey}{ '.$symbol.'}}}' ;
					}
					else {
						$morbidsmall++;
						$geneline .= '{\footnotesize \textbf{\textcolor{darkgrey}{' . $symbol .'}}}' ;
						if  ( (isset($_POST['exclude_FP']) && $class == $name_dc['FP']) || (isset($_POST['exclude_benign']) && $class == $name_dc['Ben']) ) {
							// do not print morbid summary
						}
						else {
							$morbidquery = "SELECT `morbidID`, `disorder`, `inheritance mode` AS inh  FROM `morbidmap` WHERE `omimID` = $omimid";
							$morbidresult = mysql_query($morbidquery);

							$morbidtxt = '';
							$csvmorbidtxt = '';
							for ($i=0; $row = mysql_fetch_array($morbidresult); ++$i) {
								if ($i > 0) {
									$morbidtxt .= ' \\\\'. "\n" . '& ';							
									$csvmorbidtxt .= "\n$symbol;";							
								}
								$mid = $row['morbidID'];
								$disorder = $row['disorder'];
								$disorder = str_replace(array('{','}','[',']'),'', $disorder);
								$csvmorbidtxt .= '#' . $mid . ';' . $disorder;
								$morbidtxt .= '\textbullet\hspace{1mm}' . $mid . ' & ' . $disorder;
								
							}

							if (!in_array($symbol, $morbidsarray)) {
								$morbidsarray[$symbol] = '\textbf{' . $symbol . '}' . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
								$morbidscsvarray[$symbol] = $symbol . ';' . $csvmorbidtxt . "\n";
								// $csvmorbidline .= "$symbol;$csvmorbidtxt\n";

							}

						}
					}

					if ($col == 9) {
						$geneline .= ' \\\\ ' ."\n"; 
						$col = 0;
					}
					else {
						$geneline .= ' & ';
					}
				}
				$csvline = substr($csvline,0,-1) ;
			}
			if (isset($_POST['genelarge'])) {
				$csvline .= "$del";
				$mquery = "SELECT symbol, omimID, morbidTXT FROM genesum WHERE chr=$chr AND ( (stop BETWEEN $largestart AND $start) OR (start BETWEEN $stop AND $largestop)) ORDER BY symbol";
				$largegenes = mysql_query($mquery);
				$numlgenes = mysql_num_rows($largegenes);
				if ($numlgenes > 0 && $genesfound == 0) {
					$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
					$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
				}	
				while ($lrow = mysql_fetch_array($largegenes)) {
					$genesfound++;
					$largegene++;
					$col++;
					$symbol = $lrow['symbol'];
					$omimid = $lrow['omimID'];
					$csvline .= "$symbol,";
					$symbol = str_replace("_",'\_',$symbol);
					if ($col == 1) {
						$geneline .= '\hspace{1.5cm}';
					}
					if ($lrow['morbidTXT'] != '' && isset($_POST['morbid'])) {
						$morbidlarge++;
						$geneline .= '{\footnotesize \textbf{\textsl{\textcolor{darkgrey}{' . $symbol .'}}}}' ;
						if  (! (isset($_POST['exclude_FP']) && $class == $name_dc['FP']) || (isset($_POST['exclude_benign']) && $class == $name_dc['Ben']) ) {
							$morbidquery = "SELECT `morbidID`, `disorder`, `inheritance mode` AS inh  FROM `morbidmap` WHERE `omimID` = $omimid";
							$morbidresult = mysql_query($morbidquery);

							$morbidtxt = '';
							$csvmorbidtxt = '';
							for ($i=0; $row = mysql_fetch_array($morbidresult); ++$i) {
								if ($i > 0) {
									$morbidtxt .= ' \\\\'. "\n" . '& ';								
									$csvmorbidtxt .= "\n$symbol;";							
								}
								$mid = $row['morbidID'];
								$disorder = $row['disorder'];
								$disorder = str_replace(array('{','}','[',']'),'', $disorder);
								$csvmorbidtxt .= '#' . $mid . ';' . $disorder;
								$morbidtxt .= '\textbullet\hspace{1mm}' . $mid . ' & ' . $disorder;
								
							}

							if (!in_array($symbol, $morbidsarray)) {
								$morbidsarray[$symbol] = $symbol . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
								$morbidscsvarray[$symbol] = $symbol . ';' . $csvmorbidtxt . "\n";
								// $csvmorbidline .= "$symbol;$csvmorbidtxt\n";
							}
						}
					}
					else {
						$geneline .= '{\tt{\footnotesize \textsl{\textcolor{grey}{'.$symbol.'}}}}' ;
					}
					if ($col == 9) {
						$geneline .= ' \\\\ ' ."\n"; 
						$col = 0;
					}
					else {
						$geneline .= ' & ';
		
					}
				}
				$csvline = substr($csvline,0,-1).";";
			}
			
			if ($genesfound > 0) {
				$col++;
				while ($col < 9 && $col != 1) {
					$geneline .= ' &  ';
					$col++;
				}
				if ($col == 9) {
					$geneline .= '\\\\'. "\n";
				}
				$geneline .= '\end{tabular}'."\n".  '\end{minipage}'  . "\n";
				$geneline .= ' } \\\\ '."\n ";	
				$cnvtablestring .= $geneline;
			}
			
		}
		
		else {
			$genesnotshown += $nrgenes;
			$notlistedcnv++;
			$mq = mysql_query("SELECT symbol FROM genesum WHERE morbidID IS NOT NULL AND chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ");
			$missedmorbid += mysql_num_rows($mq);
			
			
		}
		$csvline = substr($csvline,0,-1);
		$csvout .= "$csvline\n";	
		
	} 
	$endhead = '\hline' . "\n" ; 
	$endhead .= '\end{longtable}'."\n";
	$cnvtablestring .= $endhead;
}


$inmos = 1;
if ($inmos == 1) {
	$mostablestring = '';
	$subtitlestring = '\subsection*{\textsl{\underline{Mosaic Copy Number Variations}}}'."\n";
	$mostablestring .= $subtitlestring;
	$headerline = '';
	$columns = 'l';  # at least one for copynumber
	$csvheaderline = '';
	if (isset($_POST['cytoband'])) {
		$csvheaderline .= 'CytoBand' . $del;
	}

	$csvheaderline .= 'CNV_type'.$del;

	if (isset($_POST['genomic'])) {
		$columns .= 'l';
		$headerline .= '\textbf{Minimal Region} & ';
		$csvheaderline .= 'Minimal Region'.$del;
	}
	if (isset($_POST['maxgenomic'])) {
		$columns .= 'l';
		$headerline .= '\textbf{Maximal Region} & ';
		$csvheaderline .= 'Maximal Region'.$del;
	}
	if (isset($_POST['cytoband'])) {
		$columns .= 'l';
		$headerline .= '\textbf{CytoBand} & ';
	}

	$headerline .= ' \textbf{CN} & ';

	if (isset($_POST['size'])) {
		$columns .= 'l';
		$headerline .= ' \textbf{Min. Size} & ';
		$csvheaderline .= 'Minimal/Maximal Size'.$del;
	}
	if (isset($_POST['maxsize'])) {
		$columns .= 'l';
		$headerline .= ' \textbf{Max. Size} & ';
	}
	if (isset($_POST['nrprobes'])) {
		$columns .= 'l';
        if ($datatype != 'swgs') {
		    $headerline .= '\textbf{NrSnps} & ';
		    // $csvheaderline .= 'Number of Probes;';
        }
        else {
            $headerline .= ' \begin{minipage}{2cm} \textbf{NrBins} \vspace{-0.1cm} \newline  {\scriptsize \textbf{Used (Total)}} \end{minipage} & ';
            // $csvheaderline .= "Number of bins Informative(Total);";
        }
	}
	if (isset($_POST['DC'])) {
		$columns .= 'l';
		$headerline .= ' \textbf{D.C.} & ';
		$csvheaderline .= 'Diagnostic Class'.$del;
	}

	$csvheaderline .= "Genes in Minimal Region".$del;
	if (isset($_POST['genelarge'])) {
		$csvheaderline .= "Genes in Maximal Region".$del;
	}
	$nrcolumns = strlen($columns);
	$headerline = substr($headerline,0,-2);
	$headerline .= '\\\\ '. "\n";
	$csvheaderline = substr($csvheaderline,0,-1)."\n";
	$csvout .= "\nMOS Details:\n";
	$csvout .= $csvheaderline;
	// Starting long table for CNV entries
	// First the headers and footers
	$tableheadstring = '\begin{longtable}[l]{@{\extracolsep{\fill}}'.$columns.'}'."\n" . '\hline' . "\n";
	$mostablestring .= $tableheadstring;
	$mostablestring .= $headerline;
	$endhead = '\hline' . "\n" .'\endhead'."\n";
	$mostablestring .= $endhead;
	$longfooter = '\hline \multicolumn{'. "$nrcolumns" .'}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
	$longfooter .= '\endfoot' . "\n";
	$longfooter .= "\n";
	$longfooter .= '\endlastfoot' . "\n";
	$mostablestring .= $longfooter;


	// FILL TABLE
	$morbidstring = '';
	$csvmorbidstring = '';
	// $morbidsarray = array();
	// $morbidscsvarray = array();

	$aids = $_POST['mosinclude'];
	// $nrmos = count ($aids);
	$checkaids_mos = array();
	foreach( $aids as $key => $value) {
		$checkaids_mos[$value] = 1;
		#echo "$key => $value<br>\n"; 
	}


	$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.size, a.cn, a.type, a.mosaic_perc, a.nrgenes, a.nrsnps, a.total_nrsnps, a.seenby, a.class, a.largestart, a.largestop,a.pubmed,a.avgLogR, a.segmentZscore, a.avgZscore, a.likelihood_ratio, a.datatype FROM aberration_mosaic a WHERE a.sample = $sid AND a.idproj = $pid ORDER BY a.chr, a.start");
	$manualhide = 0;
	$classhide = 0;
	$qsnphide = 0;
	$pcnvhide = 0;
	$snphide = 0;
	$sizehide = 0;
	$morbidsmall = 0;
	$morbidlarge = 0;
	$genesmall = 0;
	$smallgene = 0;
	$largegene = 0;
	$genelarge = 0;
	$genesnotshown = 0;
	$notlistedcnv = 0;
	$missedmorbid = 0;
	$totalmoscnv = mysql_num_rows($query);
	// print_r($totalcnv);
	$pmids = '';
	$firstline = 1;
	while ($row = mysql_fetch_array($query)) {
		$line = '';
		$csvline = '';
		$aid = $row['id'];
		$chr = $row['chr'];
		$start = $row['start'];
		$stop = $row['stop'];
		// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
		foreach ($filter["1"][$chr] as $fstart => $farray) {
			if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $stop) {
				$totalmoscnv--;
				continue 2;
			}
		}
		$size = $row['size'];
		if ($size <= 0) {
			$size = $stop - $start + 1;
		}
		$cn = $row['cn'];
		$cntype = $row['type'];
		$mosperc = $row['mosaic_perc'];
		$nrgenes = $row['nrgenes'];
		$nrprobes = $row['nrsnps'];
		$nrtotalprobes = $row['total_nrsnps'];
		$datatype = $row['datatype'];
		$seenby = $row['seenby'];
		// the ids of the diagnostic classes do not align with the level of pathogenicity
		// using the sorted ids for this
		$class_db = $row['class'];
		$class =  $dc[$class_db]['sorted'];
		$pmid = $row['pubmed'];
		$largestart = $row['largestart'];
		if ($largestart == 'ter') {
			$largestartvalue = 0;
			$largestartstring = 'ter';
		}
		else {
			$largestartvalue = $largestart;
			$largestartstring = number_format($largestart,0,'',',');
		}
		$largestop = $row['largestop'];
		if ($largestop == 'ter') {
			$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$cendrow = mysql_fetch_array($cendquery);
			$largestopvalue = $cendrow['stop'];
			$largestopstring = 'ter';
		}
		else {
			$largestopvalue = $largestop;
			$largestopstring = number_format($largestop,0,'',',');
		}
		$largesize = $largestopvalue - $largestartvalue;
		# filter on class
		if ($maxclass != '' && ($maxclass < $class || $class == '')) {
			$classhide++;
			continue;
		}
		# filter on coverage
		if (($datatype == 'swgs' && $nrtotalprobes < $minsnp) || $nrprobes < $minsnp ){
			$snphide++;
			continue;
		}
		# filter on size
		if ($size < $minsize) {
			$sizehide++;
			continue;
		}
		# manual filtering
		if ($checkaids_mos[$aid] != 1) {
			$manualhide++;
			continue;
		}

		if ($pmid != '') {
			$pmids .= "$pmid,";
		}

		// GET KARYO
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostart = $cytorow['name'];
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$stop' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostop = $cytorow['name'];
		if ($cytostart == $cytostop) {
			$cytoband = $chromhash[$chr].$cytostart;
			$iscnband = $cytoband;
		}
		else {
			$cytoband = $chromhash[$chr]."$cytostart$cytostop";
			$iscnband = $chromhash[$chr]."$cytostart$cytostop";
		} 

		if (isset($_POST['cytoband'])) {
			$csvline .= "$cytoband".$del;
		}		

		if ($chr  == 23 && $gender == 'Male') {
			if ($cn < 1) {
				$cnvtype = 'Deletie';
			}
			else {
				$cnvtype = 'Duplicatie';
			}
		}
		else {
			if ($cn < 2) {
				$cnvtype = 'Deletie';
			}
			else {
				$cnvtype = 'Duplicatie';
			}

		}
		$csvline .= "$cnvtype$del";
		
		if (isset($_POST['genomic'])) {
			$region_fullcontext = "Chr" . $chromhash[$chr] . ":". number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');	
			$region = $start . "-" . $stop;	
			$line .= $region_fullcontext .' & ';
			$csvline .= "$region$del";
		}
		if (isset($_POST['maxgenomic'])) {
			$maxregion_fullcontext = "Chr" . $chromhash[$chr] . ":". $largestartstring . '-' . $largestopstring;
			$maxregion = $largestart . '-' . $largestop;
			$line .= $maxregion_fullcontext .' & ';
			$csvline .= "$maxregion$del";
		}
		
		if (isset($_POST['cytoband'])) {
			$line .= $cytoband . ' & ';
		}

		$line .= $cn .' & ';
		
		if (isset($_POST['size'])) {
			$line .= number_format($size,0,'',',') . ' & ';
			if ($largesize >= 1000000) {
				$csvline .= number_format($size/1000000, 2, ',', '') . '-' . number_format($largesize/1000000, 2, ',', '') . ' Mb' . $del;
			}
			elseif ($largesize >= 1000 ) {
				$csvline .= number_format($size/1000, 2, ',', '') . '-' . number_format($largesize/1000, 2, ',', '') . ' kb' . $del;
			}
			else {
				$csvline .= $size . '-' . $largesize . ' bp' . $del;
			}
		}
		if (isset($_POST['maxsize'])) {
			$line .= number_format($largesize,0,'',',') . ' & ';
			// $csvline .= "$largesize;";
		}

		if (isset($_POST['nrprobes'])) {
            // for sWGS : total & informative
            if ($datatype != 'swgs') {
			    $line .= $nrprobes . ' & ';
			    // $csvline .= "$nrprobes;";
            }
            else {
                $line .= "$nrprobes ($nrtotalprobes)".' & ';
			    // $csvline .= "$nrprobes ($nrtotalprobes)";
            }
		}
		$img = '';
		if (isset($_POST['DC'])) {
			$class_report = $dc[$class_db]['abbr'].'.';
			if ($class_report != '') {
				$logq = mysql_query("SELECT uid, entry,arguments FROM `log_mos` WHERE aid = '$aid' ORDER BY time DESC");
				while ($logrow = mysql_fetch_array($logq)) {
					if (preg_match('/Diagnostic Class/', $logrow['entry']) && preg_match('/Applied Rule/',$logrow['arguments'])) {
						$class_report .= '$\hspace{-0.15cm}\includegraphics[height=1.2em]{../images/bullets/classifierhigh.png}$';
						$img = 1;
						break;
					}
				}
			}
		
			$csvclass = $class_report;
			$line .= str_replace('_','\_',$class_report) . ' & ';
			$csvline .= "$csvclass".$del;
		}
		// add line to the table
		$line = substr($line, 0, -2);
		$line .= '\\\\'. "\n";
		if ($img != '' && $firstline != 1) {
			$mostablestring = rtrim($mostablestring);
			$mostablestring =  substr($mostablestring,0,-2).'\vspace{-0.25em} \\\\'."\n";
		}
		$firstline = 0;
		$mostablestring .= $line;

		if ($class <= $name_dc['VUS']) {
			$iscnpart = $iscnband . "(";
			$iscnpart .= number_format($start,0,'','') . "\_";
 			if ($chr > 22 && $gender == 'Male') {
				$wcn = $iscnmoscn_XY[$cntype];
			}
			else {
				$wcn = $iscnmoscn[$cntype];
			}

			$iscnpart .= number_format($stop,0,'','').")x".$wcn."[".$mosperc."]";
			$iscnpart .= ",";
			if ($chr > 22) {
				$iscnxy .= $iscnpart;
			}
			else {
				$iscn .= $iscnpart;
			}
		}
		// SET SESSION VAR FOR KARYO PLOT
		$abstoplot[$chr][] = "$start|$stop|$cn"; 
		// collect genes
		if ($_POST['mosgenes_'.$aid] == 'A') {
			$geneline = '';
			$csvgeneline = '';
			$printgenes = $_POST['mosgenes_'.$aid];
			$nrcols = strlen($columns);
			$col = 0;
			$genesfound = 0;
			if ($nrgenes > 0) {
				$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
				$gquery = mysql_query("SELECT symbol, omimID, morbidTXT FROM genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");
				$numgenes = mysql_num_rows($gquery);
				$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
				while ($grow = mysql_fetch_array($gquery)) {
					$genesfound++;
					$smallgene++;
					$col++;
					$symbol = $grow['symbol'];
					$omimid = $grow['omimID'];
					if ($col == 1) {
						$geneline .= '\hspace{1.5cm}';
						$csvline .= "$symbol,";
					}
					$symbol = str_replace("_",'\_',$symbol);

					if ($grow['morbidTXT'] == '' || !isset($_POST['morbid']) ) {
						$geneline .= '{\tt{\footnotesize \textcolor{grey}{ '.$symbol.'}}}' ;
					}
					else {
						$morbidsmall++;
						$geneline .= '{\footnotesize \textbf{\textcolor{darkgrey}{' . $symbol .'}}}' ;
						if  ( (isset($_POST['exclude_FP']) && $class == $name_dc['FP']) || (isset($_POST['exclude_benign']) && $class == $name_dc['Ben']) ) {
							// do not print morbid summary
						}
						else {
							$morbidquery = "SELECT `morbidID`, `disorder`, `inheritance mode` AS inh  FROM `morbidmap` WHERE `omimID` = $omimid";
							$morbidresult = mysql_query($morbidquery);

							$morbidtxt = '';
							$csvmorbidtxt = '';
							for ($i=0; $row = mysql_fetch_array($morbidresult); ++$i) {
								if ($i > 0) {
									$morbidtxt .= ' \\\\'. "\n" . '& ';							
									$csvmorbidtxt .= "\n$symbol;";							
								}
								$mid = $row['morbidID'];
								$disorder = $row['disorder'];
								$disorder = str_replace(array('{','}','[',']'),'', $disorder);
								$csvmorbidtxt .= '#' . $mid . ';' . $disorder;
								$morbidtxt .= '\textbullet\hspace{1mm}' . $mid . ' & ' . $disorder;
								
							}

							if (!in_array($symbol, $morbidsarray)) {
								$morbidsarray[$symbol] = '\textbf{' . $symbol . '}' . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
								$morbidscsvarray[$symbol] = $symbol . ';' . $csvmorbidtxt . "\n";
								// $csvmorbidline .= "$symbol;$csvmorbidtxt\n";

							}

						}
					}

					if ($col == 9) {
						$geneline .= ' \\\\ ' ."\n"; 
						$col = 0;
					}
					else {
						$geneline .= ' & ';
					}
				}
				$csvline = substr($csvline,0,-1) ;
			}
			if (isset($_POST['genelarge'])) {
				$csvline .= "$del";
				$mquery = "SELECT symbol, omimID, morbidTXT FROM genesum WHERE chr=$chr AND ( (stop BETWEEN $largestart AND $start) OR (start BETWEEN $stop AND $largestop)) ORDER BY symbol";
				$largegenes = mysql_query($mquery);
				$numlgenes = mysql_num_rows($largegenes);
				if ($numlgenes > 0 && $genesfound == 0) {
					$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
					$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
				}	
				while ($lrow = mysql_fetch_array($largegenes)) {
					$genesfound++;
					$largegene++;
					$col++;
					$symbol = $lrow['symbol'];
					$omimid = $lrow['omimID'];
					$csvline .= "$symbol,";
					$symbol = str_replace("_",'\_',$symbol);
					if ($col == 1) {
						$geneline .= '\hspace{1.5cm}';
					}
					if ($lrow['morbidTXT'] != '' && isset($_POST['morbid'])) {
						$morbidlarge++;
						$geneline .= '{\footnotesize \textbf{\textsl{\textcolor{darkgrey}{' . $symbol .'}}}}' ;
						if  (! (isset($_POST['exclude_FP']) && $class == $name_dc['FP']) || (isset($_POST['exclude_benign']) && $class == $name_dc['Ben']) ) {
							$morbidquery = "SELECT `morbidID`, `disorder`, `inheritance mode` AS inh  FROM `morbidmap` WHERE `omimID` = $omimid";
							$morbidresult = mysql_query($morbidquery);

							$morbidtxt = '';
							$csvmorbidtxt = '';
							for ($i=0; $row = mysql_fetch_array($morbidresult); ++$i) {
								if ($i > 0) {
									$morbidtxt .= ' \\\\'. "\n" . '& ';								
									$csvmorbidtxt .= "\n$symbol;";							
								}
								$mid = $row['morbidID'];
								$disorder = $row['disorder'];
								$disorder = str_replace(array('{','}','[',']'),'', $disorder);
								$csvmorbidtxt .= '#' . $mid . ';' . $disorder;
								$morbidtxt .= '\textbullet\hspace{1mm}' . $mid . ' & ' . $disorder;
								
							}

							if (!in_array($symbol, $morbidsarray)) {
								$morbidsarray[$symbol] = $symbol . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
								$morbidscsvarray[$symbol] = $symbol . ';' . $csvmorbidtxt . "\n";
								// $csvmorbidline .= "$symbol;$csvmorbidtxt\n";
							}
						}
					}
					else {
						$geneline .= '{\tt{\footnotesize \textsl{\textcolor{grey}{'.$symbol.'}}}}' ;
					}
					if ($col == 9) {
						$geneline .= ' \\\\ ' ."\n"; 
						$col = 0;
					}
					else {
						$geneline .= ' & ';
		
					}
				}
				$csvline = substr($csvline,0,-1).";";
			}
			
			if ($genesfound > 0) {
				$col++;
				while ($col < 9 && $col != 1) {
					$geneline .= ' &  ';
					$col++;
				}
				if ($col == 9) {
					$geneline .= '\\\\'. "\n";
				}
				$geneline .= '\end{tabular}'."\n".  '\end{minipage}'  . "\n";
				$geneline .= ' } \\\\ '."\n ";	
				$mostablestring .= $geneline;
			}
			
		}
		
		else {
			$genesnotshown += $nrgenes;
			$notlistedcnv++;
			$mq = mysql_query("SELECT symbol FROM genesum WHERE morbidID IS NOT NULL AND chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ");
			$missedmorbid += mysql_num_rows($mq);
			
			
		}
		$csvline = substr($csvline,0,-1);
		$csvout .= "$csvline\n";	
		
	} 
	$endhead = '\hline' . "\n" ; 
	$endhead .= '\end{longtable}'."\n";
	$mostablestring .= $endhead;
}





//////////////////////////////////////////////////
// SECOND : LOH (plink) INFO FOR SEPERATE TABLE //
//////////////////////////////////////////////////
if ($incloh == 1) {
	$lohtablestring = '';
	$lohsubtitlestring = '\subsection*{\textsl{\underline{LOH Regions}}}'."\n";
	$lohtablestring .= $lohsubtitlestring;
	$lohheaderline = '';
	$lohcolumns = '';
	$csvlohheaderline = "";
	if (isset($_POST['genomic'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= '\textbf{Minimal Region} & ';
		$csvlohheaderline .= "Minimal Region;";
	}
/*
	if (isset($_POST['maxgenomic'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= '\textbf{Maximal Region} & ';
		$csvlohheaderline .= "Maximal Region;";
	}
*/
	if (isset($_POST['cytoband'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= '\textbf{CytoBand} & ';
		$csvlohheaderline .= "CytoBand;";
	}
	if (isset($_POST['size'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= ' \textbf{Min. Size} & ';
		$csvlohheaderline .= "Minimal Size;";
	}
/*
	if (isset($_POST['maxsize'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= ' \textbf{Max. Size} & ';
		$csvlohheaderline .= "Maximal Size;";
	}
*/
	if (isset($_POST['nrprobes'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= ' \textbf{Nr. Probes} & ';
		$csvlohheaderline .= "Number of Probes;";
	}

	if (isset($_POST['nrgenes'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= ' \textbf{Nr. Genes} & ';
		$csvlohheaderline .= "Number of Genes;";
	}

/*
	if (isset($_POST['DC'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= ' \textbf{D.C.} & ';
		$csvlohheaderline .= "Diagnostic Class;";
	}
	if (isset($_POST['scores'])) {
		$lohcolumns .= 'l';
		$lohheaderline .= ' \textbf{Hom.Fraction} & ';
		$csvlohheaderline .= "Homozygous Fraction;";
	}
*/
	$csvlohheaderline .= "Genes in Minimal Region".$del;
/*	if (isset($_POST['genelarge'])) {
		$csvlohheaderline .= "Genes in Maximal Region;";
	}
*/
	$nrlohcolumns = strlen($lohcolumns);
	$lohheaderline = substr($lohheaderline,0,-2);
	$lohheaderline .= '\\\\ '. "\n";
	$csvheaderline = substr($csvlohheaderline,0,-1)."\n";
	$csvout .= "\nLOH Details:\n";
	$csvout .= "$csvlohheaderline\n";
	
	// Starting long table for loh entries
	// First the headers and footers
	$lohtableheadstring = '\begin{longtable}[l]{@{\extracolsep{\fill}}'.$lohcolumns.'}'."\n" . '\hline' . "\n";
	$lohtablestring .= $lohtableheadstring;
	$lohtablestring .= $lohheaderline;
	$lohendhead = '\hline' . "\n" .'\endhead'."\n";
	$lohtablestring .= $lohendhead;
	$lohlongfooter = '\hline \multicolumn{'. "$nrlohcolumns" .'}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
	$lohlongfooter .= '\endfoot' . "\n";
	$lohlongfooter .= "\n";
	$lohlongfooter .= '\endlastfoot' . "\n";
	$lohtablestring .= $lohlongfooter;
	// morbid is added to general list if specified 
	//$morbidstring = '';
	//$morbidsarray = array();

	// get included loh regions 
	$lohaids = $_POST['lohinclude'];
	foreach( $lohaids as $key => $value) {
		$checklohaids[$value] = 1;
	}
	// get all loh regions
	$lohquery = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.size, a.nrgenes, a.nrsnps, a.seenby,  a.class, a.largestart, a.largestop, a.pubmed FROM aberration_LOH a WHERE a.sample = $sid AND a.idproj = $pid ORDER BY a.chr, a.start");

	$lohmanualhide = 0;
	$lohclasshide = 0;
	$totallohsize = 0;
	//$qsnphide = 0;
	//$pcnvhide = 0;
	//$snphide = 0;
	$lohsizehide = 0;
	$lohmorbidsmall = 0;
	$lohmorbidrec = 0;
	$lohmorbidlarge = 0;
	$lohgenesmall = 0;
	$lohsmallgene = 0;
	$lohlargegene = 0;
	$lohgenelarge = 0;
	$lohgenesnotshown = 0;
	$notlistedloh = 0;
	$lohmissedmorbid = 0;
	$totalloh = mysql_num_rows($lohquery);
	$lohiscn = '';
	$lohiscnxy = '';
	//$pmids = '';
	while ($row = mysql_fetch_array($lohquery)) {
		$line = '';
		$csvline = '';
		$aid = $row['id'];
		$chr = $row['chr'];
		$start = $row['start'];
		$stop = $row['stop'];
		// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
		foreach ($filter["1"][$chr] as $fstart => $farray) {
			if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $stop) {
				$totalloh--;
				continue 2;
			}
		}	
		$size = $row['size'];
		if ($size <= 0) {
			$size = $stop - $start + 1;
		}
		$nrgenes = $row['nrgenes'];
		$nrprobes = $row['nrsnps'];
		$seenby = $row['seenby'];
		// the ids of the diagnostic classes do not align with the level of pathogenicity
		// using the sorted ids for this
		$class_db = $row['class'];
		$class =  $dc[$class_db]['sorted'];
		$pmid = $row['pubmed'];
		$largestart = $row['largestart'];
		if ($largestart == 'ter') {
			$largestartvalue = 0;
			$largestartstring = 'ter';
		}
		else {
			$largestartvalue = $largestart;
			$largestartstring = number_format($largestart,0,'',',');
		}
		$largestop = $row['largestop'];
		if ($largestop == 'ter') {
			$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$cendrow = mysql_fetch_array($cendquery);
			$largestopvalue = $cendrow['stop'];
			$largestopstring = 'ter';
		}
		else {
			$largestopvalue = $largestop;
			$largestopstring = number_format($largestop,0,'',',');
		}
		$largesize = $largestopvalue - $largestartvalue - 1;
		# filter on class
		if ($lohmaxclass != '' && ($lohmaxclass < $class || $class == '')) {
			$lohclasshide++;
			continue;
		}
		# filter on coverage
		if ($nrprobes < $minsnp) {
			$snphide++;
			continue;
		}
		# filter on size
		if ($size < $lohminsize) {
			$lohsizehide++;
			continue;
		}
		# manual filtering
		if ($checklohaids[$aid] != 1) {
			$lohmanualhide++;
			continue;
		}
		if ($pmid != '') {
			$pmids .= "$pmid,";
		}
		if (isset($_POST['genomic'])) {
			$region = "Chr" . $chromhash[$chr] . ":". number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');	
			$line .= $region .' & ';
			$csvline .= "$region;";
		}
/*		if (isset($_POST['maxgenomic'])) {
			$maxregion = "Chr" . $chromhash[$chr] . ":". $largestartstring . '-' . $largestopstring;
			$line .= $maxregion .' & ';
			$csvline .= "$maxregion;";
		}
*/		
		// GET KARYO
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostart = $cytorow['name'];
		$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$stop' BETWEEN start AND stop)");
		$cytorow = mysql_fetch_array($cytoquery);
		$cytostop = $cytorow['name'];
		if ($cytostart == $cytostop) {
			$cytoband = $chromhash[$chr].$cytostart;
			$iscnband = $cytoband;
		}
		else {
			$cytoband = $chromhash[$chr]."$cytostart-$cytostop";
			$iscnband = $chromhash[$chr]."$cytostart$cytostop";
		} 
		if (isset($_POST['cytoband'])) {
			$line .= $cytoband . ' & ';
			$csvline .= "$cytoband;";
		}
		//$line .= $cn .' & ';
		if (isset($_POST['size'])) {
			$line .= number_format($size,0,'',',') . ' & ';
			$csvline .= "$size;";
		}
/*
		if (isset($_POST['maxsize'])) {
			$line .= number_format($largesize,0,'',',') . ' & ';
			$csvline .= "$largesize;";
		}
*/
		if (isset($_POST['nrprobes'])) {
			$line .= $nrprobes . ' & ';
			$csvline .= "$nrprobes;";
		}

		if (isset($_POST['nrgenes'])) {
			// if ($nrrecmorbid > 0 ) {
			// 	$nrgenes .=  " ($nrrecmorbid)";
			// } 
			$line .= $nrgenes . ' & ';
			$csvline .= "$nrgenes;";
		}

		// add line to the table
		$line = substr($line, 0, -2);
		$line .= '\\\\'. "\n";
		$lohtablestring .= $line;
	
		// count size of all LOH regions
		$totallohsize += $size;

		// ISCN NOTATION
		// remarks: 
		//   - iscn uses pter and qter in banding info (4.3.2.1 in version 2009)
		//   - sex chromosome abberations come first
		//   - only class 1-2-3 are included in the ISCN notation.
		//   - only CNVs listed in the report are considered (filter-dependent!)
		/*
		if ($class == 1 || $class == 2 || $class == 3) {
			$iscnpart = $iscnband . "(";
			if ($largestartstring =='ter') {
				$iscnpart .= 'ter'. "-" ;
			}
			else {
				$iscnpart .= $largestartvalue;
				if ($chr > 22 && $gender == 'Male') {
					$iscnpart .= "x1,";
				}
				else {
					$iscnpart .= "x2,";
				}
				#$iscnpart .= number_format($start,0,'',',') . "-";
				$iscnpart .= number_format($start,0,'','') . "-";
			}
			if ($largestopstring == 'ter') {
				$iscnpart .= "terx$cn)";
			}
			else {
				#$iscnpart .= number_format($stop,0,'',',')."x$cn";
				$iscnpart .= number_format($stop,0,'','')."x$cn";
				$iscnpart .= ",$largestopvalue";
				if ($chr > 22 && $gender == 'Male') {
					$iscnpart .= "x1)";
				}
				else {
					$iscnpart .= "x2)";
				}
			}
			if ($inh > 0) {
				$iscnpart .= " ".$iscninh[$inh];
			}
			$iscnpart .= ",";
			if ($chr > 22) {
				$iscnxy .= $iscnpart;
			}
			else {
				$iscn .= $iscnpart;
			}
		}
		*/
		// SET SESSION VAR FOR KARYO PLOT
		$abstoplot[$chr][] = "$start|$stop|2"; 
			
		// collect genes
		if ($_POST['lohgenes_'.$aid] == 'A') {
			$geneline = '';
			$csvgeneline = "";
			$printgenes = $_POST['lohgenes_'.$aid];
			$nrcols = strlen($lohcolumns);
			$col = 0;
			$lohgenesfound = 0;
			if ($nrgenes > 0) {
				$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
				$gquery = mysql_query("SELECT symbol, omimID,morbidTXT FROM genesum WHERE chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ORDER BY symbol");	
				$numgenes = mysql_num_rows($gquery);
				$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
				while ($grow = mysql_fetch_array($gquery)) {
					$lohgenesfound++;
					$lohsmallgene++;
					$col++;
					$symbol = $grow['symbol'];
					$omimid = $grow['omimID'];
					$csvline .= "$symbol,";
					$symbol = str_replace("_",'\_',$symbol);
					if ($col == 1) {
						$geneline .= '\hspace{1.5cm}';
					}
					if ($grow['morbidTXT'] != '' && isset($_POST['lohmorbid'])) {
						$lohmorbidsmall++;
						$geneline .= '{\footnotesize \textbf{\textcolor{darkgrey}{' . $symbol .'}}}' ;
						$morbidquery = "SELECT `morbidID`, `disorder`, `inheritance mode` AS inh  FROM `morbidmap` WHERE `omimID` = $omimid";
						$morbidresult = mysql_query($morbidquery);

						$recessive = False;
						$morbidtxt = '';
						$csvmorbidtxt = '';
						for ($i=0; $row = mysql_fetch_array($morbidresult); ++$i) {
							if ($i > 0) {
								$morbidtxt .= ' \\\\'. "\n" . '& ';								
								$csvmorbidtxt .= "\n$symbol;";							
							}
							$mid = $row['morbidID'];
							$disorder = $row['disorder'];
							$disorder = str_replace(array('{','}','[',']'),'', $disorder);
							// gene associated with at least one recessive disorder in OMIM?
							if (preg_match('/\brecessive\b/',$row['inh'])) {
								$recessive = True;	
							}	
							$csvmorbidtxt .= '#' . $mid . ';' . $disorder;
							$morbidtxt .= '\textbullet\hspace{1mm}' . $mid . ' & ' . $disorder;
							
						}

						// only print recessive genes for LOH regions
						if ($recessive && !in_array($symbol,$morbidsarray)) {
							$lohmorbidrec++;
							$morbidsarray[$symbol] = '\color{red}{' . $symbol . '}' . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
							$morbidscsvarray[$symbol] = $symbol . ';' . $csvmorbidtxt . "\n";
							// $csvmorbidline .= "$symbol;$csvmorbidtxt\n";

						}
					}
					else {
						$geneline .= '{\tt{\footnotesize \textcolor{grey}{ '.$symbol.'}}}' ;
					}
					if ($col == 9) {
						$geneline .= ' \\\\ ' ."\n"; 
						$col = 0;
					}
					else {
						$geneline .= ' & ';
					}
				}
				$csvline = substr($csvline,0,-1);
				//$csvline .= "$csvgeneline;";
			}
			if (isset($_POST['genelarge'])) {
				$csvline .= ";";
				$largegenes = mysql_query("SELECT symbol, omimID, morbidTXT FROM genesum WHERE chr=$chr AND ( (stop BETWEEN $largestart AND $start) OR (start BETWEEN $stop AND $largestop)) ORDER BY symbol");
				$numlgenes = mysql_num_rows($largegenes);
				if ($numlgenes > 0 && $lohgenesfound == 0) {
					$geneline = '\multicolumn{'. $nrcols. '}{l}{'." \n". ' \begin{minipage}{\fill} '."\n" ;  // Start multi column cell to host the nested tabular
					$geneline .= '\indent \indent \begin{tabular}{p{3.7cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}p{2.2cm}} '."\n";
				}	
				while ($lrow = mysql_fetch_array($largegenes)) {
					$lohgenesfound++;
					$lohlargegene++;
					$col++;
					$symbol = $lrow['symbol'];
					$omimid = $lrow['omimID'];
					$csvline .= "$symbol,";
					$symbol = str_replace("_",'\_',$symbol);
					if ($col == 1) {
						$geneline .= '\hspace{1.5cm}';
					}
					if ($lrow['morbidTXT'] != '' && isset($_POST['lohmorbid'])) {
						$morbidlarge++;
						$geneline .= '{\footnotesize \textbf{\textsl{\textcolor{darkgrey}{' . $symbol .'}}}}' ;

						$morbidquery = "SELECT `morbidID`, `disorder`, `inheritance mode` AS inh  FROM `morbidmap` WHERE `omimID` = $omimid";
						$morbidresult = mysql_query($morbidquery);

						$recessive = False;
						$morbidtxt = '';
						$csvmorbidtxt = '';
						for ($i=0; $row = mysql_fetch_array($morbidresult); ++$i) {
							if ($i > 0) {
								$morbidtxt .= ' \\\\'. "\n" . '& ';								
								$csvmorbidtxt .= "\n$symbol;";							
							}
							$mid = $row['morbidID'];
							$disorder = $row['disorder'];
							$disorder = str_replace(array('{','}','[',']'),'', $disorder);
							// gene associated with at least one recessive disorder in OMIM?
							if (preg_match('/\brecessive\b/',$row['inh'])) {
								$recessive = True;	
							}	

							$csvmorbidtxt .= '#' . $mid . ';' . $disorder;
							$morbidtxt .= '\textbullet\hspace{1mm}' . $mid . ' & ' . $disorder;
						}

						if ($recessive && !in_array($symbol,$morbidsarray)) {
							//$morbidsarray[$symbol] = $symbol . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
							$morbidsarray[$symbol] = '\textsl{' . $symbol . '}' . ' & ' . str_replace('_','\_',$morbidtxt) . '\\\\' ."\n";
							$morbidscsvarray[$symbol] = $symbol . ';' . $csvmorbidtxt . "\n";
							// $csvmorbidline .= "$symbol;$csvmorbidtxt\n";

						}
		
					}
					else {
						$geneline .= '{\tt{\footnotesize \textsl{\textcolor{grey}{'.$symbol.'}}}}' ;
					}
					if ($col == 9) {
						$geneline .= ' \\\\ ' ."\n"; 
						$col = 0;
					}
					else {
						$geneline .= ' & ';
		
					}
				}
				$csvline = substr($csvline,0,-1);
			}
			
			if ($lohgenesfound > 0) {
				$col++;
				while ($col < 9 && $col != 1) {
					$geneline .= ' &  ';
					$col++;
				}
				if ($col == 9) {
					$geneline .= '\\\\'. "\n";
				}
				$geneline .= '\end{tabular}'."\n".  '\end{minipage}'  . "\n";
				$geneline .= ' } \\\\ '."\n ";	
				//$lohtablestring .= $geneline;
			}
			
		}
		else {
			$lohgenesnotshown += $nrgenes;
			$notlistedloh++;
			$mq = mysql_query("SELECT symbol FROM genesum WHERE morbidID IS NOT NULL AND chr=$chr AND ( (start BETWEEN $start AND $stop) OR (stop BETWEEN $start AND $stop) OR (start <= $start AND stop >= $stop) ) ");
			$lohmissedmorbid += mysql_num_rows($mq);
			
			
		}
		$csvout .= "$csvline\n";
		
		
	} 
	$endhead = '\hline' . "\n" ; 
	$endhead .= '\end{longtable}'."\n";
	$lohtablestring .= $endhead;
}
////////////////////////////////
// START PRINTING TO TEX FILE //
////////////////////////////////

// PRINT ISCN NOTATION
if ($iscn != '' || $iscnxy != '') {
    if ($datatype == 'swgs') {
        $iscn = "sseq[$buildname] $iscnxy$iscn";
    } else {
	    $iscn = "arr $iscnxy$iscn";
    }
	$iscn = substr($iscn, 0,-1);
	$subtitlestring = '\subsection*{\textsl{\underline{ISCN Notation}}}'."\n";
	$string = 'Note: ISCN notation is composed of class 1 and class 2 aberrations.'."\n\n";
	//fwrite($fh,$subtitlestring);
	$pdfout .= $subtitlestring;
	$pdfout .= '{\small ';
	//fwrite($fh,'{\small ');
	//fwrite($fh,$iscn);
	$pdfout .= str_replace(",",',\hspace{0cm}',$iscn);
	//fwrite($fh,' } '."\n");
	$pdfout .= ' } '."\n";
	$csvout .= "\nISCN Notation:\n";
	$iscn = str_replace('\\', '', $iscn);
	$csvout .= "$iscn\n";
}


// print loh statistics
if ($incloh == 1) {
	$subtitlestring = '\subsection*{\textsl{\underline{LOH Statistics}}}'."\n";
	//fwrite($fh,$subtitlestring);
	$pdfout .= $subtitlestring;
	// $csvout .= "\nLOH Statistics:\n";
	$filter = '{\small ';
	$filter .= '\begin{tabular}{p{0.4\textwidth}p{0.3\textwidth}p{0.27\textwidth} }' ."\n";
	$filter .= '\hline' . "\n" .'Filter Item & Treshold & Filtered Regions \\\\ ' ."\n";
	// $csvout .= "Filter Item;Treshold;Filtered CNVs\n";
	$filter .= '\hline' . "\n";
	if ($lohmaxclass == '') {
		$filter .= 'Shown diagnostic classes & All (including non specified) & 0 \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;All (including non specified);0\n";
	}
	elseif ($lohmaxclass == $name_dc['FP']) {
		$filter .= 'Shown diagnostic classes & Pathogenic, VUS, Benign, FP & ' . $lohclasshide . ' \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;Pathogenic, VUS, Benign, FP;$lohclasshide\n";

	}
	elseif ($lohmaxclass == $name_dc['Ben']) {
		$filter .= 'Shown diagnostic classes & Pathogenic, VUS, Benign  & ' . $lohclasshide . ' \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;Pathogenic, VUS, Benign;$lohclasshide\n";

	}
	elseif ($lohmaxclass == $name_dc['VUS']) {
		$filter .= 'Shown diagnostic classes & Pathogenic, VUS  & ' . $lohclasshide . ' \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;Pathogenic, VUS;$lohclasshide\n";
	}
	elseif ($lohmaxclass == $name_dc['L.Path']) {
		$filter .= 'Shown diagnostic classes & Pathogenic, Pathogenic recessive, Likely Pathogenic  &' . $lohclasshide . ' \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;Pathogenic, Pathogenic recessive, Likely Pathogenic;$lohclasshide\n";
	}
	elseif ($lohmaxclass == $name_dc['Path (rec)']) {
		$filter .= 'Shown diagnostic classes & Pathogenic, Pathogenic recessive &' . $lohclasshide . ' \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;Pathogenic, Pathogenic recessive;$lohclasshide\n";
	}
	elseif ($lohmaxclass == $name_dc['Path']) {
		$filter .= 'Shown diagnostic classes & Pathogenic &' . $lohclasshide . ' \\\\ '. "\n";
		// $csvout .= "Shown diagnostic classes;Pathogenic;$lohclasshide\n";
	}


	//$filter .= 'Minimal Coverage & ' . $minsnp ." & $snphide "  . '\\\\' . "\n";
	$filter .= 'Minimal LOH size {\footnotesize (in bp)} & ' . number_format($lohminsize,0,'',',') ." & $lohsizehide " . '\\\\'."\n";
	// $csvout .= "Minimal LOH size (in bp);$lohminsize;$lohsizehide\n";

	if ($lohmanualhide > 0) {
		$filter .= 'Manually Hided LOH regions & &' . $lohmanualhide . '\\\\' . "\n";
		// $csvout .= "Manually Hided LOH regions;;$lohmanualhide\n";
	}
	$log_totalsize = number_format($totallohsize,0,'',',');
	$loh_genome_perc = number_format($totallohsize/$genomesize*100,2,'.',',');
	$filter .= 'Total LOH size {\footnotesize (in bp)} & & ' . $log_totalsize . ' (' . $loh_genome_perc . '\% genome)' . '\\\\'."\n";

	$filter .= '\hline' ."\n";
	$showncnv = $totalloh - $lohclasshide - $lohsizehide - $lohmanualhide;
	$filter .= '\multicolumn{2}{r}{{\textsl{Shown LOH regions:}}} & \textsl{'.$showncnv.' out of ' . $totalloh .'} \\\\' ."\n";
	// $csvout .= "Shown LOH regions (out of $totalloh);;$showncnv\n";
	$filter .= '\hline'."\n".'\end{tabular}'."\n";
	//fwrite($fh,$filter);
	$pdfout .= $filter;
	//fwrite($fh, ' } '."\n"); // end of 'small'
	$pdfout .= ' } '."\n";
	
}


// PRINT KARYOGRAM
if ($format == 'pdf') {
	echo "<li>Create Karyogram</li>";
	flush();

	if ($_POST['inckaryo'] == 1) {
		include('karyoreport.php');
		$karyostring = '';
		$karyostring .= '\subsection*{\textsl{\underline{Karyogram}}}'."\n";
		$karyostring .= '\includegraphics[width=0.90\textwidth]{'.$samplename.'.png}'."\n";
		//fwrite($fh,$karyostring);
		$pdfout .= $karyostring;
	}
}
// PRINT MORBID GENES IF FOUND
if (count($morbidsarray) > 0) {
//if ($morbidline != '') {
	// sort array	
	$subtitlestring = '\subsection*{\textsl{\underline{MORBID Gene Summaries}}}'."\n";
	if ($incloh == 1) {
		$subtitlestring .= 'REMARK: for LOH regions, only recessive MORBID genes are listed  \\\\' . "\n";
	}
	//fwrite($fh,$subtitlestring);
	$csvout .= "\nRecessive MORBID Gene information\n";
	ksort($morbidscsvarray);
	foreach ($morbidscsvarray as $symbol => $entry) {
		$csvout .= $entry;
   	}
   	// $csvout .= $csvmorbidline;
	$pdfout .= $subtitlestring;
	//$morbidtable = '\begin{longtable}{lll}' ."\n";
	$morbidtable = '\begin{longtable}{p{3cm}p{2.5cm}p{17cm}}' ."\n";
	$morbidtable .= '\hline' . "\n" .'Gene Symbol & MORBID ID & MORBID Summary \\\\ ' ."\n";
	$morbidtable .= '\hline' . "\n". '\endhead' . "\n";
	$morbidtable .= '\hline \multicolumn{2}{r}{{\textsl{\footnotesize Continued on next page}}} \\\\ \hline' ."\n";
	$morbidtable .= '\endfoot' . "\n";
	$morbidtable .= "\n";
	$morbidtable .= '\endlastfoot' . "\n";
	//$morbidtable .= '\hline' . "\n" .'\\\\ \textbf{\underline{Recessive MORBID genes}} & \\\\ ' ."\n";
	ksort($morbidsarray);
	foreach ($morbidsarray as $symbol => $entry) {
		 $morbidtable .= $entry;
	}
	$morbidtable .= '\hline'."\n".'\end{longtable}'."\n";

	//fwrite($fh,$morbidtable);
	$pdfout .= $morbidtable;
}

// print bibliography if selected
if ($_POST['incpub'] == 1) {
	$bibliostring = '';
	$csvbibliostring = '';
	if ($pmids != '') {
		$pmitems = array();
		$firstauthors = array();
		$pmids = substr($pmids,0,-1);
		require_once 'xmlLib2.php';
		$link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=$pmids&retmode=xml&rettype=abstract";
		system("wget -O '/tmp/$pid.$sid.fetching' '$link'");
		flush();
		$abArray =  my_xml2array("/tmp/$pid.$sid.fetching");
		unlink("/tmp/$pid.$sid.fetching");
		$abArray = get_value_by_path($abArray, 'PubmedArticleSet');
		foreach ($abArray as $key => $value) {
			if (!is_numeric($key)) {
				# skip name tags etc
				continue;
			}
			$subarray = array('name' => 'PubmedArticleSet','0' => $abArray[$key]);
			$title = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/ArticleTitle');
			$title = $title['value'];
			$pmid = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/PMID');
			$pmid = $pmid['value'];
			$date = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/PubDate');
			$volume = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/Volume');
			$volume = $volume['value'];
			$issue = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/Issue');
			$issue = $issue['value'];
			$pages = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Pagination/MedlinePgn');
			$pages = $pages['value'];
			$datestring = $date['Year'] .' ' . $date['Month'] . ";$volume($issue):$pages";
			$journal = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/Title');
			$journal = $journal['value'];
			$authors = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/AuthorList');
			$pmitems[$pmid]['title'] = $title;
			$pmitems[$pmid]['datestring'] = $datestring;
			$pmitems[$pmid]['journal'] = $journal;
			$astring = '';
			$firstauthor = 1;
			foreach ($authors as $akey => $aarray) {
				if (!is_numeric($akey)) {
					continue;
				}
				if ($firstauthor == 1) {
					$lname = get_value_by_path($aarray,'LastName');
					$lname = $lname['value'];
					$ini = get_value_by_path($aarray,'Initials');
					$ini = $ini['value'];
					$astring .= $lname ." " . $ini . ',' ;
					$firstauthors["$lname $datestring"] = $pmid;
					$firstauthor = 0;
				}
				else {
					$astring .= ' \textsl{et al.}';
					break;
				}
			} 
			$pmitems[$pmid]['authors'] = $astring;
		}
		ksort($firstauthors);
		foreach($firstauthors as $aut => $pmid) {
			$bibliostring .= '\item '.$pmitems[$pmid]['authors'].', '. str_replace('&','\&',$pmitems[$pmid]['title']) .' '. str_replace('&','\&',$pmitems[$pmid]['journal']) . ' '. $pmitems[$pmid]['datestring'] . '.\\\\'."\n";
			$csvbibliostring .= $pmitems[$pmid]['authors'].', '. $pmitems[$pmid]['title'] .' '. $pmitems[$pmid]['journal'] . ' '. $pmitems[$pmid]['datestring']."\n";
		}
	}

	if ($bibliostring != '') {
		$subtitlestring = '\subsection*{\textsl{\underline{References}}}'."\n";
		//fwrite($fh,$subtitlestring);
		$pdfout .= $subtitlestring;
		$csvout .= "\nBibliography\n";
		//fwrite($fh,'{\scriptsize \begin{packed_item}'.$bibliostring. ' \end{packed_item}}'."\n");
		$pdfout .= '{\scriptsize \begin{packed_item}'.$bibliostring. ' \end{packed_item}}'."\n";
		$csvout .= $csvbibliostring;
	}
}

// PRINT LOG IF SELECTED
if ($_POST['inclog'] == 1) {
	$logstring = '';
	$csvlogstring = '';
	$history = mysql_query("SELECT l.aid, l.time, u.FirstName, u.LastName, l.entry, l.arguments FROM log l JOIN users u ON u.id = l.uid WHERE l.sid = '$sid' AND l.pid = '$pid' ORDER BY l.time DESC");	
	while ($row = mysql_fetch_array($history))  {
		$aid = $row['aid'];
		$time = $row['time'];
		$FName = $row['FirstName'];
		$LName = $row['LastName'];
		$logentry = $row['entry'];
		// aid == 0 : sample level entry.
		if ($aid == 0) {
			if (preg_match("/moved from project/",$logentry)) {
				$logstring .= '- \textsl{'.$time.' : } '.str_replace('_','\_',$samplename).' was '.str_replace('_','\_',stripslashes($logentry)).' by '.$FName.' '.$LName.'\\\\'."\n";
				$csvlogstring .= "$time;$samplename $logentry;$fname $Lname\n";
			}
			continue;
		}

		$sq = mysql_query("SELECT chr, start, stop, cn FROM aberration WHERE id = '$aid'");
		$srows = mysql_num_rows($sq);
		if ($srows > 0) {
			$srow = mysql_fetch_array($sq);
			$chr = $srow['chr'];
			$chrtxt = $chromhash[$chr];
			$start = $srow['start'];
			$starttxt = number_format($start,0,'',',');
			$stop = $srow['stop'];
			$stoptxt = number_format($stop,0,'',',');
			$cn = $srow['cn'];
		}
		else {
			$ssq = mysql_query("SELECT chr, start, stop, cn FROM deletedcnvs WHERE id = '$aid'");
			$srow = mysql_fetch_array($ssq);
			$chr = $srow['chr'];
			$chrtxt = $chromhash[$chr];
			$start = $srow['start'];
			$starttxt = number_format($start,0,'',',');
			$stop = $srow['stop'];
			$stoptxt = number_format($stop,0,'',',');
			$cn = $srow['cn'];
		}
		$arguments = $row['arguments'];
		$logstring .= '- \textsl{'.$time.' : }Chr'.$chrtxt.':'.$starttxt.'-'.$stoptxt.' (cn:'.$cn.') : '.$logentry.' by '.$FName.' '.$LName;
		$csvlogstring .= "$time;Chr$chrtxt:$starttxt-$stoptxt;cn:$cn;$logentry;$FName $LName";
		if ($arguments != '') {
			$logstring.= ' : '.$arguments .'\\\\'."\n";
			$csvlogstring .= ";$arguments\n";
		}
		else {
			$logstring .= '\\\\' ."\n";
			$csvlogstring .= "\n";
		}
	}
	
	if ($logstring != '') {
		$subtitlestring = '\subsection*{\textsl{\underline{Annotation Log}}}'."\n";
		//fwrite($fh,$subtitlestring);
		$pdfout .= $subtitlestring;
		$csvout .= "\nAnnotation Log:\n";
		//fwrite($fh,'{\scriptsize '.$logstring. ' }'."\n");
		$pdfout .= '\small{'.$logstring.'}';
		$csvout .= $csvlogstring;
	}
}
// PRINT CNV & LOH TABLEs AFTER MORBID !
//fwrite($fh,'\begin{landscape}'."\n");
// $pdfout .= '\begin{landscape}'."\n";

// cnv table
//fwrite($fh,$cnvtablestring) ;
if ($incnv == 1) {
	$pdfout .= $cnvtablestring;
}
if (count($checkaids_mos) > 0) {
	$pdfout .= $mostablestring;
}

// loh table
if ($incloh == 1) {
	//fwrite($fh,$lohtablestring);
	$pdfout .= $lohtablestring;
} 

// print end of doc & end of landscape.
//fwrite($fh,'\label{endofdoc}'."\n".'\end{landscape}'."\n");
$pdfout .= '\label{endofdoc}'."\n".'\end{landscape}'."\n";

// FINISH & COMPILE
//fwrite($fh,$cnvtablestring);

//fwrite($fh, '\end{document}');
$pdfout .=  '\end{document}';


// WRITE OUT THE TEX FILE & compile & present
if ($format == 'pdf') {
	echo "<li> Preparing LaTeX document</li>\n";
	flush();
	if(file_exists("$sitedir/Reports/$samplename.tex")) {
		unlink("$sitedir/Reports/$samplename.tex");
	}
	copy("$sitedir/Reports/preamble.tex", "$sitedir/Reports/$samplename.tex");
	$fh = fopen("$sitedir/Reports/$samplename.tex", 'a');
	fwrite($fh,$pdfout);
	fclose($fh);
	echo "<li> Compiling LaTeX document</li>\n";
	flush();
	if ($_POST['inckaryo'] == 1) {
		system("chmod 777 $sitedir/Reports/$samplename.png");
	}
	system("(chmod 777 '$sitedir/Reports/$samplename.tex' && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $sitedir/Reports && perl compiletex.pl '$samplename' && chmod 777 '$sitedir/Reports/$samplename.pdf' \" ) > /dev/null 2>&1 ");
	if ($_POST['inckaryo'] == 1) {
		system("rm '$sitedir/Reports/$samplename.png' > /dev/null 2>&1");
	}
	echo "<li> Opening PDF document</li>\n";
	flush();
	echo "<li> Done. </li></ul></p><p><a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\">Go Back to the sample details</a></p>\n";
	echo "<p></p>";
	echo "<iframe height='0' width='0' src='download.php?path=Reports&file=$samplename.pdf'></iframe>\n";


}
// WRITE OUT THE CSV FILE & present
elseif ($format == 'csv') {
	echo "<li> Preparing CSV document</li>\n";
	flush();
	// if(file_exists("$sitedir/Reports/$samplename.csv") {
	// 	unlink("$sitedir/Reports/$samplename.csv");
	// }
	// file is opened in 'w' mode: if exist file is emptied before using
	$fh = fopen("$sitedir/Reports/$samplename.csv", 'w');
	fwrite($fh,$csvout);
	fclose($fh);
	echo "<li> Opening CSV document</li>\n";
	flush();
	echo "<li> Done. </li></ul></p><p><a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\">Go Back to the sample details</a></p>\n";
	echo "<p></p>";
	echo "<iframe height='0' width='0' src='download.php?path=Reports&file=$samplename.csv'></iframe>\n";
}
?>

</div>
