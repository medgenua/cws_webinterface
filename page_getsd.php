<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>

<?php
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

mysql_select_db("CNVanalysis".$_SESSION['dbname']);
$ucscdb = str_replace('-','',$_SESSION['dbname']);
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_end_flush();



// This function returns the standard deviation of an array 
// of numerical values where $std is the array name. 

function standard_deviation($std)  
{ 
    $total; 
    while(list($key,$val) = each($std)) 
        {
        $total += $val; 
        } 
    reset($std); 
    $mean = $total/count($std); 
          
    while(list($key,$val) = each($std)) 
        {  
        $sum += pow(($val-$mean),2); 
        }  
    $var = sqrt($sum/(count($std)-1)); 
    return $var;  
} 

function mmmr($array, $output = 'mean'){ 
    if(!is_array($array)){ 
        return FALSE; 
    }else{ 
        switch($output){ 
            case 'mean': 
                $count = count($array); 
                $sum = array_sum($array); 
                $total = $sum / $count; 
            break; 
            case 'median': 
                rsort($array); 
                $middle = round(count($array) / 2); 
                $total = $array[$middle-1]; 
            break; 
            case 'mode': 
                $v = array_count_values($array); 
                arsort($v); 
                foreach($v as $k => $v){$total = $k; break;} 
            break; 
            case 'range': 
                sort($array); 
                $sml = $array[0]; 
                rsort($array); 
                $lrg = $array[0]; 
                $total = $lrg - $sml; 
            break; 
        } 
        return $total; 
    } 
} 


## START ##

for ($j = 0; $j<=4; $j++) {
	echo "<div class=sectie>";
	echo "<h3>Copy Number $j</h3>";
	# set variables;
	$snps = array();
	$cnvi = array();
	$query = mysql_query("SELECT a.idproj as pid, s.chip_dnanr, s.id as sid, p.chiptype,p.chiptypeid, a.id, a.start, a.stop, a.chr, a.nrsnps FROM aberration a JOIN project p JOIN sample s  ON a.sample = s.id AND p.id = a.idproj WHERE p.chiptypeid > 1 AND a.nrsnps > 100 AND a.cn = $j AND a.chr < 23 ");
	echo "<p><table cellspacing=0>";
	echo "<tr><th $firstcell>Sample</th><th>regio</th><th>nrsnps</th><th>SNPs avg logR</th><th>CNVI avg logR</th><th>SNPs StDev</th><th>CNVI StDev</th></tr>";
	while ($row = mysql_fetch_array($query)) {
		$lsnps = array();
		$lcnvi = array();
		$aid = $row['id'];
		$sample = $row['chip_dnanr'];
		$chip = $row['chiptype'];
		$start = $row['start'];
		$stop = $row['stop'];
		$chr = $row['chr'];
		$sid = $row['sid'];
		$pid = $row['pid'];	
		$chipid = $row['chiptypeid'];
		$subquery = mysql_query("SELECT LRRSD, INIWAVE, callrate FROM projsamp WHERE idproj = $pid AND idsamp = $sid");
		$subrow = mysql_fetch_array($subquery);
		if ($subrow['LRRSD'] > 0.2 || $subrow['INIWAVE'] != '0' || $subrow['callrate'] < 0.994) {
			#echo "bad sample: ".$subrow['LRRSD'] . " - " . $subrow['INIWAVE'] . " - " . $subrow['callrate'] . "<br/>";;	
			# bad sample, continue
			continue;
		}
		$subquery = mysql_query("SELECT indications FROM BAFSEG WHERE idsamp = $sid AND idproj = $pid");
		$subnr = mysql_num_rows($subquery);
		if ($subnr > 0) {
			# mosaic sample 
			continue;
		}
		$nrprobes = $row['nrsnps'];
		$subquery = mysql_query("SELECT content FROM datapoints WHERE id = '$aid'");
		$subrow = mysql_fetch_array($subquery);
		$content = $subrow['content'];
		$data = explode("_",$content);
		$nrelements = count($data);
		#$sum = 0;
		#$nr = 0;
		for ($i=0;$i<$nrelements;$i+=3) {
			$pos = $data[$i];
			if ($pos < $start || $pos > $stop) {
				continue;
			}
			$logr = $data[$i+1];
			$sum = $sum + $logr;
			$nr++;
			$snpquery = mysql_query("SELECT zeroed FROM probelocations WHERE chromosome = '$chr' AND position = '$pos' AND chiptype = '$chipid'");
			$zres = mysql_fetch_array($snpquery);
			$zeroed = $zres['zeroed'];
			if ($zeroed == 1) {
				$cnvi[] = $logr;
				$lcnvi[] = $logr;
			}
			else {
				$snps[] = $logr;
				$lsnps[] = $logr;
			}
		}
		## get stdev
		$cnvistdev = number_format(standard_deviation($lcnvi),4,'.','');
		$snpsstdev = number_format(standard_deviation($lsnps),4,'.','');
		$cnvimean = number_format(mmmr($lcnvi),4,'.','');
		$snpsmean = number_format(mmmr($lsnps),4,'.','');
		echo "<tr><td $firstcell onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event))\" onmouseout=\"UnTip()\" NOWRAP>$sample ($chip)</td><td NOWRAP>Chr$chr:$start-$stop</td><td>$nrprobes</td><td>$snpsmean</td><td>$cnvimean</td><td>$snpsstdev</td><td>$cnvistdev</tr>";
		flush();
	}
	$cnvistdev = number_format(standard_deviation($cnvi),4,'.','');
	$snpsstdev = number_format(standard_deviation($snps),4,'.','');
	$cnvimean = number_format(mmmr($cnvi),4,'.','');
	$snpsmean = number_format(mmmr($snps),4,'.','');
	echo "<tr><th colspan = 3 $firstcell>SUMMARY</th><td>$snpsmean</td><td>$cnvimean</td><td>$snpsstdev</td><td>$cnvistdev</td></tr>";
	echo "</table>";
	echo "</p>";
	echo "</div>";
	flush();
}


?>
<div id="txtHint">something must appear here</div>
<!-- context menu loading -->
<script type="text/javascript" src="javascripts/details_context.js"></script>

