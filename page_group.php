<?php
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

if ($loggedin != 1) {
	echo "<div class=sectie><h3>Manage Usergroups</h3>\n";
	include('login.php');
	echo "</div>";
}
else {
	$type = $_GET['type'];
	if ($type == 'create') {
		include('inc_groups_create.inc');
	}
	elseif ($type == 'join') {
		include('inc_groups_join.inc');
	}
	elseif ($type == 'mine') {
		include('inc_groups_mine.inc');
	}
	elseif ($type == 'manage') {
		include('inc_groups_manage.inc');
	}
	else {
		echo "work in progress...<br>";
	}

}
?>
