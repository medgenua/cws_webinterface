<?php

if ($loggedin != 1) {
	include('login.php');
}
else {
	#################
	# CONNECT TO DB #
	#################
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");
	// VARIABLES FOR LAYOUT
	$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
	$readicon = array('<img src=images/content/mailnew.png>', '<img src=images/content/mailold.png>');
	$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
	# CHROM HASH
	for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}
	$chromhash["23"] = "X";
	$chromhash["24"] = "Y";


	//check for page settings?
	if (isset($_GET['part'])) {
		$part = $_GET['part'];
		if ($part < 1) {
			$part = 1;
		}
		$subpage = $part - 1;  
		$start = $subpage*20;
		$limit = "LIMIT $start,20";
	}
	else {
		$part = 1;
		$limit = '';
	}
	// process delete action
	if (isset($_GET['d'])) {
		mysql_query("DELETE FROM inbox WHERE id = '".$_GET['d']."'");
		// check if any unread remain
		$query = mysql_query("SELECT i.id FROM inbox i WHERE i.read = '0' AND i.to = '$userid'");
		$rows = mysql_num_rows($query);
		if ($rows > 0) {
			$_SESSION['newmessages'] = 1;
		}
		else {
			$_SESSION['newmessages'] = 0;
		}
		
	}
	// if a message is to be shown, mark as read
	if (isset($_GET['s'])) {
		mysql_query("UPDATE inbox SET inbox.read = '1' WHERE inbox.id = '".$_GET['s']."'");
		$query = mysql_query("SELECT inbox.id FROM inbox  WHERE inbox.read = '0' AND inbox.to = '$userid'");
		$rows = mysql_num_rows($query);
		if ($rows > 0) {
			$_SESSION['newmessages'] = 1;
		}
		else {
			$_SESSION['newmessages'] = 0;
		}
	}	
	// show messages
	echo "<div class=sectie>";
	echo "<h3>User Inbox</h3>\n";
	echo "<h4>Click to open messages</h4>";
	$query = mysql_query("SELECT inbox.id, inbox.subject, inbox.date, inbox.read, users.FirstName, users.LastName FROM inbox JOIN users ON inbox.from = users.id WHERE inbox.to = '$userid' ORDER BY inbox.date DESC $limit" );
	if ($part <= 1 ) {
		echo "<p><a href='index.php?page=inbox&part=2'>Next 20 messages</a></p>";
	}
	else {
		echo "<p><a href='index.php?page=inbox&part=". ($part - 1) ."'>Previous 20 messages</a> / <a href='index.php?page=inbox&part=".($part + 1)."'>Next 20 messages</a></p>";
	}
	if (mysql_num_rows($query) == 0) {
		echo "<p>No messages available.</p>";
	}
	else {
		echo "<table cellspacing=0>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt>From</th><th class=topcellalt>Subject</th><th class=topcellalt>Date</th><th class=topcellalt>&nbsp;</th></tr>";
		while ($row = mysql_fetch_array($query)) {
			$mid = $row['id'];
			$subject = $row['subject'];
			$date = $row['date'];
			$read = $row['read'];
			$readimg = $readicon[$read];
			$from = $row['FirstName'] . ' ' . $row['LastName'];
			$url = "index.php?page=inbox&amp;s=$mid&amp;part=$part";
			echo "<tr ><td $firstcell onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$readimg</td><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$from</td><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$subject</td><td onmouseover=\"this.style.cursor='pointer'\"  onClick=\"location.href='$url'\">$date</td><td><a href='index.php?page=inbox&d=$mid&part=$part&s=$show' class=img><img src='images/content/icon_trash.gif'></a></td></tr>";
		}
	}
	echo "</table></p>";
	echo "</div>";
	
	// show selected message content  
	if (isset($_GET['s']) && $_GET['s'] != '') {
		$mid = $_GET['s'];
		$query = mysql_query("SELECT i.subject, i.date, i.read, u.FirstName, u.LastName, i.body, i.type,i.values FROM inbox i JOIN users u ON i.from = u.id WHERE i.id = $mid" );
		$row = mysql_fetch_array($query);
		$subject = $row['subject'];
		$date = $row['date'];
		$from = $row['FirstName'] . ' ' . $row['LastName'];
		$body = $row['body'];
		$type = $row['type'];
		$values = $row['values'];
		echo "<div class=sectie>";
		echo "<h3>Message From $from</h3>";
		echo "<h4>Recieved: $date</h4>";
		echo "<p><span class=nadruk>Subject:</span> $subject</p>";
		if ($type == '') {
			echo "<p><span class=nadruk>Message:</span>";
			echo "<pre class=scrollbarbox style='height:250px;padding:5px'>$body</pre>";
			echo "</p></div>";		
		}
		elseif ($type == 'shared_project') {
			$yesno = array('Not Allowed','Allowed');
			$perm = mysql_query("SELECT pp.editcnv,pp.editclinic, pp.editsample,p.created, p.naam, p.chiptype, p.collection FROM projectpermission pp JOIN project p ON pp.projectid = p.id WHERE p.id = $values");
			$permrow = mysql_fetch_array($perm);
			$editcnv = $yesno[$permrow['editcnv']];
			$editclinic = $yesno[$permrow['editclinic']];
			$editsample = $yesno[$permrow['editsample']];
			$created = $permrow['created'];
			$pname = $permrow['naam'];
			$chip = $permrow['chiptype'];
			$collection = $permrow['collection'];
			$samq = mysql_query("SELECT idsamp FROM projsamp WHERE idproj = $values");
			$nrsamples = mysql_num_rows($samq);
			echo "<p>$from shared a project with you. Some details are listed below:</p>";
			echo "<table cellspacing=0>";
			echo "<tr><th $firstcell colspan=2>Project Details</th></tr>";
			echo "<tr><th class=specalt $firstcell>Project Name </th><td>$pname</td></tr>";
			echo "<tr><th class=specalt $firstcell>Creation Date</th><td>$created</td></tr>";
			echo "<tr><th class=specalt $firstcell>Collection</th><td>$collection</td></tr>";
			echo "<tr><th class=specalt $firstcell>Nr. Samples</th><td>$nrsamples</td></tr>";
			echo "<tr><th class=specalt $firstcell>Chiptype</th><td>$chip</td></tr>";
			echo "<tr><th $firstcell colspan=2>Permission Settings</th></tr>";
			echo "<tr><th class=specalt $firstcell>Edit CNVs</th><td>$editcnv</td></tr>";
			echo "<tr><th class=specalt $firstcell>Edit Clinic</th><td>$editclinic</td></tr>";
			echo "<tr><th class=specalt $firstcell>Project Control</th><td>$editsample</td></tr>";
			echo "</table></p>";
			echo "<p>Go To <a href=\"index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$values&amp;sample=\" target='_blank'>Sample Details </a> or <a href='index.php?page=overview&amp;p=$values' target='_blank'>Project Overview</a></p>";
			echo "</div>";
		}
		elseif ($type == 'shared_filter') {
			$yesno = array('Not Allowed','Allowed');
			$perm = mysql_query("SELECT fu.editing, fu.sharing , f.Name, f.Comment FROM `Filters_x_Userpermissions` fu JOIN `Filters` f ON fu.fid = f.fid WHERE f.fid = $values AND fu.uid = '$userid'");
			$permrow = mysql_fetch_array($perm);
			$editing = $yesno[$permrow['editing']];
			$sharing = $yesno[$permrow['sharing']];
			$fname = stripslashes($permrow['Name']);
			$comment = stripslashes($permrow['Comment']);
			echo "<p>$from shared a filter with you. Some details are listed below:</p>";
			echo "<p><span class=nadruk>Filter Name:</span> $fname<br/>";
			echo "   <span class=nadruk>Information:</span> $comment<br/>";
			echo "   <span class=nadruk>Editing:</span> $editing<br/>";
			echo "   <span class=nadruk>Sharing:</span> $sharing</p>";
			echo "<p><a href=\"index.php?page=filters&amp;type=activate\" >Activate the filter</a></p>";
			echo "</div>";
		}
		elseif ($type == 'cnvannotation') {
			$logid = $values;
			$logq = mysql_query("SELECT aid, uid, entry, arguments FROM log WHERE id = '$logid'");
			$setby = '';
			$prevclass = '';
			$newclass = '';
			
			$logrow = mysql_fetch_array($logq);
			$aid = $logrow['aid'];
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
			$arguments = $logrow['arguments'];
			preg_match('/Diagnostic\sClass\sChanged\sFrom\s(\d)\sto\s(.+)$/',$logrow['entry'],$matches);
			$prevclass = $matches[1];
			$newclass = $matches[2];
			
			
			if ($setby == '' || $prevclass == '' || $newclass == '') {
				echo "<p>The requested information could not be found...(setby $setby : logentry : ".$logrow['entry']."</p>";
			}
			else {
				$cnvq = mysql_query("SELECT a.idproj, a.sample, a.chr, a.start, a.stop, a.cn, a.inheritance, s.chip_dnanr, p.chiptype FROM aberration a JOIN sample s JOIN project p ON a.sample = s.id AND p.id = a.idproj WHERE a.id = '$aid'");
				$cnvrow = mysql_fetch_array($cnvq);
				$chrtxt = $chromhash[$cnvrow['chr']];
				$start = $cnvrow['start'];
				$stop = $cnvrow['stop'];
				$region = "Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
				$sample = $cnvrow['chip_dnanr'];
				$sid = $cnvrow['sample'];
				$pid = $cnvrow['idproj'];
				$chip = $cnvrow['chiptype'];
				$cn = $cnvrow['cn'];
				$inheritance = $inh[$inh[$cnvrow['inheritance']]];
				echo "<p>$from changed the diagnostic class of the CNV below from $prevclass (as set by you) to $newclass.</p>";
				echo "<table cellspacing=0>";
				echo "<tr><th $firstcell colspan=2>CNV Details</th></tr>";
				echo "<tr><th class=specalt $firstcell>Region</th><td>$region</td></tr>";
				echo "<tr><th class=specalt $firstcell>CopyNumber</th><td>$cn</td></tr>";
				echo "<tr><th class=specalt $firstcell>Sample</th><td>$sample</td></tr>";
				echo "<tr><th class=specalt $firstcell>Chiptype</th><td>$chip</td></tr>";
				echo "<tr><th class=specalt $firstcell>Inheritance</th><td>$inheritance</td></tr>";
				echo "<tr><th $firstcell colspan=2>Annotation Details</th></tr>";
				echo "<tr><th class=specalt $firstcell>Previous Classification</th><td>$prevclass</td></tr>";
				echo "<tr><th class=specalt $firstcell>Current Classification</th><td>$newclass</td></tr>";
				echo "<tr><th class=specalt $firstcell>Reason for change</th><td>$arguments</td></tr>";
				echo "</table></p>";
				echo "<p>Go To <a href=\"index.php?page=details&amp;project=$pid&amp;sample=$sid\" target='_blank'>Sample Details </a> or <a href='index.php?page=results&amp;type=region&amp;region=$region' target='_blank'>Region Overview</a></p>";
			echo "</div>";

			}

		}
	}







}
?>
