<?php
#################
## LOGIN CHECK ##
#################
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

##########################
## CHECK IF CUSTOM DATA ##
##########################
if (isset($_GET['cstm'])) {
	$cstm = $_GET['cstm']; 
}
else {
	$cstm = 0;
}
if (isset($_GET['loh'])) {
	$loh = $_GET['loh'];
}
else {
	$loh = 0;
}
####################################
## check if sample or gene based. ##
####################################
if (isset($_GET['source'])) {
	$source = $_GET['source'];
}
else {
	$source = 's';
}
if ($source != 's' && $source != 'g') {
	$source = 's';
}

##########################################################################################
## if showing gene associated literature, use this block, otherwise, use rest of script ##
##########################################################################################
if ($source == 'g') {
	$id = $_GET['gid']; # id from genesum table !
	$query = mysql_query("SELECT symbol, chr, start, stop, pubmed FROM genesum WHERE geneID = $id");
	$row = mysql_fetch_array($query);
	$symbol = $row['symbol'];
	$chr = $row['chr'];
	$chrtxt = $chromhash[$chr];
	$start = $row['start'];
	$stop = $row['stop'];
	$pubmed = $row['pubmed'];
	$region = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
	echo "<div class=sectie>";
	echo "<h3>Publications Associated to $symbol on $region</h3>";
	if ($pubmed == '') {
		echo "<p>There are no publications linked to this CNV</p>";
	}
	else {
		$first = 1;
		require_once 'xmlLib2.php';
		$link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=$pubmed&retmode=xml&rettype=abstract";
		system("wget -O '/tmp/output.fetching' '$link'");
		$abArray =  my_xml2array('/tmp/output.fetching');
		unlink("/tmp/output.fetching");
		$abArray = get_value_by_path($abArray, 'PubmedArticleSet');
		echo "<p ><table cellspacing=0>";
		$pmidarray = explode(",",$pubmed);
		
		foreach ($abArray as $key => $value) {
			if (!is_numeric($key)) {
				# skip name tags etc
				continue;
			}
			$subarray = array('name' => 'PubmedArticleSet','0' => $abArray[$key]);
			$title = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/ArticleTitle');
			$title = $title['value'];
			$pmid = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/PMID');
			$pmid = $pmid['value'];
			$date = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/PubDate');
			$volume = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/Volume');
			$volume = $volume['value'];
			$issue = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/Issue');
			$issue = $issue['value'];
			$pages = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Pagination/MedlinePgn');
			$pages = $pages['value'];
			$datestring = $date['Year'] .' ' . $date['Month'] . ";$volume($issue):$pages ";
			$journal = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/Title');
			$journal = $journal['value'];
			$abstract = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Abstract');
			$abstr = '';
			for ($i = 0; $i < count($abstract); $i++) {
				if ($abstract[$i]['attributes']['Label'] != '') {
					$abstr .= $abstract[$i]['attributes']['Label'] . ': ';
				}
				$abstr .= $abstract[$i]['value'].'<br/>';
			}
			$abstract = $abstr;
			$abstract = str_ireplace($search,$replace,$abstract);
			$authors = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/AuthorList');
			$language = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Language');
			$language = $language['value'];
			$substances = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/ChemicalList/Chemical');
			echo "<tr><th $firstcell>$title</th></tr>";
			echo "<tr><td $firstcell><span class=italic>Authors:</span> ";
			$astring = '';
			foreach ($authors as $akey => $aarray) {
				if (!is_numeric($akey)) {
					continue;
				}
				$astring .= "";
				$lname = get_value_by_path($aarray,'LastName');
				$lname = $lname['value'];
				$ini = get_value_by_path($aarray,'Initials');
				$ini = $ini['value'];
				$astring .= $lname ." " .$ini . ', ';
			} 
			$astring = substr($astring,0,-2);
			echo "$astring </td></tr>";
			echo "<tr><td $firstcell><span class=italic>Journal: </span>$journal $datestring";
			echo "<span style='float:right'><a href='http://www.ncbi.nlm.nih.gov/pubmed/$pmid' target='_blank'>To Pubmed</a>";
			echo "</span>";
			echo "</td></tr>";
			if ($language != 'eng') {
				echo "<tr><td $firstcell><span class=italic>Language:</span> $language</td></tr>";
			}
		
			echo "<tr><td $firstcell>$abstract</td></tr>";
			flush();
		}
		echo "</table>";
	}
	ob_flush;

	echo "</div>";


	exit;
}
## end of gene-associated literature



########################
## PROCESS ADDED PMID ##
########################
if (isset($_POST['addsubmit']) ){
	$pmid = $_POST['pmid'];
	$addok = 1;

	if (!is_numeric($pmid)) {
		echo "<div class=sectie><h3>Provided PMID is not numeric</h3><p>The provided PMID was not valid and thus not assocatiated to the CNV</p></div>";
		$addok = 0;
	}
	$aid = $_GET['aid'];
	mysql_select_db($_SESSION['dbname']);
	if ($cstm == 1) {
		$table = 'cus_aberration';
		$log = 'cus_log';
	}
	elseif ($loh == 1) {
		$table = 'aberration_LOH';
		$log = 'log_loh';
	}
	else {
		$table = 'aberration';
		$log = 'log';
	}
	$query = mysql_query("SELECT pubmed FROM $table WHERE id = $aid");
	$row = mysql_fetch_array($query);
	$pubmed = $row['pubmed'];
	if ($pubmed == '') {
		$pubmed = $pmid;
	}
	else {
		$pmids = explode(',',$pubmed);
		foreach($pmids as $key => $item) {
			if ($item == $pmid) {
				echo "	<div class=sectie><h3>Provided PMID Already stored</h3><p>The provided PMID was assocatiated to this CNV previously</p></div>";
				$addok = 0;
				break;
			}
		}
		$pubmed .= ",$pmid";
	}
	if ($addok == 1) {
		$query = mysql_query("UPDATE $table SET pubmed = '$pubmed' WHERE id = $aid");
		$query = mysql_query("SELECT sample, idproj FROM $table WHERE id = $aid");
		$row = mysql_fetch_array($query);
		$sid = $row['sample'];
		$pid = $row['idproj'];
		$entry = "PMID $pmid Linked to CNV"; 
		$uid = $_SESSION['userID'];
		$query = mysql_query("INSERT INTO $log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid', '$uid', '$entry')");
	}

}
##########################
## ADD PUBLICATION FORM ##
##########################
if ($_GET['type'] == 'add') {
	$aid = $_GET['aid'];
	mysql_select_db($_SESSION['dbname']);
	if ($cstm == 1) {
		$table = 'cus_aberration';
		$ptable = 'cus_project';
		$stable = 'cus_sample';
		$dpage = 'cusdetails';
		$fields = ',cn';
	}
	elseif ($loh == 1) {
		$table = 'aberration_LOH';
		$ptable = 'project';
		$stable = 'sample';
		$dpage = 'details';
		$fields = '';
	}
	else {
		$table = 'aberration';
		$ptable = 'project';
		$stable = 'sample';
		$dpage = 'details';
		$fields = ',cn';
	}
	$query = mysql_query("SELECT chr, start, stop,pubmed, sample, idproj $fields FROM $table WHERE id = $aid");
	$row = mysql_fetch_array($query);
	$chr = $row['chr'];
	$chrtxt = $chromhash[$chr];
	$start = $row['start'];
	$stop = $row['stop'];
	$sid = $row['sample'];
	$pid = $row['idproj'];
	if ($loh != 1) {
		$cn = $row['cn'];
	}
	else {
		$cn = 'LOH';
	}
	$pubmed = $row['pubmed'];
	$query = mysql_query("SELECT naam FROM $ptable WHERE id = $pid");
	$row = mysql_fetch_array($query);
	$pname = $row['naam'];
	if ($cstm != 1) {
		$query = mysql_query("SELECT chip_dnanr, gender FROM $stable where id = '$sid'");
		$row = mysql_fetch_array($query);
		$sample = $row['chip_dnanr'];
		$gender = $row['gender'];
	}
	else {
		$query = mysql_query("SELECT gender FROM $stable WHERE idsamp = '$sid' AND idproj = '$pid'");
		$row = mysql_fetch_array($query);
		$sample = $sid;
		$gender = $row['gender'];
	}
	$region = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
	echo "<div class=sectie>";
	echo "<h3>Publications Associated to $region</h3>";
	echo "<p>The CNV was detected in $sample ($gender) with CopyNumber $cn. '$sample' is taken from project '$pname'.</p>";
	
	#echo" <p><a href='index.php?page=$dpage&sample=$sid&project=$pid'>Back To Sample Details</a></p>";
	## input form
	echo "<p>Provide the PubMed ID of the publication to associate to the CNV below. The PMID is the unique identifier shown usually below the abstract on the PubMed site. It is also the numeric part of the URL when browsing an abstract. As an example, the PMID for <a href='http://www.ncbi.nlm.nih.gov/pubmed/19249392' target='_blank'>this publication</a> is 19249392.</p>";
	echo "<p>";
	echo "<form action='index.php?page=literature&type=add&aid=$aid&cstm=$cstm&loh=$loh' method=POST>";
	echo "Pubmed ID: <input type=text name=pmid size=30> &nbsp; &nbsp; <input type=submit class=button name=addsubmit value='Add Publication'></form></p></div>";
}

##########################
## REMOVE A PUBLICATION ##
##########################
if (isset($_GET['dpmid'])) {
	$aid = $_GET['aid'];
	mysql_select_db($_SESSION['dbname']);
	if ($cstm == 1) {
		$table = 'cus_aberration';
		$log = 'cus_log';
	}
	elseif($loh == 1) {
		$table = 'aberration_LOH';
		$log = 'log_loh';
	}
	else {
		$table = 'aberration';
		$log = 'log';
	}

	$query = mysql_query("SELECT pubmed, sample, idproj FROM $table WHERE id = $aid");	
	$row = mysql_fetch_array($query);
	$pmidstring = $row['pubmed'];
	$sid = $row['sample'];
	$pid = $row['idproj'];
	$pmids = explode(',',$pmidstring);
	#echo "string : $pmidstring<br/>";
	$newstring = '';
	foreach ($pmids as $key => $pmid) {
		#echo "pmid: $key => $pmid<br/>";
		if ($pmid != $_GET['dpmid']) {
			$newstring .= $pmid .',';
		}
	}
	if ($newstring != '') {
		$newstring = substr($newstring,0,-1);
	}
	$entry = "Association of PMID ". $_GET['dpmid']." Removed"; 
	$uid = $_SESSION['userID'];
	$query = mysql_query("INSERT INTO $log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid', '$uid', '$entry')");
	$query = mysql_query("UPDATE $table SET pubmed = '$newstring' WHERE id = $aid");
	#echo "UPDATE $table SET pubmed = '$newstring' WHERE id = $aid<br/>";
}

##############################################################
## Currently assocatied articles? EQUAL FOR ADD & READ type ##
##############################################################
if ($_GET['type'] == 'add' || $_GET['type'] == 'read') {
	if ($_GET['type'] == 'add') {
		echo "<div class=sectie>";
		echo "<h3>Currently Associated Publications</h3>";
	}
	elseif ($_GET['type'] == 'read') {
		$aid = $_GET['aid'];
		mysql_select_db($_SESSION['dbname']);
		if ($cstm == 1) {
			$table = 'cus_aberration';
			$permtable = 'cus_projectpermission';
			$ptable = 'cus_project';
			$dpage = 'cusdetails';
			$fields = ',cn';
		}
		elseif ($loh == 1) {
			$table = 'aberration_LOH';
			$permtable = 'projectpermission';
			$ptable = 'project';
			$dpage = 'details';
			$fields = '';
		}
		else {
			$table = 'aberration';
			$permtable = 'projectpermission';
			$ptable = 'project';
			$dpage = 'details';
			$fields = ',cn';
		}
		
		$query = mysql_query("SELECT chr, start, stop, pubmed, sample, idproj $fields FROM $table WHERE id = $aid");
		$row = mysql_fetch_array($query);
		$chr = $row['chr'];
		$chrtxt = $chromhash[$chr];
		$start = $row['start'];
		$stop = $row['stop'];
		$sid = $row['sample'];
		$pid = $row['idproj'];
		if ($loh != 1) {
			$cn = $row['cn'];
		}
		else {
			$cn = 'LOH';
		}
		$pubmed = $row['pubmed'];
		$uid = $_SESSION['userID'];
		$query = mysql_query("SELECT editcnv FROM $permtable WHERE projectid = '$pid' AND userid = '$uid'");
		$row = mysql_fetch_array($query);
		$editcnv = $row['editcnv'];
		$query = mysql_query("SELECT naam FROM $ptable WHERE id = $pid");
		$row = mysql_fetch_array($query);
		$pname = $row['naam'];
		if ($cstm == 1) {
			$query = mysql_query("SELECT gender FROM cus_sample where id = $sid");
			$row = mysql_fetch_array($query);
			$sample = $sid;
			$gender = $row['gender'];
		}
		else {
			$query = mysql_query("SELECT chip_dnanr, gender FROM sample where id = $sid");
			$row = mysql_fetch_array($query);
			$sample = $row['chip_dnanr'];
			$gender = $row['gender'];
		}
		$region = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
		echo "<div class=sectie>";
		echo "<h3>Publications Associated to $region</h3>";
		echo "<p class=bold>Region Details:";
		echo "<ul><li> &nbsp; - Sample:  $sample ($gender) </li><li> &nbsp; - CopyNumber: $cn </li><li> &nbsp; - Project: $pname</li></ul></p>";
		#echo" <p><a href='index.php?page=$dpage&sample=$sid&project=$pid'>Back To Sample Details</a></p>";

	}
	if ($pubmed == '') {
		echo "<p>There are no publications linked to this CNV</p>";
	}
	else {
		$first = 1;
		require_once 'xmlLib2.php';
		$link = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=$pubmed&retmode=xml&rettype=abstract";
		
		system("wget -O '/tmp/output.fetching' '$link'");
		flush();
		$abArray =  my_xml2array('/tmp/output.fetching');
		unlink("/tmp/output.fetching");
		$abArray = get_value_by_path($abArray, 'PubmedArticleSet');
		echo "<p><table cellspacing=0>";
		$pmidarray = explode(",",$pubmed);
		
		foreach ($abArray as $key => $value) {
			if (!is_numeric($key)) {
				# skip name tags etc
				continue;
			}
			$subarray = array('name' => 'PubmedArticleSet','0' => $abArray[$key]);
			$title = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/ArticleTitle');
			$title = $title['value'];
			#$title = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['ArticleTitle'];
			#$title = str_ireplace($search,$replace,$title);
			$pmid = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/PMID');
			$pmid = $pmid['value'];
			#$pmid = $pmidarray[$key];
			#$date = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['PubDate'][0];
			$date = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/PubDate');
			#print_r($date);
			#$volume = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['Volume'];
			$volume = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/Volume');
			$volume = $volume['value'];
			#$issue = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['JournalIssue'][0]['Issue'];
			$issue = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/JournalIssue/Issue');
			$issue = $issue['value'];
			#$pages = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Pagination'][0]['MedlinePgn'];
			$pages = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Pagination/MedlinePgn');
			$pages = $pages['value'];
			$datestring = $date['Year'] .' ' . $date['Month'] . ";$volume($issue):$pages ";
			#$journal = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Journal'][0]['Title'];
			$journal = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Journal/Title');
			$journal = $journal['value'];
			## abstract can be array of subsections (if more than 2 items in array)
			#$abstract = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Abstract'][0]['AbstractText'];
			$abstract = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Abstract');
			$abstr = '';
			#if (count($abstract) > 2) {
				#echo "here!<br/>";
				for ($i = 0; $i < count($abstract); $i++) {
					#echo $abstract[$i]['value'].'<br/>';
					if ($abstract[$i]['attributes']['Label'] != '') {
						$abstr .= $abstract[$i]['attributes']['Label'] . ': ';
					}
					$abstr .= $abstract[$i]['value'].'<br/>';
				}
				$abstract = $abstr;
			#}
			#else {
			#	$abstract = $abstract[0]['value'];
			#}
			$abstract = str_ireplace($search,$replace,$abstract);
			#$authors = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['AuthorList'][0]['Author'];
			$authors = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/AuthorList');
			#$language = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['Article'][0]['Language'];
			$language = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/Article/Language');
			$language = $language['value'];
			#$substances = $abArray['PubmedArticleSet']['PubmedArticle'][$key]['MedlineCitation'][0]['ChemicalList'][0]['Chemical'];
			$substances = get_value_by_path($subarray,'PubmedArticle/MedlineCitation/ChemicalList/Chemical');
			echo "<tr><th $firstcell>$title</th></tr>";
			echo "<tr><td $firstcell><span class=italic>Authors:</span> ";
			$astring = '';
			foreach ($authors as $akey => $aarray) {
				if (!is_numeric($akey)) {
					continue;
				}
				$astring .= "";
				$lname = get_value_by_path($aarray,'LastName');
				$lname = $lname['value'];
				$ini = get_value_by_path($aarray,'Initials');
				$ini = $ini['value'];
				$astring .= $lname ." " .$ini . ', ';
			} 
			$astring = substr($astring,0,-2);
			echo "$astring </td></tr>";
			echo "<tr><td $firstcell><span class=italic>Journal: </span>$journal $datestring";
			echo "<span style='float:right'><a href='http://www.ncbi.nlm.nih.gov/pubmed/$pmid' target='_blank'>To Pubmed</a>";
			if ($editcnv == 1) {
				echo " | <a href='index.php?page=literature&type=read&aid=$aid&dpmid=$pmid&cstm=$cstm'>Remove</a>";
			}
			echo "</span>";
			echo "</td></tr>";
			if ($language != 'eng') {
				echo "<tr><td $firstcell><span class=italic>Language:</span> $language</td></tr>";
			}
		
			echo "<tr><td $firstcell>$abstract</td></tr>";
			flush();
		}
		echo "</table>";


	}
	echo "</div>";
}



########################
## END OF LOGIN CHECK ##
########################
}
?>
