<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");
ob_end_flush();

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

if (!isset($_POST['loaddata'])) {
	// print the form to upload the datafile
	echo "<div class=sectie>";
	echo "<h3>Load CNV-WebStore Data Dump</h3>";
	echo "There are two ways to upload data previously dumped in MySQL format into this CNV-WebStore Server. You can either provide a link containing the downloadkey given you you after dumping on the source server, or you can provide the downloaded file to upload it yourself. Please not that files >2Gb are not supported !</p>";
	echo "<form enctype='multipart/form-data' action='index.php?page=loaddump' method=POST>";
	echo "<p><span class=italic>Option 1: </span> Provide downloadlink: <input type=text size=40  name='keylink'><br/>";
	echo "<span class=italic>Option 2: </span> Provide local file: <input type=file size=40 name='localfile'></p>";
	echo "<p><input type=submit class=button name=loaddata value='Submit'></form></p>";
	echo "</div>";
	exit;
}

////////////////////////
// Process sumbission //
////////////////////////
$step = 0;
// option 1 : provided link with key
if (isset($_POST['keylink'])) {
	$keylink = $_POST['keylink'];
	$content = file_get_contents($keylink); 
	if ($content !== false) {
	   	// do something with the content
		$content = rtrim($content);
		if ($content == 'INVALID KEY') {
			echo "<div class=sectie>";
			echo "<h3>Specified KEY not valid </h3>";
			echo "<p>The specified Key was not recognised by the server This either means it was badly copied, or that the created dump was deleted (this is done after two weeks).</p>";
			echo "</div>";
			exit;	   
		}
		if ($content == 'ARCHIVE NOT FOUND') {
			echo "<div class=sectie>";
			echo "<h3>Archive not found</h3>";
			echo "<p>The specified key was recognised by the server, but the corresponding project dump was not found. This means that the created dump was deleted (this is done after two weeks).</p>";
			echo "</div>";
			exit;	   
		}
		// got here, archive is found, we now have the archive name.
		$file = "data/$content.tar.gz";
		$url = preg_replace('/keycheck\.php.*/',$file,$keylink);
		echo "<div class=sectie>";
		echo "<h3>Dowloading Archive</h3>";
		echo "<p>Progress:<br/>";
		$step++;
		echo "<span class=bold>step $step: </span> ";
		echo "<span>The file is being downloaded to the server. This might take a while, depending on the size...</span>";
		flush();
		system("mkdir /tmp/$content.webstore && wget '$url' -O '/tmp/$content.webstore/$content.tar.gz'");
		echo "done<br/>";
		flush();
	} else {
		// url was not fetched.
		echo "<div class=sectie>";
		echo "<h3>Specified URL not valid </h3>";
		echo "<p>The specified URL was not reachable.</p>";
		echo "</div>";
		exit;	   
	}
}
//option 2 : todo


// Next, extract.
$step++;
echo "<span class=bold>step $step: </span><span>Extracting Archive...</span>";
flush();
system("cd /tmp/$content.webstore/ && tar xzf $content.tar.gz");
echo "done<br/>";
flush();
///////////////
// load data //
///////////////
echo "<span class=bold>step $step: </span><span>Loading Data...</span>";
echo "<ol style='padding-left:20px'>";
// get existing collections
echo "<li>Fetching existing collections</li>";
flush();
$collections = array();
$query = mysql_query("SELECT name FROM collections");
$newcollections = array();
while ($row = mysql_fetch_array($query)) {
	$ccol = $row['name'];
	$collections[$ccol] = 1;
}
// get existing chiptypes
echo "<li>Fetching existing chiptypes</li>";
flush();
$chiptypes = array();
$query = mysql_query("SELECT ID, name FROM chiptypes");
while ($row = mysql_fetch_array($query)) {
	$cname = $row['name'];
	$cid = $row['ID'];
	$chiptypes[$cname] = $cid;
}
// projects
echo "<li>Storing Projects</li>";
flush();
$fh = fopen("/tmp/$content.webstore/project.txt", 'r');
$projects = array();
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$pid = $pieces[0];
	$name = $pieces[1];
	$col = $pieces[4];
	$chip = $pieces[2];
	$created = $pieces[3];
	$isdef = $pieces[7];
	if (!array_key_exists($col, $collections)) {
		// unexisting collection, create it
		$query = mysql_query("INSERT INTO collections (name, comment) VALUES ('$col', '')");
		$colid = mysql_insert_id();
		$newcollections[] = $colid;
	}
	if (!array_key_exists($chip, $chiptypes)) {
		// unexisting chiptype, create it
		$query = mysql_query("INSERT INTO chiptypes (name) VALUES ('$chip')");
		$chipid = mysql_insert_id();
		$newchips[] = $chipid;
	}
	else {
		$chipid = $chiptypes[$chip];
	}
	$query = mysql_query("INSERT INTO project (naam, chiptype, created, collection, userID, chiptypeid, isdefault) VALUES ('$name', '$chip', '$created', '$col', '$userid', '$chipid', '$isdef')");
	$newpid = mysql_insert_id();
	$query = mysql_query("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic, editsample) VALUES ('$newpid', '$userid', 1,1,1)");
	$projects[$pid] = $newpid;
}
fclose($fh);
echo "<li>Storing Samples</li>";
flush();
$fh = fopen("/tmp/$content.webstore/sample.txt", 'r');
$samples = array();
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$sid = $pieces[0];
	$name = $pieces[1];
	$gender = $pieces[2];
	$intrack = $pieces[3];
	$tfp = $pieces[4];
	// check if tfp exists
	if (array_key_exists($tfp, $projects)) {
		$tfp = $projects[$tfp];
	}
	else {
		$tfp = 0;
		$intrack = 0;
	}
	// check if sample exists
	$query = mysql_query("SELECT id FROM sample WHERE chip_dnanr = '$name'");
	if (mysql_num_rows($query) == 1) {
		$row = mysql_fetch_array($query);
		$samples[$sid] = $row['id'];
		//this sample does not have to inserted. but notify user.
		$existingsamples[$row['id']] = $line;
	}
	else {
		// new sample, insert
		$query = mysql_query("INSERT INTO sample (chip_dnanr, gender, intrack, trackfromproject) VALUES ('$name', '$gender', '$intrack', '$tfp')");
		$newsid = mysql_insert_id();
		$samples[$sid] = $newsid;
	}	
}
fclose($fh);
// Clinical Info 
// freetext
$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
echo "<li>Storing Clinical Information</li>";
flush();
if (file_exists("/tmp/$content.webstore/clinic.xml")) {
  $xml_data = file_get_contents("/tmp/$content.webstore/clinic.xml");
  $abArray = explode("\n",$xml_data);
  foreach ($abArray as $key => $line) {
	if (preg_match('/<sample>/',$line)) {
		$sample = 1;
		$sid = '';
		$clinic = '';
	}
	elseif (preg_match('/<sid>/',$line)) {
		$sid = preg_replace('/<sid>(\d+)<\/sid>/','$1',$line);
	}
	elseif (preg_match('/<clinic>/',$line)) {
		$line = str_replace('<clinic>','',$line);
		$clinic .= $line;
	}
	elseif (preg_match('/<\/clinic>/',$line)) {
		$line = str_replace('</clinic>','',$line);
		$clinic .= $line;
	}
	elseif (preg_match('/<\/sample>/',$line)) {
		$sample = 0;
		// INSERT sample
		$query = mysql_query("UPDATE sample set clinical = ENCODE('".addslashes($clinic)."','$encpass') WHERE id = ".$samples[$sid]);
	}
	elseif ($sample == 1) {
		$clinic .= $line;
	}
  }	
}
// structured
flush();
$fh = fopen("/tmp/$content.webstore/sample_phenotypes.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$sid = $pieces[0];
	$code = $pieces[1];
	$newsid = $samples[$sid];
	$query = mysql_query("INSERT INTO sample_phenotypes (sid, lddbcode) VALUES ('$newsid', '$code')");
}
fclose($fh);

// project_to_sample links
echo "<li>Linking samples to projects</li>";
flush();
$fh = fopen("/tmp/$content.webstore/projsamp.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);	
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$sid = $pieces[0];
	$pid = $pieces[1];
	$callrate = $pieces[2];
	$idx = $pieces[3];
	$lrrsd = $pieces[4];
	$bafsd = $pieces[5];
	$wave = $pieces[6];
	$calc = $pieces[7];
	$iniwave = $pieces[8];	
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	$query = mysql_query("INSERT INTO projsamp (idsamp, idproj, callrate, idx, LRRSD, BAFSD, WAVE, calc, INIWAVE) VALUES ('$newsid', '$newpid', '$callrate', '$idx', '$lrrsd', '$bafsd', '$wave', '$calc', '$iniwave')");
}
fclose($fh);

// Aberrations Table
echo "<li>Storing CNV information</li>";
flush();
$fh = fopen("/tmp/$content.webstore/aberration.txt", 'r');
$aids = array();
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$aid = $pieces[0];
	$cn = $pieces[1];
	$start = $pieces[2];
	$stop = $pieces[3];
	$chr = $pieces[4];
	$largestart = $pieces[5];
	$largestop = $pieces[6];
	$sid = $pieces[7];
	$pid = $pieces[8];	
	$inh = $pieces[9];
	$class= $pieces[10];
	$sb = $pieces[11];
	$snp = $pieces[12];
	$sone = $pieces[13];
	$stwo = $pieces[14];
	$sthree = $pieces[15];
	$size = $pieces[16];
	$sfour = $pieces[17];
	$genes = $pieces[18];
	$logr = $pieces[19];
	$pubmed = $pieces[20];
	$lift = $piece[21];
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	if ($lift == '\N') {
		$query = mysql_query("INSERT INTO aberration (cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, seenby, nrsnps, seeninchip, seenincontrols, schipacoll, size, seenall, nrgenes, avgLogR, pubmed) VALUES ('$cn', '$start', '$stop', '$chr', '$largestart', '$largestop', '$newsid', '$newpid', '$inh', '$class', '$sb', '$snp', '$sone', '$stwo', '$sthree', '$size', '$sfour', '$genes', '$logr', '$pubmed')");
	}
	else {
		$query = mysql_query("INSERT INTO aberration (cn, start, stop, chr, largestart, largestop, sample, idproj, inheritance, class, seenby, nrsnps, seeninchip, seenincontrols, schipacoll, size, seenall, nrgenes, avgLogR, pubmed, LiftedFrom) VALUES ('$cn', '$start', '$stop', '$chr', '$largestart', '$largestop', '$newsid', '$newpid', '$inh', '$class', '$sb', '$snp', '$sone', '$stwo', '$sthree', '$size', '$sfour', '$genes', '$logr', '$pubmed', '$lift')");
	}
	$aids[$aid] = mysql_insert_id();
}
fclose($fh);


// deletedcnvs
echo "<li>Storing previously deleted cnvs</li>";
flush();
$fh = fopen("/tmp/$content.webstore/deletedcnvs.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$aid = $pieces[0];
	$cn = $pieces[1];
	$start = $pieces[2];
	$stop = $pieces[3];
	$chr = $pieces[4];
	$sid = $pieces[5];
	$pid = $pieces[6];
	$inh = $pieces[7];
	$class= $pieces[8];
	$sb = $pieces[9];
	$lift = $pieces[10];
	$final = $pieces[11];
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	// first insert into aberrations for a new aid
	$query = mysql_query("INSERT INTO aberration (cn, start, stop, chr, sample, idproj, inheritance, class, seenby) VALUES ('$cn', '$start', '$stop', '$chr', '$newsid', '$newpid', '$inh', '$class')");
	$newaid = mysql_insert_id();
	$aids[$aid] = $newaid;
	if ($lift == '\N' || $lift == '') {
		$lift1 = '';
		$lift2 = '';
	}
	else {
		$lift1 = ', LiftedFrom';
		$lift2 = ",'$lift'";
	}
	if ($final == '\N' || $final == '') {
		$final1 = '';
		$final2 = '';
	}
	else {
		$final1 = ', FinalBuild';
		$final2 = ",'$final'";
	}
	$query = mysql_query("INSERT INTO deletedcnvs (id, cn, start, stop, chr, sample, idproj, inheritance, class, seenby $lift1 $final1) VALUES ('$newaid', '$cn', '$start', '$stop', '$chr','$newsid', '$newpid', '$inh', '$class', '$sb' $lift2 $final2)");
}
fclose($fh);

// DATAPOINTS
echo "<li>Storing probe level data</li>";
flush();
$fh = fopen("/tmp/$content.webstore/aberration.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$aid = $pieces[0];
	$content = $pieces[1];
	$lift = $pieces[2];
	$newaid = $aids[$aid];
	if ($lift == '\N' || $lift == '') {
		$query = mysql_query("INSERT INTO datapoints (id, content) VALUES ('$newaid', '$content')");
	}
	else {
		$query = mysql_query("INSERT INTO datapoints (id, content, LiftedFrom) VALUES ('$newaid', '$content', '$lift')");
	}	
}
fclose($fh);

// bafseg
echo "<li>Storing Baf Segmentation results</li>";
flush();
$fh = fopen("/tmp/$content.webstore/bafseg.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode('@@@',$line);
	$sid = $pieces[0];
	$pid = $pieces[1];
	$resultlist = $pieces[2];
	$indications = $pieces[3];
	$file = $pieces[4];
	$lift = $pieces[5];
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	if ($lift == '\N' || $lift == '') {
		$query = mysql_query("INSERT INTO bafseg (idsamp, idproj, resultlist, indications, filename) VALUES ('$newsid', '$newpid', '$resultlist', '$indications', '$file')");
	}
	else {
		$query = mysql_query("INSERT INTO bafseg (idsamp, idproj, resultlist, indications, filename, LiftedFrom) VALUES ('$newsid', '$newpid', '$resultlist', '$indications', '$file', '$lift')");
	}	
}
fclose($fh);


// log
echo "<li>Storing log file</li>";
flush();
$fh = fopen("/tmp/$content.webstore/log.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$time = $pieces[1];
	$sid = $pieces[2];
	$pid = $pieces[3];
	$aid = $pieces[4];
	// reset all log entries to current user
	$uid = $userid;
	$entry = $pieces[6];
	$arguments = $pieces[7];
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	$newaid = $aids[$aid];
	$query = mysql_query("INSERT INTO log (time, sid, pid, aid, uid, entry, arguments) VALUES ('$time', '$newsid', '$newpid', '$newaid', '$uid', '$entry', '$arguments')");
}
fclose($fh);


// plots
echo "<li>Storing Whole Genome Plot information</li>";
flush();
$fh = fopen("/tmp/$content.webstore/plots.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$sid = $pieces[0];
	$pid = $pieces[1];
	$file = $pieces[2];
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	$query = mysql_query("INSERT INTO plots (idsamp, idproj, filename) VALUES ('$newsid', '$newpid', '$file')");
}
fclose($fh);


// prioritize
echo "<li>Storing Prioritisation results</li>";
flush();
$fh = fopen("/tmp/$content.webstore/prioritize.txt", 'r');
$prids = array();
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$prid = $pieces[0];
	$sid = $pieces[1];
	$pid = $pieces[2];
	$model = $pieces[3];
	$set = $pieces[4];
	$sigfound = $pieces[5];
	$newsid = $samples[$sid];
	$newpid = $projects[$pid];
	$query = mysql_query("INSERT INTO prioritize (sample, project, model, trainingset, sigfound) VALUES ('$newsid', '$newpid', '$model', '$set', '$sigfound')");
	$prids[$prid] = mysql_insert_id();
}
fclose($fh);
if (file_exists("/tmp/$content.webstore/priorabs.txt")) {
	$fh = fopen("/tmp/$content.webstore/priorabs.txt", 'r');
	while (!feof($fh)) {
		$line = fgets($fh);
		$line = rtrim($line);
		if ($line == '') {
			continue;
		}
		$pieces = explode("\t",$line);
		$prid = $pieces[0];
		$aid = $pieces[1];
		$rank = $pieces[2];
		$sigGenes = $pieces[3];
		$newprid = $prids[$prid];
		$newaid = $aids[$aid];
		$query = mysql_query("INSERT INTO priorabs (prid, aid, rank, signGenes) VALUES ('$newprid', '$newaid', '$rank', '$sigGenes')");
		$prids[$prid] = mysql_insert_id();
	}
	fclose($fh);
}

//parents_relations
echo "<li>Storing parental  relations</li>";
flush();
$fh = fopen("/tmp/$content.webstore/parents_relations.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$sid = $pieces[0];
	$fid = $pieces[1];
	$fpid = $pieces[2];
	$mid = $pieces[3];
	$mpid = $pieces[4];
	$sib = $pieces[5];
	if (!array_key_exists($sid, $samples)) {
		// sample not in dump. (might be parent that was dumped)
		continue;
	}
	else {
		$newsid = $samples[$sid];
	}
	if (!array_key_exists($fid, $sample)) {
		// father not found, set to 0
		$newfid = 0;
		$newpfid = 0;
	}
	else {
		$newfid = $samples[$fid];
		if (array_key_exists($fpid, $projects)) {
			$newfpid = $projects[$fpid];
		}
		else {
			// check db for tfp for father. 
			$query = mysql_query("select trackfromproject FROM sample WHERE id = '$newfid'");
			$row = mysql_fetch_array();
			if ($row['trackfromproject'] != 0) {
				$newfpid = $row['trackfromproject'];
			}
			else {
				$newfpid = 0;
			}
		}
	}
	if (!array_key_exists($mid, $sample)) {
		// father not found, set to 0
		$newmid = 0;
		$newpmid = 0;
	}
	else {
		$newmid = $samples[$mid];
		if (array_key_exists($mpid, $projects)) {
			$newmpid = $projects[$mpid];
		}
		else {
			// check db for tfp for father. 
			$query = mysql_query("select trackfromproject FROM sample WHERE id = '$newmid'");
			$row = mysql_fetch_array();
			if ($row['trackfromproject'] != 0) {
				$newmpid = $row['trackfromproject'];
			}
			else {
				$newmpid = 0;
			}
		}
	}
	// got info, now store.
	$query = mysql_query("INSERT INTO parents_relations (id, father, father_project, mother, mother_project, siblings) VALUES ('$newsid', '$newfid', '$newfpid', '$newmid', '$newmpid', '$sib')");

}
fclose($fh);

// parents datapoints
$paids = array();
echo "<li>Storing parental datapoints</li>";
flush();
$fh = fopen("/tmp/$content.webstore/parents_datapoints.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$paid = $pieces[0];
	$content = $pieces[1];
	$lift = $pieces[2];
	if ($lift == '' || $lift == '\N') {
		$query = mysql_query("INSERT INTO parents_datapoints (content) VALUES ('$content')");
		$paids[$paid] = mysql_insert_id();
	}
	else {
		$query = mysql_query("INSERT INTO parents_datapoints (content, LiftedFrom) VALUES ('$content','$lift')");
		$paids[$paid] = mysql_insert_id();
	}
}

// parents_regions
echo "<li>Storing parental CNVs</li>";
flush();
$fh = fopen("/tmp/$content.webstore/parents_regions.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$paid = $pieces[0];
	$chr = $pieces[1];
	$start = $pieces[2];
	$stop = $pieces[3];
	$sid = $pieces[4];
	$logr = $pieces[5];
	$lift = $pieces[6];
	$newsid = $samples[$sid];
	$newpaid = $paids[$paid];
	if ($lift == '' || $lift == '\N') {
		$query = mysql_query("INSERT INTO parents_regions (id,chr,start,stop,sid,avgLogR) VALUES ('$newpaid','$chr','$start','$stop','$newsid','$logr')");
	}
	else {
		$query = mysql_query("INSERT INTO parents_regions (id,chr,start,stop,sid,avgLogR,LiftedFrom) VALUES ('$newpaid','$chr','$start','$stop','$newsid','$logr','$lift')");

	}
}
fclose($fh);

//parent_offspring cnv relations
$fh = fopen("/tmp/$content.webstore/parents_regions.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$aid = $pieces[1];
	$pseen = $pieces[2];
	$paid = $pieces[3];
	$psid = $pieces[4];
	if (!array_key_exists($psid, $samples)) {
		// parent is not known. 
		continue;
	}
	else {
		$newpsid = $samples[$psid];
	}
	$newaid = $aids[$aid];
	if ($pseen == 1) {
		$newpaid = $aids[$paid];
	}
	else {
		$newpaid = $paids[$paid];
	}
	// insert
	$query = mysql_query("INSERT INTO parents_offspring_cnv_relations (aid, parent_seen, parent_aid, parent_sid) VALUES ('$newaid', '$pseen', '$newpaid', '$newpsid')");

}
fclose($fh);

// clean up a bit
$aids = array();
$paids = array();

//parents_upd
echo "<li>Storing uniparental disomy results</li>";
flush();
$fh = fopen("/tmp/$content.webstore/parents_upd.txt", 'r');
$upds = array();
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$id = $pieces[0];
	$sid = $pieces[1];
	$fid = $pieces[2];
	$mid = $pieces[3];
	$type = $pieces[4];
	$chr = $pieces[5];
	$start = $pieces[6];
	$stop = $pieces[7];
	$pval = $pieces[8];
	$probes = $pieces[9];
	$logr = $pieces[10];
	$data = $pieces[11];
	$spid = $pieces[12];
	$fpid = $pieces[13];
	$mpid = $pieces[14];
	$lift = $pieces[15];
	if (!array_key_exists($sid, $samples)) {
		// sample not in dump. (might be parent that was dumped), skipping
		continue;
	}
	else {
		$newsid = $samples[$sid];
	}
	if (!array_key_exists($fid, $sample)) {
		// father not found, set to 0
		// skip (needs id to store datapoints)
		continue;
		#$newfid = 0;
		#$newpfid = 0;
	}
	else {
		$newfid = $samples[$fid];
		if (array_key_exists($fpid, $projects)) {
			$newfpid = $projects[$fpid];
		}
		else {
			// check db for tfp for father. 
			$query = mysql_query("select trackfromproject FROM sample WHERE id = '$newfid'");
			$row = mysql_fetch_array();
			if ($row['trackfromproject'] != 0) {
				$newfpid = $row['trackfromproject'];
			}
			else {
				$newfpid = 0;
			}
		}
	}
	if (!array_key_exists($mid, $sample)) {
		// father not found, set to 0
		// skip (needs id for data storage) (look it up?)
		continue;
		#$newmid = 0;
		#$newpmid = 0;
	}
	else {
		$newmid = $samples[$mid];
		if (array_key_exists($mpid, $projects)) {
			$newmpid = $projects[$mpid];
		}
		else {
			// check db for tfp for father. 
			$query = mysql_query("select trackfromproject FROM sample WHERE id = '$newmid'");
			$row = mysql_fetch_array();
			if ($row['trackfromproject'] != 0) {
				$newmpid = $row['trackfromproject'];
			}
			else {
				$newmpid = 0;
			}
		}
	}
	if ($lift == '\N' || $lift == '') {
		$query = mysql_query("INSERT INTO parents_upd (sid, fid, mid, type, chr, start, stop, pval, nrprobes, logR, data, spid, fpid, mpid ) VALUES ('$newsid', '$newfid', '$newmid', '$type', '$chr', '$start', '$stop', '$pval', '$probes', '$logr', '$data', '$newspid', '$newfpid', '$newmpid')");
	}
	else {
		$query = mysql_query("INSERT INTO parents_upd (sid, fid, mid, type, chr, start, stop, pval, nrprobes, logR, data, spid, fpid, mpid, LiftedFrom ) VALUES ('$newsid', '$newfid', '$newmid', '$type', '$chr', '$start', '$stop', '$pval', '$probes', '$logr', '$data', '$newspid', '$newfpid', '$newmpid','$lift')");
	}
	$upds[$id] = mysql_insert_id();

}
fclose($fh);

//parent_upd_data (if present)
$fh = fopen("/tmp/$content.webstore/parents_upd_datapoints.txt", 'r');
while (!feof($fh)) {
	$line = fgets($fh);
	$line = rtrim($line);
	if ($line == '') {
		continue;
	}
	$pieces = explode("\t",$line);
	$id = $pieces[0];
	$sid = $pieces[1];
	$content = $pieces[2];
	$lift = $pieces[3];
	if (!array_key_exists($sid, $samples)) {
		// sample not found
		continue;	
	}
	else {
		$newsid = $samples[$sid];
	}
	$newid = $upds[$id];
	if ($lift == '\N' || $lift == '') {
		$query = mysql_query("INSERT INTO parents_upd_datapoints (id, sid, content) VALUES ('$newid', '$newsid', '$content')");
	}
	else {
		$query = mysql_query("INSERT INTO parents_upd_datapoints (id, sid, content,LiftedFrom) VALUES ('$newid', '$newsid', '$content','$lift')");
	}		
}
fclose($fh);

// copy WGP && BAFSEG
echo "<li>Copying Whole Genome Plots to target directory</li>";
flush();
system("cp -Rf /tmp/$content.webstore/WGP/* $plotdir/WGP/");
echo "<li>Copying Whole Genome Plots to target directory</li>";
flush();
system("cp -Rf /tmp/$content.webstore/BAFSEG/* $plotdir/BAFSEG/");

echo "<li>Cleaning up</li>";
flush();
system("rm -Rf /tmp/$content.webstore");
echo "<li>All Done.</li>";
echo "</ol>";
echo "</div>";

?>
