	<div class="sectie">
<h3>Welcome </h3>
<p>Welcome to the CNV-analysis page of the Centre of Medical Genetics. We have developed this page to streamline the data-analysis of Illumina's Beadarray data and custom CNV reports. This includes, among others, a centralised storage and visualisation platform, interpretation and priorisation tools, reporting and searching functionality and crosslinking with multiple public resources.</p>

 <p>This site is password protected and you have to log in to use the site. If you want a guest account, please fill in the form available <a href='index.php?page=request_user'>here</a>. You will then gain access to the modules described below.</p>
</div>

<!-- <div class=sectie>
<h3>Main updates in CVN-WebStore 2.0</h3>
<p><ol>
<li>Rewritten Genome Browser : modular approach, more flexible to future adaptations</li>
<li>Fetch and install updates from the web-interface. Easy updating !</li>
<li>Analysis pipeline upgraded to QuantiSNPv2, VanillaICE 1.8 (for now)</li>
<li>Option to detect homozygous stretches using plink</li>
<li>Hardware upgraded from 8 to 48 cores</li>
<li>Option to upload datafiles for analysis using FTP !</li>
<li>Chip barcode and position of sample on chip can be stored</li>
<li>Option to retrieve all abstracts linked to a gene from inside WebStore</li>
<li> And Many more to come ...</li>
</ol>
</p>
</div> -->


 <?php
if (isset($loggedin) && $loggedin == 1) {
	$shortname = $_SESSION['dbname'];
	$query = mysql_query("SELECT ID FROM `GenomicBuilds`.`FailedLifts` WHERE uid = '$userid' AND tobuild = '$shortname'");
	if (mysql_num_rows($query) > 0) {
		$nr = mysql_num_rows($query);
		$stringname = $_SESSION['dbstring'];
		echo "<div class=sectie>";
		echo "<div style='padding-left:5px;padding-right:8px;border:solid 1px red'>";
		echo "<h3 style='color:red'>Manual CNV curation required</h3>";
		echo "<p>CNV-WebStore was recently moved to a more up-to-date genome build ($stringname). During this conversion $nr regions failed to convert automatically, and should be converted manually. </p>";
		echo "<p>To do so, click 'Curate' below and follow the guidelines. You can use the <a href='http://genome.ucsc.edu/cgi-bin/hgLiftOver' target='_blank'>LiftOver tool from UCSC</a> to convert regions between genome builds.</p>";
		echo "<p><form action='index.php?page=LiftCuration' method=POST><input type=submit class=button name=StartCuration value='Curate'></form></p>";
		echo "</div></div>";
	}
	## compare gene content between genome builds.
	echo "<div class=sectie><h3>Compare Gene Content between builds</h3>";
	echo "<p>During Genome Build lifting, the gene content might have changed for some regions.  <a href='index.php?page=LiftCompareGenes'>Click here to get an overview of the affected regions</a></p>";
	echo "</div>";
}

?> 
<div class=sectie>
<h3>Useful Downloads</h3>
<ol>
  <li><a href='getDemoData.php'>Demo Data Set</a> : We have composed a 20 minutes tutorial to learn the functions of the platform. First get a username and then go to the manual : <a href='index.php?page=demo'> here</a></li>
  <li><a href='files/ConvertBuild.tar'>ConvertBuild</a> : A perl script bundled with two datafiles to convert exported GenomeStudio datatables to the new GRCh37/hg19 Genome build. Run without options for a short manual.</li>
  <li><a href='files/FCR2BS.zip'>FCR2BS</a> : Convert Full Call Reports to Bead/GenomeStudio table format.</li>
  <li><a href='index.php?page=chiptypes'>Probe Locations</a> : Lists of the used probelocations for all supported chiptypes in the current Genome Build. 
</ol>
</div> 
<div class="sectie">
<?php
echo "<h3>News From the System Administrator</h3>";
if (isset($level) && $level == 3) {
	echo "<h4><a href='index.php?page=addnews'>Add News Item</a></h4>";
}
echo "<p><ul id=ul-simple>";
$query = mysql_query("SELECT Time, Entry FROM News WHERE TIMESTAMPDIFF(MONTH,Time,NOW())<12 ORDER BY Time DESC LIMIT 10");
//$query = mysql_query("SELECT Time, Entry FROM News ORDER BY Time DESC LIMIT 10");
while ($row = mysql_fetch_array($query)) {
	$time = $row['Time'];
	$times = explode(' ',$time);
	$time = $times[0];
	$entry = stripslashes($row['Entry']);
	echo "<li><span class=italic>$time</span> : $entry</li>";
}
?>
</ul>
</p>

</div>
<div class="sectie">
  <h3>Beadchip CNV-analysis</h3>
  <h4>analyse...</h4>
  <p><img src="images/content/bookmark.png" class="kader_links"/>
  We have combined three Hidden-Markov-Model algorithms and optimized their parameters to increase the resolution of our analysis to a minimum of 3 aberrant SNP's in a row.  The three used methods are QuantiSNP, PennCNV and VanillaICE. We have based our approach on an experimental validation of aberrations found using different parameter settings of each algorithm<br/><br/>
  To analyse your data, you will need two tables exported from Beadstudio. A table containing sample names, genders, callrates and index numbers, and a table containing snp names and chromosomal positions together with Genotype, BAF and LogR data for each sample. From here on, the input files are processed and the samples are analysed by the three methods and results are automatically combined in a BeadStudio compatible format. Incomplete results can be visualised for the samples that are already complete.<br/><br/>
 Once finished BeadStudio Bookmark files for each algorithm seperately and for the combined results are presented, containing information on size, number of probes and statistical confidences. Besides visualisation in BeadStudio, we  provide the donwstream analysis tools described below.
</p>

</div>

<div class="sectie">
<h3>Results</h3>
<h4>browse...</h4>
<p><img src="images/content/browser_scaled.png" class="kader_rechts"/>
After analysing your data with one or all of the supported algorithms, they can be presented to you for further analysis. For an easy overview, all samples can be plotted onto a single graphic per chromosome. Called CNV's can be selected here to show further details and linking to sample details. <br/><br/> Another option is to browse the called CNV's on a per sample basis, giving detailed information in a tabular format. 
When viewed here, an overview of size, position, number of probes will be available, together with an indication of how many times the aberration was already observed in analyses run on this server. On top of that, links are available to decipher cases or syndromes overlapping the aberration, refseq genes in the region are listed, tested qPCR primers are listed if there are any, and direct links to genome browsers are available for further investigation. All results are also stored in PDF files available for download, presenting the LogR and BAF plots for each chromosome, and the detected aberrations.  
<br/><br/>
Besides regular CNV analysis, data is screened for indications of mosaicism, and inheritance patterns can be checked if parental data is linked to the offspring data. These results are also browsable from easy to use crosslinks between the relevant samples.</p>
<br style="clear:both;">
</div>

<div class=sectie>
<h3>Visualise CNV's</h3>
<h4>inspect...</h4>
<p> 
<img src="images/content/ab_details_scaled.png" class="kader_links" />
To facilitate sharing your projects with users who don't have access to BeadStudio, or for quick inspection of the called CNV's, all probe data is stored in a central database.  This makes it possible to visualise all called aberrations in a convenient way in the web-interface. 
<br/><br/>
When hovering the mouse over a CNV region in overview graphics like the one shown above, a popup appears containing information on the CNV in question. It shows some basic information like the genomic position, the predicted copynumber state, the number of probes covering the CNV and the chiptype used. Together with some more specific and interactive details, the methods that called the region are listed, each with their respective scores.  <br/><br/>
When the sample identifier is clicked, the user is redirected to a page containing a tabular overview of the results for this sample. When there are genes affected by the aberration, the number of affected genes is a hyperlink leading to a page showing which genes are involved, together with easy links to databases, and literature searches. <br/><br/>
Finally, when the user hovers the mouse over the probes in the LogR and B-allele frequency plots, probe specific data are presented to the user as shown on the right.  To further facilitate interpretation, non polymorphic probes (cnvi-probes) are shown in red. 
</p>
<br style="clear:both;">
</div> 

<div class=sectie>
<h3>Quality Check</h3>
<h4>provided by PennCNV...</h4>
<p>
<img src="images/content/sample_details.png" class="kader_rechts"/>
BeadStudio provides some quality control measures with their 'Control Dashboard', but interpretation of these values is not straightforward.  That's why some basic quality measurements are available for each sample analysed on the server.  They are presented by PennCNV as a side-effect of CNV-calling, and can provide additional information on your data quality. Please refer to the original publications for interpretion of these values. Before any sample is analysed, they are checked for genomic wave and normalised if needed.  
<br style="clear:both;">
</div>

<div class=sectie>
<h3>Search on Region</h3>
<h4>quickly compare to other samples...</h4>
<p>
<img src="images/content/search_region_scaled.png" class=kader_links />
One of the questions we get most frequently is: "Hi, our friends from lab X have detected a deletion in chromosome 17p13.3, do we have one, so we can collaborate?". <br/>That is why a search function is so important.  Users can search on chromosomal locations (or genes in the future) and check if they have samples harbouring CNV's in that region. Results are again presented in a tabular format, or in the graphical interface described above. Besides their own samples, some control projects are also available for each supported chiptype to check for chip-specific artefacts. 
</p>
<br style="clear:both;">
</div>

<div class="sectie">
<h3>Design qPCR-Primers</h3>
<h4>confirmation...</h4>
<p> 
<img src="images/content/primerdesign_result_scaled.png" class=kader_rechts />
Based on our experience with qPCR on genomic DNA, we have automised the process of designing new primers.  This module takes the region you're interested in and retrieves the sequence from the UCSC browsers.  It will then extract exons and known SNP's from it.  After identifying unique sequences using a local Blast implementation, primers will be designed using primer3, optimized following parameter settings based on our experience and checked for secondary structures using mfold. The results are stored and presented as shown on the right. <br/><br/>When the user clicks a successfull row, he will be presented with primer and amplicon details. Because this design pipe runs in the background all the time, you have a high chance of finding already designed primers by just searching a the region you're interested in, before starting a design process yourself.  <br/><br/>When you use qPCR primers and they work well (we had over 80% success rate using them), we would like you to insert them into our qPCR primer database. Primers inserted here are presented when browsing CNV results for a sample in tabular form, or can be searched. <br/><br/>
For qPCR validation of array data, we use SybrGreen analysis on the Roche LightCycler 480.  Ct-value output can be uploaded here, and with a few clicks you have Copy-number estimates for the tested alleles. Mathematical explaination will follow when I find a nice way to format Math formula's in websites
</p>
<br style="clear:both;">
</div>

<div class=sectie>
<h3>Other Features</h3>
<h4>to be complete...</h4>
<p>There are some other features worth mentioning: 
<ul id=ul-simple>
<li> - CNV prioritization by Endeavour
<li> - Possibility to share your projects with other users
<li> - Filtering results to create custom bookmark files for BeadStudio
<li> - Creation of BED-files for visualisation in UCSC/ensemble genome browsers
<li> - UniSeq Finder: the part of the qPCR analysis pipeline to identify unique fragments in a DNA sequence
</ul>
</p>
</div>

<div class=sectie>
<h3>References:</h3>
<p><ul id=ul-disc>
<li>Vandeweyer G, Reyniers E, Wuyts W, Rooms L, Kooy RF. <a href="http://www.biomedcentral.com/1471-2105/12/4">CNV-WebStore: Online CNV Analysis, Storage and Interpretation</a> <journal>BMC Bioinformatics</journal> 2011 jan5;12(1):4.</li>
<li>Wang K, Li M, Hadley D, Liu R, Glessner J, Grant S, Hakonarson H, Bucan M. <a href="http://genome.cshlp.org/cgi/content/short/17/11/1665">PennCNV: an integrated hidden Markov model designed for high-resolution copy number variation detection in whole-genome SNP genotyping data</a> <journal>Genome Research</journal> 17:1665-1674, 2007 </li>
<li>Colella S, Yau C, Taylor J, Mirza G, Butler H, Clouston P, Bassett A, Seller A, Holmes C, Ragoussis J. <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC1874617/">QuantiSNP: an Objective Bayes Hidden-Markov Model to detect and accurately map copy number variation using SNP genotyping data</a> <journal>Nucleic Acids Research</journal> 35(6): 2013-2025, 2007</li>
<li>Scharpf RB, Parmigiani G, Pevsner J, Ruczinski I. <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2710854/">Hidden Markov models for the assessment of chromosomal alterations using high-throughput SNP arrays</a> <journal>The annals of Applied Statistics</journal> 2(2): 687-713, 2008</li>
</ul>
</div>

  
