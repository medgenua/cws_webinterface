
<?php
ob_start();
if ($loggedin != 1) {
	echo "<div class=sectie><h3>qPCR Primerdatabase</h3>\n";
	echo "<h4>... of validated primers</h4>\n";
	include('login.php');
	echo "</div>\n";
}
else {
	for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}
	$chromhash["X"] = "23";
	$chromhash["Y"] = "24";
	$chromhash["23"] = "X";
	$chromhash["24"] = "Y";

	$username = $_SESSION['username'];
	$tdtype= array("","class=alt");
	$thtype= array("class=spec","class=specalt");
	$topstyle = array("class=topcell","class=topcellalt");
	$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("CNVanalysis");


	////////////////////
	// SEARCH PRIMERS //
	////////////////////
	if ($_GET['type'] == "search") {
		if (!empty($_POST['location'])) {
			$location = $_POST['location'] ;
		}
		else {
			if (!empty($_GET['location'])) {
				$location = $_GET['location'];
			}
			else{
				$location = "";
			}
		}	
		echo "<div class=sectie>\n";
		echo "<h3>Search designed Realtime Primers</h3>\n";
		echo "<p>Search primers based on chromosomal region.  Queries are made using UCSC location notation (eg: chr15:30000-7064200).</p>\n";
		echo "<form action=index.php?page=manageprimers&amp;type=search method=POST>\n";
		echo "<p><table id=mytable cellspacing=0>\n";
		echo "<tr class=clear>\n";
		echo " <th class=clear>Chromosomal Location</td>\n";
		echo " <td class=clear><input type=text name=location width=150px value=$location></td>\n";
		echo " <td class=clear><input type=submit class=button value=Search></td>\n";
		echo "</tr>\n";
		echo "</table>\n";
		echo "</div>\n";
		if ($location <> "") {
			#$location = $_POST['location'];
			$pieces = preg_split("/[:\-_+(\.\.)]/",$location);
			$chrtxt = preg_replace("/chr(\w+)/","$1",$pieces[0]);
			$chr = $chromhash[ $chrtxt ] ;
			$start = $pieces[1];
			$end = $pieces[2];
			$end = str_replace(",","",$end);
			$start = str_replace(",","",$start);
			$query = mysql_query("SELECT Chr, Start, End, Forward, Reverse, Class, Creator FROM Primers WHERE chr = \"$chr\" AND (Start >= \"$start\" AND End <= \"$end\")");
			#echo "chrom $chr from $start to $end<br>\n";
			$numrows = mysql_num_rows($query);
			if ($numrows == 0 ) { 
				echo "<div class=sectie>\n";
				echo "<h3>No amplicons found for $location</h3>\n";
				echo "<p>Please insert working primers into the database for future usage!</p>\n";
				echo "</div>\n"; 
			}
			else {
				echo "<div class=sectie>\n";
				echo "<h3> Available amplicons for $location</h3>\n";
				echo "<p>\n";
				echo "<table id=mytable cellspacing=0>\n";
				echo " <tr>\n";
				echo "  <th $firstcell>Location</td>\n";
				echo "  <th>Forward</td>\n";
				echo "  <th>Reverse</td>\n";
				echo "  <th>Class</td>\n";
				echo "  <th>Creator</td>\n";
				echo " </tr>\n";
				$switch=0;
				while ($row = mysql_fetch_assoc($query)) {
					echo " <tr>\n";
					echo "  <td $tdtype[$switch] $firstcell>" .$chromhash [ $row['Chr'] ].":".$row['Start']."-".$row['End']."</td>\n";
					echo "  <td $tdtype[$switch]>".$row['Forward']."</td>\n";
					echo "  <td $tdtype[$switch]>".$row['Reverse']."</td>\n";
					echo "  <td $tdtype[$switch]>".$row['Class']."</td>\n";
					echo "  <td $tdtype[$switch]>".$row['Creator']."</td>\n";
					echo " </tr>\n";
					$switch = $switch + pow(-1,$switch);
					$found = 1;
				}
				echo "</table>\n";
				echo "</div>\n";
	
			}
		}
	}
	////////////////////////
	// INSERT NEW PRIMERS //
	////////////////////////

	elseif ($_GET['type'] == "insert") {
	   if ($level > 1){
	 	echo " <div class=sectie>\n";
 		echo " <h3>Insert Realtime Primers into the database</h3>\n";
 		echo "<p> On this page, the database of realtime primers can be managed. Please fill in all working primers consequently, so no time is lost in reinventing the wheel all the time. </p>\n";
 		echo "<p> There are two options: enter the start and end positions manually, or paste the locations UCSC style.</p>\n";
 		echo "</div>\n";
 		echo "<div class=sectie>\n";
 		echo "<h3>Manually enter new primer pairs</h3>\n";
 		echo "<p>\n";
 		echo "<form action=index.php?page=insertprimers&amp;type=manual method=POST>\n";

 		echo "<table id=mytable cellspacing=0>\n";
 		echo " <tr class=clear>\n";
 		echo "  <th class=clear>Creator: </th>\n";
 		echo "  <td class=clear><input type=text name=creator width=150px></td>\n";
 		echo " </tr>\n";
 		echo " <tr>\n";
		echo "  <th $firstcell>Chromosome:</th>\n";
 		echo "  <th >Start position: </th>\n";
 		echo "  <th >End position: </th>\n";
 		echo "  <th >Forward: </th>\n";
 		echo "  <th >Reverse: </th>\n";
 		echo "  <th >Class: </th>\n";
 		echo " </tr>\n";

		  
 		$switch=0;

  		for($i=1;$i<=10;$i++){
			echo " <tr>\n";
			echo "  <td $firstcell $tdtype[$switch]><input type=text name=chr$i style=\"width: 90px\"></td>\n";
			echo "  <td $tdtype[$switch]><input type=text name=start$i style=\"width:120px\"></td>\n";
			echo "  <td $tdtype[$switch] ><input type=text name=end$i style=\"width:120px\"></td>\n";
			echo "  <td $tdtype[$switch] ><input type=text name=forward$i ></td>\n";
			echo "  <td $tdtype[$switch] ><input type=text name=reverse$i ></td>\n";
			echo "  <td $tdtype[$switch]><select name=class$i>\n";
			echo "	<option value=TP selected>True Positive</option>\n";
			echo "	<option value=FP>False Postive</option>\n";
			echo "	<option value=FN>False Negative</option>\n";
			echo "	</select></td>\n";
			echo " </tr>\n";
			$switch = $switch + pow(-1,$switch);
 		 }  
 		echo "</table>\n";
 		echo "</p><p>\n";
 		echo "<input type=submit class=button value=submit>\n";
 		echo "</form>\n";
 		echo "</div>\n";


 		echo "<div class=sectie>\n";
 		echo "<h3>Paste Position UCSC style</h3>\n";
 		echo "<h4>eg: chr17:2130500-2870100</h4>\n";
 		echo "<p>\n";
 		echo "<form action=index.php?page=insertprimers&amp;type=ucsc method=POST>\n";
 		echo "<table id=mytable cellspacing=0>\n";
 		echo " <tr class=clear>\n";
 		echo "  <th class=clear>Creator:</th>\n";
 		echo "  <td class=clear><input type=text name=creator width=150px></td>\n";
 		echo " </tr>\n";
  		echo "<tr>\n";
  		echo " <th $firstcell>Position String:</th>\n";
 		echo "  <th >Forward: </th>\n";
 		echo "  <th >Reverse: </th>\n";
 		echo "  <th >Class: </th>\n";
 		echo " </tr>\n";


 
  		$switch=0;

  		for($i=1;$i<=10;$i++){
			echo " <tr>\n";
	 		echo "  <td $firstcell $tdtype[$switch]><input type=text name=position$i style=\"width:250px\"></td>\n";
	 		echo "  <td $tdtype[$switch] ><input type=text name=forward$i ></td>\n";
	 		echo "  <td $tdtype[$switch] ><input type=text name=reverse$i ></td>\n";

	 		echo "  <td $tdtype[$switch]><select name=class$i>\n";
			echo "	<option value=TP selected>True Positive</option>\n";
	 		echo "	<option value=FP>False Postive</option>\n";
	 		echo "	<option value=FN>False Negative</option>\n";
	 		echo "	</select></td>\n";
	 		echo " </tr>\n";
	 		$switch = $switch + pow(-1,$switch);
  		}  
  		echo "</table>\n";
  		echo "</p><p>\n";
  		echo "<input type=submit class=button value=submit>\n";
  		echo "</form>\n";
  		echo "</div>\n";

	    }
	    else {
		include('page_noguest.php');
	    }
	}
	elseif ($_GET['type'] == "delete") {
		echo "Under construction ";
	}
	else {
		echo "<div class=sectie>\n";
		echo "<h3>qPCR Primerdatabase</h3>\n";
		echo "<h4>... of validated primers</h4>\n";
		echo "<p>From here primers that are experimentally used and confirmed to work well, can be stored and searched in a database. "; 
		echo "These are also the primers that are listed next to aberrations in the analysis overviews.</p>\n";
		echo "</div>\n";
	}
	
}

?>

