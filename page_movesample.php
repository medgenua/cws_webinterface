<?php
$yesno = array("1" => 'Yes', '0' => 'No');
if ($loggedin != 1) {
	include('login.php');
}
else {
?>
<script type='text/javascript' src='javascripts/movesample.js'></script>

<?php
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

###################
# GET POSTED VARS #
###################
$sid = $_POST['sid'];
$pid = $_POST['pid'];
$oldpid = $_POST['oldpid'];


if (isset($_POST['Cancel'])) {
	// move cancelled, redirect to sample page
	echo "<div class=sectie>";
	echo "<h3>Move Cancelled</h3>";
	echo "<p>You will be redirected back to the sample details.</p>";
	echo "<p><a href='index.php?page=details&amp;project=$oldpid&amp;sample=$sid'>Click here if redirect does not work</p>";
	echo "<meta http-equiv='refresh' content='2;URL=index.php?page=details&amp;project=$oldpid&amp;sample=$sid'>\n";
	echo "</div>";
	exit();	
}

if (isset($_POST['MoveSid'])) {
	if ($_POST['newpid'] == 'newproject') {
		//////////////////////////
		// create a new project //
		//////////////////////////
		$newpname = $_POST['newprojectname'];
		$newcollection = $_POST['newcollection'];
		if ($newpname == '') {
			echo "<div class=sectie>";
			echo "<h3>New Project Name missing</h3>";
			echo "<p>Go back and fill in a name for the new project.</p></div>";
			exit();
		}
		// get original project details
		$query = mysql_query("SELECT p.chiptype, p.chiptypeid, p.collection,p.naam,p.asymFilter,p.minsnp,p.minmethods,p.pipeline,p.plink,p.datatype FROM project p WHERE id = $oldpid");
		$row = mysql_fetch_array($query);
		$chip = $row['chiptype'];
		$chipid = $row['chiptypeid'];
		$pcol = $row['collection'];
		$opname = $row['naam'];
		$asym = $row['asymFilter'];
		$minsnp = $row['minsnp'];
		$minmeth = $row['minmethods'];
		$pipe = $row['pipeline'];
		$plink = $row['plink'];
		$datatype = $row['datatype'];

		// create new collection if specified
		if ($newcollection == 'new') {
			$ncname = $_POST['newcolname'];
			$ncdescr = $_POST['newcolcomment'];
			if ($ncname == '' || $ncdescr = '') {
				echo "<div class=sectie>";
				echo "<h3>New Collection Details Missing</h3>";
				echo "<p>Go back and fill in all details for creating a new collection.</p></div>";
				exit();
			}
			$query = mysql_query("INSERT INTO collections (name, comment) VALUES ('$ncname', '$ncdescr')");
			#$colid = mysql_insert_id();
			$newcollection = $ncname;
		}
		// create the project
		$created = date("j/n/Y - G:i:s"); 
		$query = mysql_query("INSERT INTO project (naam, chiptype, created, collection, userID, chiptypeid,asymFilter,minsnp,minmethods,pipeline,plink,finished,datatype) VALUES ('$newpname', '$chip', '$created','$newcollection', '$userid', '$chipid','$asym','$minsnp','$minmeth','$pipe','$plink','1', '$datatype' )");
		$newpid = mysql_insert_id();
		$query = mysql_query("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic, editsample) VALUES ('$newpid', '$userid', '1','1','1')");
		// some sample details for nice printout
		/*
		$query = mysql_query("SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'");
		$row = mysql_fetch_array($query);
		$samplename = $row['chip_dnanr'];
		$gender = $row['gender'];
		*/
		// print page for permission settings
		echo "<div class=sectie>";
		echo "<h3>Samples Moved to a New Project</h3>";
		echo "<h4>Old project: '$opname' / New Project: '$newpname'</h4>";
		$skipped = array();
		$ok = array();
		foreach ($_POST['sids'] as $k => $sid) {
			// add moving event to the log.
			$query = "INSERT INTO log (sid,pid,aid,uid,entry,arguments) VALUES ('$sid','$newpid','0','$userid','moved from project \'".addslashes($opname)."\' to \'$newpname\'','')";
			mysql_query($query);
			// update all tables !!!!!
			mysql_query("UPDATE aberration SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE aberration_mosaic SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE aberration_LOH SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE BAFSEG SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			mysql_query("UPDATE deletedcnvs SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE log SET pid = '$newpid' WHERE pid = '$oldpid' AND sid = '$sid'");	
			mysql_query("UPDATE log_mos SET pid = '$newpid' WHERE pid = '$oldpid' AND sid = '$sid'");	
			mysql_query("UPDATE parents_relations SET father_project = '$newpid' WHERE father_project = '$oldpid' AND father = '$sid'");
			mysql_query("UPDATE parents_relations SET mother_project = '$newpid' WHERE mother_project = '$oldpid' AND mother = '$sid'");
			mysql_query("UPDATE plots SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			mysql_query("UPDATE prioritize SET project = '$newpid' WHERE project = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE projsamp SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			mysql_query("UPDATE sample SET trackfromproject = '$newpid' WHERE trackfromproject = '$oldpid' AND id = '$sid'");
			mysql_query("UPDATE parents_upd SET spid = '$newpid' WHERE sid = '$sid' AND spid = '$oldpid'");
			mysql_query("UPDATE parents_upd SET fpid = '$newpid' WHERE fid = '$sid' AND fpid = '$oldpid'");
			mysql_query("UPDATE parents_upd SET mpid = '$newpid' WHERE mid = '$sid' AND mpid = '$oldpid'");
			// get sample name
			$query = mysql_query("SELECT chip_dnanr FROM `sample` WHERE id = $sid");
			$row = mysql_fetch_array($query);
			$ok[$row['chip_dnanr']] = 1;
		}
		if (count($ok) > 0) {
			ksort($ok);
			echo "<p><span class=nadruk>The following samples were moved to the new project:</span></p>";
			echo "<div style='padding-left:1em;padding-bottom:2em;>";
			foreach($ok as $s => $v) {
				echo "<span style='float:left;width:16%'>$s</span>";
			}
			echo "</div>";
		}
		echo "<p><span class=nadruk>Update Permissions:</span></p>";

		echo "<p>Moving samples might have implications on permissions for the users and groups listed below. Please specify the access details below if needed by clicking on the provided options. For more complicated changes, please use the 'share project' options from the target project.</p>";
		echo "<p><table cellspacing=0>";
		// list groups with old permission and option to change it
		$groups = mysql_query("SELECT groupid FROM projectpermissiongroup WHERE projectid = $oldpid OR projectid = $newpid");
		if (mysql_num_rows($groups) > 0) {
			echo "<tr><th colspan=4 $firstcell>User Groups</th></tr>";
			echo "<tr><th class=specalt $firstcell>Group Name</th><th class=specalt>Access Type</th><th class=specalt>Source Project</th><th class=specalt>Target Project</th></tr>";
			while ($gr = mysql_fetch_array($groups)) {
				$groupid = $gr['groupid'];
				## get group permissions
				$gpq = mysql_query("SELECT editclinic, editcnv, editsample, name FROM usergroups WHERE id = '$groupid'");
				$gpr = mysql_fetch_array($gpq);
				$editcnv = $gpr['editcnv'];
				$editclinic = $gpr['editclinic'];
				$editsample = $gpr['editsample'];
				$gname = $gpr['name'];
				if ($editcnv == 1 && $editclinic == 1) {
					$perm = 'Full';
				}
				elseif ($editcnv == 1) {
					$perm = 'CNV';
				}
				elseif ($editclinic == 1) {
					$perm = 'Clinic';
				}
				else {
					$perm = 'Read-only';
				}
				if ($editsample == 1 ) {
					$perm .= " / Project Control";
				}
				$check = mysql_query("SELECT groupid FROM projectpermissiongroup WHERE projectid = '$oldpid' AND groupid = '$groupid'");
				if (mysql_num_rows($check) > 0) {
					$oldacc = 1;
				}
				else {
					$oldacc = 0;
				}
				$check = mysql_query("SELECT groupid FROM projectpermissiongroup WHERE projectid = '$newpid' AND groupid = '$groupid'");
				if (mysql_num_rows($check) > 0) {
					$newacc = 1;
				}
				else {
					$newacc = 0;
					$addacc = "<span  style='float:right' title='Give Access To This Group' onclick='addaccess($groupid,$newpid)'><img src='images/bullets/plus-icon.png' width=8px height=8px></span>";
				}
				#$yesno = array("1" => 'Yes', '0' => 'No');
				echo "<tr><th class=specalt $firstcell NOWRAP>$gname</th><td>$perm</td><td>$yesno[$oldacc]</td><td><span id=add$groupid>$yesno[$newacc] $addacc</span></td></tr>";
			}
			#echo "</table>";
		}
		// seperate users
		echo "</table></p>";
		echo "<p><table cellspacing=0>";
		$users = mysql_query("SELECT u.FirstName, u.LastName, userid,editcnv, editclinic, editsample, projectid FROM projectpermission pp JOIN users u ON pp.userid = u.id WHERE projectid = $oldpid OR projectid = $newpid");
		if (mysql_num_rows($users) > 0) {
			$doneusers = array();	
			echo "<tr><th colspan=4 $firstcell>Seperate Users</th></tr>";
			echo "<tr><th class=specalt $firstcell>User Name</th><th class=specalt>Source Project Permission</th><th class=specalt>Target Project Permission</th><th class=specalt>Transfer Permission</tr>";
			while ($ur = mysql_fetch_array($users)) {
				$thisuserid = $ur['userid'];
				if ($doneusers[$thisuserid] == 1) {
					# user done before
					continue;
				}
				$doneusers[$thisuserid] = 1;
				$thisusername = $ur['FirstName'] . " " . $ur['LastName'];
				## get group permissions
				$editcnv = $ur['editcnv'];
				$editclinic = $ur['editclinic'];
				$editsample = $ur['editsample'];
				$project = $ur['projectid'];
				if ($editcnv == 1 && $editclinic == 1) {
					$perm = 'Full';
				}
				elseif ($editcnv == 1) {
					$perm = 'CNV';
				}
				elseif ($editclinic == 1) {
					$perm = 'Clinic';
				}
				else {
					$perm = 'Read-only';
				}
				if ($editsample == 1 ) {
					$perm .= " / Project Control";
				}
				## check other project permission for current user.
				if ($project == $oldpid) {
					$oldperm = $perm;
					$check = mysql_query("SELECT editcnv, editclinic, editsample FROM projectpermission WHERE projectid = $newpid AND userid = $thisuserid");					 if (mysql_num_rows($check) > 0) {
						$cr = mysql_fetch_array($check);
						$editcnv = $cr['editcnv'];
						$editclinic = $cr['editclinic'];
						$editsample = $cr['editsample'];
						$project = $cr['projectid'];
						if ($editcnv == 1 && $editclinic == 1) {
							$perm = 'Full';
						}
						elseif ($editcnv == 1) {
							$perm = 'CNV';
						}
						elseif ($editclinic == 1) {
							$perm = 'Clinic';
						}
						else {
							$perm = 'Read-only';
						}
						if ($editsample == 1 ) {
							$perm .= " / Project Control";
						}
						$newperm = $perm;
					}
					else {
						$newperm = 'none';
					}
				}
				else {
					$newperm = $perm;
					$check = mysql_query("SELECT editcnv, editclinic, editsample FROM projectpermission WHERE projectid = $oldpid AND userid = $thisuserid");					 if (mysql_num_rows($check) > 0) {
						$cr = mysql_fetch_array($check);
						$editcnv = $cr['editcnv'];
						$editclinic = $cr['editclinic'];
						$editsample = $cr['editsample'];
						$project = $cr['projectid'];
						if ($editcnv == 1 && $editclinic == 1) {
							$perm = 'Full';
						}
						elseif ($editcnv == 1) {
							$perm = 'CNV';
						}
						elseif ($editclinic == 1) {
							$perm = 'Clinic';
						}
						else {
							$perm = 'Read-only';
						}
						if ($editsample == 1 ) {
							$perm .= " / Project Control";
						}
						$oldperm = $perm;
					}
					else {
						$oldperm = 'none';
					}

				}
				#print user
				if ($oldperm != $newperm) {
					$addacc = "<span id=adduser$thisuserid><a href='javascript:void()' style='font-size:10px' onclick='addaccessuser($thisuserid,$oldpid,$newpid)'>$newperm => $oldperm</a></span>";
				}
				else {
					$addacc = '/';
				}
				echo "<tr><th class=specalt $firstcell NOWRAP>$thisusername</th><td id=oldperm$thisuserid>$oldperm</td><td id=newperm$thisuserid>$newperm </td><td>$addacc</td></tr>";		

			}
			
		}
		echo "</table>";
		// store newpid in recent
		$recentq = mysql_query("SELECT RecentTargetProjects FROM usersettings WHERE id = '$userid'");
		$recent = mysql_fetch_array($recentq);
		$rp = explode(":",$recent['RecentTargetProjects']);
		foreach ($rp as $key => $link)
		{
    			if ($rp[$key] == '')
    			{
        			unset($rp[$key]);
    			}
		}
		if (!in_array($newpid,$rp)) {
			if (count($rp) > 5) {
				array_shift($rp);
			}
			array_push($rp,$newpid);
		}
		$newrecent = implode(":",$rp);
		mysql_query("UPDATE `usersettings` SET RecentTargetProjects = '$newrecent' WHERE id = '$userid'");
		// form to go back to the updated sample
		echo "</div>";
		echo "<div class=sectie>";
		echo "<h3>Sample was successfully moved</h3>";
		echo "<p>You will need to refresh some of the pages you were visiting to update all the information. To go back to the updated sample details page, click <a href='index.php?page=details&sample=$sid&project=$newpid'>here</a>.</p>";
		echo "</div>";
		exit();
	}
	elseif ($_POST['newpid'] == 'delete') {
		// delete the sample
		// first warn !

		// get original project details
		$query = mysql_query("SELECT chiptype, chiptypeid, collection,naam FROM project WHERE id = $oldpid");
		$row = mysql_fetch_array($query);
		$chip = $row['chiptype'];
		$chipid = $row['chiptypeid'];
		$pcol = $row['collection'];
		$opname = $row['naam'];
		// some sample details for nice printout
		/*
		$query = mysql_query("SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'");
		$row = mysql_fetch_array($query);
		$samplename = $row['chip_dnanr'];
		$gender = $row['gender'];
		*/
		// start deleting
		# datapoints
		$skipped = array();
		$ok = array();
		$formstring = '';
		$sidstring = '';
		foreach ($_POST['sids'] as $k => $sid) {
			$abs = mysql_query("SELECT id FROM aberration WHERE idproj = '$oldpid' AND sample = '$sid'");
			$s = '';
			while ($row = mysql_fetch_array($abs)) {
				//$aid = $row['id'];
				//$del = mysql_query("DELETE FROM datapoints WHERE id = '$aid'");
				$s .= $row['id'].",";
			}
			if ($s != '') {
				$s = substr($s,0,-1);
				mysql_query("DELETE FROM datapoints WHERE id IN ($s)");
			}


			# datapoints LOH
			$abs = mysql_query("SELECT id FROM aberration_LOH WHERE idproj = '$oldpid' AND sample = '$sid'");
			$s = '';
			while ($row = mysql_fetch_array($abs)) {
				//$aid = $row['id'];
				//$del = mysql_query("DELETE FROM datapoints_LOH WHERE id = '$aid'");
				$s .= $row['id'].",";
			}
			if ($s != '') {
				$s = substr($s,0,-1);
				mysql_query("DELETE FROM datapoints_LOH WHERE id IN ($s)");
			}

			# plots (and delete the files themselve)
			$files = mysql_query("SELECT filename FROM plots WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			// you really need filenames from table, because joining and renaming projects can change pid for a file. 
			while (list($filename) = mysql_fetch_array($files)) {
				unlink("Whole_Genome_Plots/$filename");
			}
			$del = mysql_query("DELETE FROM plots WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			$files = mysql_query("SELECT filename FROM BAFSEG WHERE idproj = '$oldpid' AND idsamp = '$sid' ");
			while (list($filename) = mysql_fetch_array($files)) {
				unlink("Whole_Genome_Plots/$filename");
			}
			$del = mysql_query("DELETE FROM BAFSEG WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			# aberrations
			$del = mysql_query("DELETE FROM aberration WHERE idproj = '$oldpid' AND sample = '$sid'");
			$del = mysql_query("DELETE FROM aberration_mosaic WHERE idproj = '$oldpid' AND sample = '$sid'");
			$del = mysql_query("DELETE FROM aberration_LOH WHERE idproj = '$oldpid' AND sample = '$sid'");

			#Prioritizations
			$prs = mysql_query("SELECT id FROM prioritize WHERE project = '$oldpid' AND sample = '$sid'");
			while ($row = mysql_fetch_array($prs)) {
				$prid = $row['id'];
				$del = mysql_query("DELETE FROM priorabs WHERE prid = '$prid'");

			}
			$del = mysql_query("DELETE FROM prioritize WHERE project = '$oldpid' AND sample = '$sid'");
			#project permissions
			#$del = mysql_query("DELETE FROM projectpermission WHERE projectid = '$oldpid' AND sample = '$sid'");
			# group permissions 
			#$del = mysql_query("DELETE FROM projectpermissiongroup WHERE projectid = '$oldpid' AND sample = '$sid'");
			# project samples & samples
			//$samples = mysql_query("SELECT idsamp FROM projsamp WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			//while ($row = mysql_fetch_array($samples)) {
			//$sid = $row['idsamp'];
			$details = mysql_query("SELECT intrack, trackfromproject, chip_dnanr FROM sample WHERE id = '$sid'");
			$detrow = mysql_fetch_array($details);
			$intrack = $detrow['intrack'];
			$tfp = $detrow['trackfromproject'];
			$sample = $detrow['chip_dnanr'];
			$ok[$sample] = 1;
			if ($intrack == 1 && $tfp == $oldpid) { 
				$alternatives = mysql_query("SELECT ps.idproj, p.naam, p.created FROM project p JOIN projsamp ps ON ps.idproj = p.id WHERE ps.idsamp = '$sid' AND  NOT ps.idproj = '$oldpid' ORDER BY ps.idproj DESC");
				if (mysql_num_rows($alternatives) > 0) {
					$formstring .= "- $sample : Alternative projects:<select name=$sid>";
					$sidstring .= "$sid"."_";
					$needsupdate = 1;
					#echo "$sample needs alternative : <br>";
					# delete from projsamp, and update table sample to most recent project by default,  and create form to update table if other project is wanted
					$selected = "selected";
					while ($prow  = mysql_fetch_array($alternatives)) {
						$altid =$prow['idproj'];
						$altname = $prow['naam'];
						$altcreated = $prow['created'];
						#echo " - $altid : $altname<br>";
						if ($selected != "") {
							$update = mysql_query("UPDATE sample SET trackfromproject = '$altid' WHERE id = '$sid'");
							$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$oldpid'");
							$formstring .="<option value='$sid-$altid' $selected>$altname ($altcreated)</option>";
							$selected = '';
						}
						else { 
							$formstring .= "<option value='$sid-$altid'>$altname ($altcreated)</option>";
						}

					}
					$formstring .= "</select><br/>\n";
				}
				else {
					//echo "$sample will be deleted (no other projects<br>";
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$oldpid'");
					$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
					# delete from projsamp &amp; sample
				}
		
			}
			elseif ($intrack == 1) {
				# delete from projsamp ?
				$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$oldpid'");
				//continue;
				# in track from different project (assume it exists)
				
			}
			else{ 
				# not in track, check if it is included in a different project 
				$checkproj = mysql_query("SELECT idproj FROM projsamp WHERE idsamp = '$sid' AND NOT idproj = '$oldpid'");
				if (mysql_num_rows($checkproj) > 0) {
					#echo "$sample not in track but in other projects, not deleting completely<br>";
					#only delete from projsamp
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$oldpid'");
				}
				else {
					#echo "$sample not in track nor other projects, deleting completely<br>";
					#delete from projsamp &amp; sample
					$delete = mysql_query("DELETE FROM projsamp WHERE idsamp = '$sid' AND idproj = '$oldpid'");
					$delete = mysql_query("DELETE FROM sample WHERE id = '$sid'");
					$delete = mysql_query("DELETE FROM `sample_phenotypes` WHERE sid = '$sid'");
					
				}	
			}
		//}
		} // end of foreach $sids
		if (count($ok) > 0) {
			ksort($ok);
			echo "<p><span class=nadruk>The following samples were deleted without issues:</span></p>";
			echo "<div style='padding-left:1em;padding-bottom:2em;>";
			foreach($ok as $s => $v) {
				echo "<span style='float:left;width:16%'>$s</span>";
			}
			echo "</div>";
		}

		if ($formstring != '') {
			$sidstring = substr($sidstring,0,-1);
			echo "<div class=sectie>";
			echo "<h3>Update needed:</h3>";
			echo "<p>Some samples were accessed by default from the project you just deleted them from. If you want them to be accessed by default from another project than the one selected here, change it and press submit. Otherwise, you can close this window.</p>";
			echo "<p><form action=change_permissions.php method=POST>\n";
			echo "<input type=hidden name=sids value='$sidstring'>\n";
			echo $formstring;
			echo "<input type=submit class=button name=changedef value=Submit>";
			echo "</form>\n";
		}
		// form to go back to the updated sample
		#echo "</div>";
		echo "<div class=sectie>";
		echo "<h3>Sample was successfully deleted</h3>";
		echo "<p>Refresh any pages that show information about the sample to update their content.</p>";
		echo "</div>";

		## and exit
		exit();

	}
	else {
		/////////////////////
		// move the sample //
		/////////////////////
		$newpid = $_POST['newpid'];
		if ($newpid == '') {
			echo "error: no new project selected";
			exit();
		}
		# new pid details
		$query = mysql_query("SELECT userID, naam, collection FROM project WHERE id = $newpid");
		$row = mysql_fetch_array($query);
		$newpname = $row['naam'];
		$newpown = $row['userID'];
		//$updatetfp = $_POST['updatetfp'];
		// new pid samples
		$query = mysql_query("SELECT idsamp FROM `projsamp` WHERE idproj = '$newpid'");
		$nps = array();
		while ($row = mysql_fetch_array($query)) {
			$nps[$row['idsamp']] = 1;
		}
		// get original project details
		$query = mysql_query("SELECT chiptype, chiptypeid, collection,naam FROM `project` WHERE id = '$oldpid'");
		
		$row = mysql_fetch_array($query);
		$chip = $row['chiptype'];
		$chipid = $row['chiptypeid'];
		$pcol = $row['collection'];
		$opname = $row['naam'];
		// submitted sample track-from-project details for oldpid.
		/*
		$sidstring = implode(",",$_POST['sids']);
		$query = mysql_query("SELECT s.id FROM `sample` WHERE intrack = 1 AND trackfromproject = '$oldpid' AND s.id IN ($sidstring)");
		$intrack = array();
		while ($row = mysql_fetch_array($query)) {
			$intrack[$row['id']] = 1;
		}
		*/
		// some sample details for nice printout
		/*
		$query = mysql_query("SELECT chip_dnanr, gender FROM sample WHERE id = '$sid'");
		$row = mysql_fetch_array($query);
		$samplename = $row['chip_dnanr'];
		$gender = $row['gender'];
		*/
		echo "<div class=sectie>";
		echo "<h3>Samples Moved to Project '$newpname'</h3>";
		$skipped = array();
		$ok = array();
		foreach ($_POST['sids'] as $k => $sid) {
			// get sample name
			$query = mysql_query("SELECT chip_dnanr FROM `sample` WHERE id = $sid");
			$row = mysql_fetch_array($query);
			$sname = $row['chip_dnanr'];
			// check if not in target pid.
			if (isset($nps[$sid])) {
				$skipped[$sname] = 1;
				continue;
			}
			// ok. 
			// add moving event to the log.
			$query = "INSERT INTO log (sid,pid,aid,uid,entry,arguments) VALUES ('$sid','$newpid','0','$userid','moved from project \'".addslashes($opname)."\' to \'$newpname\'','')";
			mysql_query($query);

			// ACTUALLY MOVE THE SAMPLE
			mysql_query("UPDATE aberration SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE aberration_mosaic SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE aberration_LOH SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE BAFSEG SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			mysql_query("UPDATE deletedcnvs SET idproj = '$newpid' WHERE idproj = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE log SET pid = '$newpid' WHERE pid = '$oldpid' AND sid = '$sid'");	
			mysql_query("UPDATE log_mos SET pid = '$newpid' WHERE pid = '$oldpid' AND sid = '$sid'");	
			mysql_query("UPDATE parents_relations SET father_project = '$newpid' WHERE father_project = '$oldpid' AND father = '$sid'");
			mysql_query("UPDATE parents_relations SET mother_project = '$newpid' WHERE mother_project = '$oldpid' AND mother = '$sid'");
			mysql_query("UPDATE plots SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			mysql_query("UPDATE prioritize SET project = '$newpid' WHERE project = '$oldpid' AND sample = '$sid'");
			mysql_query("UPDATE projsamp SET idproj = '$newpid' WHERE idproj = '$oldpid' AND idsamp = '$sid'");
			mysql_query("UPDATE sample SET trackfromproject = '$newpid' WHERE trackfromproject = '$oldpid' AND id = '$sid'");
			mysql_query("UPDATE parents_upd SET spid = '$newpid' WHERE spid = '$oldpid' AND sid = '$sid'");
			mysql_query("UPDATE parents_upd SET fpid = '$newpid' WHERE fpid = '$oldpid' AND fid = '$sid'"); 
			mysql_query("UPDATE parents_upd SET mpid = '$newpid' WHERE mpid = '$oldpid' AND mid = '$sid'");
			$ok[$sname] = 1;
		}
		if (count($ok) > 0) {
			ksort($ok);
			echo "<p><span class=nadruk>The following samples were moved to the new project:</span></p>";
			echo "<div style='padding-left:1em;padding-bottom:2em;>";
			foreach($ok as $s => $v) {
				echo "<span style='float:left;width:16%'>$s</span>";
			}
			echo "</div>";
		}
		if (count($skipped) > 0) {
			ksort($skipped);
			echo "<p><span class=nadruk>The following samples were skipped, because they are already present in the target project:</span></p>";
			echo "<div style='padding-left:1em;padding-bottom:2em;>";
			foreach($skipped as $s => $v) {
				echo "<span style='float:left;width:16%'>$s</span>";
			}
			echo "</div>";
		}
		echo "<p><span class=nadruk>Update Permissions:</span></p>";
		echo "<p style='padding-left:1em;'>Moving samples might have implications on permissions for the users and groups listed below. Please specify the access details below if needed by clicking on the provided options. For more complicated changes, please use the 'share project' options from the target project.</p>";
		echo "<p><table cellspacing=0 style='padding-left:1em;'>";
		// list groups with old permission and option to change it
		$donegroups = array();
		$groups = mysql_query("SELECT groupid FROM projectpermissiongroup WHERE projectid = $oldpid OR projectid = $newpid");
		if (mysql_num_rows($groups) > 0) {
			echo "<tr><th colspan=4 $firstcell>User Groups</th></tr>";
			echo "<tr><th class=specalt $firstcell>Group Name</th><th class=specalt>Access Type</th><th class=specalt>Source Project</th><th class=specalt>Target Project</th></tr>";
			while ($gr = mysql_fetch_array($groups)) {
				$groupid = $gr['groupid'];
				if ($donegroups[$groupid] == 1) {
					continue;
				}
				$donegroups[$groupid] = 1;
				## get group permissions
				$gpq = mysql_query("SELECT editclinic, editcnv, editsample, name FROM usergroups WHERE id = '$groupid'");
				$gpr = mysql_fetch_array($gpq);
				$editcnv = $gpr['editcnv'];
				$editclinic = $gpr['editclinic'];
				$editsample = $gpr['editsample'];
				$gname = $gpr['name'];
				if ($editcnv == 1 && $editclinic == 1) {
					$perm = 'Full';
				}
				elseif ($editcnv == 1) {
					$perm = 'CNV';
				}
				elseif ($editclinic == 1) {
					$perm = 'Clinic';
				}
				else {
					$perm = 'Read-only';
				}
				if ($editsample == 1 ) {
					$perm .= " / Project Control";
				}
				$check = mysql_query("SELECT groupid FROM projectpermissiongroup WHERE projectid = '$oldpid' AND groupid = '$groupid'");
				if (mysql_num_rows($check) > 0) {
					$oldacc = 1;
				}
				else {
					$oldacc = 0;
				}
				$check = mysql_query("SELECT groupid FROM projectpermissiongroup WHERE projectid = '$newpid' AND groupid = '$groupid'");
				if (mysql_num_rows($check) > 0) {
					$newacc = 1;
				}
				else {
					$newacc = 0;
					if ($newpown == $userid) {
						$addacc = "<span  style='float:right' title='Give Access To This Group' onclick='addaccess($groupid,$newpid)'><img src='images/bullets/plus-icon.png' width=8px height=8px></span>";
					}
					else {
						$addacc = '';
					}
				}
				#$yesno = array("1" => 'Yes', '0' => 'No');
				echo "<tr><th class=specalt $firstcell NOWRAP>$gname</th><td>$perm</td><td>$yesno[$oldacc]</td><td><span id=add$groupid>$yesno[$newacc] $addacc</span></td></tr>";
			}
			#echo "</table>";
		
		}
		// seperate users
		echo "</table></p>";
		echo "<p><table cellspacing=0>";
		$users = mysql_query("SELECT u.FirstName, u.LastName, userid,editcnv, editclinic, editsample, projectid FROM projectpermission pp JOIN users u ON pp.userid = u.id WHERE projectid = $oldpid OR projectid = $newpid");
		if (mysql_num_rows($users) > 0) {
			$doneusers = array();	
			echo "<tr><th colspan=4 $firstcell>Seperate Users</th></tr>";
			echo "<tr><th class=specalt $firstcell>User Name</th><th class=specalt>Source Project Permission</th><th class=specalt>Target Project Permission</th><th class=specalt>Transfer Permission</tr>";
			while ($ur = mysql_fetch_array($users)) {
				$thisuserid = $ur['userid'];
				if ($doneusers[$thisuserid] == 1) {
					# user done before
					continue;
				}
				$doneusers[$thisuserid] = 1;
				$thisusername = $ur['FirstName'] . " " . $ur['LastName'];
				## get group permissions
				$editcnv = $ur['editcnv'];
				$editclinic = $ur['editclinic'];
				$editsample = $ur['editsample'];
				$project = $ur['projectid'];
				if ($editcnv == 1 && $editclinic == 1) {
					$perm = 'Full';
				}
				elseif ($editcnv == 1) {
					$perm = 'CNV';
				}
				elseif ($editclinic == 1) {
					$perm = 'Clinic';
				}
				else {
					$perm = 'Read-only';
				}
				if ($editsample == 1 ) {
					$perm .= " / Project Control";
				}
				## check other project permission for current user.
				if ($project == $oldpid) {
					$oldperm = $perm;
					$check = mysql_query("SELECT editcnv, editclinic, editsample FROM projectpermission WHERE projectid = $newpid AND userid = $thisuserid");
					if (mysql_num_rows($check) > 0) {
						$cr = mysql_fetch_array($check);
						$editcnv = $cr['editcnv'];
						$editclinic = $cr['editclinic'];
						$editsample = $cr['editsample'];
						$project = $cr['projectid'];
						if ($editcnv == 1 && $editclinic == 1) {
							$perm = 'Full';
						}
						elseif ($editcnv == 1) {
							$perm = 'CNV';
						}
						elseif ($editclinic == 1) {
							$perm = 'Clinic';
						}
						else {
							$perm = 'Read-only';
						}
						if ($editsample == 1 ) {
							$perm .= " / Project Control";
						}
						$newperm = $perm;
					}
					else {
						$newperm = 'none';
					}
				}
				else {
					$newperm = $perm;
					$check = mysql_query("SELECT editcnv, editclinic, editsample FROM projectpermission WHERE projectid = $oldpid AND userid = $thisuserid");
					if (mysql_num_rows($check) > 0) {
						$cr = mysql_fetch_array($check);
						$editcnv = $cr['editcnv'];
						$editclinic = $cr['editclinic'];
						$editsample = $cr['editsample'];
						$project = $cr['projectid'];
						if ($editcnv == 1 && $editclinic == 1) {
							$perm = 'Full';
						}
						elseif ($editcnv == 1) {
							$perm = 'CNV';
						}
						elseif ($editclinic == 1) {
							$perm = 'Clinic';
						}
						else {
							$perm = 'Read-only';
						}
						if ($editsample == 1 ) {
							$perm .= " / Project Control";
						}
						$oldperm = $perm;
					}
					else {
						$oldperm = 'none';
					}

				}
				#print user
				if ($oldperm != $newperm) {
					$addacc = "<span id=adduser$thisuserid><a href='javascript:void()' style='font-size:10px' onclick='addaccessuser($thisuserid,$oldpid,$newpid)'>$newperm => $oldperm</a></span>";
				}
				else {
					$addacc = '/';
				}
				echo "<tr><th class=specalt $firstcell NOWRAP>$thisusername</th><td id=oldperm$thisuserid>$oldperm</td><td id=newperm$thisuserid>$newperm </td><td>$addacc</td></tr>";		

			}
			
		}
		echo "</table>";
		
		// store newpid in recent
		$recentq = mysql_query("SELECT RecentTargetProjects FROM usersettings WHERE id = '$userid'");
		$recent = mysql_fetch_array($recentq);
		$rp = explode(":",$recent['RecentTargetProjects']);
		foreach ($rp as $key => $link)
		{
    			if ($rp[$key] == '')
    			{
        			unset($rp[$key]);
    			}
		}
		if (!in_array($newpid,$rp)) {
			if (count($rp) > 5) {
				array_shift($rp);
			}
			array_push($rp,$newpid);
		}
		$newrecent = implode(":",$rp);
		mysql_query("UPDATE `usersettings` SET RecentTargetProjects = '$newrecent' WHERE id = '$userid'");
		// form to go back to the updated sample
		echo "</div>";
		echo "<div class=sectie>";
		echo "<h3>Sample was successfully moved</h3>";
		echo "<p>You will need to refresh some of the pages you were visiting to update all the information. To go back to the updated sample details page, click <a href='index.php?page=details&sample=$sid&project=$newpid'>here</a>.</p>";
		echo "</div>";

		## and exit
		exit();
	}
}
##############
# PRINT FORM #
##############

// what chip are we looking at?
$query = mysql_query("SELECT chiptypeid, collection, naam, chiptype, created, u.FirstName, u.LastName FROM project p JOIN users u ON p.userID = u.id WHERE p.id = '$pid'");
$row = mysql_fetch_array($query);
$chipid = $row['chiptypeid'];
$pcol = $row['collection'];
$chip = $row['chiptype'];
$created = preg_replace('/(.*)(\s-\s.*)/','$1',$row['created']) . " by ". $row['FirstName'] . " " . $row['LastName'];
$pname = $row['naam'];

$query = mysql_query("SELECT id, chip_dnanr FROM sample s WHERE s.id = '$sid'");
$row = mysql_fetch_array($query);
$sample = $row['chip_dnanr'];

// what projects is this sample in? 
/*
$query = mysql_query("SELECT idproj FROM projsamp WHERE idsamp = '$sid'");
$pidstring = '';
while ($row = mysql_fetch_array($query)) {
	$idproj = $row['idproj'];
	$pidstring .= "$idproj,";
}
$pidstring = substr($pidstring,0,-1);
// was this project data included in track for this sample? 
$query = mysql_query("SELECT trackfromproject, chip_dnanr FROM sample WHERE id = '$sid'");
$row = mysql_fetch_array($query);
$sample = $row['chip_dnanr'];
if ($row['trackfromproject'] == $pid) {
	$updatetfp = 1;
}
else {
	$updatetfp = 0;
}
*/
// print intro
echo "<div class=sectie>";
echo "<h3>Move $sample (and/or others) to a different project</h3>";
echo "<p>Samples can only be moved between projects with the same chiptype that do not yet contain the sample, or to a new project. If available, select a project below, or provide a name for the new project. <span class=nadruk>NOTE:</span> Selected samples that are present in the target project will NOT be moved (you will be notified).</p>";
echo "<form action='index.php?page=movesample' method=POST>";
echo "<p><span class=bold>Source Details</span>";
// source project details.
echo "<ul id=ul-simple style='padding-left:1em;'><li> - Project : <span class=italic>$pname</span></li><li> - Chiptype: <span class=italic>$chip</span></li><li> - Created: <span class=italic>$created</span></li></ul></p>";
// select the samples.
echo "<p><span class=bold>Select Samples </span></p><div style='padding-left:1em;padding-bottom:1.5em;'>";
$query = mysql_query("SELECT s.chip_dnanr,s.id FROM `sample` s JOIN `projsamp` ps ON s.id = ps.idsamp WHERE ps.idproj = '$pid' ORDER BY chip_dnanr");
while ($row = mysql_fetch_array($query)) {
	if ($row['id'] == "$sid") {
		$c = 'CHECKED';
	}
	else {
		$c = '';
	}
	echo "<span style='float:left;width:17%'><input type='checkbox' name='sids[]' value='".$row['id']."' $c> ".$row['chip_dnanr']."</span>";
}
echo "</div>";
// select target projects
echo "<p><span class=bold>Select a target project</span></p>";

echo "<input type=hidden name=oldpid value=$pid>";
// option 0: recent target projects
$query = mysql_query("SELECT RecentTargetProjects FROM `usersettings` WHERE id = '$userid'");
$row = mysql_fetch_array($query);
$rp = str_replace(":",",",$row['RecentTargetProjects']);
$recent = '';
if ($rp != '') {
	$query = mysql_query("SELECT p.id, p.naam, p.collection, u.FirstName, u.LastName FROM `project` p JOIN `users` u JOIN `projectpermission` pp ON p.userID = u.id AND pp.projectid = p.id WHERE p.chiptypeid = '$chipid' AND p.id != '$pid' AND p.id IN ($rp) AND pp.userid = '$userid' ORDER BY p.collection, p.naam");
	while ($row = mysql_fetch_array($query)) {
		$idproj = $row['id'];
		$pname = $row['naam'];
		$pcol = $row['collection'];
		$creator = $row['FirstName'] . ' ' .$row['LastName'];
		$recent .= "<tr>";
		$recent .= "<td $firstcell><input type=radio name=newpid value=$idproj></td>";
		$recent .= "<td>$pname</td>";
		$recent .= "<td>$pcol</td>";
		$recent .= "<td>$creator</td>";
		$recent .= "</tr>";
	}
	echo "<p><table cellspacing=0 style='padding-left:1em;'>";
	echo "<tr>";
	echo "<th class=topcellalt $firstcell>Select</th><th class=topcellalt>Project</th><th class=topcellalt>Collection</th><th class=topcellalt>Created By</th>";
	echo "</tr>";
	echo "<tr><th class=topcell $firstcell colspan=4>Recent Target Projects</th></tr>";
	echo $recent;
	echo "</table></p>";
	echo "<p><input type=submit class=button name=MoveSid value='Move Sample'> &nbsp; <input type=submit class=button name=Cancel value='Cancel'></p>";
	
}


// option 1 : what projects exist for this chip (except the ones containing the sample)?
$query = mysql_query("SELECT p.id, p.naam, p.collection, u.FirstName, u.LastName FROM `project` p JOIN `users` u JOIN `projectpermission` pp ON pp.projectid = p.id AND p.userID = u.id WHERE p.chiptypeid = '$chipid' AND p.id != '$pid' AND pp.userid = '$userid' ORDER BY p.collection, p.naam");
if (mysql_num_rows($query) > 0) {
	echo "<p ><table cellspacing=0  style='padding-left:1em;'>";
	echo "<tr>";
	echo "<th class=topcellalt $firstcell>Select</th><th class=topcellalt>Project</th><th class=topcellalt>Collection</th><th class=topcellalt>Created By</th>";
	echo "</tr>";
	echo "<tr><th class=topcell $firstcell colspan=4>All Projects</th></tr>";
	$tablestarted = 1;
}
while ($row = mysql_fetch_array($query)) {
	$idproj = $row['id'];
	$pname = $row['naam'];
	$pcol = $row['collection'];
	$creator = $row['FirstName'] . ' ' .$row['LastName'];
	echo "<tr>";
	echo "<td $firstcell><input type=radio name=newpid value=$idproj></td>";
	echo "<td>$pname</td>";
	echo "<td>$pcol</td>";
	echo "<td>$creator</td>";
	echo "</tr>";
}
if ($tablestarted ==1) {
	echo "</table></p>";
	echo "<p><span class=bold>Or Create a new project</span></p>";
}
// option 2 : a new project
echo "<p><input type=radio name=newpid value=newproject> &nbsp; <input type=text name=newprojectname size=40> : New Project Name <br/>";
$query = mysql_query("SELECT id, name FROM collections ORDER BY name");
echo "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp &nbsp; Place the new project in : <select id=colselect name=newcollection onchange='showspan()' >";
while ($row = mysql_fetch_array($query)) {
	$cid = $row['id'];
	$cname = $row['name'];
	if ($cname == $pcol) {
		$selected = 'SELECTED';
	}
	else {
		$selected = '';
	}
	echo "<option value='$cname' $selected>$cname</option>";
}
echo "<option value='new'>New Collection</option></select>";
echo " <span id='newcolspan' style='display:none;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; => New Collection: <input type=text name=newcolname size=10 >&nbsp;Descr.<input type=text name=newcolcomment size=20></span>";

//option 3 : delete the sample
echo "<p><span class=bold> OR delete the sample</span></p>";
echo "<p><input type=radio name=newpid value=delete> &nbsp; <input type=text name=deletereason size=40> : Why will this sample be deleted ?</p>"; 
echo "<p><input type=submit class=button name=MoveSid value='Move Sample'> &nbsp; <input type=submit class=button name=Cancel value='Cancel'>";
echo "</form>";
echo "</p>";
echo "</div>";











}// end if loggedin
?>
