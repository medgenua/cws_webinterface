<?php
####################
# LAYOUT VARIABLES #
#################### 
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

#######################
# CONNECT TO DATABASE #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

####################
# GLOBAL VARIABLES #
####################
for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}

$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["x"] = "23";
$chromhash["y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

####################
# POSTED VARIABLES #
####################
if (isset($_GET['c'])) {
	$chr = $_GET['c'];
}
else {
	$chr = 1;
}
if (isset($_GET['region'])) {
	$region = $_GET['region'];
	$pieces = preg_split("/[:\-_+]/",$region);
	$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
	$chr = $chromhash[ $chrtxt ] ;
	$start = $pieces[1];
	$end = $pieces[2];
	$end = str_replace(",","",$end);
	$stop = $end;
	$start = str_replace(",","",$start);
}
else {
	$start = '';
	if (isset($_GET['start'])) {
		$start = $_GET['start'];
	}
	$stop = '';
	if (isset($_GET['stop'])) {
		$stop = $_GET['stop'];
	}
	$chrtxt = $chromhash[ $chr ];
}
$project = '';
if (isset($_GET['p'])) {
	$project = $_GET['p'];
}
$custom = '';
if (isset($_GET['cstm'])) {
	$custom = $_GET['cstm'];
}

# GET PROJECT DETAILS
if ($custom == 1) {
	$pq = mysql_query("SELECT naam FROM cus_project WHERE id = '$project'");
	$cuspart = "cus_";
}
else {
	$pq = mysql_query("SELECT naam FROM project WHERE id = '$project'");
	$cuspart = "";
}


$result = mysql_fetch_array($pq);
$projectname = $result['naam'];


#######################
## check permissions ##
#######################
$pp = mysql_query("SELECT editcnv FROM $cuspart"."projectpermission WHERE userid = '$userid' AND projectid = '$project'");
if (mysql_num_rows($pp) == 0) {
	echo "<div class=sectie>";
	echo "<h3>Access Denied</h3>";
	echo "<p>You don't have access to this project.</p>";
	echo "</div>";
	exit();
} 


echo "<div class=sectie id=testing >\n";
echo "<h3>Overview of Project '$projectname'</h3>\n";
echo "<p>Chromosome: "; 
for ( $i = 1; $i <= 22; $i += 1) {
	echo "<a href='index.php?page=overviewbrowse&amp;p=$project&amp;cstm=$custom&amp;c=$i'>$i</a> | \n";
 }
echo "<a href='index.php?page=overviewbrowse&amp;p=$project&amp;cstm=$custom&amp;c=23'>X</a> | \n";
echo "<a href='index.php?page=overviewbrowse&amp;p=$project&amp;cstm=$custom&amp;c=24'>Y</a>\n";
echo "</p>\n";
ob_start();
echo "</div>\n";

echo "<div class=sectie>\n";
if ($start != '' && $stop != '') {
	echo "<h3>Chromosome $chromhash[$chr] : $start - $stop</h3>\n";
}
else {
	echo "<h3>Chromosome $chromhash[$chr] </h3>\n";
}
echo "<p>\n";
echo "<div class=nadruk style='color:red;'>COLORS CODES UPDATED: </div>\n";
echo "<ul id=ul-simple><li>- Black Borders are placed around aberrations with a 'Diagnostic Class' of 1 or 2</li><li>- CNVs classified as class 4 are shown in a lighter shade.</li><li>- Unclassified or Class 3 CNVs are shown wihtout specific decorations.</li></ul>\n";
echo "</p><p>\n";

//include('inc_create_image.inc');
include('Browser/LoadBrowser.php');
echo "</p>\n";
echo "</div>\n";

?>
