<?php

// posted vars
foreach($_POST as $key => $value) {
	$_GET[$key] = $value;
}

$sid = $_GET['s'];
$psid = $_GET['ps'];
$parent = $_GET['w'];
$pid = $_GET['p'];
$action = isset($_GET['a']) ? $_GET['a'] : '';

// ARRAYS
$parents = array('f' => 'father', 'm' => 'mother');
$genders = array('f' => 'Male','m' => 'Female');


// sample details
$query = mysql_query("SELECT chip_dnanr FROM sample WHERE id = '$sid'");
$row = mysql_fetch_array($query);
$childname = $row['chip_dnanr'];
 
// parent details
if ($psid != 0 && $psid != '') {
	$query = mysql_query("SELECT chip_dnanr FROM sample WHERE id = '$psid'");
	$row = mysql_fetch_array($query);
	$parentname = $row['chip_dnanr'];
}
// print page headers
echo "<div class=sectie>";
echo "<h3>Managing parents for $childname</h3>";
// link to go back to sample
echo "<h4><a href='index.php?page=details&sample=$sid&project=$pid'>Go back to sample details.</a></h4>";

## check permissions
$query = mysql_query("SELECT editsample, editcnv FROM projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
$row = mysql_fetch_array($query);
if ($row['editsample'] == 0 && $row['editcnv'] == 0) {
	echo "<p><span class=bold>NOTE:</span> You don't have enough permissions to change parental information for $childname.</p>";
	echo "</div>";
	exit();
}

## PROCESS SUBMITTED FORM 
if (isset($_GET['addparent'])) {
	echo "<p><span class=bold>Processing parent update:</span></p>";
	$newpname = $_GET['newpname'];
	$newpsid = $_GET['newpsid'];
	$datatype_parent = '';
	$sampleinfopath = '';
	$valid = 1;
	$headercols = array();
	if ($newpsid != 0) {
		// probably a bit redundant but keep to test the main functionality
		// check if samplename is valid
		if ( $newpsid == '' ) {
			$query = mysql_query("SELECT s.`id`, s.`chip_dnanr`, s.`gender`, ps.`datatype`, ps.`sampleinfo_file` FROM `sample` s JOIN `projsamp` ps ON s.`id` = ps.`idsamp` WHERE s.`chip_dnanr` = '$newpname'");
		}
		else {
			$query = mysql_query("SELECT s.`id`, s.`chip_dnanr`, s.`gender`, ps.`datatype`, ps.`sampleinfo_file` FROM `sample` s JOIN `projsamp` ps ON s.`id` = ps.`idsamp` WHERE s.`id` = '$newpsid'");
		}

		if (mysql_num_rows($query) == 1) {
			$row = mysql_fetch_array($query);
			$newpsid = $row['id'];
			$newpname = $row['chip_dnanr'];
			$newgender = $row['gender'];
			$datatype_parent = $row['datatype'];
			$sampleinfopath = $row['sampleinfo_file'];
			if ($newgender != $genders[$parent]) {
				$valid = 0;
				echo "<p>Gender mismatch. Provided samplename is unique but gender is incorrect</p>";
			}
		}
		else {
			$valid = 0;
			echo "<p>Invalid samplename or sample ID. Sample name is either non-unique, or non-existing.</p>";
		}
	}

	else {
		// Delete the relation
		$valid =0;
		echo "<p> => Deleting " . $parentname . " of $childname</p>";
		// first check mendelian error entries
		$query = mysql_query("SELECT mendelian_errors, father, mother FROM parents_relations WHERE id = '$sid'");
		$row = mysql_fetch_array($query);
		$menderrs = $row['mendelian_errors'];
		if ($row['mendelian_errors'] != '') {
			$mes = explode('@@@',$row['mendelian_errors']);
			$me = $mes[0];
		}
		else {
			$me = '';
		}
		// update if parent ID matches
		if ($parent == 'f' && $psid == $row['father']) {
			if ($me == 'Non-Paternity') {
				$menderrs = '';
			}
			$query = mysql_query("UPDATE parents_relations SET father = '0', father_project = '0', mendelian_errors = '$menderrs' WHERE id = '$sid'");
		}
		elseif ($parent = 'm' && $psid == $row['mother'] ) {
			if ($me == 'Non-Maternity') {
				$menderrs = '';
			}
			$query = mysql_query("UPDATE parents_relations SET mother = '0', mother_project = '0', mendelian_errors = '$menderrs' WHERE id = '$sid'");
		}
		// delete CNV relations
		mysql_query("DELETE FROM parent_offspring_cnv_relations WHERE parent_sid = '$psid'");
		mysql_query("DELETE FROM parents_regions WHERE sid = '$psid'");
		
		// delete upd data, as both parents are needed to be informative.
		mysql_query("DELETE FROM parents_upd_datapoints WHERE sid = '$psid'");
		mysql_query("DELETE FROM parents_upd WHERE sid = '$sid'");	
		// delete non-cnv datapoints for parent
		mysql_query("DELETE FROM parents_datapoints WHERE sid = '$psid'");
		
	} 

	if ($valid == 1) {
		// write pedigree file
		$target = $config['DATADIR']; //"$scriptdir/datafiles/";
		$random = rand();
		$pedigree = $target . "ped_$random.pedfile" ;
		$fh = fopen($pedigree,'w');
		fwrite($fh,"Child\tParent1\tParent2\n");
		fwrite($fh,"$childname\t$newpname\t\n");
		fclose($fh);
		// check and upload datafile
		$datafile = $_FILES['datafile'];
		// $usedata = 0;
		$nrswgsdata = -1;
		$nrsnpdata = -1;
		$swgssamples = array();
		$data = array();

		// either sWGS data
		if ($datatype_parent == 'swgs') {
			error_log($sampleinfopath);
			error_log($newpname);
			if ($sampleinfopath != '' and file_exists($sampleinfopath)) {
				// get swgs data
				$fhswgs = fopen("$sampleinfopath", "r");
				$headerline = fgets($fhswgs);
				while(!feof($fhswgs)) {
					$swgsline = fgets($fhswgs);
					$swgsline = rtrim($swgsline);
					$cols = explode("\t",$swgsline);
					
					if ($cols[0] == $newpname) {
						$sampleinfodir = dirname($sampleinfopath);
						$swgsdatafile = $sampleinfodir . '/' . $cols[15];
						$destinationfile = $target . "ped_$random.swgsdatafile.0";
						array_push($swgssamples, $newpname);
						system("gunzip -c $swgsdatafile > $destinationfile && chmod 777 $destinationfile");
						$nrswgsdata = 0;
						break;
					}
				}
			}
		}

		// or SNP data (cant be both)
		else {
			if (basename( $datafile['name']) == "") {
				echo " => No datafiles specified.<br/>";
			}
			else {
				$thefile = basename( $datafile['name']);
				$thefile = str_replace(' ','_',$thefile);
				$data[0] = $target . "ped_$random.snpdatafile.0" ;
				$datatypes[0] = $datafile['type'];
				// tmp_name is PHP built-in for $_FILES
				if(move_uploaded_file($datafile['tmp_name'], $data[0])) {
					$fh = fopen($data[0], 'r');
					$headline = fgets($fh);
					$headline = rtrim($headline);
					fclose($fh);
					$headercols = array_merge($headercols, explode("\t",$headline));
					$nrsnpdata = 0;
					echo "Datafile of parent '$newpname' is uploaded for analysis<br/>\n";
				}
				else {
					echo "upload of $thefile failed ! (will be discarded)<br/>";
				}
			}

			if ($nrsnpdata == 0) {
				$colnames = array("Log R Ratio", "B Allele Freq", "GType");
				// $shortvals = array("Log R Ratio" => 'LogR', "B Allele Freq" => 'BAF', "GType" => 'GType');
				foreach ($colnames as $key => $value) {
					$needle = $newpname . ".$value";
					if (in_array($needle,$headercols)) {
						// just checking
					}
					else {
						echo "'$value'-Data missing from datafile for $newpname. The datafile will not be used!<br/>"; 
						$nrsnpdata = -1;
					}
				}
			}

			if ($nrsnpdata == 0) {
				system("(chmod 777 $target"."ped_$random.snpdatafile.0) > /dev/null");
				system("(echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $target && dos2unix -n 'ped_$random.snpdatafile.0' 'ped_$random.snpdatafile.0.new' && mv 'ped_$random.snpdatafile.0.new' 'ped_$random.snpdatafile.0' && sed '1n; s/,/\./g' 'ped_$random.snpdatafile.0' > '/tmp/$random.snpdatafile.0' && mv '/tmp/$random.snpdatafile.0' 'ped_$random.snpdatafile.0'\") > /dev/null");
			}
		}
		$swgssamples = implode(",", $swgssamples);

		$command = "(chmod 777 $pedigree && echo \"1\" > status/status.$random.ped && echo $scriptpass | sudo -u $scriptuser -S bash -c \"cd $target && dos2unix -n '$pedigree' '$pedigree.new' && mv '$pedigree.new' '$pedigree' && cd $scriptdir && ./Bin/addped.pl -r '$random' -m '$nrsnpdata' -n '$nrswgsdata' -s '$swgssamples' > output/addped.$random.out\" && echo \"0\" > status/status.$random.ped && chmod a+rw status/status.$random.ped ) > /dev/null & ";
		// error_log($command);
		// exit;
		system($command);
		echo "<p>The analysis output below will refresh every 10 seconds</p>\n";
		$outfile = "$scriptdir/output/addped.$random.out";
		$statusfile = "status/status.$random.ped";
		echo "<script type='text/javascript'>var intervalresults;intervalresults = setInterval(\"reloadresults('AlgoOutput','$outfile')\",10000)</script>\n";
		echo "<p><pre id='AlgoOutput' class=scrollbarbox style='height:125px;'>Loading ...</pre>";
		echo "<script type='text/javascript'>var intervalstatus;intervalstatus = setInterval(\"checkstatus('$statusfile')\",10000)</script>\n";
		echo "</div><div class=sectie id=formdiv style='display:none'>";
	}
	else {
		echo "</div><div class=sectie id=formdiv >";
	}

}
## PRINT ACTION OPTIONS 
echo "<p><span class=bold>Specify new parental sample:</span></p>";
if ($psid != 0 && $psid != '') {
	echo "<p>Currently, $parentname is assigned as the ". $parents[$parent] . " of $childname. When a different sample is selected here, all regions that are present in the database for the new parent will be checked against the CNVs present in the child. </p>";
echo "<p>If you want a full update of corresponding regions, you should also provide a datafile containing the LogR/BAF/GT info for the new parent. To remove the parents-offspring link, type 'NONE' or 'DELETE' as parent in the text field below.</p>";
}
else {
	echo "<p>When a sample is selected here as ". $parents[$parent] . " of $childname, all regions that are present in the database for this parent will we checked against the CNVs present in the child. If you want a full update of corresponding regions, you should also provide a datafile containing the LogR/BAF/GT info for the parent.</p>";
}

//INPUT FORM 

echo "<form action='index.php?page=parents' enctype='multipart/form-data' method=POST>\n";
#echo "<input type=hidden name=page value='parents'>";
echo "<input type=hidden name=s value='$sid'>";
echo "<input type=hidden name=p value='$pid'>";
echo "<input type=hidden name=ps value='$psid'>";
echo "<input type=hidden name=w value='$parent'>";
echo "<input type=hidden name=newpsid id=newpsid value=''>\n";
echo "<table cellspacing=0>";
echo "<tr><td class=clear><input type=text id=newpname name='newpname' size=40></td><td class=clear> Type the sample name that should be assigned as ". $parents[$parent] . " of $childname : </td></tr>"; 
echo "<tr><td class=clear><input type='file' name='datafile' size='40'/></td><td class=clear>Provide datafile for parental sample (optional).</td></tr></table>";

echo "<p><input type=submit class=button name='addparent' value='Connect Parent'></p>";
echo "</form>";
echo "<p><span class=italic>NOTE: Only ". $genders[$parent] . " samples are available for selection.</span></p>";
echo "</div>";
 



?>

<!-- Start autocomplete function -->
<script type='text/javascript'>

var options = {
	script:"sample_suggestions.php?json=true&limit=10&gender=<?php echo $genders[$parent]; ?>&",
	varname:"input",
	json:true,
	shownoresults:true,
	delay:200,
	cache:false,
	timeout:10000,
	minchars:2,
	callback: function (obj) { document.getElementById('newpname').value = obj.value;document.getElementById('newpsid').value = obj.id; }
};
var as_json = new bsn.AutoSuggest('newpname', options);

</script>
