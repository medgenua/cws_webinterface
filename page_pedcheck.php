<?php 

function determineDatatype($samplename) {
	$sql = "SELECT ps.datatype, ps.sampleinfo_file FROM `projsamp` ps JOIN `sample` s ON s.`id` = ps.`idsamp` WHERE s.`chip_dnanr` = '$samplename' LIMIT 1";
	$query = mysql_query("SELECT ps.datatype, ps.sampleinfo_file FROM `projsamp` ps JOIN `sample` s ON s.`id` = ps.`idsamp` WHERE s.`chip_dnanr` = '$samplename' LIMIT 1");
	$row = mysql_fetch_array($query);
	if (mysql_num_rows($query) == 0) {
		return [null, null];
	}
	$datatype = $row['datatype'];
	if ($datatype && $datatype == 'swgs') {
		return ['swgs', $row['sampleinfo_file']];
	}
	else {
		return ['array', null];
	}
}


$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username/"; // "$sitedir/uploads/";
echo "<form action=\"addped.php\" method=\"post\">\n";
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
ob_start();
echo "<div class=sectie>\n";
echo " <h2>Checking Pedigree Information</h2>\n";
echo " <p>Please check and confirm everything below before submitting the family information</p>\n";
echo "</div>\n";
$target = $config['DATADIR']; //"$scriptdir/datafiles/";
$uploadeddata = 0;
$ok = 1;
$random = rand();
$headercols = array();
// Pedigree file => MANDATORY  (exit script on failure);
if (basename( $_FILES['pedigree']['name']) != "") {
	if ($_POST['uploadedpedigree'] != "") {
		echo "<li style='color:red'>You can only specify one source for the Pedigree table!</li>\n";
		$ok = 0;
	}
	$pedigreefile = basename( $_FILES['pedigree']['name']);
	$pedigreefile = str_replace(' ','_',$pedigreefile);
	$pedigree = $target . "ped_$random.pedfile" ;
	$pedigree_type = $_FILES['pedigree']['type'];
	$usepedigree = 1;
	if (move_uploaded_file($_FILES['pedigree']['tmp_name'],$pedigree)) {
		echo "Pedigree file uploaded<br/>";
	}
	else {
		#$ok = 0;
		echo "Upload of pedigree file failed. please go back and try again.<br/>";
		exit();
	}
	

}
elseif ($_POST['uploadedpedigree'] != "") {
	$pedigreefile = $_POST['uploadedpedigree'];
	$spedigreefile = $pedigreefile;
	$pedigree = $target . "ped_$random.pedfile" ;
	if (!symlink($dir.$pedigreefile, $pedigree)) {
		echo "Copying pedigreefile from ftp to analysis location failed. Analysis will not start !<br/>";
		exit();
	}	
	$uploadedpedigree = 1;
	$pedigree_type = "text/plain";
	#echo "Using pedigree table from FTP";
}
else {
	#$ok = 0;
	echo "A pedigree file is mandatory ! please go back and select a file.<br/>";
	exit();
}

// DATA FILES
# new uploaded files
$datafiles = $_FILES['datafiles'];
$nrfiles = count($datafiles['name']);

$snpdata = array();
for($idx = 0; $idx < $nrfiles ; $idx++) {
	#echo "Idx $idx: ";
	if (basename( $datafiles['name'][$idx]) != "") {
		$thefile = basename( $datafiles['name'][$idx]);
		$thefile = str_replace(' ','_',$thefile);
		$snpdata[$idx] = $target . "ped_$random.snpdatafile.$idx" ;
		$datatypes[$idx] = $datafiles['type'][$idx];
		#echo "moving $thefile to destination (".$datafiles['tmp_name'][$idx] . " => " . $snpdata[$idx] .")<br/>";
		if(move_uploaded_file($datafiles['tmp_name'][$idx], $snpdata[$idx])) {
			#echo "$thefile uploaded.<br/>";
			$fh = fopen($snpdata[$idx], 'r');
			$headline = fgets($fh);
			$headline = rtrim($headline);
			fclose($fh);
			$headercols = array_merge($headercols, explode("\t",$headline));
		}
 		else {
			echo "upload of $thefile failed ! (will be discarded)<br/>";
  			$ok = 0;
 		}

		$usedata = 1;
	}
	else {
		#echo "empty basename<br/>";
	}
}
if (isset($_POST['uploadeddata']) && $_POST['uploadeddata'] != "") {
	$nrup = count($_POST['uploadeddata']);
	for ($i = 0; $i < $nrup; $i++) {
		$idx = count($snpdata); # + 1;
		$thefile = $_POST['uploadeddata'][$i];
		#$sdatafile = substr($thefile, (strlen($userid)+1));
		$snpdata[$idx] = $target . "ped_$random.snpdatafile.$idx";
		#$uploadeddata = 1;
		if(symlink($dir.$thefile,$snpdata[$idx])) {
			echo "Previously uploaded file included in analysis: $thefile<br>\n";
		}
		else {
			echo "Copying '$thefile' to analysis location failed. File will be discarded.<br>\n";
			unset($snpdata[$idx]);
			continue;
		}
		#echo "$thefile uploaded.<br/>";
		$fh = fopen($snpdata[$idx], 'r');
		$headline = fgets($fh);
		$headline = rtrim($headline);
		fclose($fh);
		$headercols = array_merge($headercols, explode("\t",$headline));
		

	}
}
if (count($snpdata) == 0) {
	$usedata = 0;
	echo "no datafiles specified.<br/>";
}
else {
	$nrfiles = count($snpdata);
	echo "$nrfiles SNP datafiles were uploaded for analysis<br/>\n";
}


// READ IN PEDIGREE FILE
$swgssamples = array();
$swgs_idx = 0;
if ($ok == 1 ) {
	$nrsamples = 0;
  	$fh = fopen("$pedigree","r") or die("Could not open pedigree file : $pedigree");
  	$line = fgets($fh);
  	//echo "$line<br>\n";
  	$line = rtrim($line);
	echo "<div class=sectie><h3>Samples:</h3>\n<h4>File ok...</h4>";
   	echo "<p><table cellspacing=0>\n <tr>\n  <th $firstcell colspan=3>Child</td>\n  <th colspan=3>File_Parent_1</td>\n  <th colspan=3>File_Parent_2</td>\n  <th NOWRAP>New ?</td>\n  <th>DB_Father</td>\n  <th>DB_Mother</td><th>Status</td></tr>\n";
	echo "<tr><th class=specalt $firstcell>ID</th><th class=specalt>Type</th><th class=specalt>Values</th><th class=specalt $firstcell>ID</th><th class=specalt>Type</th><th class=specalt>Values</th><th class=specalt $firstcell>ID</th><th class=specalt>Type</th><th class=specalt>Values</th><th class=specalt>&nbsp;</th><th class=specalt>&nbsp;</th><th class=specalt>&nbsp;</th><th class=specalt>&nbsp;</th></tr>";

	
	while(!feof($fh)) {
		$line = fgets($fh);
		$line = rtrim($line);
		$pieces = explode("\t",$line);
		if ($pieces[0] != "") {

			$nrsamples += 1;
			$query = mysql_query("SELECT id FROM sample WHERE chip_dnanr = '$pieces[0]'");
			$nrrows = mysql_num_rows($query);
			if ($nrrows == 0) {
				echo "<tr>\n";
				echo "  <td $firstcell>$pieces[0]</td>\n";
				echo "  <td colspan=5>Unknown sample ID. Offspring Sample ID has to be an existing sample! </td>\n";
				echo " <td>Skipping</td>\n";
				echo " </tr>\n";
				continue;
			}
			$row = mysql_fetch_array($query);
			$sampleid = $row['id'];
			$query = mysql_query("SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sampleid'");
				$row = mysql_fetch_array($query, MYSQL_ASSOC);
				$nrrows = mysql_num_rows($query);	 
				if ($nrrows == 0) {
					$indb = "<img src=\"images/bullets/accept.png\">";
				$dbfather = '';
				$dbmother = '';
				$status = "Full";
				}
				else { 
				$indb = "<img src=\"images/bullets/no.png\">";
				if ($row['father_project'] != 0) {
					$squery = mysql_query("SELECT chip_dnanr FROM sample WHERE id = '".$row['father']."'");
					$srow = mysql_fetch_array($squery);
					$dbfather = $srow['chip_dnanr'];
				}
				if ($row['mother_project'] != 0) {
					$squery = mysql_query("SELECT chip_dnanr FROM sample WHERE id = '".$row['mother']."'");
					$srow = mysql_fetch_array($squery);
					$dbmother = $srow['chip_dnanr'];
				}
				$status = "Update";
			}
			
			echo " <tr>\n";
			for ($x = 0; $x <= 2; $x++) {
				if (! isset($pieces[$x])) {
					echo "  <td $firstcell><img src=\"images/bullets/no.png\"></td>\n";
					echo "  <td $firstcell><img src=\"images/bullets/no.png\"></td>\n";
					echo "  <td $firstcell><img src=\"images/bullets/no.png\"></td>\n";
					continue;
				}
				$ind = $pieces[$x];
				
				// foreach ($pieces as $ind) {
				list($datatype, $sampleinfopath) = determineDatatype($ind);
				echo "  <td $firstcell>$ind</td>\n";
				echo "  <td $firstcell>$datatype</td>\n";

				$content = '';
				if ($datatype == 'array') {
					// echo "<td>";
					$colnames = array("Log R Ratio", "B Allele Freq", "GType");
					$shortvals = array("Log R Ratio" => 'LogR', "B Allele Freq" => 'BAF', "GType" => 'GType');
					# check available data in datafiles?
					foreach ($colnames as $key => $value) {
						$needle = $ind . ".$value";
						if (in_array($needle,$headercols)) {
							$content .= $shortvals[$value]."<br/>";
						}
					}
					if ($content == '') {
						echo "<td><img src=\"images/bullets/no.png\"></td>";
					}
					else {
						echo "<td><img src=\"images/bullets/accept.png\"></br>$content</td>";
					}
				}
				elseif ($datatype == 'swgs') {
					// parse sample info data if it exists (should be, unless it's deleted) -> then skip
					if ($sampleinfopath == '' or ! file_exists($sampleinfopath)) {
						echo "<td><img src=\"images/bullets/no.png\"></td>";
					}
					else {
						$fhswgs = fopen("$sampleinfopath","r");
						$headerline = fgets($fhswgs);
						while(!feof($fhswgs)) {
							$swgsline = fgets($fhswgs);
							$swgsline = rtrim($swgsline);
							$cols = explode("\t",$swgsline);
							if ($cols[0] == $ind) {
								$sampleinfodir = dirname($sampleinfopath);
								$swgsdatafile = $sampleinfodir . '/' . $cols[15];
								$destinationfile = $target . "ped_$random.swgsdatafile.$swgs_idx";
								array_push($swgssamples, $ind);
								system("gunzip -c $swgsdatafile > $destinationfile");
								echo "<td><img src=\"images/bullets/accept.png\"></br>LogR</br>Z</td>";
								$swgs_idx++;
								$usedata = 1;
								break;
							}
						}
					}
				}
			}

			echo "  <td><center>$indb</center></td>\n";
			echo "  <td>$dbfather</td>\n";
			echo "  <td>$dbmother</td>\n";
			echo "  <td>$status</td>\n";
			echo " </tr>\n";
		}
	}
	echo "</table>\n";
	echo "</p>";
}
else {
	echo "pedigree file not found?<br/>";
}
echo "<input type=hidden name=usedata value='$usedata'>\n";
$nrsnpdata = count($snpdata) - 1;
$nrswgsdata = count($swgs_idx) - 1;
$swgssamples = implode(",", $swgssamples);

echo "<input type=hidden name=\"nrsnpdata\" value=\"$nrsnpdata\">\n";
echo "<input type=hidden name=\"nrswgsdata\" value=\"$nrswgsdata\">\n";
echo "<input type=hidden name=\"swgssamples\" value=\"$swgssamples\">\n";
echo "<input type=hidden name=\"pedigree\" value='$pedigree'>\n";
echo "<input type=hidden name=random value=$random>\n";
echo "<input type=hidden value=\"$username\" name=username>\n";
if ($ok == 1) {
	echo "<p><input type=submit class=button value=Go></p>\n";
}
else {
	echo "<p>There were problems, please check and retry</p>\n";
}
echo "</form>\n";

?>
