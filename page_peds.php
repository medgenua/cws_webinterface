<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	
	include('login.php');
	echo "</div>";
	exit();
}
 
?>
<script type='text/javascript'>
var fieldidx = 1;
function additem() {
	fieldidx++;
		document.getElementById('items'+(fieldidx - 1)).innerHTML = '<br/>'+(fieldidx+1)+') <input type=file name=\'datafiles[]\' size=60 /><span id=items'+fieldidx+'> </span>\n';
}
</script>
<?php
///////////////////////////////////////////////////////
// LOAD THE FILE NAMES OF FILES IN THE FTP DIRECTORY //
///////////////////////////////////////////////////////
$ftpfiles = "";
// set ftp upload dir. /home/SCRIPTUSER/ftp-data/USERNAME/
$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username"; //$sitedir/uploads/";
$Files = array();
$It =  opendir($dir);
if ($It) {
	while ($Filename = readdir($It)) {
 		if ($Filename == '.' || $Filename == '..' )
  			continue;
 		//$LastModified = filemtime($dir . $Filename);
 		$Files[] = $Filename;
	}
	sort($Files);
	$num = count($Files) - 1 ;
	For($i=0;$i<=$num;$i += 1) {
		//$filename = substr($Files[$i], (strlen($userid)+1));
		$filename = $Files[$i];
		$ftpfiles .= "<option value=\"". $Files[$i]."\">$filename</option>\n";
	}
}



$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
echo "<div class=sectie>\n";
echo "<h3>Add Family Information Without Re-Analysing All Samples</h3>\n";
echo "<p>Family information can be added by providing a simple pedigree file. This file should contain three columns, specifying the child in the first column, and parents in column 2 and 3. A headerline is also needed. An example file can be found <a href='files/pedigree.example.txt' target='_blank'>here</a></p>\n";
echo "<p>The samplenames have to be equal to the samplenames used to analyse them. Based on the samplename, the data type (SNP array or sWGS) will also be determined.</p>\n";
echo "<p>The raw data of the sWGS samples will be fetched from the archive servers, for SNP array data, please provide the original data table in case you want to import them.</p>\n";
echo "</div>\n";

echo "<div class=sectie>";
echo "<form enctype='multipart/form-data' action='index.php?page=pedcheck' method=POST>";
echo "<p><table  cellspacing=0>";
echo "<tr>";
echo "  <th scope=col colspan=3 style='border-left: 1px solid #a1a6a4;'>Data:</th>";
echo "</tr>";

echo "<tr>";
echo "<th scope=row class=specalt rowspan=2>Pedigree information:</td>";
echo "<th scope=row class=specalt >Specify local file:</td>";
echo "<td class=alt><input type='file' name='pedigree' size='40' /> (<a href='files/pedigree.example.txt' target='_blank'>Example file</a>)</td>";
echo "</tr>";
echo "<tr><th scope=row class=specalt >Specify Uploaded file:</td>";
echo "<td class=alt><select name='uploadedpedigree' ><option value=''></option>";
echo $ftpfiles;
echo "</select>\n";
echo "</td>";
echo "<tr>";
echo "  <th scope=row class=specalt rowspan=2>Table of LogR and BAF:</td>";
echo "  <th scope=row class=specalt >Specify local file:</td>";
#if ($level == 3) {
$idx = 0;
echo "<td>";
// file input field
echo ($idx + 1) .") <input type=file name='datafiles[]' size=40 />";
// span to contain the next field and link to add a field
echo "<span id='items$idx'></span><i>(<a href='javascript:void(0)' onclick='additem();'>Extra File</a>)</i><br/>";
// update field index in javascript.
echo "<script type='text/javascript'>fieldidx=$idx;</script>";
echo "</td>";

echo "</tr>";
echo "<tr>";
echo "  <th scope=row class=specalt>Select uploaded file:</td>";
echo "  <td class=alt><select name='uploadeddata[]' multiple='multiple'><option value=''></option>";
echo $ftpfiles;
echo "</select>\n";

echo "  <a href='index.php?page=upload'>(Upload datafiles)</a>";
echo "  </td>";
echo "</tr>";
echo "</table>";
echo "</p><p>";
echo "<input type=submit class=button name=submit>";
echo "</p>";
echo "</form>";
echo "</div>";

?>
