<?php 

if ($loggedin != 1) {
	include('login.php');
	exit();
}

// SOME VARIABLES
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$checked = array("","checked");

//BUILD PAGE
echo "<div class=sectie>\n";
if (isset($_POST['submit'])) {
	$UserName = $_POST['uname'];
	$FirstName = $_POST['firstname'];
	$LastName = $_POST['lastname'];
	$email = $_POST['email'];
	$Affiliation = $_POST['affiliation'];
	if ($Affiliation == 'other') {
		$Affiliation = $_POST['NewAffiliation'];
		$newaffi = 1;
	}
	else {
		$newaffi = 0;
	}
	$password = $_POST['pass'];
	$query = "SELECT id FROM users WHERE id = '$userid' AND password=MD5('$password')";
	$auth  = mysql_query("$query"); 
	$error = "";
	if ($password == "") {
		$error = $error . "<li>Password needed to update settings</li>\n";
	}
	elseif (mysql_num_rows($auth) == 0) {
		$error = $error . "<li>Specified password incorrect for logged in user </li>\n";
	}
	if ($FirstName == "") {
		$error = $error . "<li>First name can not be empty</li>\n";
	}

	if ($LastName == "") {
		$error = $error . "<li>Last name can not be empty</li>\n";
	}
	if ($username == "") {
		$error = $error . "<li>Username can not be empty</li>\n";
	}
	if ($Affiliation == "") {
		$error = $error . "<li>Affiliation can not be empty</li>\n";
	}
	if (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") {
		$error = $error . "<li>You specified an invalid email address<li>\n";
	}

    if (isset($_POST['CreateAPIkey'])) {
        $apikey = generateRandomString(32);
        $result = mysql_query("SELECT id FROM `users` WHERE apikey = '$apikey'");
		$row = mysql_fetch_array($result);
		while (!empty($row)) {
            $apikey = generateRandomString(32);
            $row = mysql_query("SELECT id FROM `users` WHERE apikey = '$apikey'");
			$row = mysql_fetch_array($result);
		}
        // $apiExpDate = date('Y-m-d', (time() + 7 * 24 * 60 * 60));
    }
	// elseif (isset($_POST['ReNewAPI'])) {
    //     $apikey = $_POST['currentAPI'];
    //     switch ($_POST['ReNewAPI']) {
    //         case 'disable':
    //             $apiKey = '';
    //             $apiExpDate = '2000-01-01';
    //             $apiExpires = 1;
    //             break;
    //         case 'week':
    //             $apiExpDate = date('Y-m-d', (time() + 7 * 24 * 60 * 60));
    //             $apiExpires = 1;
    //             break;
    //         case 'forever':
    //             $apiExpDate = '2000-01-01';
    //             $apiExpires = 0;
    //             break;
    //     }
    // }	

	if ($error == "") {
		// inserting new affiliation if needed
		if ($newaffi == 1) {
			mysql_query("INSERT INTO affiliation (name) VALUES ('$Affiliation')");
			$affid = mysql_insert_id();
			$Affiliation = $affid;
		}
		//Updating entry
		$query = "UPDATE users SET username = '$UserName', FirstName = '$FirstName', LastName = '$LastName', Affiliation = '$Affiliation', email = '$email', apikey = '$apikey' WHERE id = '$userid'";
		//echo "<p>$query</p>\n";
		mysql_query($query);
		echo "<h3>Success !</h3>\n";
		echo "<p>Your user details were successfully updated.</p>\n";
		echo "</div>\n";
	}
	else {
		echo "<h3>Incorrect information provided !</h3>\n";
		echo "<p>There were some problems, so nothing has been updated. Please fix them and try again:\n<ol>\n";
		echo "$error";
		echo "</ol>\n";
		echo "</p></div>\n";
	}
}	
else {
	$result = mysql_query("SELECT u.LastName, u.FirstName, u.Affiliation, u.email, u.username, u.motivation, u.apikey FROM users u WHERE u.id = '$userid'");
	$row = mysql_fetch_array($result);
	$LastName = $row['LastName'];
	$FirstName = $row['FirstName'];
	$Affiliation = $row['Affiliation'];
	$email = $row['email'];
	$UserName = $row['username'];
	$apikey = $row['apikey'];
	echo "<h3>User Details</h3>\n";

	echo "<p>You can change all your personal details here. Change them below and press submit. It is always necessary to fill in all fields and to include your current password.</p>\n";
	echo "</div>\n"; 
	
}
// MAIN FORM
echo "<div class=sectie>\n";
echo "<h3>Personal details</h3>\n";
echo "<h4>*: required fields</h4>\n";
echo "<p>\n";
echo "<form action=index.php?page=personal method=POST>\n";
echo "<table cellspacing=0 >\n";
echo "<input type=hidden name=userid value=$userid>\n";
echo " <tr>\n  <th $firstcell $topcell[0]>Option</td>\n";
echo "  <th $topcell[1]>Value</td>\n </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0] $firstcell>Username (*)</th>\n";
echo "  <td $tdtype[0]><input type=text name=uname value=\"$UserName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[1] $firstcell>First Name (*)</th>\n";
echo "  <td $tdtype[1]><input type=text name=firstname value=\"$FirstName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0] $firstcell>Last Name (*)</th>\n";
echo "  <td $tdtype[0]><input type=text name=lastname value=\"$LastName\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[1] $firstcell>Email (*)</th>\n";
echo "  <td $tdtype[1]><input type=text name=email value=\"$email\" size=60></td>\n";
echo " </tr>\n";
echo " <tr>\n";
echo "  <th $thtype[0] $firstcell>Affiliation (*)</th>\n";
$query = mysql_query("SELECT name, id FROM affiliation ORDER BY name");
echo "  <td $tdtype[0] ><select name='affiliation'>";
while ($row = mysql_fetch_array($query)) {
	$aid = $row['id'];
	$aname = $row['name'];
	if ($aid == $Affiliation) {
		$selected = 'SELECTED';
	}
	else {
		$selected = '';
	}
	echo "  <option value=\"$aid\" $selected>$aname</option>\n";
}
echo "</select></td>\n";

echo " </tr>\n";
echo "</table></p>\n";

// API information //
echo "<h3>API Key</h3>";
echo "<p>The CNV-WebStore API allows you to access the system to perform data/sample imports</p>";
// case 1: api is active : show info && option to renew.
echo "<table cellspacing=0 style='margin-left:1em;'>\n";
if ($apikey != '' ) {
    echo "<tr><th class=clear>Current API-Key : </th><td class=clear>$apikey<input type=hidden name='currentAPI' value='$apikey'/></td></tr>";
} 
else {
    echo "<tr><td class=clear>Create API Key:</th><td class=clear><input type=checkbox name=CreateAPIkey></td></tr>";
}
echo "</table>";

echo "</div>\n";

echo "<div class=sectie>\n";
echo "<h3>Double check and submit</h3>\n";
echo "<p>\n";
echo "<table class=clear>\n";
echo "<tr>\n";
echo " <th class=clear>Password (*)</th>\n";
echo " <td class=clear><input type=password name=pass size=40 maxlength=20 /></td>\n";
echo "</tr>\n";
echo "</table>\n";
echo "</p><p>\n";
echo "<input type=submit class=button name=submit value=Submit>\n";
echo "</form>\n";
echo "</div>\n";

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}


?>
