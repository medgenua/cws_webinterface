
<?php
$username = $_SESSION['username'];
if ($loggedin != 1) {
	echo "<div class=sectie>\n";
	echo "<h3>qPCR Primerdesign</h3>\n";
	echo "<h4>Automagically design qPCR primers</h4>\n";
	include('login.php');
	echo "</div>\n";
}
else {
for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

    $tdtype= array("","class=alt");
  $thtype= array("class=spec","class=specalt");
  $topstyle = array("class=topcell","class=topcellalt");
  $firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
  mysql_select_db("primerdesign".$_SESSION['dbname']);

  ob_start();
?>

<?php
// DESIGN NEW PRIMERS 
if ($_GET['type'] == "design") {

   if ($level > 1) {
	//single region
	echo "<div class=sectie>\n";
	echo "<h3>Design qPCR-Primers By Region</h3>\n";
	echo "<h4>eg: Chr5:12,265,148-12,324,482</h4>\n";
	echo "<p>Enter your name and a genomic region in the fields below and click \"Start\".  Analysis will start with the exons, and continue in intron sequence if nothing is found. These entries have priority over batch jobs (see below).</p>\n";
	echo "<p><form action=findprimers2.php method=POST>\n";
	echo "<table class=clear>\n";
	echo "<tr class=clear>\n";
	echo "<td class=clear>Operator: </td><td class=clear><input type=text name=operator value=$firstname></td></tr>\n";
	echo "<tr class=clear><td class=clear>Region: </td><td class=clear><input type=text name=region></td></tr>\n";
	echo "<tr class=clear><td class=clear colspan=2>";
	echo "<input type=hidden name=submitter value=$userid>\n";
	echo "<input type=submit name=submit value=start class=button></td>\n";
	echo "</tr></table></form>\n";
	echo "</div>\n";
    }
	//batch processing
	echo "<div class=sectie>\n";
	echo "<h3>Submit Batch Jobs</h3>\n";
	echo "<h4>one region per line...</h4>\n";
	echo "<p>Batch jobs are started when there is idle CPU-time detected for 5 minutes.  Single region jobs (only for full user accounts), or other tasks on the server have priority, so use this function for less urgent regions.</p>\n";
	echo "<p>To start a batch job, enter one UCSC-style region per line in the field below, enter your name in the operator field and click \"start\".  You can go to the results via the \"Running processes\" tab. Please enter amplicons you have experimentally tested into the qPCR-PrimerDB accessible on the left.</p>\n";
	echo "<p><form action=findprimers2.php method=POST>\n";
	echo "<table class=clear>\n";
	echo "<tr class=clear>\n";
	echo "<td class=clear>Operator: </td><td class=clear align=right><input type=text name=operator value=$firstname></td></tr>\n";
	echo "<tr class=clear><td class=clear colspan=2>Enter regions \"one per line\" <br>\n";
	echo "<textarea name=batch rows=15 cols=52></textarea></td></tr>\n";
	echo "<tr class=clear><td class=clear colspan=2>";
	echo "<input type=hidden name=submitter value=$userid>\n";
	echo "<input type=submit name=submit value=start class=button></td>\n";
	echo "</tr></table></form>\n";
	echo "</div>\n";

}
// SEARCH DESIGNED PRIMERS 
elseif ($_GET['type'] == "search") {
        if (!empty($_POST['location'])) {
		$location = $_POST['location'] ;
	}
	else {
		if (!empty($_GET['location'])) {
			$location = $_GET['location'];
		}
		else{
			$location = "";
		}
	}	
	echo "<div class=sectie>\n";
	echo "<h3>Search in-silico designed Realtime Primers</h3>\n";
	echo "<p>Search primers based on chromosomal region.  Queries are made using UCSC location notation (eg: chr15:30000-7064200). More information on the found primers can be obtained by clicking the corresponsing row.</p> \n";
	echo "<form action=index.php?page=primerdesign&amp;type=search method=POST>\n";
	echo "<p><table id=mytable cellspacing=0>\n";
	echo "<tr class=clear>\n";
	echo " <th class=clear>Chromosomal Location</td>\n";
	echo " <td class=clear><input type=text name=location width=150px value=$location></td>\n";
	echo " <td class=clear><input type=submit class=button value=Search></td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</div>\n";
	if ($location <> "") {
		#$location = $_POST['location'];
		$pieces = preg_split("/[:\-_+(\.\.)]/",$location);
		$chrtxt = preg_replace("/chr(\w+)/","$1",$pieces[0]);
		$chr = $chromhash[ $chrtxt ];
		$start = $pieces[1];
		$end = $pieces[2];
		$end = str_replace(",","",$end);
		$start = str_replace(",","",$start);
		$query = mysql_query("SELECT chr, start, end, forward, reverse, remark, amplicon  FROM Primers WHERE chr = \"$chr\" AND (start >= \"$start\" AND end <= \"$end\") ORDER BY start");
		#echo "chrom $chr from $start to $end<br>\n";
		$numrows = mysql_num_rows($query);
		if ($numrows == 0 ) { 
			echo "<div class=sectie>\n";
			echo "<h3>No amplicons found for $location</h3>\n";
			echo "<p>You will have to start a design process yourself!</p>\n";
			echo "</div>\n"; 
		}
		else {
			echo "<div class=sectie>\n";
			echo "<h3> Available amplicons for $location</h3>\n";
			echo "<p>The results are ordered by starting position inside the target region.  When you click a row, a new window will open with some more details (amplicon sequence, etc). The thermodynamic values in the Gibbs column represent the Gibbs free energy for the predicted secondary structure at 60 degrees Celsius by MFold. Normally, postive free energy values don't lead to spontaneous reactions, but they are shown just for sure. </p>\n<p>";
			echo "<table cellspacing=0 width=100%>\n";
			echo " <tr>\n";
			echo "  <th $firstcell>Location</td>\n";
			echo "  <th>Forward</td>\n";
			echo "  <th>Reverse</td>\n";
			echo "  <th>Gibbs</td>\n";
			echo "  <th>Size</td>\n";
			echo " </tr>\n";
			$switch=0;
			while ($row = mysql_fetch_assoc($query)) {
				$url = "showprimers.php?chr=$chrtxt&amp;start=".$row['start']."&amp;end=".$row['end']; 
				$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.open('$url')\"";	
				echo " <tr $trstyle>\n";
				echo "  <td $tdtype[$switch] $firstcell>Chr" .$chromhash[ $row['chr'] ].":".number_format($row['start'],0,'',',')."-".number_format($row['end'],0,'',',')."</td>\n";
				echo "  <td $tdtype[$switch]>".$row['forward']."</td>\n";
				echo "  <td $tdtype[$switch]>".$row['reverse']."</td>\n";
				echo "  <td $tdtype[$switch]>".substr($row['remark'],0,strpos($row['remark'],'mol')+3)."</td>\n";
				echo "  <td $tdtype[$switch]>".strlen($row['amplicon'])."</td>\n";
				echo " </tr>\n";
				$switch = $switch + pow(-1,$switch);
				$found = 1;
			}
			echo "</table>\n";
			echo "</div>\n";
	
		}
	}

}

// MANAGE PROCESSES 
elseif ($_GET['type'] == "Running") {
   #if ($level > 1) {
	// TABLE OF RUNNING PROCESSES 
	echo "<div class=sectie>\n";
	echo "<h3>Running Primer Design Processes</h3>\n";
	echo "<h4>Be carefull not to end someone elses processes...</h4>\n";
	$dir = "status/";
	$Files = array();
	$It =  opendir($dir);
	if (! $It)
 		die('Cannot list files for ' . $dir);
	while ($Filename = readdir($It))
	{
 		if ($Filename == '.' || $Filename == '..' || !strstr($Filename,".pd.status") )
  			continue;
		$fstat = fopen("$dir$Filename",'r');
		$stat = fread($fstat,1);
		#echo "file $Filename : stat: $stat<br>";
		if ($stat != "1") 
			continue;
 		$LastModified = filemtime($dir . $Filename);
		$Files[] = array($Filename, $LastModified);
	}
	usort($Files, 'DateCmp');
	$num = count($Files) - 1 ;
	if ($num >= 0) {
		echo "<p> Below are processes currently (listed as) running on the server.";
		echo "  For each process, you can view the progress by going to the results page by clicking \"Progress\", ";
		echo "or if it was started by yourself, you can stop the analysis by clicking \"Stop\".  Keep in mind however that once you cancel an analysis ";
		echo "it will be marked as \"finished\" and resubmitting the same region will bring you to the unfinished results! </p>\n";
		echo "<p><table cellspacing=0>\n";
		echo " <tr>\n";
		echo " <th scope=col class=topcellalt $firstcell>Operator</th>\n";
		echo " <th scope=col class=topcellalt >Region</th>\n";
		echo " <th scope=col class=topcellalt >View Progress</th>\n";
		echo " <th scope=col class=topcellalt >Kill Process</th>\n";
		echo "</tr>\n";
		
		For($i=0;$i<=$num;$i += 1) {
			#$start = str_replace(",","",$start);
			$regionstring = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$1", $Files[$i][0]);
			$pieces=preg_split("/_/",$regionstring); 
			$chr = preg_replace("/chr(\w+)/", "$1", $pieces[0]);
			$start = $pieces[1];
			$end = $pieces[2];
			$operator = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$3", $Files[$i][0]);
			$uid = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$5", $Files[$i][0]);
			echo "<td $tdtype[$switch] $firstcell>$operator</td>\n";
			echo "<td $tdtype[$switch]>chr$chr:". number_format($start,0,'',',')."-". number_format($end,0,'',',')."</td>\n";
			echo "<td $tdtype[$switch]><a href=index.php?page=primerdesign&amp;type=result&amp;region=$regionstring&amp;operator=$operator&amp;uid=$uid>Progress</a></td>\n";
			if ($userid == $uid) {
				echo "<td $tdtype[$switch]><a href=endprimerdesign.php?region=$regionstring&amp;operator=$operator&amp;u=$uid>Stop</a></td>\n";
			}
			else {
				echo "<td $tdtype[$switch]>Stop</td>\n";
			}
			echo "</tr>\n";
			
		}
		echo "</table></div>\n";
	}
	else {
		echo "<p>There are currently no processes running...</p></div>\n";
	}
	// FINISHED PROCESSES
	echo "<div class=sectie>\n";
	echo "<h3>Finished Primer Design Processes</h3>\n";
	echo "<h4>View previous results...</h4>\n";
	$dir = "status/";
	$Files = array();
	$It =  opendir($dir);
	if (! $It)
 		die('Cannot list files for ' . $dir);
	while ($Filename = readdir($It))
	{
 		if ($Filename == '.' || $Filename == '..' || !strstr($Filename,".pd.status") )
  			continue;
		$fstat = fopen("$dir$Filename",'r');
		$stat = fread($fstat,1);
		#echo "file $Filename : stat: $stat<br>";
		if ($stat != "0") 
			continue;
		$uid = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$5", $Filename);
		if ($uid != $userid){
			#echo "$uid does not match $userid<br>"; 
			continue;
		}
 		$LastModified = filemtime($dir . $Filename);
		$Files[] = array($Filename, $LastModified);
	}
	usort($Files, 'DateCmp');
	$num = count($Files) - 1 ;
	if ($num >= 0) {
		echo "<p> Below are the most recent finished analysis results. ";
		echo "For each analysis, you can view the results by clicking \"Results\". \n";
		echo "Analysis output files are stored for approximately a month and are then deleted. Designed primers can still be accessed after that by searching on region.</p>";
		echo "<p><table cellspacing=0>\n";
		echo " <tr>\n";
		echo " <th scope=col class=topcellalt $firstcell>Operator</th>\n";
		echo " <th scope=col class=topcellalt >Region</th>\n";
		echo " <th scope=col class=topcellalt >Results</th>\n";
		echo "</tr>\n";
		For($i=0;$i<=$num;$i += 1) {
			#$start = str_replace(",","",$start);
			$regionstring = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$1", $Files[$i][0]);
			$pieces=preg_split("/_/",$regionstring); 
			$chr = preg_replace("/chr(\w+)/", "$1", $pieces[0]);
			$start = $pieces[1];
			$end = $pieces[2];
			$operator = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$3", $Files[$i][0]);
			$uid = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$5", $Files[$i][0]);
			echo "<td $tdtype[$switch] $firstcell>$operator</td>\n";
			echo "<td $tdtype[$switch]>chr$chr:". number_format($start,0,'',',')."-". number_format($end,0,'',',')."</td>\n";
			echo "<td $tdtype[$switch]><a href=index.php?page=primerdesign&amp;type=result&amp;region=$regionstring&amp;operator=$operator&amp;uid=$uid>Results</a></td>\n";
			echo "</tr>\n";
			
		}
		echo "</table></div>\n";
	}
	else {
		echo "<p>There are currently no finished projects to show...</p></div>\n";
	}
	// QUEUED BATCH PROCESSES 
	echo "<div class=sectie>\n";
	echo "<h3>Queued Batch Jobs</h3>\n";
	echo "<h4>To be processed...</h4>\n";
	$dir = "status/";
	$Files = array();
	$It =  opendir($dir);
	if (! $It)
 		die('Cannot list files for ' . $dir);
	while ($Filename = readdir($It))
	{
 		if ($Filename == '.' || $Filename == '..' || !strstr($Filename,".pd.status") )
  			continue;
		$fstat = fopen("$dir$Filename",'r');
		$stat = fread($fstat,2);
		#echo "file $Filename : stat: $stat<br>";
		if ($stat != "-1") 
			continue;
 		$LastModified = filemtime($dir . $Filename);
		$Files[] = array($Filename, $LastModified);
	}
	usort($Files, 'DateCmp');
	$num = count($Files) - 1 ;
	if ($num >= 0) {
		echo "<p> Below are queued jobs, submitted by the batch processing form.";
		echo " Aftar analysis they will appear in the \"Finished Processes\" table above. </p> ";
		echo "<p><table cellspacing=0>\n";
		echo " <tr>\n";
		echo " <th scope=col class=topcellalt $firstcell>Operator</th>\n";
		echo " <th scope=col class=topcellalt >Region</th>\n";
		echo " <th scope=col class=topcellalt >Submitted</th>\n";
		echo "</tr>\n";
		For($i=$num;$i>=0;$i -= 1) {
			#$start = str_replace(",","",$start);
			$regionstring = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$1", $Files[$i][0]);
			$pieces=preg_split("/_/",$regionstring); 
			$chr = preg_replace("/chr(\w+)/", "$1", $pieces[0]);
			$start = $pieces[1];
			$end = $pieces[2];
			$operator = preg_replace('/(.*)(\.)(\w+)(\.)(\d+)(\.pd\.status)/', "$3", $Files[$i][0]);
			echo "<td $tdtype[$switch] $firstcell>$operator</td>\n";
			echo "<td $tdtype[$switch]>chr$chr:". number_format($start,0,'',',')."-". number_format($end,0,'',',')."</td>\n";
			echo "<td $tdtype[$switch]>".date("l, dS F, Y @ h:ia", $Files[$i][1])."</td>\n";
			echo "</tr>\n";
			
		}
		echo "</table></div>\n";
	}
	else {
		echo "<p>There are currently no finished projects to show...</p></div>\n";
	}
   #}
   #else {
#	include('page_noguest.php');
   #}
}


// RESULTS
elseif ($_GET['type'] == "result") {
	include ("page_primerdesignresults.php");	
}

// DEFAULT 
else {
	echo "<div class=sectie>\n";
	echo "<h3>qPCR Primerdesign</h3>\n";
	echo "<h4>Automagically design qPCR primers</h4>\n";
	echo "<p>This page contains an automatic primerdesign module.  Using the the options above you can navigate the available functions. </p>\n";
	echo "<p>During design, there are multiple requirements to take into account.  For now, these parameters are predefined and cannot be changed. They include stringency for Blast, settings for Primer3 and cut-off values for the mfold algorithms. An overview will be added in the future.</p>\n";
	echo "</div>\n"; 
}

// CLOSE THE IF $user = cmg
}
?>	


