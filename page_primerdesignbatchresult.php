
<?php
$file = $_GET['data'];
$fh = fopen("data/$file", 'r');
$theData = fread($fh, filesize("data/$file"));
fclose($fh);
unlink($file);
$pieces = explode("&", $theData);
$good = $pieces[0];
$bad = $pieces[1];
$goods = explode("=",$good);
$bads = explode("=",$bad);
#$chr = preg_replace("/chr(\w+)/", "$1", $pieces[0]);
$finished = explode("finished=", $theData);
echo "<div class=sectie>\n";
unlink("$data/$file");
echo "<h3>Succesfull Primer Design Batch Job Submissions</h3>";
if ($goods[1] == "" && $finished[1] == "") {
	echo "<p>There were no succesfull submissions.  Please go back and check your region notation.\n";
	echo "A valid example would be Chr7:12,254,004-13,334,687</p>\n";
	
}
else {
	if ($finished[1] != '') {
		echo "<p>The following submitted regions were already analysed before. Click the links to show the results.</p>\n";
		echo "<p><ol>\n";
		echo "$finished[1]\n";
		echo "</ol></p>\n";
	}
	if ($goods[1] != '') {
		echo "<p>The following regions were succesfully submitted for processing. You can view the results by going to the \"Running Processes\" tab.</p>\n";
		echo "<p><ol>\n";
		echo "$goods[1]\n";
		echo "</ol></p>\n";
	}
}
echo "</div>\n";
if ($bads[1] != "") {
	echo "<div class=sectie>\n";
	echo "<h3>Some Problems Arose With Your Submission</h3>\n";
	echo "<p>The following regions were not submitted due to incorrect formatting.  Please check them and resubmit.  </p>\n";
	echo "<p><ol>\n";
	echo "$bads[1]\n";
	echo "</ol></p>\n";
	echo "</div>\n";
}



?>
