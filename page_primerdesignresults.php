<?php
// SET TABLE-STYLE variables
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$types['ex'] = "E";
$types['in'] = "I";
$icons = array("<img src=images/bullets/no.png>", "<img src=images/bullets/accept.png>");
// Connect to the database
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("primerdesign".$_SESSION['dbname']);
for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";




ob_start();
if ($_GET['region'] != "" && $_GET['operator'] != "" && $_GET['uid'] != "" ) {
	$operator = $_GET['operator'];
	$uid = $_GET['uid'];
	//if ($_GET['existed'] == "auto" ) {
	//	$operator = "automatic";
	//	$uid = 0;
	//}
	$region = $_GET['region'];
	
	$pieces=preg_split("/[:\-_+(\.\.)]/",$region); 
	$chrtxt = preg_replace("/chr(\w+)/", "$1", $pieces[0]);
	$chr = $chromhash[ $chrtxt ];
	$start = $pieces[1];
	$start = str_replace(",","",$start);
	$end = $pieces[2];
	$end = str_replace(",","",$end);
	$statusfile = "status/chr".$chrtxt."_".$start."_".$end.".$operator.$uid.pd.status";
	$outfile = "/opt/mkprimer/status/chr".$chrtxt."_".$start."_".$end.".output";
	$nrlines = count(file($outfile)) ;
	#echo "this is number of lines: $nrlines<br/>";
	$fstatus = fopen($statusfile,'r');
	$status = fread($fstatus,1);
	$tablename = "chr".$chrtxt."_".$start."_".$end;
	$query = mysql_query("SELECT ID, finished, submitter FROM Succeeded WHERE region='$tablename'");
	$row = mysql_fetch_assoc($query);
	$RegionID = $row['ID'];
	$finished = $row['finished'];
	$submitter = $row['submitter'];
	if ($status == "1" ) {
		echo "<meta http-equiv='refresh' content='15;URL=index.php?page=primerdesign&amp;type=result&amp;region=$region&amp;operator=$operator&amp;uid=$uid'>\n"; 
		echo "<div class=sectie><h3>Results for Chr$chrtxt:". number_format($start,0,'',',')."-". number_format($end,0,'',',')."</h3>\n";
	}
	else {
		#unlink($statusfile);
		echo "<div class=sectie><h3>Final Results for Chr$chrtxt:". number_format($start,0,'',',')."-". number_format($end,0,'',',')."</h3>\n";
	}	
	echo "<p>Below is an overview of the primerdesign in the submitted region.  When all steps are successful, clicking on the row will give you the designed primers.  Each exon and intron was extended during analysis by 150bp both upstream and downstream.  This makes it possible that exons analysis leads to primers completely inside intron sequence.  </p>\n";
	
	if ($status == "1") {
		echo "<p> If you have found suitable primers, the process can be stopped by clicking the \"End Analysis\" button.";
		echo " Keep in mind however that once you cancel an analysis ";
		echo "it will be marked as \"finished\" and resubmitting the same region will bring you to the unfinished results!</p>\n";
		echo "<p><form action=\"endprimerdesign.php?region=$region&amp;operator=$operator&amp;u=$submitter&amp;from=results\" method=GET><input type=submit class=button value=\"End Analysis\"></form></p>\n";
		echo "</div><div class=sectie><h3>Overview of the results so far...</h3>\n";
		echo "<h4>Remark: Introns are processed as fragments. X doesn't mean final failure</h4>\n";

	}
	else {
		echo "</div><div class=sectie><h3>Overview of the final results</h3>\n";
		echo "<p>\n";
		if ($finished == 0) {
			echo "<b>Remark:</b> The results are marked as unfinished !<br>\n";
		}
		if ($submitter != $userid) {
			mysql_select_db("$db");
			$uquery = mysql_query("SELECT FirstName, LastName FROM users WHERE id = '$submitter'");
			$row = mysql_fetch_array($uquery);
			$Fname = $row['FirstName'];
			$Lname = $row['LastName'];
			mysql_select_db("primerdesign".$_SESSION['dbname']);
			echo "<b>Remark:</b> This region was already submitted by $Fname $Lname.<br>\n";
		}
		//if ($_GET['existed
		echo "</p>\n";
	}
	if ($RegionID == 0 || $RegionID == "") {
		echo "<p>Starting up...</p>\n";
	}
	else {	
		$query = mysql_query("SELECT exin, start, end, primers, blast, primer3, optimal, mfold FROM Results WHERE region='$RegionID' ORDER BY exin ASC, mfold DESC, optimal DESC, primer3 DESC, blast DESC, primers DESC, start ASC");
		$nrrows = mysql_num_rows($query);
		if ($nrrows > 200) {
			echo "<p><span class=nadruk>NOTE: </span>Due to the large number of fragments, failed fragments are left out of the table. This means that only successful and non-finished exon fragments are shown. Failed exons and unfinished introns are skipped.</p>"; 
		}
		echo "<table cellspacing=0>\n";
		$switch=0;
		echo " <tr>\n"; 
		echo "  <th scope=col class=topcellalt $firstcell>Type</th>\n";
		echo "  <th scope=col class=topcellalt>Start (extended)</th>\n";
		echo "  <th scope=col class=topcellalt>End (extended)</th>\n";
		echo "  <th scope=col class=topcellalt>Suitable</th>\n";
		echo "  <th scope=col class=topcellalt>Unique</th>\n";
		echo "  <th scope=col class=topcellalt>Primer3</th>\n";
		echo "  <th scope=col class=topcellalt>Primers OK</th>\n";
		echo "  <th scope=col class=topcellalt>Mfold</th>\n";
		echo " </tr>\n";

		while ($row = mysql_fetch_assoc($query)) {
			$trstyle = "";
			$type = $row['exin'];
			$url = "";
			if ($row['mfold'] == 1 ) {
				$primerstart = $row['start']-150+$start-1;
				$primerend = $row['end']+150 +$start-1;
				$url = "showprimers.php?chr=$chrtxt&amp;start=".$primerstart."&amp;end=".$primerend."&amp;uid=$userid"; 
				$trstyle = "onmouseover=\"this.style.cursor='pointer'\"  onClick=\"window.open('$url')\"";	
     	   			#$trstyle = "<a href=\"http://www.google.be\"></a>"; 
			}  	
			elseif ($nrrows > 200) {		// for large regions, only print selection
				if ($row['exin'] == 'ex') {	// only print exons that are not finally failed 
					if ($row['primers'] == 0 || $row['blast'] == 0 || $row['primer3'] == 0|| $row['optimal'] == 0 || $row['mfold'] == 0) {
						continue;
					}
				}
				else {				// don't print introns without success
					continue;
				}
			}
			echo " <tr $trstyle>\n";
			echo "  <td $tdtype[$switch] $firstcell>". $types[$row['exin']]."</td>\n";
    			echo "  <td $tdtype[$switch] >".number_format($row['start'],0,'',',')." (".number_format($row['start']-150,0,'',',').") </td>\n";
   			echo "  <td $tdtype[$switch] >".number_format($row['end'],0,'',',')." (".number_format($row['end']+150,0,'',',').") </td>\n";
   			echo "  <td $tdtype[$switch] >".$icons[$row['primers']]."</td>\n";
			echo "  <td $tdtype[$switch] >".$icons[$row['blast']]."</td>\n";
			echo "  <td $tdtype[$switch] >".$icons[$row['primer3']]."</td>\n";
			echo "  <td $tdtype[$switch] >".$icons[$row['optimal']]."</td>\n";
			echo "  <td $tdtype[$switch] >$link".$icons[$row['mfold']]."</td>\n";
			echo " </tr>\n";
			$switch = $switch + pow(-1,$switch);
		}
		echo "</table>\n";
		echo "</div><div class=sectie>\n";
		echo "<h3>Runtime output voor Chr$chrtxt:". number_format($start,0,'',',')."-". number_format($end,0,'',',')."</h3>";
		if ($nrlines > 200) {
			echo "<p>Due to the large size of the Runtime output, only the top 50 and last 150 lines are shown. If you want to browse the entire output, click <a href=\"index.php?page=runtimeoutput&file='$outfile'\" target='_blank'>here </a></p>\n";
			echo "<p><span class=nadruk>Runtime Output:</span></p>";
			echo "<PRE>\n";
			system("head -n 50 $outfile");
			echo "</PRE>\n";
			echo "<img src='images/content/vdots.png'><br/>\n";
			echo "<img src='images/content/vdots.png'><br/>\n";
			echo "<PRE>\n";
			system("tail -n 150 $outfile");
			echo "</PRE>\n";
		}
		else {
			echo "<PRE>\n";
	 		readfile("$outfile");
	  		echo "</PRE>\n";
		}
	}
}
else{
	echo "No region and/or no operator and/or no uid specified<br>\n";
}
echo "</div>\n";

?>
