<?php

if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

$fieldvalue = 'Chromosomal Region';

// POSTED ITEMS FROM INPUT FIELD ? => add to the $_GET.
if (isset($_GET['region']) && $_GET['region'] != '') {
	$region = $_GET['region'];
	$validfound =0;	
	// UCSC style
	$region = trim($region);
	if (preg_match("/[c|C]hr(\w{1,2}):(\S+)(-|_|\+)(\S+)/i", $region)) {				
		$pieces = preg_split("/[:\-_+]/",$region);
		$length = count($pieces);
		# start was negative, '-' used as delimiter
		if ($length > 3) {
			$pieces[1] = -1 * abs($pieces[2]);
			$pieces[2] = $pieces[3];
		}
		$chrtxt = preg_replace("/[c|C]hr(\w+)/","$1",$pieces[0]);
		if ($chrtxt == '23' || $chrtxt == '24'){
			$chrtxt = $chromhash[$chrtxt];
		}
		$chr = $chromhash[ $chrtxt ] ;
		$start = $pieces[1];
		$start = str_replace(",","",$start);
		if ($start < 0) {
			$start = 1;
		}
		$end = $pieces[2];
		$end = str_replace(",","",$end);
		// check end
		$nq = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
		$nrow = mysql_fetch_array($nq);
		$chrstop = $nrow['stop'];
		if ($end > $chrstop) {
			$end = $chrstop;
		}
		
		$regiontxt = "Chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',');
		$validfound = 1;
	}
	// CYTOBAND style
	elseif (preg_match("/(\d{1,2}|X|x|Y|y)(p|q)(\d+|ter)/",$region)) {
		$fiveprimevalid = 0;
		$threeprimevalid = 0;
		$invalidprinted = 0;
		$regiontxt = $region;
		$chrtxt = preg_replace("/(\w{1,2})(p|q)(\S+)/","$1",$region);
		$chr = $chromhash[$chrtxt];
		if (preg_match("/\-/",$region)) {  // cytoband range provided
			$startband = preg_replace("/(\w{1,2})(p|q)(\S+)\-(\S+)/","$2$3",$region);
			$stopband = preg_replace("/(\w{0,2})(p|q)(\S+)\-(\S+)/","$4",$region);
			$stopband = preg_replace("/(\w{0,2})(p|q)(\S+)/","$2$3",$stopband);
		}
		else {
			$startband = preg_replace("/(\w{1,2})(p|q)(\S+)/","$2$3",$region);
			$stopband = $startband;
		}
		if ($startband == 'pter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY start ASC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$startband = $nrow['name'];
		}
		if ($stopband == 'pter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY start ASC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$stopband = $nrow['name'];
		}
		if ($startband == 'qter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$startband = $nrow['name'];
		}
		if ($stopband == 'qter') {
			$nq = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$nrow = mysql_fetch_array($nq);
			$stopband = $nrow['name'];
		}

		$qstart = mysql_query("SELECT start, stop FROM cytoBand WHERE chr = '$chr' AND name = '$startband'");
		$qstop = mysql_query("SELECT start, stop FROM cytoBand WHERE chr= '$chr' AND name = '$stopband'");
		if (mysql_num_rows($qstart) == 0 ) {
			$invalidprinted = 1;
			echo "<div class=sectie>\n<h3>Invalid Band provided</h3>\n";
			echo "<p><span class=nadruk>5' Band</span></p>\n";
			echo "<p>According to ".$_SESSION['dbstring'].", there is no '$startband'-band for chromosome $chrtxt.\n";
			$qsquery = mysql_query("SELECT start, name FROM cytoBand WHERE chr = '$chr' AND name LIKE '$startband%' ORDER BY start ASC LIMIT 1");
			if (mysql_num_rows($qsquery) != 0) {
				$cstartarray = mysql_fetch_array($qsquery);
				$fivestart = $cstartarray['start'];
				$name1 = $cstartarray['name'];
				//if ($startband == $stopband) {
				$qssquery = mysql_query("SELECT stop, name FROM cytoBand WHERE chr= '$chr' AND name LIKE'$startband%' ORDER BY stop DESC LIMIT 1");	
				$cstartarray = mysql_fetch_array($qssquery);
				$fivestop = $cstartarray['stop'];
				$name2 = $cstartarray['name'];
				if ($name1 == $name2) {
					echo " $chrtxt$startband was converted to $chrtxt$name1.";
				}
				else {
					echo " $chrtxt$startband was converted to $chrtxt$name1-$chrtxt$name2.";
				}
				$fiveprimevalid = 1;
			}
			else {
				echo "Please check your query and try again.\n";	
			}

		}
		else {
			$cstartarray = mysql_fetch_array($qstart);
			$fivestart = $cstartarray['start'];
			$fivestop = $cstartarray['stop'];
			$fiveprimevalid = 1;
		}
		if (mysql_num_rows($qstop) == 0 ) {
			if ($invalidprinted == 0) {
				echo "<div class=sectie>\n<h3>Invalid Band provided</h3>\n";
			}
			echo "<p><span class=nadruk>3' Band</span></p>";
			echo "<p>According to ".$_SESSION['dbstring'].", there is no '$stopband'-band for chromosome $chrtxt. \n";
			$qsquery = mysql_query("SELECT start, name FROM cytoBand WHERE chr = '$chr' AND name LIKE '$stopband%' ORDER BY start ASC LIMIT 1");
			if (mysql_num_rows($qsquery) != 0) {
				$cstoparray = mysql_fetch_array($qsquery);
				$threestart = $cstoparray['start'];
				//echo "<h3>threestart $threestart</h3>";
				$name1 = $cstoparray['name'];
				$qssquery = mysql_query("SELECT stop, name FROM cytoBand WHERE chr= '$chr' AND name LIKE'$stopband%' ORDER BY stop DESC LIMIT 1");	
				$cstoparray = mysql_fetch_array($qssquery);
				$threestop = $cstoparray['stop'];
				$name2 = $cstoparray['name'];
				if ($name1 == $name2) {
					echo " $chrtxt$stopband was converted to $chrtxt$name1.";
				}
				else {
					echo " $chrtxt$stopband was converted to $chrtxt$name1-$chrtxt$name2.";
				}
				$threeprimevalid = 1;
			}
			else {
				echo "Please check your query and try again.\n";	
			}

		}
		else {
			$cstoparray = mysql_fetch_array($qstop);
			$threestop = $cstoparray['stop'];
			$threestart = $cstoparray['start'];
			$threeprimevalid = 1;
		}
		if ($fivestart >= $threestop) {
			$start = $threestart;
			$end = $fivestop;
		}
		else {
			$start = $fivestart;
			$end = $threestop;
		}
		if ($invalidprinted == 1) {
			echo "</div>";
		}
		if ($fiveprimevalid == 1 && $threeprimevalid == 1) {
			$validfound = 1;
		}
		$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";

	}
	// ASSUME THIS WAS A GENE SYMBOL
	else {				
		$symbol = $region;
		$regiontxt = $region;
		if (strpos($symbol,'*')) {
			$querysymbol = str_replace('*','%',$symbol);
			$querystring = "SELECT id, chr, start, end, symbol FROM genes WHERE symbol LIKE '$querysymbol' group by start, end ORDER BY symbol, chr, start, end ";
		}
		else {
			$querystring = "SELECT id, chr, start, end, symbol FROM genes WHERE symbol = '$symbol' group by start, end ";
		}
		$genquery = mysql_query($querystring);
		if (mysql_num_rows($genquery) > 1) {
			echo "<div class=sectie>\n";
			echo "<h3>Multiple hits found</h3>\n";
			echo "<p>Your query returned multiple chromosomal locations, please pick one. </p>\n<ul id=ul-simple>\n";
			while ($row = mysql_fetch_array($genquery)) {
				$chr = $row['chr'];
				$chrtxt = $chromhash[$chr];
				$start = $row['start'];
				$end = $row['end'];
				$gid = $row['id'];
				$symbol = $row['symbol'];
				
				echo "<li> - <span class=bold>$symbol</span> : <a href=\"index.php?page=probes&amp;region=chr$chrtxt:$start-$end\">Chr$chrtxt:". number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</a></li>\n";
			}
			echo "</ul></p></div>\n";
		}
		elseif (mysql_num_rows($genquery) == 1) {
			$row = mysql_fetch_array($genquery);
			$chr = $row['chr'];
			$chrtxt = $chromhash[ $chr ];
			$start = $row['start'];
			$end = $row['end'];
			$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";
			$validfound = 1;
		}
		else {
			// check synonyms?
			$osym = $symbol;
			$genquery = mysql_query("SELECT id,chr, start, stop, symbol,synonyms FROM genesum WHERE synonyms REGEXP '(^|".'\\\\|)'.$symbol.'(\\\\||$)'."' GROUP BY symbol");
			if (mysql_num_rows($genquery) > 1) {
				echo "<div class=sectie>\n";
				echo "<h3>Multiple hits found</h3>\n";
				echo "<p>Your query ($osym) did not return an exact hit, but is present as a synonym for multiple genes. Please pick one. </p>\n<ul id=ul-simple>\n";
				while ($row = mysql_fetch_array($genquery)) {
					$chr = $row['chr'];
					$chrtxt = $chromhash[$chr];
					$start = $row['start'];
					$end = $row['stop'];
					$symbol = $row['symbol'];
					$synonyms = $row['synonyms'];
					$synonyms = str_replace('|',' ',$synonyms);
					echo "<li> - <span class=bold>$symbol</span> : <a href=\"index.php?page=results&amp;type=region&amp;region=chr$chrtxt:$start-$end&amp;v=$v&amp;gid=$symbol\">Chr$chrtxt:". number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</a> (Synonyms: $synonyms)</li>\n";
				}
				echo "</ul></p></div>\n";
			}
			elseif (mysql_num_rows($genquery)) {
				$row = mysql_fetch_array($genquery);
				$chr = $row['chr'];
				$chrtxt = $chromhash[$chr];
				$start = $row['start'];
				$end = $row['stop'];
				$symbol = $row['symbol'];
				$synonyms = $row['synonyms'];
				$synonyms = str_replace('|',' ',$synonyms);
				echo "<div class=sectie>";
				echo "<h3>Query Found as Synonym</h3>\n";
				echo "<p>Your query ($osym) was found as a synonym for $symbol on chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</p></div>";
				$isregion = "<h4>Query corresponds to chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($end,0,'',',')."</h4>\n";
				$validfound = 1;
			}
			else {
				echo "<div class=sectie>";
				echo "<h3>No Results</h3>";
				echo "<p>Your query was not recognised as a region, and did not match any gene symbol or synonym. Please check your query and try again.</p>";
				echo "</div>";
			}
		}
	}
	if ($validfound == 1) {
		// put into $_GET;
		$_GET['chr'] = $chrtxt;
		$_GET['start'] = $start;
		$_GET['stop'] = $end;
		
	}
	else {
		$fieldvalue = $region;
	}
}


######################
## POSTED VARIABLES ##
######################
$chr = isset($_GET['chr']) ? $_GET['chr'] : '';
$start = isset($_GET['start']) ? $_GET['start'] : '';
$stop = isset($_GET['stop']) ? $_GET['stop'] : '';
$chiptype = isset($_GET['ct']) ? $_GET['ct'] : '';
$rchr = isset($_GET['rc']) ? $_GET['rc'] : '';
$rstart=isset($_GET['rsa']) ? $_GET['rsa'] : '';
$rstop = isset($_GET['rso']) ? $_GET['rso'] : '';
if ($rchr == '') {
	$rchr = $chr;
}
if ($rstart == '') {
	$rstart = $start;
	$start = $start - 7500;
}
if ($rstop == '') {
	$rstop = $stop;
	$stop = $stop + 7500;
}
$plotwidth = $_COOKIE['plotwidth'];

if ($chr != '' && $start != '' && $stop != '') {
	$fieldvalue = "chr$chrtxt:".number_format($start,0,'',',').'-'.number_format($stop,0,'',',');
}

// input form
?>
<div class=sectie>
<h3>Show BeadChip Coverage by chromosomal region</h3>
<p>Enter the chromosomal region in the field below en click 'search'. The region should be specified in UCSC-style or by chromosome band, e.g. chr7:7500000-9200000,  4q21.3 or 16p11.2-p12.2<p>

<p><form action='index.php' method=GET>
<input type=hidden name='page' value='probes' />
Search region: &nbsp;
<input type=text name=region size=30 value="<?php echo $fieldvalue ?>" onfocus="if (this.value == 'Chromosomal Region') {this.value = '';}"> &nbsp;  
<input type=submit class=button value=search>

</form>
</div>
<?php
// only print genes if selection is made.
if ($chr == '' || $start == '' || $stop == '') {
	exit;
} 



###################
## get chiptypes ##
###################
$query = mysql_query("SELECT name, ID FROM chiptypes ORDER BY name");
$table = '';
$firstline = '';
while ($row = mysql_fetch_array($query)) {
	$id = $row['ID'];
	$name = $row['name'];
	$subquery = mysql_query("SELECT count(ID) as nrprobes FROM probelocations WHERE chromosome = '$chr' AND position BETWEEN '$start' AND '$stop' AND chiptype = '$id'");
	$subrow = mysql_fetch_array($subquery);
	$nrprobes = $subrow['nrprobes'];
	if ($id == $chiptype) {
		$firstline = "<tr><td $firstcell ><span style='color:red'>$name</span></td><td>$nrprobes</td></tr>";
	}
	else {
		$table .= "<tr><td $firstcell >$name</td><td>$nrprobes</td></tr>";
	}
}
echo "<div class=sectie>";
echo "<h3>Overview of Probe Coverage in chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',')."</h3>";
echo "<p>Coverage in this region per supported chiptype is presented below. The source chiptype is shown in red.</p>";
echo "<p><span class=nadruk>1. Tabular overview</span></p>";
echo "<p ><table cellspacing=0 style='padding-left:15px'><tr><th $firstcell>Chiptype</th><th>Coverage</th></tr>$firstline$table</table></p>";
echo "<p><span class=nadruk>2. Graphical overview</span></p>";

############################
## ZOOMING AND NAVIGATION ##
############################
$size = $stop - $start + 1;
$size15 = round($size*15/100);
$size50 = round($size/2);
$size95 = round($size*95/100);
$out15 = $size * 3/2;
$out5 = $size * 5;
$out10 = $size * 10;
$in15 = $size * 2/3;
$in5 = $size / 5;
$in10 = $size / 10;

 // Navigate Left
$startleft15 = $start - $size15;
$stopleft15 = $stop - $size15;
if ($startleft15 < 1) {
	$startleft15 = 1;
}
$regionleft15 = "&chr=$chr&start=$startleft15&stop=$stopleft15";
$startleft50 = $start - $size50;
$stopleft50 = $stop - $size50;
if ($startleft50 < 1) {
	$startleft50 = 1;
}
$regionleft50 = "&chr=$chr&start=$startleft50&stop=$stopleft50";
$startleft95 = $start - $size95;
$stopleft95 = $stop - $size95;
if ($startleft95 < 1) {
	$startleft95 = 1;
}
$regionleft95 = "&chr=$chr&start=$startleft95&stop=$stopleft95";
 // Navigate Right
$startright15 = $start + $size15;
$stopright15 = $stop + $size15;
$regionright15 = "&chr=$chr&start=$startright15&stop=$stopright15";
$startright50 = $start + $size50;
$stopright50 = $stop + $size50;
$regionright50 = "&chr=$chr&start=$startright50&stop=$stopright50";
$startright95 = $start + $size95;
$stopright95 = $stop + $size95;
$regionright95 = "&chr=$chr&start=$startright95&stop=$stopright95";

 //zoom out
$startout15 = $start - round(($out15 - $size)/2);
$stopout15 = $stop + round(($out15 - $size)/2);
$regionout15 = "&chr=$chr&start=$startout15&stop=$stopout15";
$startout5 = $start - round(($out5 - $size)/2);
$stopout5 = $stop + round(($out5 - $size)/2);
$regionout5 = "&chr=$chr&start=$startout5&stop=$stopout5";
$startout10 = $start - round(($out10 - $size)/2);
$stopout10 = $stop + round(($out10 - $size)/2);
$regionout10 = "&chr=$chr&start=$startout10&stop=$stopout10";
 //zoom in
$startin15 = $start + round(($size - $in15)/2);
$stopin15 = $stop - round(($size - $in15)/2);
$regionin15 = "&chr=$chr&start=$startin15&stop=$stopin15";
$startin5 = $start + round(($size - $in5)/2);
$stopin5 = $stop - round(($size - $in5)/2);
$regionin5 = "&chr=$chr&start=$startin5&stop=$stopin5";
$startin10 = $start + round(($size - $in10)/2);
$stopin10 = $stop - round(($size - $in10)/2);
$regionin10 = "&chr=$chr&start=$startin10&stop=$stopin10";
// add original to region
$linkpart = "page=probes&rc=$rchr&rsa=$rstart&rso=$rstop&ct=$chiptype";
// navation menu
echo "<p>\n";
echo "<center>";
echo "<table cellspacing = 0>\n";
echo "<tr>\n";
echo " <th $firstcell class=center>Move Left</th>\n";
echo " <th class=center>Move Right</th>\n";
echo " <th class=center>Zoom In</th>\n";
echo " <th class=center>Zoom out</th>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=button class=button value='15%'  onClick=\"location.href='index.php?$linkpart$regionleft15'\">"; 
echo " <input type=button class=button value='50%'  onClick=\"location.href='index.php?$linkpart$regionleft50'\">";
echo " <input type=button class=button value='95%'  onClick=\"location.href='index.php?$linkpart$regionleft95'\"></td>\n";
echo " <td><input type=button class=button value='15%'  onClick=\"location.href='index.php?$linkpart$regionright15'\">"; 
echo " <input type=button class=button value='50%'  onClick=\"location.href='index.php?$linkpart$regionright50'\">";
echo " <input type=button class=button value='95%'  onClick=\"location.href='index.php?$linkpart$regionright95'\"></td>\n";
echo " <td><input type=button class=button value='1.5x'  onClick=\"location.href='index.php?$linkpart$regionin15'\">"; 
echo " <input type=button class=button value='5x'  onClick=\"location.href='index.php?$linkpart$regionin5'\">";
echo " <input type=button class=button value='10x'  onClick=\"location.href='index.php?$linkpart$regionin10'\"></td>\n";
echo " <td><input type=button class=button value='1.5x' onClick=\"location.href='index.php?$linkpart$regionout15'\">"; 
echo " <input type=button class=button value='5x' onClick=\"location.href='index.php?$linkpart$regionout5'\">";
echo " <input type=button class=button value='10x' onClick=\"location.href='index.php?$linkpart$regionout10'\"></td>";
echo "</tr>\n";
echo "<tr><td colspan=4 $firstcell><img id=chrplot src='chromosomeplot.php?pw=$plotwidth&amp;c=$chr&amp;start=$start&amp;stop=$stop' border=0></td></tr>\n";
echo "</table></center></p>\n";
flush();

###################
## include image ##
###################
echo "<p><img src='plotprobes.php?chr=$chr&start=$start&stop=$stop&ct=$chiptype&pw=$plotwidth&rc=$rchr&rsa=$rstart&rso=$rstop' id=probeplot usemap='#probes' border=0 style='padding-left:5px'></p>";
?>
