<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Browse Analysis Results</h3>\n";
	include('login.php');
	echo "</div>\n";
}
else {

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$icons = array("<img src=images/bullets/no.png>", "<img src=images/bullets/accept.png>");

############
# PROJECTS #
############
if ($_GET['type'] == "projects" ){
	include('inc_projects_permissions.inc');	
}
elseif ($_GET['type'] == "custom") {
	include('inc_custom_permissions.inc');
}
##############
# STATISTICS #
##############
elseif ($_GET['type'] == "stats" ) {
	if ($_GET['step'] == 2) {
		include('inc_profile_stats2.inc');
	}
	else {
		include('inc_profile_stats1.inc');
	}
}	
# ##############
# USER DETAILS #
################
elseif ($_GET['type'] == "personal" ) {
	include('inc_profile_personal.inc');
}
####################
# RECENT &amp; default #
####################
else {
	include('inc_profile_recent.inc');	
}


# END If loggedin block
}
?>

