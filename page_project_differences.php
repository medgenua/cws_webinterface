<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/details_tooltips.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>


<?php 
////////////////////////////////
// Variables for table layout //
////////////////////////////////
if ($loggedin != 1) {
	include('login.php');
	exit();
}

//variables
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$fc =  "style=\"border-left: 1px solid #a1a6a4;\"";
$firstc = array();
$firstc[0] =  "style=\"border-left: 2px solid red;\"";
$firstc[1] = "style=\"border-left: 2px solid blue;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");

// go !
if (!isset($_GET['sid']) || !isset($_GET['pids'])) {
	echo "<div class=sectie>";
	echo "<h3>Missing Data !</h3>";
	echo "<p>No sample or projects selected. Please go back and try again.</p>";
	echo "</div>";
	exit();
}

// get posted data and check permissions
$sid = $_GET['sid'];
$pids = $_GET['pids'];
$pids = explode('|',$pids);
$pchipids = array();
$pkeys = array();
// get projects
asort($pids);
$pkey = 0;
$projecttable = "<table cellspacing=0><tr><th $firstcell>Key</th><th>Project Name</th><th>Chiptype</th><th>Collection</th></tr>";
$problems = '';
foreach ($pids as $key => $pid) {
	$query = mysql_query("SELECT naam, chiptype, created, collection,chiptypeid FROM project WHERE id = '$pid' ");
	$row = mysql_fetch_array($query);
	$pname = $row['naam'];
	$pchip = $row['chiptype'];
	$pcol = $row['collection'];
	$pchipids[$pid] = $row['chiptypeid'];
	// permissions? 
	$query = mysql_query("SELECT projectid FROM projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
	if (mysql_num_rows($query) == 0) {
		$problems .= "<p><span class=em style='color:red;'>Insufficient Permissions:</span> You don't have permission to the project '".$row['naam']."'. This project will <span class=em>not</span> be used in the comparison.</p>";
		unset($pids[$key]);
		continue;
	}
	$pkey++;	
	$pkeys[$pid] = $pkey;
	$projecttable .= "<tr><td $fc>P_$pkey</td><td>$pname</td><td>$pchip</td><td>$pcol</td></tr>";
}
$projecttable .= "</table>";

// print any permission problems if present.
if ($problems != '') {
	echo "<div class=sectie>";
	echo "<h3>Projects excluded on insufficient permissions</h3>";
	echo "$problems";
	echo "</div>";
}

// exit if access to less than two projects remain
if (count($pids) < 2) {
	echo "<div class=sectie>Comparison can not start</h3>";
	echo "<p><span class=em style='color:red'>Critical Problem:</span> There are less than 2 projects available for comparison. This might be due to insufficient permissions. The comparison will not start.</p>";
	echo "</div>";
	exit();
}

// get sample details
$query = mysql_query("SELECT chip_dnanr FROM sample WHERE id = '$sid'");
$row = mysql_fetch_array($query);
$sname = $row['chip_dnanr'];

// print general info
echo "<div class=sectie>";
echo "<h3>Project comparison for sample '$sname'</h3>";
echo "<p>The comparison will be performed between the projects listed below. All CNVs are listed in a table, and presence in different projects is shown on the right. Presence is defined as at least 50% of the region is included in the CNV, and equality of CNV direction (del/dup). Hovering over the project markers for each CNV will show you the CNV details from that project. For those CNVs not found in a certain project, the number of probes in the region on the used chip is given.</p></div>\n";

//print project details. 

echo "<div class=sectie><h3>Project details</h3><p>$projecttable</p></div>";
unset($projecttable);

flush();

//get all CNVs
$pidlist = join(',',$pids);
$query = mysql_query("SELECT a.id, a.cn, a.start, a.stop, a.chr, a.LiftedFrom, a.idproj,a.nrsnps FROM aberration a WHERE a.sample = '$sid' AND a.idproj IN ($pidlist) ORDER BY a.chr, a.start, a.stop");
$regions = array();
$allregions = array();
$byproject = array();
$ridx = 0;
while ($row = mysql_fetch_array($query)) {
	$aid = $row['id'];
	$cn = $row['cn'];
	$start = $row['start'];
	$stop = $row['stop'];
	$chr = $row['chr'];
	$chrtxt = $chromhash[$chr];
	$lifted = $row['LiftedFrom'];
	$apid = $row['idproj'];
	$probes = $row['nrsnps'];
	if (!array_key_exists("$chr-$start-$stop",$regions) || !array_key_exists($cn, $regions["$chr-$start-$stop"])) {
		// only store if new.
		$regions["$chr-$start-$stop"][$cn] = $ridx;
		$allregions[$ridx] = "$chr-$start-$stop";
		$ridx++;
	}
	$byproject[$apid][$chr]["$start-$stop"]['cn'] = $cn;
	$byproject[$apid][$chr]["$start-$stop"]['aid'] = $aid;
	$byproject[$apid][$chr]["$start-$stop"]['lifted'] = $lifted;
	$byproject[$apid][$chr]["$start-$stop"]['probes'] = $probes;
	#print "chr$chrtxt:$start-$stop => project $apid ($cn)<br/>";
}

// print out
ksort($allregions);
echo "<div class=sectie>";
echo "<h3>CNV comparison results for '$sname'</h3>";
echo "<table cellspacing=0><tr><th $fc>Region</th><th>Size</th><th>C.N.</th>";
foreach ($pkeys as $pid => $key) {
	echo "<th>P_$key</th>";
}
echo "</tr>";
$group = 1;
$prevchr = 1;
$largestend = 0;
foreach($allregions as $ridx => $region) {
	$rparts = explode('-',$region);
	$rsize = $rparts[2] - $rparts[1] + 1;
	// new region? 
	if ($rparts[0] != $prevchr || $rparts[1] > $largestend) {
		$group = $group + pow(-1,$group);
		$prevchr = $rparts[0];
		$largestend = $rparts[2];
		$border = "style='border-top:2px solid #a1a6a4;'";
		$borderfc = "style='border-left: 1px solid #a1a6a4;border-top:2px solid #a1a6a4;'";
	}
	else {
		$border = '';
		$borderfc = "style='border-left: 1px solid #a1a6a4;'";
	}
	// increase end if larger than stored
	if ($rparts[2] > $largestend) {
		$largestend = $rparts[2];
	}
	// check regions 
	foreach ($regions[$region] as $rcn => $dummy) {
		//start row
		$row = "<tr>";
		// region
		$row .= "<td $borderfc>Chr".$chromhash[$rparts[0]].":".number_format($rparts[1],0,'',',')."-".number_format($rparts[2],0,'',',')."</td>";
		// size
		$row .= "<td $border>".number_format(($rparts[2] - $rparts[1] + 1),0,'',',')."</td>";
		// copy number
		$row .= "<td $border>$rcn</td>";
		// look for coverage in projects.
		foreach ($pkeys as $pid => $key) {
			$string = '';
			$onmouse = '';
			// select regions from current project on current chromosome.
			ksort($byproject[$pid][$rparts[0]]);
			foreach($byproject[$pid][$rparts[0]] as $startstop => $subarray) {
				// skip if wrong copynumber
				if ($subarray['cn'] != $rcn) {
					continue;
				}
				// upd ?
				if ($subarray['cn'] == 9) {
					$upd = 1;
				}
				else {
					$upd = 0;
				}	
				// check for overlap (should be ordered by start)
				$sparts = explode('-',$startstop);
				if ($sparts[1] < $rparts[1]) {
					// end of this region < target region start => delete from array, as all targets have passed.
					#unset($byproject[$pid][$rparts[0]][$startstop]);
					continue;
				}
				// easy : perfect match
				if ($sparts[0] == $rparts[1] && $sparts[1] == $rparts[2]) {
					$string = 'Exact Match';
					#$onmouse = "onmouseover=\"Tip(ToolTip('".$subarray['aid']."','$userid','i',0,event))\" onmouseout=\"UnTip()\"";
					$myaid = $subarray['aid'];
					#$mystart = $sparts[
					$onmouse = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;upd=$upd&amp;id=$myaid','IlluminaSearchSamples','$userid','full',event)\"";
					break;
				}
				// harder: this region contains target
				if ($sparts[0] <= $rparts[1] && $sparts[1] >= $rparts[2]) {
					$size = $sparts[1] - $sparts[0] + 1;
					$string = "Contains target\n";
					#$onmouse = "onmouseover=\"Tip(ToolTip('".$subarray['aid']."','$userid','i',0,event))\" onmouseout=\"UnTip()\"";
					$myaid = $subarray['aid'];
					$onmouse = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;upd=$upd&amp;id=$myaid','IlluminaSearchSamples','$userid','full',event)\"";
					
					break;
				}
				//hardest: other overlap
				if (($sparts[0] >= $rparts[1] && $sparts[0] <= $rparts[2]) || ($sparts[1] >= $rparts[1] && $sparts[1] <= $rparts[2]))  {
					// there is overlap, check size
					$size = $sparts[1] - $sparts[0] + 1;
					$overlap = round(($size/$rsize)*100,2);
					if ($overlap >= 50) {
							$string = "Partial overlap<br/>($overlap% of targetsize)";
							#$onmouse = "onmouseover=\"Tip(ToolTip('".$subarray['aid']."','$userid','i',0,event))\" onmouseout=\"UnTip()\"";
							$myaid = $subarray['aid'];
							$onmouse = "onclick=\"ClearTip(0);TagToTip('tooltipdiv');CustomTT('1&amp;upd=$upd&amp;id=$myaid','IlluminaSearchSamples','$userid','full',event)\"";

							break;	
					}
				}
			}
			if ($string == '') {
				$string = 'Not Found<br/>';
				$probesq = mysql_query("SELECT COUNT(ID) as nr FROM probelocations WHERE position BETWEEN ".$rparts[1]." AND ".$rparts[2]." AND chromosome = ".$rparts[0]." AND chiptype = '". $pchipids[$pid]."'");
				$probesrow = mysql_fetch_array($probesq);
				$nrp = $probesrow['nr'];
				$string .= "($nrp probes in region)";
				$onmouse = '';
			}
			$row .= "<td $onmouse $border>$string</td>";
		}
		$row .= "</tr>";
		echo "$row";
		flush();	
	}
}	
echo "</table>";
echo "</div>";
?>
<div id="txtHint">something must appear here</div>
<!-- context menu loading -->

<!-- LOAD page specific javascript functions -->
<script type="text/javascript" src="javascripts/ajax_tooltips.js"></script>
<script type="text/javascript" src="javascripts/setclass.js"></script>
<script type='text/javascript' src='javascripts/details.js'></script> <!-- mendelian error stuff -->
