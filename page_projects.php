<?php
if (isset($loggedin) && $loggedin != 1) {
	echo "<div class=sectie><h3>Browse Analysis Results</h3>\n";
	include('login.php');
	echo "</div>\n";
	exit;
}

// layout & general variables
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$icons = array("<img src=images/bullets/no.png>", "<img src=images/bullets/accept.png>");

$switch = 0;

// set DB
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");


$type = $_GET['type'];
if ($type == 'illumina') {
  // INTEGRATED, created, set permissions, or delete
  echo "<div class=sectie>\n";
  if (isset($_GET['result']) && $_GET['result'] == 1) {
	echo "<h3>Permissions : Update Successful !</h3>\n";
	echo "<h4>... permissions successfully updated</h4>\n";
  }
  elseif (isset($_GET['result']) && $_GET['result'] == 2) {
	echo "<h3>Project successfully deleted</h3>\n";
  }

  else {
	echo "<h3>Manage Illumina BeadArray projects</h3>\n";
  }
  // set collection specific? 
  $setCollection = isset($_GET['collection']) ? $_GET['collection'] : '';
  $collectionquery = mysql_query("SELECT DISTINCT collection FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE p.userID = '$userid' OR (pp.editsample = 1 AND pp.userid = ' $userid') order by p.collection");
  echo "<p><form action='index.php?page=projects&type=illumina' method=POST>";
  echo "Select Collection : <select name='collection' onchange=\"reloadProjects(this.form,'illumina')\"><option value='ALL'>All collections</option>";
  while ($row = mysql_fetch_array($collectionquery)) {
	$selected = ($row['collection'] == $setCollection) ? 'SELECTED' : '';
 	echo "<option $selected value='".$row['collection']."'>".$row['collection']."</option>";
  }
  echo "</select></form></p>";
  if ($setCollection == 'ALL') {
	  $query = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection, p.userID, p.protected FROM project p JOIN projectpermission pp ON pp.projectid = p.id WHERE p.userID = '$userid' OR (pp.editsample = 1 AND pp.userid = ' $userid') GROUP BY p.id ORDER BY p.naam ASC");
  }
  else {
	$query = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection, p.userID, p.protected FROM project p JOIN projectpermission pp ON pp.projectid = p.id WHERE p.collection = '$setCollection' AND (p.userID = '$userid' OR (pp.editsample = 1 AND pp.userid = ' $userid')) GROUP BY p.id ORDER BY p.naam ASC");
  }
  if (mysql_num_rows($query) == 0) {
	echo "<p>You have not yet started any CNV-projects</p>\n";
  }
  else {

	
	echo "<p>Below is the list of projects you have started on this server. From here, you can allow other users to see and browse your results, join or delete projects.\n";
	echo "<p>\n";
	echo "<form action=change_permissions.php method=POST>\n";
	echo "<input type=hidden name=uid value='$userid'>\n";
	echo "<table cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</th>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</th>\n";
	echo "  <th scope=col class=topcellalt>Collection</th>\n";
	echo "  <th scope=col class=topcellalt>Samples</th>\n";
	echo "  <th scope=col class=topcellalt>Created</th>\n";
	if (!isset($_SESSION['Archived']) || $_SESSION['Archived'] != 1) {
		echo "  <th scope=col class=topcellalt title='Share Project With Collaborators'>Share</th>\n";
		echo "  <th scope=col class=topcellalt title='Delete a Project'>Delete</th>\n";
		echo "  <th scope=col class=topcellalt title='Join Projects from identical chiptypes'>Join</th>\n";
		echo "  <th scope=col class=topcellalt title='Protect a Project against deletion'>Protect</th>\n";
	}
	echo " </tr>\n";
	$samples_per_project = array();
	while($row = mysql_fetch_array($query)) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$chiptype = preg_replace('/(Human)(.*)/',"$2",$chiptype);
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$countquery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS number FROM aberration WHERE idproj = $id");
		$qres = mysql_fetch_array($countquery);
		$samples_per_project[$id] = $qres['number'];
		$collection = $row['collection'];
		$owner = $row['userID'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name </td>\n";
		echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		echo "  <td $tdtype[$switch] >$samples_per_project[$id]</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		if (!isset($_SESSION['Archived']) || $_SESSION['Archived' ] != 1) {
			echo" <td $tdtype[$switch]><a href='index.php?page=shareprojects&pid=$id' target='_blank'>Share</a></td>\n";
			if ($row['protected'] == 1) {
				echo "  <td $tdtype[$switch] ><span style='font-size:0.75em;color:red'>protected</span></td>\n";
			}				
			else {
				echo "  <td $tdtype[$switch] ><input type='checkbox' name='deletearray[]' value=$id></td>\n";
			}
			if ($row['protected'] == 1) {
				echo "  <td $tdtype[$switch] ><span style='font-size:0.75em;color:red'>protected</span></td>\n";
			}
			else {	
				echo "<td $tdtype[$switch] ><input type='checkbox' name='joinarray[]' value=$id></td>\n";
			}
			if ($row['protected'] == 1) {
				$checked = 'checked';
			}
			else {
				$checked = '';
			}
			echo "<td $tdtype[$switch] ><input type='checkbox' name='protectarray[$id]' value=$id $checked></td>\n";
		}
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
	  //echo "<p><input type=submit class=button value='Delete Selected Projects' name=delete></p>\n";
	echo "<p><input type=submit class=button value='Join Selected Projects' name=join></p>\n";
	echo "<p><input type=submit class=button value='Protect Selected Projects' name=protectsubmit></p>\n";
	echo "</form>\n";
    echo "<p><input type=button class=button value='Delete Selected Projects' name=delete onClick='CheckDelete($userid)'></p>\n";

  }
  echo "</div>\n"; 
}
elseif ($type == 'Accessible') {
  # INTEGRATED, access to see
  echo "<div class=sectie>\n";
  echo "<h3>Integrated Analysis: Overview</h3>\n";
  echo "<h4>Of all projects you are entitled to see</h4>\n";

  // set collection specific? 
  $setCollection = isset($_GET['collection']) ? $_GET['collection'] : '';
  $collectionquery = mysql_query("SELECT DISTINCT collection FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' order by p.collection");
  echo "<p><form action='index.php?page=projects&type=Accessible' method=POST>";
  echo "Select Collection : <select name='collection' onchange=\"reloadProjects(this.form,'Accessible')\"><option value='ALL'>All collections</option>";
  while ($row = mysql_fetch_array($collectionquery)) {
	$selected = ($row['collection'] == $setCollection) ? 'SELECTED' : '';
 	echo "<option $selected value='".$row['collection']."'>".$row['collection']."</option>";
  }
  echo "</select></form></p>"; 
  if ($setCollection == 'ALL') {
	  $pquery = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection, p.userID FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY id DESC");
  }
  else {
	  $pquery = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection, p.userID FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' AND p.collection = '$setCollection' ORDER BY id DESC");
  }
  if (mysql_num_rows($pquery) == 0) {
	echo "<p>You don't have access to any project.</p>\n";
  }
  else {
	echo "<p>\n";
	echo "These are all the projects you have access to.  Click on 'Details' for further exploration</p>\n";
	echo "<p>\n";
	echo "<table cellspacing=0>\n";
	$switch = 0;
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Collection</td>\n";
	echo "  <th scope=col class=topcellalt>Samples</td>\n";	
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo "  <th scope=col class=topcellalt>Recreate XML</td>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($pquery)) {
		$id = $row['id'];
		$name = $row['naam'];
		$chiptype = $row['chiptype'];
		$chiptype = preg_replace('/(Human)(.*)/',"$2",$chiptype);

		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		$owner = $row['userID'];
		$countquery = mysql_query("SELECT COUNT(idsamp) AS number FROM projsamp WHERE idproj = $id");
		$qres = mysql_fetch_array($countquery);
		$number = $qres['number'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		echo "  <td $tdtype[$switch] >$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=results&amp;type=tree&amp;collection=$collection&amp;project=$id&amp;sample=\">Details</a></td>\n";
		echo "  <td $tdtype[$switch]><a href='index.php?page=create_custom_bookmark&amp;p=$id'>XML</a></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
  }
  echo "</div>\n";
}

// SINGLE-ALGO, created
elseif ($type == 'single') {
  echo "<div class=sectie>\n";
  echo "<h3>Single Algorithm: Overview</h3>\n";
  echo "<h4>Of all projects you have started</h4>\n";
  $query = mysql_query("SELECT nm.ID, nm.algo, nm.project, nm.stalen, nm.bookmarks, nm.date, ct.name FROM nonmulti nm JOIN chiptypes ct ON nm.chiptype = ct.ID WHERE nm.userID = '$userid' ORDER BY ID DESC ");
  if (mysql_num_rows($query) == 0) {
	echo "<p>You didn't start any projects yet.</p>\n";
  }
  else {
	$switch = 0;
	echo "<p>\n";
	echo "These are all the single-algorithm projects you have created.  Click on 'Details' for further exploration, or on 'XML' to download the bookmark file.</p>\n";
	echo "<p>\n";
	echo "<table cellspacing=0>\n";
	$switch = 0;
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</td>\n";
	echo "  <th scope=col class=topcellalt>Created</td>\n";
	echo "  <th scope=col class=topcellalt>Chiptype</td>\n";
	echo "  <th scope=col class=topcellalt>Algorithm</td>\n";
	echo "  <th scope=col class=topcellalt>Samples</td>\n";
	echo "  <th scope=col class=topcellalt>XML File</td>\n";	
	echo "  <th scope=col class=topcellalt>Details</td>\n";
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($query)) {
		$id = $row['ID'];
		$name = $row['project'];
		$chiptype = $row['name'];
		$created = $row['date'];
		$created = substr($created, 0, strpos($created, "-"));
		$algo = $row['algo'];
		$filename = $row['bookmarks'];
		$number = $row['stalen'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$algo</td>\n";
		echo "  <td $tdtype[$switch] >$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"download.php?file=$filename\">XML</a></td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=detailsxml&amp;project=$filename\">Details</a></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
  }
  echo "</div>\n"; 
}
// CUSTOM accessible
elseif ($type == 'custom') {
  // first delete
  if (isset($_POST['del'])) {
	$todel = $_POST['del'];
	// project
	$query = mysql_query("DELETE FROM cus_project WHERE id = '$todel'");
	// sample
	$query = mysql_query("DELETE FROM cus_sample WHERE idproj = '$todel'");
	// pheno
	$query = mysql_query("DELETE FROM cus_sample_phenotypes WHERE pid = '$todel'");
	// permissions
	$query = mysql_query("DELETE FROM cus_projectpermissiongroup WHERE projectid = '$todel'");
	$query = mysql_query("DELETE FROM cus_projectpermission WHERE projectid = '$todel'");
	// prioritize
	$query = mysql_query("SELECT id FROM cus_prioritize WHERE project = '$todel'");
	if (mysql_num_rows($query) > 0) {
		$instr = '';
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$instr = $instr."$id,";
		}
		$instr = substr($instr,0,-1);
		$query = mysql_query("DELETE FROM cus_prioritize WHERE project = '$todel'");
		$query = mysql_query("DELETE FROM cus_priorabs WHERE prid in ($instr)");
	}
	// log
	$query = mysql_query("DELETE FROM cus_log WHERE pid = '$todel'");
	//deleted
	$query = mysql_query("DELETE FROM deletedcnvs WHERE idproj = '$todel'");
	//aberration
	$query = mysql_query("SELECT id FROM cus_aberration WHERE idproj = '$todel'");
	if (mysql_num_rows($query) > 0) {
		$instr = '';
		while ($row = mysql_fetch_array($query)) {
			$instr = $instr . $row['id'] . ',';
		}
		$instr = substr($instr,0,-1);
		$query = mysql_query("DELETE FROM cus_aberration WHERE idproj = '$todel'");
		$query = mysql_query("DELETE FROM cus_datapoints WHERE id IN ($instr)");
		$instr = '';
	}
  }	 
  // now print list
  echo "<div class=sectie>\n";
  echo "<h3>Custom Datasets: Overview</h3>\n";
  echo "<h4>Of all projects you are entitled to see</h4>\n";
  $pquery = mysql_query("SELECT p.id, p.naam, p.created, p.collection, p.userID FROM cus_project p JOIN cus_projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY id DESC");
  if (mysql_num_rows($pquery) == 0) {
	echo "<p>You don't have access to any custom datasets.</p>\n";
  }
  else {
	echo "<p>\n";
	echo "These are all the projects you have access to.  Click on 'Details' for further exploration</p>\n";
	echo "<p>\n";
	echo "<table cellspacing=0>\n";
	$switch = 0;
	echo " <tr>\n";
	echo "  <th scope=col class=topcellalt $firstcell>Name</th>\n";
	echo "  <th scope=col class=topcellalt>Created</th>\n";
	echo "  <th scope=col class=topcellalt>Collection</th>\n";
	echo "  <th scope=col class=topcellalt>Samples</th>\n";	
	echo "  <th scope=col class=topcellalt>Details</th>\n";
	echo "  <th scope=col class=topcellalt>Overview</th>\n";
	if ($_SESSION['Archived'] != 1) {
		echo "  <th scope=col class=topcellalt>Share</th>\n";
		echo "  <th scope=col class=topcellalt>Delete</th>";
	}
	echo " </tr>\n"; 
	while($row = mysql_fetch_array($pquery)) {
		$id = $row['id'];
		$name = $row['naam'];
		$created = $row['created'];
		$created = substr($created, 0, strpos($created, "-"));
		$collection = $row['collection'];
		$owner = $row['userID'];
		$countquery = mysql_query("SELECT COUNT(DISTINCT(sample)) AS number FROM cus_aberration WHERE idproj = $id");
		$qres = mysql_fetch_array($countquery);
		$number = $qres['number'];
		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>$name</td>\n";
		echo "  <td $tdtype[$switch] >$created</td>\n";
		//echo "  <td $tdtype[$switch] >$chiptype</td>\n";
		echo "  <td $tdtype[$switch] >$collection</td>\n";
		echo "  <td $tdtype[$switch] >$number</td>\n";
		echo "  <td $tdtype[$switch]><a href=\"index.php?page=custom_methods&amp;type=tree&amp;collection=$collection&amp;project=$id&amp;sample=\">Details</a></td>\n";
		echo "  <td $tdtype[$switch]><a href='index.php?page=overviewbrowse&amp;p=$id&amp;cstm=1'>Overview</a></td>\n";
		if ($_SESSION['Archived'] != 1) {
  		  if ($owner == $userid) {
			echo "<td $tdtype[$switch]><a href='index.php?page=customshareprojects&amp;pid=$id'>Share</a></td>\n";
		  }
		  else {
			echo "<td $tdtype[$switch]>Share</td>\n";
		  }
		  if ($owner == $userid) {
			echo "<td $tdtype[$switch]><form action='index.php?page=custom_methods&type=custom&v=c' method=POST><input type=hidden name=del value='$id'><input type=submit class=button value='Delete'></form></td>";
		  }
		  else {
			echo "<td $tdtype[$switch]>&nbsp;</td>";
		  }
		}
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
  }
  echo "</div>\n";
}
else {
	## DEFAULT
	echo "<div class=sectie>";
	echo "<h3>Manage Projects</h3>";
	echo "<p>Please select an option: </p>";
	echo "<ul id=ul-simple>";
	echo "<li style='padding-left:5px'><span class=bold><a href='index.php?page=projects&type=illumina&collection=ALL'>My Illumina Projects</a></span> : List the projects with options to share, join or delete them.</li>";
	echo "<li style='padding-left:5px'><span class=bold><a href='index.php?page=projects&type=Accessible&collection=ALL'>All Illumina Projects</a></span> : List the projects with options to browse them and to (re)create XML files with GenomeStudio bookmarks.</li>";
	echo "<li style='padding-left:5px'><span class=bold><a href='index.php?page=projects&type=custom'>Custom Projects</a></span> : List the projects containing non-BeadArray data. Also has the options to share and browse them.</li>";
	echo "<li style='padding-left:5px'><span class=bold><a href='index.php?page=projects&type=single'>Single Algo Projects</a></span> : List projects based on a single method. Has the option to download the corresponding XML and to browse the results.</li>";
	echo "</ul></p>";
	echo "<p>Click the button Below to add some default projects to your account. These include the projects described in the CNV-WebStore Publication, and a small Demo Project.</p><p><form action='index.php?page=add_default_projects' method=POST><input type=submit name='adddefproj' class=button value='Add Default Projects'></form></p>";


	echo "</div>"; 	



}









?>
