<?php

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$ucscdb = str_replace('-','',$_SESSION['dbname']);


$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
// get public resources ids
$query = mysql_query("SELECT db_links FROM usersettings WHERE id = '$userid'");
$row = mysql_fetch_array($query);
$db_links = $row['db_links'];

// comma-separated files with gene IDs
$genes = $_POST['genes'];
//error_log(print_r($genes, TRUE));
$samplename=stripslashes($_POST['sample']);


echo "<div class=sectie>\n";
echo "<h3>Recessive genes in all homozygous regions for sample $samplename</h3>\n";
echo "<p>The following genes are disrupted by, or completely inside the queried chromosomal region.  For each gene, links to some handy sites are provided. To change the displayed resources, or add new ones, go to 'Profile'->'Plot Settings'->'Public Databases', or click <a href='index.php?page=settings&type=plots&st=publ&v=p'>here</a>.\n";

echo "</div><div class=sectie><p><br><table cellspacing=0>\n";

echo "<tr>\n";
echo " <th scope=col class=topcellalt $firstcell>Symbol</td>\n";
echo " <th scope=col class=topcellalt>Description</td>\n";
echo " <th scope=col class=topcellalt>Omim</td>\n";
echo " <th scope=col class=topcellalt>Morbid Description(s)</td>\n";
echo " <th scope=col class=topcellalt>Public Resources</td>\n";
echo "</tr>\n";

$recquery = "SELECT symbol, omimID, morbidID, morbidTXT, geneTXT, geneID FROM genesum WHERE omimID IN ( $genes ) ORDER BY symbol";
error_log(print_r($recquery, TRUE));
$results = mysql_query($recquery);
//AND ( (start BETWEEN $start AND $stop) OR (end BETWEEN $start AND $stop) OR (start <= $start AND end >= $stop))
while ($row = mysql_fetch_array($results)) {
	$symbol = $row['symbol'];
	$omimID = $row['omimID'];
	$morbidID = $row['morbidID'];
	$morbidTXT = $row['morbidTXT'];
	$geneid = $row['geneID'];
	if ($morbidTXT != '') {
		$morbidTXT = preg_replace('/^(\|)(.*)/','$2',$morbidTXT);
		#$morbidTXT = str_replace('|','; ',$morbidTXT);
		#$morbidTXT = preg_replace('/(.*),\s\d+\s.*$/','$1',$morbidTXT);
	}
	$geneTXT = $row['geneTXT'];
	echo "<tr>\n";
	echo " <td $firstcell>$symbol</td>\n";
	$sigcol = $sigs[ $symbol ];
	if ($sigcol != "") {
		echo "$sigcol";
	}
	elseif ($extracoltop != "") {
		echo "<td>&nbsp;</td>\n";
	}
	echo "<td>$geneTXT&nbsp;</td>\n";
	#echo " <td><a href=\"http://www.ensembl.org/Homo_sapiens/Search/Details?species=Homo_sapiens;idx=;q=$symbol\" target=new>Synonyms</a></td>\n";	
	// OMIM column
	if ($omimID != 0) {
		echo " <td><a href=\"http://www.omim.org/entry/$omimID\" target='_blank' class=italic>$omimID</a></td>\n";
	}
	else {
		echo " <td>/</td>\n";
	}
	$morbidresults = mysql_query("SELECT `morbidID`, `disorder`, `inheritance mode` AS inheritance FROM `morbidmap` WHERE `omimID` = $omimID ORDER BY `morbidID`");
	if (mysql_num_rows($morbidresults) > 0) {
		echo " <td><ul>\n";
		while ($morbidrow = mysql_fetch_array($morbidresults)) {
			$morbidID = $morbidrow['morbidID'];
			$disorder = $morbidrow['disorder'];
			$inheritance = $morbidrow['inheritance'];
			if ($inheritance) {
				echo "<li><a href=\"http://www.omim.org/entry/$morbidID\" target='_blank' class=italic>[$morbidID] $disorder</a><span style=\"color:blue\"> [$inheritance]</span></li>\n";
			}
			else {
				echo "<li><a href=\"http://www.omim.org/entry/$morbidID\" target='_blank' class=italic>[$morbidID] $disorder</a></li>\n";
			}
		}
		echo " </ul></td>\n";
	}
	else {
		echo " <td>/</td>\n";
	}

	// public resources:
	$query = mysql_query("SELECT Resource, link, Description FROM db_links WHERE id in ($db_links) AND type=1 ORDER BY Resource");
	$nrlinks = mysql_num_rows($query);
	if ($nrlinks == 0) {
		echo "<td> - No resources specified </td>\n";
	}
	else {
		$content = "<td>";
		while ($row = mysql_fetch_array($query)) {
			$dbname = $row['Resource'];
			$link = $row['link'];
			$title = $row['Description'];
			$link = str_replace('%s',$symbol,$link);
			$link = str_replace('%o',$omimID,$link);	
			$link = str_replace('%g',$geneid,$link);
			$link = str_replace('%u',$ucscdb,$link);
			$content .= "<a title='$title' href='$link' target='_blank'>$dbname</a>, ";
		}
		$content = substr($content,0,-2) . "</td>\n";
		echo "$content";
	}

	#echo " <td><a href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=pubmed&amp;cmd=search&amp;term=$symbol\" target=new>PubMed</a></td>\n";
	#echo " <td><a href=\"http://scholar.google.com/scholar?hl=en&amp;lr=&amp;btnG=Search&amp;q=$symbol\" target=new>Scholar</a></td>\n";
	#echo " <td><a href=\"http://www.ncbi.nlm.nih.gov/sites/entrez?db=unigene&amp;cmd=search&amp;term=$symbol%20AND%20Homo%20Sapiens\" target=new>UniGene</a></td>\n";
	#echo " <td><a href=\"http://www.pubgene.org/tools/Ontology/BioAssoc.cgi?mode=simple&amp;terms=$symbol&amp;organism=hs&amp;termtype=gap\" target=new>PubGene</a></td>\n";
	#echo " <td><a href=\"index.php?page=results&amp;type=region&amp;region=$symbol\" target=new>Search</a></td>\n";
	echo "</tr>\n";
}

echo "</table></p>\n";
echo "</div>\n";
?>
	
