<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inhhash = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

$dc = array();
$name_dc = array();
$dc_query = mysql_query("SELECT id, name, abbreviated_name, sort_order AS class_sorted FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
	$name_dc[$row['abbreviated_name']] = $row['class_sorted'];
}

$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// GET VARS
$sid = $_POST['sid'];
$pid = $_POST['pid'];

//GET Format options
$includescores = 1;
$includegenes = 1;
$query = mysql_query("SELECT DECODE(clinical, '$encpass') AS clinical, chip_dnanr FROM sample WHERE id = $sid");
$row = mysql_fetch_array($query);
$samplename = $row['chip_dnanr'];
$experimental = "Experimental Comments";
$clinicaldata = $row['clinical'];
if ($clinicaldata == '') {
	$clinicaldata = "Clinical Information";
}	
ob_flush();
flush();

//////////////////
// LOAD FILTERS //
//////////////////
$exp = array("IlluminaProjectSamples","CustomProjectSamples","IlluminaSearchSamples","CustomSearchSamples","Healthy");
$query = mysql_query("SELECT f.Name, uf.fid,  uf.Type, uf.ApplyTo,fr.chr,fr.start, fr.stop,fr.rid,fr.Comment FROM `Users_x_Filters` uf JOIN `Filters_x_Regions` fr JOIN `Filters` f ON f.fid = fr.fid AND fr.fid = uf.fid WHERE uf.uid = '$userid' AND uf.Type = 1 ORDER BY Type ASC");
$filter = array("1" => array(), "2" => array(), "3" => array());
while ($row = mysql_fetch_array($query)) {
	$fchr = $row['chr'];
	// create chromosome entry in filter hash
	if (!isset($filter[$row['Type']][$fchr])) {
		$filter[$row['Type']][$fchr] = array();
	}

	// skip filters that are not applied to illumina-project samples (browsing details implies this.)
	if ($row['ApplyTo'] != 'Experimental') {
		$at = explode(";",$row['ApplyTo']);
		if (!in_array("IlluminaProjectSamples")) {
			continue;
		}
	}
	// first time start pos on this chrom is seen
	if (!isset($filter[$row['Type']][$fchr][$row['start']])) {
		$filter[$row['Type']][$fchr][$row['start']] = array();		
		$filter[$row['Type']][$fchr][$row['start']]['stop'] = $row['stop'];
		$filter[$row['Type']][$fchr][$row['start']]['comment'] = $row['Comment'];
		$filter[$row['Type']][$fchr][$row['start']]['Name'] = $row['Name'];	
	}
	// else... extend if newend > end ; concat names and comments.
	else {
		if ($row['stop'] > $filter[$row['Type']][$fchr][$row['start']]['stop']) {
			$filter[$row['Type']][$fchr][$row['start']]['stop'] = $row['stop'];
		}
		if ($row['Comment'] != "") {
			$filter[$row['Type']][$fchr][$row['start']]['comment'] .= "\n".$row['Comment'];
		}
		$filter[$row['Type']][$fchr][$row['start']]['Name'] .= "; ".$row['Name'];
	}
}	



?>
<script type="text/javascript" src="javascripts/tiny_mce/tinymce.min.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	theme : "modern",
	plugins: "autoresize searchreplace",
	toolbar: "bold italic underline textcolor formats | bullist numlist | undo redo | cut copy paste searchreplace",
	menubar: false,
	statusbar: false,
	autoresize_min_height: 200
	//theme_modern_statusbar: false,
	//theme_advanced_buttons1 : "bold,italic,underline,|,bullist,numlist,|,undo,redo,",
	//theme_advanced_buttons2 : "",
	//theme_advanced_buttons3 : "",
	//theme_advanced_buttons4 : "",
	//theme_advanced_toolbar_location : "top",
	//theme_advanced_toolbar_align : "left",
	//theme_advanced_statusbar_location : "none",
	//theme_advanced_resizing : true,
});
</script>

<script type="text/javascript">
function GeneSet(setting) 
{ 
	for(i=0; i<document.pickdetails.elements.length; i++){
		var myName = document.pickdetails.elements[i].name;
		if (myName.match(/cnvgenes_/) ) {
			var myValue = document.pickdetails.elements[i].value;
			if (myValue == setting) {
				document.pickdetails.elements[i].checked = true;
			}
		}
   	}

}
function lohGeneSet(setting) 
{ 
	for(i=0; i<document.pickdetails.elements.length; i++){
		var myName = document.pickdetails.elements[i].name;
		if (myName.match(/lohgenes_/) ) {
			var myValue = document.pickdetails.elements[i].value;
			if (myValue == setting) {
				document.pickdetails.elements[i].checked = true;
			}
		}
   	}

}

function mosShowSet()
{
	if (document.pickdetails.showtoggle.checked == 1) {
		var newvalue = 1;
	}
	else {
		var newvalue = 0;
	}
	for (i=0; i<document.pickdetails.elements.length; i++) {
        	if (document.pickdetails.elements[i].name=='mosinclude[]')
            			document.pickdetails.elements[i].checked = newvalue;
    	}
}

function ShowSet()
{
	if (document.pickdetails.showtoggle.checked == 1) {
		var newvalue = 1;
	}
	else {
		var newvalue = 0;
	}
	for (i=0; i<document.pickdetails.elements.length; i++) {
        	if (document.pickdetails.elements[i].name=='include[]')
            			document.pickdetails.elements[i].checked = newvalue;
    	}
}


function lohShowSet()
{
	if (document.pickdetails.lohshowtoggle.checked == 1) {
		var newvalue = 1;
	}
	else {
		var newvalue = 0;
	}
	for (i=0; i<document.pickdetails.elements.length; i++) {
        	if (document.pickdetails.elements[i].name=='lohinclude[]')
            			document.pickdetails.elements[i].checked = newvalue;
    	}
}

</script>

<div class=sectie>
<h3>Configure Report for sample <?php echo "$samplename"; ?></h3>
<p>Please specify the information you want to include in the report. Comments and clinical information will not be shown when left blank or default. You can specify three options for listing affected genes. 'N(one)' lists no genes, 'A(ll)' lists all genes and 'P(rioritized)' lists only genes that were significantly prioritized. These settings can be changed on a per aberration level.</p>
</div>

<div class=sectie>
<form name="pickdetails" action='index.php?page=getreport' method='POST'>
<input type=hidden name=sid value=<?php echo $sid;?> >
<input type=hidden name=pid value=<?php echo $pid; ?>>

<p><span class=nadruk>1/ Include addtional comments:</span> <input type=radio name=inccom value=1 checked> Yes / <input type=radio name=inccom value=0>No</p>

<div style='margin-left:10px'>
  <TEXTAREA name="comments" rows=10 style='width:95%' onfocus="if (this.value == 'Experimental Comments') {this.value = '';}"><?php echo "$experimental"; ?></TEXTAREA>
</div>
<p><span class=nadruk>2/ Include free text clinical information:</span> <input type=radio name=incclin value=1 checked> Yes / <input type=radio name=incclin value=0>No</p>
<div style='margin-left:10px'>
  <TEXTAREA name="clinical" rows="10" style='width:95%' onfocus="if (this.value == 'Clinical Information') {this.value = '';}"><?php echo "$clinicaldata";?></TEXTAREA>
</div>
<p><span class=nadruk>2/ Include ontology based clinical information:</span> <input type=radio name=incclinontology value=1 checked> Yes / <input type=radio name=incclinontology value=0>No</p>
<div style='margin-left:10px'>
<?php 
$subarray = array();
$cnvdb = "CNVanalysis" . $_SESSION['dbname'];
$lddb = "LNDB";
mysql_select_db($cnvdb);
$subquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid' ORDER BY lddbcode");
while ($subrow = mysql_fetch_array($subquery)) {
	$code = $subrow['lddbcode'];
	$pieces = explode('.',$code);
	$subarray[$pieces[0]][$pieces[1]][] = $pieces[2];
}
mysql_select_db($lddb);
$subtablestring = '';
$match = 0;
foreach($subarray as $first => $sarray) {
	$ssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.00.00'");
	$ssubrow = mysql_fetch_array($ssubquery);
	$firstlabel = $ssubrow['LABEL'];
	foreach ($sarray as $second => $tarray) {
		if ($second == '00') {
			$subtablestring .= "<tr><td $firstcell>$firstlabel</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
		}
		else {
			$sssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.00'");
			$sssubrow = mysql_fetch_array($sssubquery);
			$secondlabel = $sssubrow['LABEL'];
			foreach ($tarray as $third) {
				if ($third == '00') {
					$subtablestring .= "<tr><td $firstcell>$firstlabel</td><td>$secondlabel</td><td>&nbsp;</td></tr>";	
				}
				else {
					$ssssubquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$first.$second.$third'");
					$ssssubrow = mysql_fetch_array($ssssubquery);
					$thirdlabel = $ssssubrow['LABEL'];
					$subtablestring.= "<tr><td $firstcell>$firstlabel</td><td>$secondlabel</td><td>$thirdlabel</td></tr>";
				}	
			}
		}
	}
}
echo "<table cellspacing=0 >";
echo "<tr><th $firstcell class=topcellalt>Primary</th><th class=topcellalt>Secondary</th><th class=topcellalt>Tertiary</th>\n";
echo "</tr>";
echo $subtablestring;
echo "</table>";
mysql_select_db($cnvdb);
?>

</div>
<p><span class=nadruk>3/ Show Karyogram:</span> <input type=radio name=inckaryo value=1> Yes / <input type=radio name=inckaryo value=0 checked>No<br/>
<span style='padding-left:10px'>The karyogram shows all chromosomes with markings for all aberrations included in the report. </span><br/><span style='padding-left:10px'>The same colorcode is used as seen elsewhere on the platform.</span></p>

<p><span class=nadruk>4/ Show Log:</span> <input type=radio name=inclog value=1 > Yes / <input type=radio name=inclog value=0 checked>No<br/>
<span style='padding-left:10px'>All changes to CNV information is stored. If selected, these changes will be added to the end of the report.</span>

<p><span class=nadruk>5/ Show Associated Publications:</span> <input type=radio name=incpub value=1 checked > Yes / <input type=radio name=incpub value=0 >No<br/>
<span style='padding-left:10px'>PubMed Publications can be associated to CNVs. If needed, these can be printed as a bibliography.</span>


<p><span class=nadruk>6/ Select Details to Show in CNV Table:</span></p>
<p>
<ul id=ul-simple>
 <li><input type=checkbox name=genomic checked>Minimal Genomic Region
 <li><input type=checkbox name=maxgenomic checked>Maximal Genomic Region
 <li><input type=checkbox name=cytoband checked>Cytoband
 <li><input type=checkbox name=size checked>Minimal Genomic Size
 <li><input type=checkbox name=maxsize checked>Maximal Genomic Size
 <li><input type=checkbox name=nrprobes>Number of probes
 <li><input type=checkbox name=nrgenes checked>Number of genes
 <li><input type=checkbox name=scores>Scores
 <!-- <li><input type=checkbox name=genes checked>Affected Genes -->
 <li><input type=checkbox name=inheritance>Inheritance (if defined)
 <li><input type=checkbox name=DC checked>Diagnostic Class (if defined)
</ul>
</p>
<p><span class=nadruk>7/ Gene List Settings</span></p>
<ul id=ul-simple>
 <li><input type=checkbox name=morbid checked>Include MORBID summary for listed genes (will be bold faced in list)</li>
 <li><input type=checkbox name='exclude_benign' checked>Exclude MORBID summaries from Benign CNVs</li>
 <li><input type=checkbox name='exclude_FP' checked>Exclude MORBID summaries from FP CNVs</li>
 <li><input type=checkbox name=genelarge checked>Include genes only present in maximal region (will be slanted in list)</li>
</ul>
</p>

<p><span class=nadruk>8/ Filter Results</span></p>
<p>
<table cellspacing=0>
<tr > 
	<td class=clear><input type=radio name=incnv value=1 checked> Yes / <input type=radio name=incnv value=0 >No</td><td class=clear> Include CNV regions in report</td>
</tr>
<tr >
<?php
	echo "<td class=clear><select name=maxclass>";
	echo "<option value='' selected>All (including non-specified)</option>";
	echo "<option value=" . $name_dc['Path'] . ">Pathogenic</option>";
	echo "<option value=" . $name_dc['Path (rec)'] . ">Pathogenic & Pathogenic recessive</option>";
	echo "<option value=" . $name_dc['L.Path'] . ">Pathogenic & Pathogenic recessive & Likely pathogenic</option>";
	echo "<option value=" . $name_dc['VUS'] . ">Pathogenic & VUS</option>";
	echo "<option value=" . $name_dc['Ben'] . ">Pathogenic & VUS & Benign</option>";
	echo "<option value=" . $name_dc['FP'] . ">Pathogenic & VUS & Benign & False Positive</option>";
	echo "</select></td><td class=clear> : On Diagnostic Class </td>";
 ?>
</tr>
<tr > 
 <td class=clear><input type=text name=minsnp value=10></td><td class=clear> : On Minimal Probe Coverage</td>
</tr>
<tr >
 <td class=clear><input type=text name=minsize value=0></td><td class=clear> : On Minimal Size (in bp)</td>
</tr>
<tr >
 <td class=clear><input type=text name=minqs value=8></td><td class=clear> : On Minimal QuantiSNP Score</td>
</tr>
<tr>
 <td class=clear><input type=text name=minpas value=10></td><td class=clear> : On Minimal PennCNV Score (Autosomal)</td>
</tr>
<tr>
 <td class=clear><input type=text name=minpxs value=0></td><td class=clear> : On Minimal PennCNV Score (X Chromosome)</td>
</tr>
</table>
</p>

<p><span class=nadruk>9/ Plink Results (LOH regions) Settings</span></p>
<div style='margin-left:10px'>
<p>LOH regions can be included in the report as a seperate table. Due to the typically very large size of these regions, additional settings can be applied.</p>
<p>
<table cellspacing=0>
<tr >
  <td class=clear><input type=radio name=incloh value=1 > Yes / <input type=radio name=incloh value=0 checked>No</td><td class=clear> Include regions in report</td>
</tr>
<tr>
  <td class=clear><input type=radio name=lohmorbid value=1 > Yes / <input type=radio name=lohmorbid value=0 checked>No</td><td class=clear> Include <u>recessive</u> genes in MORBID list</td>
</tr>
<tr>
  <td class=clear><input type=text name=lohminsize value='1,000,000' ></td><td class=clear>Minimal LOH size to include in report (Ignored if smaller than general LOH settings)</td>
</tr>
<tr >
<?php
	echo "<td class=clear><select name=lohmaxclass>";
	echo "<option value='' selected>All (including non-specified)</option>";
	echo "<option value=" . $name_dc['Path'] . ">Pathogenic</option>";
	echo "<option value=" . $name_dc['Path (rec)'] . ">Pathogenic & Pathogenic recessive</option>";
	echo "<option value=" . $name_dc['L.Path'] . ">Pathogenic & Pathogenic recessive & Likely pathogenic</option>";
	echo "<option value=" . $name_dc['VUS'] . ">Pathogenic & VUS</option>";
	echo "<option value=" . $name_dc['Ben'] . ">Pathogenic & VUS & Benign</option>";
	echo "<option value=" . $name_dc['FP'] . ">Pathogenic & VUS & Benign & False Positive</option>";
	echo "</select></td><td class=clear> : On Diagnostic Class </td>";
 ?>
</tr>
</table>
</p>

</div>
 
<p><span class=nadruk>10/ Set CNV specific Details:</span></p>
<div style='margin-left:10px'>
<p>Besides filtering based on the settings above, single CNVs can be excluded by deselecting them below.</p>
<p>
<table cellspacing=0>
<tr>
 <th <?php echo $firstcell; ?>><input type=checkbox name=showtoggle checked onclick="ShowSet()">Include</th>
 <th>Region</th>
 <th>CN</th>
 <th>Inh./D.C.</th>
 <th>Genes:<input type=radio name=genes value=N onClick="GeneSet('N')">N<input type=radio name=genes value=A checked onclick="GeneSet('A')">A</th>
</tr>

<?php 
ob_flush();
flush();
$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.cn, a.inheritance, a.class FROM aberration a WHERE a.sample = $sid AND a.idproj = $pid ORDER BY a.chr, a.start");
while ($row = mysql_fetch_array($query)) {
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
	foreach ($filter["1"][$chr] as $fstart => $farray) {
		if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $stop) {
			//$filtered++;
			continue 2;
		}
	}	
	$cn = $row['cn'];
	$aid = $row['id'];
	$inh = $row['inheritance'];
	$inh = $inhhash[$inh];
	if ($row['class'] == 0 || $row['class'] == ''){
		$class = '-';
	}
	else {
		$class = $dc[$row['class']];
	}
	$line = "<tr>\n";
	$line .= " <td $firstcell><input type=checkbox name=include[] value=$aid checked></td>\n";
	$line .= " <td >chr".$chromhash[$chr].":".number_format($start,0,'',',').'-'.number_format($stop,0,'',',')."</td>\n";
	$line .= " <td>$cn</td>\n";
	$line .= " <td>$inh / $class</td>\n";
	$line .= " <td><input type=radio name=cnvgenes_$aid value=N>N<input type=radio name=cnvgenes_$aid value=A checked>A</td>\n";
	$line .= "</tr>";
	echo $line;
}
?>

</table>
</p>
</div>

<p><span class=nadruk>11/ Set Mosaic CNV specific Details:</span></p>
<div style='margin-left:10px'>
<p>Besides filtering based on the settings above, single mosaic CNVs can be excluded by deselecting them below.</p>
<p>
<table cellspacing=0>
<tr>
 <th <?php echo $firstcell; ?>><input type=checkbox name=showtoggle onclick="mosShowSet()">Include</th>
 <th>Region</th>
 <th>CN</th>
 <th>D.C.</th>
 <th>Genes:<input type=radio name=genes value=N onClick="GeneSet('N')">N<input type=radio name=genes value=A checked onclick="GeneSet('A')">A</th>
</tr>

<?php 
ob_flush();
flush();
$query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.cn, a.class FROM aberration_mosaic a WHERE a.sample = $sid AND a.idproj = $pid ORDER BY a.chr, a.start");
while ($row = mysql_fetch_array($query)) {
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
	foreach ($filter["1"][$chr] as $fstart => $farray) {
		if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $stop) {
			//$filtered++;
			continue 2;
		}
	}	
	$cn = $row['cn'];
	$aid = $row['id'];
	if ($row['class'] == 0 || $row['class'] == ''){
		$class = '-';
	}
	else {
		$class = $dc[$row['class']];
	}
	$line = "<tr>\n";
	$line .= " <td $firstcell><input type=checkbox name=mosinclude[] value=$aid></td>\n";
	$line .= " <td >chr".$chromhash[$chr].":".number_format($start,0,'',',').'-'.number_format($stop,0,'',',')."</td>\n";
	$line .= " <td>$cn</td>\n";
	$line .= " <td>$class</td>\n";
	$line .= " <td><input type=radio name=mosgenes_$aid value=N>N<input type=radio name=mosgenes_$aid value=A checked>A</td>\n";
	$line .= "</tr>";
	echo $line;
}
?>

</table>
</p>
</div>



<?php
echo "<p><span class=nadruk>12/ Set LOH-region specific Details:</span></p>";
echo "<div style='margin-left:10px'>";
$minlohsize = $_SESSION['LOHminsize'] ;
$minlohsnps = $_SESSION['LOHminsnp'] ;
if ($_SESSION['LOHshow'] == 1) {
  echo "<p>If plink (LOH) regions were set to be included above, the following region-specific settings will be applied.</p>";
  echo "<p>Regions shown here are passed the following filters:<ol>";
  echo "<li> - Minimal Size of LOH Region : $minlohsize bps</li>";
  echo "<li> - Minimal snps in LOH Region : $minlohsnps</li>";
  echo "</ol><a href='index.php?page=settings&type=plots&st=res&v=s'>Change these settings</a></p>";
  echo "<p>";
  echo "<table cellspacing=0>";
  echo "<tr>";
  echo " <th $firstcell><input type=checkbox name=lohshowtoggle checked onclick='lohShowSet()'>Include</th>";
  echo " <th>Region</th>";
  echo " <th>Size</th>";
  echo " <th>D.C.</th>";
  echo " <th>Genes:<input type=radio name=lohgenes value=N onClick=\"lohGeneSet('N')\">N<input type=radio name=lohgenes value=A checked onclick=\"lohGeneSet('A')\">A</th>";
  echo "</tr>";

  ob_flush();
  flush();
  $query = mysql_query("SELECT a.id, a.chr, a.start, a.stop, a.class FROM aberration_LOH a WHERE a.sample = '$sid' AND a.idproj = '$pid' AND a.size >= '$minlohsize' AND a.nrsnps >= '$minlohsnps' ORDER BY a.chr, a.start");
  while ($row = mysql_fetch_array($query)) {
	$chr = $row['chr'];
	$start = $row['start'];
	$stop = $row['stop'];
	// skip hard negative filter matches (for now: only skip if CNV entirely in filtered region.)
	foreach ($filter["1"][$chr] as $fstart => $farray) {
		if ($fstart <= $start && $filter["1"][$chr][$fstart]['stop'] >= $stop) {
			$filtered++;
			continue 2;
		}
	}	
	$size = $stop - $start + 1;
	$aid = $row['id'];
	if ($row['class'] == 0 || $row['class'] == ''){
		$class = '-';
	}
	else {
		$class = $dc[$row['class']];
	}
	$line = "<tr>\n";
	$line .= " <td $firstcell><input type=checkbox name=lohinclude[] value=$aid checked></td>\n";
	$line .= " <td >chr". $chromhash[$chr].":".number_format($start,0,'',',').'-'.number_format($stop,0,'',',')."</td>\n";
	$line .= " <td >".number_format($size,0,'',',')."</td>";
	$line .= " <td>$class</td>\n";
	$line .= " <td><input type=radio name=lohgenes_$aid value=N>N<input type=radio name=lohgenes_$aid value=A checked>A</td>\n";
	$line .= "</tr>";
	echo $line;
  }
  echo "</table></p></div>";
}
else {
	echo "<p>LOH-Display disabled.</p></div>";
	echo "<input type=hidden name=lohinclude[] value=''>";
}
?>



<p><span class=nadruk>12/ Output Format:</span></p>
<div style='margin-left:10px'>
<p>Select the output format to use. 'PDF' generates a clean structured report that can be printed directly. 'CSV' generates a comma seperated file that can be opened in a program like MS Excel to reformat it to your own needs.</p>
<p><input type=radio name=format value='pdf' checked>PDF / <input type=radio name=format value='csv'>CSV</p>

</div>

<p><input type=submit class=button value='Create Report'></p>


</div>


<?php 

}
?>

<!-- functions that use values from php... -->
<script type='text/javascript'>
function LoadPreferences(subtype) {
	// put temporary response in place
	document.getElementById('infodiv').innerHTML = "<div style='text-align:center;height:100%;padding-top:15%'><img src='images/layout/loading.gif' height=50px /><br/><h3>Loading Preferences...</h3></div>";
	// set height
	var D = document;
    	var bodyh =  Math.max(
	        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
       	 	Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
       	 	Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    		);
	bodyh = bodyh*1 - 25*1; 
	//if (divh < bodyh) {
		document.getElementById('infodiv').style.height = bodyh+'px';
	//}
	// show the div
	document.getElementById('infodiv').style.display = '';
	// fetch the preferences pages with ajax
	xmlhttp=GetXmlHttpObject();
	if (xmlhttp==null)
	  {
	  alert ("Browser does not support HTTP Request");
	  return;
	  }
	var url="Browser/setplotprefs.php";
	url=url+"?subtype="+subtype;
	url=url+"&rv="+Math.random();
	xmlhttp.onreadystatechange=function ()
	{
		if (xmlhttp.readyState==4)
		{
			
			// start form to submit selection
			// add get variables to action
			<?php 
			$newcontent = "index.php?";
			foreach ($_GET as $gkey => $gval) {
				$newcontent .= "$gkey=$gval&";
			}
			$newcontent = substr($newcontent,0,-1);
			$newcontent = "<form action=\"$newcontent\"  method=POST>";
			// add hidden variable triggering db update
			$newcontent .= "<input type=hidden name=\"updatepreferences\" value=1 />";
			// add posted variables as hidden fields
			foreach ($_POST as $pkey => $pval) {
				$newcontent .= "<input type=hidden name=\"$pkey\" value=\"$pval\" />";
			}
			//print the newcontent
			echo "document.getElementById('infodiv').innerHTML='$newcontent'+xmlhttp.responseText+'</form>';";
			?>	
			// readjust height if needed
    			bodyh =  Math.max(
			        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
       	 			Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
		       	 	Math.max(D.body.clientHeight, D.documentElement.clientHeight)
		    		);
			bodyh = bodyh*1; 
			document.getElementById('infodiv').style.height = bodyh+'px';
			window.scrollTo(0,0);
		}
	}
	xmlhttp.open("GET",url,true);
	xmlhttp.send(null);
}
</script>

