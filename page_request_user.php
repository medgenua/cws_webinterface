<?php
$lastname = '';
if (isset($_POST['lastname'])) {
	$lastname = $_POST['lastname'];
}
$firstname = '';
if (isset($_POST['firstname'])) {
	$firstname = $_POST['firstname'];
}
$email = '';
if (isset($_POST['email'])) {
	$email = $_POST['email'];
}
$affiliation = '';
if (isset($_POST['affiliation'])) {
	$affiliation = $_POST['affiliation'];
}
if ($affiliation == 'other') {
	$affiliation = $_POST['NewAffiliation'];
	$newaffi =1;
}
else {
	$newaffi = 0;
}
$text = '';
if (isset($_POST['text'])) {
	$text = $_POST['text'];
}
$check = '';
if (isset($_POST['check'])) {
	$check = $_POST['check'];
}
$requsername = '';
$password = '';
$confirm = '';
if (isset($_POST['requsername'])) {
	$requsername = $_POST['requsername'];
	$password = $_POST['password'];
	$confirm = $_POST['confirm'];
}
if (isset($_POST['register']) ){
	if ($lastname == "Last Name" || $lastname == "" || $firstname == "First Name" || $firstname == "") 
	{
 		 echo "<div class=sectie><h3>No name specified</h3><p>Please enter your name.</p></div>";
 	}
	elseif (!preg_match("/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/", $email) || $email == "") 
 	{
  		echo "<div class=sectie><h3>Invalid email address</h3><p>Please correct your emailadres to a valid one.</p></div>";
 	}
	elseif ($affiliation == "Affiliation" || $affiliation == "") 
	{
		  echo "<div class=sectie><h3>No affiliation specified</h3><p>Please enter the university or company you work for.</p></div>";
 	}
	elseif ($requsername == "Requested User Name" || $requsername == "") 
	{
		  echo "<div class=sectie><h3>No username specified</h3><p>Please choose a username.</p></div>";
 	}
	elseif ($password == "") 
	{
		  echo "<div class=sectie><h3>No password specified</h3><p>Please choose your password.</p></div>";
	}
	elseif ($password != $confirm) {
		  echo "<div class=sectie><h3>Passwords don't match</h3><p>Please enter your password again and make sure the two fields match.</p></div>";
	}
		elseif ($check != "G" && $check != "g")
 		{
 		 echo "<div class=sectie><h3>Wrong answer</h3><p>Answer the question please. This is a simple anti-spam check. If you don't know the answer, it's \"G\". :-) </p></div>";
  	}
	elseif ($text == 'Please let us know why you would be interested in using CNV-WebStore' || $text == "")
 	{
 		 echo "<div class=sectie><h3>No motivation specified</h3><p>Please give a short motivation for your account, since the resources of the server are limited.</p></div>";
 	}
	else 
 	{
		 #######################
		# CONNECT to database #
		#######################
		// here, a user has not yet logged in, so fetch the common default database
		//$db = "CNVanalysis" . $_SESSION["dbname"];
		include('.LoadCredentials.php');
		mysql_select_db('GenomicBuilds');
		$query = mysql_query("SELECT name, StringName, Description FROM CurrentBuild LIMIT 1");
		$row = mysql_fetch_array($query);
		$_SESSION['dbname'] = $row['name'];
		$_SESSION['dbstring'] = $row['StringName'];
		$_SESSION['dbdescription'] = stripslashes($row['Description']);
		$db = "CNVanalysis" . $_SESSION["dbname"];
		mysql_select_db("$db");
		$result = mysql_query("SELECT id FROM users WHERE username = '". addslashes($requsername)."'");
		if (mysql_num_rows($result) > 0)
		{
			echo "<div class=sectie><h3>Username exists</h3><p>The user $requsername already exists. Please choose a different name.</p></div>";
		}
		else {
			$lastname = addslashes($lastname);
			$firstname = addslashes($firstname);
			$affiliation = addslashes($affiliation);
			$email = addslashes($email);
			$requsername = addslashes($requsername);
			$text = addslashes($text);
			if ($newaffi == 1) {
				mysql_query("INSERT INTO affiliation (name) VALUES('$affiliation')");
				$affid = mysql_insert_id();
				$affiliation = $affid;
			}
			$query = "INSERT INTO users (LastName, FirstName, Affiliation, email, username, password, motivation,level) VALUES ('$lastname', '$firstname', '$affiliation', '$email', '$requsername', MD5('$password'), '$text', '1')";
			mysql_query($query);
			$uid = mysql_insert_id();
			## get dblinks to activate (default = all)
			$query = mysql_query("SELECT id FROM `db_links`");
			$dblinks = '';
			while ($row = mysql_fetch_array($query)) {
				$dblinks .= $row['id'].",";
			}
			if ($dblinks != '') {
				$dblinks = substr($dblinks,0,-1);
			}
			## get tracks to activate (default = all)
			if (file_exists('Browser/Main_Configuration.xml')) {
				$Array = simplexml_load_file("Browser/Main_Configuration.xml");
			}
			elseif (file_exists('Main_Configuration.xml')) {
				$Array = simplexml_load_file("Main_Configuration.xml");
			}
			else {
				echo "ERROR: Main_Configuration.xml was not found<br/>";
				exit;
			}
			$tracks = '';
			foreach ($Array->{'tracks'}->children() as $ckey => $cval) {
				if ($Array->{'tracks'}->{$ckey}->{'enabled'} != 1) {
					continue;
				}
				$tracks .= $ckey.',';
			}
			if ($tracks != '') {
				$tracks = substr($tracks,0,-1);
			}
			mysql_query("INSERT INTO usersettings (id,db_links,tracks) VALUES ('$uid','$dblinks','$tracks')");
			$subject = "CNV-analysis : Account Request"; 
 			//compose message, etc
			$message = "Message sent from http://$domain\r";  
			$message .= "Sent by: $email\r";
			$message .= "Subject: $subject\r";
			$message .= "Requested username: $requsername\r";
			$message .= "Motivation: \r\r";
			$message .= "$text\r";
			$message .= "\r\rYou have now been granted with guest user permissions. This enables all the core functions of the platform.\r\r";
			$headers = "From: $email\r\n";
			$headers .= "To: $adminemail, $email\r\n";
			
			/* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
			
			if (mail($email,"$subject",$message,$headers)) {
	 
				echo "<h4>You have now been granted with guest user permissions. This enables all the core functions of the platform.</h4>";
				//echo "<p>The email has also been sent your own specified address.</p>";
				echo "<p><a href=\"index.php?page=main\">Back to Homepage</a></p>";
			} 
			else {
				  echo "<h4>Can't send email </h4>";
			}
			
		}
	}	
}
else {
?>

<div class=sectie>
<h3>Request a username</h3>
<p>Fill in the form below to request a username for the website (all fields are mandatory). Activation of the account can take some time, depending on how fast I notice the request and the time it takes to make a decision. The basic account will give access to the core data-analysis, browsing of the results and some of the less resource intensive modules available.</p>


<?php


if ($lastname == "")
	$lastname = "Last Name";
if ($firstname == "")
	$firstname = "First Name";
if ($email == "")
	$email = "Email";
if ($requsername == "")
	$requsername = "Requested User Name";
#if ($affiliation == "")
#	$affiliation = "Affiliation";
if ($text == "")
	$text = 'Please let us know why you would be interested in using CNV-WebStore';
?>

<form action="index.php?page=request_user" method="POST">
<table id=mytable cellspacing=0 >
<tr>
<th class=clear>Last Name:</td>
<td class=clear><input type="text" name="lastname" value="<?php echo $lastname ?>" size="40" maxlength="40" onfocus="if (this.value == 'Last Name') {this.value = '';}" /></td>
</tr>
<tr>
<th class=clear>First Name:</td>
<td class=clear><input type="text" name="firstname" value="<?php echo $firstname ?>" size="40" maxlength="40" onfocus="if (this.value == 'First Name') {this.value = '';}" /></td>
</tr>
<tr>
<th class=clear>Your email:</td>
<td class=clear><input type="text" name="email" value="<?php echo $email ?>" size="40" maxlength="50" onfocus="if (this.value == 'Email') {this.value = '';}" /></td>
</tr>
<tr class=clear>
<th class=clear> Affiliation:</td>
<td class=clear><select name=affiliation>
<?php
	if ($affiliation == 'other' || $affiliation == '' ) {
		echo "<option value='other'>Other, Specify below</option>\n";
	}
	$query = mysql_query("SELECT id, name FROM affiliation");
	while($row = mysql_fetch_array($query)) {
		$affid = $row['id'];
		$affname = $row['name'];
		if ($affid == $affiliation) {
			echo "<option value='$affid' SELECTED>$affname</option>\n";
		}
		else {
			echo "<option value='$affid'>$affname</option>\n";
		}
	}
	echo "</select>\n";
	if ($affiliation == '' || $affilation == 'other') {
		$affiliation = 'New Affiliation';
	}
?>
</tr>
<tr class=clear>
<th class=clear>&nbsp;</td>
<td class=clear> OR <input type="text" name="NewAffiliation" value="<?php echo $affiliation ?>" size="37" maxlength="255" onfocus="if (this.value == 'New Affiliation') {this.value = '';}" /></td>
</tr>
<tr class=clear>
<th class=clear>Requested Username:</td>
<td class=clear><input type="text" name="requsername" value="<?php echo $requsername ?>" size="40" maxlength="20" onfocus="if (this.value == 'Requested User Name') {this.value = '';}" /></td>
</tr>
<tr class=clear>
<th class=clear>Choose a Password:</td>
<td class=clear><input type="password" name="password" size="40" maxlength="20" /></td>
</tr>
<tr class=clear>
<th class=clear>Confirm Password:</td>
<td class=clear><input type="password" name="confirm" size="40" maxlength="20" /></td>
</tr>
<tr class=clear><th class=clear>DNA consists of A, C, T and ... ?</td>
<td class=clear><input type="text" name="check" value="You should know this" size = "40" onfocus="if (this.value == 'You should know this') {this.value = '';}"/></td>
</tr>
<tr>
<td colspan = 2 class=clear><textarea name="text" cols="100" rows="20" onfocus="if (this.value == 'Please let us know why you would be interested in using CNV-WebStore') {this.value = '';}"><?php echo $text ?></textarea></td>
</tr>
<tr> 
<td class=clear><input type="submit" name=register value=" Send " class=button ></td>
</tr>
</table>
</div>
<?php
}
?>
