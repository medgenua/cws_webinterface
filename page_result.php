<?php

#######################
# CONNECT to database #
#######################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

// get the currently running projects. 
$result = `ps aux | grep perl | grep MainProgram`;
$result = explode("\n",$result);
$runningpids = array();
foreach ($result as $string) {
	if (!preg_match('/MainProgram\.pl/',$string)) {
		continue;
	}
	$random = preg_replace('/(.*pl\s-r\s)(.{5})/','$2',$string);
	$fh = fopen("$scriptdir/output/$random.pid",'r');
	$pid = fgets($fh);
	rtrim($pid);
	$runningpids[$pid] = 1;
}


// During startup : print the currently running projects. 
$query = mysql_query("SELECT p.id, p.chiptype, p.created, p.userID, COUNT(ps.idsamp) AS samples, u.LastName, u.FirstName FROM project p JOIN projsamp ps JOIN users u ON p.id = ps.idproj AND p.userID = u.id WHERE p.finished = 0 GROUP BY ps.idproj ORDER BY p.created ASC");
echo "<div id=qoverview>";
echo "<span style='float:right'><a href='javascript:void(0);' onClick=\"divHide('qoverview')\">Hide</a> | <a href='javascript:void(0);' onClick=\"reloadqueue('qoverview')\">Refresh</a></span>";
echo "<div class=sectie>";
echo "<h3>Analysis Queue Overview</h3>";
if (mysql_num_rows($query) == 0) {
	echo "<p>There are no running projects. Your analysis will be able to start immediately.</p>";
}
else {
	echo "<p><table cellspacing=0 style='margin-left:3em;width:90%'>";
	echo "<tr>";
	echo "  <th class=light>Started</th>";
	if ($level > 2) {
		echo "<th class=light>Owner</th>";
	}
	echo "  <th class=light>ChipType</th>";
	echo "  <th class=light># Samples</th>";
	echo "</tr>";
	$printed = 0;
	while ($row = mysql_fetch_array($query)) {
		$chip = $row['chiptype'];
		$created = $row['created'];
		$user = $row['userID'];
		$samples = $row['samples'];
		$uname = $row['LastName'] . ' ' . $row['FirstName'];
		$bold = '';
		$pid = $row['id'];
		if (!array_key_exists($pid,$runningpids)) {
			#project is not running, update db.
			mysql_query("UPDATE project SET finished = 1 WHERE id = '$pid'");
			continue;
		}
		$printed++;

		if ($user == $userid) {
			$bold = "style='font-weight:bold;'";
		}
		echo "<tr>";
		echo "  <td class=light $bold>$created</td>";
		if ($level > 2) {
			echo "<td class=light $bold>$uname</td>";
		}
		echo "  <td class=light $bold>$chip</td>";
		echo "  <td class=light $bold>$samples</td>";
		if ($level > 2) {
			echo "<td class=light $bold><a href='index.php?page=browseruntimeoutput&p=$pid'>Switch To Output</a></td>";
		}
		echo "</tr>";
	}
	if ($level <= 2) {
		echo "<tr><td class=last colspan=3>&nbsp;</td></tr>";
	}
	else {
		echo "<tr><td class=last colspan=5>&nbsp;</td></tr>";
	}

	echo "</table></p>";
	if ($printed == 0) {
		echo "<p>There are no running projects. Your analysis will be able to start immediately.</p>";
	}

}
echo "</div>";
echo "</div>";
// print section title, project name etc
$type = $_GET['type'];
if ($_GET['type'] == 'multiple') {
	echo "<div id=tohide class=sectie style='text-align:center'><img src='images/content/ajax-loader.gif'><br/><p style='text-align:center'>Loading, please be patient.</p></div>";
	ob_flush();
	flush();
	$rand = $_GET['r'];
	## wait for the project id to be written to pid file.
	$tries = 0;
	$fh = fopen("$scriptdir/output/$rand.pid",'r');
	$pid = fgets($fh);
	rtrim($pid);
	while ($pid == '' && $tries < 12 ) {
		$tries++;
		sleep(5);
		$fh = fopen("$scriptdir/output/$rand.pid",'r');
		$pid = fgets($fh);
		rtrim($pid);
	}
	echo "<script type='text/javascript'>document.getElementById('tohide').style.display='none';</script>\n";
	if ($pid == '') {
		echo "<h3>ERROR : Project not started</h3>";
		echo "<p>The project has not been started correctly. Please notify the CNV-WebStore administrator by '$adminemail'</p>\n";
		echo "</div>";
		exit();
	}
	$statusfile = "status/status.$rand";
	$outfile = "$scriptdir/output/$rand.out";
	echo "<div class=sectie>";
	echo "<h3>Majority Vote Analysis Output</h3>\n";
	$suff = "multi";
	$det = "";
	$query = mysql_query("SELECT naam, collection FROM project WHERE id = '$pid'");
	$row = mysql_fetch_array($query);
	$projectname = $row['naam'];
	$pcol = $row['collection'];
	echo "<p><a href='index.php?page=browseruntimeoutput&p=$pid' target='_blank'>View the Runtime Output files</a></p>";

}
elseif ($_GET['type'] == 'quantisnp') {
	echo "<div class=sectie>";
	$statusfile = "status/status.$projectname";

	$projectname = $_GET['project'];
	$outfile = "$scriptdir/QuantiSNP/$projectname.out";
	echo "<h3>QuantiSNP Ouptut</h3>\n";
	$suff = "QuantiSNP";
	$det = "xml";
}
elseif ($_GET['type'] == 'penncnv') {
	echo "<div class=sectie>";
	$statusfile = "status/status.$projectname";
	$projectname = $_GET['project'];
	$outfile = "$scriptdir/PennCNV/$projectname.out";
	echo "<h3>PennCNV Output</h3>\n";
	$suff = "PennCNV";
	$det = "xml";
}
elseif ($_GET['type'] == 'faseg') {
	echo "<div class=sectie>";
	
	$statusfile = "status/status.$projectname";

	$projectname = $_GET['project'];
	$outfile = "$scriptdir/FASeg/$projectname.out";
	echo "<h3>FASeg Output</h3>\n";
	$suff = "FASeg";
	$det = "xml";
}
elseif ($_GET['type'] == 'vanillaice') {
	echo "<div class=sectie>";
	
	$statusfile = "status/status.$projectname";
	$projectname = $_GET['project'];
	$outfile = "$scriptdir/VanillaICE/$projectname.out";
	echo "<h3>VanillaICE Output</h3>\n";
	$suff = "VanillaICE";
	$det = "xml";
}
else {
	echo "<div class=sectie>";
	
	echo "<h3>No Type Specified</h3>";
	echo "<p>No analysis type was specified. This means that no output can be shown.</p>";
	exit();
}
// INITIAL OUTPUT // will be replaced when analysis is finished

echo "<div id='toptext'>";
echo "<p class=bold>Project $projectname Running...</p>\n";
echo "<p>The analysis output below will refresh every 10 seconds</p>\n";
  
if ($type == 'multiple') {
	echo "<p>You can view the incomplete results by clicking the button below. The resulting page must be manually refreshed.</p>\n";
	echo "<p><form action='index.php?page=overview&amp;p=$pid' method=POST><input type=submit class=button value='Results so far'></form></p>\n";
	echo "<p><span class=nadruk>New:</span> You will be notified by email when the project is finished. You can safely close this window.</p>";
}
echo "</div>";
echo "</div>";
// PROGRAM OUTPUT
echo "<div class=\"sectie\"><h3>Program Output:</h3>\n";
echo "<script type='text/javascript'>var intervalresults;intervalresults = setInterval(\"reloadresults('AlgoOutput','$outfile')\",10000)</script>\n";
echo "<p><pre id='AlgoOutput' class=scrollbarbox style='height:525px;'>Loading ...</pre>";
echo "<script type='text/javascript'>var intervalstatus;intervalstatus = setInterval(\"checkstatus('$statusfile','$projectname','$pid','$type','$suff','$det','$pcol')\",10000)</script>\n";
echo "\n</div>";

	
?>


