
<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Browse Analysis Results</h3>\n";
	include('login.php');
	echo "</div>\n";
}
else {
	if ($_GET['type'] == 'main' || !isset($_GET['type']) ){

		echo "<div class=sectie>\n";
		echo "<h3>Browse &amp; Manage Your Data</h3>\n";
		echo "<p>There are multiple ways to browse and manage your data, each with a specific function : </p>\n";
		echo "<span class=nadruk>Browse Projects</span>: Browse your results from Lab down to project, sample and finally aberration details.  This is handy if you known what your looking for, or if you want to sequentially go through multiple samples.</p>\n";
		echo "<p><span class=nadruk>Search on Sample</span>: If you know the sample ID, but not the project, or you want to check if a certain sample is analyzed in multiple projects, you can retrieve this information here.\n</p>";
		echo "<p><span class=nadruk>Search on Region/Gene</span>: Check if there are samples containing a specific aberration, and from here on link to the sample details. Searching is possible on UCSC-style region (chr7:21,741,335-32,487,687), cytoband notation (12q13-q14) or on RefSeq gene symbol (ATRX)\n</p> ";
		echo "<p><span class=nadruk>Search on phenotype</span>: Search for samples matching a specific phenotype, using the London Neurogenetics Database Ontology.</p>";
		echo "<p><span class=nadruk>Advanced Filtering</span>: Filter and browse data based on flexible criteria by constructing complex filter combinations.</p>";
		echo" <p><span class=nadruk>Permissions</span>: Manage your projects, share or unshare them with other users, or usergroups. Multiple projects can also be grouped into a single new one, and obsolete projects can be deleted.</p>\n";
		#echo "<p><span class=nadruk>Bookmark Files</span>: Bookmark files are xml files that can be imported into BeadStudio/GenomeStudio. These files are kept for about a month on the server and can be downloaded from here. (section only accessible from the menu on the left)\n</p> ";
		
		echo "</div>\n";
	}
	elseif ($_GET['type'] == 'tree') {
			include('inc_browse_tree.inc');
	}
	elseif ($_GET['type'] == 'sample') {
			include('inc_browse_sample.inc');
	}
	elseif ($_GET['type'] == 'region') {
			include('inc_browse_region.inc');
	}
	elseif ($_GET['type'] == 'xml') {
			include('inc_browse_xml.inc');
	}
	elseif ($_GET['type'] == 'illumina') {
			include('page_projects.php');
	}
	elseif ($_GET['type'] == 'pheno') {
			include('inc_browse_pheno.inc');
	}
	elseif ($_GET['type'] == 'filter') {
			include('inc_browse_filter.inc');
	}
}
