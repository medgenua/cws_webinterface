<?php
if (!isset($_POST['resetusersubmit'])) {
	echo "<div class=sectie>\n";
	echo "<h3>Reset your password</h3>\n";
	echo "<p>Please provide the username you'd like to assign a new password to. An email will be sent to the associated email adress to complete the process. You have 48 hours to complete this process or the provided link will become inactive. </p>\n";
	echo "<p>\n";
	echo "<form action='index.php?page=send_resetemail' method=POST>\n";
	echo "Username: <input type=text size=40 name=resetuser value='' autofill='off' />\n";
	echo "<input type=submit name=resetusersubmit value=submit class=button>\n";
	echo "</form>\n";
	echo "</p>\n";
	echo "</div>\n";
}
else {
	$resetuser = $_POST['resetuser'];
	 #######################
	# CONNECT to database #
	#######################
	$db = "CNVanalysis" . $_SESSION["dbname"];
	mysql_select_db("$db");
	
	$result = mysql_query("SELECT id, email,FirstName, LastName FROM users WHERE username = '$resetuser'");
	$row = mysql_fetch_array($result);
	$resetuid = $row['id'];
	$resetemail = $row['email'];
	$fname = $row['FirstName'];
	$lname = $row['LastName'];	
	if (mysql_num_rows($result) == 0) {
		echo "<div class=sectie>\n";
		echo "<h3>Password Reset Request Failed</h3>\n";
		echo "<p>The provided username ($resetuser) does not correspond to a registered user. Please provide a valid username</p>\n";
	}
	else {
		function random_gen($length)
		{
	  		$random= "";
	  		srand((double)microtime()*1000000);
	  		$char_list = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$char_list .= "abcdefghijklmnopqrstuvwxyz";
	  		$char_list .= "1234567890";
	  		// Add the special characters to $char_list if needed
			#$char_list .= "_";
	  		for($i = 0; $i < $length; $i++)
	  		{ 
	    			$random .= substr($char_list,(rand()%(strlen($char_list))), 1);
	  		}
	  		return $random;
		} 

		$linkkey = random_gen(20); 
		$resetkey = random_gen(10);
		$now = time();
		$subject = "CNV-analysis Page: Password Reset"; 
		//compose message, etc
		$message = "Message sent from http://$domain\r";  
		$message .= "\r";
		$message .= "A request was made to reset your password at the CNV-analysis page\r";
		$message .= "If you did not request a reset, please report this to $adminemail\r";
		$message .= "\r\r";
		$message .= "Open the link below to complete the reset:\n";
		$message .= "https://$domain/$basepath/index.php?page=resetpass&u=$resetuid&k=$linkkey\r\r";
		$message .= "Your 'Reset Key' is: $resetkey\r\r";
		$message .= "You have 48 hours to complete the reset.\r\r";
		$headers = "From: no-reply@$domain\r\n";
		$updateresult = mysql_query("UPDATE users SET resetkey = '$resetkey', resetdate=$now, linkkey='$linkkey' WHERE id = $resetuid");	
		/* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
		if (mail($resetemail,"$subject",$message,$headers)) {
			## also send to site admin 
			$message = "PASSWORD RESET REQUESTED BY $lname $fname ($resetuser)\r" . $message;
			$subject = "NOTICE: $subject";
			mail($adminemail,"$subject",$message,$headers);
		 	echo "<div class=sectie>\n";
			echo "<h3>Email sent</h3>\n";
			echo "<p>An email was sent to $resetemail with further instructions. Complete these steps within 48 hours.</p> ";
			echo "<p><a href=\"index.php?page=main\">Back to Homepage</a></p>";
			echo "</div>\n";
		} 
		else {
		  	echo "<h4>Can't send email </h4>";
		}
	}
}
?>

