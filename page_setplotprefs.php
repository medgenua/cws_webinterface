<?php

if (!isset($subtype)) {
	$subtype = '';
}


#######################
# CONNECT to database #
#######################
//$db = "CNVanalysis" . $_SESSION["dbname"];
//mysql_select_db("$db");
$uid = $userid;
// GET POSTED VARS
//$uid = $_POST['uid'];
if ($subtype == '') {
	$subtype = $_POST['st'];
}
if ($subtype == 'res') {
	$showloh = $_POST['showloh'] || 0;
	$minsizeloh = $_POST['minsizeloh'] ;
	$minsnpsloh = $_POST['minsnpsloh'] ;
	$Condense = $_POST['condense'] || 0;
	$ToCondense = $_POST['tocondense'];
	if ($ToCondense == '') {
		$ToCondense = 0;
	}
	$NullCondense = $_POST['nullcondense'] || 0;
	$_SESSION['LOHshow'] = $showloh;
	$_SESSION['LOHminsize'] = $minsizeloh;
	$_SESSION['LOHminsnp'] = $minsnpsloh;
	$_SESSION['Condense'] = $Condense;
	$_SESSION['ToCondense'] = $ToCondense;
	$_SESSION['NullCondense'] = $NullCondense;
	mysql_query("UPDATE usersettings SET LOHshow = '$showloh', LOHminsize = '$minsizeloh', LOHminsnp = '$minsnpsloh', condense = '$Condense', condense_classes = '$ToCondense', condense_null = '$NullCondense' WHERE id = '$userid'");
}
elseif ($subtype == 'ann') {
	$RefSeq = $_POST['RefSeq'] || 0;
	$GCcode = $_POST['GCcode'] || 0;
	$GCncode = $_POST['GCncode'] || 0;
	$SegDup = $_POST['SegDup'] || 0;
	$DECsyn = $_POST['DECsyn'] || 0;
	$DECpat = $_POST['DECpat'] || 0;
	$ecaruca = $_POST['ecaruca'] || 0;
	$ecarucaSize = $_POST['ecarucaSize'];
	$annotations = $_POST['userAnn'];
	if ($RefSeq == '') {
		$RefSeq = 0;
	}
	if ($GCcode == '') {
		$GCcode = 0;
	}
	if ($GCncode == '') {
		$GCncode = 0;
	}
	if ($SegDup == '') {
		$SegDup = 0;
	}
	if ($DECsyn == '') {
		$DECsyn = 1;
	}
	if ($DECpat == '') {
		$DECpat = 1;
	}
	if ($ecaruca == '') {
		$ecaruca = 1;
	}
	if ($ecarucaSize == '') {
		$ecarucasize = 30000000;
	}
	$includestring = '';
	foreach ($annotations as $key => $val) {
		$includestring .= $val . ',';
	}
	if ($includestring != '') {
		$includestring = substr($includestring, 0,-1);
	}
	// set session vars
	$_SESSION['RefSeq'] = $RefSeq;
	$_SESSION['GCcode'] = $GCcode;
	$_SESSION['GCncode'] = $GCncode;
	$_SESSION['SegDup'] = $SegDup;
	$_SESSION['DECsyn'] = $DECsyn;
	$_SESSION['DECpat'] = $DECpat;
	$_SESSION['ecaruca'] = $ecaruca;
	$_SESSION['ecarucaSize'] = $ecarucaSize;
	$_SESSION['annotations'] = $includestring;
	// update table
	mysql_query("UPDATE usersettings SET RefSeq = '$RefSeq', GCcode = '$GCcode', GCncode = '$GCncode',SegDup = '$SegDup',DECsyn = '$DECsyn', DECpat = '$DECpat', ecaruca = '$ecaruca', ecarucaSize = '$ecarucaSize' , annotations = '$includestring' WHERE id = '$uid'");
}
elseif ($subtype == 'con') {
	//$DGVshow = $_POST['DGV_show'] || 0;
	$DGVinc = $_POST['DGVinc'];
	$HapSame = $_POST['HapSame'] || 0;
	$HapAll = $_POST['HapAll'] || 0;
	if ($DGVshow == '') {
		$DGVshow = 0;
	}
	if ($HapSame == '') {
		$HapSame = 0;
	}
	if ($HapAll == '') {
		$HapAll = 0;
	}
	$_SESSION['DGVshow'] = $DGVshow;
	$_SESSION['HapSame'] = $HapSame;
	$_SESSION['HapAll'] = $HapAll;
	$includestring = '';
	sort($DGVinc);
	foreach ($DGVinc as $key => $val) {
		$includestring .= $val . ',';
	}
	if ($includestring != '') {
		$includestring = substr($includestring, 0,-1);
	}
	$_SESSION['DGVinclude'] = $includestring;
	mysql_query("UPDATE usersettings SET DGVinclude = '$includestring', HapSame = '$HapSame', HapAll = '$HapAll' WHERE id = '$uid'");

}
elseif ($subtype == 'publ') {
	$dblinks = $_POST['dblinks'];
	$string = '';
	foreach($dblinks as $dbid ) {
		$string .= "$dbid,";
	}
	$string = substr($string, 0,-1);
	$_SESSION['db_links'] = $string;
	mysql_query("UPDATE usersettings SET db_links = '$string' WHERE id = '$uid'");


}
// SET EMTPY VALUES TO ZERO 
// UPDATE SESSION VARS
// CREATE INCLUDE STRING 
// UPDATE DATABASE

echo "<meta http-equiv='refresh' content='0;URL=index.php?page=settings&type=plots&st=$subtype&v=s'>\n";
?> 
