<?php 
ob_start();
if ($loggedin != 1) {
	echo "<div class=sectie>\n<h3>Settings pages not accessible</h3>\n";
	include('login.php');
	echo "</div>\n";
}
else{
	#include('inc_top_settings.inc');
	if ($_GET['type'] == "cnv") {
		include('inc_settings_cnv.inc');
	}
	elseif ($_GET['type'] == "prior") {
		include('inc_settings_prior.inc');
	}
	elseif ($_GET['type'] == "plots") {
		include('inc_settings_plots.inc');
	}
	else {
		echo "<div class=sectie>\n";
		echo "<h3>Runtime settings</h3>\n";
		echo "<p>These pages give you an overview of the runtime analysis parameters. By selecting a category on the left you can access CNV-calling settings, gen prioritization settings and download some auxilary files for the supported chiptypes.</p>\n";
		echo "<p>If you have sufficient rights, you can change some of these settings and add support for new chiptypes.</p>\n";
		echo "</div>\n";
	}
}
?>
