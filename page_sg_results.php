
<?php
////////////////////////////////
// Variables for table layout //
////////////////////////////////

  $tdtype= array("","class=alt");
  $thtype= array("class=spec","class=specalt");
  $topstyle = array("class=topcell","class=topcellalt");


//////////////
// GET DATA //
//////////////
$seperator = $_POST['seperator'];
if ($seperator == "|") { 
	$seperator = "%7C";
}
//echo "seperator = $seperator <br>\n";
$data = file_get_contents('php://input');
$attributes = explode("&", $data);
$num = count($attributes);
for ($i = 1; $i< $num; $i++) {
  $pieces1 = explode("=", $attributes[$i]);
  $value = $pieces1[1];
  $pieces2 = explode($seperator, $pieces1[0]);
  $type = $pieces2[0];
  switch ($type) {
	case "inc":

		$cts["$pieces2[1]"]["$pieces2[2]"][]=$pieces2[3];
		//$_SESSION["i_$pieces2[1]"."_$pieces2[2]"."_".sizeof($cts["$pieces2[1]"]["$pieces2[2]"])] = 1;
		//echo "i_$pieces2[1]"."_$pieces2[2]"."_".(sizeof($cts["$pieces2[1]"]["$pieces2[2]"]))."<br>";
    		//echo "type equals inc<br>";
    		break;

  	case "samp":
    		
		if ($value == "controle" ) {
			$controles[] = "$pieces2[1]";
			//$_SESSION["c_$pieces2[1]"] = 1;
			//$_SESSION["p_$pieces2[1]"] = 0;
		}
		else {
			$patients[] = "$pieces2[1]";
			//$_SESSION["p_$pieces2[1]"] = 1;
			//$_SESSION["c_$pieces2[1]"] = 0;
		}
		//echo "type equals samp<br>";
    		break;
	case "gen":
    		
		if ($value == "target" ) {
			$targets[]="$pieces2[1]";
			//$_SESSION["t_$pieces2[1]"] = 1;
			//$_SESSION["r_$pieces2[1]"] = 0;
		}
		else {
			$references[] = "$pieces2[1]";
			//$_SESSION["r_$pieces2[1]"] = 1;
			//$_SESSION["t_$pieces2[1]"] = 0;
		}
		//echo "type equals gen<br>";
    		break;
	 case "par":
		
		if ($value != "index") {
		  $sample = $pieces2[1];
		  $parents[$value][] = $sample;
		  //$_SESSION["o_$sample"] = $value;
  		}
		elseif (!array_key_exists("$pieces2[1]", $parents)) {
			$indexpatient = $pieces2[1];
			$parents["$indexpatient"] = array();
			//$_SESSION["o_$indexpatient"] = "";
		}
		break;
		    
  }
		

}
//////////////////////////////////////////////////////
// CALCULATE ALL AVERAGE CT's  AND dCT's per sample //
//////////////////////////////////////////////////////

foreach ($patients as $name) {
	foreach( $cts["$name"] as $gene => $value) {
		$av = average($cts["$name"]["$gene"]);
		//echo "Sample: $name - gen: $gene - average: $av<br>\n";
		$av_pat["$name"]["$gene"] = $av;
	}
	$tmp = array();			
	foreach ($references as $gene) {
		if (array_key_exists("$gene", $av_pat["$name"])) {
			$tmp[] = $av_pat["$name"]["$gene"];
		}
	}
	foreach($cts["$name"] as $gene => $value) {
		$dct = $av_pat["$name"]["$gene"] - average($tmp) ;
		$dct_pat["$name"]["$gene"] = $dct;
		//echo "sample: $name - gene: $gene - dct: $dct<br>\n";
		
	}
}

foreach ($controles as $name) {
	foreach( $cts["$name"] as $gene => $value) {
		$av = average($cts["$name"]["$gene"]);
		$av_cont["$name"]["$gene"] = $av;
	}			
	$tmp = array();			
	foreach ($references as $gene) {
		if (array_key_exists("$gene", $av_cont["$name"])) {
			$tmp[] = $av_cont["$name"]["$gene"];
		}
	}
	foreach ( $cts["$name"] as $gene => $value) {
		$dct = $av_cont["$name"]["$gene"] - average($tmp); 
		$dct_cont["$name"]["$gene"] = $dct;
		//echo "sample: $name - gene: $gene - dct: $dct<br>\n";
		
	}

}

//////////////////////////////////////////
// CALCULATE AVERAGE dCT OVER CONTROLES //
//////////////////////////////////////////

foreach ($references as $gene) {
	$tmp = array();
	foreach($controles as $name) {
		if (array_key_exists("$gene", $dct_cont["$name"])) {
			$tmp[] = $dct_cont["$name"]["$gene"];
		}
	}
	$av_dct_cont["$gene"] = average($tmp);
	//echo "gen: $gene - av_dc_cont: $av_dct_cont[$gene]<br>\n";
}
foreach ($targets as $gene) {
	$tmp = array();
	foreach($controles as $name) {
		if (array_key_exists("$gene", $dct_cont["$name"])) {
			$tmp[] = $dct_cont["$name"]["$gene"];
		}
	}
	$av_dct_cont["$gene"] = average($tmp);
	//echo "gen: $gene - av_dc_cont: $av_dct_cont[$gene]<br>\n";
}

/////////////////////////////////////////////////////////////
// CALCULATE ddCT AND CopyNumber FOR ALL SAMPLES AND GENES //
/////////////////////////////////////////////////////////////

foreach ($dct_cont as $sample => $value1) {
	foreach ($dct_cont["$sample"] as $gene => $dct) {
		$ddct_cur = $dct - $av_dct_cont["$gene"];
		$ddct["$sample"]["$gene"] = $ddct_cur;
		$cn_cur =  2*pow(2,-$ddct_cur);
		$cn["$sample"]["$gene"] = $cn_cur;
		//echo "sample: $sample - gene: $gene - CN: $cn_cur<br>\n";
	}
}

foreach ($dct_pat as $sample => $value1) {
	foreach ($dct_pat["$sample"] as $gene => $dct) {
		$ddct_cur = $dct - $av_dct_cont["$gene"];
		$ddct["$sample"]["$gene"] = $ddct_cur;
		$cn_cur =  2*pow(2,-$ddct_cur);
		$cn["$sample"]["$gene"] = $cn_cur;
		//echo "sample: $sample - gene: $gene - CN: $cn_cur<br>\n";
	}
}

//////////////////////////////
// PRINT RESULTS TO WEBPAGE //
//////////////////////////////
echo "<div class=sectie>\n";
echo "<h3>SybrGreen qPCR Allele Estimates</h3>\n";
echo "<p></p>\n";
echo "<form action=index.php?page=sybrgreencheck method=POST>\n";
foreach ($_POST as $k => $s) {
	echo "<input type=hidden name=$k value=$s>\n";
} 
echo "<input type=hidden name=backfromsg value=1>\n";
echo "<input type=hidden name=seperator value=\"$seperator\">\n";     
echo "<p><input type=submit class=button value=\"Change Settings\"></p>\n";
echo "</form>\n";
echo "<p>Use ONLY the button above to change your selections. Do NOT use the back-button of the browser !!</p>\n";
echo "</p>\n";
echo "</div>\n"; 

foreach ($patients as $name) {
  $switch=0;
  if (array_key_exists($name, $parents)) {
	$nrgenes = count($cn["$name"]);
	echo "<div class=sectie>\n";
	echo "<h3>Results for Index patient: $name</h3>\n";
	echo "<p>\n";
	echo "<table id=mytable cellspacing=0>\n";
	echo " <tr>\n";
	echo "  <th class=nobg>&nbsp;</th>\n";
	foreach ($cn["$name"] as $gene => $value) {
		echo "  <th scope=col $topstyle[1]>$gene</th>\n";
	}
	echo " </tr>\n";
	echo " <tr>\n";
	echo "  <th scope=row $thtype[1]>$name (index)</td>\n";
	foreach ($cn["$name"] as $gene => $value) {
		$value = format_number($value);
		$class = getclass($value);
		if ($class == "") {
			$class = $tdtype[$switch];
		}
		echo "  <td $class>$value</td>\n";
	}
	echo "</tr>\n";
	$switch = $switch + pow(-1,$switch);
	foreach ($parents[$name] as $child =>$parname) {
		//echo "parname= $parname<br>";
		echo " <tr>\n";
		echo "  <th scope=row $thtype[1]>$parname (parent)</th>\n";
		foreach ($cn["$parname"] as $gene => $value) {
			$value = format_number($value);
			$class = getclass($value);
			if ($class == "") {
				$class = $tdtype[$switch];
			}
			echo "<td $class>$value</td>\n";
		}
		echo "</tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
	echo "</table>\n";
	echo "</div>\n";	
  }
}

$switch=0;
echo "<div class=sectie>\n";
echo "<h3>Results for Controles</h3>\n";
echo "<p>\n";
echo "<table id=mytable cellspacing=0>\n";
echo " <tr>\n";
echo "  <th class=nobg>&nbsp;</th>\n";
$tmpname = $controles[0];
foreach ($cn["$tmpname"] as $gene => $value) {
	echo "  <th scope=col $topstyle[1]>$gene</th>\n";
}
echo "</tr>\n";	
foreach ($controles as $name) {
	echo " <tr>\n";
	echo "  <th scope=row $thtype[1]>$name</th>\n";
	foreach ($cn["$name"] as $gene => $value) {
		$value = format_number($value);
		$class = getclass($value);
		if ($class == "") {
			$class = $tdtype[$switch];
		}
		echo "<td $class>$value</td>\n";
	}
	echo "</tr>\n";
	$switch = $switch + pow(-1,$switch);	
}
echo "</table>\n";
echo "</p>\n";
echo "</div>\n";

///////////////
// FUNCTIONS //
///////////////

function average($a){
  return array_sum($a)/count($a) ;
}

function format_number($str,$decimal_places='2',$decimal_padding="0"){
        /* firstly format number and shorten any extra decimal places */
        /* Note this will round off the number pre-format $str if you dont want this fucntionality */
        $str           =  number_format($str,$decimal_places,'.','');     // will return 12345.67
        $number       = explode('.',$str);
        $number[1]     = (isset($number[1]))?$number[1]:''; // to fix the PHP Notice error if str does not contain a decimal placing.
        $decimal     = str_pad($number[1],$decimal_places,$decimal_padding);
        return (float) $number[0].'.'.$decimal;
}


function getclass($value) {
	$value = floatval($value);
	switch($value) {
	   case $value <= 0.3:
		$class = "class=sg_red";
		return $class;
		break;
	   case $value <= 1.25 && 0.75 <= $value :
		$class = "class=sg_purple";
		return $class;
		break;
	   case 1.75 <= $value && $value <= 2.25:
		$class = "";
		return $class;
		break;
	   case 2.8 <= $value :
		$class = "class=sg_green";
		return $class;
		break;
	   default: 
		return "class=sg_yellow";
		break;
	}
}

?>

