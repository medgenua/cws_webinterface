<?php
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

$chrtxt = $_GET['chr'];
$chr = $chromhash[ $chrtxt ];
$start = $_GET['start'];
$end = $_GET['end'];
$type = $_GET['type'];
$case = $_GET['case'];

$query = mysql_query("SELECT * FROM sample WHERE id = $case");
$sampelrow = mysql_fetch_array($query);
$patient = $sampelrow['chip_dnanr'];

echo "<div class=sectie>\n";
echo "<h3>Decipher $type overlapping chr$chrtxt:". number_format($start,0,'',',').'-'. number_format($end,0,'',',')."</h3>\n";
if ($case != '') {
	// echo "<h4>Observed in $case<h4>\n";
	echo "<h4>Observed in $patient<h4>\n";
}
echo "<p>";
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

// SYNDROMES 
if ($type == "syndromes") {
	echo "<table cellspacing=0>\n";
	$switch=0;
	echo " <tr>\n"; 
	echo "  <th scope=col class=topcellalt $firstcell>Location</th>\n";
	echo "  <th scope=col class=topcellalt>CN</th>\n";
	echo "  <th scope=col class=topcellalt>Name</th>\n";
	echo "  <th scope=col class=topcellalt>Phenotype</th>\n";
	echo " </tr>\n";

	$query = mysql_query("SELECT Chr, Start, stop, Variance, Name, Phenotype FROM decipher_syndromes WHERE chr = \"$chr\" AND ((start >= \"$start\" AND start <= \"$end\") OR (stop >= \"$start\" AND stop <= \"$end\") OR (start <= \"$start\" AND stop >= \"$end\")) ");
	while ($row = mysql_fetch_assoc($query)) {
    		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>chr". $chromhash[ $row['Chr'] ].":" . number_format($row['Start'],0,'',',') . "-" . number_format($row['End'],0,'',',') . "</td>\n";
    		echo "  <td $tdtype[$switch] >".$row['Variance']."</td>\n";
   		echo "  <td $tdtype[$switch] >".$row['Name']."</td>\n";
   		echo "  <td $tdtype[$switch] >".$row['Phenotype']."</td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
echo "</table>\n</div></div></body>\n</html>\n";
}

// PATIENTS
if ($type == "patients") {
	echo "<table cellspacing=0>\n";
	$switch=0;
	echo " <tr>\n"; 
	echo "  <th scope=col class=topcellalt $firstcell>Location</th>\n";
	echo "  <th scope=col class=topcellalt>CN</th>\n";
	echo "  <th scope=col class=topcellalt>Origin</th>\n";
	echo "  <th scope=col class=topcellalt>Phenotype</th>\n";
	echo "  <th scope=col class=topcellalt>Link</th>\n";
	echo " </tr>\n";
	$query = mysql_query("SELECT chr, start, stop, type, inheritance, phenotype,caseid FROM decipher_patients WHERE chr = \"$chr\" AND ((start >= \"$start\" AND start <= \"$end\") OR (stop >= \"$start\" AND stop <= \"$end\") OR (start <= \"$start\" AND stop >= \"$end\")) "); 
	while ($row = mysql_fetch_assoc($query)) {
    		echo " <tr>\n";
		echo "  <td $tdtype[$switch] $firstcell>chr". $chromhash[ $row['chr'] ].":" . number_format($row['start'],0,'',',') . "-" . number_format($row['stop'],0,'',',') . "</td>\n";
    		echo "  <td $tdtype[$switch] >".$row['type']."</td>\n";
   		echo "  <td $tdtype[$switch] >".$row['inheritance']."</td>\n";
		$pheno = $row['phenotype'];
		$pheno = str_replace(';','<br/>',$pheno);
		#$pheno = substr($pheno,0,-5);
   		echo "  <td $tdtype[$switch] >$pheno</td>\n";
		$link = "https://decipher.sanger.ac.uk/patient/".$row['caseid'];
		echo "  <td $tdtype[$switch] ><a href=\"$link\" target=new>Link</a></td>\n";
		echo " </tr>\n";
		$switch = $switch + pow(-1,$switch);
	}
echo "</table>\n</div></div></body>\n</html>\n";
}
	


?>
