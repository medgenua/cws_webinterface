<?php
if ($loggedin != 1) {
	include('login.php');
}
else {

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inhhash = array(0=>'-', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

$name_dc = array();
$dc_query = mysql_query("SELECT id, name, abbreviated_name, sort_order AS class_sorted FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$name_dc[$row['abbreviated_name']] = $row['class_sorted'];
}


$encpass = 'tDgAdrgDLzQ1Mj3DJ1KbiIao5sXkPu4FEF';
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";

echo "<div class=sectie>\n";
echo "<h3>Extract Filtered results</h3>\n";
echo "<h4>Choose Filter Settings</h4>";
echo "<p>Set your preferences below and press submit to extract a .TXT file with the extracted results. This file can be opened in Excel. The tables will contain sample name, chr, region, copynumber, diagnostic class, inheritance, chiptype and any of the items specified below. </p>\n";

echo "<p><form action='index.php?page=stats_step2' method=POST>\n";

// heading
echo "<table cellspacing=0>\n";
echo "<tr><th colspan=3 $firstcell>DETAILS TO SHOW</th></tr>\n";
echo "<tr>\n";
echo " <th class=topcell $firstcell>USE</th>\n";
echo " <th class=topcell>Feature</th>\n";
echo " <th class=topcell>Details</th>\n";
echo "</tr>\n";

// rows

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=cytoband CHECKED></td>\n";
echo " <td NOWRAP>Cytoband</td>\n";
echo " <td>Show cytoband instead of chromosome (standard)</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=chiptype></td>\n";
echo " <td NOWRAP>Chiptype</td>\n";
echo " <td>Type of microarray chip used</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=region CHECKED></td>\n";
echo " <td NOWRAP>Region</td>\n";
echo " <td>Show minimal region</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=class CHECKED></td>\n";
echo " <td NOWRAP>Class</td>\n";
echo " <td>Show Diagnostic Class of the CNV</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=inheritance></td>\n";
echo " <td NOWRAP>Inheritance</td>\n";
echo " <td>Show the CNV's inheritance pattern</td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=size CHECKED></td>\n";
echo " <td NOWRAP>Size</td>\n";
echo " <td>Genomic Size in basepairs</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=maxregion CHECKED></td>\n";
echo " <td NOWRAP>Maximal Region</td>\n";
echo " <td>Show maximal region next to minimal region</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=logr></td>\n";
echo " <td NOWRAP>Avg LogR</td>\n";
echo " <td>Mean logR value of probes in the CNV region</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=genes></td>\n";
echo " <td NOWRAP>Nr. Genes</td>\n";
echo " <td>Number of Affected RefSeq Genes</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=probes></td>\n";
echo " <td NOWRAP>Nr. Probes</td>\n";
echo " <td>Number of Probes in the CNV</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=genelist CHECKED></td>\n";
echo " <td NOWRAP>Gene List</td>\n";
echo " <td>List of Affected Genes by RefSeq Symbol (based on minimal region)</td>\n";
echo "</tr>\n";
echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=clinic></td>\n";
echo " <td NOWRAP>Clinical</td>\n";
echo " <td>A combined list of structured clinical data. </td>\n";
echo "</tr>\n";

//echo "<tr>\n";
//echo " <td $firstcell><input type=checkbox name= CHECKED></td>\n";
//echo " <td NOWRAP> </td>\n";
//echo " <td> </td>\n";
//echo "</tr>\n";
echo "<tr><th colspan=3 $firstcell >FILTER SETTINGS</th></tr>\n";
echo "<tr>\n";
echo " <th class=topcell $firstcell>USE</th>\n";
echo " <th class=topcell>VALUE</th>\n";
echo " <th class=topcell>ITEM</th>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=classfilter CHECKED></td>\n";
echo " <td NOWRAP><select name=classvalue>";
// echo "   	<option value='ANY'>All (including non-specified)</option>";
// echo "   	<option value='1,2'>Only Pathogenic</option>";
// echo "		<option value='1,2,3'>Pathogenic & Unknown Significance</option>";
// echo "		<option value='1,2,3,6' selected>Pathogenic (incl. Pathogenic Recessive) & Unknown Significance</option>";
// echo "		<option value='1,2,3,4'>Pathogenic, Unknown & Benign</option>";

echo "		<option value='ANY'>All (including non-specified)</option>";
echo "		<option value=" . $name_dc['Path'] . ">Pathogenic</option>";
echo "		<option value=" . $name_dc['Path (rec)'] . ">Pathogenic & Pathogenic recessive</option>";
echo "		<option value=" . $name_dc['L.Path'] . ">Pathogenic & Pathogenic recessive & Likely pathogenic</option>";
echo "		<option value=" . $name_dc['VUS'] . " selected>All Pathogenic & VUS</option>";
echo "		<option value=" . $name_dc['Ben'] . ">All Pathogenic & VUS & Benign</option>";
echo "		<option value=" . $name_dc['FP'] . ">All Pathogenic & VUS & Benign & False Positive</option>";


echo "</select></td>\n";
echo " <td>Only retain CNVs from the specified diagnostic classes</td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=inheritancefilter CHECKED></td>\n";
echo " <td NOWRAP><select name=inheritancevalue>";
echo "	<option value='0,1,2,3' selected>All (including non-specified)</option>";
echo "	<option value='3'>De Novo</option>";
echo " 	<option value='1,2'>Inherited</option>";
echo " 	<option value='1,2,3'>Any (but specified)</option>";
echo "</select></td>\n";
echo " <td>Only retain CNVs from the specified inheritance types </td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=minsnp CHECKED></td>\n";
echo " <td NOWRAP><input type=text name=minsnpvalue value=3 size=10></td>\n";
echo " <td>Minimal Number of Probes </td>\n";
echo "</tr>\n";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=minsize CHECKED></td>\n";
echo " <td NOWRAP><input type=text name=minsizevalue value=0 </td>\n";
echo " <td>Retain only CNVs larger than the specified size (in basepairs) </td>\n";
echo "</tr>\n";


echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=copynumber CHECKED></td>\n";
echo " <td NOWRAP><select name=copynumbervalue>";
echo "<option value='0,1,2,3,4' selected>Any</option>";
echo "<option value='0,1'>Loss</option>";
echo "<option value='3,4'>Gain</option>";
echo "<option value='1'>Hemizygous loss</option>";
echo "<option value='0'>Homozygous loss</option>";
echo "</select></td>"; 
echo " <td>Retain only CNVS of the specified copynumbers </td>\n";
echo "</tr>\n";
echo" <tr>\n";
echo " <td $firstcell><input type=checkbox name=intrack CHECKED></td>\n";
echo " <td NOWRAP>Track Only</td>";
echo " <td>Retain only CNVs from samples that show up in the graphical browser tracks. Uncheck to include CNVs from samples including low QC samples etc.</td>";
echo "</tr>";

echo" <tr>\n";
echo " <td $firstcell><input type=checkbox name=Plink></td>\n";
echo " <td NOWRAP>Include Plink Results</td>";
echo " <td>Include Results from LOH (Plink) analysis.</td>";
echo "</tr>";

echo" <tr>\n";
echo " <td $firstcell><input type=checkbox name=UPD></td>\n";
echo " <td NOWRAP>Include UPD Results</td>";
echo " <td>Include Results from UPD (SNPTrio) analysis.</td>";
echo "</tr>";

echo" <tr>\n";
echo " <td $firstcell><input type=checkbox name=BAFseg></td>\n";
echo " <td NOWRAP>Include BAFseg Results</td>";
echo " <td>Include Results from Mosaicism (BAFseg) analysis.</td>";
echo "</tr>";

echo" <tr>\n";
echo " <td $firstcell><input type=checkbox name=controls></td>\n";
echo " <td NOWRAP>Controls</td>";
echo " <td>Include CNVs from samples marked as Control. These data are diplayed in the HapMap Control track.</td>";
echo "</tr>";

echo "<tr>\n";
echo " <td $firstcell><input type=checkbox name=setprojects CHECKED></td>\n";
echo " <td NOWRAP>Project</td>\n";
echo " <td>Retain only CNVs stored in certain experiments. These will be presented in an extra step.</td>\n";
echo "</tr>\n";
echo "</table>\n";

echo "</p><p><input type=submit class=button value=Submit ></p></form>\n";

}
?>




