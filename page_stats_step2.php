<?php

/////////////////////////
// CONNECT TO DATABASE //
/////////////////////////
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

// DEFINE SOME VARS
$inharray = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
$dc = array();
$dc_query = mysql_query("SELECT id, abbreviated_name FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['abbreviated_name'];
}

for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chararray = array('&nbsp;','_','&ccedil;','&amp;','&sect;','&ecirc;', '$','&micro;', '&lt;', '&gt;', '#','&deg;','&pound;');
$newchararray = array(' ','\_','\c{c}','\&','\textsection','ê','\$','$\mu$','\textless', '\textgreater', '\#', '\textdegree','\pounds' );

// POSTED VARIABLES
// show stuff
$showsize = $_POST['size'] || 0;
$showmaxregion = $_POST['maxregion'] || 0;
$showmaxsize = $_POST['maxsize'] || 0;
$showgenes = $_POST['genes'] || 0;
$showgenelist = $_POST['genelist'] || 0;
$clinic = $_POST['clinic'] || 0;

$showchiptype = $_POST['chiptype'] || 0;
$showcytoband = $_POST['cytoband'] || 0;
$showregion = $_POST['region'] || 0;
$showclass = $_POST['class'] || 0;
$showinheritance = $_POST['inheritance'] || 0;
// filter stuff
$useclass = $_POST['classfilter'] || 0;
$classvalue = $_POST['classvalue'];
$useinh = $_POST['inheritancefilter'] || 0;
$inhvalue = $_POST['inheritancevalue'];
$usesnp = $_POST['minsnp'] || 0;
$snpvalue = $_POST['minsnpvalue'];
$usesize = $_POST['minsize'] || 0;
$probes = $_POST['probes'] || 0;
$sizevalue = $_POST['minsizevalue'] || 0;
$usecn = $_POST['copynumber'] || 0;
$cnvalue = $_POST['copynumbervalue'];
$logr = $_POST['logr'] || 0;

// DEFINE Functions
function getCytoBand($chr, $chrtxt, $start, $end) {
	$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$start' BETWEEN start AND stop)");
	$cytorow = mysql_fetch_array($cytoquery);
	$cytostart = $cytorow['name'];
	//echo $cytostart;
	$cytoquery = mysql_query("SELECT name FROM cytoBand WHERE chr = '$chr' AND ('$end' BETWEEN start AND stop)");
	$cytorow = mysql_fetch_array($cytoquery);
	$cytostop = $cytorow['name'];
	//echo $cytostop;
	if ($cytostart == $cytostop) {
		$cytoband = "$chrtxt$cytostart";
	} else {
		$cytoband = "$chrtxt$cytostart$cytostop";
	}
	return $cytoband;
}
///////////////////////////////
// SELECT PROJECTS IF NEEDED //
///////////////////////////////
if (isset($_POST['setprojects'])) {
	$query = mysql_query("SELECT p.id, p.naam, p.chiptype, p.created, p.collection FROM project p JOIN projectpermission pp ON p.id = pp.projectid WHERE pp.userid = '$userid' ORDER BY id ASC");
	if (mysql_num_rows($query) == 0) {
		echo "<div class=sectie>";
		echo "<h3>Problem Detected</h3>";
		echo "<p>You don't have access to any project to export data from. </p>\n";
		echo "</div>";
	}
	else {
		echo "<div class=sectie>";
		echo "<h3>Extract Filtered results</h3>";
		echo "<h4>Select Source Projects</h4>";
		echo "<p>Select the projects you wish to extract the results from the list below.</p>";
		echo "<p><form action='index.php?page=stats_step2' method=POST>";
		// hidden fields for filter settings
		// show stuff

		echo "<input type=hidden name='chiptype' value='$showchiptype'>";
		echo "<input type=hidden name='cytoband' value='$showcytoband'>";
		echo "<input type=hidden name='region' value='$showregion'>";
		echo "<input type=hidden name='class' value='$showclass'>";
		echo "<input type=hidden name='inheritance' value='$showinheritance'>";
		echo "<input type=hidden name='size' value='$showsize'>";
		echo "<input type=hidden name='maxregion' value='$showmaxregion'>";
		echo "<input type=hidden name='maxsize' value='$showmaxsize'>";
		echo "<input type=hidden name='genes' value='$showgenes'>";
		echo "<input type=hidden name='genelist' value='$showgenelist'>";
		echo "<input type=hidden name='clinic' value='$clinic'>";
		echo "<input type=hidden name='probes' value='$probes'>";
		echo "<input type=hidden name='logr' value='$logr'>";
		// filter stuff
		echo "<input type=hidden name='classfilter' value='$useclass'>";
		echo "<input type=hidden name='classvalue' value='$classvalue'>";;
		echo "<input type=hidden name='inheritancefilter' value='$useinh'>";
		echo "<input type=hidden name='inheritancevalue' value='$inhvalue'>";
		echo "<input type=hidden name='minsnp' value='$usesnp'>";
		echo "<input type=hidden name='minsnpvalue' value='$snpvalue'>";
		echo "<input type=hidden name='minsize' value='$usesize'>";
		echo "<input type=hidden name='minsizevalue' value='$sizevalue'>";
		echo "<input type=hidden name='copynumber' value='$usecn'>";
		echo "<input type=hidden name='copynumbervalue' value='$cnvalue'>";
		if (isset($_POST['controls'])) {
			echo "<input type=hidden name='controls' value='1'>";
		}
		if (isset($_POST['intrack'])) {
			echo "<input type=hidden name='intrack' value='1'>";
		}
		if (isset($_POST['Plink'])) {
			echo "<input type=hidden name='Plink' value='1'>";
		}
		if (isset($_POST['UPD'])) {
			echo "<input type=hidden name='UPD' value='1'>";
		}
		if (isset($_POST['BAFseg'])) {
			echo "<input type=hidden name='BAFseg' value='1'>";
		}

		// table headers
		echo "<table cellspacing=0>\n";
		echo "<tr><th colspan=5 $firstcell>Projects to Include</th></tr>\n";
		echo "<tr>\n";
		echo " <th class=topcell $firstcell>USE</th>\n";
		echo " <th class=topcell>Name</th>\n";
		echo " <th class=topcell>Chiptype</th>\n";
		echo " <th class=topcell>Created</th>\n";
		echo " <th class=topcell>Collection</th>\n";

		echo "</tr>\n";
		// entries
		while ($row = mysql_fetch_array($query)) {
			$id = $row['id'];
			$name = $row['naam'];
			$col = $row['collection'];
			$chip = $row['chiptype'];
			$created = $row['created'];
			echo "<tr><td $firstcell><input type=checkbox name=useprojects[] value=$id></td>";
			echo "<td>$name</td><td>$chip</td><td>$created</td><td>$col</td></tr>";
		}
		echo "</table>";
		echo "</p><p><input type=submit class=button value=Proceed></p>";
		echo "</form>";
	}
	exit();
}

##  seperate queries for BAF/Plink/UPD
$updq = "chr, start,stop,type,nrprobes,logR";
$bafq = "resultlist";
$plink = "chr,start,stop,largestart,largestop,nrsnps,avgLogR";

## Reformat projects to use
$instring = '';
if (isset($_POST['useprojects'])) {
	foreach($_POST['useprojects'] as $key => $pid) {
		$instring .= "$pid,";
	}
}
if ($instring != '') {
	$instring = " AND p.id IN (".substr($instring,0,-1).") ";
	//echo "instring: $instring<br/>";
}
unlink("$sitedir/Reports/Table_$userid.txt");
$fh = fopen("$sitedir/Reports/Table_$userid.txt", 'a');

// GET CNVs following filter and permission settings
$extras = '';
$plinkextra = '';
$updextra = '';

$filter = '';
$mosfilter = '';
$plinkfilter = '';
$updfilter = '';

$extras .= ', a.largestart, a.largestop ';


$header = "SampleID\tChr\tCN";

if ($showregion == 1) {
	$header .= "\tMin_region";
}
if ($showmaxregion == 1) {
	$extras .= ', a.largestart, a.largestop ';
	$header .= "\tMax_region";
}

if ($showsize == 1) {
	$header .= "\tMin/Max size";
}

if ($showclass == 1) {
	$header .= "\tClass";
}

if ($showinheritance == 1) {
	$header .= "\tInheritance";
}

if ($showgenelist == 1) {
	$extras .= ", a.nrgenes";
	$plinkextra .= ", a.nrgenes";
	$header .= "\tGene List";
}

if ($showgenes == 1) {
	$extras .= ", a.nrgenes";
	$plinkextra .= ", a.nrgenes";
	$header .= "\tNr.Genes";
}

if ($showchiptype == 1) {
	$header .= "\tChiptype";
}

// FILTER settings
if ($logr == 1) {
	$extras .= ", a.avgLogR";
	$plinkextra .= ", a.avgLogR";
	$updextra .= ", a.logR";
	$header .= "\tAvg.LogR";
}
if ($probes == 1) {
	$extras .= ", a.nrsnps";
	$plinkextra .= ", a.nrsnps";
	$updextra .= ", a.nrprobes";
	$header .= "\tNr.Probes";
}

if ($clinic == 1) {
	$header .= "\tClinical Synopsis";
	$extras .= ", s.id";
	$plinkextra .= ", s.id";
	$updextra .= ", s.id";
}

if ($useclass == 1) {
	if ($classvalue != 'ANY') {
		// sorted_order is the class sorted by decreasing pathogenicity (1 = pathogenic etc.)
		$filter .= "AND dc.sort_order <= $classvalue ";
		$mosfilter .= "AND dc.sort_order <= $classvalue ";
	}
}
if ($useinh == 1) {
	$filter .= "AND a.inheritance IN ($inhvalue) ";
}
if ($usesnp == 1) {
	$mosfilter .= "AND a.nrsnps >= $snpvalue ";
	$filter .= "AND a.nrsnps >= $snpvalue ";
}
if ($usecn == 1) {
	$filter .= "AND a.cn IN ($cnvalue) ";
}
if (isset($_POST['intrack'])) {
	$filter .= "AND s.intrack = 1 AND s.trackfromproject = p.id ";
	$mosfilter .= "AND s.intrack = 1 AND s.trackfromproject = p.id ";
	$plinkfilter .= "AND s.intrack = 1 AND s.trackfromproject = p.id ";
	$updfilter .= "AND s.intrack = 1 AND s.trackfromproject = p.id ";
}
if (!isset($_POST['controls'])) {
	$filter .= "AND p.collection <> 'Controls' ";
	$plinkfilter .= "AND p.collection <> 'Controls' ";
	$updfilter .= "AND p.collection <> 'Controls' ";
}

// if genes will be shown, fetch *ALL* genes into AoA for searching later on.
if ($showgenelist == 1) {
	// get genes
	$genes = array();
	$query = mysql_query("SELECT id, chr, start, stop, symbol FROM genesum ORDER BY chr, start, stop");
	while ($row = mysql_fetch_array($query)) {
		// array -> chr -> 'start-stop' -> array of symbols = 1
		$genes[$row['chr']][$row['start']."-".$row['stop']][$row['symbol']] = 1;
	}
}

// compose query
$querystring = "SELECT s.chip_dnanr, a.chr, a.start, a.stop, a.cn, p.chiptype, a.class, a.inheritance $extras 
FROM sample s 
JOIN aberration a
JOIN diagnostic_classes dc 
JOIN projectpermission pp 
JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id AND a.class = dc.id
WHERE pp.userid = '$userid' $instring $filter 
ORDER BY chr ASC, start ASC ";

$mosquerystring = "SELECT s.chip_dnanr, a.chr, a.start, a.stop, a.cn, p.chiptype, a.class, a.inheritance $extras 
FROM sample s 
JOIN aberration_mosaic a
JOIN diagnostic_classes dc 
JOIN projectpermission pp 
JOIN project p ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id AND a.class = dc.id
WHERE pp.userid = '$userid' $instring $mosfilter 
ORDER BY chr ASC, start ASC ";

// STart output
echo "<div class=sectie>\n";
echo "<h3>Creating Overview file </h3>\n";
echo "<h4>status...</h4>\n";
echo "<ul id=ul-disc>\n";
echo "<li> Fetching results\n";
if (ob_get_level() > 0) {ob_flush();};
flush();
$header .= "\n";
fwrite($fh,$header);
$query = mysql_query($querystring);
$mosquery = mysql_query($mosquerystring);
// PRINT EXPERIMENTAL DETAILS ON SAMPLE / PROJECT
$totalcnv = mysql_num_rows($query);
$totalmoscnv = mysql_num_rows($mosquery);
echo "<li> Processing $totalcnv CNV results";
echo "<li> Processing $totalmoscnv MOS results";
echo "<ul id=ul-disc>\n";

$dbresults = array();
while ($row = mysql_fetch_array($query)) {
	array_push($dbresults, $row); 
}
while ($row = mysql_fetch_array($mosquery)) {
	array_push($dbresults, $row); 
}

if (ob_get_level() > 0) {ob_flush();};
flush();
$phenos = array();
// tracking variables for gene listing
$currchr = 0;
$geneidx = -1;
$genemax = -1;
$geneKeys = array();
$linesBySample = array();
foreach ($dbresults as $row) {
	$line = '';
	$sample = $row['chip_dnanr'];
	$chr = $row['chr'];
	// reset gene listing trackers
	if ($chr != $currchr) {
		// destroy array of finished chromosome.
		unset($genes[$currchr]);
		// reset the trackers
		$currchr = $chr;
		echo "<li>Chromosome ". $chromhash[$chr]."</li>";
		if (ob_get_level() > 0) {ob_flush();};
		flush();
		$geneidx = 0;
		$genemax = count($genes[$chr]) -1;
		$geneKeys = array_keys($genes[$chr]);
	}
	$chrtxt = $chromhash[$chr];
	$start = $row['start'];
	$stop = $row['stop'];
	$largestart = $row['largestart'];
	$largestop = $row['largestop'];
	if ($showcytoband == 1) {
		$cytoband = getCytoBand($chr, $chrtxt ,$start, $stop);

	}
	else {
		$cytoband = $chrtxt;
	}
	$size = $stop - $start + 1;
	$largesize = $largestop - $largestart + 1;
	$cn = $row['cn'];
	$chiptype = $row['chiptype'];

	$class = $row['class'];
	$class = $dc[$class];
	//$line = "$sample\t$chiptype\t$cytoband\t$start\t$stop\tchr$chrtxt:$start-$stop\t$cn\t$class\t". $inharray[$inharray[$inh]];
	// $line = "$sample\t$cytoband\t$start\t$stop\t$cn";
	$line = "$sample\t$cytoband\t$cn";
	
	if ($showregion == 1) {
		$line .= "\t$start-$stop";
	}

	if ($showmaxregion == 1) {
		if ($largestart == 'ter') {
			$largestartvalue = 0;
		}
		else {
			$largestartvalue = $largestart;
		}
		if ($largestop == 'ter') {
			$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
			$cendrow = mysql_fetch_array($cendquery);
			$largestopvalue = $cendrow['stop'];
		}
		else {
			$largestopvalue = $largestop;
		}
		$largesize = $largestopvalue - $largestartvalue +1;

		$line .= "\t$largestart-$largestop";
	}

	if ($showsize == 1) {
		if ($largesize >= 1000000) {
			$line .= "\t" . number_format($size/1000000, 2, ',', '') . '-' . number_format($largesize/1000000, 2, ',', '') . ' Mb';
		}
		elseif ($largesize >= 1000) {
			$line .= "\t" . number_format($size/1000, 2, ',', '') . '-' . number_format($largesize/1000, 2, ',', '') . ' kb';
		}
		else {
			$line .= "\t" . $size . '-' . $largesize . ' bp';
		}
	}

	if ($showclass == 1) {
		$line .= "\t$class";
	}

	if ($showinheritance == 1) {
		$inh = $row['inheritance'];
		if ($inh == '') {
			$inh = 0;
		}
		$inh = $inharray[$inharray[$inh]];
		$line .= "\t$inh";
	}

	if ($showgenelist == 1) {
		$nrgenes = $row['nrgenes'];
		error_log('nr genes: ' . $nrgenes);
		$geneline = '';
		if ($nrgenes > 0) {
			// get genes with start between start & stop
			// array -> chr -> 'start-stop' -> array of symbols = 1
			$tmpgene = array();
			for ($i = $geneidx; $i <= $genemax ; $i++) {
				$p = explode('-',$geneKeys[$i]);
				// cnvs are ordered by start so once start > genestop (no overlap), gene can be removed for future cnvs
				if ($p[1] < $start) {
					$geneidx = $i+1;
					continue;
				}
				// other case: gene is 3' from cnv (no overlap), skip but do not remove
				if ($p[0] > $stop) {
					continue;
				}
				// overlap, add to tmpgene array
				$tmpgene = array_merge($tmpgene,$genes[$chr][$geneKeys[$i]]);
			}
			// create alphabetical gene list
			ksort($tmpgene);
			$geneline = implode(',',array_keys($tmpgene));
		}
		$line .= "\t$geneline";

	}

	if ($showgenes == 1) {
		$nrgenes = $row['nrgenes'];
		$line .= "\t$nrgenes";
	}

	if ($showchiptype == 1) {
		$line .= "\t$chiptype";
	}

	if ($logr == 1) {
		if (array_key_exists('AvgLogR', $row)) {
			$line .= "\t".$row['AvgLogR'];
		}
		else {
			$line .= "\t";
		}
	}

	if ($probes == 1) {
		$line .= "\t".$row['nrsnps'];
	}

	if ($clinic == 1) {
		$sid = $row['id'];
		// get phenos
		if (array_key_exists($sid,$phenos)) {
			$phenostring = $phenos[$sid];
		}
		else {
			$pheno = array();
			$pquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid'");
			while ($prow = mysql_fetch_array($pquery)) {
				$pheno[] = $prow['lddbcode'];
			}
			mysql_select_db('LNDB');
			$phenostring = '';
			foreach ($pheno as $key => $code) {
				$pquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
				$prow = mysql_fetch_array($pquery);
				$phenostring .= $prow['LABEL'].";";
			}
			if ($phenostring != ''){
				$phenostring = substr($phenostring, 0, -1);
			}
			$phenos[$sid] = $phenostring;
			mysql_select_db("$db");
		}
		$line .= "\t$phenostring";
	}
	$line .= "\n";
	// add the line to the entries for current sample on current chromosome
	if (array_key_exists($sample,$linesBySample)) {
		$linesBySample[$sample] .= $line;
	}
	else {
		$linesBySample[$sample] = $line;
	}
}
echo "<ul></li>";
###########
## PLINK ##
###########
if (isset($_POST['Plink'])) {
	echo "<li>Fetching Plink Data</li>";
	$querystring = "SELECT s.chip_dnanr, a.chr, a.start, a.stop, p.chiptype, a.class $plinkextra 
	FROM `sample` s 
	JOIN `aberration_LOH` a
	JOIN `projectpermission` pp 
	JOIN `project` p 
	ON s.id = a.sample AND pp.projectid = a.idproj AND a.idproj = p.id
	WHERE pp.userid = '$userid' $instring $plinkfilter 
	ORDER BY chr ASC, start ASC ";
	$query = mysql_query($querystring);
	while ($row = mysql_fetch_array($query)) {
		$line = '';
		$sample = $row['chip_dnanr'];
		$chr = $row['chr'];
		// reset gene listing trackers
		if ($chr != $currchr) {
			// destroy array of finished chromosome.
			unset($genes[$currchr]);
			// reset the trackers
			$currchr = $chr;
			echo "<li>Chromosome ". $chromhash[$chr]."</li>";
			if (ob_get_level() > 0) {ob_flush();};
			flush();
			$geneidx = 0;
			$genemax = count($genes[$chr]) -1;
			$geneKeys = array_keys($genes[$chr]);
		}
		$chrtxt = $chromhash[$chr];
		$start = $row['start'];
		$stop = $row['stop'];
		if ($showcytoband == 1) {
		   $cytoband = getCytoBand($chr, $chrtxt ,$start, $stop);

		}
		else {
		   $cytoband = $chrtxt;
		}
		$size = $stop - $start +1 ;
		$chiptype = $row['chiptype'];

		//$line = "$sample\t$chiptype\t$cytoband\t$start\t$stop\tchr$chrtxt:$start-$stop\tLOH\t$class\t". $inharray[$inharray[$inh]];
		$line = "$sample\t$cytoband\t$start\t$stop\tLOH";
		//$line = "$sample\t$cytoband\t$start\t$stop\t$cn";

		if ($showchiptype == 1) {
		   $line .= "\t$chiptype";
		}
		if ($showregion == 1) {
		   $line .= "\tchr$chrtxt:$start-$stop";
		}
		if ($showclass == 1) {
			$class = $row['class'];
			if ($class == '' || $class == 0) {
				$class = 'Not Defined';
			}
			elseif ($class == 5) {
				$class = 'False Positive';
			}
			$line .= "\t$class";
		}

		// not specified in table
		if ($showinheritance == 1) {
		   $inh = $inharray[$inharray[0]];
		   $line .= "\t$inh";
		}

		if ($showsize == 1) {
			$line .= "\t$size";
		}
		if ($showmaxregion == 1 || $showmaxsize == 1) {
			#$largestart = $row['largestart'];
			#$largestop = $row['largestop'];
			$largestart = '';
			$largestop = '';

			if ($largestart == 'ter') {
				$largestartvalue = 0;
			}
			else {
				$largestartvalue = $largestart;
			}
			if ($largestop == 'ter') {
				$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
				$cendrow = mysql_fetch_array($cendquery);
				$largestopvalue = $cendrow['stop'];
			}
			else {
				$largestopvalue = $largestop;
			}
			$largesize = $largestopvalue - $largestartvalue +1;

			if ($showmaxregion == 1) {
				$line .= "\t";
			}
			if ($showmaxsize == 1) {
				$line .= "\t";
			}
		}
		if ($logr == 1) {
			$line .= "\t".$row['AvgLogR'];
		}
		if ($probes == 1) {
			$line .= "\t".$row['nrsnps'];
		}
		if ($showgenes == 1) {
			$nrgenes = $row['nrgenes'];
			$line .= "\t$nrgenes";
		}
		if ($showgenelist == 1) {
			$nrgenes = $row['nrgenes'];
			$geneline = '';
			if ($nrgenes > 0) {
				// get genes with start between start & stop
				// array -> chr -> 'start-stop' -> array of symbols = 1
				$tmpgene = array();
				for ($i = $geneidx; $i <= $genemax ; $i++) {
					$p = explode('-',$geneKeys[$i]);
					// cnvs are ordered by start so once start > genestop (no overlap), gene can be removed for future cnvs
					if ($p[1] < $start) {
						$geneidx = $i+1;
						continue;
					}
					// other case: gene is 3' from cnv (no overlap), skip but do not remove
					if ($p[0] > $stop) {
						continue;
					}
					// overlap, add to tmpgene array
					$tmpgene = array_merge($tmpgene,$genes[$chr][$geneKeys[$i]]);
				}
				// create alphabetical gene list
				ksort($tmpgene);
				$geneline = implode(',',array_keys($tmpgene));
			}
			$line .= "\t$geneline";

		}

		if ($clinic == 1) {
			$sid = $row['id'];
			// get phenos
			if (array_key_exists($sid,$phenos)) {
			   $phenostring = $phenos[$sid];
			}
			else {
			   $pheno = array();
			   $pquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid'");
			   while ($prow = mysql_fetch_array($pquery)) {
				   $pheno[] = $prow['lddbcode'];
			   }
			   mysql_select_db('LNDB');
			   $phenostring = '';
			   foreach ($pheno as $key => $code) {
				   $pquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
				   $prow = mysql_fetch_array($pquery);
				   $phenostring .= $prow['LABEL'].";";
			   }
			   if ($phenostring != ''){
				   $phenostring = substr($phenostring, 0, -1);
			   }
			   $phenos[$sid] = $phenostring;
			   mysql_select_db("$db");
			}
			$line .= "\t$phenostring";
		}
		$line .= "\n";
		// add the line to the entries for current sample on current chromosome
		$linesBySample[$sample] = $linesBySample[$sample] . $line;
	}
}

###########
## UPD ##
###########
if (isset($_POST['UPD'])) {
	echo "<li>Fetching UPD Data</li>";
	$querystring = "SELECT s.chip_dnanr, a.chr, a.start, a.stop, p.chiptype, a.type $updextra FROM `sample` s JOIN `parents_upd` a JOIN `projectpermission` pp JOIN `project` p ON s.id = a.sid AND pp.projectid = a.spid AND a.spid = p.id WHERE pp.userid = '$userid' $instring $updfilter ORDER BY chr ASC, start ASC ";
	$query = mysql_query($querystring);
	while ($row = mysql_fetch_array($query)) {
		$line = '';
		$sample = $row['chip_dnanr'];
		$chr = $row['chr'];
		// reset gene listing trackers
		if ($chr != $currchr) {
			// destroy array of finished chromosome.
			unset($genes[$currchr]);
			// reset the trackers
			$currchr = $chr;
			echo "<li>Chromosome ". $chromhash[$chr]."</li>";
			if (ob_get_level() > 0) {ob_flush();};
			flush();
			$geneidx = 0;
			$genemax = count($genes[$chr]) -1;
			$geneKeys = array_keys($genes[$chr]);
		}
		$chrtxt = $chromhash[$chr];
		$start = $row['start'];
		$stop = $row['stop'];
		if ($showcytoband == 1) {
		   $cytoband = getCytoBand($chr, $chrtxt ,$start, $stop);

		}
		else {
		   $cytoband = $chrtxt;
		}
		$size = $stop - $start +1 ;
		$chiptype = $row['chiptype'];

		$type = $row['type'];
		//$line = "$sample\t$chiptype\t$cytoband\t$start\t$stop\tchr$chrtxt:$start-$stop\t$type\t$class\t". $inharray[$inharray[$inh]];
		$line = "$sample\t$cytoband\t$start\t$stop\t$type";

		if ($showchiptype == 1) {
			$line .= "\t$chiptype";
		}
		if ($showregion == 1) {
			$line .= "\tchr$chrtxt:$start-$stop";
		}

		// not specified in table
		if ($showclass == 1) {
			$class = '';
			$line .= "\t$class";
		}
		// not specified in table
		if ($showinheritance == 1) {
			$inh = $inharray[$inharray[0]];
			$line .= "\t$inh";
		}

		if ($showsize == 1) {
			$line .= "\t$size";
		}
		if ($showmaxregion == 1 || $showmaxsize == 1) {
			$largestart = '';
			$largestop = '';

			if ($largestart == 'ter') {
				$largestartvalue = 0;
			}
			else {
				$largestartvalue = $largestart;
			}
			if ($largestop == 'ter') {
				$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
				$cendrow = mysql_fetch_array($cendquery);
				$largestopvalue = $cendrow['stop'];
			}
			else {
				$largestopvalue = $largestop;
			}
			$largesize = $largestopvalue - $largestartvalue +1;

			if ($showmaxregion == 1) {
				$line .= "\t";
			}
			if ($showmaxsize == 1) {
				$line .= "\t";
			}
		}
		if ($logr == 1) {
			$line .= "\t".$row['LogR'];
		}
		if ($probes == 1) {
			$line .= "\t".$row['nrprobes'];
		}
		if ($showgenes == 1 || $showgenelist == 1) {
			$tmpgene = array();
			for ($i = $geneidx; $i <= $genemax ; $i++) {
				$p = explode('-',$geneKeys[$i]);
				// cnvs are ordered by start so once start > genestop (no overlap), gene can be removed for future cnvs
				if ($p[1] < $start) {
					$geneidx = $i+1;
					continue;
				}
				// other case: gene is 3' from cnv (no overlap), skip but do not remove
				if ($p[0] > $stop) {
					continue;
				}
				// overlap, add to tmpgene array
				$tmpgene = array_merge($tmpgene,$genes[$chr][$geneKeys[$i]]);
			}
			// create alphabetical gene list
			ksort($tmpgene);
			$tmp = array_keys($tmpgene);
			$nrgenes = count($tmp);
			if ($showgenes == 1) {
				$line .= "\t$nrgenes";
			}
			if ($showgenelist == 1) {
				$geneline = implode(',',array_keys($tmpgene));
				$line .= "\t$geneline";
			}

		}

		if ($clinic == 1) {
			$sid = $row['id'];
			// get phenos
			if (array_key_exists($sid,$phenos)) {
			   $phenostring = $phenos[$sid];
			}
			else {
				$pheno = array();
				$pquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid'");
				while ($prow = mysql_fetch_array($pquery)) {
					$pheno[] = $prow['lddbcode'];
				}
				mysql_select_db('LNDB');
				$phenostring = '';
				foreach ($pheno as $key => $code) {
					$pquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
				   $prow = mysql_fetch_array($pquery);
				   $phenostring .= $prow['LABEL'].";";
				}
				if ($phenostring != ''){
				   $phenostring = substr($phenostring, 0, -1);
				}
				$phenos[$sid] = $phenostring;
				mysql_select_db("$db");
			}
			$line .= "\t$phenostring";
		}

		$line .= "\n";

		// add the line to the entries for current sample on current chromosome
		$linesBySample[$sample] = $linesBySample[$sample] . $line;
	}

}
###########
## BAF ##
###########
if (isset($_POST['BAFseg'])) {
	echo "<li>Fetching BAFseg Data</li>";
	$querystring = "SELECT s.chip_dnanr, a.resultlist, p.chiptype
	FROM `sample` s 
	JOIN `BAFSEG` a 
	JOIN `projectpermission` pp 
	JOIN `project` p 
	ON s.id = a.idsamp AND pp.projectid = a.idproj AND a.idproj = p.id 
	WHERE pp.userid = '$userid' $instring $plinkfilter ";
	
	$query = mysql_query($querystring);
	while ($row = mysql_fetch_array($query)) {
		$line = '';
		$sample = $row['chip_dnanr'];
		$data = explode("\n",$row['resultlist']);
		array_shift($data);
		foreach($data as $key => $dline) {
			if ($dline == '') {
				continue;
			}
			$drow = explode("\t",$dline);
			$chr = $drow[1];
			// reset gene listing trackers
			if ($chr != $currchr) {
				// destroy array of finished chromosome.
				unset($genes[$currchr]);
				// reset the trackers
				$currchr = $chr;
				#echo "<li>Chromosome ". $chromhash[$chr]."</li>";
				if (ob_get_level() > 0) {ob_flush();};
				flush();
				$geneidx = 0;
				$genemax = count($genes[$chr]) -1;
				$geneKeys = array_keys($genes[$chr]);
			}
			$chrtxt = $chromhash[$chr];
			$start = $drow[2];
			$stop = $drow[3];
			if ($showcytoband == 1) {
			 $cytoband = getCytoBand($chr, $chrtxt ,$start, $stop);

			}
			else {
			 $cytoband = $chrtxt;
			}
			$size = $stop - $start +1 ;
			$chiptype = $row['chiptype'];

			$type = '';
			if ($drow[8] < -0.05) {
				$type = 'Mos.Del';
			}
			elseif ($drow[8] > 0.05) {
				$type = 'Mos.Dup';
			}
			else {
				$type = 'Mos.LOH';
			}

			//$line = "$sample\t$chiptype\t$cytoband\t$start\t$stop\tchr$chrtxt:$start-$stop\t$type\t$class\t". $inharray[$inharray[$inh]];
			$line = "$sample\t$cytoband\t$start\t$stop\t$type";

			if ($showchiptype == 1) {
				$line .= "\t$chiptype";
			}
			if ($showregion == 1) {
				$line .= "\tchr$chrtxt:$start-$stop";
			}

			// not specified in table
			if ($showclass == 1) {
				$class = '';
				if ($class == '' || $class == 0) {
					$class = 'Not Defined';
				}
				elseif ($class == 5) {
					$class = 'False Positive';
				}
				$line .= "\t$class";
			}
			// not specified in table
			if ($showinheritance == 1) {
				$inh = $inharray[$inharray[0]];
				$line .= "\t$inh";
			}

			if ($showsize == 1) {
				$line .= "\t$size";
			}
			if ($showmaxregion == 1 || $showmaxsize == 1) {
				$largestart = '';
				$largestop = '';

				if ($largestart == 'ter') {
					$largestartvalue = 0;
				}
				else {
					$largestartvalue = $largestart;
				}
				if ($largestop == 'ter') {
					$cendquery = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' ORDER BY stop DESC LIMIT 1");
					$cendrow = mysql_fetch_array($cendquery);
					$largestopvalue = $cendrow['stop'];
				}
				else {
					$largestopvalue = $largestop;
				}
				$largesize = $largestopvalue - $largestartvalue +1;

				if ($showmaxregion == 1) {
					$line .= "\t";
				}
				if ($showmaxsize == 1) {
					$line .= "\t";
				}
			}
			if ($logr == 1) {
				$line .= "\t".$drow[8];
			}
			if ($probes == 1) {
				$line .= "\t";
			}
			if ($showgenes == 1 || $showgenelist == 1) {
				$tmpgene = array();
				for ($i = $geneidx; $i <= $genemax ; $i++) {
					$p = explode('-',$geneKeys[$i]);
					// cnvs are ordered by start so once start > genestop (no overlap), gene can be removed for future cnvs
					if ($p[1] < $start) {
						$geneidx = $i+1;
						continue;
					}
					// other case: gene is 3' from cnv (no overlap), skip but do not remove
					if ($p[0] > $stop) {
						continue;
					}
					// overlap, add to tmpgene array
					$tmpgene = array_merge($tmpgene,$genes[$chr][$geneKeys[$i]]);
				}
				// create alphabetical gene list
				ksort($tmpgene);
				$tmp = array_keys($tmpgene);
				$nrgenes = count($tmp);
				if ($showgenes == 1) {
					$line .= "\t$nrgenes";
				}
				if ($showgenelist == 1) {
					$geneline = implode(',',array_keys($tmpgene));
					$line .= "\t$geneline";
				}

			}

			if ($clinic == 1) {
				$sid = $row['id'];
				// get phenos
				if (array_key_exists($sid,$phenos)) {
					$phenostring = $phenos[$sid];
				}
				else {
					$pheno = array();
					$pquery = mysql_query("SELECT lddbcode FROM sample_phenotypes WHERE sid = '$sid'");
					while ($prow = mysql_fetch_array($pquery)) {
						$pheno[] = $prow['lddbcode'];
					}
					mysql_select_db('LNDB');
					$phenostring = '';
					foreach ($pheno as $key => $code) {
						$pquery = mysql_query("SELECT LABEL FROM FEATURE WHERE CODE = '$code'");
						$prow = mysql_fetch_array($pquery);
						$phenostring .= $prow['LABEL'].";";
					}
					if ($phenostring != ''){
						$phenostring = substr($phenostring, 0, -1);
					}
					$phenos[$sid] = $phenostring;
					mysql_select_db("$db");
				}
				$line .= "\t$phenostring";
			}

			$line .= "\n";
			// add the line to the entries for current sample on current chromosome
			$linesBySample[$sample] = $linesBySample[$sample] . $line;
		}
	}
}


echo "</ul>";
echo "<li>Printing results files</li>";
if (ob_get_level() > 0) {ob_flush();};
flush();
ksort($linesBySample);
foreach( $linesBySample as $sample => $entries) {
	fwrite($fh,$entries);
}
echo "<li>Done, providing file</li>";
echo "</ul>";
echo "</p><p> <a href=\"index.php?page=stats\">Go Back</a>\n";
fclose($fh);
if (ob_get_level() > 0) {ob_flush();};
flush();

echo "<iframe height='0' width='0' src='download.php?path=Reports&file=Table_$userid.txt'></iframe>\n";
?>

</div>
