
<div class=sectie>
<h3>SybrGreen Realtime analyse:</h3>
<?php 
#error_reporting(E_ALL);
#ob_start();
if ($loggedin != 1) {
	include('login.php');
}
else { 
	//foreach($_POST as $k => $v) {
	//	echo "$k => $v<br>\n";
	//}

//if (isset($_POST['backfromsg'])) {
	$seperator = $_POST['seperator'];
//}
////////////////////////////////
// Variables for table layout //
////////////////////////////////
  $tdtype= array("","class=alt");
  $thtype= array("class=spec","class=specalt");
  $topstyle = array("class=topcell","class=topcellalt");
  $selected = array("", "checked");

/////////////////////////
// DO IF FILE UPLOADED // 
/////////////////////////
$file1ok = 0;
if (strlen($_FILES['ctfile1']['name']) > 0) {
  $uploadok = 1;
  $file1ok = 1;
  $ctfilename = basename( $_FILES['ctfile1']['name']); 
  $ctfile = str_replace(' ','_',$ctfilename);
  $file1 = $ctfile;
  $ctfile = "/tmp/$ctfile";
  #$seperator = $_POST['seperator'];
  //$_SESSION['s_seperator'] = $seperator;
  $name1 = $ctfilename;
  //$_SESSION['n_ctfile1'] = $ctfile;
  if (move_uploaded_file($_FILES['ctfile1']['tmp_name'], $ctfile)) {
  }
  else {
 	echo "upload failed";
  }
  
}

elseif (strlen($_POST['file1'])>0) {
  $uploadok = 1;
  $file1 = $_POST['file1'];
  $ctfile = "/tmp/".$_POST['file1'];
  $file1ok = 1;
  $name1 = $ctfile;
}
if ($file1ok == 1) {
  $fh = fopen("$ctfile","r");
  $line = fgets($fh);
  echo "line 1: $line<br/>\n";
  ob_flush();
  flush();
  $parts = explode("\t",$line);
  while ($parts[0] != "Include") {
   $line = fgets($fh);
   $parts = explode("\t", $line);
  }
  $size = sizeof($parts);
  if ($size == 7) {
	$col = 0;
  }
  else {
	$col = 1;
  }
  while (!feof($fh)) {
    $line = fgets($fh);
    $pieces1 = explode("\t",$line);
    $pieces2 = explode("$seperator",$pieces1[2+$col]);
    $sample = $pieces2[0];
    $gen = $pieces2[1];
    $ct = $pieces1[3+$col];
    $ct = str_replace(',','.',$ct);
    if ($sample != "NTC" && $sample != "ntc" && $sample != "") {
     $exp["$sample"]["$gen"][] = $ct;
     $genes["$gen"] = "Target";
    }
  }
  
  fclose($fh);
  $nrgenes = count($exp["$samplename"]) ;
  $nrcols = $nrgenes * 2;
  $nrsamples = count($exp);
  ksort($exp, SORT_STRING);
}
else {
 echo "<p>no file 1 specified</p>\n";
}
// FILE 2
$file2ok = 0;
if (strlen($_FILES['ctfile2']['name']) > 0) {
    $uploadok = 1;
  $file2ok = 1;
  $ctfilename = basename( $_FILES['ctfile2']['name']); 
  $ctfile = str_replace(' ','_',$ctfilename);
  $file2 = $ctfile;

  $ctfile = "/tmp/$ctfile";
    #$seperator = $_POST['seperator'];
  $name2 = $ctfilename;
  move_uploaded_file($_FILES['ctfile2']['tmp_name'], $ctfile);
}
elseif (strlen($_POST['file2'])>0) {
  $uploadok = 1;
  $file2 = $_POST['file2'];
  $ctfile = "/tmp/".$_POST['file2'];
  $file2ok = 1;
  $name2 = $ctfile;
}
if ($file2ok == 1 ) {
  $fh = fopen("$ctfile","r");
  $line = fgets($fh);
  $parts = explode("\t",$line);
  while ($parts[0] != "Include") {
   $line = fgets($fh);
   $parts = explode("\t", $line);
  }
  $size = sizeof($parts);
  if ($size == 7) {
	$col = 0;
  }
  else {
	$col = 1;
  }
  while (!feof($fh)) {
    $line = fgets($fh);
    $pieces1 = explode("\t",$line);
    $pieces2 = explode("$seperator",$pieces1[2+$col]);
    $sample = $pieces2[0];
    $gen = $pieces2[1];
    $ct = $pieces1[3+$col];
    $ct = str_replace(',','.',$ct);
    if ($sample != "NTC" && $sample != "ntc" && $sample != "") {
     $exp["$sample"]["$gen"][] = $ct;
     $genes["$gen"] = "Target";
    }
  }
  fclose($fh);
  $nrgenes = count($exp["$samplename"]) ;
  $nrcols = $nrgenes * 2;
  $nrsamples = count($exp);
  ksort($exp, SORT_STRING);

}
else {
 echo "<p>no file 2 specified</p>\n";
}
echo "</div>\n";
//////////////////////////////////
// PRINT THE UPLOADED FILE-NAME //
//////////////////////////////////

  echo "<div class=sectie>\n";
  echo "<h3>Uploaded files</h3>\n";
  echo "<p>\n<ul>\n";
  if ($name1 != ""){echo " <li>File 1: $name1</li>\n"; }
  if ($name2 != ""){echo " <li>File 2: $name2</li>\n"; }
  echo "</ul>\n</p></div>\n";
  
//////////////////////////////////////////////
// PRINT FORM TO SET SAMPLE / GENE SETTINGS //
//////////////////////////////////////////////
if ($uploadok == 1) {
  echo "<div class=sectie>\n<p>\n";
  echo "<h3>Set Sample And Gene Specifications</h3>\n";
  echo "<p>\n";
  echo "<form action=\"index.php?page=sg_results\" method=\"post\">\n";
  echo "<input type=\"hidden\" name=\"seperator\" value=\"$seperator\">\n";
  echo "<input type=hidden name=file1 value=\"$file1\">\n";
  echo "<input type=hidden name=file2 value=\"$file2\">\n";
  echo "<table id=\"mytable\" cellspacing=0 >\n";
  echo "<tr><th scope=col colspan=3 class=title>Define Sample Types</th></tr>\n";
  echo "<tr>\n  <th scope=col class=topleft >Sample Name</th>\n  <th scope=col>Sample Type</th>\n  <th scope=col>Relation to other Samples</th>\n</tr>\n";
  $switch = 0;
  foreach($exp as $samplename => $value) {
	$sampbut = "samp". $seperator . $samplename;
	$parbut = "par" . $seperator . $samplename;
	$pselect = "";
	$cselect = "";
	if ($_POST[$sampbut] == "controle") {
		$cselect = "checked";
	}
	if ($pselect == "" && $cselect == "") {
		$pselect = "checked";
	}
	echo "<tr>\n";
	echo "  <th scope=row $thtype[$switch]>$samplename:</th>\n";
	echo "  <td $tdtype[$switch]><input type=radio name=\"$sampbut\" value=controle ". $cselect .">Controle&nbsp;<input type=radio name=\"$sampbut\" value=patient ".$pselect .">Patient</td>\n";
	echo "  <td $tdtype[$switch]>Parent of: \n";
	echo "    <select name=$parbut>\n";
	$parents = $_POST[$parbut];	
	if ($parents == "index") {
		$selectempty = "selected";
	}
	else {
		$selectempty = "";
	}
	echo "      <option value=index $selectempty>None (index)</option>\n";
 	foreach($exp as $name => $value2) {
		if ($parents == "$name") {
			echo "      <option value=$name selected>$name</option>\n";
		}
		else {
			echo "      <option value=$name >$name</option>\n";
		}
	}
	echo "    </select>\n";
	echo "</tr>\n";
	$switch = $switch + pow(-1,$switch);		
  }
  echo "<tr>\n";
  echo "  <td colspan=3 class=clear>&nbsp;</td>\n";
  echo "</tr>\n";
  echo "<tr>\n"; 
  echo "  <th scope=col colspan=3 class=title>Define Gene Types</th>\n";
  echo "</tr>\n";
  echo "<tr>\n";  
  echo "  <th scope=col class=topleft>Gene Name</th>\n";
  echo "  <th scope=col colspan=2>Gene Type</th>\n";
  echo "</tr>\n";
  $switch=0;
  foreach ($genes As $genname => $value) {
  	$genbut = "gen". $seperator . $genname;
	$rselect = "";
	$tselect = "";
	if ($_POST[$genbut] == "reference") {
		$rselect = "checked";
	}
	if ($rselect == "" && $tselect == "") {
		$tselect = "checked";
	}
	echo "<tr>\n";
	echo "  <th scope=row $thtype[$switch]>$genname:</th>\n";
	echo "  <td colspan=2 $tdtype[$switch]><input type=radio name=\"$genbut\" value=reference $rselect >Reference&nbsp;<input type=radio name=\"$genbut\" value=target $tselect >Target</td>\n";
	echo "</tr>\n";
	$switch = $switch + pow(-1,$switch);	
  }
  echo "</table>\n";
  echo "</div>\n";

////////////////////////////////////////////////////////
// PRINT FORM TO EXCLUDE REPLICATES FROM CALCULATIONS //  
////////////////////////////////////////////////////////

  echo "<div class=sectie>\n";
  echo "<h3>Exclude Failed Samples From Analysis</h3>\n";
  echo "<p>\n";
  echo "<table  cellspacing=0>\n";
  foreach($exp as $samplename => $value1) {
	echo "<tr>\n";
        echo "  <th colspan=$nrcols scope=col class=title>Sample: $samplename</th>\n";
	echo "</tr>\n";
	echo "<tr>\n";
	$switch1=0;
	$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
	foreach($genes as $gene => $gtype) {
	  if(array_key_exists($gene,$exp[$samplename])) {
		echo "<th colspan=2 scope=col $topstyle[$switch1] $firstcell>$gene</th>\n";
		$switch1 = $switch1 + pow(-1,$switch1);
		$firstcell = "";
	  }  
	  else {
		echo "<th colspan=2 scope=col $topstyle[$switch1] $firstcell>&nbsp;</th>\n";
		$switch1 = $switch1 + pow(-1,$switch1);
		$firstcell = "";
	  }
	}
	echo "</tr>\n";
	echo "<tr>\n";
	$switch2=0;
	$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";
	foreach($genes as $gene =>$gtype) {
	  if (array_key_exists($gene, $exp[$samplename])) {  
  		$cts = array();
		foreach($exp[$samplename][$gene] as $ct => $number) {
			$cts[] = $number;	
		}
		$range = round(max($cts)-min($cts),2);
		$range = format_number("$range");
		$bg = "";
		if ($range > 0.5) { 
			$celltype = "class=sg_red";
		}
		else {
			$celltype = $tdtype[$switch2];
		} 
		echo "  <td $tdtype[$switch2] $firstcell>\n";
		$firstcell = "";
		$nrcts = count($cts);	
		for ($i=0;$i<$nrcts;$i++){
			$ct = format_number("$cts[$i]");
			$incbut = "inc". $seperator . $samplename . $seperator . $gene . $seperator . $cts[$i];
			$ctname = str_replace(".","_",$incbut);
			$ctname = str_replace(" ","_",$ctname);	
			if (isset($_POST['backfromsg'])) {
				if ($_POST[$ctname] == "on" ) {
					$select = "checked";
				}	
				else {
					$select = "";
				}
			}
			else {
				$select = "checked";
			}
			echo "$ct <input type=checkbox name=\"$incbut\" $select><br>\n";
		}
		echo "  </td>\n";
		echo "  <td valign=center $celltype>$range</td>\n";
		$switch2 = $switch2 + pow(-1,$switch2);
          }
	  else {
		echo "<td colspan=2 $tdtype[$switch2] $firstcell>\n";
		$firstcell = "";
		$switch2 = $switch2 + pow(-1,$switch2);
	  }
	}
	echo "</tr>\n";
	echo "<tr>\n";
	echo "<td class=clear colspan=$nrcols>&nbsp;</td>\n";
    }
  echo "</tr>\n";
  echo "<tr>\n";
  echo "  <td colspan=$nrcols class=clear>";
  echo "<input type=submit class=button name=submit value=\"...Go...\">";
  echo "<input type=hidden name=file1 value=\"$file1\"><input type=hidden name=file2 value=\"$file2\"></form>\n";
  echo "</td>\n";
  echo "</tr>\n";
  echo "</table>\n";
  echo "</p>\n";
  echo "</div>\n"; 
}


}
function format_number($str,$decimal_places='2',$decimal_padding="0"){
        $str           =  number_format($str,$decimal_places,'.','');     // will return 12345.67
        $number       = explode('.',$str);
        $number[1]     = (isset($number[1]))?$number[1]:''; // to fix the PHP Notice error if str does not contain a decimal placing.
        $decimal     = str_pad($number[1],$decimal_places,$decimal_padding);
        return (float) $number[0].'.'.$decimal;
}



// CLOSE if $loggedin 

?>


