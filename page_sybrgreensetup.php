<div class=sectie>
<h3>SybrGreen Realtime analyse:</h3>
<?php 
ob_start();
if ($loggedin != 1) {
	include('login.php');
}
else { 
	unset($_SESSION[ 'backfromsg' ]);
	echo "variable : ". $_SESSION[ 'backfromsg' ] . "<br>";

?>
<p>Please provide the datafiles as a tab delimted text file containing the following columns: three columns including include, color and position on the plate, then the description of well-content in the format "Sample_gen" or "Sample|gen", followed by a column containing the Delta-Ct values.  Any remaining columns are ignored. This follows the standard format of exported Roche LightCycler 480 data.</p>
<p>
<form enctype="multipart/form-data" action="index.php?page=sybrgreencheck" method="POST">
  <table id="clear" width="100%">
    <tr>
     <th scope=row class=clear>Data file 1</th>
     <td class=clear><input type="file" name="ctfile1" size="40" /></td>
    </tr>
    <tr>
     <th scope=row class=clear>Data file 2</th>
     <td class=clear><input type="file" name="ctfile2" size="40" /></td>
    </tr>
    <tr>
     <th scope=row class=clear>Seperator</th>
     <td class=clear><select name="seperator" style="width:35px">
     		<option label="underscore" value="_" selected>_</option>
		<option lable="vertbar" value="|" >|</option>
		</select>
     </td>
    </tr>
    <tr>
     <td class=clear><input type="submit" value="...Go..." class="button" name=submitfiles></td>
    </tr>
</table>
</form>
</div>


<?php
}
?>
