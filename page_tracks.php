<?php

if ($loggedin != 1) {
	echo "<div class=sectie><h3>Browse Analysis Results</h3>\n";
	include('login.php');
	echo "</div>\n";
}
else {

//	$tdtype = array(
$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["X"] = "23";
$chromhash["Y"] = "24";
$chromhash["23"] = "X";
$chromhash["24"] = "Y";



$icons = array("<img src=images/bullets/no.png>", "<img src=images/bullets/accept.png>");

if ($_GET['type'] == "main") {
	echo "<div class=sectie>";
	echo "<h3>Genome Browser Tracks</h3>";
	echo "<p>Public Genome Browsers like UCSC and Ensembl allow users to upload or link their own data sets for inclusion in the browser as a custom track. From here you can create and browse such custom tracks, based upon various filtering methods and your own data. </p>";
	echo "<p>For inclusion of custom tracks into various browsers, we suggest reading the respective documentation provided by those browsers. </p>";
	echo "</div>";

}

elseif ($_GET['type'] == "create") {
	include('inc_tracks_create.inc');	

}

elseif ($_GET['type'] == "existing") {
		$dir = "/srv/www/htdocs/cnv/tracks/";
		$Files = array();
		
		$It =  opendir($dir);
		if (! $It)
			 die('Cannot list files for ' . $dir);
		
		while ($Filename = readdir($It)) {
 			if ($Filename == '.' || $Filename == '..' || substr($Filename, 0, (strlen($userid)+1)) !== "$userid." )
  				continue;
 			$Files[] = $Filename;
		}
		
		sort($Files);
		$num = count($Files) - 1 ;
		if ($num > 1) {
			echo "<div class=sectie>";
			echo "<h3>Previously created Genome Browser Tracks</h3>";
			echo "<p>";
			echo "<table cellspacing=0>";
			echo "<tr><th $firstcell>Name</th><th>Description</th><th>URL</th></tr>";
			For($i=0;$i<=$num;$i += 1) {
				$filename = substr($Files[$i], (strlen($userid)+1));
				$filename = str_replace("_", " ",$filename);
				$filename = substr($filename,0,-4);
				$fh = fopen("tracks/".$Files[$i], 'r');
				$line = fgets($fh);
				fclose($fh);
				$descr = preg_replace('/(.+description=\")(.+)(\" item.+)/i','$2',$line);
				echo "<tr><td $firstcell NOWRAP>$filename</td><td>$descr</td><td NOWRAP>https://$domain/$basepath/tracks/". $Files[$i] ."</td></tr>";
			}
			echo "</table>";
			echo "</div>";
		}
		
}

else {
	echo "main explaination to come";
}
}
?>
