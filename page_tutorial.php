
<div class=sectie>
<h3>Documentation</h3>
<h4>Please inform us if there is anything unclear in the documentation.</h4>
<p>Select the topics you need information on from the table below. Direct access to page specific documentation is available from the question mark in the top right corner of the page.</p>
<p>
<ul id=ul-simple style='width: 250px; padding:15px; border: solid 2px #e6e6e6; '>
 <li> - <a href='index.php?page=tutorial&amp;topic=intro'>Introduction</a></li>
 <li> - <a href='index.php?page=tutorial&amp;topic=handson'>Hands-On demo with sample data</a></li>
 <li> - <a href='index.php?page=tutorial&amp;topic=run'>Running an analysis</a></li>
 <li> - <a href='index.php?page=tutorial&amp;topic=browse'>Browsing results</a></li>
 <li> - <a href='index.php?page=tutorial&amp;topic=permissions'>Sharing Projects</a></li>
 <li> - <a href='index.php?page=documentation&amp;s=custom_methods'></a> </li>
</ul>
</p>
</div>
<?php
$topic = '';
if (isset($_GET['topic'])) {
	$topic = $_GET['topic'];
}
if ($topic == ''){
	$topic = 'intro';
}
include('inc_tutorial_'.$topic.'.inc');

?>

