<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}
if ($level < 3) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	echo "<p>Administrator account is required to view this page.</p>";
	echo "</div>";
	exit();
}
//require_once 'xmlLib.php';


// set database
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");


//////////////////////////
// 1. UPDATE THE SYSTEM //
//////////////////////////

if (isset($_POST['submit'])) {
	ob_end_flush();
	if (count($_POST['update']) > 0) {
		echo "<div class=sectie>";
		echo "<h3>Updating CNV-WebStore</h3>";
		echo "<p>The output of the updating process is shown below. </p>";
		echo " <p><ol>";
		$rand = rand(1,1500);
		$dodb = 0; // switch for database update.
		foreach ($_POST['update'] as $key => $value) {
			$updatedir = '';
			$title = '';
			switch ($value) {
				case "web": 
					$updatedir = $sitedir;
					$title = 'Web-Interface';
					break;
				case "analysis":
					$updatedir = $scriptdir;
					$title = 'CNV-Analysis Scripts';
					break;
				case "credentials":
					$updatedir = "$maintenancedir/../.Credentials";
					$title = 'Credentials Scripts';
					break;
				case "maintenance":
					$updatedir = $maintenancedir;
					$title = 'Maintenance Scripts';
					break;
				case "database":
					// seperate processing, can take a while !
					$dodb = 1;
					continue;
					break;
			}
			if ($updatedir == '') {
				continue;
			}
			$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $updatedir && git checkout master && git pull > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1";
			system($command);
			$content = file_get_contents("/tmp/$rand.git.out");
			system("rm -f /tmp/$rand.git.out");
			echo "<li>Update Results for $title <br/>";
			echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>";
		} 
		// now database
		if ($dodb == 1) {
			$updatedir = "$maintenancedir/../Database_Revisions";
			$command = " (echo 1 > /tmp/$rand.db.status && COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $updatedir && git checkout master && git pull > /tmp/$rand.git.out && perl ApplyUpdates.pl >> /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \" && echo 1 > /tmp/$rand.db.status && chmod 777 /tmp/$rand.db.status) > /dev/null 2>&1 &";
			echo "<script type='text/javascript'>interval1 = setInterval(\"reloadupdatespan('$rand')\",10000)</script>\n";
			echo "<li>Updating Database: <br/><pre id='db$rand' class=scrollbarbox>Starting Update Process...</pre></li>\n";
			system($command);
			flush();
			echo "<script type='text/javascript'>interval2 = setInterval(\"checkupdatestatus(2,$rand)\",10000)</script>\n"; 
		}
		// hide link untill finished.
		echo "</ol></p>";
		if ($dodb == 1) {
			echo "<p><span id='toupdates' style='display:none;'><a href='index.php?page=admin_pages&type=update'>Go back to the System updates page</a>.</span></p>";
		}
		else {
			echo "<p><a href='index.php?page=admin_pages&type=update'>Go back to the System updates page</a>.</p>";
		}			
		echo "</div>";
		// reset the cookie
		setcookie('updates','',1);
		// rerun check for updates
		//echo "<script type='text/javascript' src='javascripts/updates.js'></script>\n";
		exit();
	}
}

//////////////////////////
// 2. CHECK FOR UPDATES //
//////////////////////////

// output 
echo "<div class=sectie>";
echo "<h3>Checking for CNV-WebStore updates</h3>";
echo "<p> Check the items you want to update below. After submission, all the updates will be fetched from the main repository. It is highly recommended to perform updates to the database at a low usage time, as the system will be put in a read-only mode, and a backup will be taken, which may cause severe performance drops for a couple of minutes.</p>";
echo "<form action='index.php?page=admin_pages&type=update' method=POST>";
$updates = 0;
echo "<p><ol>";

// Check for updates for Web-Interface
$rand = rand(1,1500);
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $sitedir && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") >> /tmp/output 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! $content) {
	echo "<li>Web-Interface : no changes found</li>";
}
else {
	echo "<li>Web-Interface : <input type='checkbox' name='update[]' value='web' checked /> Update<br/>";
	echo "<textarea style='padding-left:2em;margin-left:2em;overflow:autp;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
	$updates++;
}
flush();
if ($webonly != 1) {
	// Check for updates for CNV Analysis scripts
	$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $scriptdir && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") >> /tmp/output 2>&1"; 
	system($command);
	$content = file_get_contents("/tmp/$rand.git.out");
	system("rm -f /tmp/$rand.git.out");
	if (! $content) {
		echo "<li>CNV-Analysis Scripts : no changes found</li>";
	}
	else {
		echo "<li>CNV-Analysis Scripts : <input type='checkbox' name='update[]' value='analysis' checked /> Update<br/>";
		echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
		$updates++;
	}
	flush();
}
// Check for updates for Credentials scripts
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $maintenancedir/../.Credentials && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! $content) {
	echo "<li>Credentials Scripts : no changes found</li>";
}
else {
	echo "<li>Credentials Scripts : <input type='checkbox' name='update[]' value='credentials' checked /> Update <br/>";
	echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
	$updates++;
}
flush();
// Check for updates for Maintenance scripts
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $maintenancedir && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! $content) {
	echo "<li>Maintenance Scripts : no changes found</li>";
}
else {
	echo "<li>Maintenance Scripts : <input type='checkbox' name='update[]' value='maintenance' checked /> Update <br/>";
	echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
	$updates++;
}
flush();
// Check for updates for Database 
$command = "(COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $maintenancedir/../Database_Revisions && git fetch && git log ..origin/master > /tmp/$rand.git.out && chmod 777 /tmp/$rand.git.out \") > /dev/null 2>&1"; 
system($command);
$content = file_get_contents("/tmp/$rand.git.out");
system("rm -f /tmp/$rand.git.out");
if (! $content) {
	// perhaps no changes, but not up to date (update failed?)
	$query = mysql_query("SELECT Revision FROM `GenomicBuilds`.`DatabaseRevision` ORDER BY Revision DESC LIMIT 1");
	$row = mysql_fetch_array($query);
	$dbrev = $row['Revision'];
	$next = $dbrev + 1;
	if (file_exists("$maintenancedir/../Database_Revisions/$next.sql")) {
		echo "<li>Database Revisions : <input type='checkbox' name='update[]' value='database' /> Update <br/>";
		$revstring = $next;
		$next++;
		while (file_exists("$maintenancedir/../Database_Revisions/$next.sql")){
			$revstring .= ", $next";
			$next++;
		}
		echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>Database is not in latest revision states:\n dbversion = $dbrev ; file(s) available for version(s) $revstring</textarea></li>";
		$updates++;
	}
	else {
		echo "<li>Database Revisions : no changes found</li>";
	}
}
else {
	echo "<li>Database Revisions : <input type='checkbox' name='update[]' value='database' /> Update <br/>";
	echo "<textarea style='padding-left:2em;margin-left:2em;overflow:auto;' cols=75 rows=9 readonly='yes'>$content</textarea></li>"; 
	$updates++;
}
flush();


echo "</ol></p>";
if ($updates > 0) {
	echo "<p><input type=submit name=submit value='Submit' class=button></p>";
}
echo "</form>";
echo "</div>";
?>
