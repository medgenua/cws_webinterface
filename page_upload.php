<div class=sectie>
<h3>Upload Large Data Sets</h3>
<h4>... Since php can't take them</h4>
<?php 
if ($loggedin != 1) {
	include('login.php');
	exit;
}

// Check if the FTP configuration is active
if (array_key_exists('FTP',$config) && $config['FTP'] == 1) {
	// was the form submitted ? 
	if (array_key_exists('activate',$_POST)) {
		// check if password is correct.
		$query = mysql_query("SELECT password FROM users WHERE id = '$userid'");
		$row = mysql_fetch_array($query);
		if (MD5($_POST['passwd']) != $row['password']) {
			// wrong password. 
			echo "<p><span class=nadruk style='color:red'>Wrong password provided: </span>Please try again.</p>";
		}
		else {
			// update mysql table
			$passwd = $_POST['passwd'];
			$sha1 = SHA1($passwd);
			mysql_query("UPDATE users SET passwd_sha1 = '$sha1' WHERE id = '$userid'");
		}
		
	}
	// check for the sha1 password
	$query = mysql_query("SELECT passwd_sha1 FROM users WHERE id = '$userid'");
	$row = mysql_fetch_array($query);	
	if ($row['passwd_sha1'] == '') {
		// no sha1 passwd => needs to activate FTP access
		echo "<p><span class=nadruk>Activate your FTP access</span></p>";
		echo "<p> Using FTP will allow you to upload large datasets in a more reliable way, and the datafiles will be kept on the server for reuse in multiple projects. After providing your password to activate access to the CNV-WebStore FTP account, login details will be provided.</p>";
		echo "<form METHOD='POST' action='index.php?page=upload'>";
		echo "<p>Password: <input name='passwd' type='password' size=20></p>";
		echo "<p><input type=submit class=button name=activate></p>";
		echo "</form>";
	}
	else {
		// were there files submitted to delete/unpack
		if (array_key_exists('process',$_POST)) {
			echo "<p><span class=nadruk>Processing file changes</span></p>";
			echo "<p><ol>";
			flush();
			// delete?
			$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username";
			if (array_key_exists('delete',$_POST)) {
 			  foreach ($_POST['delete'] as $key => $filename) {
				echo "<li>Deleting $filename</li>";
				flush();
				$command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $dir && rm -f '$filename' \"  ) ";
				system($command);
			  }
			}
			// unpack
			if (array_key_exists('unpack',$_POST)) {
			  foreach ($_POST['unpack'] as $key => $filename) {
				echo "<li>Unpacking $filename<br/>";
				flush();
				$command = " ( COLUMNS=1024 && echo $scriptpass | sudo -u $scriptuser -S bash -c \"COLUMNS=1024 && cd $dir && $maintenancedir/FTP_unpack/ftp_unpack.pl '$filename' \"  ) ";
				$output = shell_exec($command);
				echo "<textarea cols=50 rows=5>$output</textarea></li>";
				flush();
			  }
			}
			echo "</ol></p>";

		}
		// print instructions to log in
		echo "<p><span class=nadruk>CNV-WebStore FTP details</span></p>";
		echo "<p><ul style='padding-left:1em;'>";
		echo "<li><span class=underline>Server:</span> ".$config['FTP_HOST']."</li>";
		echo "<li><span class=underline>Port:</span> ".$config['FTP_PORT']."</li>";
		echo "<li><span class=underline>username:</span> $username</li>";
		echo "<li><span class=underline>Password:</span> Your CNV-WebStore Password</li>";
		echo" </ul></p>";
		echo "<p><span class=nadruk>Remarks:</span>";
		echo "<ul style='padding-left:1em;'>";
		echo "<li>- The allowed file types are .txt, .zip, .rar and (tar).gz.</li>";
		echo "<li>- If you upload compressed files, come back here after upload to unpack them.</li>";
		echo "</ul>";
		echo "</p>";
		// list files uploaded by the current user
		echo "<p><span class=nadruk>Uploaded Files</p>";
		$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username";
		$Files = array();
		$It =  opendir($dir);
		if ($It) {
			while ($Filename = readdir($It)) {
 				if ($Filename == '.' || $Filename == '..' )
  					continue;
 				//$LastModified = filemtime($dir . $Filename);
 				$Files[] = $Filename;
			}
		}	
		sort($Files);
		$num = count($Files) - 1 ;
		if ($num == 0) {
			echo "<p>No files uploaded. </p>";
		}
		else{
			echo "<p><form action='index.php?page=upload' method=POST>";
			echo "<table cellspacing=0>";
			echo "<tr><th $firstcell>File</th><th>Delete</th><th>Unpack</th></tr>";
			for($i=0;$i<=$num;$i += 1) {
				//$filename = substr($Files[$i], (strlen($userid)+1));
				$filename = $Files[$i];
				// check mime-type
				$mime = shell_exec("cd $dir && file --mime-type -b '$filename'");
				$mime = rtrim($mime);
				// all non-text/plain that are allowed need to be unpacked
				$unpack = '';
				if ($mime != 'text/plain') {
					$unpack = "<input type=checkbox name='unpack[]' value='$filename'> ($mime)";
				}
				echo "<tr><td $firstcell>$filename</td><td><input type=checkbox name='delete[]' value='$filename'></td><td>$unpack&nbsp;</td></tr>\n";
			}
			echo "</table></p>";
			echo "<p><input class=button type=submit value='Process' name='process'></form></p>";
		}
	}
		
}
// else, print notification and tutorial (if admin)
else {
	if ($level == 3) {
		echo "<p>CNV-WebStore supports the uploading of datafiles using FTP. To use this feature, perform the following configuration steps:\n";
		echo "<ol>\n";
		echo "<li>Install an ftp server with mysql support. The host should have access to both the CNV-WebStore mysql database and the homedir of the CNV-WebStore Scriptuser</li>";
		echo "<li>The following example steps are for proftpd, with the modules 'sql, sql-mysql AND sql-mysql-password' activated (this requires version >=  1.3.3d-8)</li>"; // and sql-passwd'</li>\n";
		// load modules
		echo "<li>Load The needed modules in /etc/proftpd/modules.conf<ul style='padding-left:1em;'>";
		echo "<li>LoadModule mod_sql.c</li>";
		echo "<li>LoadModule mod_sql_mysql.c</li>";
		echo "</ul></li>";
		// configure mysql access
		echo "<li>Set the following items in the /etc/proftpd/proftpd.conf file.<ul style='padding-left:1em'>";
		echo "<li>#Jail users to their homes</li>";
		echo "<li>DefaultRoot &nbsp;&nbsp;&nbsp;~</li>";
		echo "<li># Create home directories as needed, using chmod 755</li>";
		echo "<li># chmod 755 is set to make them www-user readable. Adapt to use groups</li>";
		echo "<li>CreateHome on 755 dirmode 755</li>";
		echo "<li># Set up mod_sql_password - CNV-WebStore passwords are stored as md5-encoded </li>";
		echo "<li>SQLPasswordEngine &nbsp;&nbsp;&nbsp; on</li>";
		echo "<li>SQLPasswordEncoding &nbsp;&nbsp;&nbsp;   hex </li>";
		echo "<li># Set up mod_sql to authenticate against the WebStore database</li>";
		echo "<li>SQLEngine&nbsp;&nbsp;&nbsp;on</li>";
		echo "<li>AuthOrder&nbsp;&nbsp;&nbsp;&nbsp;mod_sql.c</li>";
		echo "<li>SQLBackend&nbsp;&nbsp;&nbsp;     mysql</li>";
		echo "<li># copy DBHOST, DBUSER and DBPASS from .credentials file !</li>";
		echo "<li># adapt 'CNVanalysis-hg19' after a Genome Build update</li>";
		echo "<li>SQLConnectInfo&nbsp;&nbsp;&nbsp;CNVanalysis-hg19@DBHOST DBUSER DBPASS </li>";
		echo "<li>SQLAuthTypes &nbsp;&nbsp;&nbsp;  SHA1</li>";
		echo "<li>SQLAuthenticate&nbsp;&nbsp;&nbsp; users</li>";
		echo "<li># An empty directory in case chroot fails</li>";
		echo "<li>SQLDefaultHomedir &nbsp;&nbsp;&nbsp; /tmp</li>";
		echo "<li># Define a custom query for lookup that returns a passwd-like entry.  UID and GID should match your WebStore Script user.</li>";
		echo "<li>SQLUserInfo&nbsp;&nbsp;&nbsp; custom:/LookupWebStoreUser</li>";
		echo "<li>#SQLNamedQuery&nbsp;&nbsp;&nbsp;  LookupWebStoreUser SELECT \"username,passwd_sha1,'1002','1003','/home/cnvscriptuser/ftp-data/%U','/bin/bash' FROM users WHERE username='%U'\"</li>";
		echo "</ul></li>";
		// set .credentials
		echo "<li>Add the following lines to '$file':<ul style='padding-left:1em;'>";
		echo "<li>FTP=1</li>";
		echo "<li>FTP_HOST=YOUR FTP SERVER IP/NAME</li>";
		echo "<li>FTP_PORT=YOUR FTP SERVER PORT</li></ul>";
		echo "</ol></p>";
		
		echo "<p>FTP access is provided on a per user basis. Each user will have to activate access by providing their password here.</p>"; 
	}
	else {
		echo "<p>CNV-WebStore supports the uploading of datafiles using FTP, but this feature is not activated yet. Ask your system administrator to perform the additional configuration. If a WebStore Administrator visits this page, the configuration steps will be shown here. </p>";
	}

}

?>

</div>

