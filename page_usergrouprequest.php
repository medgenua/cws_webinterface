<?php
#################
# CONNECT TO DB #
#################
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

// get key and store as session
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
// check login
if ($loggedin != 1) {
	$key = $_GET['key'];
	$_SESSION['linkkey'] = $key;
	echo "<div class=sectie><h3>Manage Usergroups</h3>\n";
	include('login.php');
	echo "</div>";
}
elseif (!isset($_POST['approve']) && !isset($_POST['deny'])) {
	$ok = 1;
	if (isset($_GET['key'])) {
		$key = $_GET['key'] ;
	}
	elseif (isset($_SESSION['linkkey'])) {
		$key = $_SESSION['linkkey'];
	}
	else {
		echo "no key found. page will not function !";
		$ok = 0;
	}
	// get request details
	$query = mysql_query("SELECT uid, gid, motivation FROM usergrouprequests WHERE linkkey = '$key'");
	$row = mysql_fetch_array($query);
	$uid = $row['uid'];
	$gid = $row['gid'];
	$motivation = $row['motivation'];
	// get requesting user details
	$query = mysql_query("SELECT u.LastName, u.FirstName, a.name FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE u.id = '$uid'");
	$row = mysql_fetch_array($query);
	$reqfname = $row['FirstName'];
	$reqlname = $row['LastName'];
	$reqinst = $row['name'];
	// get usergroup details
	$query = mysql_query("SELECT name, description, administrator FROM usergroups WHERE id = '$gid'");
	$row = mysql_fetch_array($query);
	$groupname = $row['name'];
	$groupdescr = $row['description'];
	$groupadmin = $row['administrator'];
	if ($groupadmin != $userid) {
		echo "You are not the administrator for this usergroup. You are not allowed to process this usergroup request.";
		$ok = 0;
	}
	if ($ok == 1) {
		// print the form
		echo "<div class=sectie>\n";
		echo "<h3>Usergroup Access Requested</h3>\n";
		echo "<form action='index.php?page=usergrouprequest' method=POST>\n";
		echo "<input type=hidden name=key value='$key'>\n";
		echo "<p>\n";
		echo "<table cellspacing=0>\n";
		echo "<tr>\n";
		echo "<th colspan=2 $firstcell>Requesting User</th>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th $firstcell class=spec>User</th>\n";
		echo "<td>$reqfname $reflname</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th $firstcell class=spec>Institute</th>\n";
		echo "<td>$reqinst</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th colspan=2 $firtcell >Requested Usergroup</th>\n";
		echo "</tr>\n";
		echo "<th $firstcell class=spec>Group Name</th>\n";
		echo "<td>$groupname</td>\n";
		echo "</tr>\n";
		echo "<tr>\n";
		echo "<th $firstcell class=spec>Group Info</th>\n";
		echo "<td>$groupdescr</td>\n";
		echo "</tr>\n";
		if ($motivation != '') {
			echo "<tr>\n";
			echo "<th $firstcell class=spec>Motivation</th>\n";
			echo "<td>$motivation</td>\n";
		}
		echo "</table>\n";
		echo "</p><p>\n";
		echo "<input type=submit class=button value=Approve name=approve>&nbsp;&nbsp;&nbsp;<input type=submit class=button value=Deny name=deny></p>\n";
		echo "</div>\n";

	}
	
	
}
elseif (isset($_POST['approve']) ) {
	// get details
	$ok = 1;
	$key = $_POST['key'];
	// get request details
	$query = mysql_query("SELECT uid, gid, motivation FROM usergrouprequests WHERE linkkey = '$key'");
	$row = mysql_fetch_array($query);
	$uid = $row['uid'];
	$gid = $row['gid'];
	$motivation = $row['motivation'];
	// get requesting user details
	$query = mysql_query("SELECT u.LastName, u.FirstName, a.name, u.email FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE u.id = '$uid'");
	$row = mysql_fetch_array($query);
	$reqfname = $row['FirstName'];
	$reqlname = $row['LastName'];
	$reqinst = $row['name'];
	$email = $row['email'];
	// get usergroup details
	$query = mysql_query("SELECT name, description, administrator,editcnv, editclinic FROM usergroups WHERE id = '$gid'");
	$row = mysql_fetch_array($query);
	$groupname = $row['name'];
	$groupdescr = $row['description'];
	$groupadmin = $row['administrator'];
	$editcnv = $row['editcnv'];
	$editclinic = $row['editclinic'];
	if ($groupadmin != $userid) {
		echo "You are not the administrator for this usergroup. You are not allowed to process this usergroup request.";
		$ok = 0;
	}
	if ($ok == 1) {
		// join the usergroup
		$query = mysql_query("INSERT INTO usergroupuser (uid, gid) VALUES ('$uid','$gid') ON DUPLICATE KEY UPDATE uid = '$uid'");
		// update permissions on projects shared with groups. 
		$query = mysql_query("SELECT projectid FROM projectpermissiongroup WHERE groupid = '$gid'");
		$projects = array();
		if (mysql_num_rows($query) > 0) {
			while ($row = mysql_fetch_array($query)) {
				$pid = $row['projectid'];
				$projects[$pid] = 1;
				$insquery = mysql_query("INSERT INTO projectpermission (projectid, userid, editcnv, editclinic) VALUES ('$pid','$uid', '$editcnv', '$editclinic') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '$editcnv', VALUES(editcnv),'$editcnv'), editclinic = if(VALUES(editclinic) < '$editclinic', VALUES(editclinic),'$editclinic')");
			}
		}
			
		echo "<div class=sectie>\n";
		
		echo "<h3>Request processed</h3>\n";
		echo "<h4>$groupname...</h4>\n";
		echo "<p>$reqfname $reqlname is now a member of the group '$groupname'. When new projects are shared with this group, the user will gain access to them.</p>\n";
		
		if (count($projects) > 0) {
			echo "<p>In addition, the user gained access to the following previously shared Illumina projects: </p>\n";
			echo "<table cellspacing=0>\n";
			echo "<tr>\n";
			echo "<th $firstcell>Name</th><th>Collection</th><th>Chiptype</th><th>Details</th>\n";
			echo "</tr>\n";
			foreach($projects as $pid => $value) {
				$query = mysql_query("SELECT naam, collection, chiptype FROM project WHERE id = '$pid'");
				$row = mysql_fetch_array($query);
				$pname = $row['naam'];
				$pcol = $row['collection'];
				$pchip = $row['chiptype'];
				echo "<tr>\n";
				echo "<td $firstcell>$pname</td>\n";
				echo "<td>$pcol</td>\n";
				echo "<td>$pchip</td>\n";
				echo "<td><a href='index.php?page=results&type=tree&collection=$pcol&project=$pid&sample=' target='_blank'>Details</a></td>\n";
				echo "</tr>\n";
			}
			echo "</table>\n";
		
		}
		// send mail to user to notify
		$message = "Message sent from http://$domain\r";  
		  $message .= "Subject: CNV-analysis Page: Usergroup Request Approved\r\r";
		  $message .= "Your request to join the '$groupname' usergroup at the Illumina BeadArray data analysis and interpretation platform website was approved.\r\r";
		  $message .= "\r\rGeert Vandeweyer\r\n";
		  $headers = "From: no-reply@$domain\r\n";
		 /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
		// step 4: send the email
		mail($email,"CNV-analysis Page: Usergroup Request Approved",$message,$headers);
		$query = mysql_query("DELETE FROM usergrouprequests WHERE linkkey = '$key'");
	}
}
elseif (isset($_POST['deny']) ) {
	// get details
	$ok = 1;
	$key = $_POST['key'];
	// get request details
	$query = mysql_query("SELECT uid, gid, motivation FROM usergrouprequests WHERE linkkey = '$key'");
	$row = mysql_fetch_array($query);
	$uid = $row['uid'];
	$gid = $row['gid'];
	$motivation = $row['motivation'];
	// get requesting user details
	$query = mysql_query("SELECT u.LastName, u.FirstName, a.name, u.email FROM users u JOIN affiliation a ON u.Affiliation = a.id WHERE u.id = '$uid'");
	$row = mysql_fetch_array($query);
	$reqfname = $row['FirstName'];
	$reqlname = $row['LastName'];
	$reqinst = $row['name'];
	$email = $row['email'];
	// get usergroup details
	$query = mysql_query("SELECT name, description, administrator,editcnv, editclinic FROM usergroups WHERE id = '$gid'");
	$row = mysql_fetch_array($query);
	$groupname = $row['name'];
	$groupdescr = $row['description'];
	$groupadmin = $row['administrator'];
	$editcnv = $row['editcnv'];
	$editclinic = $row['editclinic'];
	if ($groupadmin != $userid) {
		echo "You are not the administrator for this usergroup. You are not allowed to process this usergroup request.";
		$ok = 0;
	}
	if ($ok == 1) {
		// remove from requests
		$query = mysql_query("DELETE FROM usergrouprequests WHERE linkkey = '$key'");
		// send email to user of denial?
		// send mail to user to notify
		$message = "Message sent from http://$domain\r";  
		  $message .= "Subject: CNV-analysis Page: Usergroup Request Denied\r\r";
		  $message .= "We are sorry to inform you your request to join the '$groupname' usergroup at the Illumina BeadArray data analysis and interpretation platform website was denied.\r\r";
		  $message .= "If you believe this is a mistake, feel free to reapply and include a clear motivation statement.\r\r";
		  $message .= "\r\rGeert Vandeweyer\r\n";
		  $headers = "From: no-reply@$domain\r\n";
		 /* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
		// step 4: send the email
		mail($email,"CNV-analysis Page: Usergroup Request Denied",$message,$headers);
		echo "<div class=sectie>\n";
		echo "<h3>Request Denied</h3>\n";
		echo "<p>You denied the request of $reqlname $reqfname to join the '$groupname' usergroup.</p>\n";
		echo "</div>\n";
	}
		
		
}
?>
