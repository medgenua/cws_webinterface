<?php
if ($loggedin != 1) {
	echo "<div class=sectie><h3>Access Denied</h3>\n";
	include('login.php');
	echo "</div>";
	exit();
}
echo "<div class=sectie>";

if (!isset($_GET['item'])) {
	echo "<h3>Server Version History</h3>";
	echo "<p>Please select a history section from the menu.</p>";
	echo" </div>";
}
elseif ($_GET['item'] == 'w') {
	echo "<h3>Web-Interface Version History</h3>";
	echo "<p>";
	exec("cd $sitedir && git log --pretty='format:%H%|%s|%ci' ",$output,$exit);
	$printed = 0;
	$rev = '';
	$date = '';
	$summary = '';
	echo "<ul class=indent>";
	foreach($output as $key => $line) {
		list($commit, $summary, $date) = explode("|", $line);
		$rev = substr($commit, 0, 7);
		echo "<li style='text-indent:-5px;' >$date&nbsp;&nbsp;&nbsp;&nbsp;Revision $rev&nbsp;&nbsp;&nbsp;&nbsp;$summary</li>";
	}
	echo "</ul></p>";


}
elseif ($_GET['item'] == 'a') {
	echo "<h3>Analysis Pipeline Version History</h3>";
	echo "<p>";
	exec("cd $scriptdir && git log --pretty='format:%H%|%s|%ci' ",$output,$exit);
	$printed = 0;
	$rev = '';
	$date = '';
	$summary = '';
	echo "<ul class=indent>";
	foreach($output as $key => $line) {
		list($commit, $summary, $date) = explode("|", $line);
		$rev = substr($commit, 0, 7);
		echo "<li style='text-indent:-5px;' >$date&nbsp;&nbsp;&nbsp;&nbsp;Revision $rev&nbsp;&nbsp;&nbsp;&nbsp;$summary</li>";
	}	
	echo "</ul></p>";


}
elseif ($_GET['item'] == 'd') {
	echo "<h3>Database Version History</h3>";
	echo "<p>";
	exec("cd $scriptdir/../Database_Revisions && git log --pretty='format:%H%|%s|%ci' ",$output,$exit);
	$printed = 0;
	$rev = '';
	$date = '';
	$summary = '';
	echo "<ul class=indent>";
	foreach($output as $key => $line) {
		list($commit, $summary, $date) = explode("|", $line);
		$rev = substr($commit, 0, 7);
		echo "<li style='text-indent:-5px;' >$date&nbsp;&nbsp;&nbsp;&nbsp;Revision $rev&nbsp;&nbsp;&nbsp;&nbsp;$summary</li>";
	}
	echo "</ul></p>";


}

echo "</div>";	
