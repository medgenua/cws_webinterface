<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/png");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

//$font = 'arial';





#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET data
$aid = $_GET['aid'];
#$userid = $_GET['u'];
$psid = $_GET['psid'];
$ppid = $_GET['ppid'];

# Get chiptype id 
$query = mysql_query("SELECT chiptypeid, chiptype, datatype FROM project WHERE id = '$ppid'");
$row = mysql_fetch_array($query);
$chipid = $row['chiptypeid'];
$chiptype = $row['chiptype'];
$datatype = $row['datatype'];

# GET the region
$query = mysql_query("SELECT a.cn, a.start, a.stop, a.chr FROM aberration a WHERE a.id = '$aid'");
$row = mysql_fetch_array($query);
$chr = $row['chr'];
$cn = $row['cn'];
$start = $row['start'];

function calculateMargin($aid) {
	$query = mysql_query("SELECT content, structure FROM datapoints WHERE id = '$aid'");
	$row = mysql_fetch_array($query);
	$content = $row['content'];
	$structure = $row['structure'];
	$increment = 0;
	for ($i = 0; $i<4;$i++) {
		if (substr($structure,$i,1) == '1') {
			$increment++;
		}
	}

	$datapoints = explode("_",$content);
	$nrelements = count($datapoints);
	$firstpos = null;
	$lastpos = null;

	for ($i=0; $i<$nrelements; $i+=$increment) {
		if (! is_numeric($datapoints[$i])) {
			continue;
		}
		if ($firstpos) {
			if ($datapoints[$i] < $firstpos) {
				$firstpos = $datapoints[$i];
			}
		}
		else {
			$firstpos = $datapoints[$i];
		}

		if ($lastpos) {
			if ($datapoints[$i] > $lastpos) {
				$lastpos = $datapoints[$i];
			}
		}
		else {
			$lastpos = $datapoints[$i];
		}
	}

	return array($firstpos, $lastpos);
}


#$chipid = $row['ID'];
#$chiptype =  $row['name'];
list($firstpos, $lastpos) = calculateMargin($aid);
error_log("Firstpos: <<$firstpos>>");
error_log("Lastpos: <<$lastpos>>");

// $firstpos = $start - 250000;
$stop = $row['stop'];
// $lastpos = $stop + 250000;
$window = $lastpos-$firstpos+1; 

//Specify constant values
$width = 415; //Image width in pixels
$height = 515; // Image height in pixels
$xoff = 25;

//Create the image resource
$image = imagecreatetruecolor($width, $height);
//making opaque colors
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$gray = ImageColorAllocate($image,190,190,190);
$orange = ImageColorAllocate($image,255, 179,0);

// making transparent colors;
$red75   = ImageColorAllocateAlpha($image, 255, 0, 0,90);
$blue75  = imageColorallocateAlpha($image,0,0,255,90); 
$green75 = imageColorAllocateAlpha($image,0,190,0,90);
$purple75 = ImageColorAllocateAlpha($image,136,34,135,90);
$orange75 = ImageColorAllocateAlpha($image,255, 179,0,90);
$cns = array('0' => $red75, '1' => $red75, '2' => $orange75, '3' => $blue75, '4' => $blue75);
$cnsopaque = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
#Fill background
imagefill($image,0,0,$white);

# DRAW MAIN GRAPH FIELDS
imagerectangle($image,$xoff,15,$xoff+380,195,$black);
imagerectangle($image,$xoff,220,$xoff+380,400,$black);

# PRINT TITLES
$title1 = "Log R Ratio";
if ($datatype == 'swgs') {
	$title2 = "Z-score";
}
else {
	$title2 = "B Allele Frequency";

}
$fontwidth = imagefontwidth(4);
$txtwidth = strlen($title)*$fontwidth;
$txtx = $xoff + 190 - ($txtwidth/2);
imagestring($image,4,$txtx,-3,$title1,$black);
$fontwidth = imagefontwidth(4);
$txtwidth = strlen($title)*$fontwidth;
$txtx = $xoff + 190 - ($txtwidth/2);
imagestring($image,4,$txtx,200,$title2,$black);

# DRAW GRID LINES
imageline($image,$xoff,38,$xoff + 380,38,$gray);
imageline($image,$xoff,60,$xoff + 380,60,$gray);
imageline($image,$xoff,83,$xoff+380,83,$gray);
imageline($image,$xoff,105,$xoff+380,105,$gray);
imageline($image,$xoff,128,$xoff+380,128,$gray);
imageline($image,$xoff,150,$xoff+380,150,$gray);
imageline($image,$xoff,173,$xoff+380,173,$gray);


# PRINT SCALES
imagestring($image,2,$xoff-7,8,'2',$black);
imagestring($image,2,$xoff-7,53,'0',$black);
imagestring($image,2,$xoff-12,98,'-2',$black);
imagestring($image,2,$xoff-12,143,'-4',$black);
imagestring($image,2,$xoff-12,187,'-6',$black);

error_log("Select datatype : $datatype");
if ($datatype == 'swgs') {
	# DRAW GRID LINES
	imageline($image,$xoff,238,$xoff+380,238,$gray);
	imageline($image,$xoff,256,$xoff+380,256,$gray);
	imageline($image,$xoff,274,$xoff+380,274,$gray);
	imageline($image,$xoff,292,$xoff+380,292,$gray);
	imageline($image,$xoff,310,$xoff+380,310,$gray);
	imageline($image,$xoff,328,$xoff+380,328,$gray);
	imageline($image,$xoff,346,$xoff+380,346,$gray);
	imageline($image,$xoff,364,$xoff+380,364,$gray);
	imageline($image,$xoff,382,$xoff+380,382,$gray);

	# PRINT SCALES
	imagestring($image,2,$xoff-7,213,'5',$black);
	imagestring($image,2,$xoff-7,231,'4',$black);
	imagestring($image,2,$xoff-7,249,'3',$black);
	imagestring($image,2,$xoff-7,267,'2',$black);
	imagestring($image,2,$xoff-7,285,'1',$black);
	imagestring($image,2,$xoff-7,303,'0',$black);
	imagestring($image,2,$xoff-12,321,'-1',$black);
	imagestring($image,2,$xoff-12,339,'-2',$black);
	imagestring($image,2,$xoff-12,357,'-3',$black);
	imagestring($image,2,$xoff-12,375,'-4',$black);
	imagestring($image,2,$xoff-12,393,'-5',$black);
}
else {
	imageline($image,$xoff,256,$xoff+380,256,$gray);
	imageline($image,$xoff,292,$xoff+380,292,$gray);
	imageline($image,$xoff,328,$xoff+380,328,$gray);
	imageline($image,$xoff,364,$xoff+380,364,$gray);
	
	imagestring($image,2,$xoff-7,213,'1',$black);
	imagestring($image,2,$xoff-20,249,'0.8',$black);
	imagestring($image,2,$xoff-20,285,'0.6',$black);
	imagestring($image,2,$xoff-20,321,'0.4',$black);
	imagestring($image,2,$xoff-20,357,'0.2',$black);
	imagestring($image,2,$xoff-7,393,'0',$black);
}

$firstx = number_format($firstpos,0,'',',');
imageline($image,$xoff,400,$xoff,420,$black);
imageline($image,$xoff+2,410,$xoff+5,407,$black);
imageline($image,$xoff+2,410,$xoff+5,413,$black);
imageline($image,$xoff+2,410,$xoff+15,410,$black);
imagestring($image,2,$xoff+18,404,$firstx,$black);

$lastx = number_format($lastpos,0,'',',');
$fontwidth = imagefontwidth(2);
$txtwidth = strlen($lastx)*$fontwidth;
$txtx = $xoff+362 - $txtwidth; 
imageline($image,$xoff+380,400,$xoff+380,420,$black);
imageline($image,$xoff+378,410,$xoff+375,407,$black);
imageline($image,$xoff+378,410,$xoff+375,413,$black);
imageline($image,$xoff+378,410,$xoff+365,410,$black);
imagestring($image,2,$txtx,404,$lastx,$black);

# GET corresponding cnv list
error_log("Fetching parental cnv list");
$cnvquery = mysql_query("SELECT parent_seen, parent_aid,ParentalCNVs FROM parent_offspring_cnv_relations WHERE aid = '$aid' AND parent_sid = '$psid'");


$scalex = 380 / $window;
$logrzero = 60;
$scalel = 180 / 8; // -6 to 2
$scaleb = 180 / 1; // 0 to 1
$scalez = 180 / 10; // -5 to +5
$bafzero = 400;
$zscorezero = 310;
$zeroc = array("0" => $black, "1" => $red);


$p_logR = array();
$p_zscore = array();
// more than one possible?
while ($row = mysql_fetch_array($cnvquery)) {
	$pseen = $row['parent_seen'];
	$paid = $row['parent_aid'];
	$pcnvs = $row['ParentalCNVs'];
	error_log("pcnvs : $pcnvs");
        error_log("pseen : $pseen");
        error_log("paid  : $paid");
	if ($pseen ==1) {
		#$table = "datapoints";
                error_log("Fetching data for parental cnv");
		$dataq = mysql_query("SELECT content, structure FROM datapoints WHERE id = '$paid'");
		if (mysql_num_rows($dataq) == 0) {
			// set structure to dummy value to prevent infinite loop
			$structure = "1100";
                } else {
			$datarow = mysql_fetch_array($dataq);
			$content = $datarow['content'];
			$structure = $datarow['structure'];
		}
	}
	else {
		$dataq = mysql_query("SELECT content FROM parents_datapoints WHERE id = '$paid'");
		## preset this, workaround because lack of 'structure' field in 'parents_datapoint' table
                if (mysql_num_rows($dataq) == 0) {
                        // set structure to dummy value to prevent infinite loop
                        $structure = "1100";
                } else {
			$datarow = mysql_fetch_array($dataq);
			$content = $datarow['content'];
			$structure = '1110';
		}
	}
	$increment = 0;
	for ($i = 0; $i<4;$i++) {
		if (substr($structure, $i, 1) == '1') {
			$increment++;
		}
	}
        error_log("Parental data ($structure)  has increment of $increment");
	$datapoints = explode("_",$content);
	$nrelements = count($datapoints);
	for ($i=0;$i<$nrelements;$i+=$increment) {
		$pos = $datapoints[$i];
		if ($pos < $firstpos || $pos > $lastpos) {
			continue;
		}
		$query = mysql_query("SELECT zeroed FROM probelocations WHERE chromosome = '$chr' AND position = '$pos' AND chiptype = '$chipid'");
		$zres = mysql_fetch_array($query);
		$zeroed = $zres['zeroed'];
		$x = $xoff + intval(round(($pos-$firstpos)*$scalex));

		// LogR
		$logr = $datapoints[$i+1];

		if ($logr != '.') {
			if ($logr < -6) {
				$logr = -6;
			}
			if ($pos >= $start && $pos <= $stop) {
				$p_logR[$pos] = $logr;
			}
			$logry = $logrzero - ($logr*$scalel);
			imagefilledellipse($image,$x,$logry,5,5,$zeroc[$zeroed]);

		}
		else {
			$logry = $logrzero;
			imageline($image,$x-1.5,$logry-1.5,$x+1.5,$logry+1.5,$zeroc['1']);
			imageline($image,$x+1.5,$logry-1.5,$x-1.5,$logry+1.5,$zeroc['1']);
		}

		if ($datatype == 'swgs') {
			$zscore = $datapoints[$i+2];
			if ($zscore != '.') {
				if ($zscore < -5) {
					$zscore = -5;
				}
				if ($zscore > 5) {
					$zscore = 5;
				}
				if ($pos >= $start && $pos <= $stop) {
					$p_zscore[$pos] = $zscore;
				}
				$zscorey = $zscorezero - ($zscore*$scalez);
				imagefilledellipse($image,$x,$zscorey,5,5,$zeroc['0']);
			}
			else {
				$zscorey = $zscorezero;
				imageline($image,$x-1.5,$zscorey-1.5,$x+1.5,$zscorey+1.5,$zeroc['1']);
				imageline($image,$x+1.5,$zscorey-1.5,$x-1.5,$zscorey+1.5,$zeroc['1']);
	
			}
		}
		else {
			$baf = $datapoints[$i+2];
			$bafy = $bafzero - ($baf*$scaleb);
			imagefilledellipse($image,$x,$bafy,5,5,$zeroc[$zeroed]);
		}

	}
	# DRAW CNV delineation
	// legacy code
	if ($pseen == 1) {
		$rquery = mysql_query("SELECT cn, start, stop, avgLogR, avgZscore, segmentZscore FROM aberration WHERE id = '$paid'");
		$rrow = mysql_fetch_array($rquery);
		$rstart = $rrow['start'];
		$rcn = $rrow['cn'];
		$rstop = $rrow['stop'];
		$avgLogR = $rrow['avgLogR'];
		$avgZ = $rrow['avgZscore'];
		$segmentZ = $rrow['segmentZscore'];
		if ($rstart < $firstpos) {
			$rstart = $firstpos;
		}
		if ($rstop > $lastpos) {
			$rstop = $lastpos;
		}
		$pstartx = $xoff + intval(round(($rstart-$firstpos)*$scalex));
		$pstopx = $xoff + intval(round(($rstop-$firstpos)*$scalex));
		imagefilledrectangle($image,$pstartx,15,$pstopx,195,$cns[$rcn]);
		imagefilledrectangle($image,$pstartx,220,$pstopx,400,$cns[$rcn]);
		$avgLogR = $logrzero - ($avgLogR*$scalel);
		imageline($image,$pstartx,$avgLogR,$pstopx,$avgLogR,$cnsopaque[$rcn]);
		if ($datatype == 'swgs') {
			if ($avgZ >= 5) {
				$avgZ = 4.7;
			}
			elseif ($avgZ <= -5) {
				$avgZ = -4.7;
			} 
			if ($segmentZ >= 5) {
				$segmentZ = 4.9;
			} 
			elseif ($segmentZ <= -5 ) {
				$segmentZ = -4.9;
			}
			$zavg = $zscorezero - ($avgZ*$scalez);
			$zsegment = $zscorezero - ($segmentZ*$scalez);
			imageline($image,$startx,$zavg,$stopx,$zavg,$black);
			imageline($image,$startx,$zsegment,$stopx,$zsegment,IMG_COLOR_STYLED);
		}

		
	}
	// new code : all are 'not seen' but hold info of all overlapping cnvs in parents;
	elseif ($pcnvs != '') {
		$rquery = mysql_query("SELECT cn, start, stop, avgLogR, avgZscore, segmentZscore FROM `aberration` WHERE id IN ($pcnvs)");
		while ($r = mysql_fetch_array($rquery)) {
			$rstart = $r['start'];
			$rcn = $r['cn'];
			$rstop = $r['stop'];
			$avgLogR = $r['avgLogR'];
			if ($rstart < $firstpos) {
				$rstart = $firstpos;
			}
			if ($rstop > $lastpos) {
				$rstop = $lastpos;
			}
			$startx = $xoff + intval(round(($rstart-$firstpos)*$scalex));
			$stopx = $xoff + intval(round(($rstop-$firstpos)*$scalex));
			imagefilledrectangle($image,$startx,15,$stopx,195,$cns[$rcn]);
			imagefilledrectangle($image,$startx,220,$stopx,400,$cns[$rcn]);
			$avgLogR = $logrzero - ($avgLogR*$scalel);
			imageline($image,$startx,$avgLogR,$stopx,$avgLogR,$cnsopaque[$rcn]);

			if ($datatype == 'swgs') {
				if ($avgZ >= 5) {
					$avgZ = 4.7;
				}
				elseif ($avgZ <= -5) {
					$avgZ = -4.7;
				} 
				if ($segmentZ >= 5) {
					$segmentZ = 4.9;
				} 
				elseif ($segmentZ <= -5 ) {
					$segmentZ = -4.9;
				}
				$zavg = $zscorezero - ($avgZ*$scalez);
				$zsegment = $zscorezero - ($segmentZ*$scalez);
				imageline($image,$startx,$zavg,$stopx,$zavg,$black);
				imageline($image,$startx,$zsegment,$stopx,$zsegment,IMG_COLOR_STYLED);
			}

		}
		
	}

}
// index CNV delineation
$startx = $xoff + intval(round(($start-$firstpos)*$scalex));
$stopx = $xoff + intval(round(($stop-$firstpos)*$scalex));
imageline($image,$stopx,15,$stopx,195,$red);
imageline($image,$startx,15,$startx,195,$red);
imageline($image,$startx,220,$startx,400,$red);
imageline($image,$stopx,220,$stopx,400,$red);

// print expected intensity for goal copy number
$style = array($cnsopaque[$cn], $cnsopaque[$cn], $cnsopaque[$cn], $cnsopaque[$cn], $cnsopaque[$cn], IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT, IMG_COLOR_TRANSPARENT);
imagesetstyle($image, $style);
$expect = array(-5.5,-0.45,0,0.3,0.75);
$logexp = $logrzero - ($expect[$cn] * $scalel);
imageline($image,$xoff,$logexp,$xoff+380,$logexp,IMG_COLOR_STYLED);

// print actual avg LogR
$avgLogR = array_mean($p_logR);
$logavg = $logrzero - ($avgLogR*$scalel);
imageline($image,$startx,$logavg,$stopx,$logavg,$black);
if ($datatype == 'swgs') {
	$avgZ = array_mean($p_zscore);
	if ($avgZ >= 5) {
		$avgZ = 4.7;
	}
	elseif ($avgZ <= -5) {
		$avgZ = -4.7;
	} 
	$zavg = $zscorezero - ($avgZ*$scalez);
	imageline($image,$startx,$zavg,$stopx,$zavg,$black);
}



// DRAW SCALE BOX
$scales = array("3Mb" => 3000000, "1Mb" => 1000000, "500Kb" => 500000, "250Kb" => 250000, "100Kb" => 100000, "75Kb" => 75000, "50Kb" => 50000);
foreach($scales as $stext => $scale) {
	$scaledscale = intval(round($scale*$scalex));
	if ($scaledscale < 55) {
		imagerectangle($image, $xoff + 320, 0, $xoff+380, 13, $black);
		$fontwidth = imagefontwidth(1);
		$txtwidth = strlen($stext)*$fontwidth;
		$txtx = $xoff + 350 -($txtwidth/2);
		imagestring($image,1,$txtx,1,$stext,$black);
		$xstart = $xoff + 350 -($scaledscale/2);
		$xstop = $xoff + 350 +($scaledscale/2);
		imageline($image,$xstart,11,$xstart,7,$black);
		imageline($image,$xstop,11,$xstop,7,$black);
		imageline($image,$xstart,9,$xstop,9,$black);
		break;
	}
}


# DRAW GENES
$locquery = "AND ((start BETWEEN '$firstpos' AND '$lastpos') OR (stop BETWEEN '$firstpos' AND '$lastpos') OR (start <= '$firstpos' AND stop >= '$lastpos'))";
$genequery = mysql_query("SELECT start, stop, strand, omimID, morbidID,exonstarts,exonends FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");

$title = "Genes (RefSeq & ENCODE)";
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($title)*$fontwidth;
$txtx = $xoff + 190 - ($txtwidth/2);
imagestring($image,3,$txtx,416,$title,$black);
imageline($image,$xoff,417,$xoff + 380,417,$black);



$y = 435;
//imagefilledrectangle($image,$xoff,48,$plotwidth,52,$gpos25);
while ($generow = mysql_fetch_array($genequery)) {

	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$gomim = $generow['omimID'];
	$gmorbid = $generow['morbidID'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($gmorbid != '') {
		$gcolor = $red;
	}
	elseif ($gomim != '') {
		$gcolor = $blue;
	}
	else {
		$gcolor = $black;
	}
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}
	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y,$gcolor);

	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y,$hooktip-2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip-2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y,$hooktip+2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip+2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}


	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $firstpos) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $lastpos) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$firstpos) * $scalex));
		$gexscaledstop = intval(round(($gexe-$firstpos) * $scalex));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		}
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-3,$xoff+$gexscaledstop,$y+3,$gcolor);
	}

}
# DRAW ENCODE CODING GENES

$y = $y + 10;
$genequery = mysql_query("SELECT start, stop, strand, exonstarts,exonends FROM encodesum WHERE chr = '$chr' $locquery ORDER BY start");
while ($generow = mysql_fetch_array($genequery)) {

	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	$gcolor = $gray;
	if ($gstart < $firstpos) {
		$gstart = $firstpos;
	}
	if ($gstop > $lastpos) {
		$gstop = $lastpos;
	}

	$scaledstart = intval(round(($gstart-$firstpos) * $scalex));
	$scaledstop = intval(round(($gstop-$firstpos) * $scalex));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y,$gcolor);

	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y,$hooktip-2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip-2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y,$hooktip+2,$y-2,$gcolor);
			imageline($image,$hooktip,$y,$hooktip+2,$y+2,$gcolor);
			$hooktip = $hooktip +5;
		}
	}


	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $firstpos) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $lastpos ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$firstpos) * $scalex));
		$gexscaledstop = intval(round(($gexe-$firstpos) * $scalex));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		}
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-3,$xoff+$gexscaledstop,$y+3,$gcolor);
	}

}

# DRAW CONTROLES
$y = $y+10;
$title = "HapMap for $chiptype";
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($title)*$fontwidth;
$txtx = $xoff + 190 - ($txtwidth/2);
imagestring($image,3,$txtx,$y,$title,$black);
imageline($image,$xoff,$y,$xoff+380,$y,$black);
$chipstring = $chipid;
$chipstring = str_replace('4','10',$chipstring);
if ($chr == 23) {
	$arr = array(0,1,2,3,4);
}
else {
	$arr = array(0,1,3,4);
}
$y = $y+5;
foreach ($arr as $i) {
	$y = $y+10;
	$string = "CN:$i";
	imagestring($image,2,$xoff-25,$y-5,$string,$gray);
	$querystring = "SELECT start, stop FROM consum WHERE chr = '$chr' AND chips = '$chipstring' AND cn = '$i' $locquery ";
	$abquery = mysql_query("$querystring");
	while ($abrow = mysql_fetch_array($abquery)) {
		$cstart = $abrow['start'];
		$cstop = $abrow['stop'];
		if ($cstart < $firstpos) {
			$cstart = $firstpos;
		}
		if ($cstop > $lastpos) {
			$cstop = $lastpos;
		}
		$scaledstart = intval(round(($cstart-$firstpos) * $scalex));
		$scaledstop = intval(round(($cstop-$firstpos) * $scalex));
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$cnsopaque[$i]);
	}

}
$y = $y+10;
imageline($image,$xoff+380,400,$xoff+380,$y,$black);
imageline($image,$xoff,400,$xoff,$y,$black);
imageline($image,$xoff,$y,$xoff+380,$y,$black);


//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 


function array_mean($a) {
	$v = array_values($a);
	if (count($v) > 0) {
		return(array_sum($a) / count($a));
	}
	else {
		return(0);
	}
}
?>

