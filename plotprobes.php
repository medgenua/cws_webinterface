<?php
//Tell the browser what kind of file is coming in
header("Content-Type: image/jpeg");
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
#ob_start();
$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include(".LoadCredentials.php");
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("$db");
###################
# GET POSTED VARS #
###################
$chr = $_GET['chr'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$rchr = $_GET['rc'];
$rstart = $_GET['rsa'];
$rstop = $_GET['rso'];
#$plotwidth = $_COOKIE['plotwidth'];
#$plotwidth = 500;
$plotwidth = $_GET['pw'] ;
$chiptype = $_GET['ct'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
###################################
# DEFINE SOME VARS for IMAGE SIZE #
###################################
$window = $stop-$start+1;

###########################
# DEFINE IMAGE PROPERTIES #
###########################
$xoff = 110; 
$scalef = ($plotwidth-$xoff-1)/($window);
$scale = 10000;
$stext = '10kb';
##########################
# GET CENTROMER POSITION #
##########################
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];


#############################
# Create the image resource #
#############################
# query nr chiptypes
$query = mysql_query("SELECT count(ID) as types FROM chiptypes");
$row = mysql_fetch_array($query);
$height = 10*$row['types'] + 150;
$width = $plotwidth; //Image width in pixels
$image = ImageCreate($width, $height);

#################
# CREATE COLORS #
#################
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$lightred = ImageColorAllocate($image, 255,205,205);
$blue  = imageColorallocate($image,0,0,255); 
$darklightblue = ImageColorAllocate($image,122,189,255);
$lightblue = ImageColorAllocate($image,235,235,255);
$green = ImageColorAllocate($image,0,190,0);
$lightgreen = ImageColorAllocate($image,156,255,56);
$purple = ImageColorAllocate($image,136,34,135);
$lightpurple = ImageColorAllocate ($image, 208, 177, 236);
$orange = ImageColorAllocate($image,255, 179,0);
$lightorange = ImageColorAllocate($image,255,210,127);
$cyan = ImageColorAllocate($image,0,255,255);
$lightpink = ImageColorAllocate($image,205,183,181);
$pink = ImageColorAllocate($image,255,182,193);
$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$cnslight = array('0' => $lightred, '1' => $lightred, '2' => $lightorange, '3' => $darklightblue, '4' => $darklightblue,'9' => $lightpink);
$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue, '9' => $pink);
#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

// OUTPUT settings link
#$fontwidth = imagefontwidth(2);
#$pref = 'Preferences';
#$txtwidth = strlen($pref)*$fontwidth;
#$txtx = $width - $txtwidth - 3;
#imagestring($image,2,$txtx,1,$stop,$gpos50);

# CREATE SCALE
$scaledscale = intval(round($scale*$scalef));
imagerectangle($image, 0, 0, 60, 20, $black);
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($stext)*$fontwidth;
$txtx = 30 -($txtwidth/2);
imagestring($image,3,$txtx,1,$stext,$black);
$xstart = 30-($scaledscale/2);
$xstop = 30+($scaledscale/2);
imageline($image,$xstart,18,$xstart,14,$black);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
#imagestring($image,3,$txtx,20,$project,$black);
# CREATE KARYO BANDS
$y = 25;  //start here


$arm = 'p';
$query = mysql_query("SELECT start, stop, name, gieStain FROM cytoBand WHERE chr= '$chr' $locquery ORDER BY start");

while ($row = mysql_fetch_array($query)) { 
	$cstart = $row['start'];
	$cstop = $row['stop'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$name = $row['name'];
	$gieStain = $row['gieStain'];
	$scaledstart = intval(round(($cstart-$start)*$scalef));
	$scaledstop = intval(round(($cstop-$start)*$scalef));
	//$xend = $x + $scaled;
	if ($cstop == $lastp) {
		if ($gieStain != 'gneg') {
			imagefilledpolygon($image, array($xoff+$scaledstart,$y,$xoff+$scaledstop,($y+10),$xoff+$scaledstart,($y+20)),3, $colors[$gieStain]);
		}
		imagepolygon($image, array($xoff+$scaledstart,$y,$xoff+$scaledstop,$y+10,$xoff+$scaledstart,$y+20),3,$black);
		$nexttriang = 1;
	}
	elseif (isset($nexttriang) && $nexttriang == 1) {
		if ($gieStain != 'gneg') {
			imagefilledpolygon($image, array($xoff+$scaledstart,$y+10,$xoff+$scaledstop,$y,$xoff+$scaledstop,$y+20),3,$colors[$gieStain]);
		}
		imagepolygon($image, array($xoff+$scaledstart,$y+10,$xoff+$scaledstop,$y,$xoff+$scaledstop,$y+20),3,$black);
		$nexttriang = 0;
	}
	else{
		if ($gieStain != 'gneg') {
			imagefilledrectangle($image, $xoff+$scaledstart, $y, $xoff+$scaledstop, $y+20, $colors[$gieStain]);
		}
		imagerectangle($image, $xoff+$scaledstart, $y, $xoff+$scaledstop, $y+20, $black);
	}
	$fontwidth = imagefontwidth(3);
	$fullname = $chrtxt . $name;
	$txtwidth = strlen($fullname)*$fontwidth;
	if ($txtwidth+5 < ($scaledstop-$scaledstart)) {
		$txtx = $xoff + ($scaledstop+$scaledstart)/2 - $txtwidth/2;
		if ($gieStain != "gpos100") {
			imagestring($image,3,$txtx,$y+4,$fullname,$black);		
		}
		else {
			imagestring($image,3,$txtx,$y+4,$fullname,$white);
		}

	}
	
	
	//$x = $xend;

}
$y=50;
#############
# DRAW GRID #
#############
#$scaledstop = intval(round(($cstop-$start)*$scalef));
$xline1 = intval(round(($start-$start)*$scalef));
$xline2 = intval(round((($window / 10))*$scalef));
$xline3 = intval(round((($window / 10)*2)*$scalef));
$xline4 = intval(round((($window / 10)*3)*$scalef));
$xline5 = intval(round((($window / 10)*4)*$scalef));
$xline6 = intval(round((($window / 10)*5)*$scalef));
$xline7 = intval(round((($window / 10)*6)*$scalef));
$xline8 = intval(round((($window / 10)*7)*$scalef));
$xline9 = intval(round((($window / 10)*8)*$scalef));
$xline10 = intval(round((($window / 10)*9)*$scalef));
$xline11 = intval(round(($stop-$start)*$scalef));

imageline($image,$xoff+$xline1,$y,$xoff+$xline1,$height,$lightblue);
imageline($image,$xoff+$xline2,$y,$xoff+$xline2,$height,$lightblue);
imageline($image,$xoff+$xline3,$y,$xoff+$xline3,$height,$lightblue);
imageline($image,$xoff+$xline4,$y,$xoff+$xline4,$height,$lightblue);
imageline($image,$xoff+$xline5,$y,$xoff+$xline5,$height,$lightblue);
imageline($image,$xoff+$xline6,$y,$xoff+$xline6,$height,$lightblue);
imageline($image,$xoff+$xline7,$y,$xoff+$xline7,$height,$lightblue);
imageline($image,$xoff+$xline8,$y,$xoff+$xline8,$height,$lightblue);
imageline($image,$xoff+$xline9,$y,$xoff+$xline9,$height,$lightblue);
imageline($image,$xoff+$xline10,$y,$xoff+$xline10,$height,$lightblue);
imageline($image,$xoff+$xline11,$y,$xoff+$xline11,$height,$lightblue);


# DRAW REFSEQ GENES
$y = 48;
imagestring($image,3,0,$y-5,"Genes",$black);
imagefilledrectangle($image,$xoff,$y+2,$plotwidth,$y+3,$gpos75);

$y = $y+10;
$genequery = mysql_query("SELECT start, stop, strand, omimID, morbidID,exonstarts,exonends FROM genesum WHERE chr = '$chr' $locquery ORDER BY start");
imagestring($image,2,0,$y-5,'RefSeq',$black);
while ($generow = mysql_fetch_array($genequery)) {
	
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$gomim = $generow['omimID'];
	$gmorbid = $generow['morbidID'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($gmorbid != '') {
		$gcolor = $red;
		$garrow = $black;
	}
	elseif ($gomim != '') {
		$gcolor = $blue;
		$garrow = $white;
	}
	else {
		$gcolor = $black;
		$garrow = $white;
	}
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y+2,$hooktip-2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y+2,$hooktip+2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $start) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $stop ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$start) * $scalef));
		$gexscaledstop = intval(round(($gexe-$start) * $scalef));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-1,$xoff+$gexscaledstop,$y+5,$gcolor);
	}

}


# DRAW ENCODE CODING GENES	
$y = $y + 10;
$genequery = mysql_query("SELECT start, stop, strand, level,exonstarts,exonends FROM encodesum WHERE coding = 1 AND chr = '$chr' $locquery ORDER BY start");
imagestring($image,2,0,$y-5,'Enc.Coding',$black);
while ($generow = mysql_fetch_array($genequery)) {
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$glevel = $generow['level'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($glevel == 1 ) {
		$gcolor = $black;
		$garrow = $black;
	}
	elseif ($glevel == 2) {
		$gcolor = $gpos50;
		$garrow = $white;
	}
	elseif ($glevel == 3) {
		$gcolor = $gpos25;
	}
	else {
		$gcolor = $black;
		$garrow = $white;
	}
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y+2,$hooktip-2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y+2,$hooktip+2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $start) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $stop ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$start) * $scalef));
		$gexscaledstop = intval(round(($gexe-$start) * $scalef));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-1,$xoff+$gexscaledstop,$y+5,$gcolor);
	}
}
# DRAW ENCODE NON-CODING GENES
$y = $y + 10;
$genequery = mysql_query("SELECT start, stop, strand, level,exonstarts,exonends FROM encodesum WHERE coding = 0 AND chr = '$chr' $locquery ORDER BY start");
imagestring($image,2,0,$y-5,'Enc.N.Coding',$black);
while ($generow = mysql_fetch_array($genequery)) {
	$gstart = $generow['start'];
	$gstop = $generow['stop'];
	$gstrand = $generow['strand'];
	//$gsymbol = $gene
	$glevel = $generow['level'];
	$gexstart = $generow['exonstarts'];
	$gexstarts = explode(',',$gexstart);
	$gexend = $generow['exonends'];
	$gexends = explode(',',$gexend);
	if ($glevel == 1 ) {
		$gcolor = $black;
		$garrow = $black;
	}
	elseif ($glevel == 2) {
		$gcolor = $gpos50;
		$garrow = $white;
	}
	elseif ($glevel == 3) {
		$gcolor = $gpos25;
	}
	else {
		$gcolor = $black;
		$garrow = $white;
	}
	if ($gstart < $start) {
		$gstart = $start;
	}
	if ($gstop > $stop) {
		$gstop = $stop;
	}
	$scaledstart = intval(round(($gstart-$start) * $scalef));
	$scaledstop = intval(round(($gstop-$start) * $scalef));
	//imagefilledrectangle($image,$xoff+$scaledstart,57,$xoff+$scaledstop,63,$gcolor);
	imageline($image,$xoff+$scaledstart,$y+2,$xoff+$scaledstop,$y+2,$gcolor);
	
	if ($gstrand == '+') {
		$hooktip = $xoff + $scaledstart + 5;
		while ($hooktip <= $scaledstop + $xoff -1) {
			imageline($image,$hooktip,$y+2,$hooktip-2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip-2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	elseif ($gstrand == '-')  {
		$hooktip = $xoff + $scaledstart +2;
		while ($hooktip <= $scaledstop + $xoff - 5) {
			imageline($image,$hooktip,$y+2,$hooktip+2,$y,$gcolor);
			imageline($image,$hooktip,$y+2,$hooktip+2,$y+4,$gcolor);
			$hooktip = $hooktip +5;
		}
	}
	

	foreach($gexstarts as $key => $gexs) {
		if ($gexs < $start) {
			continue;
		}
		$gexe = $gexends[$key];
		if ($gexe > $stop ) {
			continue;
		}
		$gexscaledstart = intval(round(($gexs-$start) * $scalef));
		$gexscaledstop = intval(round(($gexe-$start) * $scalef));
		if ($gexscaledstop-$gexscaledstart < 1){
			$gexscaledstop = $gexscaledstart +1;
		} 
		imagefilledrectangle($image,$xoff+$gexscaledstart,$y-1,$xoff+$gexscaledstop,$y+5,$gcolor);
	}
}

# DRAW SEGMENTAL DUPLICATIONS
$y = $y +12;
imagestring($image,3,0,$y-7,"SegDups",$black);
imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
$query = mysql_query("SELECT start, stop FROM segdup_sum WHERE chr = '$chr' $locquery");
$nrows = mysql_num_rows($query);
$y = $y+10;
imagestring($image,2, 0,$y-5,'UCSC SegDups',$black);
while ($row = mysql_fetch_array($query)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
}
		


$y = $y + 12;

#############################
## DRAW REGiON OF INTEREST ##
#############################
imagestring($image,3,0,$y-7,"Your Region",$black);
imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
$y = $y+10;
if ($rstart < $start) {
	$rstart = $start;
}
if ($rstop > $stop) {
	$rstop = $stop;
}
$scaledstart = intval(round(($rstart-$start) * $scalef));
$scaledstop = intval(round(($rstop-$start) * $scalef));
if ($chr == $rchr) {
	imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$gpos25);
	imagestring($image,1,0,$y-2,"Chr".$chromhash[$rchr].":".number_format($rstart,0,'',',')."-".number_format($rstop,0,'',','),$black);
}
$y = $y + 12;
#################
## DRAW PROBES ##
#################

imagestring($image,3,0,$y-7,"Probes",$black);
imagefilledrectangle($image,$xoff,$y,$plotwidth,$y+1,$gpos75);
$query = mysql_query("SELECT ID, name FROM chiptypes ORDER BY name");
while ($row = mysql_fetch_array($query)) {
	$y = $y+10;
	$name = $row['name'];
	$id = $row['ID'];
	$name = str_replace("Human","",$name);
	$fontwidth = imagefontwidth(2);
	$txtwidth = strlen($name)*$fontwidth;
	$txtx = 10;
	if ($id == $chiptype) {
		imagestring($image,2,0,$y-5,$name,$red);
	}
	else {
		imagestring($image,2,0,$y-5,$name,$black);
	}
	$subquery = mysql_query("SELECT position, zeroed FROM probelocations WHERE chromosome = '$chr' AND (position BETWEEN $start AND $stop) AND chiptype = '$id'");
	while ($subrow = mysql_fetch_array($subquery)) {
		$pos = $subrow['position'];
		$zer = $subrow['zeroed'];
		$scaledstart = intval(round(($pos  - $start) * $scalef));
		$scaledstop = intval(round(($pos - $start) * $scalef));
		if ($zer == 1) {
			$col = $red;
		}
		else {
			$col = $black;
		}
		#if ($scaledstart == $scaledstop) {
		#	$scaledstop++;
		#	$scaledstart--;
		#}
		imagefilledrectangle($image,$xoff+$scaledstart,$y,$xoff+$scaledstop,$y+5,$black);
	}
}
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

