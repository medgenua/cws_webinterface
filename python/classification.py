import sys
from dataclasses import dataclass, field
from typing import Dict, List

from CMG.DataBase import MySQL, MySqlError


@dataclass
class Region:
    chr: int
    start: int 
    stop: int

@dataclass
class CNV(Region):
    cn: int
    bins: int
    dc: int = None
    
    def __post_init__(self):
        # round CN (for mosaicisms)
        self.cn = round(self.cn)


@dataclass
class Classifier:
    credentials: Dict # Db connection credentials
    uid: int # ID of CWS user
    id: int # Db id
    
    name: str = None                # Name of the classifier
    type: str = None                # local or wgs
    dc : int  = None                # diagnostic class to assign
    sdc: int = None                 # if local, do dc of CNV need to match?
    cns: List = None                # on which copy number does the classifier work
    cn_match: int = None            # do a CN comparison?
    cn_match_type: str = None       # exact or type CN comparison?
    number_matches: int = None      # the minimal amount of matches needed
    sbigger: int = None             # maximum allowed size ratio if CNV is bigger compared to classifier
    ssmaller: int = None            # minimal size ratio if CNV is smaller compared to classifier
    soverlap: int = None            # minimal size ratio if CNV is overlapping with classifier
    sextend: int  = None            # maximum allowed number of extending basepairs if CNV is extended on 1/both sides
    regions: List = field(default_factory=list)     # list of regions (for local classifiers)
    gender: List = field(default_factory=list)      # genders to apply the classifier
    cns: List = field(default_factory=list)         # CN's to apply the classifier on
    min_bins: int = None
    trackonly: int = None
    include_parents: int = None
    include_controls: int = None    
    sorteddc: int = None
    
    # Db handler
    dbh: str = None
    overlapping_cnvs: List = field(default_factory=list)
    
    def __post_init__(self):
        # log.info("Connecting to database")
        # Get Db connection
        self.dbh = MySQL(self.credentials['user'], self.credentials['passwd'], self.credentials['host'], self.credentials['db'])
        # get info classifier
        classifierquery = self.dbh.runQuery(f"SELECT c.Name,c.Gender,c.WholeGenome,c.CN,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter,uc.Type,uc.TrackOnly, uc.IncludeParents,uc.IncludeControls,uc.MinSNP FROM `Classifier` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE uc.uid = '{self.uid}' AND uc.Type IN (1,3) and c.id = '{self.id}'  ")
        classifierquery = classifierquery.pop()

        self.genders = classifierquery['Gender'].strip().split(",")
        self.cns = [int(c) for c in classifierquery['CN'].strip().split(",")]
        if classifierquery['WholeGenome'] == 1:
            self.type = 'wg'
        else:
            self.type = 'local'
            #  load the regions
            self.get_classifier_regions()
        self.name = classifierquery['Name']
        self.dc = classifierquery['dc']
        self.sdc = classifierquery['sdc']
        self.cn_match = classifierquery['scn']
        self.cn_match_type = classifierquery['scnd']
        self.number_matches =  classifierquery['snr']
        self.sbigger = classifierquery['sbigger']
        self.ssmaller = classifierquery['ssmaller']
        self.soverlap = classifierquery['soinner']
        self.sextend = classifierquery['soouter']
        self.trackonly = classifierquery['TrackOnly'] 
        self.include_parents = classifierquery['IncludeParents'] 
        self.include_controls = classifierquery['IncludeControls']
        self.min_bins = classifierquery['MinSNP']
        
        classquery = self.dbh.runQuery("SELECT id, sort_order FROM diagnostic_classes")
        for classrow in classquery:
            if self.dc == classrow['id']:
                self.sorteddc = classrow['sort_order']
                break
    
    
    # compare with cnv object: return True/False
    def run(self, cnv) -> bool:
        nr_matches = 0
        # Does it need to classify on CN of CNV?
        if not cnv.cn in self.cns:
            return False
        #  Minimum nr of bins is not reached to classify
        if self.min_bins > cnv.bins:
            return False
                
        if self.type == 'wg':
            self.get_overlapping_cnvs(cnv)
            for cnv in self.overlapping_cnvs:
                if self.compare_cnvs(cnv):
                    nr_matches += 1
            if nr_matches >= self.number_matches:
                return True
            else:
                return False
                        
        elif self.type == 'local':
            # check if CNV overlap with associated region, scan through regions until a hit is found
            for region in self.regions:
                if not self.does_region_intersect(cnv, region):
                    # print("does not intersect")
                    continue
                # compare the region
                if self.number_matches == 0:
                    if self.compare_boundaries(cnv, region): 
                        return True
                else:
                    self.get_overlapping_cnvs(cnv)
                    for cnv in self.overlapping_cnvs:
                        if self.compare_cnvs(cnv):
                            nr_matches += 1
                    if nr_matches >= self.number_matches:
                        return True
                    else:
                        return False


    # 'region' can be either Region or CNV object
    def compare_boundaries(self, cnv, region):
        # calculate size ratio
        ratio = (cnv.stop - cnv.start) / (region.stop - region.start)
        # Get inner coordinates for calculating overlap size ratio
        coord = [cnv.start, cnv.stop, region.start, region.stop]
        coord.sort()
        overlap_start,overlap_stop = coord[1:3]
        overlap_ratio = (overlap_stop - overlap_start) / (region.stop - region.start)
        
	# scenario A: CNV is exactly the region, always True
	# making separate scenario for this, else it would go to scenario B/C
        if region.start == cnv.start and region.stop == cnv.stop:
            return True

        # scenario B: new CNV is bigger: meets maximal requirement
        if region.start >= cnv.start and region.stop <= cnv.stop and (self.sbigger == -1 or self.sbigger >= ratio):
            return True
        
        # scenario C: new CNV is smaller: meets minimal requirement
        if region.start <= cnv.start and region.stop >= cnv.stop and self.ssmaller <= ratio:
            return True
    
        # scenario D: overlapping remaining
        # meets minimal required overlap
        if self.soverlap <= overlap_ratio:
            # CNV is extended on 5' side: meets maximal bp requirement
            if region.start > cnv.start and region.stop >= cnv.stop and region.start - cnv.start <= self.sextend:
                return True

            # CNV is extended on 3' side: meets maximal bp requirement
            if region.start <= cnv.start and region.stop < cnv.stop and cnv.stop - region.stop <= self.sextend:
                return True
        
        return False


    def compare_cnvs(self, new_cnv, db_cnv):
        #  first check boundaries
        if not self.compare_boundaries(new_cnv, db_cnv):
            return False
        
        #  check diagnostic class
        if self.sdc and new_cnv.dc != db_cnv:
            return False
        
        # check CN
        if self.cn_match:
            if self.cn_match_type == 'exact' and new_cnv.dc != db_cnv.dc:
                return False
            if self.cn_match_type == 'type':
                if db_cnv.cn >= 2 and new_cnv.cn < 2:
                    return False
                if db_cnv.cn <= 2 and new_cnv.cn > 2:
                    return False
                if db_cnv.cn == 2 and new_cnv.cn != 2:
                    return False

        return True


    def does_region_intersect(self, cnv, region):
        if cnv.chr != region.chr: # different chromosome
            return False
        elif region.stop < cnv.start: # region is upstream from CNV
            return False
        elif region.start > cnv.stop: #region is downstream from CNV
            return False
        else:
            return True
                   
                    
    def get_classifier_regions(self):
        result = self.dbh.runQuery(f"SELECT chr, start, stop FROM `Classifier_x_Regions` WHERE cid = {self.id}")
        for row in result:
            self.regions.append(Region(chr=row['chr'], start=row['start'], stop=row['stop']))


    def get_overlapping_cnvs(self, cnv):
        query = f"SELECT chr, start, stop, cn, class FROM `aberration` \
            a JOIN `project` p JOIN `projectpermission` pp \
            JOIN `sample` s ON a.idproj = p.id AND a.sample = s.id AND a.idproj = pp.projectid \
            WHERE pp.userid = '$uid' AND a.sample != '$sid' AND chr = '{cnv.chr}' AND \
            ( (start BETWEEN {cnv.start} AND {cnv.stop}) OR (stop BETWEEN {cnv.start} AND {cnv.stop}) OR (start <= {cnv.start} AND stop >= {cnv.stop}))"
        
        if self.trackonly:
            query += " AND s.intrack = 1 AND a.idproj = s.trackfromproject"
        if not self.include_parents:
            query += " AND p.collection <> 'Parents'"
        if not self.include_controls:
            query += " AND p.collection <> 'Controls'"
        
        result = self.dbh.runQuery(query);
        for row in result:
            self.overlapping_cnvs.append(CNV(chr=row['chr'], start=row['start'], stop=row['stop'], cn=row['cn'], dc=row['class']))
