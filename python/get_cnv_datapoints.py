#!/usr/bin/env python

import cgi
import json
import pandas as pd

from utils import getcnvdata

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# from cgi call
form = cgi.FieldStorage()
# sample_name = form['sample'].value
datafile = form['data'].value
chromosome = str(form['chromosome'].value)
start = int(float(form['start'].value))
stop = int(float(form['stop'].value))
maxstart = int(float(form['maxstart'].value))
maxstop = int(float(form['maxstop'].value))
resolution = form['resolution'].value


for repl in [('23', 'X'), ('24', 'Y')]:
    chromosome = chromosome.replace(repl[0], repl[1])
chromosome = 'chr' + chromosome

inputs = {
    'datafile': datafile,
    'chromosome': chromosome,
    'start': start,
    'stop': stop,
    'maxstart': maxstart,
    'maxstop': maxstop,
    'resolution': resolution
}


# # load data
data = pd.read_csv(datafile, sep='\t')

datapoints, nrbins = getcnvdata(data, chromosome, start, stop, maxstart, maxstop, resolution)

# inputs = getcnvdata(data, chromosome, start, stop, maxstart, maxstop, resolution)

output = {
    'datapoints': datapoints,
    'nrbins': nrbins
}

    
print("Content-Type: application/json\n")
print(json.dumps(output))

