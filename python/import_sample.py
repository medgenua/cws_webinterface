#!/usr/bin/env python

"""
pre-reqs:
cws conda package including
CMG package for MySQL
"""

import argparse
import cgi
from collections import defaultdict
import json
import os
import pandas as pd
import datetime
import sys

from pathlib import Path
from classification import Classifier, CNV
from utils import getcnvdata
from CMG.DataBase import MySQL, MySqlError
# from CMG.UZALogger import setup_logging, get_logger

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


api = True

# get input from form / cli arguments
form = cgi.FieldStorage()
formdata = dict()
input = dict()
remarks = dict()


# get either POST variables
if form:
    input['sample'] = sample = form['sample'].value
    input['gender'] = gender = form['gender'].value
    input['datafile'] = datafile = form['datafile'].value
    input['cnvfile'] = cnvfile = form['cnvfile'].value
    input['projectname'] = projectname = form['projectname'].value
    input['resolution'] = resolution = form['resolution'].value
    input['macon'] = macon = form['macon'].value
    input['n_reads'] = n_reads = form['n_reads'].value
    input['gendertest'] = gendertest = form['gendertest'].value
    input['apikey'] = apikey = form['apikey'].value
    if 'upd_indication' in form:
        input['upd_indication'] = upd_indication = form['upd_indication'].value
    else:
        input['upd_indication'] = upd_indication = None
    dbuser = form['dbuser'].value
    dbpass = form['dbpass'].value
    dbhost = form['dbhost'].value
    input['library'] = library = form['library'].value
    if 'share' in form:
        input['sharegroup'] = sharegroup = form['share'].value
    else:
        input['sharegroup'] = sharegroup = None

    if 'infofile' in form:
        input['sampleinfofile'] = sampleinfofile = form['infofile'].value
    else:
        input['sampleinfofile'] = sampleinfofile = None
        
    

# or get CLI arguments (you can only use the short argument, else the cgi form is filled! (why?))
else:
    api = False
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--apikey", help="API key of user to do import for", default=None)
    parser.add_argument("-c", "--cnvfile", help="sample", default=None)
    parser.add_argument("-d", "--datafile", help="sample", default=None)
    parser.add_argument("-g", "--gender", help="sample", default=None)
    parser.add_argument("-G", "--gendertest", help="Data predicted gender", default=None)
    parser.add_argument("-H", "--dbhost", help="Database host server", default=None)
    parser.add_argument("-i", "--infofile", help="(Optional) Sample info file", default=None)
    parser.add_argument("-l", "--library", help="Library kit used for sequencing", default=None)
    parser.add_argument("-m", "--macon", help="maternal contamination", default=None)
    parser.add_argument("-n", "--n_reads", help="Number of filtered reads", default=None)
    parser.add_argument("-p", "--projectname", help="sample", default=None)
    parser.add_argument("-P", "--dbpass", help="Database password", default=None)
    parser.add_argument("-r", "--resolution", help="sample", default=None)
    parser.add_argument("-s", "--sample", help="sample", default=None)
    parser.add_argument("-S", "--share", help="(Optional) Name of CNV-WebStore group to share the data with", default=None)
    parser.add_argument("-u", "--dbuser", help="Database user", default=None)
    parser.add_argument("-U", "--upd_indication", help="List of chromosome where UPD indication is given", default=None)

    
    # load the provided settings.
    args = parser.parse_args()
    input['sample'] = sample = args.sample
    input['gender'] = gender = args.gender
    input['datafile'] = datafile = args.datafile
    input['cnvfile'] = cnvfile = args.cnvfile
    input['projectname'] = projectname = args.projectname
    input['resolution'] = resolution = args.resolution
    input['macon'] = macon = args.macon
    input['n_reads'] = n_reads = args.n_reads
    input['gendertest'] = gendertest = args.gendertest
    input['apikey'] = apikey = args.apikey
    input['upd_indication'] = upd_indication = args.upd_indication
    dbuser = args.dbuser
    dbpass = args.dbpass
    dbhost = args.dbhost
    input['library'] = library = args.library
    input['sharegroup'] = sharegroup = args.share
    input['sampleinfofile'] = sampleinfofile = args.infofile
        

## derive plot path.

plot_path = cnvfile.replace('cnvs.byZ.sign.tsv','plot.pdf')
if not os.path.isfile(plot_path):
   plot_path = ''

# find UPD imputation data (dont check, just import, thats done later before analysis)
if upd_indication:
    _tmp = Path(cnvfile)
    vcf_upd_path = _tmp.parents[1] / 'glimpse' / (sample + '.vcf.gz')
else:
    vcf_upd_path = None
    
"""
DATABASE
"""

# CONNECT TO DATABASE
# log.info("Connecting to database")
# Get current genome build/database to update
dbh = MySQL(dbuser, dbpass, dbhost, 'GenomicBuilds')
build = dbh.runQuery("SELECT name, StringName FROM CurrentBuild")

if build:
    ucscbuild = build[0]['name']
    newbuild = build[0]['StringName']
    db = "CNVanalysis" + ucscbuild
    # print(f"Current build database (build {newbuild})")
else:
    sys.exit("No genomic build found")

dbh.select_db(db)

dbcredentials = {
    'user'      : dbuser,
    'passwd'    : dbpass,
    'host'      : dbhost,
    'db'        : db
}


# get user id for import
userquery = dbh.runQuery(f"SELECT * FROM users WHERE apikey = '{apikey}'")
uid = userquery[0].get('id')
input['import_as'] = userquery[0].get('username')


# libquery, only when directly calling this script (API fills in default)
if not library:
    libq = dbh.runQuery("SELECT name FROM `chiptypes` where datatype = 'sWGS' ORDER BY `chiptypes`.`ID`  DESC LIMIT 1")
    if libq[0].get('name'):
        library = libq[0].get('name')
        remarks = {
            'library': f"Default is used: {library}" 
        }
        
"""
Classifiers
- wholegenome (wg): based on characteristics of existing CNV's in the database
- local: based on a curated list of regions
"""
classifiers = list()
classifiersquery = dbh.runQuery(f"SELECT c.id, c.Name,c.Gender,c.WholeGenome,c.CN,c.dc,c.scn,c.scnd,c.sdc,c.snr,c.sbigger,c.ssmaller,c.soinner,c.soouter,uc.Type,uc.TrackOnly, uc.IncludeParents,uc.IncludeControls,uc.MinSNP FROM `Classifier` c JOIN `Users_x_Classifiers` uc ON c.id = uc.cid WHERE uc.uid = '{uid}' AND uc.Type IN (1,3)")
for classifier in classifiersquery:
    cid = classifier['id']
    new_classifier = Classifier(
        credentials = dbcredentials,
        uid = uid,
        id = cid,
    )
    classifiers.append(new_classifier)


# load data
data = pd.read_csv(datafile, sep='\t')


"""
PARSE DATA
"""
cnvs = list()
mosaics = list()
# parse CNV data
with open(cnvfile) as f:
    for linenr, line in enumerate(f):
        if linenr == 0:
            continue
        mos = False
        nonmos = False
        
        id, idx, chrom, start, stop, nrbins, maxstart, maxstop, meanz, segmentz, logr, significance, cnv_call, mos_call = line.strip().split('\t')
        # both shouldn't be empty when the input file are already filtered on significance level
        if cnv_call == '.' and mos_call == '.':
            continue
        
        
        if cnv_call != '.':
            nonmos = True
            cnvtype, cn, likelihood = cnv_call.split(";")
            try:
                cn = int(cn)
                likelihood = float(likelihood)
            except Exception as e:
                print(str(e))   

        if mos_call != '.':
            mos = True
            try:
                moscnvtype, mosperc, moscn, moslikelihood = mos_call.split(";")
                moscn = float(moscn)
                moslikelihood = float(moslikelihood)
                mosfrac = float(mosperc.rstrip("%"))/100
            except Exception as e:
                print(str(e))   
            
        try:
            nrbins = int(nrbins)
            start = int(float(start))
            stop = int(float(stop))
            maxstart = int(float(maxstart))
            maxstop = int(float(maxstop))
            logr = float(logr)
            meanz = float(meanz)
            segmentz = float(segmentz)
        except Exception as e:
            print(str(e))

        size = stop - start
        maxsize = maxstop - maxstart

        # clean up chr for webstore import
        chrom_int = chrom
        for repl in [('X', '23'), ('Y', '24'), ('chr', '')]:
            chrom_int = chrom_int.replace(repl[0], repl[1])
        try:
            chrom_int = int(chrom_int)
        except Exception as e:
            print(str(e))

        # get datapoints
        data_string, total_nrbins = getcnvdata(data, chrom, start, stop, maxstart, maxstop, resolution)


        # for aberration (ie nonmosaic) table        
        if nonmos:
            # run classifiers
            suggestedclass = 99
            classified = False
            cnv_obj = CNV(chrom_int, start, stop, cn, nrbins)
            for classifier in classifiers:
                # return True on matching classifier
                if classifier.run(cnv_obj):
                    # suggest the lowest class possible
                    if classified == True and classifier.sorteddc < suggestedclass.sorteddc:
                        suggestedclass = classifier
                        classified = True
                    elif classified == False and classifier.sorteddc < suggestedclass:
                        suggestedclass = classifier
                        classified = True
                        
                        
            
            cnv = {
                'chr'           : chrom_int,
                'start'         : start,
                'stop'          : stop,
                'largestart'    : maxstart,
                'largestop'     : maxstop,
                'nrbins'        : nrbins,
                'totalnrbins'   : total_nrbins,
                'cn'            : cn,
                'size'          : stop - start,
                'logr'          : logr,
                'meanz'         : meanz,
                'segmentz'      : segmentz,
                'significance'  : significance,
                'resolution'    : resolution,
                'datapoints'    : data_string,
                'likelihood'    : likelihood,
                'classified'    : classified,                
                'classifier'    : suggestedclass
            }
            # print(f"{sample}\t{cnv['cn']}\t{cnv['chr']}:{cnv['start']}-{cnv['stop']}\t{cnv['nrbins']}\t{applied_classifier_id}({applied_classifier_name})")
            cnvs.append(cnv)

        if mos:
            # run classifiers
            suggestedclass = 99
            classified = False
            cnv_obj = CNV(chrom_int, start, stop, moscn, nrbins)
            for classifier in classifiers:
                # return True on matching classifier
                if classifier.run(cnv_obj):
                    # suggest the lowest class possible
                    if classified == True and classifier.sorteddc < suggestedclass.sorteddc:
                        suggestedclass = classifier
                        classified = True
                    elif classified == False and classifier.sorteddc < suggestedclass:
                        suggestedclass = classifier
                        classified = True

            mosaic = {
                'chr'           : chrom_int,
                'start'         : start,
                'stop'          : stop,
                'largestart'    : maxstart,
                'largestop'     : maxstop,
                'nrbins'        : nrbins,
                'totalnrbins'   : total_nrbins,
                'cn'            : moscn,
                'size'          : stop - start,
                'logr'          : logr,
                'meanz'         : meanz,
                'segmentz'      : segmentz,
                'significance'  : significance,
                'resolution'    : resolution,
                'likelihood'    : moslikelihood,
                'fraction'      : mosfrac,
                'type'          : moscnvtype,
                'datapoints'    : data_string,
                'classified'    : classified,                
                'classifier'    : suggestedclass
                
            }
            # print(f"{sample}\t{cnv['cn']}\t{cnv['chr']}:{cnv['start']}-{cnv['stop']}\t{cnv['nrbins']}\t{applied_classifier_id}({applied_classifier_name})")
            mosaics.append(mosaic)



# Create CWS project
created_time = datetime.datetime.now().strftime('%d/%m/%Y - %H:%M:%S')

# Get Library kit ID
lidq = dbh.runQuery(f"SELECT ID as id from `chiptypes` WHERE name = '{library}'")
lid = lidq[0].get('id')

# check if project exist & user has access to add samples.
pidq = dbh.runQuery("SELECT p.id FROM `project` p JOIN `projectpermission` pp ON pp.projectid = p.id WHERE p.naam = %s AND pp.userid = %s AND editsample = 1", [projectname, uid])
if pidq:
    pid = pidq[0].get('id')
    # print(f"project already exists: adding sample to project {projectname} with PID {pid}")
else:
    # else create new project
    # now minsnp is hard-coded as two. update to dynamic option with filtering of results ? 
    projectquery = f"INSERT INTO `project` (naam, created, collection, userID, chiptype, chiptypeid, datatype,minsnp) VALUES ('{projectname}', '{created_time}', 'Diagnostics', '{uid}', '{library}', '{lid}', 'swgs','2')"
    # pid = dbh.insertQuery(projectquery, [projectname, created_time, uid])
    pid = dbh.insertQuery(projectquery)
    
    # print(f"Creating new project {projectname} with PID {pid}")
    # Set projectpermissions for user
    ppermissionsquery = f"INSERT INTO `projectpermission` (userid, projectid, editsample, editclinic, editcnv) VALUES ('{uid}', '{pid}', '1' , '1', '1') " # ON DUPLICATE KEY UPDATE userid=userid"
    dbh.doQuery(ppermissionsquery)


# share project if requested
if sharegroup:
    gidq = dbh.runQuery("SELECT id FROM `usergroups` WHERE name = %s", sharegroup)
    with open("/tmp/db.log","w") as fh:
        fh.write("SELECT id FROM `usergroups` WHERE name = %s\n")
        fh.write(sharegroup)
    if gidq: 
        gid = gidq[0].get('id')
        # set group permissions
        dbh.insertQuery(f"INSERT INTO projectpermissiongroup (projectid, groupid, sharedby) VALUES ('{pid}', '{gid}','{uid}') ON DUPLICATE KEY UPDATE groupid = '{gid}'");
        # get other users from group
        guidq = dbh.runQuery(f"SELECT u.id FROM `users` u JOIN usergroupuser ug ON u.id = ug.uid WHERE ug.gid = {gid}")
        other_uids = [row.get('id') for row in guidq if row.get('id') != uid]
        for other_uid in other_uids:
            dbh.insertQuery(f"INSERT INTO projectpermission (projectid, userid, editcnv, editclinic,editsample) VALUES ('{pid}','{other_uid}', '1', '1','1') ON DUPLICATE KEY UPDATE editcnv = if(VALUES(editcnv) < '1', VALUES(editcnv),'1'), editclinic = if(VALUES(editclinic) < '1', VALUES(editclinic),'1'),editsample = if(VALUES(editsample) < '1', VALUES(editsample),'1')")
    else:
        pass
        # TODO add log entry that it couldn't be shared
            

# Parse data and create samples
samplequery = f"INSERT INTO `sample` (chip_dnanr, gender, intrack, trackfromproject) VALUES ('{sample}', '{gender}' , '1', '{pid}')"
sid = dbh.insertQuery(samplequery)
# add quality metrics here : 
psquery = f"INSERT INTO `projsamp` (idsamp, idproj, `swgs_resolution`,`swgs_plots`, `swgs_nreads`,`swgs_macon`,`swgs_gendertest`,`datatype`, `sampleinfo_file`, `upd_indication`, `upd_imput_data`) VALUES ('{sid}', '{pid}','{resolution}','{plot_path}','{n_reads}','{macon}','{gendertest}','swgs', '{sampleinfofile}', '{upd_indication}', '{vcf_upd_path}')"
psid = dbh.insertQuery(psquery)


# import cnvs
if cnvs:
    for cnv in cnvs:
        # get gene count
        genequery = f"SELECT Count(DISTINCT symbol) as nr FROM `genes` \
            WHERE chr='{cnv['chr']}' AND (\
                ( (start BETWEEN {cnv['start']} AND {cnv['stop']}) OR (end BETWEEN {cnv['start']} AND {cnv['stop']}) ) \
                OR (start <= {cnv['start']} AND end >= {cnv['stop']}) \
            )"
        genequeryresult = dbh.runQuery(genequery)
        nrgenes = genequeryresult[0]['nr'] 

        # insert cnv
        cnvquery = f"INSERT INTO `aberration` (chr, start, stop, largestart, largestop, sample, idproj, cn, nrsnps, total_nrsnps, nrgenes, avgLogR, avgZscore, segmentZscore, likelihood_ratio, size, datatype) VALUES \
        ('{cnv['chr']}', '{cnv['start']}', '{cnv['stop']}',  '{cnv['largestart']}', '{cnv['largestop']}', '{sid}', '{pid}', '{cnv['cn']}', '{cnv['nrbins']}', '{cnv['totalnrbins']}', '{nrgenes}', '{cnv['logr']}', '{cnv['meanz']}', '{cnv['segmentz']}', '{cnv['likelihood']}', '{cnv['size']}', 'swgs')"
        aid = dbh.insertQuery(cnvquery)
        cnv['aid'] = aid
        # insert datapoints
        dataquery = f"INSERT INTO datapoints (id, content, structure, datatype) VALUES ('{aid}', '{cnv['datapoints']}','1110', 'swgs')"
        dataid = dbh.insertQuery(dataquery)

        #  update class if classified
        if cnv['classified']:
            classquery = f"SELECT c.id, c.Name as appliedrule, c.dc as class, dc.name as classname FROM Classifier c JOIN diagnostic_classes dc ON c.dc = dc.id WHERE c.id = {cnv['classifier'].id}"
            result = dbh.runQuery(classquery)
            classinfo = result.pop()
            # update aberration table
            dbh.doQuery(f"UPDATE `aberration` SET class = '{classinfo['class']}' WHERE id = '{aid}'")
            # insert log statement
            logentry = f"Diagnostic Class Set (automatically) to {classinfo['classname']}"
            arguments = f" Applied Rule : {classinfo['appliedrule']}";
            dbh.insertQuery(f"INSERT INTO `log` (sid, pid, aid, uid, entry, arguments) VALUES ('{sid}', '{pid}', '{aid}', '{uid}', '{logentry}', '{arguments}')")

# import mosaic cnvs 
if mosaics:
    for mosaic in mosaics:
        # get gene count
        genequery = f"SELECT Count(DISTINCT symbol) as nr FROM `genes` \
            WHERE chr='{mosaic['chr']}' AND (\
                ( (start BETWEEN {mosaic['start']} AND {mosaic['stop']}) OR (end BETWEEN {mosaic['start']} AND {mosaic['stop']}) ) \
                OR (start <= {mosaic['start']} AND end >= {mosaic['stop']}) \
            )"
        genequeryresult = dbh.runQuery(genequery)
        nrgenes = genequeryresult[0]['nr'] 

        # insert mosaic
        mosaicquery = f"INSERT INTO `aberration_mosaic` (chr, start, stop, largestart, largestop, sample, idproj, cn, type, mosaic_perc, nrsnps, total_nrsnps, nrgenes, avgLogR, avgZscore, segmentZscore, likelihood_ratio, size, datatype) VALUES \
        ('{mosaic['chr']}', '{mosaic['start']}', '{mosaic['stop']}',  '{mosaic['largestart']}', '{mosaic['largestop']}', '{sid}', '{pid}', '{mosaic['cn']}', '{mosaic['type']}', '{mosaic['fraction']}', '{mosaic['nrbins']}', '{mosaic['totalnrbins']}', '{nrgenes}', '{mosaic['logr']}', '{mosaic['meanz']}', '{mosaic['segmentz']}', '{mosaic['likelihood']}', '{mosaic['size']}', 'swgs')"
        aid = dbh.insertQuery(mosaicquery)
        mosaic['aid'] = aid
        
        # insert datapoints
        dataquery = f"INSERT INTO datapoints_mosaic (id, content, structure, datatype) VALUES ('{aid}', '{mosaic['datapoints']}','1110', 'swgs')"
        dataid = dbh.insertQuery(dataquery)

        #  update class if classified
        if mosaic['classified']:
            classquery = f"SELECT c.id, c.Name as appliedrule, c.dc as class, dc.name as classname FROM Classifier c JOIN diagnostic_classes dc ON c.dc = dc.id WHERE c.id = {mosaic['classifier'].id}"
            result = dbh.runQuery(classquery)
            classinfo = result.pop()
            # update aberration table
            dbh.doQuery(f"UPDATE `aberration_mosaic` SET class = '{classinfo['class']}' WHERE id = '{aid}'")
            # insert log statement
            logentry = f"Diagnostic Class Set (automatically) to {classinfo['classname']}"
            arguments = f" Applied Rule : {classinfo['appliedrule']}";
            dbh.insertQuery(f"INSERT INTO `log_mos` (sid, pid, aid, uid, entry, arguments) VALUES ('{sid}', '{pid}', '{aid}', '{uid}', '{logentry}', '{arguments}')")




# export results as JSON for API
results = defaultdict(dict)
results = {
    'input': input,
    'result' : {
            'result'    : 'success',
            'sid'       : sid,
            'pid'       : pid
    }      
}
if remarks:
    results['remarks'] = remarks

print("Content-Type: application/json\n")
print(json.dumps(results))
