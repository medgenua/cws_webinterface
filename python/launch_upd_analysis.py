#!/usr/bin/env python

import argparse
import cgi
import configparser
import json
import os
import subprocess
import sys

from CMG.DataBase import MySQL
from CMG.UZALogger import setup_logging, get_logger

def parse_config(file_path):
    config = configparser.ConfigParser()
    try:
        config.read(file_path)
    except:
        with open(file_path, 'r') as file:
            # Add a dummy section header to make the file compatible
            content = '[DEFAULT]\n' + file.read()
        config.read_string(content)
        return config['DEFAULT']


def get_vcf_path(sid, pid):
    vcfquery = f"SELECT upd_imput_data FROM projsamp WHERE idsamp='{sid}' and idproj='{pid}'"
    return dbh.runQuery(vcfquery)[0].get('upd_imput_data')



"""
GET CGI/CLI INPUT
"""

form = cgi.FieldStorage()
formdata = dict()
input = dict()
remarks = dict()

if form:
    sid = form.getvalue("sid", "No input received")
    pid = form.getvalue("pid", "No input received")
    scriptuser = form.getvalue("scriptuser", "No input received")
    credentialsfile = form.getvalue("credentialsfile", "No input received")

else:
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sid", help="sample ID of index samples", default=None)
    parser.add_argument("-p", "--pid", help="Project ID of index sample", default=None)
    parser.add_argument("-u", "--scriptuser", help="User to run the docker with", default=None)
    parser.add_argument("-c", "--credentialsfile", help="File with DB credentials", default=None)
    args = parser.parse_args()
    
    sid = args.sid
    pid = args.pid
    scriptuser = args.scriptuser
    credentialsfile = args.credentialsfile

# subprocess.Popen()    

"""
GET CONFIG INFO
"""
config = parse_config(credentialsfile)
email = config.get("log_email")


"""
LOGGING
"""
setup_logging(
    name = "UPD_analysis_" + sid,
    level = "DEBUG",
    log_dir = "/home/" + scriptuser + "/CNV-WebStore/UPD_logs",
    to_addr = email,
    quiet=True
)
log = get_logger()


"""
Get input (from POST call or CLI)
"""

log.debug("Launching UPD analysis")
# log.debug("Get input data")


# db
dbuser = config.get("dbuser")
dbpass = config.get("dbpass")
dbhost = config.get("dbhost")


"""
CONNECT TO DATABASE
"""
log.debug("Connecting to database")

dbh = MySQL(dbuser, dbpass, dbhost, 'GenomicBuilds')
build = dbh.runQuery("SELECT name, StringName FROM CurrentBuild")

if build:
    ucscbuild = build[0]['name']
    newbuild = build[0]['StringName']
    db = "CNVanalysis" + ucscbuild
else:
    sys.exit("No genomic build found")

dbh.select_db(db)

samplenamequery = f"SELECT chip_dnanr FROM sample WHERE id='{sid}'"
indexname = dbh.runQuery(samplenamequery)[0].get('chip_dnanr')

# launch UPD analysis
py_bin = sys.executable 
script = os.path.join(os.getcwd(), 'upd_analysis.py')
command = [
    py_bin,
    script,
    "-s", sid,
    "-p", pid,
    "-u", scriptuser,
    "-c", credentialsfile
]

# Send JSON response to finish AJAX call
try:
    # Start the secondary script as a separate process
    subprocess.Popen(command, start_new_session=True)
    log.debug("upd_analysis.py launched successfully.")
    status = "success"
    how = "successfully"
except Exception as e:
    log.debug(f"An error occurred while launching the script: {e}", file=sys.stderr)
    status = "error"
    how = "unsuccessfully"
finally:
    # Exit the main script
    results = {
        'status': status,
        'message': f"UPD analysis launched {how} for sample {indexname} (sid={sid}, pid={pid})",
        'data': {
            'input': {
                "sid" : sid,
                "pid" : pid,
                "user" : scriptuser
            },
            'output' : {
                "dnanr" : indexname,
                "user"  : scriptuser,
                "cmd"   : ' '.join(command)
            }
        }
    }
    log.debug(results)
    print("Content-Type: application/json\n")
    print(json.dumps(results))
    log.debug("upd_analysis.py launched successfully.")
