#!/usr/bin/env python

import cgi
from collections import defaultdict
import glob
import json
import os
import sys

# from cgi call
form = cgi.FieldStorage()
# print(form)
# sys.exit()
sampleinfofile = form['sampleinfo'].value
projectname = form['projectname'].value
# for testing

basepath = os.path.dirname(sampleinfofile)

# so it's the same as in cnv-webstore 
gender_convert = {
    'male'  : 'Male',
    'female': 'Female',
    'unknown': 'Unknown'
}

samples = dict()
with open(sampleinfofile) as f:
    for linenumber, line in enumerate(f):
        if linenumber == 0:
            continue
        
        sample_name, gender, resolution, macon, gender_test, p_chrY, n_reads, n_reads_auto, sd, z_sd, n_segments, n_sign_segm, p_sign_bins, qc, cnvfile, datafile, *rest = line.rstrip('\n').split("\t")
        if rest:
           try:
               upd_imputation = rest[0]
           except:
               upd_imputation = ''
        else:
            upd_imputation = ''

        gender = gender_convert[gender]
        
               
        sample = {
            'sample'            : sample_name,
            'gender'            : gender,
            'resolution'        : resolution,
            'sd'                : sd,
            'z_sd'              : z_sd,
            'qc'                : qc,
            'cnvfile'           : os.path.join(basepath, cnvfile),
            'datafile'          : os.path.join(basepath, datafile),
            'projectname'       : projectname,
            'macon'             : macon,
            'n_reads'           : n_reads,
            'gendertest'        : gender_test,
            'upd_indication'    : upd_imputation
        }
        
        samples[sample_name] = sample

print("Content-Type: application/json\n")
print(json.dumps(samples))
