#!/usr/bin/env python

import argparse
import configparser
import os
import shutil
import sys
import time

from datetime import datetime
from glob import glob

from CMG.PBS import PbsJob
from CMG.DataBase import MySQL
from CMG.UZALogger import setup_logging, get_logger

def parse_config(file_path):
    config = configparser.ConfigParser()
    try:
        config.read(file_path)
    except:
        with open(file_path, 'r') as file:
            # Add a dummy section header to make the file compatible
            content = '[DEFAULT]\n' + file.read()
        config.read_string(content)
        return config['DEFAULT']


def get_vcf_path(sid, pid):
    vcfquery = f"SELECT upd_imput_data FROM projsamp WHERE idsamp='{sid}' and idproj='{pid}'"
    return dbh.runQuery(vcfquery)[0].get('upd_imput_data')


if __name__ == '__main__':
    # hardcoded, so docker version is tracked in cws code versioning
    docker = '391209680344.dkr.ecr.eu-central-1.amazonaws.com/swgs-upd-caller:1.0.0'

    """
    GET CLI INPUT
    """
    input = dict()
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sid", help="sample ID of index samples", default=None)
    parser.add_argument("-p", "--pid", help="Project ID of index sample", default=None)
    parser.add_argument("-u", "--scriptuser", help="User to run the docker with", default=None)
    parser.add_argument("-c", "--credentialsfile", help="File with DB credentials", default=None)
    args = parser.parse_args()

    sid = args.sid
    pid = args.pid
    scriptuser = args.scriptuser
    credentialsfile = args.credentialsfile


    """
    GET CONFIG INFO
    """
    config = parse_config(credentialsfile)
    email = config.get("log_email")

    now = datetime.now()
    timestamp_start = now.strftime("%y%m%d-%H%M%S")


    """
    LOGGING
    """
    setup_logging(
        name = "UPD_analysis_" + sid,
        level = "DEBUG",
        log_dir = "/home/" + scriptuser + "/CNV-WebStore/UPD_logs",
        to_addr = email,
        quiet=True
    )
    log = get_logger()
    log.info(f"Starting UPD analysis for sample {sid} and project {pid}")

    """
    GET PATHS / DB INFO
    """

    # db
    dbuser = config.get("dbuser")
    dbpass = config.get("dbpass")
    dbhost = config.get("dbhost")

    # paths
    outputdirpbs = config.get("cjo")
    log.debug(f"outputdirpbs = {outputdirpbs}")
    os.makedirs(outputdirpbs, exist_ok=True)

    submitscripts = config.get("submitscripts")
    log.debug(f"submitscripts = {submitscripts}")
    os.makedirs(submitscripts, exist_ok=True)

    resultfolder = config.get("datadir")
    resultfolder = resultfolder + '/UPD_results'
    os.makedirs(resultfolder, exist_ok=True)
    log.debug(f"resultfolder = {resultfolder}")

    plotdir = config.get('plotdir') + '/UPD'
    os.makedirs(plotdir, exist_ok=True)
    log.debug(f"plotdir = {plotdir}")


    """
    CONNECT TO DATABASE
    """
    log.info("Connecting to database")

    dbh = MySQL(dbuser, dbpass, dbhost, 'GenomicBuilds')
    build = dbh.runQuery("SELECT name, StringName FROM CurrentBuild")

    if build:
        ucscbuild = build[0]['name']
        newbuild = build[0]['StringName']
        db = "CNVanalysis" + ucscbuild
    else:
        sys.exit("No genomic build found")

    dbh.select_db(db)

    """
    GET INPUT FOR UPD ANALYSIS (VCF files trio)
    """
    log.debug("Get VCF input data")

    samplenamequery = f"SELECT chip_dnanr FROM sample WHERE id='{sid}'"
    indexname = dbh.runQuery(samplenamequery)[0].get('chip_dnanr')

    fidquery = f"SELECT father, father_project FROM parents_relations WHERE id='{sid}' LIMIT 1"
    fid_result = dbh.runQuery(fidquery)[0]  
    fid = fid_result.get('father')
    fpid = fid_result.get('father_project') 

    midquery = f"SELECT mother, mother_project FROM parents_relations WHERE id='{sid}' LIMIT 1"
    mid_result = dbh.runQuery(midquery)[0]
    mid = mid_result.get('mother')
    mpid = mid_result.get('mother_project') 

    vcf_index = get_vcf_path(sid, pid)
    vcf_father = get_vcf_path(fid, fpid)
    vcf_mother = get_vcf_path(mid, mpid)

    fileprefix = indexname + '_UPD_results_' + timestamp_start
    outputfileprefix = os.path.join(resultfolder, fileprefix)

    # create db entry for analysis
    upd_query = f"INSERT INTO `upd_analyses_swgs` (`iid`, `ipid`, `fid`, `fpid`, `mid`, `mpid`, `status`) VALUES ('{sid}','{pid}','{fid}','{fpid}','{mid}','{mpid}', 'RUNNING')"
    upd_analysis_id = dbh.insertQuery(upd_query)



    """
    CREATE CLUSTER JOB
    """
    log.info("Creating cluster job")

    job = PbsJob(
        name=indexname + '_UPD_analysis_' + timestamp_start, 
        cores=4, memory=8, 
        pbsqueue='cnvwebstore', 
        outputdir=outputdirpbs, 
        scriptdir=submitscripts, 
        email=email,
        runas=scriptuser)

    job.command(f"echo $PATH")

    # copy vcf file to tmp on compute node (to circumvent possible CIFS share connection issues during analysis)
    vcf_index_local = os.path.join('/tmp/', os.path.basename(vcf_index))
    vcf_father_local = os.path.join('/tmp/', os.path.basename(vcf_father))
    vcf_mother_local = os.path.join('/tmp/', os.path.basename(vcf_mother))

    job.command(f"rsync -a {vcf_index} {vcf_index_local}")
    job.command(f"rsync -a {vcf_father} {vcf_father_local}")
    job.command(f"rsync -a {vcf_mother} {vcf_mother_local}")

    # docker command
    docker_cmd = f"AWS_PROFILE=admin-001 docker run --rm  -v /tmp/:/tmp/ -v {resultfolder}:{resultfolder} -v /home/webstoredev/:/home/webstoredev/ {docker} python3 sWGS_UPD_Caller.py --child_vcf {vcf_index_local} --father_vcf {vcf_father_local} --mother_vcf {vcf_mother_local} --blacklist blacklisted_snps.txt --output {outputfileprefix}"
    job.command(docker_cmd)

    # cleanup vcf files
    job.command(f"rm {vcf_index_local}")
    job.command(f"rm {vcf_father_local}")
    job.command(f"rm {vcf_mother_local}")

    log.info("Run UPD analysis in docker via PBS job")
    job.execute()

    while not job.status():
        time.sleep(10)
        log.debug("Waiting for job to finish")
        continue

    log.info("UPD analysis finished, processing results...")

    now = datetime.now()
    timestamp_finished = now.strftime("%Y-%m-%d %H:%M:%S") # sql formatted

    # move PDF to CNVplots for access in web-ui
    src_pdf = f"{outputfileprefix}.pdf"
    dst_pdf = os.path.join(plotdir, os.path.basename(src_pdf))
    db_pdf = 'UPD/' + os.path.basename(src_pdf)
    datatable = f"{outputfileprefix}.table.txt"
    log.debug(src_pdf)
    log.debug(dst_pdf)
    log.debug(db_pdf)
    log.debug(datatable)
    shutil.copyfile(src_pdf, dst_pdf)

    log.info("Update Database")

    # save path in Db
    upd_query_finished = f"UPDATE `upd_analyses_swgs` SET analysis_finished = '{timestamp_finished}', pdf_path = '{db_pdf}', status = 'FINISHED'  WHERE id={upd_analysis_id}"
    dbh.doQuery(upd_query_finished)

    # import bin data table in Db
    with open(datatable, 'r') as f:
        for i,line in enumerate(f):
            if i == 0:
                continue
            chrom, start, stop, infosnps, bpi, upim, upip, call = line.split('\t')
            chrom = chrom.replace('chr', '')
            upd_data_query = f"INSERT INTO `upd_results_swgs` (`analysisid`, `chr`, `start`, `stop`, `informative_snps`, `bpi_frac`, `upim_frac`, `upip_frac`, `call`) VALUES ('{upd_analysis_id}', '{chrom}', '{start}', '{stop}', '{infosnps}', '{bpi}', '{upim}', '{upip}', '{call}')"
            log.debug(upd_data_query)
            upd_results_id = dbh.insertQuery(upd_data_query)

    log.debug("Cleaning up...")
    # clean up the results after importing table and PDF
    datafiles = glob(outputfileprefix + '*')
    for datafile in datafiles:
        try:
            os.remove(datafile)
        except OSError as e:
            log.error(f"Error deleting file {datafile}: {str(e)}")


    log.info("UPD analysis finished")



