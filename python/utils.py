import math
import pandas as pd

def getcnvdata(df:pd.DataFrame, chromosome:str, start:int, stop:int, maxstart:int=None, maxstop:int=None, resolution:str=None) -> str:
    # if CNV size <= 500kb: +- 250kb margin ; if CNV size > 500kb: +- half of CNV as margin (size of datapoint range is double that of the CNV)
    
    # chromosome structure : chr1/2/3/4/5/6/7/8/9/.../X/Y
    if not resolution:
        resolution = 'low'
    
    if resolution == 'high':
        default_margin = 250000
        rounding_number = 10000
    else:
        default_margin = 1000000
        rounding_number = 40000
    
    if maxstart and maxstop:
        if (maxstop - maxstart) > 2 * default_margin:
            margin = int(math.ceil((maxstop - maxstart) / (2 * float(rounding_number)) ) ) * rounding_number
        else:
            margin = default_margin    
        points_begin = maxstart - margin
        points_end = maxstop + margin     
    else:
        if (stop - start) > 2 * default_margin:
            margin = int(math.ceil((stop - start) / (2 * float(rounding_number)) ) ) * rounding_number
        else:
            margin = default_margin    
        points_begin = start - margin
        points_end = stop + margin     
    
    
    # get datapoints with margin
    datapoints = df.loc[ (df['chr'] == chromosome) & (df['start'] >= points_begin ) & ( df['end'] <= points_end ), ['start', 'end', 'logR', 'z']]
    # non-informative bins are double in the data files
    datapoints = datapoints.drop_duplicates()

    datapoints['center'] = datapoints[['start', 'end']].mean(axis=1) #['end'] - datapoints['start'] / 2
    datapoints = datapoints[['center', 'logR', 'z']]
    
    
    # get datapoints without margins (for the total # bins in the cnv incl non-informative bins)
    datapoints_cnv = df.loc[ (df['chr'] == chromosome) & (df['start'] >= start ) & ( df['end'] <= stop ), ['start']]
    datapoints_cnv = datapoints_cnv.drop_duplicates()
    total_bins = len(datapoints_cnv.index)
    
    # first join each row, then all columns
    datapoints = datapoints.astype(str)

    return '_'.join(datapoints.apply("_".join, axis=1)), total_bins

