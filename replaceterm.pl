#!/usr/bin/perl

@list = `ls -l | grep ^-`;

$needle = $ARGV[0];
$replacement = $ARGV[1];
$needle = quotemeta($needle);
$replacement = quotemeta($replacement);
foreach (@list) {
	chomp($_);
	$file = $_;
	$file =~ m/\d\d:\d\d (.*)$/;
	$file = $1;
	$output = `cat -n $file | grep '$needle' 2>/dev/null`;
	if ($output ne ""  ) {
		print "########################\n";
		print "# $file\n";
		print "########################\n";
		print $output;
		print "\tReplace? [Y]/n ";
		my $answer = <STDIN>;
		if ($anser =~ m/y|Y/ || $anser eq '') {
			`perl -pi -e 's/$needle/$replacement/g' $file`;
		}
	}
}
