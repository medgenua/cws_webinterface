<div class="sectie">
<h3>New FASEG Analysis</h3>
<h4>Read before you ask...</h4>
<p>
Needed things for a FASeg analysis:</p>
  <ul id="ul-mine">
    <li>Projectname, used for naming resultsfile</li>
    <li>Location of table containing the following columns:<br>
<pre>- Sample ID, following XXXX_DnaID consensus, where XXXX are the last four digits of the chip barcode
- Sample Gender
- Call Rate
- Index</pre>
    <li>Location of table containing the following columns:<br>
<pre>- SNP name (name)
- SNP chromosome (Chr)
- SNP position (Position)
- Per sample: LogR ratio (Log R Ratio)
</pre>
</ul>
</div>

<div class="sectie">
<h3>Analysis details:</h3>
<h4>Ready, Set...</h4>
<form enctype="multipart/form-data" action="index.php?page=checkdata&amp;type=faseg" method="post">
<table width="75%" id="mytable" cellspacing=0>
<tr>
  <th scope=col colspan=2>Data:</th>
</tr>
<tr>
  <th width="25%" scope=row class=spec>Projectname:</td>
  <td width="50%"><input type="text" name="TxtName" value="<?php echo date('Y-m-d') ."_".date('H\ui\ms\s');?>" size="40" maxlength="40" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Table of Genders:</td>
  <td class=alt><input type="file" name="genders" size="40" /></td>
</tr>
<tr>
  <th scope=row class=spec>Table of LogR and BAF:</td>
  <td><input type="file" name="data" size="40" /></td>
</tr>
<tr>
  <th scope=col colspan=2>Program Parameters:</td>
</tr>
<tr>
  <th scope=row class=spec>Smoothing SNP-range:</td>
  <td><input type=text name=span value=25 size=40></td>
</tr>
<tr>
  <th scope=row class=specalt>Significance:</td>
  <td class=alt><input type=text size=40 name=sig value="1e-03"></td>
</tr>
<tr>
  <th scope=row class=spec>Sensitivity (delta_LogR):</td>
  <td><input type=text size=40 name=delta value="0.2"></td>
</tr>
<tr>
  <th scope=row class=specalt>Min. &amp;#35;SNP's:</td>
  <td class=alt><input type="text" name="minsnp" size="40" value="5" /></td>
</tr>
<tr> 
  <th scope=col colspan="2" align="right"><span class="span-cmg"><input type="submit" value="...Go..." class=button></span></td>
</tr>
</table>
</div>
