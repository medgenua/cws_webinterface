<?php
$parameterfile = "files/parameters.txt";
$fh = fopen($parameterfile,"r");
if ($fh) {
  while (!feof($fh)) {
   $line = fgets($fh);
   $trimmed = rtrim($line);
   $pieces = explode("=",$trimmed);
   if (substr($pieces[0],0,1) == "Q") {
	   $parameters["$pieces[0]"] = "$pieces[1]";
   }
  }
}
?>
<div class="sectie">
<h3>New QuantiSNP analysis</h3>
<h4>Read before you ask...</h4>
<p>
Things needed for a QuantiSNP analysis:</p>
  <ul id="ul-mine">
    <li>Projectname, used for naming resultsfile</li>
    <li>Location of table with following BeadStudio columns in this exact order:<br>
<pre>- Sample Name (Sample ID), we recommend XXXX_DnaID where XXXX are the last 4 digits of the chip barcode.
- Sample Gender (Gender)
- Callrate (Call Rate)
- Index (Index)</pre>
    <li>Location of the table with following BeadStudio Columns in this exact order:<br>
<pre>- SNP name (name)
- SNP chromosome (Chr)
- SNP position (Position)
- Per sample: LogR ratio (Log R Ratio)
- Per sample: B-allele frequency (B Allele Freq)</pre>
    <li>Runtime parameters for QuantiSNP (<a href="files/userguide.pdf">Short explanation</a> / <a href="files/Qsnp_publication.pdf">Statistics</a>):
<pre>- Location of the config file (with HMM parameters)
- emiters (number of 'Expectation Maximisation' stps)
- Lsetting (smoothing factor)
- maxcopy (Maximale copy number value)
- doGCcorrect (Correction for local GC-values, ja/nee)
- gcdir (Directory with data for GC-correction)
- printRS (Needed for correct handling of intensity-only probes, ja/nee)</pre>
  </ul>
</div>

<div class="sectie">
<h3>Analysis details:</h3>
<h4>Ready, Set...</h4>
<form enctype="multipart/form-data" action="index.php?page=checkdata&amp;type=quantisnp" method="post">
<table width="75%" id="mytable" cellspacing=0>
<tr>
  <th scope=col colspan=2>Data:</th>
</tr>
<tr>
  <th width="25%" scope=row class=spec>Projectname:</td>
  <td width="50%"><input type="text" name="TxtName" value="<?php echo date('Y-m-d') ."_".date('H\ui\ms\s');?>" size="40" maxlength="40" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Table of Genders:</td>
  <td class=alt><input type="file" name="genders" size="40" /></td>
</tr>
<tr>
  <th scope=row class=spec>Table of LogR and BAF:</td>
  <td><input type="file" name="data" size="40" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Chip type used: </td>
  <td><select name=chiptype >
<?php
 $db = "CNVanalysis" . $_SESSION["dbname"];
 mysql_select_db("$db");
 $result = mysql_query("SELECT name FROM chiptypes ORDER BY name");
 while ($row=mysql_fetch_row($result)) {
    echo "<option value=\"".$row[0]."\">".$row[0]."</option>\n";
 }
?>
</select></td></tr>

<tr>
<th scope=col colspan=2>QuantiSNP Parameters:</td>
</tr>
<tr>
  <th class=spec scope=row>Configuration File:</td>
  <td><input type="text" name="config" size="40" value="<?php echo $parameters["Qconfig"]  ?>" /></td>
</tr>
<tr>
  <th class=specalt scope=row>Emiters:</td>
  <td class=alt><input type="text" name="emiters" size="40" value="<?php echo $parameters["Qemiters"] ?>" /></td>
</tr>
<tr>
  <th scope=row class=spec>Lsetting: </td>
  <td><input type="text" name="lsetting" size="40" value="<?php echo $parameters["Qlsetting"] ?>" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Maxcopy: </td>
  <td class=alt><input type="text" name="maxcopy" size="40" value="<?php echo $parameters["Qmaxcopy"] ?>" /></td>
</tr>
<tr>
  <th scope=row class=spec>Do GC Correction:</td>
  <td><input type="text" name="dogccorrect" size="40" value="<?php echo $parameters["Qdogccorrect"] ?>" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Location of GC-data:</td>
  <td class=alt><input type="text" name="gcdir" size="40" value="<?php echo $parameters["Qgcdir"] ?>" /></td>
</tr>
<tr>
  <th scope=row class=spec>PrintRS:</td>
  <td><input type="text" name="printrs" size="40" value="<?php echo $parameters["Qprintrs"] ?>" /></td>
</tr>
<tr>
  <th scope=col colspan=2>Parameters Resultsfilter:</td>
</tr>
<tr>
  <th scope=row class=spec>Min. &#35;SNP's:</td>
  <td><input type="text" name="minsnp" size="40" value="10" /></td>
</tr>
<tr>
  <th class=specalt scope=row>Min. Log Bayes Factor:</td>
  <td class=alt><input type="text" name="minlbf" size="40" value="10" /></td>
</tr> 
<tr> 
  <th scope=col colspan="2" align="right"><span class="span-cmg"><input type="submit" value="...Go..." class=button></span></td>
</tr>
</table>
