<?php
// select correct database
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

// GET configuration files
require_once 'xmlLib2.php';

///////////////////////
// methods for multi //
///////////////////////
$methods = array(0 => 'QuantiSNPv2', 1 => 'PennCNV', 2 => 'VanillaICE');
$versions = array();
$neededcols = array();
$keepcols = 0;
foreach ($methods as $key => $method) {
	// hack for penncnv
	if ($method == 'PennCNV') {
		$filemethod = 'PennCNVas';
	}
	else {
		$filemethod = $method;
	}
	// read config xml
	$configuration[$method] = my_xml2array("$scriptdir/Analysis_Methods/Configuration/$filemethod.xml");
	$configuration[$method]['version'] = get_value_by_path($configuration[$method],"configuration/general/version");
	$configuration[$method]['version'] = $configuration[$method]['version']['value'];
	$configuration[$method]['minconf'] = get_value_by_path($configuration[$method],"configuration/general/min_conf");
	$configuration[$method]['minconf'] = $configuration[$method]['minconf']['value'];
	$configuration[$method]['pars'] = '';
	// loop input parameters
	$subconf = get_value_by_path($configuration[$method],"configuration/input_values");
	foreach ($subconf as $subkey => $subval) {
		// skip irrelevant
		if ($subval['name'] == 'outdir' || $subval['name'] == 'logfile' || $subval['name'] == 'output' || $subval['name'] == 'prefix' || $subval['name'] == 'i' || $subval['name'] == 'inputfile' || $subval['name'] == 'input-files') {
			continue;
		}
		$configuration[$method]['pars'] = $configuration[$method]['pars'] . $subval['name']." =&gt; ". $subval['value']."<br/>";	
	}
	$configuration[$method]['pars'] =  $configuration[$method]['pars'] . "min_conf =&gt; ".$configuration[$method]['minconf']."<br/>";
	// get needed columns
	$general = get_value_by_path($configuration[$method],"configuration/general");
	foreach ($general as $subkey => $subval) {
		if (substr($subval['name'],0,5) == 'incol'){
			if (!array_key_exists($subval['value'], $neededcols)) {
				$neededcols[$subval['value']] = $keepcols;
				$keepcols++;
			}
		}
	}	
}

// get default column order and assign order to $neededcols.
$colxml = my_xml2array("$scriptdir/Analysis_Methods/Configuration/Global_ColumnOrder.xml");
$colxml = get_value_by_path($colxml,"ColOrder");
$colxml = array();
foreach ($colxml as $subkey => $subval) {
	$pos = substr($subval['name'],6);
	$colname = $subval['value'];
	if (array_key_exists($colname,$neededcols)) {
		$neededcols[$colname] = $pos;
	}
	else {
		// column not needed. skip it
	}
}
// sort needed columns by assigned order
asort($neededcols);

# print intro
$output =  "<div class='sectie'>\n";
$output .= "<h3>New Multi-Algorithm analysis</h3>";
$output .= "<h4>Standard pipeline</h4>";
$output .= "<p>This pipeline consists of a 2/3 majority-vote analysis using QuantiSNP2, PennCNV and VanillaICE. Since this is sort of a validated setup, only a few parameters can be customized. The used settings were tested and shown to maximize sensitivity and minimize the false discovery rate for all aberrations of 3 SNP's and more. The following settings are used:</p>";
echo $output;
flush();
# print param table
$output = "<p><table cellspacing=0>";
$output .= "<tr>";
foreach ($configuration as $method => $subarray) {
	$output .= " <th $firstcell>$method (".$configuration[$method]['version'] .")</th>";
}
$output .= "</tr>";
$output .= "<tr>";
foreach ($configuration as $method => $subarray) {
	$output .= " <td $firstcell>".$configuration[$method]['pars'] ."</td>";
}
$output .= "</tr>";
$output .= "</table>";
echo $output;
flush();
// print GENDER input file structure
echo '<p>';
echo "<table cellspacing=0>";
echo "<tr><th colspan=2 $firstcell>Input files</th></tr>";
echo "<tr><th $firstcell>'Gender' file structure:</th> <td>Must contain these columns, as named in GenomeStudio (tab-seperated), including headerline: </td></tr>";
echo "<tr><td $firstcell>Sample ID (Sample ID)</td><td>following XXXX_DnaID where XXXX are the last four number of the chip barcode</td></tr>\n";
echo "<tr><td $firstcell>Sample Gender (Gender)</td><td>Male/Female/Unknown</td></tr>\n";
echo "<tr><td $firstcell>Callrate (Call Rate)</td><td>numeric, 0-1</td></tr>\n";
echo "<tr><td $firstcell>Index (Index)</td><td>Index of the sample in GenomeStudio Project</td></tr>\n";
echo "<tr><td $firstcell>Array Info.Sentrix ID (optional)</td><td>Array Slide Barcode</td></tr>\n";
echo "<tr><td $firstcell>Array Info.Sentrix Position (optional)</td><td>Position On The Array Slide</td></tr>\n";


// print DATA input file structure
$replace = array('%probename%' => 'Name', '%chromosome%' => 'Chr', '%position%' => 'Position', '%samplelogr%' => 'SampleName.Log R Ratio', '%samplebaf%' => 'SampleName.B Allele Freq', '%samplegt%' => 'SampleName.GType');
$explain = array('%probename%' => 'SNP name (rs254783)', '%chromosome%' => 'SNP Chromosome', '%position%' => 'SNP Position (1-based)', '%samplelogr%' => 'Per Sample, LogR Ratio (numeric)', '%samplebaf%' => 'Per Sample, B Allele Frequency (numeric, 0-1)', '%samplegt%' => 'Per Sample, Genotype (AA/AB/BB)');
echo "<tr><th $firstcell>'Data' file structure: </th><td> Must contain these columns in this order, as named in GenomeStudio (tab-seperated), including headerline:</td></tr>";
foreach ($neededcols as $colname => $idx) {
	echo "<tr><td $firstcell>$replace[$colname]</td><td>$explain[$colname]</td></tr>";
}
echo "<tr><th $firstcell>'Pedigree' file structure: </th><td>This is optional. Must contain these columns (tab-seperated), including headerline. Each line represents a family:</td></tr>";
echo "<tr><td $firstcell>Offspring Sample ID</td><td>specified as in Gender file</td></tr>";
echo "<tr><td $firstcell>Parent 1 Sample ID</td><td>father/mother specification is optional</td></tr>";
echo "<tr><td $firstcell>Parent 2 Sample ID</td><td>father/mother specification is optional</td></tr>";
echo "</table></p>";
echo "</div>";

///////////////////////////////////////////////////////
// LOAD THE FILE NAMES OF FILES IN THE FTP DIRECTORY //
///////////////////////////////////////////////////////
$ftpfiles = "";
// set ftp upload dir. /home/SCRIPTUSER/ftp-data/USERNAME/
$dir = "/home/".$config['SCRIPTUSER']."/ftp-data/$username"; //$sitedir/uploads/";
$Files = array();
$It =  opendir($dir);
if ($It) {
	while ($Filename = readdir($It)) {
 		if ($Filename == '.' || $Filename == '..' )
  			continue;
 		//$LastModified = filemtime($dir . $Filename);
 		$Files[] = $Filename;
	}
	sort($Files);
	$num = count($Files) - 1 ;
	For($i=0;$i<=$num;$i += 1) {
		//$filename = substr($Files[$i], (strlen($userid)+1));
		$filename = $Files[$i];
		$ftpfiles .= "<option value=\"". $Files[$i]."\">$filename</option>\n";
	}
}

////////////////////
// PRINT THE FORM //
////////////////////
?>
<div class="sectie">
<h3>Analysis details:</h3>
<h4>Ready, Set...</h4>
<form enctype="multipart/form-data" action="index.php?page=checkdata&type=multiple" method="post">
<table  cellspacing=0>
<tr>
  <th scope=col colspan=3 style="border-left: 1px solid #a1a6a4;">Data:</th>
</tr>

<tr>
  <th width="50%" scope=row class=spec colspan=2>Projectname:</td>
  <td width="50%"><input type="text" name="TxtName" value="<?php echo date('Y-m-d') ."_".date('H\ui\ms\s');?>" size="40" maxlength="40" /></td>
</tr>

<tr>
  <th width="25%" scope=row class=specalt colspan=2>Collection:</td>
<?php 
#if ($level > 1) {
	$query = mysql_query("SELECT id, name FROM collections ORDER BY name");
	echo "<td width='50%' class=alt><select id=colselect name=group onchange='showspan()'>";
	while ($row = mysql_fetch_array($query)) {
		$cid = $row['id'];
		$cname = $row['name'];
		echo "<option value='$cname'>$cname</option>";
	}
	echo "<option value='new'>New Collection</option></select>";
	echo "<span id='newcolspan' style='float:right;display:none;'>=> &nbsp New Collection: <input type=text name=newcolname size=10 >&nbsp;Descr.<input type=text name=newcolcomment size=10></span></td>";
#}
#else {
#	echo "<td width='50%' class=alt><select name=group><option value=guest>Guest</option></td>\n";
#}
?>
</tr>
<!-- gender file -->
<tr>
  <th scope=row class=spec rowspan=2>Table of Genders:</td>
  <th scope=row class=spec >Specify local file:</td>
  <td ><input type="file" name="genders" size="40" /></td>
</tr>
<tr>
  <th scope=row class=spec>Select uploaded file:</td>
  <td ><select name=uploadedgender><option value=''></option><?php echo $ftpfiles ?></select></td>
</tr>
<!-- data file -->
<tr>
  <th scope=row class=specalt rowspan=2>Table of LogR and BAF:</td>
  <th scope=row class=specalt >Specify local file:</td>
  <td class=alt><input type="file" name="data" size="40" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Select uploaded file:</td>
  <td class=alt><select name=uploadeddata><option value=''></option><?php echo $ftpfiles ?></select>
  <a href=index.php?page=upload>(Upload >500Mb datafiles here)</a>
  </td>
</tr>
<!-- pedigree file -->
<tr>
<th scope=row class=specalt rowspan=2>Pedigree information:</th>
<th scope=row class=specalt >Specify local file:</td>
<td class=alt><input type='file' name='pedigree' size='40' /> (<a href='files/pedigree.example.txt' target='_blank'>Example file</a>)</td>
</tr>
<tr>
  <th scope=row class=specalt>Select uploaded file:</td>
  <td class=alt><select name=uploadedpedigree><option value=''></option><?php echo $ftpfiles ?></select>
  </td>
</tr>
<!-- Additional datafiles -->
<tr>
<th scope=row class=specalt rowspan=2>Supplementary data files:</th>
<th scope=row class=specalt >Specify local files:</td>
<?php
$idx = 0;
echo "<td>";
// file input field
echo ($idx + 1) .") <input type=file name='datafiles[]' size=40 />";
// span to contain the next field and link to add a field
echo "<span id='items$idx'></span><i>(<a href='javascript:void(0)' onclick='additem();'>Extra File</a>)</i><br/>";
// update field index in javascript.
echo "<script type='text/javascript'>fieldidx=$idx;</script>";
echo "</td>";
echo "</tr>";
?>
<tr>
  <th scope=row class=specalt>Select uploaded files:</td>
  <td class=alt><select name='uploadeddatafiles[]' multiple='multiple'><?php echo $ftpfiles ?></select>
  </td>
</tr>



<!-- chip information -->
<tr>
  <th scope=row class=spec colspan=2>Chip type used: </td>
  <td><select name=chiptype >
<?php
  $result = mysql_query("SELECT name,ID FROM chiptypes ORDER BY name");
 while ($row=mysql_fetch_row($result)) {
	#if ($row[1] == 12) {
	#	continue;
	#}
    echo "<option value=\"".$row[0]."\">".$row[0]."</option>\n";
 }
?>
</select></td></tr>

<?php
// FROM HERE : SETTINGS
echo "<tr>";
echo " <th scope=col  style='border-left: 1px solid #a1a6a4;' colspan=3>Asymmetric Filter</td>";
echo "</tr>";
echo "<tr>";
echo " <td style='border-left: 1px solid #a1a6a4;' class=alt colspan=2>Activate Asymmetric Filtering : <input type=checkbox name=asym> </td ><td>Consider high confidence calls</td>";
echo "</tr>";
echo "<tr>";
echo " <td style='border-left: 1px solid #a1a6a4;' class=alt colspan=2>Minimal Confidence: <input type=text size=3 value=20 name='asymminconf'></td><td >Specify the minimal confidence in asymmetric filtering</td>";
echo "</tr>";
echo "<tr>";
echo " <td style='border-left: 1px solid #a1a6a4;' class=alt colspan=2>Asymmetric Method: PennCNV <input type=hidden name='asymmethod' value='PennCNVas'></td><td >PennCNV calls are used in asymmetric filtering (not changable)</td>";
echo "</tr>";
echo "<tr>";
echo " <td style='border-left: 1px solid #a1a6a4;' class=alt colspan=2>Allow for CNV types: Duplications<input type=hidden name='asymtype' value='dup'></td><td >Asymmetric filtering is only used for duplication calls (not changable). </td>";
echo "</tr>";
echo "<tr>";
echo " <th scope=row colspan=3 style='border-left: 1px solid #a1a6a4;' >Detection of Homozygous Stretches</td>";
echo "</tr>";
echo "<tr>";
echo " <td class=alt colspan=2 style='border-left: 1px solid #a1a6a4;'>Activate : <input type=checkbox name=plink></td><td>Identify homozygous stretches using plink</td>";
echo "</tr>";
echo "<tr>";
echo " <th scope=row colspan=3 style='border-left: 1px solid #a1a6a4;' >Y-Chromosome Analysis</td>";
echo "</tr>";
echo "<tr>";
echo " <td class=alt colspan=2 style='border-left: 1px solid #a1a6a4;'>Activate : <input type=checkbox name=doY></td><td>Include Y-chromosome. For now, this is done by VanillaICE only. Make sure the intensities are normalised on male-only samples !</td>";
echo "</tr>";

echo "<tr> ";
echo "  <th scope=col colspan='3' align='right' class=spec><span class='span-cmg'><input type='submit' value='...Go...' class=button></span></td>";
echo "</tr>";
echo "</table>";
echo "<input type=hidden name=submitter value='$username'>";
echo "<input type=hidden name=minsnp value=3>";
$colstring = '';
foreach ($neededcols as $colname => $idx) {
	$colstring .= "$colname|";
}
$colstring = substr($colstring,0,-1);
echo "<input type=hidden name='neededcols' value='$colstring'>";

echo "</form>";
echo "<script type='text/javascript' src='javascripts/runsetup.js'></script>";
?>
