<?php 
//just for syntax
?>
<div class="sectie">
<h3>New VanillaICE Analysis</h3>
<h4>Read before you ask...</h4>
<p>
Needed things for a PennCNV analysis:</p>
  <ul id="ul-mine">
    <li>Projectname, used for naming resultsfile</li>
    <li>Location of table containing the following columns:<br>
<pre>- Sample ID, following XXXX_DnaID consensus, where XXXX are the last four digits of the chip barcode
- Sample Gender
- Call Rate
- Index</pre>
    <li>Location of table containing the following columns:<br>
<pre>- SNP name (name)
- SNP chromosome (Chr)
- SNP position (Position)
- Per sample: LogR ratio (Log R Ratio)
- Per sample: Genotype calls (GType)</pre>
</pre>
</ul>
</div>

<div class="sectie">
<h3>Analysis details:</h3>
<h4>Ready, Set...</h4>
<form enctype="multipart/form-data" action="index.php?page=checkdata&amp;type=vanillaice" method="post">
<table width="75%" id="mytable" cellspacing=0>
<tr>
  <th scope=col colspan=2>Data:</th>
</tr>
<tr>
  <th width="25%" scope=row class=spec>Projectname:</td>
  <td width="50%"><input type="text" name="TxtName" value="<?php echo date('Y-m-d') ."_".date('H\ui\ms\s');?>" size="40" maxlength="40" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Table of Genders:</td>
  <td class=alt><input type="file" name="genders" size="40" /></td>
</tr>
<tr>
  <th scope=row class=spec>Table of LogR and BAF:</td>
  <td><input type="file" name="data" size="40" /></td>
</tr>
<tr>
  <th scope=row class=specalt>Chip type used: </td>
  <td><select name=chiptype style="width:232px">
<?php
 $db = "CNVanalysis" . $_SESSION["dbname"];
 mysql_select_db("$db");
 $result = mysql_query("SELECT name FROM chiptypes ORDER BY name");
 while ($row=mysql_fetch_row($result)) {
    echo "<option value=\"".$row[0]."\">".$row[0]."</option>\n";
 }
?>
</select></td></tr>

<tr>
  <th scope=col colspan=2>Algorithm Parameters :</td>
</tr>
<tr>
  <th scope=row class=spec>Min. &#35;SNP's:</td>
  <td><input type="text" name="minsnp" size="40" value="10" /></td>
</tr>
<tr>
  <th class=specalt scope=row>Smoothing factor:</td>
  <td class=alt><input type="text" name="taufactor" size="40" value="1e06" /></td>
</tr> 
<tr>
  <th scope=row class=spec>HMM states type:</td>
  <td><select name="hmm" style="width:232px">
	<option label="regular" value="regular">Theoretical</option>
	<option label="experimental" value="experimental" selected>Experimental</option>
	</select>
  </td>
</tr>
<tr>
  <th scope=row class=specalt>Probe Variance Model: </td>
  <td><select name="variance" style="width:232px">
	<option label="insample" value="insample" selected>Per sample robust estimate</option>
	<!--
	<option label="robustref" value="robustref" >RefSet robust estimate per probe</option>
	<option label="stdevref" value="stdevref" >RefSet Standard Deviation per probe</option>
	-->
      </select>
 </td>
</tr>
<tr> 
  <th scope=col colspan="2" align="right"><span class="span-cmg"><input type="submit" value="...Go..." class=button></span></td>
</tr>
</table>
</div>
