<?php
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

###################
# GET FORM PARAMS #
###################
$chr = $_GET['c'];
$start = $_GET['start'];
$stop = $_GET['stop'];
$chrtxt = $chromhash[ $chr ];
$size = $stop - $start +1;
$sizeformat = number_format($size,0,'',',');
# GET CONTROL DATA FROM DATABASE


echo "<div class=nadruk>UCSC Segmental Duplications</div>";
echo "<p><ul id=ul-simple>";
echo "<li> - Region: chr$chr:" .number_format($start,0,'',',') . "-" . number_format($stop,0,'',',') ."<li>";
echo "</ul></p>";
echo "<p>";
echo "<img src='segdupplot.php?c=$chr&amp;start=$start&amp;stop=$stop' border=0>\n";
echo "</p>";



?>
