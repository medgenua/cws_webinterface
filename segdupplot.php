<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/jpeg");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$chromhash["X"] = "X";
$chromhash["Y"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$window = $stop-$start+1;

# GET included studies
# DEFINE IMAGE PROPERTIES
$querystring = "SELECT start, stop, strand, otherchr, otherstart, otherstop, fraction FROM segdup WHERE chr = '$chr' $locquery ORDER BY start, stop";
$result = mysql_query($querystring);
$nrlines = mysql_num_rows($result);
$height = $nrlines*9 + 20;
$scalef = (400-180)/($window);
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];

//Specify constant values
$width = 400; //Image width in pixels
//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$orange = ImageColorAllocate($image,255, 179,0);

$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
#Fill background
imagefill($image,0,0,$white);
$querystring = "SELECT start, stop, strand, otherchr, otherstart, otherstop, fraction FROM segdup WHERE chr = '$chr' $locquery ORDER BY start, stop";
$result = mysql_query($querystring);

$y = 10;
$cy = $y;
$xoff = 180;
$cstudid = '';

while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$cstrand = $row['strand'];
	$ochr = $row['otherchr'];
	$ostart = $row['otherstart'];
	$ostop = $row['otherstop'];
	$fraction = $row['fraction'];
	if ($fraction < 0.98) {
		$color = $gpos25;
	}
	elseif ($fraction < 0.99) {
		$color = $gpos50;
	}
	else {
		$color = $gpos75;
	}
	$otherregion = "Chr". $chromhash[$ochr] .":".number_format($ostart,0,'',','). '-'.number_format($ostop,0,'',',');
	$fraction = number_format($fraction,3,'.','');
	$y = $y + 8;
	
	imagestring($image,1,2,$y-2,$otherregion,$black);
	imagestring($image,1,150,$y-2,$fraction,$black);	
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	if ($scaledstop - $scaledstart < 1) {
		$scaledstop++;
	}
	imagefilledrectangle($image,$xoff+$scaledstart,$y+1,$xoff+$scaledstop,$y+3,$color);
	
}

# Draw the table borders
imagestring($image,2,10,1,"Other Region",$gpos75);
imagestring($image,2,147,1,"Match",$gpos75);
imagefilledrectangle($image,0,0,0,$y+10,$gpos50);
imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
imagefilledrectangle($image,$width-1,0,$width-1,$y+10,$gpos50);
imagefilledrectangle($image,145,0,145,$y+10,$gpos50);
imagefilledrectangle($image,180,0,180,$y+10,$gpos50);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);		

#draw position indications: 
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,180,7,184,4,$gpos75);
imageline($image,180,7,184,10,$gpos75);
imageline($image,180,7,191,7,$gpos75);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
imagestring($image,2,194,1,$formatstart,$gpos75);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
imageline($image, 400,7,390,7,$gpos75);
imageline($image, 400,7,396,4,$gpos75);
imageline($image, 400,7,396,10,$gpos75);
imagestring($image,2,388-$txtwidth,2,$formatstop,$gpos75);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

