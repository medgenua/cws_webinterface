<?php 
  
// +------------------------------------------------------------------------+ 
// | PHP version 5.0                                                           | 
// +------------------------------------------------------------------------+ 
// | Description:                                                              | 
// | Class to populate tree view using AJAX + PHP                               | 
// |                                                                         | 
// +------------------------------------------------------------------------+ 
// | Author                : Neeraj Thakur <neeraj_th@yahoo.com>               | 
// | Created Date         : 19-11-2007                                          | 
// | Last Modified        : 19-11-2007                                          | 
// | Last Modified By     : Neeraj Thakur                                      | 
// +------------------------------------------------------------------------+ 

# LOAD JSON WRAPPER
require('jsonwrapper/jsonwrapper.php');

# load database info
include('.LoadCredentials.php');

DEFINE ('DB_USER', $user); 
DEFINE ('DB_PASSWORD', $pw); 
DEFINE ('DB_HOST', $host); 
DEFINE ('DB_NAME', 'LNDB'); 
    
    if ( @$_REQUEST['method'] == 'getCat' ) 
    {
	 
        $objServices = new AjaxTreeview();
        $catid = isset($_REQUEST['catid'])?$_REQUEST['catid']:0;
        $arr = $objServices->getCatList($catid); 
        $arrReturn['data'] = $arr; 
        $arrReturn['id'] = @$_REQUEST['id']; 
        $arrReturn['value'] = $catid; 
        $jsonstring = json_encode($arrReturn); 
        echo $jsonstring;
    } 
    
    class AjaxTreeview 
    { 
        public function AjaxTreeview() 
        { 
            // Make the connnection and then select the database. 
            $dbc = @mysql_connect (DB_HOST, DB_USER, DB_PASSWORD) OR die ('Could not connect to MySQL: ' . mysql_error() ); 
            mysql_select_db (DB_NAME) OR die ('Could not select the database: ' . mysql_error() ); 
            $this->table = "FEATURE"; 
        } 
        
        function dbConnect() 
        { 
            DEFINE ('LINK', mysql_connect (DB_HOST, DB_USER, DB_PASSWORD)); 
        } 
        
        public function getCatList($catid) 
        { 
            $this->dbConnect();
	    $originalcatid = $catid;
	    $arr = array();
	    $runquery = 0;
	    if ($catid == '0') {
		$catid = '__.00.00';
		$runquery = 1;
	    }
	    elseif (preg_match('/\.00\./',$catid)) {
		$catid = str_replace('.00.','.__.',$catid);
		$runquery = 1;
	    }
	    elseif (preg_match('/.00$/',$catid)) {
		$catid = str_replace('00','__',$catid);
		$runquery = 1;
	    }
	    if ($runquery == 1) {
	   	$query = "SELECT CODE,LABEL FROM {$this->table} where CODE LIKE '{$catid}' AND NOT CODE = '$originalcatid' ORDER BY LABEL asc"; 
            	$result = mysql_db_query (DB_NAME, $query, LINK);            
            	 
            	$i = 0;            
            	while( $rec = mysql_fetch_array($result) ) 
            	{
                	#$arr[$i]['id'] = $rec['id']; 
                	$arr[$i]['code'] = $rec['CODE']; 
                	$arr[$i]['label'] = $rec['LABEL']; 
                	$i ++ ; 
            	}
	    }
	    
	 
        return $arr; 
        } 
    } 
?>
