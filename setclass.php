<?php
# POSTED VARS
$aid = $_GET['aid'];
$class = $_GET['class'];
$uid = $_GET['u'];
$from = $_GET['from'];
if (isset($_GET['loh'])) {
	$loh = $_GET['loh'];
}
else {
	$loh = 0;
}

if (isset($_GET['mos'])) {
	$mos = $_GET['mos'];
}
else {
	$mos = 0;
}

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
if ($loh == 1) {
	$atable = 'aberration_LOH';
	$ltable = 'log_loh';
	$fields = '';
}
elseif ($mos == 1) {
	$atable = 'aberration_mosaic';
	$ltable = 'log_mos';
	$fields = ',cn';
}
else {
	$atable = 'aberration';
	$ltable = 'log';
	$fields = ',cn';
}

$query = mysql_query("SELECT sample, idproj, class, chr, start, stop $fields FROM $atable WHERE id = $aid ");
$row = mysql_fetch_array($query);
$origclass = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$start = $row['start'];
$stop = $row['stop'];
if ($loh != 1) {
	$cn = $row['cn'];
}
$close = 1;

$dc = array();
$name_dc = array();
$dc_query = mysql_query("SELECT id, name, abbreviated_name, sort_order AS class_sorted FROM diagnostic_classes ORDER BY sort_order ASC");
while ($row = mysql_fetch_array($dc_query)) {
	$dc[$row['id']] = $row['name'];
	$name_dc[$row['abbreviated_name']] = $row['id'];
}

if ($class == $origclass && $class != 'null') {
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	
	if ($setbyuid != $uid) {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Updating 'Set By User'</h3>";
		echo "<p>The diagnostic class of the following CNV was previously set by a different user, if you are validating this region, pleas use 'Validated' as the reason below:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		if ($loh != 1) {
			echo "<li>CopyNumber: $cn</li>";
		}
		echo "<li>Previous Class: $dc[$origclass]</li>";
		echo "<li>New Class: $dc[$class]</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Confirmed As Class $dc[$class]";
		echo "<p><form action=changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "<input type=hidden name=loh value='$loh'>\n";
		echo "<input type=hidden name=mos value='$mos'>\n";
		echo "<input type=hidden name=setbyid value='$setbyuid'>\n";
		echo "Reason: <br/><input type=text value='Validated' name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Update'>\n";
		echo" </form>\n";
	}
	else {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Confirming Diagnostic Class</h3>";
		echo "<p>The diagnostic class of the following CNV was previously set by you, if you are validating this region, pleas use 'Validated' as the reason below:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		if ($loh != 1) {
			echo "<li>CopyNumber: $cn</li>";
		}
		echo "<li>Previous Class: $dc[$origclass]</li>";
		echo "<li>New Class: $dc[$class]</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Confirmed As Class $dc[$class]";
		echo "<p><form action=changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "<input type=hidden name=setbyid value='$setbyuid'>\n";
		echo "<input type=hidden name=loh value='$loh'>\n";
		echo "<input type=hidden name=mos value='$mos'>\n";
		echo "Reason: <br/><input type=text value='Validated' name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Update'>\n";
		echo" </form>\n";
	}
}

if ($class != $origclass && $class != 'null' ) {
	if ($origclass == '' && $class != $name_dc['FP']) {
		// new classification, no reason needed
		$logentry = "Diagnostic Class Set to $dc[$class]";
		mysql_query("INSERT INTO $ltable (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry')");
	}
	elseif ($origclass == '' && $class == $name_dc['FP']) {
		// SETTING FALSE POSITIVE => NEEDS REASON
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Setting False Positive Region Demands Explanation!</h3>";
		echo "<p>Please argument your decisision to consider this region a false positive:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>New Class: False Positive</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Set to $dc[$class]";
		echo "<p><form action=changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "<input type=hidden name=setbyid value='$uid'>\n";
		echo "<input type=hidden name=loh value='$loh'>\n";
		echo "<input type=hidden name=mos value='$mos'>\n";
		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Class'>\n";
		echo" </form>\n";
	}

	else {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		$logq = mysql_query("SELECT uid, entry FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$setbyid = $logrow['uid'];

				$usrow = mysql_fetch_array($usq);
				$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
				break;
			}
		}
		echo "<h3>Changing Diagnostic class needs Explanation!</h3>";
		echo "<p>Please argument your decisision to change the diagnostic class of the following CNV:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		if ($loh != 1) {
			echo "<li>CopyNumber: $cn</li>";
		}
		echo "<li>Previous Class: $dc[$origclass]</li>";
		echo "<li>New Class: $dc[$class]</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Changed From $dc[$origclass] to $dc[$class]";
		echo "<p><form action=changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "<input type=hidden name=setbyid value='$setbyid'>\n";
		echo "<input type=hidden name=loh value='$loh'>\n";
		echo "<input type=hidden name=mos value='$mos'>\n";
		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Class'>\n";
		echo" </form>\n";
	}
}
// probably deprecated (null is always filled in from now on, but keep just in case)
elseif ($origclass > 0 && $class == 'null') {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		$logq = mysql_query("SELECT uid, entry FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$setbyid = $logrow['uid'];
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.$usrow['LastName'];
				break;
			}
		}
		echo "<h3>Changing Diagnostic class needs Explanation!</h3>";
		echo "<p>Please argument your decisision to change the diagnostic class of the following CNV:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $region</li>";
		echo "<li>Previous Class: $dc[$origclass] </li>";
		echo "<li>New Class: $dc[0]</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Changed From $dc[$origclass] to $dc[0]";
		echo "<p><form action=changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "<input type=hidden name=setbyid value='$setbyid'>\n";
		echo "<input type=hidden name=loh value='$loh'>\n";
		echo "<input type=hidden name=mos value='$mos'>\n";
		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Class'>\n";
		echo" </form>\n";

	
	
	//mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
}

if ($close == 1) {
	$query = "UPDATE $atable SET class='$class' WHERE id = '$aid'";
	mysql_query($query);
	//$query = "UPDATE scantable SET class='$class' WHERE aid = '$aid'";
	//mysql_query($query);
	// redirect has to be split up, reload does not work for POST under chrome...
	echo "<script type='text/javascript'>";
	echo "var parenturl = window.opener.location.href;\n";
	echo "if (parenturl.indexOf('details') > -1  ){\n";
	#echo "   document.cookie='scrolltop='+document.body.scrollTop;\n";
	echo "	 window.opener.location = parenturl + '&sample=$sid&project=$pid';\n";
	#echo "   document.body.scrollTop = scrolltop;\n";
	echo "}\n";
	echo "else {\n";
	echo "	 window.opener.location.reload();\n";
	echo "}\n";
	echo "</script>";
	echo "<script type='text/javascript'>window.close();</script>";
}
?>
