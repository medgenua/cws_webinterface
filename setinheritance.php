<?php
# POSTED VARS
$aid = $_GET['aid'];
$inh = $_GET['inh'];
$uid = $_GET['u'];
$from = $_GET['from'];

# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
$inhtypes = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$query = mysql_query("SELECT sample, idproj, class, inheritance, chr, start, stop, cn FROM aberration WHERE id = $aid ");
$row = mysql_fetch_array($query);
$origclass = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$start = $row['start'];
$stop = $row['stop'];
$cn = $row['cn'];
$originh = $row['inheritance'];
$close = 1;

if ($inh == $originh && $inh != 0) {
	
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Inheritance/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	
	if ($setbyuid != $uid) {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Updating 'Set By User'</h3>";
		echo "<p>The inheritance of the following CNV was previously set by a diffent user, if you are validating this, pleas use 'Validated' as the reason below:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>Previous inheritance: ".$inhtypes[$inhtypes[$originh]]."</li>";
		echo "<li>New inheritance: ".$inhtypes[$inhtypes[$inh]]."</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Inheritance Confirmed As ".$inhtypes[$inhtypes[$inh]];
		echo "<p><form action=changeinheritance.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=inh value='$inh'>\n";
		echo "<input type=hidden name=setbyid value='$setbyuid'>\n";
		echo "Reason: <br/><input type=text value='Validated' name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Update'>\n";
		echo" </form>\n";
	}
	else {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		echo "<h3>Confirming Inheritance</h3>";
		echo "<p>The inheritance of the following CNV was previously set by you, if you are validating this, pleas use 'Validated' as the reason below:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>Previous inheritance: ".$inhtypes[$inhtypes[$originh]]."</li>";
		echo "<li>New inheritance: ".$inhtypes[$inhtypes[$inh]]."</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Inheritance Confirmed As ".$inhtypes[$inhtypes[$inh]];
		echo "<p><form action=changeinheritance.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=inh value='$inh'>\n";
		echo "<input type=hidden name=setbyid value='$setbyuid'>\n";
		echo "Reason: <br/><input type=text value='Validated' name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Update'>\n";
		echo" </form>\n";
	}
}

if ($inh != $originh ) { #&& $inh != 0 ) {
	if ($originh == 0) {
		// new inheritance, no reason needed
		$logentry = "Inheritance Changed From ".$inhtypes[$inhtypes[$originh]]." to ".$inhtypes[$inhtypes[$inh]];
		mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$uid', '$logentry')");
	}
	else {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		$logq = mysql_query("SELECT uid, entry FROM log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Inheritance/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$setbyid = $logrow['uid'];
				$usrow = mysql_fetch_array($usq);
				$setby = $usrow['FirstName'] .' '.$usrow['LastName'];
				break;
			}
		}
		echo "<h3>Changing inheritance needs explanation!</h3>";
		echo "<p>Please argument your decisision to change the inheritance of the following CNV:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $cn</li>";
		echo "<li>Previous inheritance: ".$inhtypes[$inhtypes[$originh]]."</li>";
		echo "<li>New inheritance: ".$inhtypes[$inhtypes[$inh]]."</li>";
		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Inheritance Changed From ".$inhtypes[$inhtypes[$originh]]." to ".$inhtypes[$inhtypes[$inh]];
		echo "<p><form action=changeinheritance.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=inh value='$inh'>\n";
		echo "<input type=hidden name=setbyid value='$setbyid'>\n";
		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Inheritance'>\n";
		echo" </form>\n";
	}
}
/*
elseif ($origclass > 0 && $class == 'null') {
		$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
		$logq = mysql_query("SELECT uid, entry FROM log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$setbyid = $logrow['uid'];
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.$usrow['LastName'];
				break;
			}
		}
		echo "<h3>Changing Diagnostic class needs Explanation!</h3>";
		echo "<p>Please argument your decisision to change the diagnostic class of the following CNV:</p>";
		echo "<ul>\n";
		echo "<li>Region: $region</li>";
		echo "<li>CopyNumber: $region</li>";
		if ($origclass == 5) {
			echo "<li>Previous Class: False Positive</li>";
		}
		else {
			echo "<li>Previous Class: $origclass</li>";
		}	
		if ($class == 5) {
			echo "<li>New Class: False Positive</li>";
		}
		else {
			echo "<li>New Class: Undefined</li>";
		}

		echo "<li>Previously set by: $setby</li>";
		echo "</ul></p>";
		$close = 0;
		$logentry = "Diagnostic Class Changed From $origclass to Undefined";
		echo "<p><form action=changeclass.php method=POST>";
		echo "<input type=hidden name=logentry value='$logentry'>\n";
		echo "<input type=hidden name=sid value='$sid'>\n";
		echo "<input type=hidden name=pid value='$pid'>\n";
		echo "<input type=hidden name=aid value='$aid'>\n";
		echo "<input type=hidden name=uid value='$uid'>\n";
		echo "<input type=hidden name=class value='$class'>\n";
		echo "<input type=hidden name=setbyid value='$setbyid'>\n";
		echo "Reason: <br/><input type=text name=arguments maxsize=255 size=35></p>";
		echo "<input type=submit name=submit value='Change Class'>\n";
		echo" </form>\n";

	
	
	//mysql_query("INSERT INTO log (sid, pid, aid, uid, entry) VALUES ('$sid', '$pid', '$aid','$userid', '$logentry')");
}
*/
if ($close == 1) {
	$query = "UPDATE aberration SET inheritance = '$inh' WHERE id = '$aid'";
	mysql_query($query);
	$query = "UPDATE scantable SET inheritance ='$inh' WHERE aid = '$aid'";
	mysql_query($query);
	// redirect has to be split up, reload does not work for POST under chrome...
	echo "<script type='text/javascript'>";
	echo "var parenturl = window.opener.location.href;\n";
	echo "if (parenturl.indexOf('details') > -1  ){\n";
	#echo "   document.cookie='scrolltop='+document.body.scrollTop;\n";
	echo "	 window.opener.location = parenturl + '&sample=$sid&project=$pid';\n";
	#echo "   document.body.scrollTop = scrolltop;\n";
	echo "}\n";
	echo "else {\n";
	echo "	 window.opener.location.reload();\n";
	echo "}\n";
	echo "</script>";
	echo "<script type='text/javascript'>window.close();</script>";
}
?>
