<?php

# POSTED VARS
if (isset($_GET['loh'])) {
	$loh = $_GET['loh'];
}
else {
	$loh = 0;
}
if (isset($_GET['mos'])) {
	$mos = $_GET['mos'];
}
else {
	$mos = 0;
}


# CHROM HASH
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
#$inharray = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');
if ($loh == 1) {
	$atable = 'aberration_LOH';
	$ltable = 'log_loh';
	$fields = '';
}
elseif ($mos == 1) {
	$atable = 'aberration_mosaic';
	$ltable = 'log_mos';
	$fields = ',a.cn';
}
else {
	$atable = 'aberration';
	$ltable = 'log';
	$fields = ',a.cn';
}

#######################
# CONNECT TO DATABASE #
#######################
include('inc_query_functions.inc');
include('.LoadCredentials.php');

$db = "CNVanalysis" . $_SESSION["dbname"];
$config['DB'] = $db;
mysql_select_db("$db");

$aid = SqlEscapeValues($_GET['aid']);
if (!is_numeric($aid) ) {
	echo "invalid aberration ID (GET variable 'aid') given: should be numeric";
	exit();
}
$uid = SqlEscapeValues($_GET['u']);
if (!is_numeric($uid) ) {
	echo "invalid user ID (GET variable 'u') given: should be numeric";
	exit();
}

$query = mysql_query("SELECT a.sample, a.idproj, a.class, a.chr, a.start, a.stop, a.largestart, a.largestop,  a.seenby, s.chip_dnanr, a.validation, a.validationdetails $fields FROM $atable a JOIN sample s ON s.id =a.sample WHERE a.id = $aid ");
$row = mysql_fetch_array($query);
$class = $row['class'];
$sid = $row['sample'];
$pid = $row['idproj'];
$chr = $row['chr'];
$chrtxt = $chromhash[$chr];
$start = $row['start'];
$stop = $row['stop'];
if ($loh != 1) {
	$cn = $row['cn'];
}
$sb = $row['seenby'];
$samplename = $row['chip_dnanr'];
$largestart = $row['largestart'];
$largestop = $row['largestop'];
$validation = $row['validation'];
$details = $row['validationdetails'];
$close = 1;
$region = "Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',');

$firstcell =  "style=\"border-left: 1px solid black;\"";

if ($cn == 9) {
	# get upd type
	$updq = mysql_query("select type from parents_upd WHERE sid = '$sid' AND chr = '$chr' AND start = '$start' AND stop = '$stop'");
	$updr = mysql_fetch_array($updq);
	$updtype = $updr['type'];
}

## SET HTML HEADERS 
?>
<html>
<head><title>CNV VALIDATION DETAILS: <?php echo $region; ?></title>
<style type="text/css">
td {
	font-size:12;
	border-right: 1px solid black;
	border-bottom: 1px solid black;
	text-align: left;
	padding: 6px 6px 6px 12px;
	padding: 3px 3px 3px 12px; 
}
td.grey {
	font-size:14;
	border-right: 1px solid black;
	border-top: 1px solid black;
	text-align: center;
	font-weight: bold;
	padding: 6px 6px 6px 12px;
	background:#eeeeee;
	padding: 3px 3px 3px 12px; 
	
}

</style>
<script type=text/javascript language='javascript'>
  	function clearfield(fieldid) {
		document.getElementById(fieldid).value = "";
	}
</script>
<script language="javascript" type="text/javascript">
function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
</script>
</head>
<body>
<?php
if (isset($_GET['cancel'])) {
	$close = 1;
}
# UPDATE DATA !
elseif (isset($_GET['save'])) {
	if (isset($_GET['arguments']) && $_GET['arguments'] == '') {
		# arguments are mandatory when adapting 
		# skip this block, go to else... 
		echo "<p>Error, missing mandatory fields, go back and check everything!</p>";
		exit();
	}
	elseif (isset($_GET['arguments'])) {
		$arguments = $_GET['arguments'];
	}
	else {
		$arguments = '';
	}
	# Get new values
	$newdetails = $_GET['details'];
	// check if validation is different.
	$newval = 0;
	$method = $_GET['method'];
	if ($method == 'None') {
		# set none to empty
		$method = '';
		$methodtxt = 'None';
	}
	else {
		$methodtxt = $method;
	}
	if ($method != $validation || $newdetails != $details || $arguments != '') {
		# insert into log
		if ($details != $newdetails && $details != '') {
			$entry .= " Updated Validation method to $methodtxt and updated details";
		}
		else {
			$entry = "Set Validation method to $methodtxt";
		}
		$entry = SqlEscapeValues($entry);
		$arguments = SqlEscapeValues($arguments);
		mysql_query("INSERT INTO $ltable (sid, aid, pid, uid, entry, arguments) VALUES ('$sid', '$aid', '$pid', '$uid', '$entry', '$arguments')");
	}
	# update aberration table
	$method = SqlEscapeValues($method);
	$newdetails = SqlEscapeValues($newdetails);
	mysql_query("UPDATE $atable SET validation = '$method', validationdetails = '$newdetails' WHERE id = '$aid'");
	# set close-variable 
	$close = 1;
}
else {
	$region = "Chr".$chromhash[$chr].":".number_format($start,0,'',',')."-".number_format($stop,0,'',',');
	$close = 0;
	$logq = mysql_query("SELECT uid, entry FROM $ltable WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$setbyuid = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Validation/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = $usrow['FirstName'] .' '.$usrow['LastName'] ;
			$setbyuid = $logrow['uid'];
			break;
		}
	}
	echo "<h3>Set CNV Validation: $region</h3>";
	echo "<p>Please specify validation details like probe names, or primer location for the used method.</p>\n";
	echo "<p>";
	

	echo "<p><form action=setvalidation.php method=GET>";
	echo "<input type=hidden name=loh value='$loh'>";
	echo "<input type=hidden name=mos value='$mos'>";

	echo "<table cellspacing=0>";
	echo "<tr><td colspan=2 $firstcell class=grey>Region Details</td></tr>";
	# region
	echo "<tr><td $firstcell>Region</td><td>Chr$chrtxt:".number_format($start,0,'',',')."-".number_format($stop,0,'',',')."</td></tr>";
	# CopyNumber
	if ($loh != 1) {
		echo "<tr><td $firstcell>CopyNumber</td><td>$cn</td></tr>";
	}
	echo "<tr><td $firstcell>Validation</th><td><select name=method>";
	$found = 0;
	$methods = array('FISH', 'MLPA', 'MAQ','qPCR','Micro-Array','Karyogram','Sequencing','Other');
	echo "<option value='none'>Not Validated</option>";
	foreach ($methods as $key => $val) {
		if ($val == $validation) {
			$sel = 'SELECTED';
			$found = 1;
		}
		else {
			$sel = '';
		}
		echo "<option value='$val' $sel>$val</option>";
	}
	echo "</select></td></tr>";
	#echo "</table>";
	echo "<tr><td colspan=2 $firstcell class=grey>Validation Details</td></tr>";
	echo "<tr><td $firstcell><input type=hidden name=aid value='$aid'>";
	echo "<input type=hidden name=u value='$uid'>";
	echo "e.g. Probe Name/Location (max 255 chars)</td><td><textarea name=details cols=35 rows=5 onKeyDown='limitText(this.form.details,this.form.countdown,255);' onKeyUp='limitText(this.form.details,this.form.countdown,255);' onpaste='limitText(this.form.details,this.form.countdown,255);'>$details</textarea></td></tr>";
	if ($details == '') {
		$left = 255;
	}
	else {
		$left = 255 - strlen($details);
	}
	echo "<tr><td $firstcell>Characters Left </td><td ><input type=text name=countdown value='$left'></td></tr>";
	echo "</table></p><p>";
	if ($setbyuid != '') {
		echo "<p>Please specify why you are changing validation details set by ";
		if ($setbyuid == $uid) {
			echo "yourself (this is mandatory!):<br/>";
		}
		else {
			echo "$setby (this is mandatory!):<br/>";
		}
		echo "<input type=text name=arguments size=32></p>";
	}
	echo "<input type=submit name=save value='Save'> &nbsp; &nbsp; <input type=submit name=cancel value='Cancel'></p>\n";
	echo" </form>\n";
	
}

## CLOSE POPUP & GO BACK TO CALLING PAGE
if ($close == 1) {
	echo "<script type='text/javascript'>window.opener.location.reload()</script>";
	echo "<script type='text/javascript'>window.close()</script>";
}
?>

</body>
</html>
