<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>

<head>
	<title>(CMG) CNV-Analysis Page</title>
	<!-- Bundled General Stylesheet file -->
	<link rel='stylesheet' type='text/css' href='stylesheets/All_Format2.css'/>
</head>
<body>
<div id=container>
<div id=inhoud-rechts>
<div class=sectie>
<?php
	for ( $i = 1; $i <= 22; $i += 1) {
		$chromhash["$i"] = "$i";
	}
	$chromhash["X"] = "23";
	$chromhash["Y"] = "24";
	$chromhash["23"] = "X";
	$chromhash["24"] = "Y";

$tdtype= array("","class=alt");
$thtype= array("class=spec","class=specalt");
$topstyle = array("class=topcell","class=topcellalt");
$firstcell = "style=\"border-left: 1px solid #a1a6a4;\"";

include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION['dbname'];
mysql_select_db("primerdesign".$_SESSION['dbname']);




$chrtxt = $_GET['chr'];
$chr = $chromhash[ $chrtxt ];
$start = $_GET['start'];
$end = $_GET['end'];
#echo "chr: $chr : start: $start : end: $end<br>\n";
$end = str_replace(",","",$end);
$start = str_replace(",","",$start);
$uid = $_GET['uid'];
$uquery = mysql_query("SELECT submitter FROM Succeeded WHERE chr = '$chr' AND start <= '$start' AND end >='$end' AND submitter <> '$uid' AND submitter <> '0' GROUP BY submitter");
$namelist = "";
if (mysql_num_rows($uquery) > 0) {
	mysql_select_db("$db");
	while ($row = mysql_fetch_array($uquery) ) {
		$suid = $row['submitter'];
		$query = mysql_query("SELECT FirstName, LastName FROM users WHERE id = '$suid'");
		$subrow = mysql_fetch_array($query);
		$FirstName = $subrow['FirstName'];
		$LastName = $subrow['LastName'];
		if ($namelist == "") {
			$namelist = $namelist . " $FirstName $LastName";
		}
		else {
			$namelist = $namelist . ", $FirstName $LastName";
		}
	}
	mysql_select_db("primerdesign".$_SESSION['dbname']);
}
$query = mysql_query("SELECT chr, start, end, forward, reverse, amplicon, remark FROM Primers WHERE chr = \"$chr\" AND (Start >= \"$start\" AND End <= \"$end\")");
		#echo "chrom $chr from $start to $end<br>\n";
		$numrows = mysql_num_rows($query);
		if ($numrows == 0 ) { 
			echo "<h3>No Primers found for $location</h3>\n";
			echo "<p>Please report this, as it should not be possible</p>\n";
		}
		else {
			echo "<h3> Available amplicons for chr$chrtxt:".number_format($start,0,'',',')."-".number_format($end,0,'',',')."</h3>\n";
			if ($namelist != "") {
				echo "<h4> Overlapping regions submitted by$namelist</h4>\n";
			}
			echo "<p>\n";
			echo "<table cellspacing=0 >\n";
			echo " <tr>\n";
			echo "  <th $firstcell>Location</td>\n";
			echo "  <th>Forward</td>\n";
			echo "  <th>Reverse</td>\n";
			echo "  <th>Amplicon</td>\n";
			echo "  <th>Remarks</td>\n";
			echo " </tr>\n";
		 
			$switch=0;
			while ($row = mysql_fetch_assoc($query)) {
				$amplicon = $row['amplicon'];
				$ampl = "";
				for ($i = 0; $i < strlen($amplicon) ; $i = $i+30) {
					$ampl = $ampl . substr($amplicon, $i, 30) ."<br>\n";;
				}
				echo " <tr>\n";
				echo "  <td $tdtype[$switch] $firstcell>chr" .$chromhash[ $row['chr'] ].":".number_format($row['start'],0,'',',')."-".number_format($row['end'],0,'',',')."</td>\n";
				echo "  <td $tdtype[$switch]>".strtolower($row['forward'])."</td>\n";
				echo "  <td $tdtype[$switch]>".strtolower($row['reverse'])."</td>\n";
				echo "  <td $tdtype[$switch]>".$ampl."</td>\n";
				echo "  <td $tdtype[$switch]>".$row['remark']."</td>\n";
				echo " </tr>\n";
				$switch = $switch + pow(-1,$switch);
				$found = 1;
			}
			echo "</table>\n";
		}

?>
</div>
</div>
</div>
</body>
</html>
