<?php
//Tell the browser what kind of file is come in
header("Content-Type: image/png");

#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();

$font = 'arial'; 
#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");

# GET VARS
$chr = $_GET['c'];
$chrtxt = $chromhash[ $chr ];
$start = $_GET['start'];
$stop = $_GET['stop'];
$userid = $_GET['u'];
#$chips = $_GET['chips'];
$locquery = "AND ((start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))";
$window = $stop-$start+1;
$cn = $_GET['cn'];

# GET included chiptypes/projects
#$chiptypes = explode('-',$chips);
$correlations = array();
$instring = '';

#foreach ($chiptypes as $key => $cid) {
$query = mysql_query("SELECT Controles, ID FROM chiptypes"); # WHERE ID = '$cid'");
#$row = mysql_fetch_array($query);
while ($row = mysql_fetch_array($query)) {
	if ($row['Controles'] == '') {
		# no controle project for this chip
		continue;
	}
	$cid = $row['ID'];
	$correlations[$cid] = $row['Controles']; 
	$instring .= $row['Controles'] . ',';
}
#}
$instring = substr($instring,0,-1); 

# DEFINE IMAGE PROPERTIES
$querystring = "SELECT COUNT(a.id) AS aantal FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE ps.INIWAVE = 0 AND ps.calc = 1 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2 AND a.idproj IN ($instring) AND a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))  AND a.cn = '$cn' GROUP BY s.chip_dnanr, p.chiptype";

$result = mysql_query($querystring);
$nrlines = mysql_num_rows($result);
	
$querystring = "SELECT COUNT(a.id) AS aantal FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE ps.INIWAVE = 0 AND ps.calc = 1 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2 AND a.idproj IN ($instring) AND a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))  AND a.cn = '$cn' GROUP BY s.chip_dnanr";

$result = mysql_query($querystring);
$nrsamples = mysql_num_rows($result);


$height = $nrlines*10 + $nrsamples*4 + 20;
	
//$height = 300;
//$height = 60 + $nrabs*10;
if ($window > 47000000) {
	$scale = 3000000;
	$stext = "3 Mb";
}
elseif ($window > 32000000) {
	$scale = 2000000;
	$stext = "2 Mb";
}
elseif ($window > 16000000) {
	$scale = 1000000;
	$stext = "1 Mb";
	}
else {
	$scale = 500000;
	$stext = "500 kb";
} 

$scalef = 280/($window);
$result = mysql_query("SELECT stop FROM cytoBand WHERE chr = '$chr' AND name LIKE '%p%' ORDER BY stop DESC LIMIT 1");
$row = mysql_fetch_array($result);
$lastp = $row['stop'];


//Specify constant values
$width = 400; //Image width in pixels
//Create the image resource
$image = ImageCreate($width, $height);
//We are making four colors, white, black, blue and red
$white = ImageColorAllocate($image, 255, 255, 255);
$black = ImageColorAllocate($image, 0, 0, 0);
$red   = ImageColorAllocate($image, 255, 0, 0);
$blue  = imageColorallocate($image,0,0,255); 
$green = imageColorAllocate($image,0,190,0);
$purple = ImageColorAllocate($image,136,34,135);
$orange = ImageColorAllocate($image,255, 179,0);

$gneg = ImageColorAllocate($image, 255, 255, 255);
$gpos25 = ImageColorAllocate($image, 208,208,208);
$gpos50 = ImageColorAllocate($image, 144,144,144);
$gpos75 = ImageColorAllocate($image, 88,88,88);
$gpos100 = ImageColorAllocate($image, 0, 0, 0);
$acen = ImageColorAllocate($image, 144,144,144);
$gvar = ImageColorAllocate($image, 144,144,144);
$stalk = ImageColorAllocate($image, 144,144,144);

$colors = array('gneg' => $gneg, 'gpos25' => $gpos25, 'gpos50' => $gpos50, 'gpos75' => $gpos75, 'gpos100' => $gpos100, 'acen' => $acen, 'gvar' => $gvar, 'stalk' => $stalk);
$cns = array('0' => $red, '1' => $red, '2' => $orange, '3' => $blue, '4' => $blue);
#Fill background
imagefill($image,0,0,$white);

//Output header
//46944323 end of 21

/*
# CREATE SCALE
$scaledscale = intval(round($scale*$scalef));
imagerectangle($image, 0, 0, 60, 20, $black);
$fontwidth = imagefontwidth(3);
$txtwidth = strlen($stext)*$fontwidth;
$txtx = 30 -($txtwidth/2);
imagestring($image,3,$txtx,1,$stext,$black);
$xstart = 30-($scaledscale/2);
$xstop = 30+($scaledscale/2);
imageline($image,$xstart,18,$xstart,14,$black);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
*/
$querystring = "SELECT a.start, a.stop, s.chip_dnanr, s.id AS sid, p.chiptype, p.id AS pid FROM aberration a JOIN project p JOIN projsamp ps JOIN sample s ON a.idproj = p.id AND a.idproj = ps.idproj AND a.sample = ps.idsamp AND a.sample = s.id WHERE ps.calc = 1 AND ps.INIWAVE = 0 AND ps.callrate > 0.994 AND ps.LRRSD < 0.2 AND a.idproj IN ($instring) AND a.chr = '$chr' AND ( (start BETWEEN '$start' AND '$stop') OR (stop BETWEEN '$start' AND '$stop') OR (start <= '$start' AND stop >= '$stop'))  AND a.cn = '$cn' ORDER BY s.chip_dnanr, p.chiptype, a.start";
//echo "<div class=nadruk>Currently Reformatting !</div>\n";
$result = mysql_query($querystring);
$csample = "";
$cct = "";
//echo "<ul id=ul-simple>\n";
$y = 5;
$cy = $y;
$xoff = 110;
while ($row = mysql_fetch_array($result)) {
	$cstart = $row['start'];
	$cstop = $row['stop'];
	$csid = $row['sid'];
	$cpid = $row['pid'];
	$chip_dnanr = $row['chip_dnanr'];
	$chiptype = $row['chiptype'];
	if ($chip_dnanr != $csample) {
		if ($csample != '') {
			imagestring($image,1,10,($cy+$y)/2-2,$csample,$black);
			//if ($y != $cy) {
			//	imagefilledrectangle($image,47,$cy-1,47,$y+7,$black);	
			//}
			imagefilledrectangle($image,0,$y+10,$width-1,$y+10,$gpos50);
		}
		
		//echo "</ul>\n$chip_dnanr<ul id=ul-simple>\n";
		$csample = $chip_dnanr;
		$cy = $y+14;
		$y = $y +4;
		$cct = '';
	}
	
	if ($chiptype != $cct) {
		$y = $y+10;
		$chipstring = preg_replace('/(Human)(.*)/',"$2",$chiptype);		

		imagestring($image,1,50,$y-2,$chipstring,$black);
		$cct = $chiptype;
	}
	if ($cstart < $start) {
		$cstart = $start;
	}
	if ($cstop > $stop) {
		$cstop = $stop;
	}
	$scaledstart = intval(round(($cstart-$start) * $scalef));
	$scaledstop = intval(round(($cstop-$start) * $scalef));
	imagefilledrectangle($image,$xoff+$scaledstart,$y+1,$xoff+$scaledstop,$y+3,$cns[$cn]);
	
	//$region = "chr$chrtxt:". number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	//$nrq = mysql_query("SELECT COUNT(id) AS 'aantal' FROM aberration WHERE sample = '$sid' AND idproj = '$pid'");
	//$res = mysql_fetch_array($nrq);
	//$nr = $res['aantal'];
	//echo "<li><a class=tt href='index.php?page=details&amp;project=$pid&amp;sample=$sid' target=new>$chiptype</a>: $region</li>";
}

imagestring($image,1,10,($cy+$y)/2-2,$csample,$black);
//if ($y != $cy) {
//	imagefilledrectangle($image,47,$cy-1,47,$y+7,$black);
//}

# Draw the table borders
imagestring($image,2,10,1,"Sample",$gpos75);
imagestring($image,2,50,1,"ChipType",$gpos75);
imagefilledrectangle($image,0,0,0,$height,$gpos50);
imagefilledrectangle($image,0,0,$width-1,0,$gpos50);
imagefilledrectangle($image,0,$height-1,$width-1,$height-1,$gpos50);
imagefilledrectangle($image,$width-1,0,$width-1,$height-1,$gpos50);
imagefilledrectangle($image,47,0,47,$height-1,$gpos50);
imagefilledrectangle($image,108,0,108,$height-1,$gpos50);
imagefilledrectangle($image,0,15,$width-1,15,$gpos50);		

#draw position indications: 
$formatstart = number_format($start,0,'',',');
$formatstop = number_format($stop,0,'',',');
imageline($image,110,7,114,4,$gpos75);
imageline($image,110,7,114,10,$gpos75);
imageline($image,110,7,120,7,$gpos75);
imageline($image,$xstop,18,$xstop,14,$black);
imageline($image,$xstart,16,$xstop,16,$black);
imagestring($image,2,124,1,$formatstart,$gpos75);

$fontwidth = imagefontwidth(2);
$txtwidth = strlen($formatstop)*$fontwidth;
imageline($image, 390,7,380,7,$gpos75);
imageline($image, 390,7,386,4,$gpos75);
imageline($image, 390,7,386,10,$gpos75);
imagestring($image,2,378-$txtwidth,2,$formatstop,$gpos75);


		
//Output the newly created image in jpeg format
imagepng($image);

//Free up resources
ImageDestroy($image); 
?>

