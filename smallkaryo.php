<?php
session_start();
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$sid = $_GET['sid'];
$pid = $_GET['uid'];
if (isset($_GET['cstm'])) {
	$cstm = $_GET['cstm'];
}
else {
	$cstm = 0;
}
echo "<div id=smallkaryoplot style='text-align:center'>";
echo "<img src='create_smallkaryo.php?sid=$sid&uid=$pid&cstm=$cstm' border=0 float:center>";
echo "</div>";
echo "<div><span class=nadruk>Legend:</span><br/>";
echo "<table cellspacing=0 width='95%'>";
echo" <tr>";
echo "<th class=topcellalt $firstcell colspan=2>Copy Numbers</th><th class=topcellalt colspan=2>Additional Info</th>";
echo "</tr>";
echo "<tr class=alt>";
echo "<td class=alt $firstcell style='font-size:9px'><span style='background-color:red'>&nbsp;&nbsp;</span></td><td class=alt NOWRAP style='font-size:9px'>CopyNumber 0</td>";
echo "<td class=alt><span style='color:red' style='font-size:9px'>*</span></td><td class=alt NOWRAP style='font-size:9px'>Pathogenic CNV</td>";
echo "</tr>";

echo "<tr>";
echo "<td class=alt $firstcell style='font-size:9px'><span style='background-color:red'>&nbsp;&nbsp;</span></td><td class=alt NOWRAP style='font-size:9px'>CopyNumber 1</td>";
echo "<td class=alt><span style='color:dimgray' style='font-size:9px'>*</span></td><td class=alt NOWRAP style='font-size:9px'>Unknown Significance CNV</td>";
echo "</tr>";

echo "<tr>";
echo "<td class=alt $firstcell style='font-size:9px'><span style='background-color:orange'>&nbsp;&nbsp;</span></td><td class=alt NOWRAP style='font-size:9px'>CopyNumber 2</td>";
echo "<td class=alt style='font-size:9px'>Lighter Colors</td><td class=alt NOWRAP style='font-size:9px'>Benign CNV</td>";
echo "</tr>";

echo "<tr>";
echo "<td class=alt $firstcell style='font-size:9px'><span style='background-color:blue'>&nbsp;&nbsp;</span></td><td class=alt NOWRAP style='font-size:9px'>CopyNumber 3</td>";
## GET LOH SETTINGS
$lohshow = $_SESSION['LOHshow'];
$lohsize = $_SESSION['LOHminsize'];
$lohsnp = $_SESSION['LOHminsnp'];
if ($lohshow == 1) {
	echo "<td class=alt NOWRAP style='font-size:9px'>Show LOH</td><td class=alt NOWRAP style='font-size:9px'>Yes</td>";
}
else {
	echo "<td class=alt NOWRAP style='font-size:9px'>Show LOH</td><td class=alt NOWRAP style='font-size:9px'>No</td>";
}
echo "</tr>";

echo "<tr>";
echo "<td class=alt NOWRAP $firstcell style='font-size:9px'><span style='background-color:blue'>&nbsp;&nbsp;</span></td><td class=alt NOWRAP style='font-size:9px'>CopyNumber 4</td>";
if ($lohshow == 1) {
	if ($_GET['cstm'] == 1) {
		echo "<td class=alt NOWRAP style='font-size:9px'>LOH.min.</td><td class=alt NOWRAP style='font-size:9px'>$lohsize"."bp</td>";
	}
	else {
		echo "<td class=alt NOWRAP style='font-size:9px'>LOH.min.</td><td class=alt NOWRAP style='font-size:9px'>$lohsize"."bp, $lohsnp probes</td>";
	}	
}
else {
	echo "<td class=alt NOWRAP style='font-size:9px'>&nbsp;</td><td class=alt NOWRAP style='font-size:9px'>&nbsp;</td>";
}
echo "</tr>";
echo "<tr>";
echo "<td class=alt NOWRAP $firstcell style='font-size:9px'><span style='background-color:pink'>&nbsp;&nbsp;</span></td><td class=alt colspan=4 NOWRAP style='font-size:9px'>UniParental Disomy</td>";
echo "</tr>";
echo "</table>";
echo "</div>";

?>
