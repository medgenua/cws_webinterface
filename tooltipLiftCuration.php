
<?php
echo "<div style='border:solid 2px red;padding:2px'>";

ob_start();
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
// get posted vars
$aid = $_GET['q'];
$userid = $_GET['u'];
$table = $_GET['t'];
$build = $_GET['b'];
$sourcedb = "CNVanalysis$build";
// get details on region from database
if ($table == 'aberration') {
	$header = 'CNV Region';
	$result = mysql_query("SELECT p.chiptype, a.cn, a.start, a.stop, a.chr, s.chip_dnanr, s.gender, a.seenby, a.nrsnps, a.nrgenes, a.sample, a.idproj, a.class, a.inheritance, a.pubmed, a.validation, a.validationdetails FROM `$sourcedb`.aberration a JOIN `$sourcedb`.sample s JOIN `$sourcedb`.project p ON a.sample = s.id AND a.idproj = p.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$chiptype = $row['chiptype'];
	$gender = $row['gender'];
	$sid = $row['sample'];
	$pid = $row['idproj'];
	$cn = $row['cn'];
	$start = $row['start'];
	$stop = $row['stop'];
	$size = $stop - $start + 1;
	$chr = $row['chr'];
	$chrtxt = $chromhash[ $chr];
	$sample = $row['chip_dnanr'];
	$seenby = $row['seenby'];
	$sseenby = substr($seenby,2);
	$seenrow = explode('-',$sseenby);
	$nrsnps = $row['nrsnps'];
	$nrgenes = $row['nrgenes'];
	$class= $row['class'];
	$inheritance = $row['inheritance'];
	$pubmed = $row['pubmed'];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	$validation = $row['validation'];
	$validationdetails = $row['validationdetails'];
	// GET EXTRA DATA IF UPD EVENT
	if ($cn == 9) {
		# get id and type and parents from UPD-table
		$updq = mysql_query("SELECT id, type, fid, mid, data FROM parents_upd WHERE sid = '$sid' AND chr = '$chr' AND start = '$start' AND stop = '$stop'");
		$urows = mysql_num_rows($updq);
		$updr = mysql_fetch_array($updq);
		$updaid = $updr['id'];
		$updtype = $updr['type'];
		$updfid = $updr['fid'];
		$updmid = $updr['mid'];
		$updgts = $updr['data'];
		# get datapoints
		$upddata = array();
		$dq = mysql_query("SELECT sid, content FROM parents_upd_datapoints WHERE id = $updaid");
		while ($dqr = mysql_fetch_array($dq)) {
			$dqrs = $dqr['sid'];
			$dqrc = $dqr['content'];
			$dqrc = explode('_',$dqrc);
			$dqre = count($dqrc);
			for ($i = 0; $i < $dqre; $i = $i+ 3) {
				$upddata[$dqrs][$dqrc[$i]]['logR'] = $dqrc[($i + 1)];
				$upddata[$dqrs][$dqrc[$i]]['GT'] = $dqrc[($i + 2)];
			}
		}
	
	}
	// set output left column
	$left =  " <ul id=ul-simple>";
	$left .= "<li>- Source Table : $header</li>";
	$left .= "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	if ($cn == 9) {
		$left .= "<li>- UPD-type: $updtype</li>";
	}
	else {
		$left .= "<li>- Copy Number : $cn</li>";
	}
	$left .= "<li>- Number of Probes : $nrsnps</li>";
	$left .= "<li>- Seen by: <ol>";
	$nrseen = 0;
	if ($seenby != '') {
		foreach ($seenrow as $item) {
			$nrseen++;
			$left .= "<li>$item</li>";
		}
	}
	else {
		$left .= "<li>Manually Inserted</li>";
	}
	$left .= "</ol></li>";
	$left .= "</ul>";
	$left .= "<span class=nadruk>CNV Classification</span>";
	$left .= "<ul id=ul-simple>";
	$left .= "<li>- Inheritance: ". $inh[$inh[$inheritance]] . "</li>\n";
	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM `$sourcedb`.log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		if ($class == 0) {
			$class = 'Not Defined';
		}
		if ($class == 5) {
			$left .= "<li>- Diagnostic Class: False Positive <span class=italic style='font-size:9px;'>$setby</span>";
		}
		else {
			$left .= "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
		}
	}
	else {
		$left .= "<li>- Diagnostic Class: Not Defined ";
	}
	if (isset($arguments) && $arguments != '') {
		$left .= "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
	}
	$left .= "</li>\n";
	if ($validation != '') {
		if ($validationdetails != '') {
			$valtitle = $validationdetails;
		}
		else {
			$valtitle = 'No Additional Details Specified';
		}
		$left .= "<li><span title='$valtitle'>- Validated by $validation </span>";
	}
	else {
		$left .= "<li>- Not Validated";
	}
	# check log for user who validated 
	$logq = mysql_query("SELECT uid, entry,arguments FROM `$sourcedb`.log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$arguments = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Validation/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM `$sourcedb`.users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
			$arguments = $logrow['arguments'];
			$logentry = $logrow['entry'];
			break;
		}
	}
	if ($setby != '') {
		$left .= "<span class=italic style='font-size:9px;'>$setby</span>";
	}
	if ($arguments != '') {
		$left .= "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Was adapted:</span> $arguments</div>";
	}
	$left .= "</li>";	
	if ($pubmed != '') {
		$left .= "<li>- <a class=tt href='index.php?page=literature&type=read&aid=$aid' target='_blank'>Show Associated Publications</a></li>";
	}
	$left .= "</ul>\n";
	// PRINT SAMPLE DETAILS
	$right = '';
	$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$lsd = $row['LRRSD'];
	$bsd = $row['BAFSD'];
	$wave = $row['WAVE'];
	$iniwave = $row['INIWAVE'];
	$callrate = $row['callrate'];
	$remark = "<div class=nadruk>Quality Remarks:</div><ul id=ul-simple>";
	$remarkstring = $remark;
	if ($callrate < 0.994) {
		//$cbg = 'style="background:red;color:black"';
		$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
	}
	if ($lsd > 0.2 && $lsd < 0.3) {
		$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
	}
	if ($lsd >=0.3) {
		$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
	}
	if ($bsd >= 0.6) {
		$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
	} 
	if ($iniwave != 0) {
		$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
	}
	$query = mysql_query("select COUNT(id) as aantal FROM `$sourcedb`.aberration WHERE sample = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	
	$right .= "<span class=nadruk>Sample Details</span>";
	$right .= "<ul id=ul-simple>";
	$right .= "<li>- Sample :$sample</li>\n";
	$right .= "<li>- Chiptype: $chiptype</li>";
	$right .= "<li>- Gender : $gender</li>\n";
	$right .= "<li>- #CNV's : $ncnvs</li>\n";
	if ($remarkstring != $remark) {
		$right .= "</ul>\n";
		$right .= "<p>";
		$right .= $remarkstring;
		$right .= "</ul></p>";
	}
	else {
		$right .= "<li>- Quality : OK</li>\n";
		$right .= "</ul>\n";
	}

}	
elseif ($table == 'cus_aberration') {
	$header = 'Custom Data CNV Region';
	$result = mysql_query("SELECT a.chiptype,a.cn, a.start, a.stop, a.chr, a.sample, s.gender, a.nrgenes, a.idproj, a.class, a.inheritance, a.pubmed FROM `$sourcedb`.cus_aberration a JOIN `$sourcedb`.cus_sample s ON a.sample = s.idsamp AND a.idproj = s.idproj WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$chiptype = $row['chiptype'];
	$gender = $row['gender'];
	$sample = $row['sample'];
	$pid = $row['idproj'];
	$cn = $row['cn'];
	$start = $row['start'];
	$stop = $row['stop'];
	$size = $stop - $start + 1;
	$chr = $row['chr'];
	$chrtxt = $chromhash[ $chr];
	$nrgenes = $row['nrgenes'];
	$class= $row['class'];
	$inheritance = $row['inheritance'];
	$pubmed = $row['pubmed'];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	// set output left column
	$left =  " <ul id=ul-simple>";
	$left .= "<li>- Source Table : $header</li>";
	$left .= "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	$left .= "<li>- Copy Number : $cn</li>";
	$left .= "</ul>";
	$left .= "<span class=nadruk>CNV Classification</span>";
	$left .= "<ul id=ul-simple>";
	$left .= "<li>- Inheritance: ". $inh[$inh[$inheritance]] . "</li>\n";
	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM `$sourcedb`.cus_log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		if ($class == 0) {
			$class = 'Not Defined';
		}
		if ($class == 5) {
			$left .= "<li>- Diagnostic Class: False Positive <span class=italic style='font-size:9px;'>$setby</span>";
		}
		else {
			$left .= "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
		}
	}
	else {
		$left .= "<li>- Diagnostic Class: Not Defined ";
	}
	if ($arguments != '') {
		$left .= "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
	}
	$left .= "</li>\n";
	if ($pubmed != '') {
		$left .= "<li>- <a class=tt href='index.php?page=literature&type=read&aid=$aid' target='_blank'>Show Associated Publications</a></li>";
	}
	$left .= "</ul>\n";
	// PRINT SAMPLE DETAILS
	$right = '';
	$query = mysql_query("select COUNT(id) as aantal FROM `$sourcedb`.cus_aberration WHERE sample = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	
	$right .= "<span class=nadruk>Sample Details</span>";
	$right .= "<ul id=ul-simple>";
	$right .= "<li>- Sample : $sample</li>\n";
	if ($chiptype != '') {
		$right .= "<li>- Chiptype: $chiptype</li>";
	}
	if ($gender != '') {
		$right .= "<li>- Gender : $gender</li>\n";
	}
	$right .= "<li>- #CNV's : $ncnvs</li>\n";
	$right .= "</ul>";

}
elseif ($table == 'parents_regions') {
	$header = 'Parental non-CNV ';	
	$result = mysql_query("SELECT a.start, a.stop, a.chr, a.sid, s.chip_dnanr, s.gender FROM `$sourcedb`.parents_regions a JOIN `$sourcedb`.sample s ON a.sid = s.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$gender = $row['gender'];
	$sample = $row['chip_dnanr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$size = $stop - $start + 1;
	$chr = $row['chr'];
	$chrtxt = $chromhash[ $chr];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	// set output left column
	$left =  " <ul id=ul-simple>";
	$left .= "<li>- Source Table : $header</li>";
	$left .= "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	$left .= "</ul>";
	// PRINT SAMPLE DETAILS
	$right = '';
	$right .= "<span class=nadruk>Sample Details</span>";
	$right .= "<ul id=ul-simple>";
	$right .= "<li>- Sample : $sample</li>\n";
	if ($chiptype != '') {
		$right .= "<li>- Chiptype: $chiptype</li>";
	}
	if ($gender != '') {
		$right .= "<li>- Gender : $gender</li>\n";
	}
	$right .= "</ul>";
}
elseif ($table == 'parents_upd') {
	$header = 'UPD-Regions';	
	$result = mysql_query("SELECT a.type, a.id, a.start, a.stop, a.chr, a.sid, s.chip_dnanr, s.gender,a.data, a.fid, a.mid FROM `$sourcedb`.parents_upd a JOIN `$sourcedb`.sample s ON a.sid = s.id WHERE a.id = '$aid'");
	$row = mysql_fetch_array($result);
	$aid = $row['id'];
	$updtype = $row['type'];
	$gender = $row['gender'];
	$sample = $row['chip_dnanr'];
	$start = $row['start'];
	$stop = $row['stop'];
	$size = $stop - $start + 1;
	$chr = $row['chr'];
	$updgts = $row['data'];
	$sid = $row['sid'];
	$updmid = $row['mid'];
	$updfid = $row['fid'];
	$chrtxt = $chromhash[ $chr];
	$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
	// set output left column
	$left =  " <ul id=ul-simple>";
	$left .= "<li>- Source Table : $header</li>";
	$left .= "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	$left .= "</ul>";
	// PRINT SAMPLE DETAILS
	$right = '';
	$right .= "<span class=nadruk>Sample Details</span>";
	$right .= "<ul id=ul-simple>";
	$right .= "<li>- Sample : <a class=tt href='index.php?page=details&project=$pid&sample=$sid' target=new>$sample</a></li>\n";
	if ($chiptype != '') {
		$right .= "<li>- Chiptype: $chiptype</li>";
	}
	if ($gender != '') {
		$right .= "<li>- Gender : $gender</li>\n";
	}
	$right .= "</ul>";
	$upddata = array();
	$dq = mysql_query("SELECT sid, content FROM parents_upd_datapoints WHERE id = $updaid");
	while ($dqr = mysql_fetch_array($dq)) {
		$dqrs = $dqr['sid'];
		$dqrc = $dqr['content'];
		$dqrc = explode('_',$dqrc);
		$dqre = count($dqrc);
		for ($i = 0; $i < $dqre; $i = $i+ 3) {
			$upddata[$dqrs][$dqrc[$i]]['logR'] = $dqrc[($i + 1)];
			$upddata[$dqrs][$dqrc[$i]]['GT'] = $dqrc[($i + 2)];
		}
	}
	
}


	

// PRINT TITLE  
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:red;'>";
echo "<h3 style='margin-top:5px'>$region <span class=italic style='font-weight:bold;color:red'>(UCSC$build)</span></h3>";
echo "</div>\n";

// PRINT CNV DETAILS
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:red;'> ";
## LEFT PANEL
echo "<div style='float:left;width:50%;'>";
echo "<span class=nadruk>CNV Details</span>";
echo $left;
echo "</div>";
## RIGHT PANEL
echo "<div style='float:right;width:46%;'>";
echo $right;
echo "</div>";
echo "<p></p>";
echo "</div>";
ob_flush();
flush();


// bottom of panel 

if (($cn == 9 && $table == 'aberration') || $table == 'parents_upd') {
	echo "<div class=nadruk>Informative Probes for $updtype</div>";
	echo "<p><table cellspacing=0 width='75%'>";
	echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
	echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
	$datapoints = explode('@',$updgts);
	foreach($datapoints as $key => $item) {
		$entries = explode(';',$item);
		echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
	}
	echo "</table>";
}
else {
	
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:red;text-align:center;width:100%' >";
	echo "<div id='loadingdiv' style='text-align:center;'><img src='images/content/ajax-loader.gif'><br/>Loading Plot</div>";
	flush();
	echo "<img src='LiftCuration.plot.php?aid=$aid&t=$table&b=$build' border=0 onload=\"document.getElementById('loadingdiv').style.display='none';\">\n";
	echo "</div>";
}
echo "</div>";
?>

