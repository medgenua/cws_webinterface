
<?php
//echo "<div id=ttcontainer >";

ob_start();
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);

/////////////////////
// get posted vars //
/////////////////////
$aid = $_GET['q'];
$sid = $_GET['sid'];
$pid = $_GET['pid'];
$userid = $_GET['u'];
$show = $_GET['s'];
$fromimage = $_GET['fi'];
$fromtable = $_GET['ft'];


// get details on sample & project from database
$result = mysql_query("SELECT p.chiptype, s.chip_dnanr, s.gender FROM projsamp ps JOIN sample s JOIN project p ON ps.idproj = p.id AND ps.idsamp = s.id WHERE ps.idproj = '$pid' AND ps.idsamp = '$sid'");
$row = mysql_fetch_array($result);
$chiptype = $row['chiptype'];
$gender = $row['gender'];
$sample = $row['chip_dnanr'];
// get details on the UPD region from database
$result = mysql_query("SELECT chr, start, stop, fid, mid, type, pval, nrprobes, logR, LiftedFrom, data FROM parents_upd WHERE id = '$aid'");
$row = mysql_fetch_array($result);
$updmid = $row['mid'];
$updfid = $row['fid'];
$start = $row['start'];
$stop = $row['stop'];
$size = $stop - $start + 1;
$chr = $row['chr'];
$chrtxt = $chromhash[ $chr];
$updtype = $row['type'];
$pval = $row['pval'];
$nrprobes = $row['nrprobes'];
$logR = $row['logR'];
$lifted = $row['LiftedFrom'];
$updgts = $row['data'];
if ($lifted != '') {
	$lifted = "<span class=italic style='font-size:xx-small;color:#610000'>(Lifted from UCSC$lifted)</span>";
}
$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
# get datapoints
$upddata = array();
$dq = mysql_query("SELECT sid, content FROM parents_upd_datapoints WHERE id = $aid");
while ($dqr = mysql_fetch_array($dq)) {
	$dqrs = $dqr['sid'];
	$dqrc = $dqr['content'];
	$dqrc = explode('_',$dqrc);
	$dqre = count($dqrc);
	for ($i = 0; $i < $dqre; $i = $i + 3) {
		$upddata[$dqrs][$dqrc[$i]]['logR'] = $dqrc[($i + 1)];
		$upddata[$dqrs][$dqrc[$i]]['GT'] = $dqrc[($i + 2)];
	}
}

# Check parental info
$parq = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
$parents = mysql_query("$parq");
$parrow = mysql_fetch_array($parents);
$father = $parrow['father'];
$ppid = $parrow['father_project'];
$mother = $parrow['mother'];
$mpid = $parrow['mother_project'];

if ($mother != 0 || $father != 0) {
	# SET mouseover entries
	if ($show == 'i') {
		$mousei = '';
		$stylei = '';
		$stylef = 'text-decoration:none;';
		$stylem = 'text-decoration:none;';
		$mousef = "onmouseover=\"Tip(ToolTipUPD('$aid','$userid','f',0,'$sid','$pid',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousem = "onmouseover=\"Tip(ToolTipUPD('$aid','$userid','m',0,'$sid','$pid',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
	}
	elseif ($show == 'f') {
		$psid = $father;
		$ppid = $ppid;
		$mousei = "onmouseover=\"Tip(ToolTipUPD('$aid','$userid','i',0,'$sid','$pid',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousef = '';
		$stylei = "text-decoration:none;"; 
		$stylef = '';
		$stylem = "text-decoration:none;";
		$mousem = "onmouseover=\"Tip(ToolTipUPD('$aid','$userid','m',0,'$sid','$pid',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
	}
	elseif ($show == 'm') {
		$psid = $mother;
		$ppid = $mpid;
		$mousei = "onmouseover=\"Tip(ToolTipUPD('$aid','$userid','i',0,'$sid','$pid',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousef = "onmouseover=\"Tip(ToolTipUPD('$aid','$userid','f',0,'$sid','$pid',event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousem = '';
		$stylei = "text-decoration:none;";
		$stylef = 'text-decoration:none;';
		$stylem = '';
	}
}


// PRINT TITLE  
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
echo "<h3 style='margin-top:5px'>$region</h3>";
// get links to ensembl, ucsc and dgv
$lquery = mysql_query("SELECT Resource, link FROM db_links WHERE Resource IN ('UCSC','Ensembl','DGV')");
while ($row = mysql_fetch_array($lquery)) {
	$link = $row['link'];
	if ($row['Resource'] == 'Ensembl') {
		$linkparts = explode('@@@',$link) ;
		if ($size < 1000000) {
			$link = $linkparts[0];
		}
		else {
			$link = $linkparts[1];
		}
	}
	$link = str_replace('%c',$chrtxt,$link);
	$link = str_replace('%s',$start,$link);
	$link = str_replace('%e',$stop,$link);
	$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
	$link = str_replace('%u',$ucscdb,$link);
	$links[$row['Resource']] = $link;
}
echo "<span style='font-style:italic;font-size:9px;position:absolute;left:400px;top:1px;'>";
echo "<a class=ttsmall href='".$links['UCSC']."' target='_blank'>UCSC</a><br/>";
echo "<a class=ttsmall href='".$links['Ensembl']."' target='_blank'>ENSEMBL</a><br/>";
echo "<a class=ttsmall href='".$links['DGV']."' target='_blank'>DGV</a><br/>";
echo "</span>";	

echo "</div>\n";

// PRINT FAMILY
if ($mother != 0 || $father != 0) {
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center' >";
	echo "<div style='float:left;padding-left:5pt;$stylei' class=nadruk $mousei>INDEX</div>\n";
	echo "<div style='float:right;padding-right:5pt;$stylem' class=nadruk $mousem>MOTHER</div>\n";
	echo "<span style='text-align:center;$stylef' class=nadruk $mousef>FATHER</span>\n";
	echo "</div>";
}
if ($show == 'i') {
	// PRINT CNV DETAILS
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	## LEFT PANEL
	echo "<div style='float:left;width:50%;'>";
	echo "<span class=nadruk>CNV Details</span> $lifted";
	echo " <ul id=ul-simple>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	echo "<li>- UPD-type: $updtype</li>";
	echo "<li>- Number of Probes : $nrprobes</li>";
	echo "</ul>";

	echo "</div>";

	## RIGHT PANEL
	echo "<div style='float:right;width:46%;'>";
	// PRINT SAMPLE DETAILS
	$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$lsd = $row['LRRSD'];
	$bsd = $row['BAFSD'];
	$wave = $row['WAVE'];
	$iniwave = $row['INIWAVE'];
	$callrate = $row['callrate'];
	$remark = "<div class=nadruk>Quality Remarks:</div><ul id=ul-simple>";
	$remarkstring = $remark;
	if ($callrate < 0.994) {
		//$cbg = 'style="background:red;color:black"';
		$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
	}
	if ($lsd > 0.2 && $lsd < 0.3) {
		$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
	}
	if ($lsd >=0.3) {
		$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
	}
	if ($bsd >= 0.6) {
		$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
	} 
	if ($iniwave != 0) {
		$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
	}
	echo "<span class=nadruk>Sample Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$pid&sample=$sid' target=new>$sample</a></li>\n";
	echo "<li>- Chiptype: $chiptype</li>";
	echo "<li>- Gender : $gender</li>\n";
	if ($remarkstring != $remark) {
		echo "</ul>\n";
		echo "<p>";
		echo $remarkstring;
		echo "</ul></p>";
	}
	else {
		echo "<li>- Quality : OK</li>\n";
		echo "</ul>\n";
	}

	echo "</div>";
	echo "<p></p>";
	echo "</div>";
	ob_flush();
	flush();
	$styleupd = "text-decoration:underline";
	$styleplot = "text-decoration:none";
	$styleclin = 'text-decoration:none;';
	$styletool = 'text-decoration:none;';
	$stylekaryo = 'text-decoration:none;';
	$mouseupd = "onmouseover=\"setsubtip('upd','$aid','$userid',event)\"";
	$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
	$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
	$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
	$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
	# 5 entries, each 20%
	echo "<span style='float:left;width:20%;text-align:center;'><span id=linkprobes style='$styleupd' class=nadruk $mouseupd>UPD-GTypes</span></span>\n";
	echo "<span style='float:left;width:20%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
	echo "<span style='float:left;width:19%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
	echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
	echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
	echo "<br/></div>";
	// table with upd informative points
	echo "<div id=UPDtable style='padding-left:5px'>";
	echo "<div class=nadruk>Informative Probes for $updtype</div>";
	echo "<p><table cellspacing=0 width='75%'>";
	echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
	echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
	$datapoints = explode('@',$updgts);
	#echo "<ul><li>gtypes: $updgts</li></ul>";
	foreach($datapoints as $idx => $item) {
	#foreach($upddata[$sid] as $key => $item) {
		$entries = explode(';',$item);
		$key = $entries[0];
		if ($lifted == '' && ($key < $start || $key > $stop )){
			continue;
		}
		echo "<tr>";
		// position
		echo "<td $firstcell>".number_format($key,0,'',',')."</td>";
		// child GT & logR
		echo "<td>".$upddata[$sid][$key]['GT']."</td>";
		echo "<td NOWRAP>".$upddata[$sid][$key]['logR'] ."</td>";
		// father GT && logR
		echo "<td>".$upddata[$updfid][$key]['GT']."</td>";
		echo "<td NOWRAP>".$upddata[$updfid][$key]['logR'] ."</td>";
		// mother GT && logR
		echo "<td>".$upddata[$updmid][$key]['GT']."</td>";
		echo "<td NOWRAP>".$upddata[$updmid][$key]['logR'] ."</td>";
		echo "</tr>";
	}
	echo "</table>";
	echo "</div>";
	// divs for other subtips
	echo "<div id=subtip style='display:none'>";
	echo "</div>";
	// data plot
	echo "<div id=plotdiv style='display:none'>";
	$setupd = 2;
	include('inc_create_plot.inc');
	echo "</div>";
}
else {
	// PRINT CNV DETAILS (for cnv from child)
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	echo "<div style='float:left;width:48%;'>";
	echo "<span class=nadruk>Offspring CNV Details</span>";
	echo " <ul id=ul-simple>";
	echo "<li>- Sample: $sample</li>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	echo "<li>- UPD Type : $updtype</li>";
	echo "<li>- Number of Probes : $nrprobes</li>";
	echo "</ul>";
	// PRINT CLASSIFICATION DETAILS
	echo" </div>\n";
	// PRINT PARENT DETAILS
	
	$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate FROM projsamp WHERE idsamp = '$psid' AND idproj = '$ppid'");
	$row = mysql_fetch_array($query);
	$lsd = $row['LRRSD'];
	$bsd = $row['BAFSD'];
	$wave = $row['WAVE'];
	$iniwave = $row['INIWAVE'];
	$callrate = $row['callrate'];
	$remark = "<div class=nadruk>Quality Remarks:</div><ul id=ul-simple>";
	$remarkstring = $remark;
	
	if ($callrate < 0.994) {
		//$cbg = 'style="background:red;color:black"';
		$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
		$nrlines++;
	}
	if ($lsd > 0.2 && $lsd < 0.3) {
		$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
	}
	if ($lsd >=0.3) {
		$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
		$nrlines++;
	}
	if ($bsd >= 0.6) {
		$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
		$nrlines++;
	} 
	if ($iniwave != 0) {
		$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
		$nrlines++;
	}
	
	$query = mysql_query("select chip_dnanr, gender FROM sample WHERE id = '$psid'");
	$row = mysql_fetch_array($query);
	$parentname = $row['chip_dnanr'];
	$gender = $row['gender'];
	
	$query = mysql_query("SELECT chiptype FROM project WHERE id = '$ppid'");
	$row = mysql_fetch_array($query);
	$chiptype = $row['chiptype'];
	echo "<div style='float:right;width:48%;'>";
	#echo "<div style='position:absolute;left:215px;top:80px;'>";
	echo "<span class=nadruk>Parental Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$ppid&sample=$psid' target=new>$parentname</a></li>\n";
	echo "<li>- Chiptype: $chiptype</li>";
	echo "<li>- Gender : $gender</li>\n";
	if ($remarkstring != $remark) {
		echo "</ul>\n";
		echo "<p>";
		echo $remarkstring;
		echo "</ul></p>";
	}
	else {
		echo "<li>- Quality : OK</li>\n";
		echo "</ul>\n";
	}
	echo "</div>\n";
	echo "<p></p>";
	echo "</div>";
	ob_flush();
	flush();
	$styleupd = "text-decoration:underline";
	$styleplot = "text-decoration:none";
	$styleclin = 'text-decoration:none;';
	$styletool = 'text-decoration:none;';
	$stylekaryo = 'text-decoration:none;';
	$mouseupd = "onmouseover=\"setsubtip('upd','$aid','$userid',event)\"";
	$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
	$mouseclin = "onmouseover=\"setsubtip('clinical','$psid','$userid',event)\"";
	$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
	$mousekaryo = "onmouseover=\"setsubtip('karyo','$psid','$ppid',event)\"";
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
	# 5 entries, each 20%
	echo "<span style='float:left;width:20%;text-align:center;'><span id=linkprobes style='$styleupd' class=nadruk $mouseupd>UPD-GTypes</span></span>\n";
	echo "<span style='float:left;width:20%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
	echo "<span style='float:left;width:19%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
	echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
	echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
	echo "<br/></div>";
	echo "<div id=UPDtable style='padding-left:5px'>";
	echo "<div class=nadruk>Informative Probes for $updtype</div>";
	echo "<p><table cellspacing=0 width='75%'>";
	echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
	echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
	$datapoints = explode('@',$updgts);
	foreach($datapoints as $key => $item) {
		$entries = explode(';',$item);
		echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
	}
	echo "</table>";
	echo "</div>";
	echo "<div id=subtip style='display:none'>";
	echo "</div>";
	echo "<div id=plotdiv style='display:none'>";
	$setupd = 2;
	include('inc_create_parent_plot.inc');
	echo "</div>";



	#echo "<div class=nadruk>Informative Probes for $updtype</div>";
	#echo "<p><table cellspacing=0 width='75%'>";
	#echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
	#echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
	#$datapoints = explode('@',$updgts);
	#foreach($datapoints as $key => $item) {
	#	$entries = explode(';',$item);
	#	echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
	#}
	#echo "</table>";
	#include('inc_create_parent_plot.inc');

}
//echo "</div>";	
?>

