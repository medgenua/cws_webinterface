
<?php
//echo "<div id=ttcontainer >";

ob_start();
#####################
# GENERAL VARIABLES #
#####################
for ( $i = 1; $i <= 22; $i += 1) {
	$chromhash["$i"] = "$i";
}
$chromhash["23"] = "X";
$chromhash["24"] = "Y";
ob_start();
$inh = array(0=>'ND', 1=>'P', 2=> 'M', 3=>'DN', 'DN' => 'De Novo', 'P' => 'Paternal', 'M' => 'Maternal', 'ND' => 'Not Defined');

////////////////////////////////
// Variables for table layout //
////////////////////////////////
$tdtype= array("","class='alt'");
$thtype= array("class='spec'","class='specalt'");
$topstyle = array("class='topcell'","class='topcellalt'");
$firstcell =  "style=\"border-left: 1px solid #a1a6a4;\"";
$color = array(1 => "yellow", 2 => "yellow", 3 => "orange", 4 => "orange", 5 => "red", 6 => "red");

#######################
# CONNECT TO DATABASE #
#######################
include('.LoadCredentials.php');
$db = "CNVanalysis" . $_SESSION["dbname"];
mysql_select_db("$db");
$ucscdb = str_replace('-','',$_SESSION['dbname']);
// get posted vars
$aid = $_GET['q'];
$userid = $_GET['u'];
$show = $_GET['s'];
$fromimage = $_GET['fi'];
$fromtable = $_GET['ft'];
// get details on cnv from database
$result = mysql_query("SELECT p.chiptype, a.cn, a.start, a.stop, a.chr, s.chip_dnanr, s.gender, a.seenby, a.nrsnps, a.nrgenes, a.sample, a.idproj, a.class, a.inheritance, a.pubmed, a.validation, a.validationdetails, a.LiftedFrom FROM aberration a JOIN sample s JOIN project p ON a.sample = s.id AND a.idproj = p.id WHERE a.id = '$aid'");
$row = mysql_fetch_array($result);
$chiptype = $row['chiptype'];
$gender = $row['gender'];
$sid = $row['sample'];
$pid = $row['idproj'];
$cn = $row['cn'];
$start = $row['start'];
$stop = $row['stop'];
$size = $stop - $start + 1;
$chr = $row['chr'];
$chrtxt = $chromhash[ $chr];
$sample = $row['chip_dnanr'];
$seenby = $row['seenby'];
$sseenby = substr($seenby,2);
$seenrow = explode('-',$sseenby);
$nrsnps = $row['nrsnps'];
$nrgenes = $row['nrgenes'];
$class= $row['class'];
$inheritance = $row['inheritance'];
$pubmed = $row['pubmed'];
$region = "chr" . $chrtxt . ":" . number_format($start,0,'',',') . "-" . number_format($stop,0,'',',');
$validation = $row['validation'];
$lifted = $row['LiftedFrom'];
if ($lifted != '') {
	$lifted = "<span class=italic style='font-size:xx-small;color:#610000'>(Lifted from UCSC$lifted)</span>";
}
$validationdetails = $row['validationdetails'];
// GET EXTRA DATA IF UPD EVENT
if ($cn == 9) {
	# get id and type and parents from UPD-table
	$updq = mysql_query("SELECT id, type, fid, mid, data FROM parents_upd WHERE sid = '$sid' AND chr = '$chr' AND start = '$start' AND stop = '$stop'");
	$urows = mysql_num_rows($updq);
	$updr = mysql_fetch_array($updq);
	$updaid = $updr['id'];
	$updtype = $updr['type'];
	$updfid = $updr['fid'];
	$updmid = $updr['mid'];
	$updgts = $updr['data'];
	# get datapoints
	$upddata = array();
	$dq = mysql_query("SELECT sid, content FROM parents_upd_datapoints WHERE id = $updaid");
	while ($dqr = mysql_fetch_array($dq)) {
		$dqrs = $dqr['sid'];
		$dqrc = $dqr['content'];
		$dqrc = explode('_',$dqrc);
		$dqre = count($dqrc);
		for ($i = 0; $i < $dqre; $i = $i+ 3) {
			$upddata[$dqrs][$dqrc[$i]]['logR'] = $dqrc[($i + 1)];
			$upddata[$dqrs][$dqrc[$i]]['GT'] = $dqrc[($i + 2)];
		}
	}

}

// check permissions for this cnv
$permquery = mysql_query("SELECT editcnv FROM projectpermission WHERE projectid = '$pid' AND userid = '$userid'");
$permrow = mysql_fetch_array($permquery);
$editcnv = $permrow['editcnv'];

# Check parental info
$parq = "SELECT father, father_project, mother, mother_project FROM parents_relations WHERE id = '$sid'";
$parents = mysql_query("$parq");
$parrow = mysql_fetch_array($parents);
$father = $parrow['father'];
$ppid = $parrow['father_project'];
$mother = $parrow['mother'];
$mpid = $parrow['mother_project'];

if ($mother != 0 || $father != 0) {
	# SET mouseover entries
	if ($show == 'i') {
		$mousei = '';
		$stylei = '';
		$stylef = 'text-decoration:none;';
		$stylem = 'text-decoration:none;';
		$mousef = "onmouseover=\"Tip(ToolTip('$aid','$userid','f',0,event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousem = "onmouseover=\"Tip(ToolTip('$aid','$userid','m',0,event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
	}
	elseif ($show == 'f') {
		$psid = $father;
		$ppid = $ppid;
		$mousei = "onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousef = '';
		$stylei = "text-decoration:none;"; 
		$stylef = '';
		$stylem = "text-decoration:none;";
		$mousem = "onmouseover=\"Tip(ToolTip('$aid','$userid','m',0,event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
	}
	elseif ($show == 'm') {
		$psid = $mother;
		$ppid = $mpid;
		$mousei = "onmouseover=\"Tip(ToolTip('$aid','$userid','i',0,event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousef = "onmouseover=\"Tip(ToolTip('$aid','$userid','f',0,event), EXCLUSIVE, true, BGCOLOR, 'white', CLICKSTICKY, true, CLICKCLOSE, true, OFFSETX, 5, OPACITY, 90)\" onmouseout=\"UnTip()\"";
		$mousem = '';
		$stylei = "text-decoration:none;";
		$stylef = 'text-decoration:none;';
		$stylem = '';
	}
}


// PRINT TITLE  
echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'>";
echo "<h3 style='margin-top:5px'>$region</h3>";
if ($fromimage == 1) {
	// get links to ensembl, ucsc and dgv
	$lquery = mysql_query("SELECT Resource, link FROM db_links WHERE Resource IN ('UCSC','Ensembl','DGV')");
	while ($row = mysql_fetch_array($lquery)) {
		$link = $row['link'];
		if ($row['Resource'] == 'Ensembl') {
			$linkparts = explode('@@@',$link) ;
			if ($size < 1000000) {
				$link = $linkparts[0];
			}
			else {
				$link = $linkparts[1];
			}
		}
		$link = str_replace('%c',$chrtxt,$link);
		$link = str_replace('%s',$start,$link);
		$link = str_replace('%e',$stop,$link);
		$link = str_replace('%r',"chr$chrtxt:$start-$stop",$link);
		$link = str_replace('%u',$ucscdb,$link);
		$links[$row['Resource']] = $link;
	}
	echo "<span style='font-style:italic;font-size:9px;position:absolute;left:400px;top:1px;'>";
	echo "<a class=ttsmall href='".$links['UCSC']."' target='_blank'>UCSC</a><br/>";
	echo "<a class=ttsmall href='".$links['Ensembl']."' target='_blank'>ENSEMBL</a><br/>";
	echo "<a class=ttsmall href='".$links['DGV']."' target='_blank'>DGV</a><br/>";
	echo "</span>";	
}
echo "</div>\n";

// PRINT FAMILY
if ($mother != 0 || $father != 0) {
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center' >";
	echo "<div style='float:left;padding-left:5pt;$stylei' class=nadruk $mousei>INDEX</div>\n";
	echo "<div style='float:right;padding-right:5pt;$stylem' class=nadruk $mousem>MOTHER</div>\n";
	echo "<span style='text-align:center;$stylef' class=nadruk $mousef>FATHER</span>\n";
	echo "</div>";
}
if ($show == 'i') {
	// PRINT CNV DETAILS
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	## LEFT PANEL
	echo "<div style='float:left;width:50%;'>";
	echo "<span class=nadruk>CNV Details</span> $lifted";
	echo " <ul id=ul-simple>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	if ($cn == 9) {
		echo "<li>- UPD-type: $updtype</li>";
	}
	else {
		echo "<li>- Copy Number : $cn</li>";
	}
	echo "<li>- Number of Probes : $nrsnps</li>";
	if ($nrgenes > 0) {
		echo "<li>- Affected Genes : <a class=tt href='index.php?page=genes&chr=$chr&start=$start&stop=$stop' target=new>$nrgenes</a></li>";
	}
	else {
		echo "<li>- Affected Genes : 0</li>";
	}
	echo "<li>- Seen by: <ol>";
	$nrseen = 0;
	if ($seenby != '') {
		foreach ($seenrow as $item) {
			$nrseen++;
			echo "<li>$item</li>";
		}
	}
	else {
		echo "<li>Manually Inserted</li>";
	}
	echo "</ol></li>";
	echo "</ul>";

	// PRINT CLASSIFICATION DETAILS
	echo "<span class=nadruk>CNV Classification</span>";
	if ($editcnv == 1) {
		echo "<span class=italic style='font-size:9px;'> (set class: ";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setclass.php?aid=$aid&class=1&u=$userid&ftt=1')\">1</a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setclass.php?&aid=$aid&class=2&u=$userid&ftt=1')\">2</a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setclass.php?aid=$aid&class=3&u=$userid&ftt=1')\">3</a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setclass.php?aid=$aid&class=4&u=$userid&ftt=1')\">4</a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setclass.php?aid=$aid&class=5&u=$userid&ftt=1')\">FP</a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('deletecnv.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/delete.gif' width=8px height=8px></a>&nbsp;";
		echo "<a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('editcnv.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/edit.gif' width=8px height=8px></a>";
		echo ")</span>";
	}

	echo "<ul id=ul-simple>";
	echo "<li>- Inheritance: ". $inh[$inh[$inheritance]];
	echo "  <span class=italic style='font-size:9px;'> (set: ";
	$setlinks = array('0' => "<a class=ttsmall title='Not Defined' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=0&u=$userid&ftt=1')\">ND</a>,", '1' =>  "<a class=ttsmall title='Paternal' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=1&u=$userid&ftt=1')\">P</a>,", '2' => "<a class=ttsmall title='Maternal' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=2&u=$userid&ftt=1')\">M</a>,", '3' => "<a class=ttsmall title='De Novo' href=\"javascript:void(null)\" onclick=\"return popitup('setinheritance.php?aid=$aid&inh=3&u=$userid&ftt=1')\">DN</a>,"); 
	$setlinks[$inheritance] = '';
	$string = '';
	for ($i = 0; $i<= 3; $i++) {
		$string .= $setlinks[$i];
	}
	$string = substr($string, 0,-1);
	echo $string;
	echo ")</span>";		
	echo "</li>\n";

	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		if ($class == 0) {
			$class = 'Not Defined';
		}
		if ($class == 5) {
			echo "<li>- Diagnostic Class: False Positive <span class=italic style='font-size:9px;'>$setby</span>";
		}
		else {
			echo "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
		}
	}
	else {
		echo "<li>- Diagnostic Class: Not Defined ";
	}
	if ($arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
	}
	echo "</li>\n";
	# validation...
	if ($validation != '') {
		if ($validationdetails != '') {
			$valtitle = $validationdetails;
		}
		else {
			$valtitle= 'No Additional Details Specified';
		}
		echo "<li><span title='$valtitle'>- Validated by $validation </span>";
	}
	else {
		echo "<li>- Not Validated";
	}
	# check log for user who validated 
	$logq = mysql_query("SELECT uid, entry,arguments FROM log WHERE aid = '$aid' ORDER BY time DESC");
	$setby = '';
	$arguments = '';
	while ($logrow = mysql_fetch_array($logq)) {
		if (preg_match('/Validation/', $logrow['entry'])) {
			$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
			$usrow = mysql_fetch_array($usq);
			$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
			$arguments = $logrow['arguments'];
			$logentry = $logrow['entry'];
			break;
		}
	}
	if ($setby != '') {
		echo "<span class=italic style='font-size:9px;'>$setby</span>";
	}
	if ($editcnv == 1) {
		echo " <a class=ttsmall href=\"javascript:void(null)\" onclick=\"return popitup('setvalidation.php?aid=$aid&u=$userid&ftt=1')\"><img src='images/content/edit.gif' width=8px height=8px></a>";
	}
	if ($arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Was adapted:</span> $arguments</div>";
	}
	echo "</li>";	
	if (($mother != 0 && $father != 0) && $cn >= 1 && $cn <= 3) {
		echo "<li>- <a href='index.php?page=PoO&cn=$cn&aid=$aid' target='_blank' class=tt>Check Parent Of Origin</a></li>"; 
	}
	## new: check for de novo deletions/duplications in presence of single parent 
	elseif (($mother != 0 || $father != 0) && $cn == 1) {
		echo "<li>- <a href='index.php?page=PoO&cn=$cn&aid=$aid' target='_blank' class=tt>Check Parent Of Origin</a></li>";
	}
	if ($pubmed != '') {
		echo "<li>- <a class=tt href='index.php?page=literature&type=read&aid=$aid' target='_blank'>Show Associated Publications</a></li>";
	}
	echo "</ul>\n";
	
	echo "</div>";

	## RIGHT PANEL
	echo "<div style='float:right;width:46%;'>";
	// PRINT SAMPLE DETAILS
	$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate FROM projsamp WHERE idsamp = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$lsd = $row['LRRSD'];
	$bsd = $row['BAFSD'];
	$wave = $row['WAVE'];
	$iniwave = $row['INIWAVE'];
	$callrate = $row['callrate'];
	$remark = "<div class=nadruk>Quality Remarks:</div><ul id=ul-simple>";
	$remarkstring = $remark;
	if ($callrate < 0.994) {
		//$cbg = 'style="background:red;color:black"';
		$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
	}
	if ($lsd > 0.2 && $lsd < 0.3) {
		$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
	}
	if ($lsd >=0.3) {
		$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
	}
	if ($bsd >= 0.6) {
		$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
	} 
	if ($iniwave != 0) {
		$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
	}
	$query = mysql_query("select COUNT(id) as aantal FROM aberration WHERE sample = '$sid' AND idproj = '$pid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	
	echo "<span class=nadruk>Sample Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$pid&sample=$sid' target=new>$sample</a></li>\n";
	echo "<li>- Chiptype: $chiptype</li>";
	echo "<li>- Gender : $gender</li>\n";
	echo "<li>- #CNV's : $ncnvs</li>\n";
	if ($remarkstring != $remark) {
		echo "</ul>\n";
		echo "<p>";
		echo $remarkstring;
		echo "</ul></p>";
	}
	else {
		echo "<li>- Quality : OK</li>\n";
		echo "</ul>\n";
	}

	echo "</div>";
	echo "<p></p>";
	echo "</div>";
	ob_flush();
	flush();
	if ($cn == 9) {
		#if ($userid == 1) {
		$styleupd = "text-decoration:underline";
		$styleplot = "text-decoration:none";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$mouseupd = "onmouseover=\"setsubtip('upd','$aid','$userid',event)\"";
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		# 5 entries, each 20%
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linkprobes style='$styleupd' class=nadruk $mouseupd>UPD-GTypes</span></span>\n";
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
		echo "<span style='float:left;width:19%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
		echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		echo "<br/></div>";
		echo "<div id=UPDtable style='padding-left:5px'>";
		echo "<div class=nadruk>Informative Probes for $updtype</div>";
		echo "<p><table cellspacing=0 width='75%'>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
		echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
		$datapoints = explode('@',$updgts);
		foreach($datapoints as $key => $item) {
			$entries = explode(';',$item);
			echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
		}
		echo "</table>";
		echo "</div>";
		echo "<div id=subtip style='display:none'>";
		echo "</div>";
		echo "<div id=plotdiv style='display:none'>";
		$setupd = 1;
		include('inc_create_plot.inc');
		echo "</div>";

		#}
		/*
		else {
		echo "<div class=nadruk>Informative Probes for $updtype</div>";
		echo "<p><table cellspacing=0 width='75%'>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
		echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
		$datapoints = explode('@',$updgts);
		foreach($datapoints as $key => $item) {
			$entries = explode(';',$item);
			echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
		}
		echo "</table>";
		}
		*/
	}
	else {
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$styleplot = "text-decoration:underline";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		# 4 entries, each 25%
		echo "<span style='float:left;width:25%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
		echo "<span style='float:left;width:24%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
		echo "<span style='float:left;width:24%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
		echo "<span style='float:left;width:25%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		echo "<br/></div>";
		echo "<div id=subtip style='display:none'>";
		echo "</div>";
		echo "<div id=plotdiv>";
		$setupd = 0;
		include('inc_create_plot.inc');
		echo "</div>";
		// print following to prevent javascript from crashin on non-existing div/span
		echo "<div id=UPDtable ><span id=linkprobes></span></div>";


	}
}
else {
	// PRINT CNV DETAILS (for cnv from child)
	echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;'> ";
	echo "<div style='float:left;width:48%;'>";
	echo "<span class=nadruk>Offspring CNV Details</span>";
	echo " <ul id=ul-simple>";
	echo "<li>- Sample: $sample</li>";
	echo "<li>- Size: " . number_format($size,0,'',',') . "</li>\n";
	if ($cn == 9) {
		echo "<li>- UPD-type: $updtype</li>";
	}
	else {
		echo "<li>- Copy Number : $cn</li>";
	}
	echo "</ol></li>";
	echo "</ul>";
	// PRINT CLASSIFICATION DETAILS
	echo "<span class=nadruk>Offspring CNV Classification</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Inheritance: ". $inh[$inh[$inheritance]] . "</li>\n";
	$nrlines = 0;
	if ($class != '') {
		$logq = mysql_query("SELECT uid, entry,arguments FROM log WHERE aid = '$aid' ORDER BY time DESC");
		$setby = '';
		while ($logrow = mysql_fetch_array($logq)) {
			if (preg_match('/Diagnostic Class/', $logrow['entry'])) {
				$usq = mysql_query("SELECT FirstName, LastName FROM users WHERE id = ".$logrow['uid']);
				$usrow = mysql_fetch_array($usq);
				$setby = "(" . $usrow['FirstName'] .' '.substr($usrow['LastName'],0,1).".)";
				$arguments = $logrow['arguments'];
				$logentry = $logrow['entry'];
				break;
			}
		}
		if ($class == 5) {
			echo "<li>- Diagnostic Class: False Positive <span class=italic style='font-size:9px;'>$setby</span>";
		}
		else {
			echo "<li>- Diagnostic Class: $class <span class=italic style='font-size:9px;'>$setby</span>";
		}
	}
	else {
		echo "<li>- Diagnostic Class: Not Defined ";
	}
	if ($arguments != '') {
		echo "<br/><div style='margin-left:15px;font-size:9px;width:190px'><span style='text-decoration:underline;font-size:9px;'>Reason:</span> $arguments</div>";
		$nrlines--;
	}
	echo "</li>\n";
	echo "</ul>\n";
	echo" </div>\n";
	// PRINT PARENT DETAILS
	
	$query = mysql_query("SELECT LRRSD, BAFSD, WAVE, INIWAVE, callrate FROM projsamp WHERE idsamp = '$psid' AND idproj = '$ppid'");
	$row = mysql_fetch_array($query);
	$lsd = $row['LRRSD'];
	$bsd = $row['BAFSD'];
	$wave = $row['WAVE'];
	$iniwave = $row['INIWAVE'];
	$callrate = $row['callrate'];
	$remark = "<div class=nadruk>Quality Remarks:</div><ul id=ul-simple>";
	$remarkstring = $remark;
	
	if ($callrate < 0.994) {
		//$cbg = 'style="background:red;color:black"';
		$remarkstring .= "<li>- Low call rate: $callrate</li>\n";
		$nrlines++;
	}
	if ($lsd > 0.2 && $lsd < 0.3) {
		$remarkstring .= "<li>- Elevated LogR-variance: $lsd</li>";
	}
	if ($lsd >=0.3) {
		$remarkstring .= "<li>- Unacceptable LogR-variance: $lsd</li>";
		$nrlines++;
	}
	if ($bsd >= 0.6) {
		$remarkstring .= "<li>- Elevated BAF-variance: $bsd</li>";
		$nrlines++;
	} 
	if ($iniwave != 0) {
		$remarkstring .= "<li>- GC-corrected Sample (wave: $iniwave)</li>";
		$nrlines++;
	}
	$query = mysql_query("select COUNT(id) as aantal FROM aberration WHERE sample = '$psid' AND idproj = '$ppid'");
	$row = mysql_fetch_array($query);
	$ncnvs = $row['aantal'];
	
	$query = mysql_query("select chip_dnanr, gender FROM sample WHERE id = '$psid'");
	$row = mysql_fetch_array($query);
	$parentname = $row['chip_dnanr'];
	$gender = $row['gender'];
	
	$query = mysql_query("SELECT chiptype FROM project WHERE id = '$ppid'");
	$row = mysql_fetch_array($query);
	$chiptype = $row['chiptype'];
	echo "<div style='float:right;width:48%;'>";
	#echo "<div style='position:absolute;left:215px;top:80px;'>";
	echo "<span class=nadruk>Parental Details</span>";
	echo "<ul id=ul-simple>";
	echo "<li>- Sample : <a class=tt href='index.php?page=details&project=$ppid&sample=$psid' target=new>$parentname</a></li>\n";
	echo "<li>- Chiptype: $chiptype</li>";
	echo "<li>- Gender : $gender</li>\n";
	echo "<li>- #CNV's : $ncnvs</li>\n";
	if ($remarkstring != $remark) {
		echo "</ul>\n";
		echo "<p>";
		echo $remarkstring;
		echo "</ul></p>";
	}
	else {
		echo "<li>- Quality : OK</li>\n";
		echo "</ul>\n";
	}
	echo "</div>\n";
	echo "<p></p>";
	echo "</div>";
	ob_flush();
	flush();
	
	if ($cn == 9) {
		#if ($userid == 1) {
		$styleupd = "text-decoration:underline";
		$styleplot = "text-decoration:none";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$mouseupd = "onmouseover=\"setsubtip('upd','$aid','$userid',event)\"";
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$mouseclin = "onmouseover=\"setsubtip('clinical','$sid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$sid','$pid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		# 5 entries, each 20%
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linkprobes style='$styleupd' class=nadruk $mouseupd>UPD-GTypes</span></span>\n";
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
		echo "<span style='float:left;width:19%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
		echo "<span style='float:left;width:19%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
		echo "<span style='float:left;width:20%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		echo "<br/></div>";
		echo "<div id=UPDtable style='padding-left:5px'>";
		echo "<div class=nadruk>Informative Probes for $updtype</div>";
		echo "<p><table cellspacing=0 width='75%'>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
		echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
		$datapoints = explode('@',$updgts);
		foreach($datapoints as $key => $item) {
			$entries = explode(';',$item);
			echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
		}
		echo "</table>";
		echo "</div>";
		echo "<div id=subtip style='display:none'>";
		echo "</div>";
		echo "<div id=plotdiv style='display:none'>";
		$setupd = 1;
		include('inc_create_plot.inc');
		echo "</div>";

	}
	else {
		$mouseplot = "onmouseover=\"setsubtip('plots','$aid','$userid',event)\"";
		$styleplot = "text-decoration:underline";
		$styleclin = 'text-decoration:none;';
		$styletool = 'text-decoration:none;';
		$stylekaryo = 'text-decoration:none;';
		$mouseclin = "onmouseover=\"setsubtip('clinical','$psid','$userid',event)\"";
		$mousetool = "onmouseover=\"setsubtip('tools','$aid','$userid',event)\"";
		$mousekaryo = "onmouseover=\"setsubtip('karyo','$psid','$ppid',event)\"";
		echo "<div class=sectie style='border-bottom-style:solid;border-bottom-width:1px;border-color:darkblue;text-align:center;width:100%' >";
		# 4 entries, each 25%
		echo "<span style='float:left;width:25%;text-align:center;'><span id=linkplot style='$styleplot' class=nadruk $mouseplot>Plots</span></span>\n";
		echo "<span style='float:left;width:24%;text-align:center;'><span id=linkclin style='$styleclin' class=nadruk $mouseclin>Clinical</span></span>";
		echo "<span style='float:left;width:24%;text-align:center;'><span id=linkkaryo style='$stylekaryo' class=nadruk $mousekaryo>KaryoGram</span></span>\n";
		echo "<span style='float:left;width:25%;text-align:center;'><span id=linktool style='$styletool' class=nadruk $mousetool>Public Tools</span></span>\n";
		echo "<br/></div>";
		echo "<div id=subtip style='display:none'>";
		#include('inc_create_plot.inc');
		echo "</div>";
		echo "<div id=plotdiv>";
		$setupd = 0;
		include('inc_create_parent_plot.inc');
		echo "</div>";
		// print following to prevent javascript from crashin on non-existing div/span
		echo "<div id=UPDtable ><span id=linkprobes></span></div>";


	}
	/*	
	if ($cn == 9) {
		echo "<div class=nadruk>Informative Probes for $updtype</div>";
		echo "<p><table cellspacing=0 width='75%'>";
		echo "<tr><th class=topcellalt $firstcell>&nbsp;</th><th class=topcellalt colspan=2>Child</th><th class=topcellalt colspan=2>Father</th><th class=topcellalt colspan=2>Mother</th></tr>";
		echo "<tr><th class=topcellalt $firstcell>Position</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th><th class=topcellalt>GT</th><th class=topcellalt>LogR</th></tr>";
		$datapoints = explode('@',$updgts);
		foreach($datapoints as $key => $item) {
			$entries = explode(';',$item);
			echo "<tr><td $firstcell>".number_format($entries[0],0,'',',')."</td><td>".$entries[1]."</td><td NOWRAP>".$upddata[$sid][$entries[0]]['logR'] ."</td><td>".$entries[2]."</td><td NOWRAP>".$upddata[$updfid][$entries[0]]['logR'] ."</td><td NOWRAP>".$upddata[$updmid][$entries[0]]['logR'] ."</td><td>".$entries[3]."</td></tr>";
		}
		echo "</table>";

	}
	else {

	include('inc_create_parent_plot.inc');

	}
	*/
	
}
//echo "</div>";	
?>

