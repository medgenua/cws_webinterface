<?php
class XmlToArray 
{ 
    
    var $xml=''; 
    
    /** 
    * Default Constructor 
    * @param $xml = xml data 
    * @return none 
    */ 
    
    function XmlToArray($xml) 
    { 
       $this->xml = $xml;    
    } 
    
    /** 
    * _struct_to_array($values, &$i) 
    * 
    * This is adds the contents of the return xml into the array for easier processing. 
    * Recursive, Static 
    * 
    * @access    private 
    * @param    array  $values this is the xml data in an array 
    * @param    int    $i  this is the current location in the array 
    * @return    Array 
    */ 
    
    function _struct_to_array($values, &$i) 
    { 
        $child = array(); 
        if (isset($values[$i]['value'])) array_push($child, $values[$i]['value']); 
        
        while ($i++ < count($values)) { 
            switch ($values[$i]['type']) { 
                case 'cdata': 
                array_push($child, $values[$i]['value']); 
                break; 
                
                case 'complete': 
                    $name = $values[$i]['tag']; 
                    if(!empty($name)){ 
                    	$child[$name]= ($values[$i]['value'])?($values[$i]['value']):''; 
                    	#if(isset($values[$i]['attributes'])) {                    
                    	#    $child[$name] .= $values[$i]['attributes']; 
                    	#} 
                    }    
              	    break; 
                
                case 'open': 
                    $name = $values[$i]['tag']; 
                    $size = isset($child[$name]) ? sizeof($child[$name]) : 0; 
                    $child[$name][$size] = $this->_struct_to_array($values, $i); 
                break; 
                
                case 'close': 
                return $child; 
                break; 
            } 
        } 
        return $child; 
    }//_struct_to_array 
    
    /** 
    * createArray($data) 
    * 
    * This is adds the contents of the return xml into the array for easier processing. 
    * 
    * @access    public 
    * @param    string    $data this is the string of the xml data 
    * @return    Array 
    */ 
    function createArray() 
    { 
        $xml    = $this->xml; 
        $values = array(); 
        $index  = array(); 
        $array  = array(); 
        $parser = xml_parser_create(); 
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1); 
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0); 
        xml_parse_into_struct($parser, $xml, $values, $index); 
        xml_parser_free($parser); 
        $i = 0; 
        $name = $values[$i]['tag'];
        $array[$name] = isset($values[$i]['attributes']) ? $values[$i]['attributes'] : ''; 
        $array[$name] = $this->_struct_to_array($values, $i); 
        return $array; 
    }//createArray 
    
    
}//XmlToArray 


function my_xml2array($__url) 
{ 
    $xml_values = array(); 
    $contents = file_get_contents($__url); 
    $parser = xml_parser_create(''); 
    if(!$parser) 
        return false; 

    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, 'UTF-8'); 
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0); 
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1); 
    xml_parse_into_struct($parser, trim($contents), $xml_values); 
    xml_parser_free($parser); 
    if (!$xml_values) 
        return array(); 
    
    $xml_array = array(); 
    $last_tag_ar =& $xml_array; 
    $parents = array(); 
    $last_counter_in_tag = array(1=>0); 
    foreach ($xml_values as $data) 
    { 
        switch($data['type']) 
        { 
            case 'open': 
                $last_counter_in_tag[$data['level']+1] = 0; 
                $new_tag = array('name' => $data['tag']); 
                if(isset($data['attributes'])) 
                    $new_tag['attributes'] = $data['attributes']; 
                if(isset($data['value']) && trim($data['value'])) 
                    $new_tag['value'] = trim($data['value']); 
                $last_tag_ar[$last_counter_in_tag[$data['level']]] = $new_tag; 
                $parents[$data['level']] =& $last_tag_ar; 
                $last_tag_ar =& $last_tag_ar[$last_counter_in_tag[$data['level']]++]; 
                break; 
            case 'complete': 
                $new_tag = array('name' => $data['tag']); 
                if(isset($data['attributes'])) 
                    $new_tag['attributes'] = $data['attributes']; 
                if(isset($data['value']) && trim($data['value'])) 
                    $new_tag['value'] = trim($data['value']); 

                $last_count = count($last_tag_ar)-1; 
                $last_tag_ar[$last_counter_in_tag[$data['level']]++] = $new_tag; 
                break; 
            case 'close': 
                $last_tag_ar =& $parents[$data['level']]; 
                break; 
            default: 
                break; 
        }; 
    } 
    return $xml_array; 
} 

// 
// use this to get node of tree by path with '/' terminator 
// 
function get_value_by_path($__xml_tree, $__tag_path) 
{ 
    $tmp_arr =& $__xml_tree; 
    $tag_path = explode('/', $__tag_path); 
    foreach($tag_path as $tag_name) 
    { 
        $res = false; 
        foreach($tmp_arr as $key => $node) 
        { 
            if(is_int($key) && $node['name'] == $tag_name) 
            { 
                $tmp_arr = $node; 
                $res = true; 
                break; 
            } 
        } 
        if(!$res) 
            return false; 
    } 
    return $tmp_arr; 
}

?>
